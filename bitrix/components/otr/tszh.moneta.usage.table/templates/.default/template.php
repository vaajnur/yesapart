<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2017 otr@rarus.ru
 * @date 15.09.2017 11:24
 */

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

$this->setFrameMode(false);

/*if (isset($arResult['ERRORS']) && count($arResult['ERRORS']) > 0)
{
	ShowError(implode('<br/>', $arResult['ERRORS']));
}*/

// $component->showError();
// $this->__component->showError();
// Bitrix\Main\Diag\Debug::dump($arResult);
?>
<div class="moneta-usage-form" id="moneta-usage-form">
	<?php
	if (isset($arResult['ERRORS']) && count($arResult['ERRORS']) > 0)
	{
		ShowError(implode('<br/>', $arResult['ERRORS']));
	}
	?>
	<div class="moneta-usage-block<?=$arResult['MONETA_OFFER'] ? ' moneta-usage-block_active' : ''?>">
		<div class="moneta-usage-block__title">
			<div class="moneta-usage-block__title-text">
				<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_COMISSION_RECEPIENT');?>
			</div>
		</div>
		<div class="moneta-usage-block__status">
			<button class="moneta-usage-block__activate-button<?=$arResult['MONETA_OFFER'] ? ' element-hide' : ''?>" id="recepient">
				<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_BUTTON_ACTIVATE');?>
			</button>
			<div class="moneta-usage-block__status-activated moneta-usage-block__status-activated_use<?=!$arResult['MONETA_OFFER'] ? ' element-hide' : ''?>">
				<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_STATUS_USE')?>
			</div>
		</div>
		<div class="moneta-usage-block__descr">
			<div class="moneta-usage-block__descr-text">
				<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_COMISSION_RECEPIENT_INFO');?>
			</div>
			<div class="moneta-usage-block__example">
				<div>
					<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_EXAMPLE');?>:
				</div>
				<div><span class="text-strong"><?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_SUM');?>:</span>
					100.00 <?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_RUB');?></div>
				<div><span class="text-strong"><?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_TOTAL');?>:</span>
					100.00 <?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_RUB');?></div>
			</div>
		</div>
		<div class="moneta-usage-request">
			<?php
			if (!$arResult['MONETA_OFFER_ADDITITIONAL_DATA'])
			{
				echo Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_FILL_REQUEST');
			}
			else
			{
				echo Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_FILLED_REQUEST');
			}
			?>
		</div>
		<div class="moneta-usage-activation-scheme">
			<table class="moneta-usage-activation-scheme__table">
				<tr>
					<td class="moneta-usage-activation-scheme__arrow-cell">
						<!--<div class="moneta-usage-activation-scheme__arrow moneta-usage-activation-scheme__arrow_done"></div>-->
						<div class="moneta-usage-activation-scheme__arrow <?=($arResult['MONETA_OFFER_DATA']['STATUS'] == 'N' ? 'moneta-usage-activation-scheme__arrow_active' : ($arResult['MONETA_OFFER_DATA']['STATUS'] > 'N' ? 'moneta-usage-activation-scheme__arrow_done' : 'moneta-usage-activation-scheme__arrow_not-active'))?>"></div>
					</td>
					<td class="moneta-usage-activation-scheme__info-cell">
						<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_OFFER_STATUS_NOT_USE')?>
					</td>
				</tr>
				<tr>
					<td class="moneta-usage-activation-scheme__arrow-cell">
						<!--<div class="moneta-usage-activation-scheme__arrow moneta-usage-activation-scheme__arrow_active">-->
						<div class="moneta-usage-activation-scheme__arrow <?=($arResult['MONETA_OFFER_DATA']['STATUS'] == 'V' ? 'moneta-usage-activation-scheme__arrow_active' : ($arResult['MONETA_OFFER_DATA']['STATUS'] > 'V' ? 'moneta-usage-activation-scheme__arrow_done' : 'moneta-usage-activation-scheme__arrow_not-active'))?>">
							<div class="moneta-usage-activation-scheme__arrow-line element-hide"></div>
						</div>
					</td>
					<td class="moneta-usage-activation-scheme__info-cell">
						<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_OFFER_STATUS_ON_VERIFICATION')?>
					</td>
				</tr>
				<tr>
					<td class="moneta-usage-activation-scheme__arrow-cell">
						<!--<div class="moneta-usage-activation-scheme__arrow moneta-usage-activation-scheme__arrow_not-active">-->
						<div class="moneta-usage-activation-scheme__arrow <?=($arResult['MONETA_OFFER_DATA']['STATUS'] == 'W' ? 'moneta-usage-activation-scheme__arrow_active' : ($arResult['MONETA_OFFER_DATA']['STATUS'] > 'W' ? 'moneta-usage-activation-scheme__arrow_done' : 'moneta-usage-activation-scheme__arrow_not-active'))?>">
							<div class="moneta-usage-activation-scheme__arrow-line"></div>
						</div>
					</td>
					<td class="moneta-usage-activation-scheme__info-cell">
						<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_OFFER_STATUS_WITHOUT_DOCS')?>
					</td>
				</tr>
				<tr>
					<td class="moneta-usage-activation-scheme__arrow-cell">
						<!--<div class="moneta-usage-activation-scheme__arrow moneta-usage-activation-scheme__arrow_not-active">-->
						<div class="moneta-usage-activation-scheme__arrow <?=($arResult['MONETA_OFFER_DATA']['STATUS'] == 'Y' ? 'moneta-usage-activation-scheme__arrow_done' : 'moneta-usage-activation-scheme__arrow_not-active')?>">
							<div class="moneta-usage-activation-scheme__arrow-line"></div>
						</div>
					</td>
					<td class="moneta-usage-activation-scheme__info-cell">
						<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_OFFER_STATUS_ACTIVE')?>
					</td>
				</tr>
				<tr>
					<td class="moneta-usage-activation-scheme__arrow-cell">
						<div class="moneta-usage-activation-scheme__arrow <?=($arResult['MONETA_OFFER_DATA']['STATUS'] == 'Y' ? 'moneta-usage-activation-scheme__arrow_active' : 'moneta-usage-activation-scheme__arrow_not-active')?>"></div>
						<!--<div class="moneta-usage-activation-scheme__arrow">
							<div class="moneta-usage-activation-scheme__arrow-line"></div>
						</div>-->
					</td>
					<td class="moneta-usage-activation-scheme__info-cell">
						<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_STATUS_USE')?>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="moneta-usage-block<?=$arResult['MONETA_NO_OFFER'] ? ' moneta-usage-block_active' : ''?>">
		<div class="moneta-usage-block__title">
			<div class="moneta-usage-block__title-text">
				<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_COMISSION_PAYER');?>
			</div>
		</div>
		<div class="moneta-usage-block__status">
			<button class="moneta-usage-block__activate-button<?=$arResult['MONETA_NO_OFFER'] ? ' element-hide' : ''?>" id="payer">
				<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_BUTTON_ACTIVATE');?>
			</button>
			<div class="moneta-usage-block__status-activated moneta-usage-block__status-activated_use<?=!$arResult['MONETA_NO_OFFER'] ? ' element-hide' : ''?>">
				<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_STATUS_USE')?>
			</div>
		</div>
		<div class="moneta-usage-block__descr">
			<div class="moneta-usage-block__descr-text">
				<?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_COMISSION_PAYER_INFO');?>
			</div>
			<div class="moneta-usage-block__example">
				<div><?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_EXAMPLE');?>:</div>
				<div><span class="text-strong"><?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_SUM');?>:</span>
					100.00 <?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_RUB');?></div>
				<div><span class="text-small moneta-usage-block__commission"><?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_COM');?> 1.8%</span>
				</div>
				<div><span class="text-strong"><?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_TOTAL');?>:</span>
					101.80 <?=Loc::getMessage('TSZH_MONETA_USAGE_TABLE_TEMPLATE_RUB');?></div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    var arResult = <?=CUtil::PhpToJSObject($arResult)?>;
    var arParams = <?=CUtil::PhpToJSObject($arParams)?>;
    var componentTemplatePath = '<?=CUtil::JSEscape($templateFolder)?>';
    var componentPath = '<?=CUtil::JSEscape($componentPath)?>';
    BX.message({
        'MONETA_OFFER_ADDITITIONAL_INFO_POPUP_CONTENT': '<?=CUtil::JSEscape(Loc::getMessage('MONETA_OFFER_ADDITITIONAL_INFO_POPUP_CONTENT'))?>',
        'MONETA_OFFER_ADDITITIONAL_INFO_POPUP_TITLEBAR': '<?=CUtil::JSEscape(Loc::getMessage('MONETA_OFFER_ADDITITIONAL_INFO_POPUP_TITLEBAR'))?>',
        'MONETA_OFFER_ADDITITIONAL_INFO_DIALOG_TITLE': '<?=CUtil::JSEscape(Loc::getMessage('MONETA_OFFER_ADDITITIONAL_INFO_DIALOG_TITLE'))?>',
        'MONETA_OFFER_ADDITITIONAL_INFO_PARTNER_AGREEMENT_DIALOG_TITLE': '<?=CUtil::JSEscape(Loc::getMessage('MONETA_OFFER_ADDITITIONAL_INFO_PARTNER_AGREEMENT_DIALOG_TITLE'))?>',
    });
</script>