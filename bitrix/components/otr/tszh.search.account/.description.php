<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

$arComponentDescription = array(
	"NAME" => Loc::getMessage("TSZH_SEARCH_ACCOUNT_NAME"),
	"DESCRIPTION" => Loc::getMessage("TSZH_SEARCH_ACCOUNT_DESCRIPTION"),
	"CACHE_PATH" => "Y",
);

?>