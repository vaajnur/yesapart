<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CUtil::InitJSCore(array('chart'));
CUtil::InitJSCore(array('chartjspluginzoom'));

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs("/bitrix/components/otr/tszh.chart/templates/.default/js/script_for_popup.js");

if ($arParams["ANIMATION_EASING"] == "default")
    $arParams[ "ANIMATION_EASING"] = (($arParams["CHART_TYPE"] == "CHART_DEBT") || ($arParams["CHART_TYPE"] == "CHART_RATE")) ? "easeOutQuart" : "easeOutBounce";

$arParams['CHART_TOOLTIPS_CURRENCY'] = GetMessage("CHART_TOOLTIPS_CURRENCY");
$arParams['CHART_TOOLTIPS_CURRENCY_RATE'] = GetMessage("CHART_TOOLTIPS_CURRENCY_RATE");

if (($arParams["ARCHART_VAL"]) && ($arParams["ARCHART_DATE"]) && ($arParams["ARCHART_CAPTION"])) {

    $arParams["CHART_ID"] = $arResult["popchart_id"];

    if ($arParams["CHART_TYPE"] == "CHART_DEBT") {
        $arParams["CHART_TITLE"] = GetMessage("TSZH_CHART_DEBT");
        $arParams["CITRUS_TSZH_PERIOD"] = GetMessage("TSZH_SHEET_PERIOD");
        $arParams["CITRUS_TSZH_CONSUMPTION"] = GetMessage("TSZH_SHEET_DEBT_END");
        $arParams["CHART_TYPES"] =  "line";
        $arParams["BACKGROUNDCOLOR_COLORS"] = "#fbcd74";
        $arParams["CHART_COLORS"] =  "#d76008";
    }

    if ($arParams["CHART_TYPE"] == "CHART_SUMM_SUMMPAYED") {
        $arParams["CHART_TITLE"] = GetMessage("TSZH_CHART_SUMM_SUMMPAYED");
        $arParams["CITRUS_TSZH_PERIOD"] = GetMessage("TSZH_SHEET_PERIOD");
        $arParams["CITRUS_TSZH_CONSUMPTION"] =  GetMessage("TSZH_SHEET_SUMM_ALL");
        $arParams["CHART_TYPES" ] = "line|bar";
        $arParams["CHART_COLORS"] = "#ffc312,#4cd137";
    }

    if ($arParams["CHART_TYPE"] == "CHART_SUMM") {
        $arParams["CHART_TITLE"] = GetMessage("TSZH_CHART_SUMM");
        $arParams["CITRUS_TSZH_PERIOD"] =  GetMessage("TSZH_SHEET_PERIOD");
        $arParams["CITRUS_TSZH_CONSUMPTION"] = GetMessage("TSZH_SHEET_SUMM");
        $arParams["CHART_TYPES"] =  "bar";
        $arParams["CHART_COLORS"] = "#ffc312,#c4e538,#12cbc4,#ed4c67,#f79f1f,#a3cb38,#1289a7,#b53471,#ee5a24,#009432,#0652dd,#833471,#9980fa,#006266,#fda7df";
    }

    if ($arParams["CHART_TYPE"] == "CHART_PIE") {
        $arParams["CHART_TITLE"] = GetMessage("TSZH_CHART_PIE");
        $arParams["CITRUS_TSZH_PERIOD"] = GetMessage("TSZH_SHEET_PERIOD");
        $arParams["CHART_TYPES"] = "pie";
        $arParams["CHART_COLORS"] = "#ffc312,#c4e538,#12cbc4,#ed4c67,#f79f1f,#a3cb38,#1289a7,#b53471,#ee5a24,#009432,#0652dd,#833471,#9980fa,#006266,#fda7df";
        $arParams["PIE_SUMM_LAST_YEAR"] = GetMessage("PIE_SUMM_LAST_YEAR");
        $arParams["PIE_SUMM_CURRENT"] = GetMessage("PIE_SUMM_CURRENT");
    }

    if ($arParams["CHART_TYPE"] == "CHART_RATE") {
        $arParams["CHART_TITLE"] = GetMessage("TSZH_CHART_RATE");
        $arParams["CITRUS_TSZH_PERIOD"] = GetMessage("TSZH_SHEET_PERIOD");
        $arParams["CITRUS_TSZH_CONSUMPTION"] = GetMessage("TSZH_SHEET_RATE");
        $arParams["CHART_TYPES"] = "line";
        $arParams["CHART_COLORS"] = "#37b5f8,#ffc312,#1dd989,#f79f1f,#f7516f,#12cbc4,#f18f4e,#c4e538,#9085ba,#a3cb38,#ffc312,#12cbc4,#c4e538,#ed4c67,#f79f1f,#a3cb38,#1289a7,#b53471,#ee5a24,#009432,#0652dd,#833471,#9980fa,#006266";
    }

    if ($arParams["CHART_TYPE"] == "CHART_RATE") {
        foreach ($arParams["CHART_RATE"] as $key => $service) {
             $arParams["ARCHART_VAL"] = $service["ARCHART_VAL"];
             $arParams["ARCHART_DATE"] = $service ["ARCHART_DATE"];
             $arParams["ARCHART_CAPTION"]= $service["ARCHART_LEGEND"];
             $arParams["ARCHART_CAPTION_SERVICE"]= $service["ARCHART_CAPTION"];
            break;        }
    }
    ?>

    <div class="chart_container">
        <b><p align="center" class="ptitlechart"> <?= $arParams["CHART_TITLE"]; ?> <a href="javascript:void(0)"
                                                                                      window="chart_help_<?= $arParams["CHART_TYPE"]; ?>"
                                                                                      class="window-open_chart rollover"> </a>
            </p></b>

        <div class="shadow_chart hidden"></div>
        <div class="window_chart hidden" id="chart_help_<?= $arParams["CHART_TYPE"]; ?>">
            <div class="window__close_chart">X</div>
            <p class="chart_description ptitlechart"> <?= GetMessage("TSZH_CHART_DESCRIPTION_" . $arParams["CHART_TYPE"]); ?> </p>
        </div>

        <?
        if ($arParams["CHART_TYPE"] == "CHART_RATE") {
            // ���������� ������ ��� ������ ��������

            ?>
            <form id="form_chart_rate_button" action="#">
                <div align="center"> <?
                    foreach ($arParams["CHART_RATE"] as $key => $service) {
                        $arParams["ARCHART_VAL"] = $service["ARCHART_VAL"];
                        $arParams["ARCHART_DATE"] = $service ["ARCHART_DATE"];
                        $arParams["ARCHART_CAPTION"] = $service["ARCHART_LEGEND"];
                        $arParams["ARCHART_CAPTION_SERVICE"] = $service["ARCHART_CAPTION"];
                        if ($key != 0) $arParams["CHART_COLORS"] = substr(stristr(substr(stristr($arParams["CHART_COLORS"], ','), 1), ','), 1);
                        $CHART_COLORS = explode(',', $arParams["CHART_COLORS"]);
                        ?>
                        <button type="button" class="rate-btn" style="background:<?
                        echo $CHART_COLORS[0]; ?>; border-color:<?
                        echo $CHART_COLORS[0]; ?> " id= <?
                        echo "button-" . $key; ?>  data = '<?
                        echo \Bitrix\Main\Web\Json::encode($arParams, $options = null); ?>'>
                        <span class="icon-btn-round">
                            <span class="icon-btn-round-current current-white" style="background:<?
                            echo $CHART_COLORS[0]; ?>">
                            </span>
                        </span>
                        <div class="textdiv"> <? echo $arParams["ARCHART_CAPTION_SERVICE"]; ?></div>
                        </button>
                        <?
                    }
                    ?>
                </div>
            </form>

            <script>  document.getElementById(<?echo \Bitrix\Main\Web\Json::encode("button-" . $key, $options = null);?>).querySelector("span").querySelector("span").className = "icon-btn-round-current"; </script>

            <?

        }
        ?>
        <div>
            <canvas height="200" class="canvaschart" id= <?
            echo \Bitrix\Main\Web\Json::encode($arResult["popchart_id"], $options = null); ?>></canvas>
        </div>
    </div>
    <script>
        arParams = <?echo \Bitrix\Main\Web\Json::encode($arParams, $options = null);?>;
        chart_start();
    </script>
    <?
}
?>