$(document).ready(function() {
    /*** ������� �� ������������ ������ ��� ������� �� ������������� start ***/
    var switchButtons = $(".radio-switcher a input, .radio-switcher a label");
    switchButtons.each(function(){
        $(this).click(function(){
            var parEl = $(this).parent();
            var onclickVal = parEl.attr('onclick');
            if(onclickVal == null || onclickVal.indexOf('BX.ajax') == -1){
                var href = parEl.attr('href');
                if(href && href != "")
                    document.location.href = href;
            }
        });
    });

    /*** ������� �� ������������ ������ ��� ������� �� ������������� end ***/
    BX.ready(function() {
        function initWindow(){
            var shadow = $(".shadow_chart");
            var openLinks = $(".window-open_chart"), close = $(".window__close_chart,.window-close_chart"), windowEls = $(".window_chart");
            openLinks.each(function(){
                if($(this).attr("init")==true)
                    return;
                $(this).attr("init",true);
                $(this).click(function(){
                    shadow.hide();
                    windowEls.hide();
                    var windowEl = $('#' + $(this).attr('window'));
                    if(windowEl.is(':hidden')){
                        windowEl.show();
                        shadow.show();
                        $(document).scrollTop(windowEl.offset().top - window.innerHeight/2);
                    }
                    else{
                        windowEl.hide();
                        shadow.hide();
                    }
                })
            });
            close.each(function(){
                if($(this).attr("init")==true)
                    return;
                $(this).click(function(){
                    $(this).closest(".window_chart").hide();
                    shadow.hide();
                });
            });
            if(!shadow.attr("init")){
                shadow.attr("init",true);
                shadow.click(function() {
                    shadow.hide();
                    windowEls.hide();
                });
            }
        }

        initWindow();
    });
});




