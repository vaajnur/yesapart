<?
$MESS["TSZH_CHART_TSZH_MODULE_NOT_FOUND_ERROR"] = "Модуль «1С: Сайт ЖКХ» не установлен.";
$MESS["TSZH_CHART_MODULE_NOT_FOUND_ERROR"] = "Модуль «Графики для 1С: Сайта ЖКХ» не установлен.";
$MESS["TSZH_CHART_MODULE_NOT_INCLUDED"] = "Модуль «Графики для 1С: Сайта ЖКХ» не доступен в данной редакции продукта «1С: Сайт ЖКХ»";

$MESS["TSZH_NOT_A_MEMBER"] = "Вы не являетесь владельцем лицевого счета.";
$MESS['ACCESS_DENIED'] = "Доступ запрещен";

$MESS["TSZH_NO_DATA"] = "Нет данных о периодах ";
$MESS["TSZH_NO_METER"] = "Нет данных о счетчиках или показаниях ";
$MESS["TSZH_NO_ACCOUNT"] = "Нет данных о лицевом счете ";
$MESS['TSZH_SHEET_NO_DATA'] = "Нет данных ";

$MESS['TSZH_SHEET_SUMM'] = "Начислено";
$MESS['TSZH_SHEET_SUMMPAYED'] = "Оплачено";
$MESS['TSZH_SHEET_DEBT'] = "Задолженность";

$MESS['TSZH_GRAFIC'] = "для построения графика \"";

?>