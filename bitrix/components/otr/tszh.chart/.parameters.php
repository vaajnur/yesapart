<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$legend_position = Array(
    "top" => GetMessage("TSZH_CHART_LEGEND_POSITION_TOP"),
    "left" => GetMessage("TSZH_CHART_LEGEND_POSITION_LEFT"),
    "bottom" => GetMessage("TSZH_CHART_LEGEND_POSITION_BOTTOM"),
    "right" => GetMessage("TSZH_CHART_LEGEND_POSITION_RIGHT"),
);
$animation_easing = Array(
    "default" => "default",
    "easeOutQuart" => "easeOutQuart",
    "easeOutBounce" => "easeOutBounce"

);
$chartType = Array(
    "CHART_DEBT" => GetMessage("TSZH_CHART_DEBT"),
    "CHART_SUMM" => GetMessage("TSZH_CHART_SUMM"),
    "CHART_SUMM_SUMMPAYED" => GetMessage("TSZH_CHART_SUMM_SUMMPAYED"),
    "CHART_RATE" => GetMessage("TSZH_CHART_RATE"),
    "CHART_PIE" => GetMessage("TSZH_CHART_PIE"),
);
$arComponentParameters = array(
	"GROUPS" => array(
        "PARAMETERS_CHART" => array(
            "NAME" => GetMessage("TSZH_CHART_PARAMETERS"),
        ),
        "OPTIONS_LEGEND" => array(
            "NAME" => GetMessage("TSZH_CHART_OPTIONS_LEGEND"),
        ),
        "OPTIONS_ANIMATION" => array(
            "NAME" => GetMessage("TSZH_CHART_OPTIONS_ANIMATION"),
        ),
	),
	"PARAMETERS" => array(
// ==============================================PARAMETERS_CHART
        "CHART_TYPE" => array(
            "NAME" => GetMessage("TSZH_CHART_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $chartType,
            "MULTIPLE" => "N",
            "DEFAULT" => "CHART_DEBT",
            "PARENT" => "PARAMETERS_CHART",
        ),
   /*     "CHART_COLORS" => array(
            "NAME" => GetMessage("TSZH_CHART_COLORS"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
            "PARENT" => "PARAMETERS_CHART",
        ),
   */
        "CHART_ZOOM" => array(
            "NAME" => GetMessage("TSZH_CHART_ZOOM"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
            "REFRESH" => "Y",
            "PARENT" => "PARAMETERS_CHART",
        ),
// ==============================================OPTIONS_LEGEND
        "LEGEND_POSITION" => array(
            "NAME" => GetMessage("TSZH_CHART_LEGEND_POSITION"),
            "TYPE" => "LIST",
            "VALUES" => $legend_position,
            "MULTIPLE" => "N",
            "DEFAULT" => "bottom",
            "PARENT" => "OPTIONS_LEGEND",
        ),
// ==============================================OPTIONS_ANIMATION
        "ANIMATION_EASING" => array(
            "NAME" => GetMessage("TSZH_CHART_ANIMATION_EASING"),
            "TYPE" => "LIST",
            "VALUES" => $animation_easing,
            "MULTIPLE" => "N",
            "DEFAULT" => $animation_easing["default"],
            "PARENT" => "OPTIONS_ANIMATION",
        ),
        "ANIMATION_DURATION" => array(
            "NAME" => GetMessage("TSZH_CHART_ANIMATION_DURATION"),
            "TYPE" => "INTEGER",
            "DEFAULT" => "3000",
            "PARENT" => "OPTIONS_ANIMATION",
        ),
    ),
);

?>