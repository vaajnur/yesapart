<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (!CModule::IncludeModule("vdgb.tszhepasport"))
	die("failure\n" . GetMessage("VDGB_TSZH_EPASPORT_1C_ERROR_MODULE"));

$exchangeFacade = new \Vdgb\TszhEPasport\Exchange\Facade($params["SKIP_PERMISSION_CHECK"] == "Y");
$exchangeFacade->handleRequest($_GET["mode"], $_GET['type']);
