<?
$MESS["TSZH_MODULE_NOT_FOUND_ERROR"] = "Модуль 1С: Сайт ЖКХ не установлен.";
$MESS["TSZH_NOT_A_MEMBER"] = "Вы не являетесь владельцем лицевого счета.";
$MESS["TSZH_NO_DATA"] = "Нет данных о периодах.";
$MESS["MKD"] = "МКД";
$MESS["GD"] = "ЖД";
$MESS["VDGB_HOUSES_WITH_INCORRECT_COORDINATES"] = "У некоторых домов координаты определены некорректно. <a target='_blank' href='/bitrix/admin/tszh_house_list.php?set_filter=Y&adm_filter_applied=0&find_types=ID&find_ep_latitude_from=0&find_ep_latitude_to=0'>Показать список домов</a>";
?>