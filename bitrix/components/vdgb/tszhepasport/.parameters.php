<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"CACHE_TIME" => Array(
			"DEFAULT" => 300
		),
		/*"FILE" => array(
			"PARENT" => "BASE",
			"NAME" => "���� ������",
			"TYPE" => "FILE",
			"FD_TARGET" => "F",
			"FD_EXT" => "png,gif,jpg,jpeg",
			"DEFAULT" => "��� �����"
		),*/
		"YANDEX_CLUSTERIZATION" => Array(
			"PARENT" => "BASE", //������ ��������
			"NAME" => GetMessage("USE_YMAP_CLUSTERIZATION"), //��������
			"TYPE" => "CHECKBOX", //��� ����
			"DEFAULT" => "Y"
		),
		"MAPCONTROLS" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MAP_CONTROLS"),
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"VALUES" => array(
				"searchControl" => GetMessage("SEARCHCONTROL"),
				"trafficControl" => GetMessage("TRAFFICCONTROL"),
				"zoomControl" => GetMessage("ZOOMCONTROL"),
				"typeSelector" => GetMessage("TYPESELECTOR"),
				"rulerControl" => GetMessage("RULERCONTROL"),
				"fullscreenControl" => GetMessage("FULLSCREENCONTROL"),
				"geolocationControl" => GetMessage("GEOLOCATIONCONTROL")
			),
			"SIZE" => 6,
			"ADDITIONAL_VALUES" => "N"
		)
	),
);

if (CModule::IncludeModule("iblock"))
{
	CIBlockParameters::AddPagerSettings($arComponentParameters, GetMessage("COMP_NAV_PAGER"), true, true);
}
?>