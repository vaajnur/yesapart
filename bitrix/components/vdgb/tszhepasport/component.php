<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!CModule::IncludeModule("vdgb.tszhepasport"))
{
	ShowError(GetMessage("PASPORT_NO_MODULE"));

	return false;
}
else
{
	$componentPage = "all";
	if ($_REQUEST["home-id"])
	{
		$componentPage = "detail";
	}
	$this->IncludeComponentTemplate($componentPage);
}
?>