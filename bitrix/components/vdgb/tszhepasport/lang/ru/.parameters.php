<?
$MESS["COMP_NAV_PAGER"] = "Периоды";
$MESS["COMP_MAX_COUNT"] = "Количество периодов на одной странице";
$MESS["RECEIPT_URL"] = "Путь к квитанции за период";
$MESS["CITRUS_TSZH_SHEET_SHOW_SERVICE_CORRECTIONS"] = "Отображать столбец перерасчетов в списке услуг";
$MESS["USE_YMAP_CLUSTERIZATION"] = "Использовать кластеризацию яндекс-карт";
$MESS["MAP_CONTROLS"] = "Настройки карты";
$MESS["SEARCHCONTROL"] = "Панель поиска";
$MESS["TRAFFICCONTROL"] = "Панель пробок";
$MESS["ZOOMCONTROL"] = "Управление масштабированием";
$MESS["TYPESELECTOR"] = "Панель переключения типа карты";
$MESS["RULERCONTROL"] = "Рулетка";
$MESS["FULLSCREENCONTROL"] = "Во весь экран";
$MESS['GEOLOCATIONCONTROL'] = "Определить местоположение";

?>