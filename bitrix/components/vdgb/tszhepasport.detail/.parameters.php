<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS"  =>  array(		
		"CACHE_TIME"  =>  Array("DEFAULT"=>300),
	),
);

if (CModule::IncludeModule("iblock"))
	CIBlockParameters::AddPagerSettings($arComponentParameters, GetMessage("COMP_NAV_PAGER"), true, true);


?>