<?
$MESS["TSZH_MODULE_NOT_FOUND_ERROR"] = "Модуль 1С: Сайт ЖКХ не установлен.";
$MESS["TSZH_NOT_A_MEMBER"] = "Вы не являетесь владельцем лицевого счета.";
$MESS["TSZH_NO_DATA"] = "Нет данных о периодах.";
$MESS ['PASSPORT_NOT_FOUND'] = "Паспорт не найден";
$MESS['TYPE_OF_HOUSE'] = "Тип дома: ";
$MESS["SUMM_COM_AREA"] = "Общая площадь: ";
$MESS["FLOORS"] = "Количество этажей: ";
$MESS["PORCHES"] = "Количество подъездов: ";
$MESS["YEAR_OF_BUILT"] = "Год постройки: ";
$MESS["YEAR_OF_COMMISSIONING"] = "Год начала обслуживания дома: ";
$MESS["DETAIL"] = "Подробная информация";
$MESS["HOUSE"] = "Дом ";
$MESS["MKD"] = "МКД";
$MESS["GD"] = "ЖД";
$MESS["MAIN_HOUSE_INFORMATION"] = "Общая информация о доме";
?>