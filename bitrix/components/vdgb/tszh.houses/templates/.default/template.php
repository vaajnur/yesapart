<?php
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
?>

<table class="houses-list">
	<?
	echo "<thead><tr>";
	foreach ($arParams["SELECT_FIELDS"] as $fieldName)
	{
		echo "<th class='main-bg'>" . Loc::getMessage('TSZH_HOUSE_' . $fieldName) . "</th>";
	}
	echo "</tr></thead>";
	foreach ($arResult["TSZH"] as $tszh_id =>  $arTszh)
	{
		echo "<tr>";
		echo "<th class='ep-house-table__tszh-name' colspan='" . count($arParams['SELECT_FIELDS']) . "'>{$arTszh['NAME']}</th>";
		echo "</tr>\n";
		foreach ($arResult["HOUSES"][$tszh_id] as $arHouse)
		{
			echo "<tr>";
			foreach ($arParams["SELECT_FIELDS"] as $fieldName)
			{
				echo "<td>{$arHouse[$fieldName]}</td>";
			}
			echo "</tr>\n";
		}
	}
	?>
</table>

<?
$APPLICATION->IncludeComponent(
	"bitrix:main.pagenavigation", 
	".default", 
	array(
		"NAV_OBJECT" => $arResult["NAV"],
		"SEF_MODE" => "N",
		"SHOW_COUNT" => "N",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);
?>
