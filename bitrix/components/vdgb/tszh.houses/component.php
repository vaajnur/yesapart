<?

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!CModule::IncludeModule('citrus.tszh'))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));

	return;
}

if (!CTszhFunctionalityController::CheckEdition())
{
	return;
}


$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);

if (strlen($arParams["SORT_BY"]) <= 0)
{
	$arParams["SORT_BY"] = "ID";
}
if ($arParams["SORT_ORDER"] != "DESC")
{
	$arParams["SORT_ORDER"] = "ASC";
}

if (!is_array($arParams['SELECT_FIELDS']))
{
	$arParams["SELECT_FIELDS"] = array("TYPE", "ADDRESS_USER_ENTERED", "YEAR_OF_BUILT", "FLOORS", "PORCHES", "AREA");
}

$arParams["TSZH_COUNT"] = intval($arParams["TSZH_COUNT"]);
if ($arParams["TSZH_COUNT"] <= 0)
{
	$arParams["TSZH_COUNT"] = 20;
}


$arSelect = array_merge($arParams["SELECT_FIELDS"], array(
	"ID",
	"AREA",
	"TSZH_ID",
	"ADDRESS_USER_ENTERED",
));

$arTszhFilter = array(
	'SITE_ID' => SITE_ID,
);
$arHouseFilter = array();

/*���� ������ ���������� �� ������ � ���������� ����������*/
if (intval($arParams["TSZH_ID"]) > 0)
{
	$arTszhFilter['ID'] = $arParams["TSZH_ID"];
}

$arResult['TSZH'] = array();
$arResult['HOUSES'] = array();

$rsTszh = CTszh::GetList(array(), $arTszhFilter, false, false, array('ID', 'NAME'));
while ($arTszh = $rsTszh->GetNext())
{
	$arResult['TSZH'][$arTszh['ID']]['NAME'] = htmlspecialchars_decode($arTszh['NAME']);
	$arHouseFilter['@TSZH_ID'][] = $arTszh['ID'];
}

if (count($arResult['TSZH']) == 0)
{
	ShowError(Loc::getMessage('TSZH_NOT_FOUND_ERROR'));

	return;
};

$arOrder = array(
	$arParams["SORT_BY"] => $arParams["SORT_ORDER"],
);
if (!array_key_exists("ID", $arOrder))
{
	$arOrder["ID"] = "DESC";
}

$arResult["NAV"] = new \Bitrix\Main\UI\PageNavigation("page");
$arResult["NAV"]->allowAllRecords(true)
                ->setPageSize($arParams["TSZH_COUNT"])
                ->initFromUri();
$rsHouses = \Citrus\Tszh\HouseTable::GetList(
	array(
		'order' => $arOrder,
		'filter' => $arHouseFilter,
		'select' => $arSelect,
		"count_total" => true,
		"offset" => $arResult["NAV"]->getOffset(),
		"limit" => $arResult["NAV"]->getLimit(),
	)
);

$arResult["NAV"]->setRecordCount($rsHouses->getCount());

if ($rsHouses->getSelectedRowsCount())
{
	/*���� � ������� ���������� ��������� ����*/
	while ($arHouse = $rsHouses->fetch())
	{
		$arHouse["HREF"] = '#';
		$arSite = $arResult["SITES"][$arHouse["SITE_ID"]];
		if (is_array($arSite))
		{
			$arHouse["HREF"] = ((is_array($arSite['DOMAINS']) && strlen($arSite['DOMAINS'][0]) > 0 || strlen($arSite['DOMAINS']) > 0) ? 'http://' : '') . (is_array($arSite["DOMAINS"]) ? $arSite["DOMAINS"][0] : $arSite["DOMAINS"]) . $arSite["DIR"];
		}
		$arResult["HOUSES"][$arHouse['TSZH_ID']][] = $arHouse;
	}
}
else
{
	/*���� � ������� ���������� ��� ����� - ������� ��������� �� ������*/
	ShowError(Loc::getMessage('TSZH_HOUSES_NOT_FOUND_ERROR'));

	return;
}

// ������� ������ �� �����������, � ������� ��� �����
foreach ($arResult["TSZH"] as $id => $tszh)
{
	if (!isset($arResult["HOUSES"][$id]))
	{
		unset($arResult["TSZH"][$id]);
	}
}

$this->IncludeComponentTemplate();

?>