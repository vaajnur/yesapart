<?php
/**
 * Модуль «Документы»
 * Компонент Список документов (rarus:documents.list)
 * Выводит cписок документов
 * @package documents
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Vdgb\Documents\DocumentTable;
use Bitrix\Main\FileTable;
use Vdgb\Documents\Helper;
use Bitrix\Main\Entity\ExpressionField;

class CDocumentsList extends CBitrixComponent {
	const COUNT_PAGE = 10;
	private $iPageOffset;
	private $iPage = 1;
	static $iComponentOnPage = 1; // порядковый номер компонента на странице, для пагинации
    private $arFilter = array(); // фильтр элементов 
	
	public function __construct($component = null) {
    	if (!Loader::includeModule('vdgb.documents'))
    		return;
    	parent::__construct($component);
        $request = Application::getInstance()->getContext()->getRequest();
        $this->iPage = intval($request->get('PAGEN_' . self::$iComponentOnPage));
        if ($this->iPage <= 0) {
            $this->iPage = 1;
        }
	}

	/**
	 * Обрабатывает параметры переданные в параметре IncludeComponent
	 * @param  Array $arParams параметы
	 * @return Array           обработанные параметры
	 */
	public function onPrepareComponentParams($arParams) {
        $result = array(
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
            "ENTITY_ID" => ((intval($arParams["ENTITY_ID"]) > 0) ? intval($arParams["ENTITY_ID"]) : 0),
            "SORT" => (($arParams["SORT"] === "") ? "SORT" : $arParams["SORT"]),
            "DESC" => (($arParams["DESC"] === "DESC") ? "DESC" : "ASC"),
            //"SHOW_DESCRIPTION" => (($arParams["SHOW_DESCRIPTION"] === "Y") ? "Y" : "N"),
            "SHOW_FIELDS" => array("DOCUMENT_DESCRIPTION", "DOCUMENT_ID"),//$arParams['SHOW_FIELDS'],
            "COUNT_PAGE" => ((intval($arParams["COUNT_PAGE"]) > 0) ? intval($arParams["COUNT_PAGE"]) : self::COUNT_PAGE)
			);
        if ($result['ENTITY_ID'] > 0) {
            $this->arFilter['ENTITY_ID'] = $result['ENTITY_ID'];
        }
		return $result;
	}

	/**
	 * Логика компонента
	 * @return Array результата в массиве $arResult
	 */
	public function executeComponent()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        if ($request->get('get_file') !== null) {
            $this->getFile(intval($request->get('get_file')));
        }
        if($this->startResultCache(
            $this->arParams['CACHE_TIME'], 
            $this->arParams['ENTITY_ID'] . $this->iPage)
        )
        {
            $this->pagintaion();
            $this->setData();
            $this->arResult['HEADER'] = DocumentTable::getHeadersList();
            foreach ($this->arResult['HEADER'] as $key => $value) {
                if (in_array($value['id'], $this->arParams['SHOW_FIELDS'])) {
                    $this->arResult['HEADER'][$value['id']] = $this->arResult['HEADER'][$key];
                }
                unset($this->arResult['HEADER'][$key]);
            }
            $this->includeComponentTemplate();
        }
        self::$iComponentOnPage++;
        return $this->arResult;
    }

    private function setData() {
    	$param = array(
    		'filter' => $this->arFilter,
    		'select' => array("*", "FILE_" => "DOCUMENT.*"),
    		'order'  => array($this->arParams['SORT'] => $this->arParams['DESC']),
    		'limit'  => $this->arParams['COUNT_PAGE'],
    		'offset' => $this->iPageOffset
            );
    	$rsDoc = DocumentTable::getList($param);
    	while ($arDoc = $rsDoc->fetch()) {
    		$this->arResult['DATA'][$arDoc['ID']] = $arDoc;
    	}
        unset($arDoc);
    }

    private function pagintaion() {
        $count = DocumentTable::getCount($this->arFilter);
        $this->iPageOffset = ($this->iPage - 1) * $this->arParams['COUNT_PAGE'];
        if ($this->iPageOffset > $count || $this->iPageOffset < 0) {
            $this->iPageOffset = 0;
        }
        $navResult = new CDBResult();
        $navResult->NavPageCount = ceil($count / $this->arParams['COUNT_PAGE']); // Общее количество страниц
        $navResult->NavPageNomer = $this->iPage; // Номер текущей страницы
        $navResult->NavNum = self::$iComponentOnPage; // Номер пагинатора. Используется для формирования ссылок на страницы
        $navResult->NavPageSize = $this->arParams['COUNT_PAGE']; // Количество записей выводимых на одной странице
        $navResult->NavRecordCount = $count; // Общее количество записей
        $this->arResult['PAGINATION']['CONTEXT'] = $navResult;
    }

    private function getFile($id) {
        $arFile = \Bitrix\Main\FileTable::getList(array(
            'filter' => array('ID' => $id, 'MODULE_ID' => Helper::MODULE_ID)
        ))->fetch();
        if (empty($arFile))
            return;
        $file = Application::getDocumentRoot() . "/upload/" . $arFile['SUBDIR'] . "/" . $arFile['FILE_NAME'];
        if (file_exists($file)) {
            if (ob_get_level()) {
              ob_end_clean();
            }
            // заставляем браузер показать окно сохранения файла
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . $arFile['ORIGINAL_NAME']);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            // читаем файл и отправляем его пользователю
            readfile($file);
            exit;
        }
    }
}