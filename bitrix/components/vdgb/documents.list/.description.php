<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("DOC_LIST_NAME"),
	"DESCRIPTION" => GetMessage("DOC_LIST_DESCRIPTION"),
	"CACHE_PATH" => "Y",
	"SORT" => 100,
	"PATH" => array(
		"ID" => GetMessage("DOC_LIST_PATH"),
        "NAME" => GetMessage("DOC_LIST_PATH"),
    ),
);

?>