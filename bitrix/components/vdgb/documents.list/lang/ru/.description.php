<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$MESS["DOC_LIST_NAME"] = "Список документов";
$MESS["DOC_LIST_DESCRIPTION"] = "Компонент выводит список документов привязанных к идентификатору";
$MESS["DOC_LIST_PATH"] = "Документы";