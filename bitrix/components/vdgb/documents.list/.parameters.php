<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!\Bitrix\Main\Loader::includeModule("vdgb.documents"))
    return;
$arDocField = Vdgb\Documents\DocumentTable::getEditableFields();
$arSortList = array();
$arFieldList = array();
foreach ($arDocField as $code => $obField) {
	$arFieldList[$code] = $obField->getTitle();
    if ($code == 'DOCUMENT_ID') {
		$code = "DOCUMENT.ORIGINAL_NAME";
	}
	$arSortList[$code] = $obField->getTitle();
}
unset($arDocField);

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
        "ENTITY_ID" => array(
            "PARENT" => "BASE",
			"NAME" => GetMessage("DOC_PARAM_ENTITY_ID"),
			"TYPE" => "STRING"
        ),
        "SORT" => array(
            "PARENT" => "BASE",
			"NAME" => GetMessage("DOC_PARAM_SORT"),
			"TYPE" => "LIST",
			"VALUES" => $arSortList,
			"ADDITIONAL_VALUES" => "Y"
        ),
        "DESC" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("DOC_PARAM_DESC"),
            "TYPE" => "LIST",
            "VALUES" => array(
            	"ASC" => GetMessage("DOC_PARAM_DESC_ASC"),
            	"DESC" => GetMessage("DOC_PARAM_DESC_DESC")
            	),
            "DEFAULT" => "ASC"
        ),
        /*"SHOW_FIELDS" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("DOC_PARAM_SHOW_FIELDS"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arFieldList,
            "DEFAULT" => array_keys($arFieldList)
        ),
        "SHOW_DESCRIPTION" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("DOC_PARAM_SHOW_DESCRIPTION"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
			"ADDITIONAL_VALUES" => "Y"
        ),*/
        "COUNT_PAGE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("DOC_PARAM_COUNT_PAGE"),
            "TYPE" => "STRING",
            "DEFAULT" => "10"
        ),
        // кеш
		"CACHE_TIME"  =>  array("DEFAULT"=>36000000),
		"CACHE_GROUPS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => GetMessage("DOC_PARAM_CACHE_GROUPS"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
	),
);
?>