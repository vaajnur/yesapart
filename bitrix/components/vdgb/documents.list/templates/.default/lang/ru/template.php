<?php
$MESS["DOC_LIST_TEMPL_NOT_FOUND_DOC"] = "Для данного дома нет привязанных документов";
$MESS["DOC_LIST_TEMPL_NOT_SELECT_DOC"] = "Не выбран идентификатор для привязанного документа";
$MESS["DOC_LIST_TEMPL_NOT_SELECT_FIELD"] = "Не выбраны поля для вывода";
$MESS["DOC_LIST_TEMPL_DOWNLOAD"] = "Скачать";
$MESS["LIST_OF_DOCUMENTS"] = "Список документов";
$MESS["DOC_LIST_TEMPL_DESCRIPTION"] = "Описание";