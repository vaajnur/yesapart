<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?if ($arResult["JQUERY"] == "Y") CJSCore::Init(array('jquery'));?>

<div class="star_bxslider_outer">
	<div id="star_bxslider" data-id="0" class="star_bxslider">
		<?foreach ($arResult["ITEMS"] as $item):?>
			<div class="star_bxslider_item">
				<?if (isset($item["LINK"]) && !empty($item["LINK"])):?><a href="<?=$item["LINK"]?>" <?=$arResult["NEW_WINDOW"]?>><?endif;?>
					<img src="<?=$item["PICTURE"]?>" title="<?=$item["NAME"]?>"/>
				<?if (isset($item["LINK"]) && !empty($item["LINK"])):?></a><?endif;?>
			</div>
		<?endforeach;?>
	</div>
</div>

<script>
	sbs_id = 0;
	while(document.querySelector(".star_bxslider[data-id='"+sbs_id+"']")) sbs_id++;

	document.querySelector(".star_bxslider[data-id='0']").dataset.id = sbs_id;
	document.querySelector(".star_bxslider[data-id='"+sbs_id+"']").id = "star_bxslider_"+sbs_id;

	$("#star_bxslider_"+sbs_id).bxSlider({
		wrapperClass: 'star_bxslider_wrapper',
		infiniteLoop: <?=$arResult["INFINITE_LOOP"]?>,
		mode: '<?=$arResult["MODE"]?>',
		speed: <?=$arResult["SPEED"]?>,
		captions: <?=$arResult["CAPTIONS"]?>,
		adaptiveHeight: <?=$arResult["ADAPTIVE_HEIGHT"]?>,
		pager: <?=$arResult["PAGER"]?>,
		controls: <?=$arResult["CONTROLS"]?>,
		nextText: '',
		prevText: '',
		touchEnabled: false,
		auto: <?=$arResult["AUTO"]?>,
		pause: <?=$arResult["PAUSE"]*1000?>
		<?if ($arResult["BANNER_TYPE"] == "carousel"):?>,
			slideMargin: <?=$arResult["MARGIN"]?>,
			slideWidth: <?=$arResult["SLIDE_WIDTH"]?>,
			minSlides: <?=$arResult["COUNT_SLIDES"]?>,
			maxSlides: <?=$arResult["COUNT_SLIDES"]?>,
			moveSlides: <?=$arResult["COUNT_SLIDES"]?>
		<?endif;?>
	});
</script>
