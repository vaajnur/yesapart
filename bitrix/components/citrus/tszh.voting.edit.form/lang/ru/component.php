<?$MESS ['CVE_ELEMENT_NOT_FOUND'] = "Голосование не найдено.";
$MESS ['CVE_VOTE_MODULE_NOT_INSTALLED'] = "Модуль «Голосования» не установлен.";$MESS ['CVE_VOTE_CHANNEL_NOT_FOUND'] = "Группа голосований не найдена.";
$MESS ['CVE_F_ID'] = "ID";$MESS ['CVE_F_TIMESTAMP_X'] = "Дата изменения";$MESS ['CVE_F_ACTIVE'] = "Активность";$MESS ['CVE_F_NAME'] = "Название";$MESS ['CVE_F_DATE_BEGIN'] = "Дата начала";$MESS ['CVE_F_DATE_END'] = "Дата окончания";$MESS ['CVE_F_SORT'] = "Индекс сортировки";$MESS ['CVE_F_TITLE_TEXT'] = "Тема голосования";$MESS ['CVE_F_DETAIL_TEXT'] = "Описание голосования";$MESS ['CVE_F_VOTING_METHOD'] = "Способ подсчета голосов";$MESS ['CVE_F_ENTITY_TYPE'] = "Кто голосует";
$MESS ['CVE_F_VOTING_COMPLETED'] = "Состоялось";$MESS ['CVE_F_PERCENT_NEEDED'] = "Процент, необходимый для того, чтобы головование состоялось";$MESS ['TVE_FIELD_QUESTIONS_TITLE'] = "Вопрос №#N#";$MESS ['CVE_F_QUESTION_TEXT'] = "Текст вопроса";
$MESS ['CVE_F_DEL_QUESTION'] = "Удалить вопрос";$MESS ['CVE_F_ADD_QUESTION'] = "Добавить вопрос";
$MESS ['CVE_F_ANSWERS_LABEL'] = "Варианты ответа";
$MESS ['TVE_FIELD_TYPE_0'] = "Один вариант";$MESS ['TVE_FIELD_TYPE_1'] = "Несколько вариантов";$MESS ['CVE_F_ANSWERS'] = "Ответы";
$MESS ['TVE_F_MESSAGE'] = "Текст";
$MESS ['TVE_F_C_SORT'] = "Сортировка";$MESS ['TVE_F_DEL'] = "Удал.";$MESS ['CVE_SAVED_MESSAGE'] = "Изменения сохранены";$MESS ['CVE_ERROR_SAVING'] = "При сохранении изменений произошла ошибка.";

$MESS ['CVE_TAB1_NAME'] = "Редактирование голосования";$MESS ['CVE_ADD_VOTING'] = "Новое голосование";$MESS ['CVE_VOTING_LIST'] = "Список";

?>