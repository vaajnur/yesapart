<?
$MESS["MODULE_NOT_INSTALL"]="Модуль аварийно-диспетчерской службы не установлен.";
//$MESS["SUP_DEFAULT_TITLE"]="Список заявок";
$MESS ['SUP_PAGES'] = "Заявки";
$MESS ['SUP_ERROR_MESSAGE'] = "Вы не ввели сообщение";
$MESS ['SUP_ERROR_TELEPHONE'] = "Некорректный номер телефона";
$MESS ['SUP_ERROR'] = "Ошибка создания сообщения";
$MESS ['CTT_CLIENT_TICKET_ADDED'] = "Ваша заявка передана диспетчеру. Заявке присвоен номер #NUMBER#";
$MESS ['CTT_TICKET_ADDED'] = "Заявка добавлена, присвоен номер #NUMBER#";
?>