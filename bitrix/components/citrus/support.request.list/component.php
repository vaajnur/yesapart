<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

require_once($_SERVER["DOCUMENT_ROOT"].$componentPath."/functions.php");

if (!CModule::IncludeModule("citrus.tszhtickets"))
{
	ShowError(GetMessage("MODULE_NOT_INSTALL"));
	return;
}

$arResult = Array();

if (!function_exists('_GetDropDownDictionary')):
	function _GetDropDownDictionary($TYPE, &$TICKET_DICTIONARY)
	{
		$arReturn = Array();

		if (array_key_exists($TYPE, $TICKET_DICTIONARY))
		{
			foreach ($TICKET_DICTIONARY[$TYPE] as $key => $value)
			{
				$arReturn[$key] = htmlspecialchars($value["NAME"]);
			}
		}

		return $arReturn;
	}
endif;

$getSMS = false;
$saved = false;
if ($_REQUEST["save"] == "Y" && $_REQUEST["ID"] == "0" && $_SERVER["REQUEST_METHOD"]=="POST" && check_bitrix_sessid() && count($arParams['EDIT_FIELDS']) > 0)
{
    if (strlen(trim($_REQUEST["MESSAGE"]))<=0)
        $strError .= GetMessage("SUP_ERROR_MESSAGE")."<br>";

    $getSMS = isset($_REQUEST["GET_SMS"]) && $_REQUEST["GET_SMS"] == 'on';
    if (!checkPhoneNumber($_REQUEST["TELEPHONE"], $_REQUEST["TELEPHONE"] != "" || $getSMS))
        $strError .= GetMessage("SUP_ERROR_TELEPHONE")."<br>";
    else $telephone = $DB->ForSQL($_REQUEST["TELEPHONE"]);

    if ($strError == "")
    {
        $arFields = array(
            'SITE_ID'					=> SITE_ID,
            'HIDDEN'					=> 'N',
            'PUBLIC_EDIT_URL'			=> $APPLICATION->GetCurPage(),
        );

        $arFields['CLOSE'] = "N";
        $arDicFieldsEdit = ['CRITICALITY_ID'=>'CRITICALITY', 'CATEGORY_ID'=>'CRITICALITY'
            , 'MESSAGE'=>'MESSAGE', 'RESPONSIBLE_USER_ID'=>'RESPONSIBLE'
            , 'TELEPHONE'=>'TELEPHONE', 'GET_SMS' => 'GET_SMS'];
        foreach ($arDicFieldsEdit as $key=>$value){
            if (in_array($value, $arParams['EDIT_FIELDS']))
                $arFields[$key] = $_REQUEST[$key];
        }
        $arFields['TITLE'] = isset($_REQUEST['TITLE']) ? $_REQUEST['TITLE'] : $_REQUEST['MESSAGE'];

        if (in_array('STATUS', $arParams['EDIT_FIELDS']) && $_REQUEST["CLOSE"]!="Y") {
            $arFields['STATUS_ID'] = IntVal($_REQUEST['STATUS_ID']);

            $is_filtered = false;
            $rsStatus = CTszhTicketDictionary::GetList(($by = 's_id'), ($order = 'asc'), Array("TYPE" => 'S', 'ID' => $arFields['STATUS_ID'], 'SITE' => SITE_ID), $is_filtered);
            if ($arStatus = $rsStatus->Fetch() && in_array($arStatus['SID'], Array('done', 'closed'))) {
                $arFields['CLOSE'] = 'Y';
            }
        }

        if (!(CTszhTicket::IsSupportTeam() || CTszhTicket::IsAdmin() || $arFields["CLOSE"] == 'Y')) {
            $arFields['STATUS_ID'] = CTszhTicketDictionary::GetDefault("S", SITE_ID);
            $arFields['CRITICALITY_ID'] = CTszhTicketDictionary::GetDefault("K", SITE_ID);
        }

        $GLOBALS["USER_FIELD_MANAGER"]->EditFormAddFields("TSZH_TICKET", $arFields);
        foreach ($arFields as $key=>$value) {
            if (substr($key, 0, 3) == 'UF_' && !in_array($key, $arParams['EDIT_FIELDS'])) {
                unset($arFields[$key]);
            }
        }

        global $USER;
        // заполним поля заявителя в новой заявке
        if (CModule::IncludeModule("citrus.tszh") && $USER->IsAuthorized()) {
            $arUserAccount = CTszhAccount::GetByUserID($USER->GetID());
            if (is_array($arUserAccount)) {
                $arFields['UF_FIO'] = CTszhAccount::GetFullName($USER->GetID());
                $arFields['UF_ACCOUNT'] = $arUserAccount['XML_ID'];
                $arFields['UF_ADDRESS_BUILDING'] = CTszhAccount::GetFullAddress($arUserAccount, false);
                $arFields['UF_ADDRESS_FLAT'] = $arUserAccount['FLAT'];
            }
        }
        $ID = 0;
        $bNew = true;
        $ID = CTszhTicket::SetTicket($arFields, $ID, "Y", $NOTIFY = "Y");
        if (intval($ID)>0)
        {
            $saved = true;
            //сохраняем номер телефона и признак получения смс о смене статуса обращения
            if(isset($telephone)){
                global $DB;
	            $DB->Query("update b_tszh_ticket set TELEPHONE = '".$DB->ForSQL($telephone)."', GET_SMS='".($getSMS?'Y':'N')."' where ID = ".$ID);
            }
            $rsTmpTicket = CTszhTicket::GetByID($ID);
            if ($arTmpTicket = $rsTmpTicket->Fetch()) {
                $arResult['MESSAGE'] = str_replace('#NUMBER#', $arTmpTicket['NUMBER'], GetMessage(CTszhTicket::IsSupportClient() ? "CTT_CLIENT_TICKET_ADDED" : "CTT_TICKET_ADDED"));
            }
        }
        else
        {
            $ex = $APPLICATION->GetException();
            if ($ex)
            {
                $strError .= $ex->GetString() . '<br>';
            }
            else
            {
                $strError .= GetMessage('SUP_ERROR') . '<br>';
            }
        }
    }
    if($strError != ''){
        showError($strError);
    }
}

$arResult["DICT"] = CTszhTicketDictionary::GetDropDownArray(SITE_ID);
foreach ($arResult['DICT']['S'] as $statusID => $arStatus) {
	$arDesc = explode('--', $arStatus['DESCR']);
	if (count($arDesc) > 1) {
		$arStatus['DESCR'] = $arDesc[0];
		$arStatus['DESCR_PUBLIC'] = $arDesc[1];
	} else {
		$arStatus['DESCR'] = $arStatus['DESCR_PUBLIC'] = $arDesc[0];
	}
	$arResult['DICT']['S'][$statusID] = $arStatus;
}
$arResult["DICTIONARY"]["STATUS"] = _GetDropDownDictionary("S", $arResult["DICT"]);
$arResult["DICTIONARY"]["CATEGORY"] = _GetDropDownDictionary("C", $arResult["DICT"]);


//Permissions
if ( !($USER->IsAuthorized() && (CTszhTicket::IsSupportClient() || CTszhTicket::IsAdmin() || CTszhTicket::IsSupportTeam())) )
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
if ($saved || strlen($GLOBALS["by"]) <= 0)
{
    $GLOBALS["by"] = "s_created";
    $GLOBALS["order"] = "desc";
}
if($saved)
    unset($GLOBALS["PAGEN_1"]);
//Sorting
InitSorting();

$arResult["NUMBER_ACTIVE"] = 0; //все заявки
if(isset($_REQUEST["find_close"])){
    switch ($_REQUEST["find_close"]){
        case "N":
            $arResult["NUMBER_ACTIVE"] = 1; //только активные
            break;
        case "Y":
            $arResult["NUMBER_ACTIVE"] = 2; //только архивные
            break;
    }
}


//TICKET_EDIT_TEMPLATE
$arParams["TICKET_EDIT_TEMPLATE"] = trim($arParams["TICKET_EDIT_TEMPLATE"]);
$arParams["TICKET_EDIT_TEMPLATE"] = (strlen($arParams["TICKET_EDIT_TEMPLATE"]) > 0 ? htmlspecialchars($arParams["TICKET_EDIT_TEMPLATE"]) : "ticket_edit.php?ID=#ID#");

//TICKETS_PER_PAGE
$arParams["TICKETS_PER_PAGE"] = (intval($arParams["TICKETS_PER_PAGE"]) <= 0 ? 50 : intval($arParams["TICKETS_PER_PAGE"]));

//Get Tickets
CPageOption::SetOptionString("main", "nav_page_in_session", "N");

$arFilter = Array(
    "NUMBER"	=> $_REQUEST["find_number"],
    "STATUS_ID" => $_REQUEST['find_status'],
    'CATEGORY_ID' => $_REQUEST['find_category'],
    "CLOSE"							=> $_REQUEST["find_close"],
    "TITLE"							=> $_REQUEST["find_title"],
    "TITLE_EXACT_MATCH"				=> $_REQUEST["find_title_exact_match"],
    "MESSAGE"						=> $_REQUEST["find_message"],
    "MESSAGE_EXACT_MATCH"			=> $_REQUEST["find_message_exact_match"],
);
if (!is_array($arFilter['STATUS_ID'])) {
    if (!CTszhTicket::IsAdmin()) {
        $arFilter['STATUS_ID'] = array_keys($arResult['DICTIONARY']['STATUS']);
    } else {
        $arFilter['STATUS_ID'] = Array();
        foreach ($arResult['DICTIONARY']['STATUS'] as $statusID => $statusName) {
            if ($statusName == 'Новая заявка') {
                $arFilter['STATUS_ID'][] = $statusID;
            }
        }
    }
}
if (!is_array($arFilter['CATEGORY_ID']) || array_search(0, $arFilter['CATEGORY_ID']) !== false) {
    unset($arFilter['CATEGORY_ID']);
}

$arResult["NUMBER_ACTIVE"] = 0; //все заявки
if(isset($arFilter["CLOSE"])){
    switch ($arFilter["CLOSE"]){
        case "N":
            $arResult["NUMBER_ACTIVE"] = 1; //только активные
            break;
        case "Y":
            $arResult["NUMBER_ACTIVE"] = 2; //только архивные
            break;
    }
}

$rsTickets = CTszhTicket::GetList($GLOBALS["by"], $GLOBALS["order"], $arFilter, $is_filtered, $check_rights = "Y", $get_user_name = "N", $get_dictionary_name = "N");
$rsTickets->NavStart($arParams["TICKETS_PER_PAGE"]);

//Result array
$arResult = array_merge($arResult, Array(
	"TICKETS" => Array(),
	"TICKETS_COUNT" => $rsTickets->SelectedRowsCount(),
	"NAV_STRING" => $rsTickets->GetPageNavString(GetMessage("SUP_PAGES")),
	"CURRENT_PAGE" => htmlspecialchars($APPLICATION->GetCurPage()),
	"NEW_TICKET_PAGE" => htmlspecialchars(CComponentEngine::MakePathFromTemplate($arParams["TICKET_EDIT_TEMPLATE"], Array("ID" => "0"))),
));


if (array_key_exists('citrus_support_message', $_SESSION)) {
	$arResult['MESSAGE'] = $_SESSION['citrus_support_message'];
	unset($_SESSION['citrus_support_message']);
}

//Get Dictionary Array
$arTicketDictionary = CTszhTicketDictionary::GetDropDownArray(SITE_ID);

//Dictionary table
$arDictType = Array(
		"C" => "CATEGORY",
		"K" => "CRITICALITY",
		"S" => "STATUS",
		"M" => "MARK",
		"SR" => "SOURCE",
);
$firstIndexStatusIndex = array_keys($arTicketDictionary["S"])[0];
while ($arTicket = $rsTickets->GetNext())
{
	$arDict = Array();
	foreach ($arDictType as $TYPE => $CODE)
		$arDict += _GetDictionaryInfo($arTicket[$CODE."_ID"], $TYPE, $CODE, $arTicketDictionary);


	$url = CComponentEngine::MakePathFromTemplate($arParams["TICKET_EDIT_TEMPLATE"], Array("ID" => $arTicket["ID"]));
if($arTicket["STATUS_ID"] == null)
    $arTicket["STATUS_ID"] = $firstIndexStatusIndex;
	$arResult["TICKETS"][] = ($arTicket + $arDict + Array("TICKET_EDIT_URL" => $url));
}

$cookieName = 'messageError'.$APPLICATION->GetCurPage(false);
$arResult["MESSAGE_ERROR"] = $APPLICATION->get_cookie($cookieName);
if($arResult["MESSAGE_ERROR"] != ""){
    $APPLICATION->set_cookie($cookieName, "", time() - 60);
}


unset($rsTickets);
unset($arTicketDictionary);

$this->IncludeComponentTemplate();
?>