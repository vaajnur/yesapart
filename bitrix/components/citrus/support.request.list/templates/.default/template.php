<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (strlen($arResult['MESSAGE']) > 0) {
	echo ShowNote($arResult['MESSAGE']);
}

if (strlen($arResult['MESSAGE_ERROR']) > 0) {
    echo ShowError($arResult['MESSAGE_ERROR']);
}

?>

<div class="requests">
    <div class="requests__window-open"><a href="javascript:void(0)" title="<?=GetMessage("SUP_ASK_TITLE")?>" class="link-theme-default window-open" window="requests__window"><?=GetMessage("SUP_ASK")?></a></div>
    <div class="shadow hidden"></div>
    <div class="window requests__window" id="requests__window">
        <div class="window__close">X</div>
        <form name="support_edit" method="POST" enctype="multipart/form-data">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="set_default" value="Y" />
            <input type="hidden" name="ID" value="0" />
            <input type="hidden" name="lang" value="<?=LANG?>" />
            <input type="hidden" value="Y" name="save">
            <div class="window__title"><?=GetMessage("FR_TITLE")?></div>
            <div class="requests__input">
                <p><?=GetMessage("FR_REASON")?>:</p>
                <textarea name="MESSAGE"></textarea>
            </div>
            <div class="requests__input">
                <p><?=GetMessage("FR_TELEPHONE")?>:</p>
                <input name="TELEPHONE" />
            </div>
            <input class="link-theme-default" type="submit" value="<?=GetMessage("SUP_ASK")?>">
        </form>
    </div>
    <div class="radio-switcher">
        <form action="<?=$arResult["CURRENT_PAGE"]?>" method="get">
            <span><?=GetMessage("TT_SHOW_TICKETS")?>:</span>
            <?
            $arOpenClose= Array(
                array(GetMessage("TT_ALL"), $APPLICATION->GetCurPageParam('del_filter=Y', Array('find_close', 'set_filter', 'del_filter'))),
                array(GetMessage("TT_ONLY_ACTIVE"),$APPLICATION->GetCurPageParam('find_close=N&set_filter=Y', Array('find_close', 'set_filter', 'del_filter'))),
                array(GetMessage("TT_ARCHIVE"),$APPLICATION->GetCurPageParam('find_close=Y&set_filter=Y', Array('find_close', 'set_filter', 'del_filter')))
            );
            foreach ($arOpenClose as $value => $option):
                $tagAStart = '';
                $tagAEnd = '';
                $checked = '';
                if($arResult["NUMBER_ACTIVE"] != $value){
                    $tagAStart = '<a href="'.$option[1].'">';
                    $tagAEnd = '</a>';
                }
                else
                    $checked = 'checked="true"';
                echo $tagAStart.'<input type="radio" id="filter_'.$value.'" name="filter" '.$checked.' /><label for="filter_'.$value.'">'.$option[0].'</label>'.$tagAEnd;
            endforeach;
            ?>
        </form>
    </div>
    <table class="table1">
        <thead>
        <tr>
            <td class="hidden-mobi"><?=GetMessage("TT_F_NUMBER")?></td>
            <td class="hidden-mobi"><?=GetMessage("TT_F_DATE").' '.SortingEx("s_created")?></td>
            <td class="requests__descrip"><?=GetMessage("TT_DESCRIPTION")?></td>
            <td><a href="javascript:void(0)" class="requests__status-show"><?=GetMessage("SUP_STATUS").' '.SortingEx("s_status")?></a>
                <div class="requests__status">
                    <div class="requests__status-title">
                        <?=GetMessage("STATUS_ORDER_EXECUTION")?>:
                    </div>
                    <?php
                    foreach ($arResult['DICT']['S'] as $status_id=>$arStatus):
                        ?>
                        <div class="requests__status-item">
                            <div class="requests__lamp-<?=str_replace("_","-",$arStatus["SID"])?>"></div>
                            <div>
                                <p><?= $arStatus['NAME'] ?></p>
                                <span><?= $arStatus['DESCR_PUBLIC'] ?></span>
                            </div>
                        </div>
                        <?
                    endforeach;
                    ?>
                </div>
            </td>
        </tr>
        </thead>
        <tbody>
        <?php
        global $DB;
        foreach ($arResult["TICKETS"] as $arTicket):
            $day_create = $DB->FormatDate($arTicket['DAY_CREATE'], 'YYYY-MM-DD HH:MI:SS', 'DD.MM.YYYY');
            ?>
            <tr>
                <td class="hidden-mobi"><?= $arTicket['NUMBER'] ?></td>
                <td class="hidden-mobi"><?= $day_create ?></td>
                <td class="requests__descrip">
                    <div class="visible-mobi">
                        <div><span><?=GetMessage("TT_F_NUMBER")?>: </span><?= $arTicket['NUMBER'] ?></div>
                        <div><span><?=GetMessage("TT_F_DATE")?>: </span><?= $day_create ?></div>
                    </div>
                    <a href="<?=$arTicket["TICKET_EDIT_URL"]?>" title="<?=GetMessage("SUP_EDIT_TICKET")?>"><?= $arTicket["TITLE"] ?></a>
                </td>
                <?if ($arTicket['STATUS_ID'] > 0 && array_key_exists($arTicket['STATUS_ID'], $arResult['DICT']['S'])):
                    $arStatus = $arResult['DICT']['S'][$arTicket['STATUS_ID']];?>
                    <td class="requests__lamp-<?=str_replace("_","-",$arStatus["SID"])?>" title="<?=$arStatus['DESCR_PUBLIC']?>">
                        <?=$arStatus["NAME"]?></td>
                <?else:?>
                    <td></td>
                <?endif;?>
            </tr>
        <?endforeach?>
        </tbody>
    </table>
</div>

<?if (strlen($arResult["NAV_STRING"]) > 0):?>
	<br /><?=$arResult["NAV_STRING"]?><br />
<?endif?>
