<?
$MESS['TSZH_CON_BTN_NEXT'] = "Далее";
$MESS['TSZH_CON_BTN_PREV'] = "Назад";
$MESS['TSZH_CON_STEP'] = "Шаг";
$MESS['TSZH_CON_CHOOSE_WAY'] = "Выберите способ привязки";
$MESS['TSZH_CON_REGCODE'] = "По лицевому счету";
$MESS['TSZH_CON_REGCODE_DESC'] = "Для привязки лицевого счета необходимо будет ввести кодовое слово выданное управляющей компанией.";
$MESS['TSZH_CON_PASS'] = "По пользователю";
$MESS['TSZH_CON_PASS_DESC'] = "Для привязки лицевого счета необходимо будет ввести логин и пароль от лицевого счета.";

$MESS['TSZH_CON_CHOOSE_WAY_ERROR'] = "Выберите один из вариантов";
$MESS['TSZH_CON_DATA_INPUT'] = "Ввод данных";
$MESS['TSZH_CON_DATA_VERIFY'] = "Проверка данных";
$MESS['TSZH_CON_END'] = "Завершение привязки";
$MESS['TSZH_CON_SUCCESS'] = "Лицевой счет успешно подтвержден";
$MESS['TSZH_CON_CAB_RETURN'] = "Вернуться в личный кабинет";
?>