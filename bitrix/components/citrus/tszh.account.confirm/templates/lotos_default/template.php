<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

IncludeTemplateLangFile(__FILE__);

if (array_key_exists("ERRORS", $arResult)) {
	echo ShowError($arResult["ERRORS"]);
}
if (array_key_exists("NOTE", $arResult)) {
	echo ShowNote($arResult["NOTE"]);
}

if (is_array($arResult["ACCOUNT"])):

	?><p><?=str_replace("#ACCOUNT#", $arResult["ACCOUNT"]["XML_ID"], GetMessage("TAC_YOUR_ACCOUNT"))?></p>
	<p><?=GetMessage("TAC_GO_TO")?><a href="<?=$arParams["PERSONAL_PATH"]?>"><?=GetMessage("TAC_PERSONAL_SECTION")?></a>.</p>
<?

else:
	?>
	<form method="post" enctype="multipart/form-data" action="">
		<input type="hidden" name="action" value="confirm" />
		<?
		if (count($arResult['TSZH_LIST']) == 1)
		{
			$arTszh = array_pop($arResult['TSZH_LIST']);
			?><input type="hidden" name="tszh" value="<?=$arTszh['ID']?>" /><?
		}
		?>
		<?=bitrix_sessid_post()?>
		<table class="data-table no-border"><?

			if (count($arResult['TSZH_LIST']) > 1)
			{
				?>
				<tr>
					<td style="text-align: right; vertical-align: middle; width: 30%"><label for="confirm-tszh"><?=GetMessage("TAC_SELECT_TSZH")?><span class="starrequired">*</span></label></td>
					<td><select name="tszh" id="confirm-tszh" class="input">
							<option value="0"><?=GetMessage("TAC_NOT_SELECTED")?></option>
							<?
							foreach ($arResult["TSZH_LIST"] as $arTszh) {
								echo '<option value="' . $arTszh["ID"] . '"' . ($arTszh["ID"] == $arResult["TSZH"] ? ' selected="selected"' : '') . '>' . $arTszh["NAME"] . '</option>';
							}
							?>
						</select></td>
				</tr>
				<?
			}
			?>
			<tr>
				<td style="text-align: right; vertical-align: middle;"><label for="confirm-regcode"><?=GetMessage("TAC_REG_WORD")?><span class="starrequired">*</span></label></td>
				<td><input type="text" name="regcode" class="input" id="confirm-regcode" value="<?=$arResult['REG_CODE']?>" /></td>
			</tr>
			<?if($arResult["CAPTCHA_CODE"]):?><tr>
				<td style="vertical-align: top; text-align: right;">
					<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
					<label for="confirm-captcha"><?=GetMessage("TAC_CAPTCH_PROMPT")?><span class="starrequired">*</span></label><br />
				</td>
				<td>
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" style="border: 0;" /><br />
					<input type="text" name="captcha_word" class="input" id="confirm-captcha" style="width: 160px" />
				</td>
				</tr><?endif;?>

			<tr>
				<td>&nbsp;</td>
				<td><span class="starrequired">*</span><?=GetMessage("TAC_REQUIRED_FIELDS")?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><?
					if (function_exists("TemplateShowButton")) {
						TemplateShowButton(array(
							"type" => 'submit',
							'title' => GetMessage("TAC_BUTTON_CAPTION"),
						));
					} else {
						?><button type="submit"><?=GetMessage("TAC_BUTTON_CAPTION")?></button><?
					}
					?>
				</td>
			</tr>

		</table>
	</form>
<?
endif;
?>