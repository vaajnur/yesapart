<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule("vdgb.tszhvote"))
	return;

$arGroup = array();
$arVotes = Array();
$arTypeDiogram = array(
    "0" => GetMessage("TypeCircul"),
    "1" => GetMessage("TypeGistogram"),
);

$rs = CCitrusPollGroup::GetList(array(),array("ACTIVE" => "Y"),array("ID","NAME","TITLE","LID"));

if(is_object($rs)) {
    while($arrGroup = $rs->Fetch()) {
        $arGroup[$arrGroup["ID"]] = "[".$arrGroup["ID"]."] ".$arrGroup["LID"]."-".$arrGroup["NAME"];
    }
}

if($arCurrentValues["GROUP_ID"]!="-"){
    $arFilter = array(
        "GROUP_ID" => $arCurrentValues["GROUP_ID"],
        "ACTIVE" => "Y",
    );
}

$rsVoting = CCitrusPoll::GetList(array(),$arFilter);

if(is_object($rsVoting)) {
    while($arrVoting = $rsVoting->Fetch()) {
        $arVotes[$arrVoting["ID"]] = "[".$arrVoting["ID"]."]".$arrVoting["NAME"];

    }
}

$arComponentParameters = array(

        "GROUPS" => array(
            "SETTINGS" => array(
                "NAME" => GetMessage("SETTINGS_SECTION")
            ),
            "PARAMS" => array(
                "NAME" => GetMessage("PARAMS_PHR")
            ),
            "CACHE_SETTINGS" => array(
                "NAME" => GetMessage("CACHE_TITLE")
            ),
        ),

	"PARAMETERS" => array(		

            
		"GROUP_ID" => array(
			"NAME" => GetMessage("GROUP_ID"),
			"TYPE" => "LIST",
			"PARENT" => "SETTINGS",
			"VALUES" => $arGroup,
			"DEFAULT"=>'={$_REQUEST["GROUP_ID"]}',
			"MULTIPLE"=>"N",
			"REFRESH" => "Y",
                        "ADDITIONAL_VALUES"=>"Y",
		),

		"VOTING_ID" => array(
			"NAME" => GetMessage("VOTING_ID"),
			"TYPE" => "LIST",
			"PARENT" => "SETTINGS",
			"VALUES" => $arVotes,
			"MULTIPLE"=>"Y",
			"ADDITIONAL_VALUES"=>"Y",
		),

		"VOTE_TYPE_DIOGRAM" => array(
			"NAME" => GetMessage("VOTE_TYPE_DIOGRAM"),
			"TYPE" => "LIST",
			"PARENT" => "SETTINGS",
			"VALUES" => $arTypeDiogram,
			"MULTIPLE"=>"N",
			"REFRESH" => "N",
                        "ADDITIONAL_VALUES"=>"Y",
		),

		"CACHE_TIME"  =>  Array("DEFAULT"=>36000000),

	),
);


?>
