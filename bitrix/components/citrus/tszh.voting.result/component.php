<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!IsModuleInstalled("vdgb.tszhvote")):
	ShowError(GetMessage("VOTE_MODULE_IS_NOT_INSTALLED"));
	return;
elseif (intVal($arParams["GROUP_ID"]) <= 0):
	ShowError(GetMessage("VOTE_GROUP_EMPTY"));
	return;
elseif (is_array($arParams["VOTING_ID"])):

        foreach($arParams["VOTING_ID"] as $key => $arrey) {
            if(strlen($arrey) <= 0 || IntVal($arrey) <= 0) {
                unset($arParams["VOTING_ID"][$key]);
            }
        }
        if(empty($arParams["VOTING_ID"])) {
            ShowError(GetMessage("VOTE_EMPTY"));
            return;
        }
endif;

global $arrSaveColor;


require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/img.php");

///************** CACHE **********************************************/
//	if ($arParams["CACHE_TYPE"] == "Y" || ($arParams["CACHE_TYPE"] == "A" && COption::GetOptionString("main", "component_cache_on", "Y") == "Y"))
//		$arParams["CACHE_TIME"] = intval($arParams["CACHE_TIME"]);
//	else
//		$arParams["CACHE_TIME"] = 0;
//
//	$arParams["ADDITIONAL_CACHE_ID"] = (isset($arParams["ADDITIONAL_CACHE_ID"]) && strlen($arParams["ADDITIONAL_CACHE_ID"]) > 0 ?
//		$arParams["ADDITIONAL_CACHE_ID"] : $USER->GetGroups() );
///********************************************************************
//				/Input params
//********************************************************************/
//$arParamsCache = array(
//	$arParams["ADDITIONAL_CACHE_ID"], $arParams["CACHE_TYPE"]);
//
//if ($GLOBALS["VOTING_OK"] =="Y" || $GLOBALS["USER_ALREADY_VOTE"] =="Y" || (isset($_REQUEST['VOTE_SUCCESSFULL']) && ($_REQUEST['VOTE_SUCCESSFULL'] == 'Y')))
//    $this->ClearResultCache($arParamsCache);
//elseif (!$this->StartResultCache(false, $arParamsCache))
//    return;
/********************************************************************
				Default values
********************************************************************/
$arResult["OK_MESSAGE"] = "";
$arResult["ERROR_MESSAGE"] = "";
$arResult["CHANNEL"] = array();
$arResult["VOTE"] = array();
$arResult["QUESTIONS"] = array();
$arResult["GROUP_ANSWERS"] = array();
$arResult["~CURRENT_PAGE"] = $APPLICATION->GetCurPageParam("", array("VOTE_ID","VOTING_OK","VOTE_SUCCESSFULL"));
$arResult["CURRENT_PAGE"] = htmlspecialcharsbx($arResult["~CURRENT_PAGE"]);

/********************************************************************
				/Default values
********************************************************************/

/********************************************************************
				Data
********************************************************************/
if (!CModule::IncludeModule("vdgb.tszhvote"))
	return false;


$arChannel = array();
$arVote = array();
$arQuestions = array();
$arAnswers = array();
$arDropDown = array();
$arMultiSelect = array();
$arGroupAnswers = array();
$arResult = array();
/*
if (empty($arParams["VOTING_ID"])):
	$this->AbortResultCache();
	ShowError(GetMessage("VOTE_NOT_FOUND"));
	return;
endif;
*/
$arParams["GROUP_ID"] = intVal($arParams["GROUP_ID"]);

global $USER;
$curentUser = $USER->GetID();


$voting_size = 1;

//�������� ������ ��� ������� ������
foreach ($arParams["VOTING_ID"] as $key => $vote_id)
{
    $vote_id = IntVal($vote_id);
    
    //���� id ������ > 0 ����� ���������� ������� ���������
    if($vote_id > 0) {

        $arVote = CCitrusPoll::GetByID($vote_id);
        if($arVote)
        {
            $arResult['VOTE'][$arVote["ID"]] = $arVote;
        }

/*        if(strlen($method) > 0) {
            $allClassMethod = CCitrusPollMethodBase::GetList();
            if(is_array($allClassMethod)) {
                    if(key_exists($method, $allClassMethod)){
                        $methodClass = new $allClassMethod[$method]["CLASS"];
                    }
            }
        }*/

        if(isset($arResult['VOTE'][$vote_id]) && !empty($arResult['VOTE'][$vote_id])) {

            //���������� ������ ��� ������� ������ ��������
            $arQuestionFilter = array(
                "VOTING_ID" => $vote_id,
                "ACTIVE"    => "Y"
            );

            //������� ������ �������� ��� ������� ������
            $rsQuestions = CCitrusPollQuestion::GetList(array("SORT" => "ASC", "ID" => "ASC"),$arQuestionFilter);
            if(is_object($rsQuestions)) {
                while($arQuestion = $rsQuestions->Fetch()){
                    $arResult['VOTE'][$vote_id]["QUESTION"][$arQuestion["ID"]] = $arQuestion;
                
                    if(!empty($arQuestion)) {

                        //���������� ������ ��� ������� ������� ��� ������� �������
                        $arAnswerFilter = array(
                            "QUESTION_ID" => $arQuestion["ID"],
                            "ACTIVE" => "Y",
                        );
                        
                        $fullCount = 0;
                        $color = array();
                        $rsAnswer = CCitrusPollAnswer::GetList(array("SORT" => "ASC"),$arAnswerFilter);
                        if(is_object($rsAnswer)) {
                            while($arAnswers = $rsAnswer->Fetch()) {

                                $arResult['VOTE'][$vote_id]["QUESTION"][$arQuestion["ID"]]['ANSWER'][$arAnswers["ID"]] = $arAnswers;
                                if(empty($arAnswers)) {
                                    //���� ������ � �������� ����, �� ������� ��������� �� ������
                                    $arResult['ERRORS']['VOTE'][$vote_id] = GetMessage("ANSWER_NOT_FOUND");
                                }
                                else {
                                    $color[$arAnswers["ID"]] = $arResult['VOTE'][$vote_id]["QUESTION"][$arQuestion["ID"]]['ANSWER'][$arAnswers["ID"]]['COLOR'];
                                }
                            }

                            $strColor = "";
                            $totalRecords = $rsAnswer->SelectedRowsCount();
                            
                            foreach($color as $key => $colValue) {
                                if(strlen($colValue) > 0) {

                                }
                                else {
                                    $strColor = GetNextRGB($strColor, $totalRecords);
                                    $value = "#" . $strColor;
                                    
                                    while(array_search($value,$color)) {
                                        $strColor = GetNextRGB($strColor, $totalRecords);
                                        $value = "#" . $strColor;
                                    }
                                    
                                    $arResult['VOTE'][$vote_id]["QUESTION"][$arQuestion["ID"]]['ANSWER'][$key]['COLOR'] = $value;
                                }
                          
                            }                            
                        }
                        
                       $arResult['VOTE'][$vote_id]["QUESTION"][$arQuestion["ID"]]['FULL_COUNT'] = $fullCount;
                       
                    }
                    else {
                        //���� ������ � ��������� ����, �� ������� ��������� �� ������
                        $arResult['ERRORS']['VOTE'][$vote_id] = GetMessage("QUESTION_NOT_FOUND");
                    }
                }
            }

        }
        else {
            $arResult['ERRORS']['VOTE'][$vote_id] = GetMessage("VOTE_NOT_FOUND");
        }
    }
}

$this->IncludeComponentTemplate();
?>
