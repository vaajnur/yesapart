<?
$MESS ['VOTE_MODULE_IS_NOT_INSTALLED'] = "Модуль опросов не установлен.";
$MESS ['VOTE_ACCESS_DENIED'] = "У вас нет прав на участие в данном опросе.";
$MESS ['VOTE_NOT_FOUND'] = "Опрос не найден.";
$MESS ['VOTE_GROUP_EMPTY'] = "Не указана группа опроса";
$MESS ['VOTE_EMPTY'] = "Опрос не указан.";
$MESS ['VOTE_OK'] = "Спасибо за участие в опросе.";
$MESS ['VOTE_ALREADY_VOTE'] = "Вы не можете дважды принимать участие в этом опросе.";
$MESS ['VOTE_RED_LAMP'] = "Опрос не активен.";
$MESS ['USER_VOTE_EMPTY'] = "Вы не выбрали вариант ответа.";
$MESS ['QUESTION_NOT_FOUND'] = "Вопросы не найдены";
$MESS ['ANSWER_NOT_FOUND'] = "Ответы на вопросы не найдены";
$MESS ['VOTE_MODULE_NOT_ACCESS'] = "У вас нет прав на просмотр результатов данной группы опросов";
?>
