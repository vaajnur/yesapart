<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (empty($arResult["PAYMENTS"]))
{
	ShowNote(GetMessage("CITRUS_TSZHPAYMENT_NO_PAYMENTS"));
	return;
}

if ($arParams["DISPLAY_TOP_PAGER"])
	echo "<div>{$arResult['NAV_STRING']}</div>\n";

echo '<table class="data-table tszh-payments-table"><thead><tr>';
foreach ($arResult["FIELDS"] as $arField)
	echo"<th>{$arField['TITLE']}</th>\n";
echo "</tr></thead>\n";

foreach ($arResult["PAYMENTS"] as $arPayment)
{
	echo "<tr>";
	foreach ($arResult["FIELDS"] as $arField)
	{
		$value = $arPayment[$arField["CODE"]];
		if (in_array($arField["CODE"], Array("SUMM")))
		{
			$value = CTszhPublicHelper::FormatCurrency($value);
			$class = ' class="cost""';
		}
		elseif (is_set($arField, "TYPE") && in_array($arField['TYPE'], Array("int", "float")))
		{
			//$class = ' style="text-align: right;"';
		}
		else
			$class = '';
		echo "<td$class>$value</td>\n";
	}
	echo "</tr>";
}

echo '</table>';

if ($arParams["DISPLAY_BOTTOM_PAGER"])
	echo "<div>{$arResult['NAV_STRING']}</div>\n";

?>