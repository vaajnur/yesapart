<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION, $DB;
if ($_GET["print"] == "Y")
	$APPLICATION->RestartBuffer();
//echo "<pre>arResult: "; print_r($arResult); echo "</pre>";
if (!function_exists("__citrusReceiptNum")):
	function __citrusReceiptNum($number, $decimals=2, $canBeEmpty=false, $isComponent=false)
	{
		if ($number == 0)
		{
			if ($isComponent)
				return '<div style="text-align: center;">X</div>';

			if ($canBeEmpty)
				return "&nbsp;";
			else
				return "0";
		}
		return number_format($number, $decimals, ',', ' ');
	}
endif;

if (!function_exists("__citrusGetPlaceholderCellsHtml")):
	function __citrusGetPlaceholderCellsHtml($cellCount, $arContent)
	{
		$arCells = array();
		for ($i=0; $i<$cellCount; $i++)
			$arCells[$i] = "&nbsp;";

		foreach ($arContent as $arCellContent)
		{
			$cellNumber = $arCellContent["align"] == "left" ? 0 : $cellCount-1;
			if ($arCellContent["split"])
			{
				for ($i=0; $i<strlen($arCellContent["content"]); $i++)
				{
					if ($arCellContent["align"] == "left")
					{
						$cellNumber = $i;
						$cellContent = substr($arCellContent["content"], $i, 1);
					}
					else
					{
						$cellNumber = $cellCount-$i-1;
						$cellContent = substr($arCellContent["content"], -($i+1), 1);
					}
					$arCells[$cellNumber] = $cellContent;
					if ($arCellContent["bold"])
						$arCells[$cellNumber] = "<span class=\"bold\">{$arCells[$cellNumber]}</span>";
				}
			}
			else
			{
				$arCells[$cellNumber] = $arCellContent["content"];
				if ($arCellContent["bold"])
					$arCells[$cellNumber] = "<span class=\"bold\">{$arCells[$cellNumber]}</span>";
			}
		}?>

		<table class="placeholder-table">
			<tr>
				<?foreach ($arCells as $cell):?>
					<td><?=$cell?></td>
				<?endforeach?>
			</tr>
		</table>
	<?}
endif;

if (!function_exists("__citrusGetMeterValues")):
	function __citrusGetMeterValues($arMeter, $lastExistingValuesFlag = false)
	{
		$arResult = array();
		for ($i=1; $i<=$arMeter["VALUES_COUNT"]; $i++)
		{
			$value = false;

			if ($lastExistingValuesFlag)
			{
				if (is_array($arMeter["VALUE"]) && !empty($arMeter["VALUE"]))
					$value = $arMeter["VALUE"]["VALUE" . $i];
				elseif (is_array($arMeter["VALUE_BEFORE"]) && !empty($arMeter["VALUE_BEFORE"]))
					$value = $arMeter["VALUE_BEFORE"]["VALUE" . $i];
			}
			else
				$value = $arMeter["VALUE"]["VALUE" . $i];

			if ($value !== false)
				$arResult[] = __citrusReceiptNum($value, $arMeter["DEC_PLACES"], true);
		}

		return $arResult;
	}
endif;
if (!function_exists("__vdgbGetMeterValues")):
	function __vdgbGetMeterValues($arMeter, $lastExistingValuesFlag = false)
	{
		//echo "<pre>arMeter: "; print_r($arMeter); echo "</pre>";
		$arResult = array();
		for ($i=1; $i<=$arMeter["VALUES_COUNT"]; $i++)
		{
			$value = false;
			$value_before = false;

			if ($lastExistingValuesFlag)
			{
				if (is_array($arMeter["VALUE"]) && !empty($arMeter["VALUE"]))
					$value = $arMeter["VALUE"]["VALUE" . $i];
				elseif (is_array($arMeter["VALUE_BEFORE"]) && !empty($arMeter["VALUE_BEFORE"]))
					$value = $arMeter["VALUE_BEFORE"]["VALUE" . $i];
			}
			else
			{
				if (isset($arMeter["VALUE"]))
				{
					$value = $arMeter["VALUE"]["VALUE" . $i];
					$value_before = $arMeter["VALUE"]["VALUE" . $i] - $arMeter["VALUE"]["AMOUNT" . $i];
					$amount = $arMeter["VALUE"]["AMOUNT" . $i];
				}
				elseif (isset($arMeter["VALUE_BEFORE"]))
				{
					$value = $arMeter["VALUE_BEFORE"]["VALUE" . $i];
					$value_before = $arMeter["VALUE_BEFORE"]["VALUE" . $i] - $arMeter["VALUE_BEFORE"]["AMOUNT" . $i];
					$amount = $arMeter["VALUE_BEFORE"]["AMOUNT" . $i];
				}

			}

			if ($value !== false)
				$arResult[] = array(
					"CURRENT" => __citrusReceiptNum($value, $arMeter["DEC_PLACES"], true),
					"PREVIOUS" => __citrusReceiptNum($value_before, $arMeter["DEC_PLACES"], true),
					"AMOUNT" => __citrusReceiptNum($amount, $arMeter["DEC_PLACES"], true)
				) ;
		}
		//echo "<pre>arResult: "; print_r($arResult); echo "</pre>";
		return $arResult;
	}
endif;
if (!function_exists("__vdgbFillCountersValues")):
	/**
	 * ��������������� ������� ��� ������ ������� �� ���������� �����������
	 * @param $MeterType ��� ��������� METERS ��� HMETERS
	 * @param $pValue ������� ��������-������ ��������� ������ ������� __vdgbGetMeterValues
	 * @param $pMeterValues �������������� ������ ��������� ��������� ��� ������ � ������� ���������� ����������
	 * @param $ppu_number ����� �������� � ������� ���������� ����������
	 * @param $pIndexMeters ������������� ������ �������� ���������, ��� ������ ������� - ID �������� � ��, �������� ������� - ����� �������� � �������
	 * @param $pmeterID ID �������� � ��
	 * @param $pCharge ������ ����������
	 * @param $pResult ������� ������ arResult
	 */
	function __vdgbFillCountersValues($MeterType,$pValue,&$pMeterValues,&$ppu_number,&$pIndexMeters,$pmeterID,$pCharge,$pResult)
	{
		if (!empty($pValue))
		{
			foreach ($pValue as $value_tarif)
			{
				if (!isset($pIndexMeters[$pmeterID]))
				{
					$ppu_number++;
					$pIndexMeters[$pmeterID] = $ppu_number;
				}
				$pMeterValues[] = array(
					'pu_number' => $pIndexMeters[$pmeterID],
					'type' => ($MeterType == 'METERS') ? GetMessage("RCPT_IND") : GetMessage("RCPT_ODN"),
					'cur_value' => $value_tarif['CURRENT'],
					'bef_value' => $value_tarif['PREVIOUS'],
					'number' => $pResult[$MeterType][$pmeterID]['NUM'] ? $pResult[$MeterType][$pmeterID]['NUM'] : $pResult[$MeterType][$pmeterID]['NAME'],
					'amount' => $value_tarif['AMOUNT'],
					'inorm' => ($MeterType == 'METERS') ? $pCharge["SERVICE_NORM"] : "X",
					'hnorm' => "X",
					'volumep' => ($MeterType == 'METERS') ? $pCharge["VOLUMEP"] : "X",
					'volumeh' => ($MeterType == 'METERS') ? "X" : $pCharge["VOLUMEH"],
				);
			}
		}
	}

endif;

if ($_GET["print"] != "Y")
{
	$APPLICATION->AddHeadString('<link href="' . $this->GetFolder() . '/styles.css" rel="stylesheet" type="text/css" />');
?>
<p><?=GetMessage("RCPT_YOU_CAN")?> <a href="<?=$APPLICATION->GetCurPageParam('print=Y', Array('print'))?>" target="_blank"><?=GetMessage("RCPT_OPEN_RECEIPT_IN_WINDOW")?></a> <?=GetMessage("RCPT_FOR_PRINTING")?></p>
<?

}
else
{
	if ($arResult["IS_FIRST"]):
		if ($arResult["MODE"] == "AGENT"):?>
			<style type="text/css">
				<?=$APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"] . $this->GetFolder() . "/styles.css")?>
			</style>
		<?else:
			?><!DOCTYPE html>
			<html>
			<head>
				<meta http-equiv="content-type" content="text/html;" charset="<?=SITE_CHARSET?>" />
				<link href="<?=$this->GetFolder()?>/styles.css" rel="stylesheet" type="text/css" />
		<?endif?>
		<style type="text/css" media="print">
			@page {
				size: 21cm 29.7cm;
				margin: 0.5cm 0.5cm;
			}
			<?if ($arParams["INLINE"] != "Y"):?>
				#receipt div.receipt {
					page-break-after: always;
					/*page-break-inside: avoid;*/
				}
				#receipt div.receipt #sheet1 {
					page-break-after: always;
				}
			<?endif?>
		</style>
		<?if ($arResult["MODE"] == "ADMIN"):?>
			<style type="text/css" media="screen">
				#receipt div.receipt {
					border-bottom: 1pt dashed #000;
				}
			</style>
		<?endif;

		if ($arResult["MODE"] !== "AGENT"):?>
				<title><?=GetMessage("RCPT_TITLE")?></title>
			</head>
			<body id="receipt" onload="window.print();">
		<?else:?>
			<div id="receipt">
		<?endif;
	endif;
}
//echo '<pre>' . /*htmlspecialchars(*/print_r($arResult, true)/*)*/ . '</pre>';
?>
<div class="receipt">
	<?
	// ����� ��� �������� ��������� �� ���������� ��� ��������� � ����
	if (!$arResult["IS_FINES_RECEIPT"] && !empty($arResult["METERS"]))
	{
		?>
		<div class="top-section">
			<?
			if (!$arResult["HAS_SEPARATE_PENALTIES_RECEIPT"])
			{
				?><div class="main-header center"><?=GetMessage("RCPT_MAIN_HEADER")?></div><?
			}
			?>
			<div class="address center bold"><?=CTszhAccount::GetFullAddress($arResult["ACCOUNT_PERIOD"])?></div>
			<div class="header center"><?=GetMessage("RCPT_HEADER")?></div>

			<?
			if ($arResult["ACCOUNT_PERIOD"]["IPD"])
			{
				?>
				<div class="header center"><?=GetMessage("TPL_DOCUMENT_IPD")?>: <?=$arResult["ACCOUNT_PERIOD"]["IPD"]?></div>
				<?
			}
			?>
			<?if ($arResult["IS_OVERHAUL"])
				echo ($arResult["ACCOUNT_PERIOD"]["ELS_OVERHAUL"] ? '<div class="header center">' . GetMessage("TPL_DOCUMENT_ELS"). ': ' .$arResult["ACCOUNT_PERIOD"]["ELS_OVERHAUL"]. '</div>' :
					($arResult["ACCOUNT_PERIOD"]["ELS_MAIN"] ? '<div class="header center">' . GetMessage("TPL_DOCUMENT_ELS"). ': ' .$arResult["ACCOUNT_PERIOD"]["ELS_MAIN"]. '</div>' : "") );
			else
				echo ($arResult["ACCOUNT_PERIOD"]["ELS_MAIN"] ? '<div class="header center">' . GetMessage("TPL_DOCUMENT_ELS"). ': ' .$arResult["ACCOUNT_PERIOD"]["ELS_MAIN"]. '</div>' : "");
			?>
			<div style="margin-top: 5pt; overflow: hidden;">
				<div style="float: left;">
					<?$arContent = array(
						array(
							"content" => $arResult["ACCOUNT_PERIOD"]["XML_ID"],
							"align" => "left",
							"split" => true,
						),
					);
					__citrusGetPlaceholderCellsHtml(max(15, strlen($arResult["ACCOUNT_PERIOD"]["XML_ID"])), $arContent);?>
				</div>

				<?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES"])):?>
					<table border="0" style="float: right;">
						<tr>
							<?foreach ($arResult["ACCOUNT_PERIOD"]["BARCODES"] as $idx=>$barcode)
							{
								/** @noinspection PhpAssignmentInConditionInspection */
								if ($link = CTszhBarCode::getImageLink($barcode))
								{
									$code128 = $barcode["TYPE"] == 'code128';
									?><td class="no-border rcpt-barcode-cell"><img style="<?=($code128 ? 'width: 5.5cm' : 'width: 3cm; height: 3cm; max-height: 4cm;')?>" class="rcpt-barcode" src="<?=$link?>" alt="<?=htmlspecialcharsbx($barcode["VALUE"])?>"></td><?
								}
							}?>
						</tr>
                        <tr>
                            <?foreach ($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"] as $idx=>$barcode)
                            {
                                /** @noinspection PhpAssignmentInConditionInspection */
                                if ($link = CTszhBarCode::getImageLink($barcode))
                                {
                                    $code128 = $barcode["TYPE"] == 'code128';
                                    ?><td class="no-border rcpt-barcode-cell"><img style="<?=($code128 ? 'width: 5.5cm' : 'width: 3cm; height: 3cm; max-height: 4cm;')?>" class="rcpt-barcode" src="<?=$link?>" alt="<?=htmlspecialcharsbx($barcode["VALUE"])?>"></td><?
                                }
                            }?>
                        </tr>
					</table>
				<?endif?>
			</div>

			<div class="header-note">
				<?=GetMessage("RCPT_HEADER_NOTE", array("#START_DATE#" => $arResult['TSZH']["METER_VALUES_START_DATE"], "#END_DATE#" => $arResult['TSZH']["METER_VALUES_END_DATE"]))?>
			</div>

			<table class="border" style="margin-top: 5pt;">
				<tr>
					<td class="center"><?=GetMessage("RCPT_METER_NUMBER")?></td>
					<td class="center"><?=GetMessage("RCPT_METER_SERVICE_NAME")?></td>
					<td class="center"><?=GetMessage("RCPT_METER_LAST_VALUES")?></td>
					<td class="center"><?=GetMessage("RCPT_METER_DATE")?></td>
					<td class="center"><?=GetMessage("RCPT_METER_CUR_VALUES")?></td>
					<td class="center"><?=GetMessage("RCPT_METER_CHECK_DATE")?></td>
				</tr>
				<?foreach ($arResult["METERS"] as $arMeter):?>
					<tr>
						<td class="nowrap"><?=$arMeter["NUM"] ? $arMeter["NUM"] : $arMeter["NAME"]?></td>
						<td class="nowrap" style="max-width: 200px; overflow: hidden;"><?=strlen($arMeter["SERVICE_NAME"]) > 0 ? $arMeter["SERVICE_NAME"] : $arMeter["NAME"]?></td>
						<td class="num nowrap"><?=implode(" / ", __citrusGetMeterValues($arMeter, true))?></td>
						<? $date_update = explode(" ",$arResult["METERS_LAST_UPDATE"]);?>
						<td class="center nowrap"><?=$date_update[0]?></td>
						<td class="num nowrap">&nbsp;</td>
						<td class="center nowrap"><?=$arMeter["VERIFICATION_DATE"]?></td>
					</tr>
				<?endforeach?>
			</table>

			<div class="abonent-sign">
				<?=GetMessage("RCPT_CHECK_DATE")?> <span class="line"><?=str_repeat("&nbsp;", 40)?></span><?=GetMessage("RCPT_DATA_ACCEPT")?> <span class="line"><?=str_repeat("&nbsp;", 40)?></span> <?=$arResult["ACCOUNT_PERIOD"]["ACCOUNT_NAME"]?>
			</div>
		</div>

		<div class="cut-line"><?=GetMessage("RCPT_CUT_LINE")?></div>
		<?
	}
	?>
<table class="no-border">
	<tr>
		<td colspan="2" class="small">
			<table class="no-border" style="width: 100%;">
                <tr><td colspan=3 class="center"><b><?=$arResult["DOCUMENT_SUBTITLE"]?></b></td></tr>
                <tr><td colspan=3 class="center"><b><?=$arResult["ACCOUNT_PERIOD"]["DISPLAY_NAME"]?></b></td></tr>
				<tr>
					<td class="tl"><?=GetMessage("RCPT_RECIPIENT")?>:</td>
					<td class="bold" colspan="2" style="vertical-align: middle;"><?=$arResult["ORG_TITLE"]?></td>
				</tr>
				<tr>
					<td colspan="3">
						<table class="no-border" style="width: 100%;">
							<tr>
								<td class="nowrap"><?=GetMessage("RCPT_INN")?>:</td>
								<td class="nowrap"><span class="bb"><?=$arResult["ORG_INN"]?></span></td>
								<td class="nowrap"><?=GetMessage("RCPT_KPP")?>:&nbsp;<span class="bb"><?=$arResult["ORG_KPP"]?></span></td>
							</tr>
							<tr>
								<td class="nowrap"><?=GetMessage("RCPT_RSCH")?>:</td>
								<td colspan="2" class="nowrap">
									<span class="bb"><?=$arResult["ORG_RSCH"]?>&nbsp;<?=GetMessage("RCPT_IN")?>&nbsp;<?=$arResult["ORG_BANK"]?></span>
								</td>
							</tr>
							<tr>
								<td class="nowrap"><?=GetMessage("RCPT_KSCH")?>:</td>
								<td class="nowrap"><span class="bb"><?=$arResult["ORG_KSCH"]?></span></td>
								<td class="nowrap"><?=GetMessage("RCPT_BIK")?>: <span class="bb"><?=$arResult['ORG_BIK']?></span></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="tl"><?=GetMessage("RCPT_ORG_ADDRESS")?>:</td>
					<td><?=$arResult["TSZH"]["ADDRESS"]?></td>
					<td class="nowrap" style="vertical-align: bottom">
						<b><?=GetMessage("RCPT_FIO")?>:</b>
					</td>
				</tr>
				<tr>
					<td class="tl bold"><?=GetMessage("RCPT_ACCOUNT_NUMBER")?>:</td>
					<td>
						<?$arContent = array(
							array(
								"content" => $arResult["ACCOUNT_PERIOD"]["XML_ID"],
								"align" => "left",
								"split" => true,
							),
						);
						__citrusGetPlaceholderCellsHtml(max(15, strlen($arResult["ACCOUNT_PERIOD"]["XML_ID"])), $arContent);?>
					</td>
					<td class="nowrap">
						<?=$arResult["ACCOUNT_PERIOD"]["ACCOUNT_NAME"]?>
					</td>
				</tr>
				<tr>
					<td class="tl"><b><?=GetMessage("RCPT_ADDRESS")?>:</b></td>
					<td colspan="2"><?=CTszhAccount::GetFullAddress($arResult["ACCOUNT_PERIOD"])?></td>
				</tr>
				<tr>
					<td colspan="3">
						<table width="100%" class="no-padding">
							<tr>
								<td>
									<?if ($arResult["IS_OVERHAUL"] != "Y"):?>
                                        <table width="100%" class="no-padding">
                                            <tr>
                                                <td width="30px" style="padding:0px;">
                                                </td>
                                                <?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])):?>
                                                <td class="center" style="padding:0px;">
                                                    <?=GetMessage("RCPT_SUMM_TOTAL_TO_PAY_WITHOUT_INSURANCE")?>
                                                </td>
                                                <?else:?>
                                                <td class="center" style="padding:0px;">
                                                    <?=GetMessage("RCPT_SUMM_TOTAL_TO_PAY")?>
                                                </td>
                                                <?endif;?>
                                                <?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])):?>
                                                    <td width="30px" style="padding:0px;">
                                                    </td>
                                                    <td class="center" style="padding:0px;">
                                                        <?=GetMessage("RCPT_SUMM_TOTAL_TO_PAY_WITH_INSURANCE")?>
                                                    </td>
                                                <?endif;?>
                                            <tr>
                                                <td width="30px" height="30px">
                                                    <div class="b2" style="width: 30px; height: 30px;"></div>
                                                </td>
                                                <td class="bb" style="vertical-align: bottom; padding-bottom: 5px;">
                                                    <div style="font-size: 22px; text-align: center;"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"])?></div>
                                                </td>
                                                <?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])):?>
                                                    <td width="30px" height="30px">
                                                        <div class="b2" style="width: 30px; height: 30px;"></div>
                                                    </td>
                                                    <td class="bb" style="vertical-align: bottom; padding-bottom: 5px;">
                                                        <div style="font-size: 22px; text-align: center;"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"] + $arResult["INSURANCE"]["SUMM"])?></div>
                                                    </td>
                                                <?endif;?>
                                            </tr>
                                            <tr>
                                                <td width="30px" height="30px">
                                                </td>
                                                <td align="center">
                                                    <div style="padding-top: 5px;">
                                                        <?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES"])):?>
                                                        <table border="0">
                                                            <tr>
                                                                <?foreach ($arResult["ACCOUNT_PERIOD"]["BARCODES"] as $idx=>$barcode)
                                                                {
                                                                /** @noinspection PhpAssignmentInConditionInspection */
                                                                if ($link = CTszhBarCode::getImageLink($barcode))
                                                                {
                                                                $code128 = $barcode["TYPE"] == 'code128';
                                                                ?><td class="no-border rcpt-barcode-cell"><img style="<?=($code128 ? 'width: 5.5cm' : 'width: 3cm; height: 3cm; max-height: 4cm;')?>" class="rcpt-barcode" src="<?=$link?>" alt="<?=htmlspecialcharsbx($barcode["VALUE"])?>"></td><?
                                                                }
                                                                }?>
                                                            </tr>
                                                        </table>
                                                        <?endif?>
                                                    </div>
                                                </td>
                                                <td width="30px" height="30px">
                                                </td>
                                                <td align="center">
                                                    <div style="padding-top: 5px;">
                                                        <?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])):?>
                                                        <table border="0">
                                                            <tr>
                                                                <?foreach ($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"] as $idx=>$barcode)
                                                                {
                                                                /** @noinspection PhpAssignmentInConditionInspection */
                                                                if ($link = CTszhBarCode::getImageLink($barcode))
                                                                {
                                                                $code128 = $barcode["TYPE"] == 'code128';
                                                                ?><td class="no-border rcpt-barcode-cell"><img style="<?=($code128 ? 'width: 5.5cm' : 'width: 3cm; height: 3cm; max-height: 4cm;')?>" class="rcpt-barcode" src="<?=$link?>" alt="<?=htmlspecialcharsbx($barcode["VALUE"])?>"></td><?
                                                                }
                                                                }?>
                                                            </tr>
                                                        </table>
                                                        <?endif?>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    <?else:?>
                                        <table width="50%" class="no-padding">
                                            <tr>
                                                <td width="30px" style="padding:0px;">
                                                </td>
                                                <td class="center" style="padding:0px;">
                                                    <?=GetMessage("RCPT_SUMM_TOTAL_TO_PAY")?>
                                                </td>
    										</tr>
                                            <tr>
                                                <td width="30px" height="30px">
                                                    <div class="b2" style="width: 30px; height: 30px;"></div>
                                                </td>
                                                <td class="bb" style="vertical-align: bottom; padding-bottom: 5px;">
                                                    <div style="font-size: 22px; text-align: center;"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"])?></div>
                                                </td>
                                            </tr>
                                        </table>
                                    <?endif;?>
								</td>
								<td>
									<table width="100%" style="text-align: center">
										<tr>
											<td width="30px" height="30px">
												<div></div>
											</td>
										</tr>
										<tr>
											<td class="bt" width="30px" height="30px">
												<div><?=GetMessage("RCPT_DATE")?></div>
											</td>
										</tr>
										<tr>
											<td width="30px" height="30px">
												<div></div>
											</td>
										</tr>
										<tr>
											<td class="bt">
												<div><?=GetMessage("RCPT_SIGN")?></div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr id="sheet1">
		<td class="tl" style="padding-top: 5pt; border-top: 1pt dashed #000; width: 150px; text-align: center">
			<?=GetMessage("RCPT_CHECK")?>
		</td>
		<td class="bl small" style="padding-top: 5pt; border-top: 1pt dashed #000;">
			<table class="no-border" style="width: 100%;">
				<tr><td colspan=3 class="center"><b><?=GetMessage("RCPT_JKH_SERVICES",array("#MONTH_YEAR#" => mb_strtoupper($arResult["ACCOUNT_PERIOD"]["DISPLAY_NAME"], SITE_CHARSET)))?></b></td></tr>
				<tr>
					<td colspan="3" style="padding: 0;">
						<table class="no-padding" width="100%">
							<tr>
								<td class="bold" width="45%">���� ����������� �����������: </td><td class="bb"><?=$arResult["ORG_TITLE"]?></td>
							</tr>
							<tr>
								<td class="bold"><?=GetMessage("RCPT_ORG_ADDRESS")?>:</td><td class="bb"><?=$arResult["TSZH"]["ADDRESS"]?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="tl"><?=GetMessage("RCPT_RECIPIENT")?>:</td>
					<td class="bold" colspan="2" style="vertical-align: middle;"><?=$arResult["ORG_TITLE"]?></td>
				</tr>
				<tr>
					<td colspan="3">
						<table class="no-border" style="width: 100%;">
							<tr>
								<td class="nowrap"><?=GetMessage("RCPT_INN")?>:</td>
								<td class="nowrap"><span class="bb"><?=$arResult["ORG_INN"]?></span></td>
								<td class="nowrap"><?=GetMessage("RCPT_KPP")?>:&nbsp;<span class="bb"><?=$arResult["ORG_KPP"]?></span></td>
							</tr>
							<tr>
								<td class="nowrap"><?=GetMessage("RCPT_RSCH")?>:</td>
								<td colspan="2" class="nowrap">
									<span class="bb"><?=$arResult["ORG_RSCH"]?>&nbsp;<?=GetMessage("RCPT_IN")?>&nbsp;<?=$arResult["ORG_BANK"]?></span>
								</td>
							</tr>
							<tr>
								<td class="nowrap"><?=GetMessage("RCPT_KSCH")?>:</td>
								<td class="nowrap"><span class="bb"><?=$arResult["ORG_KSCH"]?></span></td>
								<td class="nowrap"><?=GetMessage("RCPT_BIK")?>: <span class="bb"><?=$arResult['ORG_BIK']?></span></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="tl"><?=GetMessage("RCPT_ORG_ADDRESS")?>:</td>
					<td><?=$arResult["TSZH"]["ADDRESS"]?></td>
					<td class="nowrap bold" style="vertical-align: bottom">

					</td>
				</tr>
				<tr>
					<td class="tl bold"><?=GetMessage("RCPT_ACCOUNT_NUMBER")?>:</td>
					<td>
						<?$arContent = array(
						array(
						"content" => $arResult["ACCOUNT_PERIOD"]["XML_ID"],
						"align" => "left",
						"split" => true,
						),
						);
						__citrusGetPlaceholderCellsHtml(max(15, strlen($arResult["ACCOUNT_PERIOD"]["XML_ID"])), $arContent);?>
					</td>
					<td class="nowrap">
						<table>
							<tr>
								<td><?=GetMessage("RCPT_LAST_PAYMENT_DATE")?>:</td>
								<td><?=$arResult["ACCOUNT_PERIOD"]["LAST_PAYMENT"]?></td>
							</tr>
							<tr>
								<td><?=GetMessage("RCPT_PAY_BEFORE")?>:</td>
								<?$arDate = ParseDateTime($arResult["ACCOUNT_PERIOD"]["PERIOD_DATE"], "YYYY-MM-DD");
								$ts = strtotime("1 month", mktime(0, 0, 0, $arDate["MM"], $arParams["PAY_BEFORE_DAY"], $arDate["YYYY"]));?>
								<td><?=ConvertTimeStamp($ts)?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="tl bold"><?=GetMessage("RCPT_FIO")?>:</td>
					<td class="bb" colspan="2">
						<?=$arResult["ACCOUNT_PERIOD"]["ACCOUNT_NAME"]?>
					</td>
				</tr>
				<tr>
					<td class="tl"><b><?=GetMessage("RCPT_ADDRESS")?>:</b></td>
					<td class="bb" colspan="2"><?=CTszhAccount::GetFullAddress($arResult["ACCOUNT_PERIOD"])?></td>
				</tr>
				<tr>
					<td colspan="3">
						<b><?=GetMessage("RCPT_ACCOUNT_INFO")?>:</b><br/>
						<?=GetMessage("RCPT_ACCOUNT_INFO_ADD", array(
						        "#FLAT_TYPE#" => $arResult["ACCOUNT_PERIOD"]["FLAT_TYPE"],
                                "#AREA#" => CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["AREA"]),
                                "#REGISTERED_PEOPLE#" => $arResult["ACCOUNT_PERIOD"]["REGISTERED_PEOPLE"],
                                "#PEOPLE#" => $arResult["ACCOUNT_PERIOD"]["PEOPLE"],
                                "#HOUSE_AREA#" => CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["HOUSE_AREA"], false, -1),
                                "#HOUSE_ROOMS_AREA#" => CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["HOUSE_ROOMS_AREA"], false, -1),
                                "#HOUSE_COMMON_PLACES_AREA#" => CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["HOUSE_COMMON_PLACES_AREA"], false, -1),
                            )
                        )
						?>
							</br>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<table width="100%" class="no-padding">
							<tr>
								<td width="60%">
                                    <?if ($arResult["IS_OVERHAUL"] != "Y"):?>
									<table width="100%" class="no-padding">
										<tr>
											<td width="30px" style="padding:0px;">
											</td>
											<td class="center" style="padding:0px;">
                                                <?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])):?>
                                                <?=GetMessage("RCPT_SUMM_TOTAL_TO_PAY_WITHOUT_INSURANCE")?>
                                                <?else:?>
                                                <?=GetMessage("RCPT_SUMM_TOTAL_TO_PAY")?>
                                                <?endif;?>
											</td>
                                            <?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])):?>
                                            <td width="30px" style="padding:0px;">
                                            </td>
                                            <td class="center" style="padding:0px;">
                                                <?=GetMessage("RCPT_SUMM_TOTAL_TO_PAY_WITH_INSURANCE")?>
                                            </td>
                                            <?endif;?>
										</tr>
										<tr>
											<td width="30px" height="30px">
												<div class="b2" style="width: 30px; height: 30px;"></div>
											</td>
											<td class="bb" style="vertical-align: bottom; padding-bottom: 5px;">
												<div style="font-size: 22px; text-align: center;"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"])?></div>
											</td>
                                            <?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])):?>
                                            <td width="30px" height="30px">
                                                <div class="b2" style="width: 30px; height: 30px;"></div>
                                            </td>
                                            <td class="bb" style="vertical-align: bottom; padding-bottom: 5px;">
                                                <div style="font-size: 22px; text-align: center;"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"] + $arResult["INSURANCE"]["SUMM"])?></div>
                                            </td>
                                            <?endif;?>
										</tr>
										<tr>
											<td width="30px" height="30px">
											</td>
											<td align="center">
											</td>
										</tr>
									</table>
                                    <?else:?>
                                    <table width="60%" class="no-padding">
                                        <tr>
                                            <td width="30px" style="padding:0px;">
                                            </td>
                                            <td class="center" style="padding:0px;">
                                                <?=GetMessage("RCPT_SUMM_TOTAL_TO_PAY")?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30px" height="30px">
                                                <div class="b2" style="width: 30px; height: 30px;"></div>
                                            </td>
                                            <td class="bb" style="vertical-align: bottom; padding-bottom: 5px;">
                                                <div style="font-size: 22px; text-align: center;"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"])?></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30px" height="30px">
                                            </td>
                                            <td align="center">
                                            </td>
                                        </tr>
                                    </table>
                                    <?endif;?>
								</td>
								<td width="20%">
									<table width="100%" style="text-align: center">
										<tr>
											<td width="30px" height="30px">
												<div></div>
											</td>
										</tr>
										<tr>
											<td class="bt" width="30px" height="30px">
												<div><?=GetMessage("RCPT_DATE")?></div>
											</td>
										</tr>
										<tr>
											<td width="30px" height="30px">
												<div></div>
											</td>
										</tr>
										<tr>
											<td class="bt">
												<div><?=GetMessage("RCPT_SIGN")?></div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<div style="page-break-before: always"></div>
<table class="border services" style="width: 100%;">
	<thead>
		<tr>
			<th colspan="12"><?=GetMessage("RCPT_SERVICES_TABLE_TITLE2", array("#MONTH_YEAR#" => mb_strtoupper($arResult["ACCOUNT_PERIOD"]["DISPLAY_NAME"], SITE_CHARSET)))?></th>
		</tr>
		<tr>
			<th rowspan="3"><?=GetMessage("RCPT_SERVICE_KINDS")?></th>
			<th rowspan="3"><?=GetMessage("RCPT_IND_ODN")?></th>
			<th rowspan="3"><?=GetMessage("RCPT_SERVICES_VOLUME")?></th>
			<th rowspan="3"><?=GetMessage("RCPT_UNITS")?></th>
            <th rowspan="3"><?=GetMessage("RCPT_TARIFF")?></th>
            <th rowspan="3"><?=GetMessage("RCPT_CHARGED2")?></th>
            <th rowspan="3"><?=GetMessage("RCPT_RAISE_MULTIPLIER")?></th>
            <th rowspan="3"><?=GetMessage("RCPT_RAISE_SUM")?></th>
            <th rowspan="3"><?=GetMessage("RCPT_CORRECTION")?></th>
			<th rowspan="3"><?=GetMessage("RCPT_DEBT_BEG_OR_OVERPAYED")?></th>
			<?$arPeriod = explode(" ", $arResult["ACCOUNT_PERIOD"]["DISPLAY_NAME"]);?>
			<th rowspan="3"><?=GetMessage("RCPT_PAYED_CUR_MONTH",array("#MONTH#" => $arPeriod[0]))?></th>
			<th colspan="1"><?=GetMessage("RCPT_TO_PAY2")?></th>
		</tr>
		<tr>
		</tr>
		<tr>
		</tr>
		<tr>
			<?for ($i=1; $i<=12; $i++):?>
			<th><?=$i?></th>
			<?endfor?>
		</tr>
	</thead>
	<tbody>
		<?foreach ($arResult["CHARGES"] as $idx => $arCharge):
		$isComponent = $arCharge["COMPONENT"] != "N";

		if ($arResult["HAS_GROUPS"] && ($idx == 0 || $arResult["CHARGES"][$idx-1]["GROUP"] != $arCharge["GROUP"])):?>
		<tr class="rcpt-group-title">
			<td colspan="12">
				<?=$arCharge["GROUP"]?>
			</td>
		</tr>
		<?endif?>
        <?if ($arCharge["IS_INSURANCE"] != "Y"):?>
		<tr>
			<td><?=$arCharge["SERVICE_NAME"]?><?=($arCharge["HAS_COMPONENTS"]  && substr($arCharge['SERVICE_NAME'], -1, 1) !== ':' ? ':' : '')?></td>
			<td class="center"><?=GetMessage("RCPT_IND")?></td>
			<?php
			// ������� �������� �� ������������� �� 1�
			if (isset($arCharge["AMOUNT_VIEW"]) && strlen($arCharge["AMOUNT_VIEW"])) : ?>
			<td class="num"><?=$arCharge["AMOUNT_VIEW"]?></td>
			<?php else : ?>
			<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array("AMOUNT"), $arCharge, true, $arCharge['AMOUNT'] - $arCharge['HAMOUNT'], 3)?></td>
			<?php endif; ?>
			<td class="center"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array("UNITS"), $arCharge, false, $arCharge["SERVICE_UNITS"])?></td>
			<td class="num"><?=$arCharge["RATE"]?></td>
			<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array("SUMM"/*, "HSUMM"*/), $arCharge, true, (($arCharge["SUM_WITHOUT_RAISE"] != 0) ? $arCharge["CSUM_WITHOUT_RAISE"]: $arCharge["SUMM"]) - $arCharge["HSUMM"])?></td>
            <td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array("RAISE_MULTIPLIER"/*, "HSUMM"*/), $arCharge, true, $arCharge["RAISE_MULTIPLIER"])?></td>
            <td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array("RAISE_SUM"/*, "HSUMM"*/), $arCharge, true, $arCharge["RAISE_SUM"])?></td>
    		<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("CORRECTION", $arCharge, true)?></td>
			<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("DEBT_BEG", $arCharge, true)?></td>
			<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("SUMM_PAYED", $arCharge, true)?></td>
			<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("DEBT_END", $arCharge, true)?></td>
		</tr>
            <?if ((CCitrusTszhReceiptComponentHelper::getArrayValue("HAMOUNT", $arCharge, true, false, 3) != "X") && (CCitrusTszhReceiptComponentHelper::getArrayValue("HAMOUNT", $arCharge, true, false, 3) != "0,000")):?>
            <tr>
                <td><?=$arCharge["SERVICE_NAME"]?><?=($arCharge["HAS_COMPONENTS"]  && substr($arCharge['SERVICE_NAME'], -1, 1) !== ':' ? ':' : '')?> <?=GetMessage("RCPT_ODN")?></td>
                <td class="center"><?=GetMessage("RCPT_ODN")?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("HAMOUNT", $arCharge, true, false, 3)?></td>
                <td class="center"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array("UNITS"), $arCharge, false, $arCharge["SERVICE_UNITS"])?></td>
                <td class="num"><?=$arCharge["RATE"]?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("HSUMM", $arCharge, true)?></td>
                <td class="num">X</td>
                <td class="num">X</td>
                <td class="num">X</td>
                <td class="num">X</td>
                <td class="num">X</td>
                <td class="num">X</td>
            </tr>
            <?endif;?>
        <?endif;?>
				<?endforeach?>
				<?foreach ($arResult["DEBT_CHARGES"] as $idx => $arCharge):?>
					<?if ((intval($arCharge["SUMM_PAYED"]) != 0) || (intval($arCharge["DEBT_BEG"]) !== 0)) : ?>
		<tr>
			<td><?=$arCharge["SERVICE_NAME"]?><?=($arCharge["HAS_COMPONENTS"]  && substr($arCharge['SERVICE_NAME'], -1, 1) !== ':' ? ':' : '')?></td>
			<td class="center"><?=GetMessage("RCPT_IND")?></td>
			<?php
			// ������� �������� �� ������������� �� 1�
			if (isset($arCharge["AMOUNT_VIEW"]) && strlen($arCharge["AMOUNT_VIEW"])) : ?>
			<td class="num"><?=$arCharge["AMOUNT_VIEW"]?></td>
			<?php else : ?>
			<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array("AMOUNT"), $arCharge, true, $arCharge['AMOUNT'] - $arCharge['HAMOUNT'], 3)?></td>
			<?php endif; ?>
			<td class="center"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array("UNITS"), $arCharge, false, $arCharge["SERVICE_UNITS"])?></td>
			<td class="num"><?=$arCharge["RATE"]?></td>
			<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue((($arCharge["SUM_WITHOUT_RAISE"] != 0) ? "SUM_WITHOUT_RAISE" : "SUMM"), $arCharge, true)?></td>
            <td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("RAISE_MULTIPLIER", $arCharge, true)?></td>
            <td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("RAISE_SUM", $arCharge, true)?></td>
			<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("CORRECTION", $arCharge, true)?></td>
			<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("DEBT_BEG", $arCharge, true)?></td>
			<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("SUMM_PAYED", $arCharge, true)?> </td>
			<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("SUMM2PAY", $arCharge, true)?></td>
		</tr>
		<?endif;?>
				<?endforeach?>
        <?
        if (!$arResult["HAS_SEPARATE_PENALTIES_RECEIPT"] && $arResult["TOTALS"]["PENALTIES"] > 0)
        {
        ?>
        <tr>
            <td class="num" colspan="11"><?= GetMessage("RCPT_PENALTIES_BY_PERIOD") ?>:</td>
            <td class="num bordered"><?= __citrusReceiptNum($arResult["TOTALS"]["PENALTIES"]) ?></td>
        </tr>
        <?
        }
        ?>
	</tbody>
	<tfoot>
        <?if ($arResult["IS_OVERHAUL"] != "Y"):?>
            <tr>
                <td colspan="5">
                    <?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])):?>
                        <?=GetMessage("RCPT_TOTAL_CHARGED_WITHOUT_INSURANCE")?>
                    <?else:?>
                        <?=GetMessage("RCPT_TOTAL_CHARGED")?>
                    <?endif;?>
                </td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["SUMM"])?></td>
                <td></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["RAISE_SUM"])?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["CORRECTION"])?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["DEBT_BEG"])?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num(/*$arResult["TOTALS"]["SUMM_PAYED"]*/$arResult["ACCOUNT_PERIOD"]["SUM_PAYED"])?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"])?></td>
            </tr>
            <?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])):?>
            <tr>
                <td><?=$arResult["INSURANCE"]["SERVICE_NAME"].GetMessage("TPL_FOR").CTszhPeriod::Format(date('Y-m-d',strtotime($arResult["ACCOUNT_PERIOD"]["PERIOD_DATE"]." +2 month")));?></td>
                <td></td>
                <td class="num"><?=$arResult["INSURANCE"]["AMOUNT"];?></td>
                <td></td>
                <td class="num"><?=$arResult["INSURANCE"]["RATE"]?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["INSURANCE"]["SUMM"])?></td>
                <td></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num(0)?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num(0)?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num((isset($arResult["INSURANCE"]["DEBT_BEG"])) ? $arResult["INSURANCE"]["DEBT_BEG"] : "0")?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["INSURANCE"]["SUMM_PAYED"])?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["INSURANCE"]["SUMM"])?></td>
            </tr>
            <tr>
                <td colspan="5"><?=GetMessage("RCPT_TOTAL_CHARGED_WITH_INSURANCE")?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["SUMM"] + $arResult["INSURANCE"]["SUMM"])?></td>
                <td></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["RAISE_SUM"])?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["CORRECTION"])?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["DEBT_BEG"])?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num(/*$arResult["TOTALS"]["SUMM_PAYED"] + $arResult["INSURANCE"]["SUMM_PAYED"]*/$arResult["ACCOUNT_PERIOD"]["SUM_PAYED"])?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num(/*$arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"] + $arResult["INSURANCE"]["SUMM"]*/$arResult["ACCOUNT_PERIOD"]["SUM_PAYED"])?></td>
            </tr>
            <?endif;?>
        <?else:?>
            <tr>
                <td colspan="5"><?=GetMessage("RCPT_TOTAL_CHARGED")?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["_SUMM"])?></td>
                <td></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["RAISE_SUM"])?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["CORRECTION"])?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["DEBT_BEG"])?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num(/*$arResult["TOTALS"]["SUMM_PAYED"]*/$arResult["ACCOUNT_PERIOD"]["SUM_PAYED"])?></td>
                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"])?></td>
            </tr>
        <?endif;?>
	</tfoot>
</table>
<table>
	<tr>
		<td colspan="2" class="bt small tl">

			<div class="cut-line"></div>

			<div style="overflow: hidden;">
				<?
				if (!$arResult["IS_FINES_RECEIPT"])
				{
					?>
					<table class="border counters" style="width: 100%; float: left;">
					<thead>
						<t>
							<th colspan="11"><?=GetMessage("RCPT_REF_INFO")?></th>
						</t>
						<tr style="font-size: 8px;">
							<th rowspan="3"><?=GetMessage("RCPT_CONTRACTOR_NUMBER")?></th>
							<th rowspan="3"><?=GetMessage("RCPT_SERVICE_KINDS")?></th>
							<th rowspan="3"><?=GetMessage("RCPT_IPU_ODPU")?></th>
							<th rowspan="3"><?=GetMessage("RCPT_METER_NUMBER")?></th>
							<th colspan="2"><?=GetMessage("RCPT_PU_VALUES")?></th>
							<th rowspan="3"><?=GetMessage("RCPT_VOLUME_CONSUMPTION")?></th>
							<th colspan="2"><?=GetMessage("RCPT_BY_NORM")?></th>
							<th colspan="2"><?=GetMessage("RCPT_CONSUMPTION")?></th>
						</tr>
						<tr style="font-size: 8px;">
							<th rowspan="2"><?=GetMessage("RCPT_METER_LAST_VALUES")?></th>
							<th rowspan="2"><?=GetMessage("RCPT_METER_CUR_VALUES")?></th>
							<th rowspan="2"><?=GetMessage("RCPT_IN_HOUSE_ROOMS")?></th>
							<th rowspan="2"><?=GetMessage("RCPT_PO_ODN")?></th>
							<th rowspan="2"><?=GetMessage("RCPT_IN_HOUSE_ROOMS")?></th>
							<th rowspan="2"><?=GetMessage("RCPT_PO_ODN")?></th>
						</tr>
						<tr>
						</tr>
					</thead>
					<tbody>
					<?
					$pu_number = 0;
					$arIndexMeters = array();

					foreach ($arResult["CHARGES"] as $arCharge)
					{
						if ($arCharge["COMPONENT"] == 'N')
						{
						$arMeterValues = array();
							if ($arResult["HAS_CHARGES_METERS_BINDING"])
							{
								foreach ($arCharge["METER_IDS"] as $meterID)
								{
									$value = __vdgbGetMeterValues($arResult["METERS"][$meterID]);
									__vdgbFillCountersValues("METERS",$value,$arMeterValues,$pu_number,$arIndexMeters,$meterID,$arCharge,$arResult);
								}
							}
							else
							{
								foreach ($arResult["METERS"] as $arMeter)
								{
									if (trim($arMeter["~SERVICE_NAME"]) == trim($arCharge["~SERVICE_NAME"]))
									{
										$value = __vdgbGetMeterValues($arMeter);
										__vdgbFillCountersValues("METERS",$value,$arMeterValues,$pu_number,$arIndexMeters,$meterID,$arCharge,$arResult);
										break;
									}
								}
							}
							foreach ($arCharge["HMETER_IDS"] as $meterID)
							{
								$value = __vdgbGetMeterValues($arResult["HMETERS"][$meterID]);
								__vdgbFillCountersValues("HMETERS",$value,$arMeterValues,$pu_number,$arIndexMeters,$meterID,$arCharge,$arResult);
							}
					if (!empty($arMeterValues)):?>
					<?foreach($arMeterValues as $meterValue):?>
					<tr>
						<td class="center"><?=$meterValue['pu_number']?></td>
						<td style="vertical-align: middle;/*max-width:100px;*/"><?=$arCharge["SERVICE_NAME"]?></td>
						<td class="center"><?=$meterValue['type']?><?/*=$arCharge["SERVICE_UNITS"]*/?></td>
						<td style="/*max-width:120px;*/"><?=$meterValue['number']?></td>
						<td class="num"><?=$meterValue['bef_value']?></td>
						<td class="num"><?=$meterValue['cur_value']?></td>
						<td class="num"><?=$meterValue['amount']?></td>
						<td class="num"><?=$meterValue['inorm']?></td>
						<td class="num"><?=$meterValue['hnorm']?></td>
						<td class="num"><?=$meterValue['volumep']?></td>
						<td class="num"><?=$meterValue['volumeh']?></td>
					</tr>
					<?endforeach?>
					<?endif;
					}

					}

					?>
					</tbody>
				</table>
					<?
				}
				?>
			</div>
					<div class="cut-line"></div>
			<div style="overflow: hidden">
				<?
				if (!$arResult["IS_FINES_RECEIPT"])
				{
				?>
				<table class="border counters" style="width: 25%; float:left;">
					<thead>
					<tr>
						<th colspan="3"><?=GetMessage("RCPT_CORRECTIONS_INFO")?></th>
					</tr>
					<tr>
						<th><?=GetMessage("RCPT_SERVICE_KINDS")?></th>
						<th><?=GetMessage("RCPT_SUM_RUB")?></th>
						<th><?=GetMessage("RCPT_CORRECTION_REASON")?></th>
					</tr>
					</thead>
					<tbody>
					<?foreach ($arResult["CORRECTIONS"] as $arCorrection):?>
					<tr>
						<td><?=$arCorrection["SERVICE"]?></td>
						<td class="num"><?=__citrusReceiptNum($arCorrection["SUMM"])?></td>
						<td><?=$arCorrection["GROUNDS"]?></td>
					</tr>
					<?endforeach?>
					</tbody>
				</table>
				<?
				}
				?>
				<?
				if (!$arResult["IS_FINES_RECEIPT"])
				{
				?>
				<table class="border counters" style="width: 70%; float: right;">
					<thead>
					<tr>
						<th colspan="7"><?=GetMessage("RCPT_INSTALLMENT_INFO")?></th>
					</tr>
					<tr>
						<th rowspan="2"><?=GetMessage("RCPT_CONTRACTOR_NUMBER")?></th>
						<th rowspan="2"><?=GetMessage("RCPT_SERVICE_KINDS")?></th>
						<th colspan="2"><?=GetMessage("RCPT_PAYMENT")?></th>
						<th colspan="2"><?=GetMessage("RCPT_INSTALLMENT_PERCENTS")?></th>
						<th rowspan="2"><?=GetMessage("RCPT_ITOGO")?></th>
					</tr>
					<tr>
						<th><?=GetMessage("RCPT_PAYMENT_BY_PERIOD")?></th>
						<th><?=GetMessage("RCPT_PAYMENT_BY_PREV_PERIOD")?></th>
						<th><?=GetMessage("RCPT_CURRENCY")?></th>
						<th>%</th>
					</tr>
					</thead>
					<tbody>
					<?$arInstallmentsTotalSumToPay = 0;?>
							<?foreach ($arResult["INSTALLMENTS"] as $arInstallment):?>
					<tr>
						<td class="center"><?=$arInstallment["RECEIPT_ORDER"]?></td>
						<td><?=$arInstallment["SERVICE"]?></td>
						<td class="num"><?=__citrusReceiptNum($arInstallment["SUMM_PAYED"])?></td>
						<td class="num"><?=__citrusReceiptNum($arInstallment["SUMM_PREV_PAYED"])?></td>
						<td class="num"><?=__citrusReceiptNum($arInstallment["SUMM_RATED"])?></td>
						<td class="num"><?=__citrusReceiptNum($arInstallment["PERCENT"])?></td>
						<td class="num"><?=__citrusReceiptNum($arInstallment["SUMM2PAY"])?></td>
						<?$arInstallmetnsTotalSumToPay += $arInstallment["SUMM2PAY"];?>
					</tr>
					<?endforeach?>
					</tbody>
					<tfoot>
					<tr>
						<td colspan="6"><?=GetMessage("RCPT_INSTALLMENTS_ITOGO")?></td>
						<td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arInstallmetnsTotalSumToPay)?></td>
					</tr>
					</tfoot>
				</table>
				<?
				}
				?>
			</div>
			<?
			$host = \CBXPunycode::ToUnicode($_SERVER["SERVER_NAME"], $errors);
			?>
			<div style="font-style: italic;">
				<?//=str_replace("#URL#", ($APPLICATION->IsHttps() ? "https" : "http") . "://" . $host . "/", $arParams["NOTE_TEXT"])?>
				<?=GetMessage("RCPT_NOTE_TEXT") . ":"?>
				<?=$arResult["IS_OVERHAUL"] ? $arResult["TSZH"]["ANNOTATION_OVERHAUL"] : $arResult["TSZH"]["ANNOTATION_MAIN"]?>
			</div>
		</td>
	</tr>
</table>
<?

$summ2pay = $arResult["ACCOUNT_PERIOD"]["DEBT_END"];
if (COption::GetOptionString("citrus.tszh", "pay_to_executors_only", "N") == "Y") {
	if (CModule::IncludeModule("vdgb.portaljkh") && method_exists("CCitrusPortalTszh", "setPaymentBase"))
		CCitrusPortalTszh::setPaymentBase($arResult["TSZH"]);
	$summ2pay = CTszhAccountContractor::GetList(array(), array("ACCOUNT_PERIOD_ID" => $arResult["ACCOUNT_PERIOD"]["ID"], "!CONTRACTOR_EXECUTOR" => "N"), array("SUMM"))->Fetch();
	$summ2pay = is_array($summ2pay) ? $summ2pay['SUMM'] - $arResult["ACCOUNT_PERIOD"]["PREPAYMENT"] : 0;
}
if ($summ2pay > 0 && CModule::IncludeModule("citrus.tszhpayment") && ($paymentPath = CTszhPaySystem::getPaymentPath($arResult["TSZH"]["SITE_ID"])))
	echo '<div class="no-print">' . GetMessage("CITRUS_TSZHPAYMENT_LINK", Array("#LINK#" => $paymentPath . '?ptype=' . $arResult['RECEIPT_PAYMENT_TYPE'])) . '</div>';

?>
</div>
<?if ($_GET["print"] == "Y"):
	if ($arResult["IS_LAST"]):
		if ($arResult["MODE"] == "AGENT"):?>
			</div>
		<?else:?>
			</body>
			</html>
		<?
		exit();
		endif;
	endif;

	if (in_array($arResult["MODE"], array("ADMIN", "AGENT")))
	{
		return;
	}
	else
	{
		require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
		die();
	}
endif;
