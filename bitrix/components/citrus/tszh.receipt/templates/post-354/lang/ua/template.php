<?
$MESS["TPL_DOCUMENT_DATE"] = "за <strong>#DATE#</strong> р.";
$MESS["TPL_DOCUMENT_TITLE"] = "платіжний документ";
$MESS["TPL_DOCUMENT_SUBTITLE"] = "для внесення плати за утримання і ремонт житлового приміщення та надання комунальних послуг";
$MESS["TPL_SECTION1"] = "Розділ 1 . <strong> Відомості про платника і виконавця послуг < /STRONG >";
$MESS["TPL_SECTION2"] = "Розділ 2 . <strong> Інформація для внесення плати одержувачу платежу (одержувачам платежів) </STRONG >";
$MESS["TPL_SECTION3"] = "Розділ 3 . <strong> РОЗРАХУНОК РОЗМІРУ ПЛАТИ ЗА ЗМІСТ І РЕМОНТ ЖИТЛОВОГО ПРИМІЩЕННЯ ТА КОМУНАЛЬНІ ПОСЛУГИ </STRONG >";
$MESS["TPL_SECTION4"] = "Розділ 4 . <strong> Довідкова інформація < /STRONG >";
$MESS["TPL_SECTION5"] = "Розділ 5 . <strong> Відомості про перерахунки ( донарахування + , зменшення -) </STRONG >";
$MESS["TPL_PERIOD"] = "( Розрахунковий період )";
$MESS["TPL_ACCOUNT_OWNER"] = "П.І.Б. ( Найменування ) платника власника /наймача";
$MESS["TPL_AREA"] = "Площа приміщення: <strong>#VAL#кв.м. </STRONG >";
$MESS["TPL_PEOPLE"] = "Кількість проживаючих : . <strong>#VAL#чол </STRONG >";
$MESS["TPL_ORG_NAME"] = "Найменування організації";
$MESS["TPL_ORG_INN"] = "ІПН";
$MESS["TPL_ADDRESS"] = "Адреса";
$MESS["TPL_PHONE_FAX"] = "Телефон , факс";
$MESS["TPL_DISP"] = "диспетчерська";
$MESS["TPL_EXECUTOR_TITLE"] = "Найменування організації - виконавця послуг";
$MESS["TPL_PAYEE"] = "Найменування інструменти одержувача інструменти платежу";
$MESS["TPL_BANK_INFO"] = "Номер банківського рахунку інструменти та банківські реквізити";
$MESS["TPL_ACCOUNT_NUM"] = "№ л / рахунку (інший інструменти ідентифікатор інструменти платника )";
$MESS["TPL_SERVICES"] = "види послуг";
$MESS["TPL_PERIOD_SUMM_TO_PAY"] = "Разом до сплати за інструменти розр. період грн.";
$MESS["TPL_ACCOUNT_SYN"] = "о/р №";
$MESS["TPL_NOTE_INFO"] = "Довідково";
$MESS["TPL_PREV_DEBT"] = "Заборгованість за попередні періоди";
$MESS["TPL_PREPAYMENT"] = "Аванс на початок розр. періоду";
$MESS["TPL_PREPAYMENT_DOLG"] = "Заборгованість на початок розр. періоду";
$MESS["TPL_PAYMENT_NOTE"] = "( Враховано платежі, що надійшли до 25 числа розрахункового періоду включно)";
$MESS["TPL_LAST_PAYMENT"] = "Дата останньої надійшла оплати";
$MESS["TPL_TOTAL_TO_PAY"] = "Всього до оплати";
$MESS["TPL_UNITS"] = "Од. ізм .";
$MESS["TPL_SERVICE_AMOUNT"] = "Обсяг комуна - <br>льных услуг*";
$MESS["TPL_TARIFF"] = "тариф";
$MESS["TPL_SERVICE_PAYMENT"] = "Розмір плати за інструменти ком. послуги, грн.";
$MESS["TPL_PERIOD_SUMM"] = "Всего<br>начисл. за<br>розрахунковий<br>період<br>грн.";
$MESS["TPL_RAISE_MULTIPLIER"] = "Підвищ. Коефі-<br/>цієнт";
$MESS["TPL_RAISE_SUM"] = "Сума<br/>підвищення";
$MESS["TPL_PERIOD_CORRECTIONS"] = "Перерах-<br>унки<br>всього,<br>грн.";
$MESS["TPL_PERIOD_COMPENSATIONS"] = "Пiльги,<br>субси-<br>дii,<br>грн.";
$MESS["TPL_PERIOD_PENALTIES"] = "пені";
$MESS["TPL_NORM"] = "Норматив інструменти споживання інструменти ком. послуг";
$MESS["TPL_METERS_CURRENT"] = "Поточні показання інструменти приладів обліку інструменти ком. послуг";
$MESS["TPL_TOTAL_VOLUME"] = "Сумарний обсяг інструменти ком. послуг в будинку";
$MESS["TPL_TOTAL"] = "всього";
$MESS["TPL_FOR_SERVICE"] = "в т.ч. за ком. ум.";
$MESS["TPL_PERSONAL_CONS"] = "індив. інструменти потреб .";
$MESS["TPL_SHARED_CONS"] = "общед . інструменти потреби";
$MESS["TPL_PERSONAL_CONS1"] = "інд. інструменти ( квартір. )";
$MESS["TPL_SHARED_CONS1"] = "общед - інструменти мових";
$MESS["TPL_PERIOD_TOTAL_TO_PAY"] = "Разом до сплати за розрахунковий період";
$MESS["TPL_FOOTER_NOTES"] = "<p>* &mdash; указывается объем коммунальных услуг, определенный, исходя из:</p>
<blockquote>
(1) &mdash; нормативов потребления коммунальных услуг;<br>
(2) &mdash; показаний индивидуальных (квартирных) приборов учета;<br>
(3) &mdash; среднемесячного потребления коммунальных услуг;<br>
(4) &mdash; исходя из показаний общедомового прибора учета;<br>
(5) &mdash; расчетного способа для нежилых помеещний.<br>
</blockquote>
";
$MESS["TPL_CORRECTION_GROUNDS"] = "підстави перерахунків";
$MESS["TPL_SUMM_RUB"] = "Сума , грн.";
$MESS["RCPT_OPEN_RECEIPT_IN_WINDOW"] = "Відкрити квитанцію в окремому вікні для друку";
$MESS["TPL_BACK_TO_PERSONAL"] = "Повернутися в Особистий кабінет";
$MESS["TPL_LANDSCAPE_NOTE"] = "<br><span style=\"margin-left: 1.6em;\">(</span>У налаштуваннях принтера слід встановити <strong>альбомну</strong> орієнтацію паперу, у разі якщо вона не була обрана автоматично)";
$MESS["CITRUS_TSZHPAYMENT_LINK"] = "<p>Ви можете <a href=\"#LINK#\">оплатити комунальні послуги</a> онлайн.</p>";
?>