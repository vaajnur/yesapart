<?
$MESS["RCPT_YOU_CAN"] = "Ви можете";
$MESS["RCPT_OPEN_RECEIPT_IN_WINDOW"] = "відкрити квитанцію в окремому вікні";
$MESS["RCPT_FOR_PRINTING"] = "для друку.";
$MESS["RCPT_TITLE"] = "Квитанція";
$MESS["RCPT_NOTICE"] = "ПОВІДОМЛЕННЯ";
$MESS["RCPT_FOR"] = "за";
$MESS["RCPT_YEAR_ABBR"] = "р.";
$MESS["RCPT_ACCOUNT_ABBR"] = "О/р";
$MESS["RCPT_IN"] = "в";
$MESS["RCPT_INN"] = "ІПД";
$MESS["RCPT_KPP"] = "КПП";
$MESS["RCPT_RSCH"] = "р/р";
$MESS["RCPT_KSCH"] = "к/с";
$MESS["RCPT_BIK"] = "МФО";
$MESS["RCPT_SUMM_TO_PAY"] = "Разом до сплати:";
$MESS["RCPT_CREDIT_END"] = "Залишок на кінець місяця :";
$MESS["RCPT_DEBT_END"] = "Заборгованість на кінець місяця :";
$MESS["RCPT_METER_VALUES_ON_DATE"] = "Показання лічильників на дату";
$MESS["RCPT_CHECK"] = "КВИТАНЦІЯ";
$MESS["RCPT_PEOPLE_LIVING"] = "Мешкає:#PEOPLE# чол.;";
$MESS["RCPT_AREA"] = "Заг. Площа: #AREA#;";
$MESS["RCPT_LIVING_AREA"] = "Жит. Площа: #AREA#.";
$MESS["RCPT_METER_VALUES_ON"] = "Показання лічильників на #DATE#";
$MESS["RCPT_CURRENCY"] = "грн.";
$MESS["RCPT_CREDIT_BEG"] = "Заборгованість на початок місяця";
$MESS["RCPT_DEBT_BEG"] = "Залишок на початок місяця";
$MESS["RCPT_NORM"] = "Норма";
$MESS["RCPT_TARIFF"] = "Тариф";
$MESS["RCPT_QUANTITY"] = "К-сть";
$MESS["RCPT_PENI"] = "Пені";
$MESS["RCPT_TO_PAY"] = "До сплати";
$MESS["RCPT_UNITS"] = "Од.<br/>вим.";
$MESS["RCPT_TOTAL_PAYED"] = "Разом сплачено:";
$MESS["CITRUS_TSZHPAYMENT_LINK"] = "<p> Ви можете <a href=\"#LINK#\"> оплатити комунальні послуги </a> онлайн.</p>";

$MESS["RCPT_HEADER"] = "БЛАНК ДЛЯ ПЕРЕДАЧІ показань приладів обліку";
$MESS["RCPT_NUM_SIGN"] = "№";
$MESS["RCPT_HEADER_NOTE"] = "Показання приладів обліку знімаються в період з #START_DATE# по #END_DATE# число розрахункового місяця і вписуються в таблицю нижче.<br />Заповнений відривний бланк опустіть в ящик керуючої організації.";

$MESS["RCPT_METER_SERVICE_NAME"] = "Найменування";
$MESS["RCPT_METER_NUMBER"] = "Номер приладу";
$MESS["RCPT_METER_LAST_VALUES"] = "Останні свідчення";
$MESS["RCPT_METER_CUR_VALUES"] = "Поточні показання";
$MESS["RCPT_METER_DATE"] = "Дата зняття показань";
$MESS["RCPT_DATA_ACCEPT"] = "Дані підтверджую";
$MESS["RCPT_CUT_LINE"] = "Лінія відрізу";

$MESS["RCPT_RECIPIENT"] = "Одержувач<br />платежу";
$MESS["RCPT_REQUISITES"] = "Реквізити";

$MESS["RCPT_CALC_PERIOD"] = "Розрахунковий період";
$MESS["RCPT_ADDRESS"] = "Адреса";

$MESS["RCPT_CONTACT_CENTER"] = "Контактний<br />центр";
$MESS["RCPT_PAY_SUM"] = "Сума оплати";
$MESS["RCPT_ACCOUNT_NUMBER"] = "Особовий рахунок";
$MESS["RCPT_FIO"] = "П.І.О.";

$MESS["RCPT_SIGN"] = "Підпис";
$MESS["RCPT_DATE"] = "Дата";

$MESS["RCPT_PAYER"] = "Платник";

$MESS["RCPT_FLAT_TYPE"] = "Тип квартири";
$MESS["RCPT_COMMON_LIVING_AREA"] = "Загальна / житлова площа";
$MESS["RCPT_REGISTERED_PEOPLE_PEOPLE"] = "Зареєстровано / Проживає";
$MESS["RCPT_EXEMPT_PEOPLE"] = "Пільговиків";
$MESS["RCPT_HOUSE_AREA"] = "Загальна площа будівлі";
$MESS["RCPT_HOUSE_ROOMS_AREA"] = "Площа приміщень (житлових та нежитлових)";
$MESS["RCPT_HOUSE_MOP_AREA"] = "Площа місць загального користування";

$MESS["RCPT_ACCRUED_BY_PERIOD"] = "Нараховано за період";
$MESS["RCPT_DOLG_AVANS"] = "Борг / аванс на початок періоду (+/-)";
$MESS["RCPT_PAYED_BY_PERIOD"] = "Надійшло за період";
$MESS["RCPT_CREDIT_PAYED"] = "в т.ч. передоплата розстрочки";
$MESS["RCPT_PENALTIES_BY_PERIOD"] = "Нараховано пені";
$MESS["RCPT_LAST_PAYMENT_DATE"] = "Дата останньої оплати";
$MESS["RCPT_PAY_BEFORE"] = "Сплатити рахунок до";

$MESS["RCPT_SERVICES_TABLE_TITLE"] = "РАСШИФРОВКА РАХУНКУ ДЛЯ ВНЕСЕННЯ ПЛАТИ ЗА ВИДАМИ НАДАНУ ПОСЛУГУ";
$MESS["RCPT_PERS_CONSUMPTION_ABBR"] = "індив.<br />спожив.";
$MESS["RCPT_COMMON_HOUSE_CONSUMPTION_ABBR"] = "загаль-<br />нобуд.<br />потр.";
$MESS["RCPT_TOTAL"] = "Всього";
$MESS["RCPT_INCLUDING"] = "в тому числі";

$MESS["RCPT_CONTRACTOR_NUMBER"] = "№<br />поста<br />чальника";
$MESS["RCPT_SERVICE_KINDS"] = "Види послуг";
$MESS["RCPT_VOLUME"] = "Обсяг";
$MESS["RCPT_CHARGED"] = "Размер платы за<br />оказ. услуги";
$MESS["RCPT_CHARGED2"] = "Всього<br />нарах.";
$MESS["RCPT_RAISE_MULTIPLIER"] = "Підвищ. Коефі-<br/>цієнт";
$MESS["RCPT_RAISE_SUM"] = "Сума<br/>підвищення";
$MESS["RCPT_CORRECTION"] = "Пере-<br />ра-<br />хунки";
$MESS["RCPT_PRIVIL"] = "Піль-<br />ги,<br />суб-<br />сидії";
$MESS["RCPT_TO_PAY2"] = "Разом до сплати за<br />розрахунковий період грн.";

$MESS["RCPT_OTHER_SERVICES"] = "КОМУНАЛЬНІ ПОСЛУГИ";
$MESS["RCPT_TOTAL_CHARGED"] = "Разом до сплати за розрахунковий період";

$MESS["RCPT_CONTRACTORS_INFO"] = "Відомості про постачальників";
$MESS["RCPT_CONTRACTOR_NAME"] = "Найменування постачальника";
$MESS["RCPT_CONTRACTOR_CONTACTS"] = "Адреса та телефон контактного центру";
$MESS["RCPT_CONTRACTOR_ACCOUNT"] = "Особовий рахунок у постачальника";
$MESS["RCPT_CHARGES_INFO"] = "Відомості по нарахуваннях";
$MESS["RCPT_ON_PERIOD_BEGIN"] = "На початок періоду";
$MESS["RCPT_PAID_BY_PERIOD"] = "Оплачено за період";
$MESS["RCPT_PENALTIES_PENI"] = "Штрафи, пені, грн.";
$MESS["RCPT_ACCRUED_BY_PERIOD_RUB"] = "Нараховано за період, грн.";
$MESS["RCPT_TO_PAY_RUB"] = "Разом до сплати, грн.";

$MESS["RCPT_CORRECTIONS_INFO"] = "Відомості про перерахунки";
$MESS["RCPT_CORRECTION_REASON"] = "Підстава перерахунку";
$MESS["RCPT_SUM_RUB"] = "Сума, грн.";

$MESS["RCPT_INSTALLMENT_INFO"] = "Розрахунок суми оплати з урахуванням розстрочки платежу";
$MESS["RCPT_PAYMENT_BY_PERIOD"] = "Оплата за розр. період";
$MESS["RCPT_PAYMENT_BY_PREV_PERIOD"] = "Оплата за попер. період";
$MESS["RCPT_INSTALLMENT_PERCENTS"] = "Відсотки за розстрочку";
$MESS["RCPT_ITOGO"] = "Разом";

$MESS["RCPT_REF_INFO"] = "Довідкова інформація";
$MESS["RCPT_SERVICE"] = "Вид комунальної послуги";
$MESS["RCPT_CONSUMPTION"] = "Сумарний обсяг комунальних послуг";
$MESS["RCPT_IN_HOUSE_ROOMS"] = "в приміщеннях будинку";
$MESS["RCPT_COMMON_HOUSE_CONSUMPTION"] = "на загальнобудинкові потреби";
$MESS["RCPT_PO_IPU"] = "по ІПО";
$MESS["RCPT_BY_NORM"] = "за нормативом";
$MESS["RCPT_DIFFERENCE"] = "розбіжність";
