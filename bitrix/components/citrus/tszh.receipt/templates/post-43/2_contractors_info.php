<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
if ($arResult["IS_OVERHAUL"])
{
	$arReplaceBilling = array(
		"#ORG_BANK#" => strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BANK"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BANK"] : $arResult["ORG_BANK"],
		"#ORG_BIK#" => strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BIK"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BIK"] : $arResult["ORG_BIK"],
		"#ORG_RS#" => strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_RS"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_RS"] : $arResult["ORG_RSCH"],
		"#ORG_KS#" => strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_KS"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_KS"] : $arResult["ORG_KSCH"],
	);
	$str_CONTRACTOR_BILLING = GetMessage("CITRUS_TSZH_POST354_BILLING", $arReplaceBilling);
}
else
{
	$arReplaceBilling = array(
		"#ORG_BANK#" => strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_BANK"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_BANK"] : $arResult["ORG_BANK"],
		"#ORG_BIK#" => strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_BIK"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_BIK"] : $arResult["ORG_BIK"],
		"#ORG_RS#" => strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_RS"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_RS"] : $arResult["ORG_RSCH"],
		"#ORG_KS#" => strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_KS"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_KS"] : $arResult["ORG_KSCH"],
	);
	$str_CONTRACTOR_BILLING = GetMessage("CITRUS_TSZH_POST354_BILLING", $arReplaceBilling);
}
?>
<table class="rcpt-inner rcpt-inner-table">
	<tr>
		<th><?=GetMessage("TPL_PAYEE_NUM")?></th>
		<th><?=GetMessage("TPL_PAYEE")?></th>
		<th><?=GetMessage("TPL_BANK_INFO")?></th>
		<th><?=GetMessage("TPL_ACCOUNT_NUM_IZHKU")?></th>

		<th><?=GetMessage("TPL_PERIOD_SUMM_TO_PAY")?></th>
		<th><?=GetMessage("TPL_BARCODES")?></th>
	</tr>
	<?
	foreach ($arResult['CONTRACTORS'] as $arContractor)
	{//echo "<pre>"; var_dump($arResult); echo "</pre>";

		?>
		<tr>
			<td style="white-space: nowrap; text-align: center;"><?=$arContractor['CONTRACTOR_ID']?></td>
			<td><?=$arContractor['CONTRACTOR_NAME']?></td>
			<? if (!$arResult["IS_OVERHAUL"] && $arContractor["IS_OVERHAUL"] == 1)
			{
				$arReplaceBilling = array(
					"#ORG_BANK#" => $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BANK"],
					"#ORG_BIK#" => $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BIK"],
					"#ORG_RS#" => $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_RS"],
					"#ORG_KS#" => $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_KS"],
				);
				$str_CONTRACTOR_BILLING_OVERHAUL = GetMessage("CITRUS_TSZH_POST354_BILLING", $arReplaceBilling);
				?>
				<td><?=$arContractor["CONTRACTOR_EXECUTOR"] != "N" ? $str_CONTRACTOR_BILLING_OVERHAUL : $arContractor['CONTRACTOR_BILLING']?></td>
			<? }
			else
			{ ?>
				<td><?=$arContractor["CONTRACTOR_EXECUTOR"] != "N" ? $str_CONTRACTOR_BILLING : $arContractor['CONTRACTOR_BILLING']?></td><?
			} ?>
			<td style="white-space: nowrap; text-align: center;">
				<?=GetMessage("TPL_ACCOUNT_SYN")?><?=$arResult['ACCOUNT_PERIOD']['XML_ID']?>
				<?if ($arResult["IS_OVERHAUL"])
					echo ($arResult["ACCOUNT_PERIOD"]["IZHKU_OVERHAUL"] ? ' / '. $arResult["ACCOUNT_PERIOD"]["IZHKU_OVERHAUL"] : ($arResult["ACCOUNT_PERIOD"]["IZHKU_MAIN"] ? ' / '. $arResult["ACCOUNT_PERIOD"]["IZHKU_MAIN"] : "") );
				else
					echo ($arResult["ACCOUNT_PERIOD"]["IZHKU_MAIN"] ? ' / '. $arResult["ACCOUNT_PERIOD"]["IZHKU_MAIN"] : "");
				?>
			</td>

			<td style="text-align: center;"><?=CCitrusTszhReceiptComponentHelper::num($arContractor['SUMM'] ? $arContractor['SUMM'] : $arContractor['SUMM_CHARGED'])?></td>
			<?

			$type['TYPE'] = $arContractor['BARCODE_TYPE'];
			$val['VALUE'] = $arContractor['~BARCODE_VALUE'];
			$barcode = array_merge($type, $val);
			//echo "<pre>"; var_dump($arContractor, $type, $val, $barcode); echo "</pre>";
			/** @noinspection PhpAssignmentInConditionInspection */

			if (strlen($val['VALUE']) > 0 && $link = CTszhBarCode::getImageLink($barcode))
			{
				$code128 = $barcode["TYPE"] == 'code128';
				?>
				<td class="rcpt-barcode-cell"><img style="<?=($code128 ? 'width: 5.5cm' : 'width: 3cm; height: 3cm; max-height: 4cm;')?>"
				                                             class="rcpt-barcode" src="<?=$link?>" alt="<?=htmlspecialcharsbx($barcode["VALUE"])?>">
				</td><?
			}else{?>
				<td>&nbsp</td>
			<?}

			?>
		</tr>
		<?
	}
	?>

</table>