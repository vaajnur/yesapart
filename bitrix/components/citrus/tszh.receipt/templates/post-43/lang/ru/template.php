<?
$MESS["TPL_DOCUMENT_DATE"] = "за <strong class=\"rcpt-big\">#DATE#</strong> г.";
$MESS["TPL_DOCUMENT_DATE2"] = "за ";
$MESS["TPL_DOCUMENT_TITLE"] = "Платежный документ";
$MESS["TPL_DOCUMENT_SUBTITLE_FOR_BASIC"] = "для внесения платы за содержание жилого помещения и предоставление коммунальных услуг";
$MESS["TPL_DOCUMENT_SUBTITLE_FOR_BASIC_PENALTIES"] = "для внесения платы пени за содержание жилого помещения и предоставление коммунальных услуг";
$MESS["TPL_DOCUMENT_SUBTITLE_FOR_MAJOR_REPAIRS"] = "для внесения взносов на капитальный ремонт общего имущества в многоквартирном доме";
$MESS["TPL_DOCUMENT_SUBTITLE_FOR_MAJOR_REPAIRS_PENALTIES"] = "для внесения платы пени по взносам на капитальный ремонт общего имущества в многоквартирном доме";
$MESS["TPL_DOCUMENT_IPD"] = "Идентификатор платежного документа";
$MESS["TPL_DOCUMENT_ELS"] = "Единый лицевой счет";
$MESS["TPL_SECTION1"] = "Раздел 1. <strong>Сведения о плательщике и исполнителе услуг</strong>";
$MESS["TPL_SECTION6"] = "Раздел 5. <strong>Информация для внесения платы получателю платежа (получателям платежей)</strong>";
$MESS["TPL_SECTION3"] = "Раздел 3. <strong>РАСЧЕТ РАЗМЕРА ПЛАТЫ ЗА СОДЕРЖАНИЕ И РЕМОНТ ЖИЛОГО ПОМЕЩЕНИЯ И КОММУНАЛЬНЫЕ УСЛУГИ</strong>";
$MESS["TPL_SECTION3_END"] = "<small>Линия отрыва</small>";
$MESS["TPL_SECTION4"] = "Раздел 4. <strong>Справочная информация</strong>";
$MESS["TPL_SECTION5"] = "Раздел 6. <strong>Сведения о перерасчетах (доначислении +, уменьшении -)</strong>";
$MESS["TPL_SECTION2"] = "Раздел 2. <strong>Сведения о показаниях ИПУ</strong>";
$MESS["TPL_SECTION7"] = "<strong>Сведения о получателях платежа</strong>";
$MESS["TPL_PERIOD"] = "(расчетный период)_________________________";
$MESS["TPL_ACCOUNT_OWNER"] = "Ф.И.О. (наименование) плательщика собственника/нанимателя";
$MESS["CHARGES_DATE"] = "Дата сдачи показаний:___________________________";
$MESS["CHARGES_OWNER"] = "Показания сдал:";
$MESS["CHARGES_OWNER2"] = "___________________________";
$MESS["CHARGES_OWNER3"] = "______________________________________________";
$MESS["CHARGES_OWNER4"] = "<small>(Личная подпись)</small>";
$MESS["CHARGES_OWNER5"] = "<small>(расшифровка подписи)</small>";
$MESS["CHARGES_DATE"] = "Дата сдачи показаний:___________________________";
$MESS["CHARGES_DATE"] = "Дата сдачи показаний:___________________________";
$MESS["TPL_AREA"] = "Площадь помещ. : <strong>#AREA# кв.м.</strong>";
$MESS["TPL_PEOPLE"] = "Кол-во прожив. / зарег.: <strong>#PEOPLE# / #REGISTERED_PEOPLE# чел.</strong>";
$MESS["TPL_ORG_NAME"] = "Наименование организации";
$MESS["TPL_ORG_INN"] = "ИНН";
$MESS["TPL_ADDRESS"] = "Адрес";
$MESS["TPL_PHONE_FAX"] = "тел.:";
$MESS["TPL_URL"] = "Сайт управляющей компании: ";
$MESS["TPL_DISP"] = "Диспетчерская";
$MESS["TPL_EXECUTOR_TITLE"] = "Наименование организации-исполнителя услуг";
$MESS["TPL_PAYEE"] = "Наименование<br>получателя<br>платежа";
$MESS["TPL_PAYEE_NUM"] = "№<br>получателя<br>платежа";
$MESS["TPL_BANK_INFO"] = "Номер банковского счета<br>и банковские реквизиты";
$MESS["TPL_ACCOUNT_NUM"] = "№ л/счета (иной<br>идентификатор<br>плательщика)";
$MESS["TPL_ACCOUNT_NUM_IZHKU"] = "№ л/счета (иной<br>идентификатор<br>плательщика) / Идентификатор ЖКУ";
$MESS["TPL_SERVICES"] = "Виды услуг";
$MESS["TPL_PERIOD_SUMM_TO_PAY"] = "Итого к оплате за<br>расч. период руб.";
$MESS["TPL_ACCOUNT_SYN"] = "л/с №";
$MESS["TPL_NOTE_INFO"] = "Справочно";
$MESS["TPL_PREV_DEBT"] = "Задолженность за предыдущие периоды";
$MESS["TPL_PREPAYMENT"] = "Аванс на начало расч. периода";
$MESS["TPL_PREPAYMENT_DOLG"] = "Задолженность<br/>/Переплата(-),руб.";
$MESS["TPL_LAST_PAYMENT"] = "Дата посл. оплаты";
$MESS["TPL_LAST_PAYED_SUMM"] = "Внесено оплат от ";
$MESS["TPL_TOTAL_TO_PAY"] = "Итого к оплате ";
$MESS["TPL_UNITS"] = "Ед. изм.";
$MESS["TPL_NORM_IND"] = "Норматив<br/> потребления <br/>ком.услуг";
$MESS["TPL_NORM_SOI"] = "Норматив<br/> потребления<br/> услуг СОИ и <br/>общедом.услуг";
$MESS["TPL_BARCODES"] = "Штриховой<br/>код";
$MESS["TPL_SERVICE_AMOUNT"] = "Объем коммуна-<br>льных услуг";
$MESS["TPL_TARIFF"] = "Тариф /<br>Разм.<br>пл.";
$MESS["TPL_SERVICE_PAYMENT"] = "Размер платы за<br>ком. услуги, руб.";
$MESS["TPL_PERIOD_SUMM"] = "Начисл. за<br>расчетный<br>период<br>руб.";
$MESS["TPL_RAISE_SUM"] = "Размер<br/>превыш.платы с уч.<br/>повыш.коэф";
$MESS["TPL_RAISE_MULTIPLIER"] = "Размер<br/>повышающего<br/>коэффициента";
$MESS["TPL_PERIOD_CORRECTIONS"] = "Перерас-<br>четы<br>всего,<br>руб.";
$MESS["TPL_PERIOD_COMPENSATIONS"] = "Льготы,<br>субси-<br>дии,<br>руб.";
$MESS["TPL_PERIOD_TO_PAY"] = "К оплате за<br>расчетный период,<br>рублей";
$MESS["TPL_PERIOD_PENALTIES"] = "Неустойка<br>(штраф, пеня)";
$MESS["TPL_NORM_METER_NAME"] = "Номер счетчика";
$MESS["TPL_NORM_METER_ABBR"] = "ИПУ";
$MESS["TPL_NORM_SERVICE_ABBR"] = "Наименование коммунальной<br> услуги";
$MESS["TPL_NORM"] = "Предыдущие показания<br>приборов учета<br>ком. услуг";
$MESS["TPL_METERS_CURRENT"] = "Текущие показания<br>приборов учета<br>ком. услуг";
$MESS["TPL_TOTAL_VOLUME"] = "Суммарный объем<br>ком. услуг в доме";
$MESS["TPL_TOTAL"] = "Всего";
$MESS["TPL_FOR_SERVICE"] = "в т.ч. за ком. усл.";
$MESS["TPL_PERSONAL_PERIOD"] ="Период";
$MESS["TPL_PERSONAL_CHARGES"] ="Показания";
$MESS["TPL_PERSONAL_PIC"] ="дн.";
$MESS["TPL_PERSONAL_DAY"] ="ночь";
$MESS["TPL_PERSONAL_NIGHT"] ="пик";
$MESS["TPL_PERSONAL_CONS"] = "индив.<br>потреб.";
$MESS["TPL_SHARED_CONS"] = "Объем услуг<br> СОИ или <br>общ.услуг";
$MESS["TPL_PERSONAL_CONS1"] = "инд.<br>(квартир.)";
$MESS["TPL_SHARED_CONS1"] = "общедо-<br>мовых";
$MESS["TPL_VOLUMEA"] = "учетный<br>объем";
$MESS["TPL_FOR"] = " за ";
$MESS["TPL_PERIOD_TOTAL_TO_PAY"] = "Итого к оплате";
$MESS["TPL_PERIOD_TOTAL_TO_PAY_WITHOUT_INSURANCE"] = "Итого к оплате за расчетный период без учета добр. страхования";
$MESS["TPL_PERIOD_TOTAL_TO_PAY_WITH_INSURANCE"] = "Итого к оплате за расчетный период с учетом добр. страхования";
$MESS["TPL_TO_PAY_WITHOUT_INSURANCE"] = "Без учета добровольного страхования";
$MESS["TPL_TO_PAY_WITH_INSURANCE"] = "С учетом добровольного страхования";
$MESS["TPL_PERIOD_TO_PAY_WITHOUT_INSURANCE"] = "Итого к оплате без учета добр. страх-ия: ";
$MESS["TPL_PERIOD_TO_PAY_WITH_INSURANCE"] = "Итого к оплате с учетом добр. страх-ия: ";
$MESS["TPL_FOOTER_NOTES"] = "<p>* &mdash; указывается объем коммунальных услуг, определенный, исходя из:</p>

<blockquote>
(1) &mdash; нормативов потребления коммунальных услуг;<br>
(2) &mdash; показаний индивидуальных (квартирных) приборов учета;<br>
(3) &mdash; среднемесячного потребления коммунальных услуг;<br>
(4) &mdash; исходя из показаний общедомового прибора учета;<br>
(5) &mdash; расчетного способа для нежилых помеещний.<br>
</blockquote>
";
$MESS["TPL_CORRECTION_GROUNDS"] = "Основания перерасчетов";
$MESS["TPL_SUMM_RUB"] = "Сумма, руб.";
$MESS["RCPT_OPEN_RECEIPT_IN_WINDOW"] = "Открыть квитанцию в отдельном окне для печати";
$MESS["TPL_BACK_TO_PERSONAL"] = "Вернуться в Личный кабинет";
$MESS["TPL_LANDSCAPE_NOTE"] = "<br><span style=\"margin-left: 1.6em;\">(</span>В настройках принтера следует установить <strong>альбомную</strong> ориентацию бумаги, в случае если она не была выбрана автоматически)";
$MESS["CITRUS_TSZHPAYMENT_LINK"] = "<p>Вы можете <a href=\"#LINK#\">оплатить услуги</a> онлайн.</p>";

$MESS["CITRUS_TSZH_POST354_SUMMPAYED"] = "Внесено оплат";

$MESS["CITRUS_TSZH_POST354_INSTALLMENT_SERVICES"] = "Виды услуг";
$MESS["CITRUS_TSZH_POST354_INSTALLMENT_SUMM1"] = "Сумма платы с учетом рассрочки платежа";
$MESS["CITRUS_TSZH_POST354_INSTALLMENT_SUMM2"] = "Сумма к оплате с учетом<br>рассрочки платежа и<br>процентов за рассрочку";
$MESS["CITRUS_TSZH_POST354_INSTALLMENT_PERCENT"] = "Проценты за<br>рассрочку";
$MESS["CITRUS_TSZH_POST354_INSTALLMENT_SUMM1_1"] = "от платы за<br>расчетный период";
$MESS["CITRUS_TSZH_POST354_INSTALLMENT_SUMM1_2"] = "от платы за<br>предыдущие расчетные периоды";
$MESS["CITRUS_TSZH_POST354_INSTALLMENT_CURRENCY"] = "руб.";
$MESS["CITRUS_TSZH_POST354_INSTALLMENT_TOTAL"] = "Итого к оплате за расчетный период с учетом рассрочки:";
$MESS["CITRUS_TSZH_POST354_CREDIT_PAYED"] = "в т.ч. предоплата рассрочки";
$MESS["CITRUS_TSZH_POST354_INSURANCE_PAYED"] = "в т.ч. оплата добр. страх-ия";
$MESS["CITRUS_TSZH_POST354_BILLING"] = "Р/с #ORG_RS# в #ORG_BANK#, к/с #ORG_KS#, БИК #ORG_BIK#";
$MESS["RCPT_NOTE_TEXT"] = "Примечание";

$MESS["RCPT_CONTRACTOR_NUMBER"] = "№<br />постав<br />щика";
$MESS["RCPT_SERVICE_KINDS"] = "Виды услуг";
$MESS["RCPT_VOLUME"] = "Объем";
$MESS["RCPT_CHARGED"] = "Размер платы за<br />оказ. услуги";
$MESS["RCPT_CHARGED2"] = "Всего<br />начисл.";
$MESS["RCPT_RAISE_MULTIPLIER"] = "Повыш. коэффи-<br/>циент";
$MESS["RCPT_RAISE_SUM"] = "Сумма<br/> повыше-<br/>ния";
$MESS["RCPT_CORRECTION"] = "Пере-<br />рас-<br />четы";
$MESS["RCPT_PRIVIL"] = "Льго-<br />ты,<br />суб-<br />сидии";
$MESS["RCPT_TO_PAY2"] = "Итого к оплате за<br />расч. период руб.";
