<?
$MESS["TSZH_MODULE_NOT_FOUND_ERROR"] = "Модуль 1С: Сайт ЖКХ не установлен.";
$MESS["TSZH_NOT_A_MEMBER"] = "Вы не являетесь владельцем лицевого счета.";
$MESS["TSZH_NO_DATA"] = "Нет данных о начисленных услугах.";
$MESS["TSZH_NO_PERIOD"] = "Период не найден.";
$MESS["TSZH_PRINT_ERROR_TEMPLATE_NOT_FOUND"] = "Шаблон для печати не найден.";
$MESS["TSZH_HOUSING_REPLACE"] = ", корпус ";
?>