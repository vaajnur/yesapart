<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!CModule::IncludeModule('citrus.tszh')) {
    ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));

    return;
}

if (!CTszhFunctionalityController::CheckEdition()) {
    return;
}

if (strlen($arParams["REGCODE_PROPERTY"]) <= 0) {
    $arParams["REGCODE_PROPERTY"] = "UF_REGCODE";
}
$arParams['PERSONAL_PATH'] = trim($arParams['PERSONAL_PATH']);
if (strlen($arParams["PERSONAL_PATH"]) <= 0) {
    $arParams['PERSONAL_PATH'] = "#SITE_DIR#personal/";
}
$arParams['PERSONAL_PATH'] = str_replace("#SITE_DIR#", SITE_DIR, $arParams['PERSONAL_PATH']);

$arResult = Array();
$arErrors = Array();

if (!$USER->IsAuthorized()) {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

    return;
}


$arResult["TSZH_LIST"] = Array();
$rsTszh = CTszh::GetList(
    Array("NAME" => "DESC"),
    Array(),
    false, //mixed arGroupBy = false
    false, //mixed arNavStartParams = false
    Array("ID", "NAME") //array arSelectFields = Array());
);
while ($arTszh = $rsTszh->GetNext()) {
    $arResult["TSZH_LIST"][$arTszh["ID"]] = $arTszh;
}

if (count($arResult["TSZH_LIST"]) <= 0) {
    ShowError(GetMessage("TSZH_NO_TSZHS"));

    return;
}


if (class_exists('CCitrusJkhOrg')) {
    $arTszh = CCitrusJkhOrg::getCurrent();
    $tszhCode = $arTszh['CODE'];

    $dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "jkh_users_" . $tszhCode));
    if ($arGroup = $dbGroup->Fetch()) {
        $groupID = $arGroup['ID'];
    } else {
        $groupID = false;
    }
} else {
    $groupID = false;
}

if ($_SERVER["REQUEST_METHOD"] == 'POST' && $_POST['action'] == 'confirm' && check_bitrix_sessid()) {

    $arResult["TSZH"] = IntVal($_REQUEST['tszh']);
    $arResult["REG_CODE"] = htmlspecialcharsbx(trim($_REQUEST['regcode']));

    if (CTszh::IsTenant() && is_array($arResult['ACCOUNT'])) {
        $arErrors[] = GetMessage("TSZH_ALREADY_CONFIRMED");
    }

    $user_donor = CUser::GetByLogin($_REQUEST['regcode'])->Fetch();

    if (count($arErrors) <= 0) {
        /*if ($_POST['accounts'])
        {*/
        $arAccount = CTszhAccount::GetList(array(), array("USER_ID" => $user_donor['ID'], "@ID" => $_POST['accounts']));
        //}

        while ($rsAccount = $arAccount->Fetch()) {


            define("TSZH_CANCEL_REDIRECT", true);
            if (CTszhAccount::Update($rsAccount["ID"], Array("USER_ID" => $GLOBALS["USER"]->GetID()))) {
                $userID = $GLOBALS["USER"]->GetID();
                $arGroups = array_unique(array_merge(
                    CUser::GetUserGroup($userID),
                    Array(
                        CTszh::GetTenantGroup(),
                    ),
                    false !== $groupID ? Array($groupID) : Array()
                ));
                CUser::SetUserGroup($userID, $arGroups);
                $GLOBALS['USER']->Logout();
                $GLOBALS['USER']->Authorize($userID);
                $arResult["NOTE"] = GetMessage("CONFIRM_ACCOUNT_SUCCESS");
                $arResult["ACCOUNT"] = $arAccount;

            } else {
                $_POST['error'] == 'true';
                $arErrors[] = GetMessage("ERROR_SAVING_ACCOUNT");

            }


        }

    }

    if (count($arErrors) > 0) {
    }
    ?>
    <script type="text/javascript">
        function a() {


            var expression = document.getElementById("confirm-regcode").value;

            alert(expression);
        }

    </script>

    <?

    $arResult["ERRORS"] = implode('<br />', $arErrors);
}

$arResult["REG_CODE"] = htmlspecialcharsbx(trim($_REQUEST['regcode']));

$user_donor = CUser::GetByLogin($_REQUEST['regcode'])->Fetch();
$row = $user_donor['PASSWORD'];
$salt = substr($row, 0, strlen($row) - 32);
$db_password = substr($row, -32); // ��� ������������ � ������ � �� b_user
$pass = $_REQUEST['password'];
$user_password = md5($salt . $pass);
$accs=array();
if ($db_password == $user_password) {
    $arraccs = CTszhAccount::GetList(array(), array("USER_ID" => $user_donor['ID']));


    while ($acc = $arraccs->GetNext()) {
        $accs[] = $acc;
    }
} else {
    $arraccs = 0;

}
if ($arraccs != 0) {
    foreach ($accs as $ac) {
        if ($ac['USER_ID'] != 0) {
            $arResult["ACC"] = $accs;
        } else {
            $arResult["ACC"] = $arErrors[] = GetMessage("WRONG_PASSWORD");
        }
    }
} else {
    $arResult["ACC"] = $arErrors[] = GetMessage("WRONG_PASSWORD");
}
$this->IncludeComponentTemplate();
?>