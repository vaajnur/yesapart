<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("TSZH_CONFIRM_ACCOUNT"),
	"DESCRIPTION" => GetMessage("TSZH_CONFIRM_ACCOUNT_DESC"),
	"ICON" => "/images/menu_ext.gif",
	"PATH" => array(
		"ID" => "citrus.tszh",
		"NAME" => GetMessage("COMPONENT_ROOT_SECTION"),
	),
);
?>