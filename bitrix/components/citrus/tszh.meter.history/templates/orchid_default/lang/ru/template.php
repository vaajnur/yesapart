<?
$MESS["CITRUS_TSZH_METER_VALUES_NOT_FOUND"] = "Показаний счетчиков не найдено.";
$MESS["CITRUS_TSZH_METERS_NAME"] = "Наименование счетчика:";
$MESS["CITRUS_TSZH_METERS_NUM"] = "Заводской номер:";
$MESS["CITRUS_TSZH_METERS_SERVICE_NAME"] = "Наименование услуги:";
$MESS["CITRUS_TSZH_METER_TARIF_1"] = "Дневное показание";
$MESS["CITRUS_TSZH_METER_TARIF_2"] = "Ночное показание";
$MESS["CITRUS_TSZH_METER_TARIF_3"] = "Пиковое показание";
$MESS["CITRUS_TSZH_METERS_DATE_INPUT"] = "Дата подачи";
$MESS["CITRUS_TSZH_METERS_VALUE"] = "Показания";
$MESS["CITRUS_TSZH_CONSUMPTION"] = "Расход";
$MESS["CITRUS_TSZH_PERIOD"] = "Период";
$MESS["CITRUS_TSZH_NO_DATA"] = "Недостаточно данных";
?>