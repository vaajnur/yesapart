<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (empty($arResult['METERS']))
{
	ShowNote(GetMessage("CITRUS_TSZH_METER_VALUES_NOT_FOUND"));

	return;
}
?>
<h4 class="history-title"><?=GetMessage("CITRUS_TSZH_METERS_HISTORY_TITLE")?></h4>
<table class="meter-info">
	<tr>
		<td><?=GetMessage("CITRUS_TSZH_METERS_NAME")?></td>
		<td><?=$arResult["METERS"]["NAME"]?></td>
	</tr>
	<tr>
		<td><?=GetMessage("CITRUS_TSZH_METERS_NUM")?></td>
		<td><?=$arResult["METERS"]["NUM"]?></td>
	</tr>
	<tr>
		<td><?=GetMessage("CITRUS_TSZH_METERS_DATE")?></td>
		<td><?=$arResult["METERS"]["VERIFICATION_DATE"]?></td>
	</tr>
	<tr>
		<td><?=GetMessage("CITRUS_TSZH_METERS_SERVICE_NAME")?></td>
		<td><?=$arResult["METERS"]["SERVICE_NAME"]?></td>
	</tr>
</table>
<table class="data-table meter-value-info">
	<tr>
		<th width="40%" <?=(($arResult["MAX_VALUES_COUNT"] > 1) ? "rowspan=\"2\"" : "")?>>
			<?=GetMessage("CITRUS_TSZH_METERS_DATE_INPUT")?>
		</th>
		<th colspan="<?=$arResult["MAX_VALUES_COUNT"]?>">
			<?=GetMessage("CITRUS_TSZH_METERS_VALUE")?>
		</th>
	</tr>
	<? if ($arResult["MAX_VALUES_COUNT"] > 1) : ?>
		<tr>
			<? for ($i = 1; $i <= $arResult["MAX_VALUES_COUNT"]; $i++) : ?>
				<th class="tarif">
					<?=GetMessage("CITRUS_TSZH_METER_TARIF_".$i)?>
				</th>
			<? endfor; ?>
		</tr>
	<? endif; ?>
	<? foreach ($arResult["ROWS"] as $date => $arItem) : ?>
		<tr>
			<td><?=$arItem['INPUT_DATE']?></td>
			<? for ($i = 1; $i <= $arResult["MAX_VALUES_COUNT"]; $i++) : ?>
				<td><?=($arItem["VALUE" . $i] >= 0 ? (float)($arItem["VALUE" . $i]) : "")?></td>
			<? endfor; ?>
		</tr>
	<? endforeach; ?>
	<tr>
		<td colspan="3">
			<?
			if (count($arResult["ROWS"]) > 0)
			{
				$APPLICATION->IncludeComponent(
					"bitrix:main.pagenavigation",
					"",
					array(
						"NAV_OBJECT" => $arResult["NAV"],
						"SEF_MODE" => "N",
						"SHOW_COUNT" => "N",
						"COMPONENT_TEMPLATE" => ".default",
					),
					false
				);
			}
			else
			{
				echo GetMessage("CITRUS_TSZH_NO_DATA");
			}
			?>
		</td>
	</tr>
</table>

<div class="back">
	<i class="arrow"></i><a href="<?=$arResult["BACK_URL"]?>"><?=GetMessage("CITRUS_TSZH_METERS_BACK_URL_TEXT")?></a>
</div>