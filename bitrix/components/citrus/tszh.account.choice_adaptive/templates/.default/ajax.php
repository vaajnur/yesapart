<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('citrus.tszh');

if (array_key_exists('personalAccountId', $_REQUEST)) {
    $personalAccountId = IntVal($_REQUEST['personalAccountId']);
    CTszhAccount::SetCurrent($personalAccountId);
    $response = array(
        'STATUS' => true,
    );
} else {
    $response = array(
        'STATUS' => false,
        'ERROR' => 'Не обнаружен ID лицевого счета'
    );
}

echo \Bitrix\Main\Web\Json::encode($response);
?>