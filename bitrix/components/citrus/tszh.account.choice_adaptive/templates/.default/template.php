<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="choice_adaptive-wrapper">
    <div class="personal-account" id="choice_adaptive">
        <div class="personal-account__title">
            <?=GetMessage("PERSONAL_ACCOUNT")?>
        </div>
        <div class="personal-account__number-wrapper">
            <div class="personal-account__number">{{arrayResult.CURRENT.XML_ID}}</div>
            <a class="personal-account__add-icon link-theme-default" href="<?=SITE_DIR.PATH_PERSONAL."/confirm-account/"?>">
                <i class="fa fa-plus add-icon" aria-hidden="true"></i>
            </a>
            <div class="personal-account__choice-icon link-theme-default" v-bind:class="{'background-icon-active': popupListPersonalAccountsVisibilityEnabled}" v-on:click="clickListenerForPersonalAccountsList()" v-if='choiceIconVisibilityEnabled' ref="personalAccountChoiceIcon">
                <i class="fa fa-retweet choice-icon" aria-hidden="true" v-bind:class="{'icon-active': popupListPersonalAccountsVisibilityEnabled}"></i>
                <!--popup-->
                <div v-bind:class="{'personal-account__list-wrapper': popupListPersonalAccountsVisibilityEnabled}"></div>
                <div class="personal-account__list" v-if='popupListPersonalAccountsVisibilityEnabled' ref="personalAccountsList">
                    <div class="list__element" v-bind:class="{'list__element-not-active': !personalAccount.ACTIVE, 'list__element-selected': arrayResult.CURRENT.ID == personalAccount.ID}" v-for="(personalAccount) in arrayResult.ACCOUNTS" v-on:click="postSendSelectedPersonalAccountId(personalAccount.ID, personalAccount.ACTIVE)">
                        <div>{{personalAccount.XML_ID}} ({{personalAccount.ADDRESS_POPUP_LIST}})</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="personal-account__title-debt">
            <?=GetMessage("DEBT")?>
        </div>
        <div class="personal-account__debt">
            <span v-if="arrayResult.DEBT">{{arrayResult.DEBT.DEBT_END}}<?=GetMessage("RUB")?></span>
            <span v-else=""><?=GetMessage("DEBT_ZERO")?></span>
        </div>
        <div class="personal-account__title-address">
            <?=GetMessage("ADDRESS")?>
        </div>
        <div class="personal-account__address">
            {{arrayResult.CURRENT.ADDRESS}}
        </div>
        <div class="personal-account__pay-button">
            <a href="<?=SITE_DIR.PATH_PERSONAL."/payment/"?>">
                <button class="form-variable__button form-variable__saved link-theme-default">
                    <?=GetMessage("BUTTON_PAY")?>
                </button>
            </a>
        </div>
    </div>
</div>


<?
$frame = $this->createFrame()->begin('');
?>
<?
//\Bitrix\Main\UI\Extension::load("ui.vue");
?>
<script>
	var arrayResultForJs = <?echo \Bitrix\Main\Web\Json::encode($arResult, $options = null);?>;
    var templateFolderForJs = <?echo \Bitrix\Main\Web\Json::encode($templateFolder, $options = null);?>;
</script>
<script type="text/javascript" src="<?=$templateFolder?>/vue.js" async></script>
<?
$frame->end();
?>