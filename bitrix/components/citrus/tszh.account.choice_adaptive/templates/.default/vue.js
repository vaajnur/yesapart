BX.Vue.create({
    el: "#choice_adaptive",
    data: {
        arrayResult: arrayResultForJs,
        personalAccounts: [],
        clickOutside: false,
        templateFolder: templateFolderForJs,
        choiceIconVisibilityEnabled: false,
        numberClicksForChoiceIcon: 0,
        popupListPersonalAccountsVisibilityEnabled: false,
    },
    methods: {
        strToBooleanPesonalAccountsActive: function () {
            this.personalAccounts = this.arrayResult.ACCOUNTS;
            $.each(this.personalAccounts, function (index, personalAccount) {
                if (personalAccount.ACTIVE == 'Y') {
                    personalAccount.ACTIVE = true;
                } else if (personalAccount.ACTIVE == 'N') {
                    personalAccount.ACTIVE = false;
                }
            });
        },
        visibilityChoiceIcon: function () {
            if (this.arrayResult.ACCOUNTS.length > 1) {
                this.choiceIconVisibilityEnabled = true;
            } else {
                this.choiceIconVisibilityEnabled = false;
            }
        },
        clickListenerForPersonalAccountsList: function () {
            this.numberClicksForChoiceIcon += 1;
            if (!this.popupListPersonalAccountsVisibilityEnabled) {
                document.addEventListener('click', this.listPersonalAccountsVisibilityEnabled);
                this.popupListPersonalAccountsVisibilityEnabled = true;
            }
        },
        listPersonalAccountsVisibilityEnabled: function (e) {
            clickOnChoiceIcon = this.$refs.personalAccountChoiceIcon.contains(e.target);
            clickOnPersonalAccountList = this.$refs.personalAccountsList.contains(e.target);
            if (!clickOnPersonalAccountList && (this.numberClicksForChoiceIcon > 1)) {
                document.removeEventListener('click', this.listPersonalAccountsVisibilityEnabled);
                this.popupListPersonalAccountsVisibilityEnabled = false;
                this.numberClicksForChoiceIcon = 0;
            } else if (!clickOnPersonalAccountList && !clickOnChoiceIcon) {
                document.removeEventListener('click', this.listPersonalAccountsVisibilityEnabled);
                this.popupListPersonalAccountsVisibilityEnabled = false;
                this.numberClicksForChoiceIcon = 0;
            }
        },
        postSendSelectedPersonalAccountId: function (pesonalAccountId, personalAccountActive) {
            if (personalAccountActive) {
                BX.ajax({
                    url: this.templateFolder+'/ajax.php',
                    method: 'POST',
                    data: {
                        personalAccountId: pesonalAccountId,
                    },
                    onsuccess: function (data) {
                        data = JSON.parse(data);
                        status = data["STATUS"];
                        if (status) {
                            window.location.reload();
                        }
                    }
                });
            }
        },
    },
    mounted: function () {
        this.strToBooleanPesonalAccountsActive();
        this.visibilityChoiceIcon();
    },
});