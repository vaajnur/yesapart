<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
if (!CModule::IncludeModule('citrus.tszh')) {
    ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));
    return;
}
if (!CTszhFunctionalityController::CheckEdition()) {
    return;
}
if (!CTszh::IsTenant()) {
    return;
}

global $USER;

$arResult["ACCOUNTS"] = Array();

$dbAccounts = CTszhAccount::GetList(
    $arOrder = array(),
    $arFilter = array("USER_ID" => CUser::GetID()),
    $arGroupBy = false, $arNavStartParams = false,
    $arSelectFields = array("*")
);

$arResult["CURRENT"] = null;

while ($arAccount = $dbAccounts->GetNext()) {
    $arAccount["TITLE"] = GetMessage("TSZH_ACCOUNT_TITLE", Array(
        "#NUM#" => $arAccount['XML_ID'],
        "#TEXT#" => CTszhAccount::GetFullAddress($arAccount, Array("REGION", "CITY", "TOWN", "SETTLEMENT", "DISTRICT")),
    ));
    $arAccount["ADDRESS"] = CTszhAccount::GetFullAddress(
        $arAccount,
        Array(
            "REGION",
            "TOWN",
            "DISTRICT",
        ));
    $arAccount["ADDRESS_POPUP_LIST"] = CTszhAccount::GetFullAddress(
        $arAccount,
        Array(
            "REGION",
            "CITY",
            "TOWN",
            "DISTRICT",
        ));
    $arResult["ACCOUNTS"][] = $arAccount;

    if (is_null($arResult["CURRENT"]) || $arAccount["CURRENT"] == "Y") {
        $arResult["CURRENT"] = $arAccount;
    }

}

// �������� ������� ������ �������� ������������
$rsAccountPeriod = CTszhAccountPeriod::GetList(
    false,
    array("ACCOUNT_ID" => $arResult["CURRENT"]["ID"]),
    false,
    false,
    array("PERIOD_ID")
);

$arAccountPeriod = array();

while ($arPeriod = $rsAccountPeriod->fetch()) {
    $arAccountPeriod[] = $arPeriod["PERIOD_ID"];
}

// �� ������ �������� �������� ����� "������" �� ����, ���� ������ ��� ����� ����� ��������� ������ ������� ����������
if (count($arAccountPeriod) > 0) {
    $lastAccountPeriod = CTszhPeriod::GetList(
        array('DATE' => 'DESC'),
        array("ID" => $arAccountPeriod),
        false, false, array("ID")
    )->fetch()["ID"];
} else {
    $lastAccountPeriod = CTszhPeriod::GetLast($arResult["CURRENT"]["TSZH_ID"]);
}

// �������� �������������
$arAccountsDebt = CTszhAccountPeriod::GetList(
    false,
    array(
        "ACCOUNT_ID" => $arResult["CURRENT"]["ID"],
        "PERIOD_ID" => $lastAccountPeriod,
    ),
    false,
    false,
    array("DEBT_END")
)->GetNext();

$arResult["DEBT"] = $arAccountsDebt;

if (count($arResult["ACCOUNTS"]) > 0) {
    $this->IncludeComponentTemplate();
}
?>