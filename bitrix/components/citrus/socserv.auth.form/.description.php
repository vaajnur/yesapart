<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SOCSERV_AUTH_FORM_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("SOCSERV_AUTH_FORM_COMPONENT_DESCR"),
	"PATH" => array(
		"ID" => "utility",
	),
);
?>