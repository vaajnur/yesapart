<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
__IncludeLang(dirname(__FILE__) . "/lang/" . LANGUAGE_ID . "/" . basename(__FILE__));

class CCitrusTszhOfficeHoursEdit extends CBitrixComponent
{
	public static function checkValues(&$arOfficeHours, &$arErrors)
	{
		$arErrors = array();

		if (empty($arOfficeHours))
		{
			$arOfficeHours = array();
			return true;
		}

		if (!is_array($arOfficeHours))
		{
			$arErrors[] = Array("id" => "OFFICE_HOURS_FORMAT", "text" => GetMessage("CLS_ERROR_NOT_ARRAY"));
			return false;
		}

		// ������ ������ ��������
		foreach ($arOfficeHours as $deptKey => $arDept)
		{
			$emptyDeptFlag = true;
			if (is_set($arDept, "NAME") && strlen(trim($arDept["NAME"])) > 0)
				$emptyDeptFlag = false;

			if (is_set($arDept, "SCHEDULE") && is_array($arDept["SCHEDULE"]))
			{
				foreach ($arDept["SCHEDULE"] as $scheduleKey => $arScheduleItem)
				{
					$emptyScheduleItemFlag = true;

					if (is_set($arScheduleItem, "DAY") && strlen(trim($arScheduleItem["DAY"])) > 0)
					{
						$emptyScheduleItemFlag = false;
						$emptyDeptFlag = false;
					}

					if (is_set($arScheduleItem, "HOURS") && is_array($arScheduleItem["HOURS"]))
					{
						foreach ($arScheduleItem["HOURS"] as $hourKey => $hour)
						{
							if (strlen(trim($hour)) > 0)
							{
								$emptyScheduleItemFlag = false;
								$emptyDeptFlag = false;
							}
							else
								unset($arOfficeHours[$deptKey]["SCHEDULE"][$scheduleKey]["HOURS"][$hourKey]);
						}
					}

					if ($emptyScheduleItemFlag)
						unset($arOfficeHours[$deptKey]["SCHEDULE"][$scheduleKey]);
				}
			}

			if ($emptyDeptFlag)
				unset($arOfficeHours[$deptKey]);
		}

		foreach ($arOfficeHours as $deptKey => $arDept)
		{
			/*if (count($arDept) != 2)
				$arErrors[] = Array("id" => "OFFICE_HOURS_FORMAT_WRONG_ELEMENT_COUNT", "text" => GetMessage("CLS_ERROR_WRONG_ELEMENT_COUNT", array("#IDX#" => $deptKey+1)));*/
			if (!is_set($arDept, "NAME"))
				$arErrors[] = Array("id" => "OFFICE_HOURS_FORMAT_REQ_NAME", "text" => GetMessage("CLS_ERROR_REQ_NAME", array("#IDX#" => $deptKey+1)));
			if (!is_set($arDept, "SCHEDULE"))
				$arErrors[] = Array("id" => "OFFICE_HOURS_FORMAT_REQ_SCHEDULE", "text" => GetMessage("CLS_ERROR_REQ_SCHEDULE", array("#IDX#" => $deptKey+1)));

			$value = strval($arDept["NAME"]);
			$arOfficeHours[$deptKey]["NAME"] = $value;
			if (strlen($value) <= 0)
				$arErrors[] = Array("id" => "OFFICE_HOURS_EMPTY_DEPT_NAME", "text" => GetMessage("CLS_ERROR_EMPTY_DEPT_NAME", array("#IDX#" => $deptKey+1)));

			if (is_set($arDept, "SCHEDULE"))
			{
				if (is_array($arDept["SCHEDULE"]) && !empty($arDept["SCHEDULE"]))
				{
					foreach ($arDept["SCHEDULE"] as $scheduleKey => $arScheduleItem)
					{
						/*if (count($arScheduleItem) != 2)
							$arErrors[] = Array("id" => "OFFICE_HOURS_FORMAT_WRONG_SCHEDULE_ELEMENT_COUNT", "text" => GetMessage("CLS_ERROR_WRONG_SCHEDULE_ELEMENT_COUNT", array("#IDX#" => $deptKey+1, "#SCHEDULE_IDX#" => $scheduleKey+1)));*/

						if (is_set($arScheduleItem, "DAY"))
						{
							$value = strval($arScheduleItem["DAY"]);
							$arOfficeHours[$deptKey]["SCHEDULE"][$scheduleKey]["DAY"] = $value;
							if (strlen($value) <= 0)
								$arErrors[] = Array("id" => "OFFICE_HOURS_EMPTY_SCHEDULE_DAY", "text" => GetMessage("CLS_ERROR_EMPTY_SCHEDULE_DAY", array("#IDX#" => $deptKey+1, "#SCHEDULE_IDX#" => $scheduleKey+1)));
						}
						else
							$arErrors[] = Array("id" => "OFFICE_HOURS_FORMAT_REQ_SCHEDULE_DAY", "text" => GetMessage("CLS_ERROR_REQ_SCHEDULE_DAY", array("#IDX#" => $deptKey+1, "#SCHEDULE_IDX#" => $scheduleKey+1)));

						if (is_set($arScheduleItem, "HOURS"))
						{
							if (is_array($arScheduleItem["HOURS"]) && !empty($arScheduleItem["HOURS"]))
							{
								foreach ($arScheduleItem["HOURS"] as $hourKey => $hour)
								{
									$value = strval($hour);
									$arOfficeHours[$deptKey]["SCHEDULE"][$scheduleKey]["HOURS"][$hourKey] = $value;
									if (strlen($value) <= 0)
										$arErrors[] = Array("id" => "OFFICE_HOURS_EMPTY_SCHEDULE_HOUR", "text" => GetMessage("CLS_ERROR_EMPTY_SCHEDULE_HOUR", array("#IDX#" => $deptKey+1, "#SCHEDULE_IDX#" => $scheduleKey+1, "#HOURS_IDX#" => $hourKey+1)));
								}
							}
							else
								$arErrors[] = Array("id" => "OFFICE_HOURS_WRONG_SCHEDULE_HOURS", "text" => GetMessage("CLS_ERROR_WRONG_SCHEDULE_HOURS", array("#IDX#" => $deptKey+1, "#SCHEDULE_IDX#" => $scheduleKey+1)));
						}
						else
							$arErrors[] = Array("id" => "OFFICE_HOURS_FORMAT_REQ_SCHEDULE_HOURS", "text" => GetMessage("CLS_ERROR_REQ_SCHEDULE_HOURS", array("#IDX#" => $deptKey+1, "#SCHEDULE_IDX#" => $scheduleKey+1)));
					}
				}
				else
					$arErrors[] = Array("id" => "OFFICE_HOURS_FORMAT_WRONG_SCHEDULE", "text" => GetMessage("CLS_ERROR_WRONG_SCHEDULE", array("#IDX#" => $deptKey+1)));
			}
		}

		if (empty($arErrors) && !empty($arOfficeHours))
		{
			foreach ($arOfficeHours as $deptKey => $arDept)
			{
				if (is_array($arDept["SCHEDULE"]) && !empty($arDept["SCHEDULE"]))
				{
					foreach ($arDept["SCHEDULE"] as $scheduleKey => $arScheduleItem)
					{
						if (is_array($arScheduleItem["HOURS"]) && !empty($arScheduleItem["HOURS"]))
						{
							$arOfficeHours[$deptKey]["SCHEDULE"][$scheduleKey]["HOURS"] = array_values($arScheduleItem["HOURS"]);
						}
					}

					$arOfficeHours[$deptKey]["SCHEDULE"] = array_values($arDept["SCHEDULE"]);
				}
			}

			$arOfficeHours = array_values($arOfficeHours);
		}

		return empty($arErrors);
	}
}
