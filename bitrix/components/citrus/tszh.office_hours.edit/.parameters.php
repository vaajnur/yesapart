<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS"  =>  array(
		"OFFICE_HOURS"  =>  Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("P_OFFICE_HOURS"),
			"TYPE" => "STRING",
		),
	),
);

