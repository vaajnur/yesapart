<script>
var officeHoursEdit = {
	arWeekDays: [
		'<?=GetMessage("T_D_MON")?>',
		'<?=GetMessage("T_D_TUE")?>',
		'<?=GetMessage("T_D_WED")?>',
		'<?=GetMessage("T_D_THU")?>',
		'<?=GetMessage("T_D_FRI")?>',
		'<?=GetMessage("T_D_SAT")?>',
		'<?=GetMessage("T_D_SUN")?>'
	],

	getKeyCode: function(event)
	{
		var result = false;
		if (event.which)
			result = event.which;
		else if (event.keyCode)
			result = event.keyCode;

		return result;
	},

	getDayIdx: function(dayStr)
	{
		if (!dayStr.length)
			return false;

		var re = new RegExp('^' + dayStr + '$', 'i');
		for (var i in this.arWeekDays)
		{
			//if (arWeekDays[i].match(re))
			if (re.test(this.arWeekDays[i]))
				return parseInt(i) + 1;
		}

		return false;
	},

	strToDays: function(dayStr)
	{
		var result = [];

		dayStr = dayStr.replace(/\s/g, '');

		var arDays = dayStr.split(',');
		if (!arDays.length)
			return result;

		for (var i in arDays)
		{
			if (arDays[i].indexOf('<?=GetMessage("CITRUS_MDASH")?>') > -1)
			{
				var ar = arDays[i].split('<?=GetMessage("CITRUS_MDASH")?>');
				var firstIdx = this.getDayIdx(ar[0]);
				var lastIdx = this.getDayIdx(ar[1]);
				if (firstIdx)
					result.push(firstIdx);
				if (lastIdx)
				{
					result.push(lastIdx);
					if (firstIdx)
					{
						for (var j=firstIdx+1; j<lastIdx; j++)
							result.push(j);
					}
				}
			}
		}

		for (var i in arDays)
		{
			var res = this.getDayIdx(arDays[i]);
			if (res)
				result.push(res);
		}

		return result;
	},

	daysToStr: function(arDays)
	{
		var result = '';

		var arValidDays = [];
		for (var i in arDays)
		{
			var intDay = parseInt(arDays[i]);
			if (intDay > 0 && intDay <= this.arWeekDays.length)
				arValidDays.push(intDay);
		}
		arValidDays.sort();

		var arRanges = [];
		var first = false;
		var last = false;
		for (var i in arValidDays)
		{
			if (!first)
			{
				first = arValidDays[i];
				continue;
			}
			if (!last)
			{
				last = arValidDays[i];
				if (last != first+1)
				{
					arRanges.push([first, false]);
					first = last;
					last = false;
				}
				continue;
			}

			if (last+1 == arValidDays[i])
				last = arValidDays[i];
			else
			{
				arRanges.push([first, last]);
				first = arValidDays[i];
				last = false;
			}
		}
		if (first)
			arRanges.push([first, last]);

		var arResult = [];
		for (var i in arRanges)
		{
			if (arRanges[i][1])
			{
				if (arRanges.length == 1 && arRanges[i][0] == 6 && arRanges[i][1] == 7)
				{
					arResult.push(this.arWeekDays[arRanges[i][0]-1]);
					arResult.push(this.arWeekDays[arRanges[i][1]-1]);
				}
				else
					arResult.push(this.arWeekDays[arRanges[i][0]-1] + '<?=GetMessage("CITRUS_MDASH")?>' + this.arWeekDays[arRanges[i][1]-1]);
			}
			else
				arResult.push(this.arWeekDays[arRanges[i][0]-1]);
		}

		return arResult.join(', ');
	},

	isInside: function(element, tagName, className)
	{
		var result = false;
		if (element.tagName == tagName.toUpperCase() && BX.hasClass(element, className))
			result = true;
		else
			result = BX.findParent(element, {'tag': tagName.toLowerCase(), 'class': className});

		return result;
	},

	deleteHour: function(hourDivID)
	{
		if (!confirm('<?=GetMessage("T_CONFIRM_DEL_HOUR")?>'))
			return;

		BX.remove(BX(hourDivID));
	},

	deleteScheduleItem: function(closeButton)
	{
		if (!confirm('<?=GetMessage("T_CONFIRM_DEL_SCHEDULE_ITEM")?>'))
			return;

		var row = BX.findParent(
			closeButton,
			{'tag': 'tr'}
		);
		if (row)
			BX.remove(row);
	},

	deleteDept: function(closeButton)
	{
		if (!confirm('<?=GetMessage("T_CONFIRM_DEL_DEPT")?>'))
			return;

		var dept = BX.findParent(closeButton, {'tag': 'div', 'class': 'dept-container'});
		if (dept)
		{
			var deptKey = dept.getAttribute('data-deptKey');
			BX.remove(dept);
			var deptDivider = BX.findChild(BX('office-hours-container'), {'class': 'dept-divider', 'class': 'dept-divider-' + deptKey});
			if (deptDivider)
				BX.remove(deptDivider);
		}
	},

	getMaxAttr: function(container, oParams, attrName)
	{
		var arItems = BX.findChildren(container, oParams, false);
		var maxValue = -1;
		if (arItems)
		{
			for (var i in arItems)
			{
				var value = parseInt(arItems[i].getAttribute(attrName));
				if (value > maxValue)
					maxValue = value;
			}
		}

		return maxValue;
	},

	getHourDivInnerHtml: function(deptKey, scheduleKey, hourKey, divID, hourFieldID, hourPopupID)
	{
		var html =
			'<input id="' + hourFieldID + '" type="text" class="popup-input hour-input" name="OFFICE_HOURS[' + deptKey + '][SCHEDULE][' + scheduleKey + '][HOURS][' + hourKey + ']" value="" autocomplete="off" onfocus="officeHoursEdit.openHourPopup(event, \'' + hourPopupID + '\', \'' + hourFieldID + '\')" onkeyup="officeHoursEdit.onHourInputKeyUp(event, \'' + hourPopupID + '\', \'' + hourFieldID + '\')" />'
			+ ' <span class="button set-hour-button" title="<?=GetMessage("T_TITLE_SET_HOUR_BUTTON")?>" onclick="officeHoursEdit.openHourPopup(event, \'' + hourPopupID + '\', \'' + hourFieldID + '\')"></span>'
			+ '<div class="popup set-hour-popup" id="' + hourPopupID + '">'
				+ '<div class="content-container">'
					+ '<div class="error"></div>'
					+ '<input type="text" class="popup-hour-input hour1" min="0" max="23" title="<?=GetMessage("T_TITLE_HOURS")?>" onchange="officeHoursEdit.acceptHour(event, \'' + hourPopupID + '\', \'' + hourFieldID + '\', true)" value="" />'
					+ ' : '
					+ '<input type="text" class="popup-hour-input minute1" min="0" max="59" title="<?=GetMessage("T_TITLE_MINUTES")?>" onchange="officeHoursEdit.acceptHour(event, \'' + hourPopupID + '\', \'' + hourFieldID + '\', true)" value="" />'
					+ ' &ndash; '
					+ '<input type="text" class="popup-hour-input hour2" min="0" max="24" title="<?=GetMessage("T_TITLE_HOURS")?>" onchange="officeHoursEdit.acceptHour(event, \'' + hourPopupID + '\', \'' + hourFieldID + '\', true)" value="" />'
					+ ' : '
					+ '<input type="text" class="popup-hour-input minute2" min="0" max="59" title="<?=GetMessage("T_TITLE_MINUTES")?>" onchange="officeHoursEdit.acceptHour(event, \'' + hourPopupID + '\', \'' + hourFieldID + '\', true)" value="" />'
					+ '<br />'
					<?/*+ '<span class="small"><?=GetMessage("T_HOUSE_RANGE_HINT")?></span>'*/?>
					+ '<div class="holiday-checkbox-container">'
						+ '<label><input type="checkbox" class="hour-holiday" value="Y" onclick="officeHoursEdit.acceptHour(event, \'' + hourPopupID + '\', \'' + hourFieldID + '\', true)" /><?=GetMessage("T_HOLIDAY")?></label>'
					+ '</div>'
		            + '<div class="around-clock-checkbox-container">'
		            + '<label><input type="checkbox" class="hour-around-clock" value="Y" onclick="officeHoursEdit.acceptHour(event, \'' + hourPopupID + '\', \'' + hourFieldID + '\', true)" /><?=GetMessage("T_AROUND_CLOCK")?></label>'
		            + '</div>'
				+ '</div>'
				+ '<div class="buttons-container">'
					<?/*+ '<button onclick="officeHoursEdit.acceptHour(event, \'' + hourPopupID + '\', \'' + hourFieldID + '\')"><?=GetMessage("T_CHOOSE")?></button>'
					+ '<button onclick="officeHoursEdit.closePopup(event, \'' + hourPopupID + '\')"><?=GetMessage("T_CLOSE")?></button>'*/?>
				+ '</div>'
			+ '</div>'
			+ ' <span class="button del-hour-button" title="<?=GetMessage("T_TITLE_DEL_HOUR_BUTTON")?>" onclick="officeHoursEdit.deleteHour(\'' + divID + '\')"></span>';

		return html;
	},

	getScheduleTableRowInnerHtml: function(deptKey, scheduleKey, hourKey, dayFieldID, dayPopupID, divID, hourFieldID, hourPopupID)
	{
		var html =
			'<td>'
				+ '<div class="day-container">'
					+ '<input id="' + dayFieldID + '" type="text" class="popup-input day-input" name="OFFICE_HOURS[' + deptKey + '][SCHEDULE][' + scheduleKey + '][DAY]" value="" autocomplete="off" onfocus="officeHoursEdit.openDayPopup(event, \'' + dayPopupID + '\', \'' + dayFieldID + '\')" onkeyup="officeHoursEdit.onDayInputKeyUp(event, \'' + dayPopupID + '\', \'' + dayFieldID + '\')" />'
					+ ' <span class="button set-day-button" title="<?=GetMessage("T_TITLE_SET_DAY_BUTTON")?>" onclick="officeHoursEdit.openDayPopup(event, \'' + dayPopupID + '\', \'' + dayFieldID + '\')"></span>'
					+ '<div class="popup set-day-popup" id="' + dayPopupID + '">'
						+ '<div class="content-container">'
							+ '<label tabindex="1"><input type="checkbox" class="day-checkbox day-checkbox-1" value="1" onchange="officeHoursEdit.acceptDay(event, \'' + dayPopupID + '\', \'' + dayFieldID + '\', true)" /><?=GetMessage("T_D_MON")?></label>'
							+ '<label tabindex="2"><input type="checkbox" class="day-checkbox day-checkbox-2" value="2" onchange="officeHoursEdit.acceptDay(event, \'' + dayPopupID + '\', \'' + dayFieldID + '\', true)" /><?=GetMessage("T_D_TUE")?></label>'
							+ '<label tabindex="3"><input type="checkbox" class="day-checkbox day-checkbox-3" value="3" onchange="officeHoursEdit.acceptDay(event, \'' + dayPopupID + '\', \'' + dayFieldID + '\', true)" /><?=GetMessage("T_D_WED")?></label>'
							+ '<label tabindex="4"><input type="checkbox" class="day-checkbox day-checkbox-4" value="4" onchange="officeHoursEdit.acceptDay(event, \'' + dayPopupID + '\', \'' + dayFieldID + '\', true)" /><?=GetMessage("T_D_THU")?></label>'
							+ '<label tabindex="5"><input type="checkbox" class="day-checkbox day-checkbox-5" value="5" onchange="officeHoursEdit.acceptDay(event, \'' + dayPopupID + '\', \'' + dayFieldID + '\', true)" /><?=GetMessage("T_D_FRI")?></label>'
							+ '<label tabindex="6"><input type="checkbox" class="day-checkbox day-checkbox-6" value="6" onchange="officeHoursEdit.acceptDay(event, \'' + dayPopupID + '\', \'' + dayFieldID + '\', true)" /><?=GetMessage("T_D_SAT")?></label>'
							+ '<label tabindex="7"><input type="checkbox" class="day-checkbox day-checkbox-7" value="7" onchange="officeHoursEdit.acceptDay(event, \'' + dayPopupID + '\', \'' + dayFieldID + '\', true)" /><?=GetMessage("T_D_SUN")?></label>'
						+ '</div>'
						+ '<div class="buttons-container">'
							<?/*+ '<button onclick="officeHoursEdit.acceptDay(event, \'' + dayPopupID + '\', \'' + dayFieldID + '\')"><?=GetMessage("T_CHOOSE")?></button>'
							+ '<button onclick="officeHoursEdit.closePopup(event, \'' + dayPopupID + '\')"><?=GetMessage("T_CLOSE")?></button>'*/?>
						+ '</div>'
					+ '</div>'
				+ '</div>'
			+ '</td>'
			+ '<td>'
				+ '<div class="hour-div" id="' + divID + '">' + this.getHourDivInnerHtml(deptKey, scheduleKey, hourKey, divID, hourFieldID, hourPopupID) + '</div>'
				+ '<div class="add-button-container add-hour-button-container">'
					+ '<button title="<?=GetMessage("T_ADD_HOUR_BUTTON_TITLE")?>" onclick="officeHoursEdit.addHour(event, this)"><?=GetMessage("T_ADD_HOUR_BUTTON_CAPTION")?></button>'
				+ '</div>'
			+ '</td>'
			+ '<td class="del-schedule-item">'
				+ '<span class="button del-schedule-item-button" title="<?=GetMessage("T_TITLE_DEL_SCHEDULE_ITEM_BUTTON")?>" onclick="officeHoursEdit.deleteScheduleItem(this)"></span>'
			+ '</td>';

		return html;
	},

	addHour: function(event, addButton)
	{
		BX.PreventDefault(event);
		if (document.activeElement != addButton)
			return;

		var buttonDiv = BX.findParent(addButton, { 'class': 'add-hour-button-container' });
		if (!buttonDiv)
			return;
		var td = BX.findParent(buttonDiv, { 'tag': 'td' });
		if (!td)
			return;
		var tr = BX.findParent(td, { 'tag': 'tr' });
		if (!tr)
			return;
		var deptDiv = BX.findParent(tr, { 'class': 'dept-container' });
		if (!deptDiv)
			return;

		var deptKey = deptDiv.getAttribute('data-deptKey');
		var scheduleKey = tr.getAttribute('data-scheduleKey');
		var arHourDivs = BX.findChildren(td, { 'class': 'hour-div' }, true);
		var maxHourKey = -1;
		if (arHourDivs)
		{
			for (var i in arHourDivs)
			{
				arID = arHourDivs[i].id.split('_');
				var curHourKey = parseInt(arID[3]);
				if (curHourKey > maxHourKey)
					maxHourKey = curHourKey;
			}
		}

		var hourKey = maxHourKey + 1;
		var id = deptKey + '_' + scheduleKey + '_' + hourKey;
		var divID = 'hour-div_' + id;
		var hourFieldID = 'hour_' + id;
		var hourPopupID = 'set_hour_popup_' + id;
		var innerHtml = this.getHourDivInnerHtml(deptKey, scheduleKey, hourKey, divID, hourFieldID, hourPopupID);

		td.insertBefore(
			BX.create('DIV', { 'props': {'id': divID, 'className': 'hour-div'}, 'html': innerHtml }),
			buttonDiv
		);
	},

	addScheduleItem: function(event, addButton)
	{
		BX.PreventDefault(event);
		if (document.activeElement != addButton)
			return;

		var tr = BX.findParent(addButton, { 'tag': 'tr' });
		if (!tr)
			return;
		var tbody = BX.findParent(tr, { 'tag': 'tbody' });
		if (!tbody)
			return;
		var deptDiv = BX.findParent(tbody, { 'class': 'dept-container' });
		if (!deptDiv)
			return;

		var maxScheduleKey = this.getMaxAttr(tbody, { 'tag': 'tr', 'attribute': 'data-scheduleKey' }, 'data-scheduleKey');
		var deptKey = deptDiv.getAttribute('data-deptKey');
		var scheduleKey = maxScheduleKey + 1;
		var hourKey = 0;

		var id = deptKey + '_' + scheduleKey + '_' + hourKey;
		var divID = 'hour-div_' + id;
		var hourFieldID = 'hour_' + id;
		var hourPopupID = 'set_hour_popup_' + id;

		var id = deptKey + '_' + scheduleKey;
		var dayFieldID = 'day_' + id;
		var dayPopupID = 'set_day_popup_' + id;

		var innerHtml = this.getScheduleTableRowInnerHtml(deptKey, scheduleKey, hourKey, dayFieldID, dayPopupID, divID, hourFieldID, hourPopupID);

		tbody.insertBefore(
			BX.create('TR', { 'attrs': {'data-scheduleKey': '' + scheduleKey}, 'html': innerHtml }),
			tr
		);
	},

	addDept: function(event, addButton)
	{
		BX.PreventDefault(event);
		if (document.activeElement != addButton)
			return;

		var mainContainer = BX('office-hours-container');
		if (!mainContainer)
			return;
		var lastDeptDivider = BX.findChild(mainContainer, { 'class': 'last-dept-divider' }, false);
		if (!lastDeptDivider)
			return;

		var maxDeptKey = this.getMaxAttr(mainContainer, { 'tag': 'div', 'class': 'dept-container' }, 'data-deptKey');
		var deptKey = maxDeptKey + 1;
		var scheduleKey = 0;
		var hourKey = 0;

		var id = deptKey + '_' + scheduleKey + '_' + hourKey;
		var divID = 'hour-div_' + id;
		var hourFieldID = 'hour_' + id;
		var hourPopupID = 'set_hour_popup_' + id;

		var id = deptKey + '_' + scheduleKey;
		var dayFieldID = 'day_' + id;
		var dayPopupID = 'set_day_popup_' + id;

		var innerHtml =
			'<span class="button close-dept-button" title="<?=GetMessage("T_TITLE_DEL_DEPT_BUTTON")?>" onclick="officeHoursEdit.deleteDept(this)"></span>'
			+ '<div class="dept-name-container">'
				+ '<div class="caption">'
					+ '<?=GetMessage("T_DEPT_NAME")?>:'
				+ '</div>'
				+ '<div>'
					+ '<input type="text" name="OFFICE_HOURS[' + deptKey + '][NAME]" value="" />'
					+ '<br />'
					+ '<span class="small"><?=GetMessage("T_DEPT_NAME_HINT")?></span>'
				+ '</div>'
			+ '</div>'

			+ '<table class="schedule-table">'
				+ '<tr>'
					+ '<th><?=GetMessage("T_DAYS_OF_WEEK")?></th>'
					+ '<th><?=GetMessage("T_WORK_TIME")?></th>'
					+ '<th><?=GetMessage("T_DELETE")?></th>'
				+ '</tr>'

				+ '<tr data-scheduleKey="' + scheduleKey + '">'
					+ this.getScheduleTableRowInnerHtml(deptKey, scheduleKey, hourKey, dayFieldID, dayPopupID, divID, hourFieldID, hourPopupID)
				+ '</tr>'

				+ '<tr>'
					+ '<td colspan="3">'
						+ '<div class="add-button-container add-schedule-item-button-container">'
							+ '<button title="<?=GetMessage("T_ADD_SCHEDULE_ITEM_BUTTON_TITLE")?>" onclick="officeHoursEdit.addScheduleItem(event, this)"><?=GetMessage("T_ADD_SCHEDULE_ITEM_BUTTON_CAPTION")?></button>'
						+ '</div>'
					+ '</td>'
				+ '</tr>'
			+ '</table>';

		var deptDiv = BX.create('DIV', { 'props': {'className': 'dept-container'}, 'attrs': {'data-deptKey': '' + deptKey}, 'html': innerHtml });
		mainContainer.insertBefore(
			deptDiv,
			lastDeptDivider
		);
		mainContainer.insertBefore(
			BX.create('DIV', { 'props': {'className': 'dept-divider dept-divider-' + deptKey}}),
			lastDeptDivider
		);
	},

	getPopupErrorDiv: function(popup)
	{
		return BX.findChild(popup, { 'class': 'error' }, true);
	},

	setPopupError: function(popup, errorMsg)
	{
		var errorDiv = this.getPopupErrorDiv(popup);
		if (errorDiv)
		{
			errorDiv.innerHTML = errorMsg;
			BX.addClass(errorDiv, 'active');
		}
	},

	clearPopupError: function(popup)
	{
		var errorDiv = this.getPopupErrorDiv(popup);
		if (errorDiv)
		{
			errorDiv.innerHTML = '';
			BX.removeClass(errorDiv, 'active');
		}
	},

	isPopupHasError: function(popup)
	{
		var errorDiv = this.getPopupErrorDiv(popup);
		if (errorDiv)
			return (errorDiv.innerHTML != '');

		return false;
	},

	openDayPopup: function(event, popupID, fieldID)
	{
		this.closeAllPopups(event);

		var popup = BX(popupID);
		if (!BX.hasClass(popup, 'active'))
		{
			var arCheckboxes = BX.findChildren(popup, { 'class': 'day-checkbox' }, true);
			for (var i in arCheckboxes)
				arCheckboxes[i].checked = false;

			var dayStr = BX(fieldID).value;
			var arDayNums = this.strToDays(dayStr);
			for (var i in arDayNums)
			{
				var checkbox = BX.findChild(popup, { 'class': 'day-checkbox-' + arDayNums[i] }, true);
				if (checkbox)
					checkbox.checked = 'checked';
			}
		}
		//BX.toggleClass(popup, 'active');
		BX.addClass(popup, 'active');
	},

	openHourPopup: function(event, popupID, fieldID)
	{
		this.closeAllPopups(event);

		var popup = BX(popupID);
		if (!BX.hasClass(popup, 'active'))
		{
			this.clearPopupError(popup);
			var fieldValue = BX(fieldID).value;
			var arInputValues = ['09', '00', '18', '00'];
			var isHoliday = /<?=GetMessage("T_HOLIDAY_PATTERN")?>/i.test(fieldValue);
            var isAroundClock = /<?=GetMessage("T_AROUND_CLOCK_PATTERN")?>/i.test(fieldValue);
			if (!isHoliday && !isAroundClock)
			{
				var arRange = fieldValue.split('<?=GetMessage("CITRUS_MDASH")?>');
				if (arRange.length && arRange[0].length)
				{
					var arTime = arRange[0].split(':');
					if (arTime.length)
					{
						var value = parseInt(arTime[0]);
						if (!value)
							value = 0;
						if (value < 10)
							value = '0' + value;
						arInputValues[0] = value;
					}
					if (arTime.length >= 2)
					{
						var value = parseInt(arTime[1]);
						if (!value)
							value = '0';
						if (value < 10)
							value = '0' + value;
						arInputValues[1] = value;
					}
				}
				if (arRange.length >= 2)
				{
					var arTime = arRange[1].split(':');
					if (arTime.length)
					{
						var value = parseInt(arTime[0]);
						if (!value)
							value = 0;
						if (value < 10)
							value = '0' + value;
						arInputValues[2] = value;
					}
					if (arTime.length >= 2)
					{
						var value = parseInt(arTime[1]);
						if (!value)
							value = '0';
						if (value < 10)
							value = '0' + value;
						arInputValues[3] = value;
					}
				}
			}

			var holidayCheckbox = BX.findChild(popup, { 'tag': 'input', 'class': 'hour-holiday' }, true);
			if (holidayCheckbox)
				holidayCheckbox.checked = isHoliday;

            var aroundClockCheckbox = BX.findChild(popup, { 'class': 'hour-around-clock' }, true);
            if (aroundClockCheckbox)
                aroundClockCheckbox.checked = isAroundClock;

			var arInputs = BX.findChildren(popup, { 'class': 'popup-hour-input' }, true);
			for (var i in arInputs)
			{
				BX.removeClass(arInputs[i], 'error');
				arInputs[i].disabled = isHoliday || isAroundClock;
				arInputs[i].value = arInputValues[i];
			}
		}

		//BX.toggleClass(popup, 'active');
		BX.addClass(popup, 'active');
		/*if (BX.hasClass(popup, 'active'))
		{
			var hour1 = BX.findChild(
				popup,
				{ 'class': 'popup-hour-input', 'class': 'hour1' },
				true
			);
			if (hour1)
			{
				hour1.focus();
				hour1.select();
			}
		}*/
	},

	acceptDay: function(event, popupID, fieldID, preventClose)
	{
		var preventClose = typeof preventClose !== 'undefined' ? preventClose : false;
		if (!preventClose)
			BX.PreventDefault(event);

		var popup = BX(popupID);
		var dayCheckboxes = BX.findChildren(
			popup,
			{ 'class': 'day-checkbox' },
			true
		);
		var arCheckedDays = [];
		if (dayCheckboxes)
		{
			for (var i in dayCheckboxes)
			{
				if (dayCheckboxes[i].checked)
					arCheckedDays.push(dayCheckboxes[i].value);
			}
		}
		BX(fieldID).value = this.daysToStr(arCheckedDays);

		if (!preventClose)
			this.closePopup(event, popupID);
	},

	acceptHour: function(event, popupID, fieldID, preventClose)
	{
		var preventClose = typeof preventClose !== 'undefined' ? preventClose : false;
		if (!preventClose)
			BX.PreventDefault(event);

		var popup = BX(popupID);

		var arErrorMsgs = {
			'invalidInput':         '<?=GetMessage("T_ERROR_INVALID_HOUR_INPUT")?>',
			'invalidHour1':         '<?=GetMessage("T_ERROR_INVALID_HOUR1_INPUT")?>',
			'invalidMinute1':       '<?=GetMessage("T_ERROR_INVALID_MINUTE1_INPUT")?>',
			'invalidHour2':         '<?=GetMessage("T_ERROR_INVALID_HOUR2_INPUT")?>',
			'invalidMinute2':       '<?=GetMessage("T_ERROR_INVALID_MINUTE2_INPUT")?>',
			'invalidMinute2_24h':   '<?=GetMessage("T_ERROR_INVALID_MINUTE2_24H_INPUT")?>',
			'invalidRange':         '<?=GetMessage("T_ERROR_INVALID_HOUR_RANGE")?>'
		};
		var errorKey = false;
		var arErrorInputs = [];

		var isHoliday = false, isAroundClock = false;
		var fieldValue = '';

		var holidayCheckbox = BX.findChild(popup, { 'class': 'hour-holiday' }, true);
		if (holidayCheckbox)
			isHoliday = holidayCheckbox.checked;

        var aroundClockCheckbox = BX.findChild(popup, { 'class': 'hour-around-clock' }, true);
        if (aroundClockCheckbox)
            isAroundClock = aroundClockCheckbox.checked;

        aroundClockCheckbox.disabled = isHoliday;
        holidayCheckbox.disabled = isAroundClock;

		var arInputs = BX.findChildren(popup, { 'class': 'popup-hour-input' }, true);
		for (var i in arInputs)
		{
			arInputs[i].disabled = isHoliday;
			if (!isHoliday && !arInputs[i].value.length)
				arInputs[i].value = '00';
		}

		if (!isHoliday) {
		    var arInputs = BX.findChildren(popup, { 'class': 'popup-hour-input' }, true);
		    for (var i in arInputs)
		    {
		        arInputs[i].disabled = isAroundClock;
		        if (!isAroundClock && !arInputs[i].value.length)
		            arInputs[i].value = '00';
		    }
		}

		if (isHoliday)
		{
			fieldValue = '<?=GetMessage("T_HOLIDAY")?>';
		}
		else
		    if (isAroundClock)
		    {
		        fieldValue = '<?=GetMessage("T_AROUND_CLOCK")?>';
		    }
		else
		{
			var arInputValues = [];
			if (arInputs)
			{
				for (var i in arInputs)
				{
					var inputValue = BX.util.trim(arInputs[i].value);
					if (inputValue.length)
					{
						if (!/^\d+$/.test(inputValue))
						{
							arErrorInputs.push(arInputs[i]);
							errorKey = 'invalidInput';
						}
					}

					if (!errorKey)
					{
						inputValue = parseInt(inputValue);
						if (!inputValue)
							inputValue = 0;
						if (inputValue < 10 && BX.util.in_array(i, ['1', '3']))
							inputValue = '0' + inputValue;
						arInputValues.push(inputValue);
					}
				}

				if (!errorKey)
				{
					var hour1 = parseInt(arInputValues[0]);
					var minute1 = parseInt(arInputValues[1]);
					var hour2 = parseInt(arInputValues[2]);
					var minute2 = parseInt(arInputValues[3]);

					if (hour1 < 0 || hour1 > 23)
					{
						errorKey = 'invalidHour1';
						arErrorInputs = [arInputs[0]];
					}
					else if (minute1 < 0 || minute1 > 59)
					{
						errorKey = 'invalidMinute1';
						arErrorInputs = [arInputs[1]];
					}
					else if (hour2 < 0 || hour2 > 24)
					{
						errorKey = 'invalidHour2';
						arErrorInputs = [arInputs[2]];
					}
					else if (minute2 < 0 || minute2 > 59)
					{
						errorKey = 'invalidMinute2';
						arErrorInputs = [arInputs[3]];
					}
					else if (hour2 == 24 && minute2 > 0)
					{
						errorKey = 'invalidMinute2_24h';
						arErrorInputs = [arInputs[3]];
					}
				}

				if (!errorKey)
				{
					var time1 = parseInt(arInputValues[0])*100 + parseInt(arInputValues[1]);
					var time2 = parseInt(arInputValues[2])*100 + parseInt(arInputValues[3]);
					if (time1 >= time2 && (time1 != 0 || time2 != 0))
					{
						errorKey = 'invalidRange';
						arErrorInputs = arInputs;
					}
				}
			}

			if (parseInt(arInputValues[0]) == 0 && parseInt(arInputValues[1]) == 0)
			{
				arInputValues[0] = '00';
				arInputValues[1] = '00';
			}

			for (var i in arInputValues)
			{
				var delimiter = '';
				switch (i)
				{
					case '1':
					case '3':
						delimiter = ':';
						break;

					case '2':
						delimiter = '<?=GetMessage("CITRUS_MDASH")?>';
						break;
				}
				fieldValue += delimiter + arInputValues[i];
			}
		}

		for (var i in arInputs)
		{
			BX.removeClass(arInputs[i], 'error');
		}

		if (arErrorInputs.length)
		{
			/*if (errorKey == 'invalidRange')
			{
				this.clearPopupError(popup);
			}
			else*/
			{
				this.setPopupError(popup, arErrorMsgs[errorKey]);
				var errorInputFocusedFlag = false;
				for (var i in arErrorInputs)
				{
					BX.addClass(arErrorInputs[i], 'error');
					if (!errorInputFocusedFlag)
					{
						arErrorInputs[i].select();
						errorInputFocusedFlag = true;
					}
				}
			}
		}
		else
		{
			this.clearPopupError(popup);
			if (time1 != 0 || time2 != 0)
				BX(fieldID).value = fieldValue;
			if (!preventClose)
				this.closePopup(event, popupID);
		}
	},

	onDayInputKeyUp: function(event, popupID, fieldID)
	{
		var keyCode = this.getKeyCode(event);
		if (keyCode == 27)
		{
			this.closeAllPopups(event);
			return;
		}

		this.openDayPopup(event, popupID, fieldID);
	},

	onHourInputKeyUp: function(event, popupID, fieldID)
	{
		var keyCode = this.getKeyCode(event);
		if (keyCode == 27)
		{
			this.closeAllPopups(event);
			return;
		}

		this.openHourPopup(event, popupID, fieldID);
	},

	closePopup: function(event, popupID)
	{
//		BX.PreventDefault(event);

		var popup = BX(popupID);
		if (BX.hasClass(popup, 'set-hour-popup'))
		{
			var input = BX.findChild(popup, { 'tag': 'input', 'class': 'popup-hour-input' }, true);
			if (input)
				BX.fireEvent(input, 'change');
		}

		BX.removeClass(popup, 'active');
	},

	closeAllPopups: function(event)
	{
		var arPopupClasses = ['set-day-popup', 'set-hour-popup'];
		for (var i in arPopupClasses)
		{
			var arPopups = BX.findChildren(BX('office-hours-container'), { 'class': arPopupClasses[i]/*, 'class': 'active'*/ }, true);
			if (arPopups)
			{
				for (var j in arPopups)
				{
					if (!this.isPopupHasError(arPopups[j]))
						this.closePopup(event, arPopups[j]);
				}
			}
		}
	},

	onKeyUp: function(event)
	{
		BX.PreventDefault(event);
		var keyCode = this.getKeyCode(event);
		if (keyCode == 27)
		{
			this.closeAllPopups(event);
			return;
		}

		if (keyCode == 13 || keyCode == 10)
		{
			var parentPopup = this.isInside(event.target, 'div', 'popup');
			if (parentPopup)
				this.closePopup(event, parentPopup);
		}
	},

	onClick: function(event)
	{
		var parentPopup = this.isInside(event.target, 'div', 'popup');
		var parentButton = this.isInside(event.target, 'span', 'button');
		var parentInput = this.isInside(event.target, 'input', 'popup-input');

		if (!parentPopup && !parentButton && !parentInput)
			this.closeAllPopups(event);
	},

	readyInterval: false,

	ready: function()
	{
		if (!window.BX)
			return;

		var _this = this;

		BX.ready(function () {
			BX.bind(document, 'keyup', function(e) { _this.onKeyUp(e); });
			BX.bind(document, 'click', function(e) { _this.onClick(e); });
		});

		if (this.readyInterval)
		{
			clearInterval(this.readyInterval);
			this.readyInterval = false;
		}
	},

	addNode: function(tagName, oParams)
	{
		var node = document.createElement(tagName);
		for (var i in oParams)
			node[i] = oParams[i];

		if (tagName == 'link')
		{
			var head = document.getElementsByTagName('head')[0]; 
			head.appendChild(node);
		}
		else
		{
			var script = document.getElementsByTagName('script')[0]; 
			script.parentNode.insertBefore(node, script);
		}
	},

	init: function()
	{
		if (window.BX)
			this.ready();
		else
		{
			this.addNode('script', {'type': 'text/javascript'/*, 'async': true*//*, 'defer': true*/, 'src': '/bitrix/js/main/core/core.js'});
			this.addNode('link', {'type': 'text/css', 'rel': 'stylesheet', 'href': '/bitrix/js/main/core/css/core.css'});

			this.readyInterval = setInterval('officeHoursEdit.ready();', 50);
		}
	}
};

officeHoursEdit.init();
</script>