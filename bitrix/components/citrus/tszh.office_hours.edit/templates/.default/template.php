<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!function_exists("tszhOfficeHoursShowDept")):
	function tszhOfficeHoursShowDept($deptKey, $arDept)
	{?>
		<div class="dept-container" data-deptKey="<?=$deptKey?>">
			<span class="button close-dept-button" title="<?=GetMessage("T_TITLE_DEL_DEPT_BUTTON")?>" onclick="officeHoursEdit.deleteDept(this)"></span>
			<div class="dept-name-container">
				<div class="caption">
					<?=GetMessage("T_DEPT_NAME")?>:
				</div>
				<div>
					<input type="text" name="OFFICE_HOURS[<?=$deptKey?>][NAME]" value="<?=$arDept["NAME"]?>" />
					<br />
					<span class="small"><?=GetMessage("T_DEPT_NAME_HINT")?></span>
				</div>
			</div>

			<table class="schedule-table">
				<tr>
					<th><?=GetMessage("T_DAYS_OF_WEEK")?></th>
					<th><?=GetMessage("T_WORK_TIME")?></th>
					<th><?=GetMessage("T_DELETE")?></th>
				</tr>

				<?foreach ($arDept["SCHEDULE"] as $scheduleKey => $arScheduleItem):?>
					<tr data-scheduleKey="<?=$scheduleKey?>">
						<td>
							<div class="day-container">
								<?$id = "{$deptKey}_{$scheduleKey}";
								$dayFieldID = "day_{$id}";
								$dayPopupID = "set_day_popup_{$id}";?>
								<input id="<?=$dayFieldID?>" type="text" class="popup-input day-input" name="OFFICE_HOURS[<?=$deptKey?>][SCHEDULE][<?=$scheduleKey?>][DAY]" value="<?=$arScheduleItem["DAY"]?>" autocomplete="off" onfocus="officeHoursEdit.openDayPopup(event, '<?=$dayPopupID?>', '<?=$dayFieldID?>')" onkeyup="officeHoursEdit.onDayInputKeyUp(event, '<?=$dayPopupID?>', '<?=$dayFieldID?>')" />
								<span class="button set-day-button" title="<?=GetMessage("T_TITLE_SET_DAY_BUTTON")?>" onclick="officeHoursEdit.openDayPopup(event, '<?=$dayPopupID?>', '<?=$dayFieldID?>')"></span>
								<div class="popup set-day-popup" id="<?=$dayPopupID?>">
									<div class="content-container">
										<label tabindex="1"><input type="checkbox" class="day-checkbox day-checkbox-1" value="1" onchange="officeHoursEdit.acceptDay(event, '<?=$dayPopupID?>', '<?=$dayFieldID?>', true)" /><?=GetMessage("T_D_MON")?></label>
										<label tabindex="2"><input type="checkbox" class="day-checkbox day-checkbox-2" value="2" onchange="officeHoursEdit.acceptDay(event, '<?=$dayPopupID?>', '<?=$dayFieldID?>', true)" /><?=GetMessage("T_D_TUE")?></label>
										<label tabindex="3"><input type="checkbox" class="day-checkbox day-checkbox-3" value="3" onchange="officeHoursEdit.acceptDay(event, '<?=$dayPopupID?>', '<?=$dayFieldID?>', true)" /><?=GetMessage("T_D_WED")?></label>
										<label tabindex="4"><input type="checkbox" class="day-checkbox day-checkbox-4" value="4" onchange="officeHoursEdit.acceptDay(event, '<?=$dayPopupID?>', '<?=$dayFieldID?>', true)" /><?=GetMessage("T_D_THU")?></label>
										<label tabindex="5"><input type="checkbox" class="day-checkbox day-checkbox-5" value="5" onchange="officeHoursEdit.acceptDay(event, '<?=$dayPopupID?>', '<?=$dayFieldID?>', true)" /><?=GetMessage("T_D_FRI")?></label>
										<label tabindex="6"><input type="checkbox" class="day-checkbox day-checkbox-6" value="6" onchange="officeHoursEdit.acceptDay(event, '<?=$dayPopupID?>', '<?=$dayFieldID?>', true)" /><?=GetMessage("T_D_SAT")?></label>
										<label tabindex="7"><input type="checkbox" class="day-checkbox day-checkbox-7" value="7" onchange="officeHoursEdit.acceptDay(event, '<?=$dayPopupID?>', '<?=$dayFieldID?>', true)" /><?=GetMessage("T_D_SUN")?></label>
									</div>
									<div class="buttons-container">
										<?/*<button onclick="officeHoursEdit.acceptDay(event, '<?=$dayPopupID?>', '<?=$dayFieldID?>')"><?=GetMessage("T_CHOOSE")?></button>
										<button onclick="officeHoursEdit.closePopup(event, '<?=$dayPopupID?>')"><?=GetMessage("T_CLOSE")?></button>*/?>
									</div>
								</div>
							</div>
						</td>
						<td>
							<?foreach ($arScheduleItem["HOURS"] as $hourKey => $hour):
								$id = "{$deptKey}_{$scheduleKey}_{$hourKey}";
								$divID = "hour-div_{$id}";
								$hourFieldID = "hour_{$id}";
								$hourPopupID = "set_hour_popup_{$id}";?>
								<div class="hour-div" id="<?=$divID?>">
									<input id="<?=$hourFieldID?>" type="text" class="popup-input hour-input" name="OFFICE_HOURS[<?=$deptKey?>][SCHEDULE][<?=$scheduleKey?>][HOURS][<?=$hourKey?>]" value="<?=$hour?>" autocomplete="off" onfocus="officeHoursEdit.openHourPopup(event, '<?=$hourPopupID?>', '<?=$hourFieldID?>')" onkeyup="officeHoursEdit.onHourInputKeyUp(event, '<?=$hourPopupID?>', '<?=$hourFieldID?>')" />
									<span class="button set-hour-button" title="<?=GetMessage("T_TITLE_SET_HOUR_BUTTON")?>" onclick="officeHoursEdit.openHourPopup(event, '<?=$hourPopupID?>', '<?=$hourFieldID?>')"></span>
									<div class="popup set-hour-popup" id="<?=$hourPopupID?>">
										<div class="content-container">
											<div class="error"></div>
											<input type="text" class="popup-hour-input hour1" min="0" max="23" title="<?=GetMessage("T_TITLE_HOURS")?>" onchange="officeHoursEdit.acceptHour(event, '<?=$hourPopupID?>', '<?=$hourFieldID?>', true)" value="" />
											:
											<input type="text" class="popup-hour-input minute1" min="0" max="59" title="<?=GetMessage("T_TITLE_MINUTES")?>" onchange="officeHoursEdit.acceptHour(event, '<?=$hourPopupID?>', '<?=$hourFieldID?>', true)" value="" />
											&ndash;
											<input type="text" class="popup-hour-input hour2" min="0" max="24" title="<?=GetMessage("T_TITLE_HOURS")?>" onchange="officeHoursEdit.acceptHour(event, '<?=$hourPopupID?>', '<?=$hourFieldID?>', true)" value="" />
											:
											<input type="text" class="popup-hour-input minute2" min="0" max="59" title="<?=GetMessage("T_TITLE_MINUTES")?>" onchange="officeHoursEdit.acceptHour(event, '<?=$hourPopupID?>', '<?=$hourFieldID?>', true)" value="" />
											<br />
											<?/*<span class="small"><?=GetMessage("T_HOUSE_RANGE_HINT")?></span>*/?>
											<div class="holiday-checkbox-container">
												<label><input type="checkbox" class="hour-holiday" value="Y" onchange="officeHoursEdit.acceptHour(event, '<?=$hourPopupID?>', '<?=$hourFieldID?>', true)" /><?=GetMessage("T_HOLIDAY")?></label>
											</div>
											<div class="around-clock-checkbox-container">
												<label><input type="checkbox" class="hour-around-clock" value="Y" onchange="officeHoursEdit.acceptHour(event, '<?=$hourPopupID?>', '<?=$hourFieldID?>', true)" /><?=GetMessage("T_AROUND_CLOCK")?></label>
											</div>
										</div>
										<div class="buttons-container">
											<?/*<button onclick="officeHoursEdit.acceptHour(event, '<?=$hourPopupID?>', '<?=$hourFieldID?>')"><?=GetMessage("T_CHOOSE")?></button>
											<button onclick="officeHoursEdit.closePopup(event, '<?=$hourPopupID?>')"><?=GetMessage("T_CLOSE")?></button>*/?>
										</div>
									</div>
									<span class="button del-hour-button" title="<?=GetMessage("T_TITLE_DEL_HOUR_BUTTON")?>" onclick="officeHoursEdit.deleteHour('<?=$divID?>')"></span>
								</div>
							<?endforeach?>
							<div class="add-button-container add-hour-button-container">
								<button title="<?=GetMessage("T_ADD_HOUR_BUTTON_TITLE")?>" onclick="officeHoursEdit.addHour(event, this)"><?=GetMessage("T_ADD_HOUR_BUTTON_CAPTION")?></button>
							</div>
						</td>
						<td class="del-schedule-item">
							<span class="button del-schedule-item-button" title="<?=GetMessage("T_TITLE_DEL_SCHEDULE_ITEM_BUTTON")?>" onclick="officeHoursEdit.deleteScheduleItem(this)"></span>
						</td>
					</tr>
				<?endforeach?>

				<tr>
					<td colspan="3">
						<div class="add-button-container add-schedule-item-button-container">
							<button title="<?=GetMessage("T_ADD_SCHEDULE_ITEM_BUTTON_TITLE")?>" onclick="officeHoursEdit.addScheduleItem(event, this)"><?=GetMessage("T_ADD_SCHEDULE_ITEM_BUTTON_CAPTION")?></button>
						</div>
					</td>
				</tr>
			</table>
		</div>

		<div class="dept-divider dept-divider-<?=$deptKey?>"></div>
	<?}
endif;

/*if ($arParams["FROM_WIZARD"] == "Y")
{
	echo '<link href="'.CUtil::GetAdditionalFileURL('/bitrix/js/main/core/css/core.css').'" type="text/css" rel="stylesheet" />'."\r\n";
	echo '<script type="text/javascript" src="'.CUtil::GetAdditionalFileURL('/bitrix/js/main/core/core.js').'"></script>'."\r\n";
}*/
?>
<link href="<?=$templateFolder?>/styles.css" type="text/css" rel="stylesheet">
<div id="office-hours-container">
	<?foreach ($arResult["ITEMS"] as $deptKey => $arDept):
		tszhOfficeHoursShowDept($deptKey, $arDept);
	endforeach;

	if (empty($arResult["ITEMS"]))
		tszhOfficeHoursShowDept(0, array("NAME" => "", "SCHEDULE" => array(array("DAY" => "", "HOURS" => array("")))));?>

	<div class="dept-divider last-dept-divider"></div>
	<div class="add-button-container add-dept-button-container">
		<button title="<?=GetMessage("T_ADD_DEPT_BUTTON_TITLE")?>" onclick="officeHoursEdit.addDept(event, this)"><?=GetMessage("T_ADD_DEPT_BUTTON_CAPTION")?></button>
	</div>
</div>

<?require_once($_SERVER["DOCUMENT_ROOT"] . "{$templateFolder}/script.php");?>