<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule("vdgb.tszhvote"))
{
	ShowError(GetMessage("TVL_VOTE_MODULE_NOT_INSTALLED"));
	return;
}

$arParams["GROUP_ID"] = IntVal($arParams['GROUP_ID']) > 0 ? IntVal($arParams['GROUP_ID']) : 0;

$strBackURLParam = "&backurl=" . rawurlencode($APPLICATION->GetCurPageParam());

$arParams["LIST_URL"] = trim($arParams["LIST_URL"]);
if (strlen($arParams["LIST_URL"]) <= 0)
	$arParams["LIST_URL"] = "index.php";

$arParams["QUESTIONS_URL"] = trim($arParams["QUESTIONS_URL"]);
if (strlen($arParams["QUESTIONS_URL"]) <= 0)
	$arParams["QUESTIONS_URL"] = "questions.php?ID=#ID#";
$arParams["QUESTIONS_URL"] .= $strBackURLParam;

$arParams["EDIT_URL"] = trim($arParams["EDIT_URL"]);
if (strlen($arParams["EDIT_URL"]) <= 0)
	$arParams["EDIT_URL"] = "form.php?ID=#ID#";
$arParams["EDIT_URL"] .= $strBackURLParam;

$arParams["RESULTS_URL"] = trim($arParams["RESULTS_URL"]);
if (strlen($arParams["RESULTS_URL"]) <= 0)
	$arParams["RESULTS_URL"] = "results.php?VOTING_ID=#VOTING_ID#";
$arParams["RESULTS_URL"] .= $strBackURLParam;

$arResult["GROUP"] = CCitrusPollGroup::GetByID($arParams['GROUP_ID']);
if (!is_array($arResult["GROUP"]))
{
	ShowError(GetMessage("CVL_VOTING_GROUP_NOT_FOUND"));
	return;
}

$arResult["GRID_ID"] = 'citrus_vote_edit_results';

$arResult["COLUMNS"] = Array(
	array("id"=>"ID", "default"=>true),
	array("id"=>"VALID", "default"=>true, "type" => "checkbox"),
	array("id"=>"ENTITY_ID", "default"=>true, "type" => "int"),
	array("id"=>"WEIGHT", "default"=>true, "type" => "float"),
	array("id"=>"IP", "default"=>false, "type" => "string"),
	array("id"=>"TIMESTAMP_X", "default"=>true, "type" => "date"),
	array("id"=>"VOTING_ID", "default"=>true),
	array("id"=>"QUESTION_ID", "default"=>true),
);
$arResult["FILTER"] = Array();
foreach ($arResult["COLUMNS"] as $key => $arColumn)
{
	$arResult["COLUMNS"][$key]['name'] = $arColumn['name'] = GetMessage("CVL_F_" . $arColumn["id"]);
	if (!array_key_exists('sort', $arColumn))
		$arResult["COLUMNS"][$key]['sort'] = $arResult["COLUMNS"][$key]['id'];
		
	if ($arColumn['type'] == 'checkbox')
	{
		$arResult["FILTER"][] = 
			array("id"=>$arColumn['id'], "name"=>$arColumn['name'], "type"=>'list', "items"=>Array('' => GetMessage("CVL_ALL"), "Y" => GetMessage("CVL_YES"), "N" => GetMessage("CVL_NO")));
	}
	else
	{
		$arResult["FILTER"][] = 
			array("id"=>$arColumn['id'], "name"=>$arColumn['name'], "type"=>$arColumn['type'] == 'int' ? 'number' : $arColumn['type'], "items"=>array_key_exists('items', $arColumn) ? $arColumn['items'] : false);
	}
}

$arResult["SORT_COLUMNS"] = Array();
foreach ($arResult["COLUMNS"] as $arColumn) {
	if (strlen($arColumn['sort']) > 0) {
		$arResult["SORT_COLUMNS"][$arColumn['sort']] = $arColumn['sort'];
	}
}

//�������������� ������ � ����������� ������������ ��� ������ �����
$grid_options = new CGridOptions($arResult["GRID_ID"]);

//����� ���������� �������� ������������ (�������� ��, ��� �� ���������)
$aSort = $grid_options->GetSorting(array("sort"=>array("ID"=>"DESC")));
$arResult["SORT"] = $aSort["sort"];
$arResult["SORT_VARS"] = $aSort["vars"];

//������ �������� � ����������� (�������� ���������)
$aNav = $grid_options->GetNavParams(array("nPageSize"=>20));


// ------------------------------------------------
// BEGIN FILTER
//������� ������� ������ (�������� ��������� ���� ������)
$arFilter = Array();
$aFilter = $grid_options->GetFilter($arResult["FILTER"]);
if(isset($aFilter["TIMESTAMP_X_from"]))
{
	$aFilter[">=TIMESTAMP_X"] = $aFilter["TIMESTAMP_X_from"];
	unset($aFilter["TIMESTAMP_X_from"]);
}
if(isset($aFilter["TIMESTAMP_X_to"]))
{
	$aFilter["<=TIMESTAMP_X"] = $aFilter["TIMESTAMP_X_to"];
	unset($aFilter["TIMESTAMP_X_to"]);
}
unset($aFilter["TIMESTAMP_X_datesel"]);
$arFilter = array_merge($arFilter, $aFilter);
// END FILTER
// ------------------------------------------------

$rsData = CCitrusPollVote::GetList($arResult['SORT'], $arFilter, false, $aNav);
$arResult["DATA"] = Array();
while ($arData = $rsData->GetNext(false))
{
	$strEditUrl = CComponentEngine::MakePathFromTemplate($arParams["EDIT_URL"], Array("ID" => $arData['VOTING_ID']));
	$strResultsUrl = CComponentEngine::MakePathFromTemplate($arParams["RESULTS_URL"], Array("VOTING_ID" => $arData['VOTING_ID']));
	$strQuestionsUrl = CComponentEngine::MakePathFromTemplate($arParams["QUESTIONS_URL"], Array("ID" => $arData['ID']));
	
	// TODO optimize
	$obEntity = CCitrusPollEntityBase::GetObj($arData['ENTITY_TYPE']);
	$arDataView = Array(
		"ENTITY_ID" => '[' . $arData['ENTITY_ID'] . '] ' . $obEntity->GetEntityName($arData['ENTITY_ID']),
		"VOTING_ID" => '[<a href="' . $strEditUrl . '">' . $arData['VOTING_ID'] . '</a>] ' . $arData['VOTING_NAME'],
		"QUESTION_ID" => '[' . $arData['QUESTION_ID'] . '] ' . $arData['QUESTION_TEXT'],
	);
	
	$strDeleteUrl = '?' . bitrix_sessid_get() . "&backurl=" . rawurlencode($APPLICATION->GetCurPageParam('', Array('backurl'))) . '&action_button_' . $arResult["GRID_ID"] . '=delete' . '&ID=' . $arData["ID"];
	
	$arResult["DATA"][] = Array(
		'data' => $arData,
		'columns' => $arDataView,
		'actions' => Array(
			Array(
				'TEXT' => GetMessage('�VL_VIEW'),
				'DEFAULT' => true,
				'DISABLED' => false,
				"ONCLICK" => "jsUtils.Redirect({}, '$strResultDetailUrl'); return false;",
				'ICONCLASS' => 'view',
			),
			Array(
				'TEXT' => GetMessage('�VL_DELETE'),
				'DEFAULT' => false,
				'DISABLED' => false,
				"ONCLICK" => "if (confirm('" . GetMessage('�VL_CONFIRM_DELETE') . "')) jsUtils.Redirect({}, '$strDeleteUrl'); return false;",
				'ICONCLASS' => 'delete',
			),
		),
	);
}

if ($arFilter['VOTING_ID'])
	$GLOBALS["APPLICATION"]->SetTitle(GetMessage("TVL_TITLE_NUM", Array('#ID#' => $arFilter['VOTING_ID'])));
else
	$GLOBALS["APPLICATION"]->SetTitle(GetMessage("TVL_TITLE"));

$arResult["TOOLBAR_BUTTONS"] = $arResult['GRID_ID'] . '_toolbar';
$arResult["TOOLBAR_BUTTONS"] = Array(
	array("TEXT" => GetMessage("TVL_VOTINGS_LIST"), "ICON" => 'btn-new', "LINK" => $arParams['LIST_URL']),
	array("TEXT" => GetMessage("TVL_TB_SETTINGS"), "ICON" => 'btn-settings', "TITLE" => GetMessage("TVL_TB_SETTINGS_TITLE"), "LINK" => 'javascript:void(0);', "LINK_PARAM" => 'onclick="bxGrid_' . $arResult["GRID_ID"] . '.EditCurrentView()"'),
	array("TEXT" => GetMessage("TVL_TB_VIEWS"), "ICON" => 'btn-list', "TITLE" => GetMessage("TVL_TB_VIEWS_TITLE"), "LINK" => 'javascript:void(0);', "LINK_PARAM" => 'onclick="bxGrid_' . $arResult["GRID_ID"] . '.ShowViews()"'),
);

$arResult["NAV_OBJECT"] = $rsData;
$arResult["TOTAL_CNT"] = $rsData->SelectedRowsCount();


if (array_key_exists('citrus.tszh.voting.edit.message', $_SESSION))
{
	$arResult["MESSAGE"] = Array(
		"TYPE" => "OK",
		"MESSAGE" => $_SESSION['citrus.tszh.voting.edit.message'],
	);
	unset($_SESSION['citrus.tszh.voting.edit.message']);
}

$this->IncludeComponentTemplate();

?>