<?
$MESS ['SUP_LIST_TICKETS_PER_PAGE'] = "Количество обращений на одной странице";
$MESS ['SUP_SET_PAGE_TITLE'] = "Устанавливать заголовок страницы";
$MESS ['SUP_EDIT_MESSAGES_PER_PAGE'] = "Количество сообщений на одной странице";
$MESS ['SUP_DESC_YES'] = "Да";
$MESS ['SUP_DESC_NO'] = "Нет";

$MESS ['SUP_TICKET_ID_DESC'] = "Идентификатор обращения";
$MESS ['SUP_TICKET_LIST_DESC'] = "Список обращений";
$MESS ['SUP_TICKET_EDIT_DESC'] = "Редактирование обращения";
$MESS ['SUP_SHOW_COUPON_FIELD'] = "Показывать поле ввода купона";
$MESS ['SUP_EDIT_FIELDS'] = "Поля, выводимые на редактирование";
$MESS ['SUP_SHOW_FIELDS'] = "Показывать поля";

$MESS["CTT_F_VIEWERS"] = "Кем просматривалось";
$MESS["CTT_F_OWNER"] = "Автор обращения";
$MESS["CTT_F_SOURCE"] = "Источник";
$MESS["CTT_F_DATE_CREATE"] = "Дата создания";
$MESS["CTT_F_CREATED"] = "Кем создано";
$MESS["CTT_F_TIMESTAMP_X"] = "Дата изменения";
$MESS["CTT_F_DATE_CLOSE"] = "Дата закрытия";
$MESS["CTT_F_STATUS"] = "Статус";
$MESS["CTT_F_CATEGORY"] = "Тип обращения";
$MESS["CTT_F_TIME_TO_SOLVE"] = "Котрольная дата выполнения";
$MESS["CTT_F_CRITICALITY"] = "Критичность";
$MESS["CTT_F_RESPONSIBLE"] = "Ответственный";
$MESS["CTT_F_MESSAGE"] = "Сообщение";
$MESS["CTT_F_CLOSE_TICKET"] = "Закрыть заявку";

?>