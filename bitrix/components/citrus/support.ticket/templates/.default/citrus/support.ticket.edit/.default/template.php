<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//$APPLICATION->AddHeadScript($this->GetFolder() . '/script.js');

//echo '<pre>' . htmlspecialchars(print_r($arParams, true)) . '</pre>';

if (strlen($arResult['ERROR_MESSAGE']) > 0) {
	echo ShowError($arResult["ERROR_MESSAGE"]);
}

if (strlen($arResult["NOTE"]) > 0) {
	ShowNote($arResult["NOTE"]);
}

if (!empty($arResult["TICKET"])):

	if (!empty($arResult["ONLINE"])):?>
<p>
	<?$time = intval($arResult["OPTIONS"]["ONLINE_INTERVAL"]/60)." ".GetMessage("SUP_MIN");?>
	<?=str_replace("#TIME#",$time,GetMessage("SUP_USERS_ONLINE"));?>:<br />
	<?foreach($arResult["ONLINE"] as $arOnlineUser):?>
	<small>(<?=$arOnlineUser["USER_LOGIN"]?>) <?=$arOnlineUser["USER_NAME"]?> [<?=$arOnlineUser["TIMESTAMP_X"]?>]</small><br />
	<?endforeach?>
</p>
<?
	endif
?>


<h3 style="margin: 15px 0;"><?=$arResult["TICKET"]["TITLE"]?></h3>

<table class="support-ticket-edit data-table">

	<tr>
		<th><?=GetMessage("SUP_TICKET")?></th>
	</tr>

	<tr>
		<td>

		<?=GetMessage("SUP_SOURCE")." / ".GetMessage("SUP_FROM")?>:

			<?if (strlen($arResult["TICKET"]["SOURCE_NAME"])>0):?>
				[<?=$arResult["TICKET"]["SOURCE_NAME"]?>]
			<?else:?>
				[web]
			<?endif?>

			<?if (strlen($arResult["TICKET"]["OWNER_SID"])>0):?>
				<?=$arResult["TICKET"]["OWNER_SID"]?>
			<?endif?>

			<?if (intval($arResult["TICKET"]["OWNER_USER_ID"])>0):?>
				[<?=$arResult["TICKET"]["OWNER_USER_ID"]?>]
				(<?=$arResult["TICKET"]["OWNER_LOGIN"]?>)
				<?=$arResult["TICKET"]["OWNER_NAME"]?>
			<?endif?>
		<br />


		<?=GetMessage("SUP_CREATE")?>: <?=$arResult["TICKET"]["DATE_CREATE"]?>

		<?if (strlen($arResult["TICKET"]["CREATED_MODULE_NAME"])<=0 || $arResult["TICKET"]["CREATED_MODULE_NAME"]=="tszhtickets"):?>
			[<?=$arResult["TICKET"]["CREATED_USER_ID"]?>]
			(<?=$arResult["TICKET"]["CREATED_LOGIN"]?>)
			<?=$arResult["TICKET"]["CREATED_NAME"]?>
		<?else:?>
			<?=$arResult["TICKET"]["CREATED_MODULE_NAME"]?>
		<?endif?>
		<br />


		<?if ($arResult["TICKET"]["DATE_CREATE"]!=$arResult["TICKET"]["TIMESTAMP_X"]):?>
				<?=GetMessage("SUP_TIMESTAMP")?>: <?=$arResult["TICKET"]["TIMESTAMP_X"]?>

				<?if (strlen($arResult["TICKET"]["MODIFIED_MODULE_NAME"])<=0 || $arResult["TICKET"]["MODIFIED_MODULE_NAME"]=="tszhtickets"):?>
					[<?=$arResult["TICKET"]["MODIFIED_USER_ID"]?>]
					(<?=$arResult["TICKET"]["MODIFIED_BY_LOGIN"]?>)
					<?=$arResult["TICKET"]["MODIFIED_BY_NAME"]?>
				<?else:?>
					<?=$arResult["TICKET"]["MODIFIED_MODULE_NAME"]?>
				<?endif?>

				<br />
		<?endif?>


		<? if (strlen($arResult["TICKET"]["DATE_CLOSE"])>0): ?>
			<?=GetMessage("SUP_CLOSE")?>: <?=$arResult["TICKET"]["DATE_CLOSE"]?>
		<?endif?>


		<?if (strlen($arResult["TICKET"]["STATUS_NAME"])>0) :?>
				<?=GetMessage("SUP_STATUS")?>: <span title="<?=$arResult["TICKET"]["STATUS_DESC"]?>"><?=$arResult["TICKET"]["STATUS_NAME"]?></span><br />
		<?endif;?>

		<?if (strlen($arResult["TICKET"]["CATEGORY_NAME"]) > 0):?>
				<?=GetMessage("SUP_CATEGORY")?>: <span title="<?=$arResult["TICKET"]["CATEGORY_DESC"]?>"><?=$arResult["TICKET"]["CATEGORY_NAME"]?></span><br />
		<?endif?>
		<?
		$dbCategory = CTszhTicketDictionary::GetList(($by = 'id'), ($order = 'desc'), Array("ID" => $arResult['TICKET']['CATEGORY_ID']));
		if ($arCategory = $dbCategory->Fetch()) {
			if (IntVal($arCategory['TIME_TO_SOLVE']) > 0) {
				$timestamp = strtotime("+" . $arCategory['TIME_TO_SOLVE'] . ' hours', MakeTimeStamp($arResult["TICKET"]["DATE_CREATE"]));
				?>
				<?=GetMessage("TSZH_CONTROL_DATE")?>: <?=ConvertTimeStamp($timestamp, "SHORT")?><br />
				<?
			}
		}

		/*
		?>


		<?if(strlen($arResult["TICKET"]["CRITICALITY_NAME"])>0) :?>
				<?=GetMessage("SUP_CRITICALITY")?>: <span title="<?=$arResult["TICKET"]["CRITICALITY_DESC"]?>"><?=$arResult["TICKET"]["CRITICALITY_NAME"]?></span><br />
		<?endif*/?>


		<?if (intval($arResult["RESPONSIBLE_USER_ID"])>0):?>
			<?=GetMessage("SUP_RESPONSIBLE")?>: [<?=$arResult["TICKET"]["RESPONSIBLE_USER_ID"]?>]
			(<?=$arResult["TICKET"]["RESPONSIBLE_LOGIN"]?>) <?=$arResult["TICKET"]["RESPONSIBLE_NAME"]?><br />
		<?endif?>


		<?if (strlen($arResult["TICKET"]["SLA_NAME"])>0) :?>
			<?=GetMessage("SUP_SLA")?>:
			<span title="<?=$arResult["TICKET"]["SLA_NAME"]?>"><?=$arResult["TICKET"]["SLA_NAME"]?></span>
		<?endif?>


		</td>
	</tr>


	<tr>
		<th><?=GetMessage("SUP_DISCUSSION")?></th>
	</tr>


	<tr>
		<td>
	<?=$arResult["NAV_STRING"]?>

	<?foreach ($arResult["MESSAGES"] as $arMessage):?>
		<div class="ticket-edit-message">

		<div class="support-float-quote">[&nbsp;<a href="#postform" OnMouseDown="javascript:SupQuoteMessage()" title="<?=GetMessage("SUP_QUOTE_LINK_DESCR");?>"><?echo GetMessage("SUP_QUOTE_LINK");?></a>&nbsp;]</div>


		<div align="left"><b><?=GetMessage("SUP_TIME")?></b>: <?=$arMessage["DATE_CREATE"]?></div>
		<b><?=GetMessage("SUP_FROM")?></b>:


		<?=$arMessage["OWNER_SID"]?>

		<?if (intval($arMessage["OWNER_USER_ID"])>0):?>
			[<?=$arMessage["OWNER_USER_ID"]?>]
			(<?=$arMessage["OWNER_LOGIN"]?>)
			<?=$arMessage["OWNER_NAME"]?>
		<?endif?>
		<br />


		<?foreach ($arMessage["FILES"] as $arFile):?>
		<div class="support-paperclip"></div>
		<a title="<?=GetMessage("SUP_VIEW_ALT")?>" href="<?=$componentPath?>/ticket_show_file.php?hash=<?echo $arFile["HASH"]?>&amp;lang=<?=LANG?>"><?=$arFile["NAME"]?></a>
		<?
			$size = $arFile["FILE_SIZE"];
			$a = array("b", "kb", "mb", "gb");
			$pos = 0;

			while($size >= 1024)
			{
				$size /= 1024;
				$pos++;
			}

			$size = round($size,2)." ".$a[$pos];
		?>

		(<?=$size?>)
		[ <a title="<?=str_replace("#FILE_NAME#", $arFile["NAME"], GetMessage("SUP_DOWNLOAD_ALT"))?>" href="<?=$componentPath?>/ticket_show_file.php?hash=<?=$arFile["HASH"]?>&amp;lang=<?=LANG?>&amp;action=download"><?=GetMessage("SUP_DOWNLOAD")?></a> ]
		<br class="clear" />
		<?endforeach?>


		<br /><?=$arMessage["MESSAGE"]?>

		</div>
	<?endforeach?>

	<?=$arResult["NAV_STRING"]?>

		</td>

	</tr>

</table>

<?
$dbMessages = CTszhTicket::GetMessageList(($by = 's_number'), ($order = 'asc'), Array('TICKET_ID' => $arResult['TICKET']['ID'], 'TICKET_ID_EXACT_MATCH' => 'Y', 'IS_LOG' => 'Y'), ($is_filtered = false), 'N');

if ($dbMessages->SelectedRowsCount() > 0):

	$html = '';
	while ($arMessage = $dbMessages->Fetch()) {
		if ($arMessage['STATUS_ID']) {
			$html .= "<tr>";
			$html .= "<td>" . $arResult['DICTIONARY']['STATUS'][$arMessage['STATUS_ID']] . "</td>";
			$html .= "<td>" . $arMessage['DATE_CREATE'] . "</td>";
			$html .= "<td>[" . $arMessage['MODIFIED_USER_ID'] . "] " . $arMessage['MODIFIED_NAME'] . "</td>";
			$html .= "</tr>";
		}
	}
	if (strlen($html) > 0) {
		?><h3 style="margin: 25px 0 15px 0;"><?=GetMessage("TSZH_PROGRESS")?></h3><?
		?><table style="width: auto; margin: 10px;" cellpadding="4" class="support-statuses"><tr><th style="text-align: left;"><?=GetMessage("TSZH_STATUS")?></th><th><?=GetMessage("TSZH_DATETIME")?></th><th><?=GetMessage("TSZH_MODIFIED_BY")?></th></tr><?
		echo $html;
		?></table><?
	}
endif;
?>



<br />
<?endif;?>

<?if (count($arParams['EDIT_FIELDS']) > 0):?>
<form name="support_edit" method="post" action="<?=$arResult["REAL_FILE_PATH"]?>" enctype="multipart/form-data">
<?=bitrix_sessid_post()?>
<input type="hidden" name="set_default" value="Y" />
<input type="hidden" name="ID" value=<?=(empty($arResult["TICKET"]) ? 0 : $arResult["TICKET"]["ID"])?> />
<input type="hidden" name="lang" value="<?=LANG?>" />
<table class="support-ticket-edit-form data-table">

	<?if (empty($arResult["TICKET"])):?>
	<thead>
		<tr>
			<th colspan="2"><?=GetMessage("SUP_TICKET")?></th>
		</tr>
	</thead>

	<tbody>
	<tr>
		<td class="field-name border-none"><span class="starrequired">*</span><?=GetMessage("SUP_TITLE")?>:</td>
		<td class="border-none"><input type="text" name="TITLE" value="<?=htmlspecialchars($_REQUEST["TITLE"])?>" size="48" maxlength="255" /></td>
	</tr>
	<?else:?>

	<tr>
		<th colspan="2"><?=GetMessage("SUP_ANSWER")?></th>
	</tr>

	<?endif?>

<?if (in_array('MESSAGE', $arParams['EDIT_FIELDS']) && strlen($arResult["TICKET"]["DATE_CLOSE"]) <= 0):?>
	<tr>
		<td class="field-name"><span class="starrequired">*</span><?=GetMessage("SUP_MESSAGE")?>:</td>
		<td><textarea name="MESSAGE" id="MESSAGE" rows="20" cols="45" wrap="virtual"><?=htmlspecialchars($_REQUEST["MESSAGE"])?></textarea></td>
	</tr>
<?endif;?>
<?if (in_array('CRITICALITY', $arParams['EDIT_FIELDS'])):?>
	<tr>
		<td class="field-name"><?=GetMessage("SUP_CRITICALITY")?>:</td>
		<td>
			<?
			if (empty($arResult["TICKET"]) || strlen($arResult["ERROR_MESSAGE"]) > 0 )
			{
				if (strlen($arResult["DICTIONARY"]["CRITICALITY_DEFAULT"]) > 0 && strlen($arResult["ERROR_MESSAGE"]) <= 0)
					$criticality = $arResult["DICTIONARY"]["CRITICALITY_DEFAULT"];
				else
					$criticality = htmlspecialchars($_REQUEST["CRITICALITY_ID"]);
			}
			else
				$criticality = $arResult["TICKET"]["CRITICALITY_ID"];
			?>
			<select name="CRITICALITY_ID" id="CRITICALITY_ID" class="input">
				<option value="">&nbsp;</option>
			<?foreach ($arResult["DICTIONARY"]["CRITICALITY"] as $value => $option):?>
				<option value="<?=$value?>" <?if($criticality == $value):?>selected="selected"<?endif?>><?=$option?></option>
			<?endforeach?>
			</select>
		</td>
	</tr>
<?endif;?>
<?if (in_array('CATEGORY', $arParams['EDIT_FIELDS'])):?>
	<tr>
		<td class="field-name"><?=GetMessage('SUP_CATEGORY')?>:</td>
		<td>
			<?
			if (empty($arResult["TICKET"]) || strlen($arResult["ERROR_MESSAGE"]) > 0 )
			{
				if (strlen($arResult["DICTIONARY"]["CATEGORY_DEFAULT"]) > 0 && strlen($arResult["ERROR_MESSAGE"]) <= 0)
					$category = $arResult["DICTIONARY"]["CATEGORY_DEFAULT"];
				else
					$category = htmlspecialchars($_REQUEST["CATEGORY_ID"]);
			}
			else
				$category = $arResult["TICKET"]["CATEGORY_ID"];
			?>
			<select name="CATEGORY_ID" id="CATEGORY_ID" class="input">
				<option value="">&nbsp;</option>
			<?foreach ($arResult["DICTIONARY"]["CATEGORY"] as $value => $option):?>
				<option value="<?=$value?>" <?if($category == $value):?>selected="selected"<?endif?>><?=$option?></option>
			<?endforeach?>
			</select>
		</td>
	</tr>
<?endif;?>
<?// ********************* User properties ***************************************************
if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
	<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
	<tr><td class="field-name">
		<?if ($arUserField["MANDATORY"]=="Y"):?>
			<span class="starrequired">*</span>
		<?endif;?>
		<?=$arUserField["EDIT_FORM_LABEL"]?>:</td><td class="field-value">
			<?$APPLICATION->IncludeComponent(
				"bitrix:system.field.edit",
				$arUserField["USER_TYPE"]["USER_TYPE_ID"],
				array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS"=>"Y"));?></td></tr>
	<?endforeach;?>
<?endif;
// ******************** /User properties ***************************************************?>
<?if (in_array('STATUS', $arParams['EDIT_FIELDS'])):?>
	<tr>
		<td class="field-name"><?=GetMessage('SUP_STATUS')?>:</td>
		<td>
			<?
			if (empty($arResult["TICKET"]) || strlen($arResult["ERROR_MESSAGE"]) > 0 )
			{
				if (strlen($arResult["DICTIONARY"]["STATUS_DEFAULT"]) > 0 && strlen($arResult["ERROR_MESSAGE"]) <= 0)
					$status = $arResult["DICTIONARY"]["STATUS_DEFAULT"];
				else
					$status = htmlspecialchars($_REQUEST["STATUS_ID"]);
			}
			else
				$status = $arResult["TICKET"]["STATUS_ID"];
			?>
			<select name="STATUS_ID" id="STATUS_ID" class="input">
				<option value="">&nbsp;</option>
			<?foreach ($arResult["DICTIONARY"]["STATUS"] as $value => $option):?>
				<option value="<?=$value?>" <?if($status == $value):?>selected="selected"<?endif?>><?=$option?></option>
			<?endforeach?>
			</select>
		</td>
	</tr>
<?endif;?>
<?if (in_array('RESPONSIBLE', $arParams['EDIT_FIELDS'])):?>
	<tr>
		<td class="field-name"><?=GetMessage('SUP_RESPONSIBLE')?>:</td>
		<td>
			<?
			if (empty($arResult["TICKET"]) || strlen($arResult["ERROR_MESSAGE"]) > 0 )
			{
				if (strlen($arResult["DICTIONARY"]["STATUS_DEFAULT"]) > 0 && strlen($arResult["ERROR_MESSAGE"]) <= 0)
					$user_id = false;
				else
					$user_id = htmlspecialchars($_REQUEST["RESPONSIBLE_USER_ID"]);
			}
			else
				$user_id = $arResult["TICKET"]["RESPONSIBLE_USER_ID"];
			?>
			<select name="RESPONSIBLE_USER_ID" id="RESPONSIBLE_USER_ID" class="input">
				<option value="">&nbsp;</option>
			<?foreach ($arResult["DICTIONARY"]["SUPPORT_TEAM"] as $value => $option):?>
				<option value="<?=$value?>" <?if($user_id == $value):?>selected="selected"<?endif?>><?=$option?></option>
			<?endforeach?>
			</select>
		</td>
	</tr>
<?endif;?>
<?/*	<?if (strlen($arResult["TICKET"]["DATE_CLOSE"])<=0):?>
	<tr>
		<td class="field-name"><?=GetMessage("SUP_CLOSE_TICKET")?>:</td>
		<td><input type="checkbox" name="CLOSE" value="Y" <?if($arResult["TICKET"]["CLOSE"] == "Y"):?>checked="checked" <?endif?>/>
		</td>
	</tr>
	<?else:?>
	<tr>
		<td  class="field-name"><?=GetMessage("SUP_OPEN_TICKET")?>:</td>
		<td><input type="checkbox" name="OPEN" value="Y" <?if($arResult["TICKET"]["OPEN"] == "Y"):?>checked="checked" <?endif?>/>
		</td>
	</tr>
	<?endif;?>
	<?if ($arParams['SHOW_COUPON_FIELD'] == 'Y' && $arParams['ID'] <= 0){?>
	<tr>
		<td  class="field-name"><?=GetMessage("SUP_COUPON")?>:</td>
		<td><input type="text" name="COUPON" value="<?=htmlspecialchars($_REQUEST["COUPON"])?>" size="48" maxlength="255" />
		</td>
	</tr>
	<?}?>
*/?>
	<tr>
		<td></td>
		<td><span class="starrequired">*</span><?=GetMessage("SUP_REQ")?><br /><br />
<?
			TemplateShowButton(Array(
				'type' => 'submit',
				'attr' => Array(
					'name' => 'save',
					'value' => '1',
				),
				'title' => (empty($arResult['TICKET']) ? GetMessage("TSZH_SEND_REQUEST") : GetMessage("TSZH_SAVE_REQUEST")),
			));
?>
		</td>
	</tr>
	</tbody>
</table>
<input type="hidden" value="Y" name="apply" />

</form>
<?endif;?>
