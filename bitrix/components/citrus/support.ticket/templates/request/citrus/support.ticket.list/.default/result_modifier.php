<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
return;

$arStatuses = Array();
$rsStatuses = CTszhTicketDictionary::GetList($by='s_c_sort', $order='asc', Array('TYPE' => 'S'), $is_filtered);
while ($arStatus = $rsStatuses->Fetch()) {
	$arStatuses[$arStatus['ID']] = $arStatus;
}
$arResult['STATUSES'] = $arStatuses;

foreach ($arResult["TICKETS"] as $key=>$arTicket) {
	$arResult['TICKETS'][$key]['STATUS_SID'] = $arStatuses[$arTicket['STATUS_ID']]['SID'];
}

?>
