<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Citrus\Tszh\ConfirmTable;
if(!CModule::IncludeModule("citrus.tszh"))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_INSTALLED"));
	return;
}

	if(!isset($arResult) || !is_array($arResult))
		$arResult = array();
	
	/*данные для согласия*/
	global $USER;
	
	$arResult["TSZH_DATA"] = $param = CTszh::GetList(array(),array(),false, false,array())->GetNext();
	$arResult["CONFIRM_TSZH"] = $param['CONFIRM_PARAM'];
	$arAccount = CTszhAccount::GetByUserID($USER->GetID());
	$arUser = CUser::GetByID($USER->GetID())->Fetch();
	$arConfirmCheck = ConfirmTable::getList(array(
		'filter' => array("USER_ID"=>$arUser["ID"])
	))->fetchAll();

	$arResult["CONFIRM_ACC"] = $arConfirmCheck["0"]["CONFIRM_CHECK"];

global $not_show_personal_data;
if ($not_show_personal_data)
	$arResult["USER_NAME"] = "";