<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	use Citrus\Tszh\ConfirmTable;
	
    global $USER, $APPLICATION;

	if ($USER->IsAuthorized()) {
		$confirm_check = ConfirmTable::getList()->FetchAll();
		$arUsers = CUser::GetByID($USER->GetID())->Fetch();
		//echo "<pre>"; var_dump($confirm_check); echo "</pre>"; die;
		$date = date("d.m.Y");
		$url = $_SERVER["HTTP_REFERER"];
		$ip = $_SERVER["REMOTE_ADDR"];
		if ((isset($_REQUEST['confirm']))) {
			//if ((!isset($confirm_check['0']['USER_ID'])==$arUsers['ID'])){
				$confirm = array(
					"CONFIRM_CHECK" => "Y",
					"DATE" => GetMessage("AUTH_CONFIRM_DATE", Array ("#DATE#" => $date)),
					"ACCOUNT_NAME" => $arUsers['NAME'],
					"IP" => GetMessage("AUTH_CONFIRM_IP", Array ("#IP#" => $ip)),
					"URL"=>   GetMessage("AUTH_CONFIRM_URL", Array ("#URL#" => $url)),
					"USER_ID" => $arUsers["ID"],
				);
				ConfirmTable::add($confirm);
			//}
		}
	}
	else{
		$confirm_check = ConfirmTable::getList()->FetchAll();
		$date = date("d.m.Y");
		$url = $_SERVER["HTTP_REFERER"];
		$ip = $_SERVER["REMOTE_ADDR"];
		if ((isset($_REQUEST['confirm']))) {
			$confirm = array(
				"CONFIRM_CHECK" => "Y",
				"DATE" => GetMessage("AUTH_CONFIRM_DATE", Array ("#DATE#" => $date)),
				"ACCOUNT_NAME" => $_REQUEST["user_name"],
				"IP" => GetMessage("AUTH_CONFIRM_IP", Array ("#IP#" => $ip)),
				"URL"=>   GetMessage("AUTH_CONFIRM_URL", Array ("#URL#" => $url)),
				"USER_ID" => " ",
			);
			ConfirmTable::add($confirm);
		}
	}


if (!($GLOBALS["APPLICATION"]->arAuthResult["TYPE"] || $GLOBALS["APPLICATION"]->arAuthResult["MESSAGE"])) {

    if ($USER->IsAuthorized()) {
        $rsUserGroups = $USER->GetUserGroupEx($USER->GetID());
        while ($arGroup = $rsUserGroups->Fetch())
        {
            if (in_array($arGroup['STRING_ID'], array(
                'TSZH_SUPPORT_ADMINISTRATORS',
                'TSZH_SUPPORT_CONTRACTORS',
            )))
            {
                $personalURL = SITE_DIR . "workplace/";
                die('Redirect'.'|'.$personalURL);
            }
	        if (in_array($arGroup['STRING_ID'], array(
		        'JKH_ACCESS_ADMINISTRATORS'
	        )))
	        {
		        $personalURL = SITE_DIR . "workplace_access/";
		        die('Redirect'.'|'.$personalURL);
	        }
        }
        die('Auth' . '|' . 'Y');
    }

    if (isset($arResult['ERROR_MESSAGE']['MESSAGE']) && strlen($arResult['ERROR_MESSAGE']['MESSAGE']) > 0) {
        die($arResult['ERROR_MESSAGE']['TYPE'] . '|' . $arResult['ERROR_MESSAGE']['MESSAGE']);
    } else {
        die("ERROR" . '|' . '������ �����������');
    }
} else {
    die($GLOBALS["APPLICATION"]->arAuthResult["TYPE"] . '|' . $GLOBALS["APPLICATION"]->arAuthResult["MESSAGE"] . '|' . $_REQUEST["USER_LOGIN"] . '|' . $APPLICATION->NeedCAPTHAForLogin($_REQUEST["USER_LOGIN"]) . "|" . $APPLICATION->CaptchaGetCode());

}

?>