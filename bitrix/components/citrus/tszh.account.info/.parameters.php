<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"SHOW_FIO" => Array(
			"NAME" => GetMessage("SHOW_FIO"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
        "SHOW_ADDRESS" => Array(
            "NAME" => GetMessage("SHOW_ADDRESS"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
	),
);
?>
