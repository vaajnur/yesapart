<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule('citrus.tszh'))
{
    ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));
    return;
}

if (!CTszhFunctionalityController::CheckEdition())
    return;

if (!$USER->IsAuthorized()) {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
    return;
}
$arResult['OWNER_LS'] = CTszh::IsTenant();
/*if (!CTszh::IsTenant()) {
    $APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"));
    return;
}*/

if($arParams["SHOW_FIO"] == 'Y')
    $arResult['FIO'] = CTszhAccount::GetFullName($USER->GetID());

$rsAccount = CTszhAccount::GetList(
    array(),
    array(
        "USER_ID"=>$USER->GetID(),
    //    "CURRENT"=>"Y"
    ),
    false,
    array(),
    array(
        "XML_ID",
        "CURRENT",
        "ADDRESS_FULL",
        "REGION",
        "CITY",
        "SETTLEMENT",
        "DISTRICT",
        "STREET",
        "NAME",
        "HOUSE",
        "FLAT",
        "FLAT_ABBR"
    ));

$arElementFirst = array();

if (intval($rsAccount->SelectedRowsCount()) == 1)
{
	$arElement = $rsAccount->GetNext();
}
else
{
	while ($arAccount = $rsAccount->Fetch())
	{
		if (!$arElementFirst) $arElementfirst = $arAccount;

		if ($arAccount["CURRENT"] == "Y")
		{
			{
				$arElement = $arAccount;
				break;
			}
		}
	}
	if (!$arElement)
	{
		$arElement = $arElementFirst;
	}
}

if (is_array($arElement))
{
	if ($arElement["NAME"])
	{
		$arResult["FIO"] = $arElement["NAME"];
	}
	if ($arParams["SHOW_ADDRESS"] == 'Y')
	{
		$arResult['ADDRESS'] = CTszhAccount::GetFullAddress($arElement);
	}
	$GLOBALS["VARS"]['LS_ID'] = $arElement['XML_ID'];
}

$this->IncludeComponentTemplate();
?>