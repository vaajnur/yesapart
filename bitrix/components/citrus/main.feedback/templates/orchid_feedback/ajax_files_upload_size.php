<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

if(!check_bitrix_sessid() || ($_SESSION["MAX_FILE_SIZE"] != $_POST["MAX_FILE_SIZE"])){
	$data = array('error' => 'connect');
	die(json_encode($data));
}

$MAX_UPLOAD_FILE_SIZE = $_SESSION["MAX_FILE_SIZE"];

$totalSize = 0;
if($_POST["filepath"]){
	$arUpFiles = explode(',',$_POST["filepath"]);
	foreach($arUpFiles as $ufile){
		$totalSize += filesize($ufile);
		if($totalSize > $MAX_UPLOAD_FILE_SIZE){
			$data = array('error' => 'too_big_files');
			die(json_encode($data));
			break;
		}
	}
}
$data = array('size' => $totalSize);
die(json_encode($data));
