$(document).ready(function () {


    $('.feedback').submit(
        function (event) {

            var confirm = $(this).find('[name="confirm"]');

            var input = $('#confirm_window-form');
            if (typeof confirm != 'undefined' && input.length) {
                if (confirm.is(":checked")) {
                    $('.opd__input-opd-err').hide();
                } else {
                    event.preventDefault();
                    $('.opd__input-opd-err').show();
                }
            }

        });

});