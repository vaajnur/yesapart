<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

if(!check_bitrix_sessid() || ($_SESSION["MAX_FILE_SIZE"] != $_POST["MAX_FILE_SIZE"])) die;

$MAX_UPLOAD_FILE_SIZE = $_SESSION["MAX_FILE_SIZE"];

// удаляем все папки старше 20 минут
function rmRec($path){
  if(is_file($path))
		return unlink($path);
  if(is_dir($path)){
    foreach(scandir($path) as $p) 
			if(($p!='.') && ($p!='..'))
				rmRec($path.'/'.$p);
    return rmdir($path); 
  }
  return false;
}

$dir = $_SERVER['DOCUMENT_ROOT'].'/upload/tmp_upload_files/';
if(!is_dir($dir)){
	mkdir($dir, 0777);
}
else{
	if($d = @opendir($dir)){
		while(($file = readdir($d)) !== false){
			if ($file != "." && $file != ".."){ 
				$ftime = filemtime($dir.$file);
				if(time()-$ftime > 1200)
					rmRec($dir.'/'.$file);
			}	
		}	
		closedir($d);
	}
}
			
// сохраняем файлы в отдельную папку
$visualDir = '/upload/tmp_upload_files/'.bitrix_sessid();
$uploadDir = $_SERVER['DOCUMENT_ROOT'].$visualDir;
if(!is_dir($uploadDir)) 
	mkdir($uploadDir, 0777);

$files = $_FILES;

$totalSize = 0;
if($_POST["filepath"]){
	$arUpFiles = explode(',',$_POST["filepath"]);
	foreach($arUpFiles as $ufile){
		$totalSize += filesize($ufile);
	}
}
	
	
// Bitrix\Main\Diag\Debug::writeToFile(array('$MAX_UPLOAD_FILE_SIZE' => $MAX_UPLOAD_FILE_SIZE),"","/upload/debug.txt");	

$tooSize = false;
$done_files = array();
foreach($files as $file){
	$fileName = $file['name'];
	if(move_uploaded_file($file['tmp_name'], "$uploadDir/$fileName")){
		$path = realpath("$uploadDir/$fileName");
		$done_files[] = array(
			'path' => $path,
			'filename' => $fileName,
		);
	}
	$totalSize += $file["size"];
	if($totalSize > $MAX_UPLOAD_FILE_SIZE && !$tooSize){
		$tooSize = 'too_big_files';
		// die(json_encode($data));
		// break;
	}
}
if($done_files){
	$data['files'] = $done_files;
	if($tooSize)
		$data['toosize'] = $tooSize;
}
else
	$data['error'] = 'upload_files';

// $data = $done_files ? array('files' => $done_files, 'error' => $error) : array('error' => $error);
die(json_encode($data));
