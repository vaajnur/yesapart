<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Citrus\Tszh\ConfirmTable;
use Citrus\Tszh\Types\ReceiptType;
use Citrus\Tszhpayment\PaymentPayerIdTable;
use Citrus\Tszhpayment\PaymentServicesTable;
use Citrus\Tszh\DetailsPaymentsTable;

$app = Application::getInstance();
$request = $app->getContext()->getRequest();

if (!CModule::IncludeModule("citrus.tszh"))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_INSTALLED"));

	return;
}
if (!CModule::IncludeModule("citrus.tszhpayment"))
{
	ShowError(GetMessage("TSZH_PAYMENT_MODULE_NOT_INSTALLED"));

	return;
}

$arParams["MINIMUM_SUMM"] = FloatVal($arParams["MINIMUM_SUMM"]);
if ($arParams["MINIMUM_SUMM"] <= 0)
{
	$arParams["MINIMUM_SUMM"] = 0;
}

$arParams["PAY_SYSTEM"] = IntVal($arParams["PAY_SYSTEM"]);
$arResult["PAY_SYSTEM"] = CTszhPaySystem::GetByID($arParams["PAY_SYSTEM"]);
if (!is_array($arResult["PAY_SYSTEM"]))
{
	ShowError(GetMessage("TSZH_PAYMENT_SYSTEM_DOESNT_EXISTS"));

	return;
}

$arParams['VAR'] = 'pay_amount';

$arNotes = Array();
$arErrors = Array();

$defaultSum2Pay = '';
if ($USER->IsAuthorized() && $account = CTszhAccount::GetByUserID($USER->GetID()))
{
	// check receipt type in payment params
	$ptype = is_set($request->get('ptype')) && ReceiptType::isReceiptType($request->get('ptype'), array(
		ReceiptType::MAIN,
		ReceiptType::FINES_MAIN,
		ReceiptType::OVERHAUL,
		ReceiptType::FINES_OVERHAUL,
	)) ? strtoupper($request->get('ptype')) : 'MAIN';

	$receipt_type = ReceiptType::getConstants();
	$receipt_type_titles = ReceiptType::getTitles();

	$lastPeriod = CTszhAccountPeriod::GetList(
		array('PERIOD_DATE' => 'DESC', 'PERIOD_ID' => 'DESC'),
		array('ACCOUNT_ID' => $account['ID'], 'PERIOD_ACTIVE' => 'Y', '!PERIOD_ONLY_DEBT' => 'Y', 'TYPE' => $receipt_type[$ptype]),
		false,
		false,
		array('ID', 'DEBT_END', 'SUMM_TO_PAY', 'DEBT_BEG')
	)->GetNext(true, false);

	$insurance_to_pay = CTszhCharge::GetList(
		array('PERIOD_ID' => 'DESC'),
		array('ACCOUNT_ID' => $account['ID'], 'IS_INSURANCE' => 'Y'),
		false,
		false,
		array('SUMM')
	)->GetNext(true, false);

	if (COption::GetOptionString("citrus.tszh", "pay_to_executors_only", "N") == "Y")
	{
		$contractor = CTszhAccountContractor::GetList(array(), array(
			"ACCOUNT_PERIOD_ID" => $lastPeriod["ID"],
			"!CONTRACTOR_EXECUTOR" => "N",
		), array("SUMM", "CONTRACTOR_NAME"))->Fetch();
		$defaultSum2Pay = is_array($contractor) ? $contractor['SUMM'] : 0;

		if (!(is_set($_GET, "summ") && is_numeric($_GET["summ"])) || $_GET["summ"] == $defaultSum2Pay)
		{
			ShowNote(GetMessage("CITRUS_TSZHPAYMENT_EXECUTOR_NOTICE", array('#EXECUTOR#' => $contractor["CONTRACTOR_NAME"])));
		}
	}
	else
	{
		$defaultSum2Pay = $lastPeriod["DEBT_END"];
	}
}
$arResult['AMOUNT_TO_SHOW'] = floatval($arParams['AMOUNT_TO_SHOW']) > 0 ? floatval($arParams['AMOUNT_TO_SHOW']) : $defaultSum2Pay;

if ($insurance_to_pay)
{
	$arResult['AMOUNT_TO_SHOW_INSURANCE'] = $insurance_to_pay;
}
$arResult['CURRENCY_TITLE'] = GetMessage("TSZH_PAYMENT_CURRENCY_TITLE");
$arResult["ACTION"] = $APPLICATION->GetCurPageParam("", Array($arParams["VAR"]));

if (is_set($arParams['TSZH_ID']))
{
	$arTszh = CTszh::GetList(
		array(),
		array(
			'ID' => (int)$arParams['TSZH_ID'],
		),
		false,
		false,
		array('IS_BUDGET')
	)->Fetch();

	$arResult['IS_BUDGET'] = $arTszh['IS_BUDGET'] === 'Y' && $arResult['PAY_SYSTEM']['ACTION_FILE'] == '/bitrix/modules/citrus.tszhpayment/ru/payment/moneta';

	if ($arResult['IS_BUDGET'])
	{
		$arResult['PAYER_ID']['DOCUMENT_TYPE']['LIST'] = PaymentPayerIdTable::getDocumentTypeList();
		$arResult['PAYER_ID']['NATIONALITY']['LIST'] = PaymentPayerIdTable::getNationalityList();
	}
}

// org list for unauth users
if (!$USER->IsAuthorized())
{
	// org id for selected pay system
	$rsPS = CTszhPaySystem::GetList(Array(), Array("ACTION_FILE" => $arResult["PAY_SYSTEM"]["ACTION_FILE"]), Array("TSZH_ID"));
	$tszhIdList = array();
	while ($arPS = $rsPS->Fetch())
	{
		$tszhIdList[] = $arPS['TSZH_ID'];
	}

	$rsTszh = CTszh::GetList(array("NAME" => "ASC"), array("SITE_ID" => SITE_ID, "@ID" => $tszhIdList));
	$arResult["TSZH"] = array();
	while ($arTszh = $rsTszh->GetNext())
	{
		$arResult["TSZH"][$arTszh["ID"]] = $arTszh["NAME"];
		$arResult["TSZH_LIST"][$arTszh["ID"]] = $arTszh;
	}

	// if in params set org id, show only this org
	if (is_set($arParams, "TSZH_ID") && $arParams["TSZH_ID"] > 0)
	{
		$arResult["TSZH"] = array_intersect_key($arResult["TSZH"], array(intval($arParams["TSZH_ID"]) => 1));
	}
}
else
{
	$arResult["ACC"] = $arAccounts = CTszhAccount::GetByUserID($USER->GetID());
	// check account id
	if (is_array($arAccounts)
	    && intval($arAccounts['TSZH_ID']) > 0
	    && class_exists('\\Citrus\\Tszh\\ConfirmTable'))
	{
		$arResult["TSZH"] = CTszh::GetByID($arResult["ACC"]["TSZH_ID"]);
		$arUser = CUser::GetByID($USER->GetID())->Fetch();
		$arConfirmCheck = ConfirmTable::getList(
			array(
				'filter' => array("USER_ID" => $arUser["ID"]),
			)
		)->fetch();
		$arResult["CONFIRM_ACC"] = $arConfirmCheck["CONFIRM_CHECK"];
	}
	else
	{
		$arResult["CONFIRM_ACC"] = 'N';
	}

	/*if ($arResult['IS_BUDGET']
	    && is_array($arAccounts))
	{
		$arPayerInfo = PaymentPayerIdTable::getList(array(
			'filter' => array('ACCOUNT_ID' => $arAccounts['ID'], 'PAYMENT.PAYED' => 'Y'),
			'select' => array('DOCUMENT_TYPE', 'DOCUMENT_NUMBER', 'NATIONALITY'),
			'order' => array('PAYMENT_ID' => 'DESC'),
			'limit' => 1
			)
		)->fetch();

		if (is_array($arPayerInfo))
		{
			$arResult['PAYER_FIELDS'] = $arPayerInfo;
		}
	}*/

}

if (isset($_GET['needauth']))
{
	$APPLICATION->set_cookie('citrus_tszhpayment_needauth', '1', strtotime('+1 month'));
}

global $USER_FIELD_MANAGER;
$arResult["USER_FIELDS"] = $USER_FIELD_MANAGER->GetUserFields(CTszhPayment::USER_FIELD_ENTITY, 0, LANGUAGE_ID);
foreach ($arResult['USER_FIELDS'] as $key => $value)
{
	if ($value['EDIT_IN_LIST'] !== 'Y')
	{
		unset($arResult['USER_FIELDS'][$key]);
	}
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && check_bitrix_sessid() && IntVal($_POST["pay_system_id"]) == $arResult['PAY_SYSTEM']['ID'])
{
	// if user not in group tszh members
	if ($USER->IsAuthorized() && !CTszh::IsTenant())
	{
		$APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"), true, true, "N", false);

		return;
	}

	if (!is_numeric($_POST[$arParams["VAR"]]))
	{
		$arErrors[] = GetMessage("TSZH_PAYMENT_DO_AMOUNT_ERROR");
		$arResult['AMOUNT_TO_SHOW'] = htmlspecialchars($_POST[$arParams["VAR"]]);
	}

	if (isset($_POST["insurance"]))
	{
		if ($_POST["insurance"] == "with_insurance")
		{
			$arResult["FIELDS"]["IS_INSURANCE"] = 1;
		}
		else
		{
			$arResult["FIELDS"]["IS_INSURANCE"] = 0;
		}
	}
	else
	{
		$arResult["FIELDS"]["IS_INSURANCE"] = 0;
	}

	if (!$USER->IsAuthorized() && !isset($_POST['payment_step2']))
	{
		if (isset($_GET['needauth']) || $APPLICATION->get_cookie('citrus_tszhpayment_needauth'))
		{
			$APPLICATION->AuthForm(GetMessage("CITRUS_TSZHPAYMENT_NEED_AUTH"), true, true, "N", false);

			return;
		}
		$arResult["UNATH_PAYMENT"] = "Y";
		$lastPayment = $APPLICATION->get_cookie("citrus_tszhpayment_lastpayment");
		if ($lastPayment)
		{
			$rsPayment = CTszhPayment::GetList(Array(), Array(
				"ID" => $lastPayment,
				"LID" => SITE_ID,
				"USER_ID" => false,
			));
			if ($arLastPayment = $rsPayment->GetNext())
			{
				$needFields = Array("C_PAYEE_NAME", "C_ADDRESS", "C_ACCOUNT", "TSZH_ID");
				$arResult["FIELDS"] = Array();
				foreach ($needFields as $field)
				{
					if (isset($arLastPayment[$field]))
					{
						$arResult["FIELDS"][$field] = $arLastPayment[$field];
					}
				}
			}
		}

		if (is_set($_GET, "summ") && is_numeric($_GET["summ"]))
		{
			$arResult["AMOUNT_TO_SHOW"] = $arResult["FIELDS"][$arParams["VAR"]] = htmlspecialcharsbx($_GET["summ"]);
		}

		$this->IncludeComponentTemplate();

		return;
	}

	if (!$USER->IsAuthorized())
	{
		$arResult["UNATH_PAYMENT"] = "Y";

		$arResult["FIELDS"] = $_POST;
		array_map('htmlspecialcharsbx', $arResult["FIELDS"]);

		$arResult["FIELDS"]["C_PAYEE_NAME"] = trim($arResult["FIELDS"]["C_PAYEE_NAME"]);
		if (!strlen($arResult["FIELDS"]["C_PAYEE_NAME"]))
		{
			$arErrors[] = GetMessage("CITRUS_TSZHPAYMENT_ERROR_PAYEE_NAME");
		}

		$arResult["FIELDS"]["C_ADDRESS"] = trim($arResult["FIELDS"]["C_ADDRESS"]);
		if (!strlen($arResult["FIELDS"]["C_ADDRESS"]))
		{
			$arErrors[] = GetMessage("CITRUS_TSZHPAYMENT_ERROR_C_ADDRESS");
		}

		$arResult["FIELDS"]["C_ACCOUNT"] = trim($arResult["FIELDS"]["C_ACCOUNT"]);

		if (!strlen($arResult["FIELDS"]["C_ACCOUNT"]))
		{
			$arErrors[] = GetMessage("CITRUS_TSZHPAYMENT_ERROR_C_ACCOUNT");
		}
		else
		{
			if (!CTszh::hasDemoDataOnly($arResult["FIELDS"]["TSZH_ID"]) && !CTszhAccount::GetByXmlID($arResult["FIELDS"]["C_ACCOUNT"]))
			{
				$arErrors[] = GetMessage("CITRUS_TSZHPAYMENT_ERROR_NO_C_ACCOUNT");
			}
		}
		if (!preg_match('#[\d+]+#', $arResult["FIELDS"]["C_ACCOUNT"]))
		{
			$arErrors[] = GetMessage("CITRUS_TSZHPAYMENT_ERROR_C_ACCOUNT_INCORRECT");
		}

		$arResult["FIELDS"]["TSZH_ID"] = intval($arResult["FIELDS"]["TSZH_ID"]);
		if (!array_key_exists($arResult["FIELDS"]["TSZH_ID"], $arResult["TSZH"]))
		{
			$arErrors[] = GetMessage("CITRUS_TSZHPAYMENT_ERROR_TSZH_ID");
		}
	}

	$price = FloatVal($_POST[$arParams["VAR"]]);
	if ($price < $arParams['MINIMUM_SUMM'])
	{
		$arErrors[] = str_replace('#SUMM#', number_format($arParams['MINIMUM_SUMM'], 2, ',', ''), GetMessage("TSZH_PAYMENT_DO_ERROR_MINIMUM_SUMM"));
		$arResult['AMOUNT_TO_SHOW'] = $arResult["FIELDS"][$arParams["VAR"]] = $price;
	}

	if ($arResult['IS_BUDGET'])
	{
		$payerIdData = array(
			'DOCUMENT_TYPE' => $_POST['DOCUMENT_TYPE'],
			'DOCUMENT_NUMBER' => $_POST['DOCUMENT_NUMBER'],
			'NATIONALITY' => $_POST['NATIONALITY'],
		);
		$res = new \Bitrix\Main\Entity\Result();
		PaymentPayerIdTable::checkFields($res, 'PAYMENT_ID', $payerIdData);
		if (!$res->isSuccess())
		{
			foreach ($res->getErrorMessages() as $error)
			{
				if (!is_null($error))
				{
					$arErrors[] = $error;
				}
			}
		}
	}

	if (count($arErrors) <= 0)
	{
		// save user consent to the processing of personal data
		if (class_exists('\\Citrus\\Tszh\\ConfirmTable'))
		{
			$date = date("d.m.Y");
			$url = $_SERVER["HTTP_REFERER"];
			$ip = $_SERVER["REMOTE_ADDR"];
			if ($USER->IsAuthorized())
			{
				$arUser = CUser::GetByID($USER->GetID())->Fetch();
			}
			else
			{
				$arUser['ID'] = '';
				$arUser['NAME'] = strval($_REQUEST["user_name"]);
			}
			if ((isset($_REQUEST['confirm'])) && strval($_REQUEST['confirm']) == 'Y')
			{
				$confirm = array(
					"CONFIRM_CHECK" => "Y",
					"DATE" => $date,
					"ACCOUNT_NAME" => $arUser['NAME'],
					"IP" => $ip,
					"URL" => $url,
					"USER_ID" => $arUser['ID'],
				);
				$confirm_result = ConfirmTable::add($confirm);
			}
		}

		$arFields = array(
			"LID" => SITE_ID,
			"SUMM" => $price,
			"CURRENCY" => "RUB",
			"PAY_SYSTEM_ID" => $arResult["PAY_SYSTEM"]["ID"],
		);

		// depending on the selected payment settings, we will add fields
		$arFields['IS_OVERHAUL'] = ReceiptType::isReceiptType($receipt_type[$ptype], array(
			ReceiptType::OVERHAUL,
			ReceiptType::FINES_OVERHAUL,
		)) ? 'Y' : 'N';
		$arFields['IS_PENALTY'] = ReceiptType::isReceiptType($receipt_type[$ptype], array(
			ReceiptType::FINES_MAIN,
			ReceiptType::FINES_OVERHAUL,
		)) ? 'Y' : 'N';

		if ($USER->IsAuthorized())
		{
			$arAccount = CTszhAccount::GetByUserID($USER->GetID());
			$arFields["USER_ID"] = $USER->GetID();
			$arFields["ACCOUNT_ID"] = $arAccount['ID'];
			$arFields["C_ACCOUNT"] = $arAccount['EXTERNAL_ID'];
			$arFields["EMAIL"] = $_POST['email'];
			$arFields["TSZH_ID"] = (isset($arParams["TSZH_ID"]) && intval($arParams["TSZH_ID"]) > 0 ? intval($arParams["TSZH_ID"]) : $arAccount["TSZH_ID"]);
			$arFields["INSURANCE_INCLUDED"] = $arResult["FIELDS"]["IS_INSURANCE"];
		}
		else
		{
			$arAccId = CTszhAccount::GetList(Array("ID" => "ASC"), array("XML_ID" => $arResult["FIELDS"]["C_ACCOUNT"]), false, false, Array("ID"))->GetNext();
			$arFields = $arFields + Array(
					"C_PAYEE_NAME" => $arResult["FIELDS"]["C_PAYEE_NAME"],
					"C_ADDRESS" => $arResult["FIELDS"]["C_ADDRESS"],
					"C_COMMENTS" => $arResult["FIELDS"]["C_COMMENTS"],
					"C_ACCOUNT" => $arResult["FIELDS"]["C_ACCOUNT"],
					"TSZH_ID" => $arResult["FIELDS"]["TSZH_ID"],
					"ACCOUNT_ID" => $arAccId["ID"],
					"INSURANCE_INCLUDED" => $arResult["FIELDS"]["IS_INSURANCE"],
				);
			$rsItems = CTszhAccount::GetList(
				array(),
				array("XML_ID" => $arResult["FIELDS"]["C_ACCOUNT"], "TSZH_ID" => $arResult["FIELDS"]["TSZH_ID"]),
				false,
				array(),
				array()
			);
			if ($arItem = $rsItems->Fetch())
			{
				$arFields["ACCOUNT_ID"] = $arItem['ID'];
			}
		}
		$GLOBALS["USER_FIELD_MANAGER"]->EditFormAddFields(CTszhPayment::USER_FIELD_ENTITY, $arFields);
		$arPaySystem = CTszhPaySystem::GetByID($arResult["PAY_SYSTEM"]["ID"]);
		if (strlen($arPaySystem['CURRENCY']) > 0)
		{
			$arFields["CURRENCY"] = $arPaySystem['CURRENCY'];
		}
		if (count($arErrors) <= 0)
		{
			$paymentID = CTszhPayment::Add($arFields);

		}
		$dbCharge = CTszhCharge::GetList(
			Array("SORT" => "ASC", "ID" => "ASC"),
			Array(

				'@ID'=>$_POST['period'],
			)
		);
		$tmpCharge = CTszhCharge::GetList(
			Array("SORT" => "ASC", "ID" => "ASC"),
			Array(

				'@ID'=>$_POST['period'],
			)
		);
		$arServiceFields = array();
		$arChargeArray = array();
		$arServices = array();
		$negativeTotal = 0;
		while ($tempCharges = $tmpCharge ->GetNext())
		{
			if($_POST[urlencode($tempCharges['SERVICE_NAME'])]<0)
			{
				$negativeTotal +=$_POST[urlencode($tempCharges['SERVICE_NAME'])];
			}
			if($_POST[urlencode($tempCharges['SERVICE_NAME'])]>0)
			{
				$arChargeArray[] = $tempCharges;
			}
		}
		$chargesCount = count($arChargeArray);
		$prices = $_POST[$arParams["VAR"]];
		$sumTotalFromPayment = $prices;
		$checkNegative = $lastPeriod["DEBT_END"]+$negativeTotal;
		//Bitrix\Main\Diag\Debug::dumpToFile($negativeTotal,"","__servise_log.txt");
		if(isset($negativeTotal))
		{
			$sumTotalbyCharges= $lastPeriod["DEBT_END"]-$negativeTotal;
		}
		else
		{
			$sumTotalbyCharges=$lastPeriod["DEBT_END"];
		}
		if($sumTotalbyCharges<$sumTotalFromPayment){
			$sumPrePayment = $sumTotalFromPayment - $sumTotalbyCharges;
		}
		$count = 0;
		while ($arCharges = $dbCharge ->GetNext())
		{
			if($_POST[urlencode($arCharges['SERVICE_NAME'])]>0)
			{
				$count++;
				$sumTotalbyPercent = $_POST[urlencode($arCharges['SERVICE_NAME'])]/$sumTotalbyCharges;//$arCharges['DEBT_END']/$sumTotalbyCharges;
				$chargeProportional = $sumTotalFromPayment*$sumTotalbyPercent;

				$separatedSumTotal += round($chargeProportional, 2, PHP_ROUND_HALF_DOWN);
				$arServiceFields['GUID'] = $arCharges['GUID'];
				$arServiceFields['SERVICE_NAME'] = $arCharges['SERVICE_NAME'];
				$arServiceFields['SUMM_PAYED'] = round ( $chargeProportional, 2, PHP_ROUND_HALF_DOWN);
				if($count == $chargesCount){
					$arServiceFields['SUMM_PAYED'] = round ( $chargeProportional, 2, PHP_ROUND_HALF_DOWN)+ ($sumTotalFromPayment-$separatedSumTotal);
				}
				$arServiceFields['PAYMENT_ID'] =  $paymentID;
				$arService = PaymentServicesTable::add($arServiceFields);
				$serviceById = CTszhService::GetByID($arCharges['SERVICE_ID']);
				$arServices[$serviceById["NAME"]] = round ( $chargeProportional, 2, PHP_ROUND_HALF_DOWN);
				if($count == $chargesCount){
					$arServices[$serviceById["NAME"]] = round ( $chargeProportional, 2, PHP_ROUND_HALF_DOWN)+ ($sumTotalFromPayment-$separatedSumTotal);
				}
			}

		}

		if ($paymentID)
		{
			$arServiceDesc = array();

			$arServices2 = http_build_query($arServices);

			$APPLICATION->set_cookie("citrus_tszhpayment_lastpayment", $paymentID, strtotime("+1 year"), "/");
			$redirectURL = $APPLICATION->GetCurPageParam("payment=" . $paymentID . "&services=" . \Bitrix\Main\Web\Json::encode($arServices), Array('payment', 'services'));

			if ($arResult['IS_BUDGET'])
			{
				$payerIdData['PAYMENT_ID'] = $paymentID;
				if (isset($arFields['ACCOUNT_ID']))
				{
					$payerIdData['ACCOUNT_ID'] = $arFields['ACCOUNT_ID'];
				}
				$res = PaymentPayerIdTable::add($payerIdData);
			}

			LocalRedirect($redirectURL);
		}
		else
		{
			$arErrors[] = GetMessage("TSZH_PAYMENT_DO_ERROR_ADDING_PAYMENT");
			if ($ex = $GLOBALS["APPLICATION"]->GetException())
			{
				$arErrors[] = $ex->GetString();
			}
		}

	}
}
elseif (is_set($_GET, "summ") && is_numeric($_GET["summ"]))
{
	$arResult["AMOUNT_TO_SHOW"] = $arResult["FIELDS"][$arParams["VAR"]] = htmlspecialcharsbx($_GET["summ"]);
}

$arResult["MAKE_PAYMENT_URL"] = $arParams["MAKE_PAYMENT_URL"] = $APPLICATION->GetCurPageParam("window=1", array("window", "bxajaxid", "AJAX_CALL"));
$arResult["WINDOW"] = is_set($_REQUEST, "window");

$paymentID = IntVal($_REQUEST['payment']);
if ($paymentID > 0)
{
	$arFilter = array(
		"ID" => $paymentID,
		"USER_ID" => $USER->IsAuthorized() ? $USER->GetID() : false,
	);
	$arPayment = CTszhPayment::GetList(Array(), $arFilter, false, false, array("*", "UF_*"))->GetNext();
	if (!is_array($arPayment))
	{
		ShowError(GetMessage("CITRUS_TSZHPAYMENT_NOT_FOUND"));

		return;
	}
	if ($arPayment["PAY_SYSTEM_ID"] == $arResult["PAY_SYSTEM"]["ID"])
	{
		$arResult["PAYMENT"] = $arPayment;
		if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] != "Y" || $arResult["WINDOW"])
		{
			CTszhPaySystem::InitParamArrays($arResult["PAYMENT"], $arResult["PAY_SYSTEM"]["PARAMS"]);

			$pathToAction = $_SERVER["DOCUMENT_ROOT"] . $arResult["PAY_SYSTEM"]["ACTION_FILE"];

			$pathToAction = str_replace("\\", "/", $pathToAction);
			while (substr($pathToAction, strlen($pathToAction) - 1, 1) == "/")
			{
				$pathToAction = substr($pathToAction, 0, strlen($pathToAction) - 1);
			}

			if (file_exists($pathToAction))
			{
				if (is_dir($pathToAction) && file_exists($pathToAction . "/payment.php"))
				{
					$pathToAction .= "/payment.php";
				}

				$arResult["PAY_SYSTEM"]["PATH_TO_ACTION"] = $pathToAction;
			}
		}
		else
		{
			$arNotes[] = GetMessage("PAYMENT_CREATED");
		}
	}
}

if ($_REQUEST['services']) $arResult["services"] = \Bitrix\Main\Web\Json::decode(iconv('Windows-1251', 'UTF-8',$_REQUEST['services']));


$cashbox = \Otr\Sale\Cashbox\Internals\CashboxTable::getList(
	array(
		'select' => array('ID'),
		'filter' => array(
			'ACTIVE' => 'Y',
		)
	)

);
while ($cashboxIds = $cashbox->fetchAll()) {
	$cashboxId = $cashboxIds;
}
$dbCharge = CTszhCharge::GetList(
	Array("SORT" => "ASC", "ID" => "ASC"),
	Array(
		'ACCOUNT_PERIOD_ID' => $lastPeriod["ID"],
	)
);

if ($cashboxId)
{
	while ($arCharge = $dbCharge->GetNext())
	{
		if ($arCharge["COMPONENT"] != "Y")
		{

			$ar[] = array(
				"SERVICE_NAME_GUID" => $arCharge['SERVICE_NAME'],
				"SERVICE_NAME" => $arCharge['SERVICE_NAME'],
				"SUM_TO_PAY" => $arCharge['DEBT_END'],//($arCharge['SUMM2PAY']>0? $arCharge['SUMM2PAY']:$arCharge['DEBT_END']),
				"GUID" => $arCharge['GUID'],
				"PERIOD_ID"=>$arCharge['ID'],
			);

			$arResult['CHARGES'] = $ar;

		}
	}
}

$arResult['ERROR_MESSAGE'] = count($arErrors) > 0 ? implode('<br />&mdash; ', $arErrors) : false;
$arResult['NOTE_MESSAGE'] = count($arNotes) > 0 ? implode('<br />&mdash; ', $arNotes) : false;

$this->IncludeComponentTemplate();