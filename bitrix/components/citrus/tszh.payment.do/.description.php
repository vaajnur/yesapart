<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("TSZH_PAYMENT_DO_NAME"),
	"DESCRIPTION" => GetMessage("TSZH_PAYMENT_DO_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"SORT" => 35,
	"PATH" => array(
		"ID" => "tszh",
		"NAME" => GetMessage("COMPONENT_ROOT_SECTION"),
		"CHILD" => array(
			"ID" => "tszh_personal",
			"NAME" => GetMessage("COMPONENT_SECTION"),
			"SORT" => 10,
		)
	),
);
?>