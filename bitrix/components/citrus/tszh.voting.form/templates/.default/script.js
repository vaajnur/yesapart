;(function(window, $) {

    $(function () {
        var $button = $('.voting-form .voting-submit');
        var checkSubmit = function() {
            var missingSelection = false;
            $('.voting-form .voting-question').each(function () {
                if ($(this).find('input:checked').length <= 0) {
                    missingSelection = true;
                }
            });

            if (missingSelection) {
                $button.attr('disabled', 'disabled');
            } else {
                $button.removeAttr('disabled');
            }
        };

        $('.voting-form .voting-question input').change(checkSubmit);
        checkSubmit();
    });

})(window, $);