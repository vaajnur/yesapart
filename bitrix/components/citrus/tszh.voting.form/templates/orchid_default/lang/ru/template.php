<?
$MESS ['VOTE_SUBMIT_BUTTON'] = "Голосовать";
$MESS ['VOTE_RESET'] = "Сбросить";
$MESS ['F_CAPTCHA_TITLE'] = "Защита от автоматических сообщений";
$MESS ['F_CAPTCHA_PROMT'] = "Символы на картинке";
$MESS ['COMP_MESSAGE'] = "Голосование по данному опросу прошло успешно.";
$MESS ['ACCESS_DENY'] = "У вас недостаточно прав чтобы участвовать в данном опросе";
$MESS ['CVF_DATES'] = "Время проведения";
$MESS["CVF_EDIT_VOTING"] = "Редактировать голосование";
$MESS["CVF_DELETE_VOTING"] = "Удалить голосование";
$MESS["CVF_EDIT_QUESTION"] = "Редактировать вопрос";
$MESS["CVF_DELETE_QUESTION"] = "Удалить вопрос";
$MESS["CVF_RESULT"] = "Результаты голосования";
?>
