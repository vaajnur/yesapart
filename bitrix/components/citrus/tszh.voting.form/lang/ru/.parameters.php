<?
$MESS ['CITRUS_VOTE_VOTING_ID'] = "Идентификатор опроса";
$MESS ['CITRUS_VOTE_SETTINGS_SECTION'] = "Настройки опроса";
$MESS ['CITRUS_VOTE_GROUP_ID'] = "Идентификатор группы опросов";
$MESS ['CITRUS_VOTE_CACHE_TITLE'] = "Кеширование";
$MESS ['CITRUS_VOTE_VOTE_RESULT_PAGE'] = "URL для страницы с результатами опроса";
$MESS ["CITRUS_VOTE_TSZH_ID"] = "Объект управления";
