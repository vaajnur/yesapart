<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
$this->__component->arResult["TEMPLATE_HTML"] = "";

if (empty($arResult["ITEMS"]))
	return;

if (count($arResult["ITEMS"]) == 1):?>
	<?$APPLICATION->IncludeComponent(
		"citrus:super.component",
		"single",
		Array(
			"ITEMS" => $arResult["ITEMS"],
			"FIELDS" => array("ADDRESS", "PHONE", "PHONE_DISP", "OFFICE_HOURS"),
			"CONTACTS_URL" => $arParams["CONTACTS_URL"],
			"CACHE_TYPE" => "N",
		),
		$this->__component
	);?>
<?else:?>
	<?$APPLICATION->IncludeComponent(
		"citrus:super.component",
		"multi",
		Array(
			"ITEMS" => $arResult["ITEMS"],
			"FIELDS" => array("PHONE", "PHONE_DISP"),
			"CONTACTS_URL" => $arParams["CONTACTS_URL"],
			"CACHE_TYPE" => "N",
		),
		$this->__component
	);?>
<?endif?>