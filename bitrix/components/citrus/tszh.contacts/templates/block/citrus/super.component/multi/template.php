<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if (empty($arParams["ITEMS"]))
	return;

if (!is_array($arParams["FIELDS"]))
	$arParams["FIELDS"] = array($arParams["FIELDS"]);
?>

<link type="text/css" rel="stylesheet" href="<?=$templateFolder?>/styles.css"></link>

<div id="contacts-block-container">
	<?foreach($arParams["ITEMS"] as $itemID => $arItem):?>
		<div class="org-contacts">
			<h3 class="alt"><?=$arItem["NAME"]?></h3>

			<table class="contacts-table">
				<?foreach ($arParams["FIELDS"] as $field):
					$value = trim($arItem[$field]);

					if ($field == "PHONE_DISP")
						continue;
					elseif ($field == "PHONE")
					{
						$arValues = array();
						$pattern = '#^(\+?\d+)\s*\((\d+)\)\s*(.+)$#';
						$replacement = '$1 ($2) <span class="phone-big">$3</span>';
						if (strlen($value) > 0)
						{
							$value = preg_replace($pattern, $replacement, $value);
							$arValues[] = $value;
						}
						$value = trim($arItem["PHONE_DISP"]);
						if (in_array("PHONE_DISP", $arParams["FIELDS"]) && strlen($value) > 0)
						{
							$value = preg_replace($pattern, $replacement, $value);
							$arValues[] = $value . " (" . GetMessage("T_DISPATCHER_ROOM") . ")";
						}
						$value = implode("<br />", $arValues);
					}

					if (strlen($value) <= 0)
						continue;?>
					<tr>
						<td class="caption"><?=GetMessage("T_F_" . $field)?>:</td>
						<td><?=html_entity_decode($value)?></td>
					</tr>
				<?endforeach?>
				<tr>
					<td style="padding: 0;"></td>
					<td class="detail-link"><a href="<?=preg_replace('/#CODE#/', $arItem["CODE"], $arParams["CONTACTS_URL"])?>"><?=GetMessage("T_DETAIL")?></a></td>
				</tr>
			</table>
		</div>
	<?endforeach?>
</div>