<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Citrus\Tszh\Subscribe\CTszhSubscribeTable;
use Citrus\Tszh\Subscribe\CTszhUnsubscriptionTable;

if (!CModule::includeModule("citrus.tszh"))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));
	return;
}

$res = CTszhFunctionalityController::checkSubscribe(true, $isSubscribeEdition);
if (!$isSubscribeEdition)
{
	ShowError(GetMessage("C_ERROR_TSZH_EDITION"));
	return;
}
if (!$res)
	return;

if (!$USER->IsAuthorized()) 
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
	return;
}

if (!CTszh::IsTenant() && !$USER->IsAdmin())
{
	$APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"));
	return;
}

$userId = $USER->getId();

$rsAccounts = CTszhAccount::getList(
	array(),
	array("USER_ID" => $userId),
	false,
	false,
	array("ID", "TSZH_ID")
);
$arTszhIds = array();
while ($arAccount = $rsAccounts->fetch())
{
	$arTszhIds[] = $arAccount["TSZH_ID"];
}

$arTszhIds = array_unique($arTszhIds);
if (empty($arTszhIds) && !$USER->IsAdmin())
{
	$APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"));
	return;
}

$arAllSubscribes = array();
foreach (CTszhSubscribe::getSubscribeClasses(CTszhBaseSubscribe::TYPE_OPTIONAL) as $className)
	$arAllSubscribes[$className::CODE] = $className;

$arDbSubscribeCodes = array();
$rsDbSubscribes = CTszhSubscribeTable::getList(array(
	"select" => array("CODE"),
	"filter" => array("ACTIVE" => "Y", "TSZH_ID" => $arTszhIds, "CODE" => array_keys($arAllSubscribes)),
));
while ($ar = $rsDbSubscribes->fetch())
{
	$arDbSubscribeCodes[] = $ar["CODE"];
}
$arDbSubscribeCodes = array_unique($arDbSubscribeCodes);

$arUnsubscription = array();
if (!empty($arDbSubscribeCodes))
{
	$rsUnsubscription = CTszhUnsubscriptionTable::getList(array(
		"select" => array("SUBSCRIBE_CODE"),
		"filter" => array("USER_ID" => $userId),
	));
	while ($ar = $rsUnsubscription->fetch())
	{
		$arUnsubscription[] = $ar["SUBSCRIBE_CODE"];
	}
}

$arSubscribes = array();
foreach ($arAllSubscribes as $code => $className)
{
	if (!in_array($code, $arDbSubscribeCodes))
		continue;

	$arSubscribes[$code] = array(
		"CLASS" => $className,
		"NAME" => $className::getMessage("NAME"),
		"DESCR" => $className::getMessage("DESCR-PUBLIC"),
		"USER_SUBSCRIBED" => !in_array($code, $arUnsubscription),
	);
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && strlen($_POST["save"]) && $_POST["citrus_tszh_subscription_component"] == "Y" && check_bitrix_sessid())
{
	$arSubscription = array();
	foreach ($arSubscribes as $code => $arSubscribe)
	{
		if (is_set($_POST, $code) && $_POST[$code] == "Y")
			$arSubscription[$code] = true;
		else
			$arSubscription[$code] = false;
	}

	$errMsg = false;
	try
	{
		CTszhSubscribe::setSubscription($userId, $arSubscription);
	}
	catch (Exception $e)
	{
		$errMsg = getMessage("C_ERROR_SET_SUBSCRIPTION", array("#ERROR#" => $e->getMessage()));
	}

	if ($errMsg === false)
	{
		$_SESSION[$this->__name] = getMessage("C_CHANGES_SAVED");
		localRedirect($APPLICATION->getCurUri());
	}
	else
		showError($errMsg);
}

$arResult = array(
	"ITEMS" => $arSubscribes,
	"NOTE" => false,
);

if (CTszhSubscribe::isDummyMail($USER->getEmail()) && ($profileUrl = $USER->getParam("citrus.tszh.personal")))
	$arResult["NOTE"] = getMessage("C_NOTIFY_DUMMY_MAIL", array("#URL#" => "{$profileUrl}profile/"));

$this->IncludeComponentTemplate();
