<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule("vdgb.tszhvote"))
{
	ShowError(GetMessage("TVL_VOTE_MODULE_NOT_INSTALLED"));
	return;
}


$arParams["ALLOW_NEW_ELEMENT"] = $arParams["ALLOW_NEW_ELEMENT"] != "N" ? true : false; 
$arParams["GROUP_ID"] = IntVal($arParams['GROUP_ID']) > 0 ? IntVal($arParams['GROUP_ID']) : 0;

$strBackURLParam = "&backurl=" . rawurlencode($APPLICATION->GetCurPageParam());

$arParams["QUESTIONS_URL"] = trim($arParams["QUESTIONS_URL"]);
if (strlen($arParams["QUESTIONS_URL"]) <= 0)
	$arParams["QUESTIONS_URL"] = "questions.php?ID=#ID#";
$arParams["QUESTIONS_URL"] .= $strBackURLParam;

$arParams["EDIT_URL"] = trim($arParams["EDIT_URL"]);
if (strlen($arParams["EDIT_URL"]) <= 0)
	$arParams["EDIT_URL"] = "form.php?ID=#ID#";
$arParams["EDIT_URL"] .= $strBackURLParam;

$arParams["RESULTS_URL"] = trim($arParams["RESULTS_URL"]);
if (strlen($arParams["RESULTS_URL"]) <= 0)
	$arParams["RESULTS_URL"] = "results.php?VOTING_ID=#VOTING_ID#";
$arParams["RESULTS_URL"] .= $strBackURLParam;


$arResult["GROUP"] = CCitrusPollGroup::GetByID($arParams['GROUP_ID']);
if (!is_array($arResult["GROUP"]))
{
	ShowError(GetMessage("CVL_VOTING_GROUP_NOT_FOUND"));
	return;
}

$arResult["GRID_ID"] = 'citrus_vote_edit';

$arResult["COLUMNS"] = Array(
	array("id"=>"ID", "default"=>true),
	array("id"=>"ACTIVE", "default"=>true, "type" => "checkbox"),
	array("id"=>"DATE_BEGIN", "default"=>true, "type" => "date"),
	array("id"=>"DATE_END", "default"=>true, "type" => "date"),
	array("id"=>"NAME", "default"=>true, "type" => "string"),
	array("id"=>"DETAIL_TEXT", "default"=>false, "type" => "string"),
	array("id"=>"PERCENT_NEEDED", "default"=>false, "type" => "int"),
	array("id"=>"VOTING_METHOD", "default"=>false, "type" => "list", "items" => CCitrusPollMethodBase::GetGridList()),
	array("id"=>"ENTITY_TYPE", "default"=>false, "type" => "list", "items" => CCitrusPollEntityBase::GetGridList()),
	array("id"=>"CREATED_BY", "default"=>false, "type" => "int"),
	array("id"=>"TITLE_TEXT", "default"=>false, "type" => "string"),
	array("id"=>"VOTING_COMPLETED", "default"=>true, "type" => "checkbox"),
			
);
$arResult["FILTER"] = Array();
foreach ($arResult["COLUMNS"] as $key => $arColumn)
{
	$arResult["COLUMNS"][$key]['name'] = $arColumn['name'] = GetMessage("CVL_F_" . $arColumn["id"]);
	if (!array_key_exists('sort', $arColumn))
		$arResult["COLUMNS"][$key]['sort'] = $arResult["COLUMNS"][$key]['id'];
		
	if ($arColumn['type'] == 'checkbox')
	{
		$arResult["FILTER"][] = 
			array("id"=>$arColumn['id'], "name"=>$arColumn['name'], "type"=>'list', "items"=>Array('' => GetMessage("CVL_ALL"), "Y" => GetMessage("CVL_YES"), "N" => GetMessage("CVL_NO")));
	}
	else
	{
		$arResult["FILTER"][] = 
			array("id"=>$arColumn['id'], "name"=>$arColumn['name'], "type"=>$arColumn['type'] == 'int' ? 'number' : $arColumn['type'], "items"=>array_key_exists('items', $arColumn) ? $arColumn['items'] : false);
	}
}

$arResult["SORT_COLUMNS"] = Array();
foreach ($arResult["COLUMNS"] as $arColumn) {
	if (strlen($arColumn['sort']) > 0) {
		$arResult["SORT_COLUMNS"][$arColumn['sort']] = $arColumn['sort'];
	}
}

//�������������� ������ � ����������� ������������ ��� ������ �����
$grid_options = new CGridOptions($arResult["GRID_ID"]);

//����� ���������� �������� ������������ (�������� ��, ��� �� ���������)
$aSort = $grid_options->GetSorting(array("sort"=>array("ID"=>"DESC")));
$arResult["SORT"] = $aSort["sort"];
$arResult["SORT_VARS"] = $aSort["vars"];

//������ �������� � ����������� (�������� ���������)
$aNav = $grid_options->GetNavParams(array("nPageSize"=>10));


// ------------------------------------------------
// BEGIN FILTER
$arFilter = Array(
	"GROUP_ID" => $arResult["GROUP"]["ID"],
);

//������� ������� ������ (�������� ��������� ���� ������)
$aFilter = $grid_options->GetFilter($arResult["FILTER"]);
if(isset($aFilter["DATE_BEGIN_from"]))
{
	$aFilter[">=DATE_BEGIN"] = $aFilter["DATE_BEGIN_from"];
	unset($aFilter["DATE_BEGIN_from"]);
}
if(isset($aFilter["DATE_BEGIN_to"]))
{
	$aFilter["<=DATE_BEGIN"] = $aFilter["DATE_BEGIN_to"];
	unset($aFilter["DATE_BEGIN_to"]);
}
if(isset($aFilter["DATE_END_from"]))
{
	$aFilter[">=DATE_END"] = $aFilter["DATE_END_from"];
	unset($aFilter["DATE_END_from"]);
}
if(isset($aFilter["DATE_END_to"]))
{
	$aFilter["<=DATE_END"] = $aFilter["DATE_END_to"];
	unset($aFilter["DATE_END_to"]);
}
unset($aFilter["DATE_BEGIN_datesel"]);
unset($aFilter["DATE_END_datesel"]);
$arFilter = array_merge($arFilter, $aFilter);
// END FILTER
// ------------------------------------------------

// process actions

$printProtocol = IntVal($_REQUEST['print_protocol']);
if ($printProtocol > 0)
{
	$arrFilter = $arFilter;
	$arrFilter["ID"] = $printProtocol;
	$bFound = CCitrusPoll::GetList(Array(), $arrFilter, Array()) > 0;
	if ($bFound)
	{
		$arTemplates = CCitrusPollReport::GetTemplates();
		$arTpl = array_shift($arTemplates);
		$reportData = CCitrusPollReport::GetData($printProtocol);
		if ($reportData)
		{
			$APPLICATION->RestartBuffer();
			include($arTpl['PATH']);
			die();
		}
	}
	else
	{
		ShowError(GetMessage("TVL_VOTING_NOT_FOUND"));
		return;		
	}
}

if (check_bitrix_sessid() && ($_REQUEST['action_button_' . $arResult["GRID_ID"]] == 'delete'))
{
	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$arFilterDel = $arFilter;
		$arIDs = Array();
		foreach ($_REQUEST['ID'] as $key => $ID)
		{
			$ID = IntVal($ID);
			if ($ID > 0 && !in_array($ID, $arIDs))
				$arIDs[] = $ID;
		}
		$arFilterDel["ID"] = $arIDs;
		$rsElementDel = CCitrusPoll::GetList(Array(), $arFilterDel, false, false, Array("ID"));
		$cnt = 0;
		while ($arElementDel = $rsElementDel->Fetch())
		{
			if (CCitrusPoll::Delete($arElementDel["ID"]))
				$cnt++;
		}
		if ($cnt > 0)
			$_SESSION['citrus.polls.message'] .= GetMessage("CVL_DELETED", Array('#CNT#' => $cnt));
	}
	else
	{
		$arFilterDel = $arFilter;
		$arFilterDel["ID"] = IntVal($_REQUEST['ID']);
		$rsElementDel = CCitrusPoll::GetList(Array(), $arFilterDel, false, false, Array("ID"));
		if ($arElementDel = $rsElementDel->Fetch())
		{
			if (CCitrusPoll::Delete($arElementDel["ID"]))
				$_SESSION['citrus.polls.message'] .= GetMessage("CVL_DELETED", Array('#CNT#' => 1));
		}

		LocalRedirect(strlen($_REQUEST['backurl']) > 0 ? $_REQUEST['backurl'] : $APPLICATION->GetCurPageParam('', Array(
			'action_button_' . $arResult["GRID_ID"],
			'sessid',
			'ID',
		)));
	}
}


$rsPolls = CCitrusPoll::GetList($arResult['SORT'], $arFilter, false, $aNav);

$arResult["DATA"] = Array();
while ($arData = $rsPolls->GetNext(false))
{
	$strEditUrl = CComponentEngine::MakePathFromTemplate($arParams["EDIT_URL"], Array("ID" => $arData['ID']));
	$strResultsUrl = CComponentEngine::MakePathFromTemplate($arParams["RESULTS_URL"], Array("VOTING_ID" => $arData['ID']));
	$strQuestionsUrl = CComponentEngine::MakePathFromTemplate($arParams["QUESTIONS_URL"], Array("ID" => $arData['ID']));
	
	$arDataView = Array(
		"ID" => '<a href="' . $strEditUrl . '"">' . $arData['ID'] . '</a>'
	);
	$ar = CCitrusPollMethodBase::GetGridList();
	if (array_key_exists($arData['VOTING_METHOD'], $ar))
		$arDataView["VOTING_METHOD"] = $ar[$arData['VOTING_METHOD']];
	$ar = CCitrusPollEntityBase::GetGridList();
	if (array_key_exists($arData['ENTITY_TYPE'], $ar))
		$arDataView["ENTITY_TYPE"] = $ar[$arData['ENTITY_TYPE']];
	
	$strDeleteUrl = '?' . bitrix_sessid_get() . "&backurl=" . rawurlencode($APPLICATION->GetCurPageParam('', Array('backurl'))) . '&action_button_' . $arResult["GRID_ID"] . '=delete' . '&ID=' . $arData["ID"];
	
	$arResult["DATA"][] = Array(
		'data' => $arData,
		'columns' => $arDataView,
		'actions' => Array(
/*			Array(
				'TEXT' => GetMessage("TVL_QUESTIONS_ELEMENT"),
				'TITLE' => GetMessage("TVL_QUESTIONS_ELEMENT_TITLE"),
				'DEFAULT' => false,
				'DISABLED' => false,
				"ONCLICK" => "jsUtils.Redirect({}, '$strQuestionsUrl'); return false;",
				'ICONCLASS' => 'list',
			),*/
			Array(
				'TEXT' => GetMessage("TVL_EDIT_ELEMENT"),
				'TITLE' => GetMessage("TVL_EDIT_ELEMENT_TITLE"),
				'DEFAULT' => true,
				'DISABLED' => false,
				"ONCLICK" => "jsUtils.Redirect({}, '$strEditUrl'); return false;",
				'ICONCLASS' => 'edit',
			),
			Array(
				'TEXT' => GetMessage("TVL_RESULTS_ELEMENT"),
				'TITLE' => GetMessage("TVL_RESULTS_ELEMENT_TITLE"),
				'DEFAULT' => false,
				'DISABLED' => false,
				"ONCLICK" => "jsUtils.Redirect({}, '$strResultsUrl'); return false;",
				'ICONCLASS' => 'view',
			),
			Array(
				'TEXT' => GetMessage("TVL_PROTOCOL"),
				'TITLE' => GetMessage("TVL_PROTOCOL_TITLE"),
				'DEFAULT' => false,
				'DISABLED' => false,
				"ONCLICK" => "jsUtils.Redirect({}, '?print_protocol={$arData['ID']}'); return false;",
				'ICONCLASS' => 'view',
			),
			Array(
				'TEXT' => GetMessage('�VL_DELETE'),
				'DEFAULT' => false,
				'DISABLED' => false,
				"ONCLICK" => "if (confirm('" . GetMessage('�VL_CONFIRM_DELETE') . "')) jsUtils.Redirect({}, '$strDeleteUrl'); return false;",
				'ICONCLASS' => 'delete',
			),
		),
	);
}

if (is_array($arResult["GROUP"]))
{
	$arResult["TOOLBAR_ID"] = 'citrus_polls_toolbar';
	$arResult["TOOLBAR_BUTTONS"] = Array(
		array("TEXT" => GetMessage("TVL_TB_SETTINGS"), "ICON" => 'btn-settings', "TITLE" => GetMessage("TVL_TB_SETTINGS_TITLE"), "LINK" => 'javascript:void(0);', "LINK_PARAM" => 'onclick="bxGrid_' . $arResult["GRID_ID"] . '.EditCurrentView()"'),
		array("TEXT" => GetMessage("TVL_TB_VIEWS"), "ICON" => 'btn-list', "TITLE" => GetMessage("TVL_TB_VIEWS_TITLE"), "LINK" => 'javascript:void(0);', "LINK_PARAM" => 'onclick="bxGrid_' . $arResult["GRID_ID"] . '.ShowViews()"'),
	);
	
	if ($arParams["ALLOW_NEW_ELEMENT"])
	{
		$arResult["TOOLBAR_BUTTONS"] = array_merge(Array(
			array("TEXT" => GetMessage("TVL_ADD_VOTING"), "ICON" => 'btn-new', "LINK" => CComponentEngine::MakePathFromTemplate($arParams['EDIT_URL'], Array("ID" => "0"))), 
		), $arResult["TOOLBAR_BUTTONS"]);
	}
	
	$arResult["NAV_OBJECT"] = $rsPolls;
	$arResult["TOTAL_CNT"] = $rsPolls->SelectedRowsCount();
}


if (array_key_exists('citrus.tszh.voting.edit.message', $_SESSION))
{
	$arResult["MESSAGE"] = Array(
		"TYPE" => "OK",
		"MESSAGE" => $_SESSION['citrus.tszh.voting.edit.message'],
	);
	unset($_SESSION['citrus.tszh.voting.edit.message']);
}

$this->IncludeComponentTemplate();

?>