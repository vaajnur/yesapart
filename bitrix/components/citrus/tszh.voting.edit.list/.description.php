<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$arComponentDescription = array(
	"NAME" => GetMessage("C_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("C_COMPONENT_DESC"),
	"ICON" => "/images/menu_ext.gif",
	"PATH" => array(
		"ID" => "citrus",
		"NAME" => GetMessage("C_CITRUS"),
	),
);
?>