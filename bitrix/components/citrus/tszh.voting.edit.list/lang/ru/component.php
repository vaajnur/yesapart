<?$MESS ['CVL_VOTING_GROUP_NOT_FOUND'] = "Группа голосований не найдена. Проверьте настройки компонента.";
$MESS ['CVL_ALL'] = "(все)";$MESS ['CVL_YES'] = "Да";$MESS ['CVL_NO'] = "Нет";$MESS ['CVL_F_ID'] = "ID";$MESS ['CVL_F_DATE_BEGIN'] = "Дата начала";
$MESS ['CVL_F_DATE_END'] = "Дата окончания";$MESS ['CVL_F_NAME'] = "Название";$MESS ['CVL_F_DETAIL_TEXT'] = "Описание";$MESS ['CVL_F_ACTIVE'] = "Активность";$MESS ['CVL_F_PERCENT_NEEDED'] = "Необходимый процент голосов";$MESS ['CVL_F_VOTING_METHOD'] = "Метод подсчета голосов";$MESS ['CVL_F_ENTITY_TYPE'] = "Кто голосует";$MESS ['CVL_F_CREATED_BY'] = "Кем создано";$MESS ['CVL_F_TITLE_TEXT'] = "Тема";$MESS ['CVL_F_VOTING_COMPLETED'] = "Завершено";$MESS ['СVL_DELETE'] = "Удалить";
$MESS ['СVL_CONFIRM_DELETE'] = "Вы действительно хотите удалить это голосование?";$MESS ['CVL_DELETED'] = "Удалено голосований: #CNT#";
$MESS ['TUL_ACCESS_DENIED'] = "Доступ запрещен. Вы не являетесь администратором ТСЖ";$MESS ['TVL_PORTAL_MODULE_NOT_INSTALLED'] = "Модуль «Портал» не установлен.";$MESS ['TVL_VOTE_MODULE_NOT_INSTALLED'] = "Модуль «Опросы/голосования» не установлен.";
$MESS ['TVL_ORG_NOT_FOUND'] = "Организация не найдена.";$MESS ['TVL_VOTING_GROUP_NOT_FOUND'] = "Группа опросов не найдена.";
$MESS ['TVL_EDIT_ELEMENT'] = "Редактировать";$MESS ['TVL_EDIT_ELEMENT_TITLE'] = "Редактировать опрос";$MESS ['TVL_RESULTS_ELEMENT'] = "Результаты";$MESS ['TVL_RESULTS_ELEMENT_TITLE'] = "Результаты опроса";$MESS ['TVL_QUESTIONS_ELEMENT'] = "Вопросы";$MESS ['TVL_QUESTIONS_ELEMENT_TITLE'] = "Список вопросов голосования";$MESS ['TVL_F_ID'] = "ID";$MESS ['TVL_F_LAMP'] = "Инд.";
$MESS ['TVL_F_DATE_START'] = "Начало";
$MESS ['TVL_F_DATE_END'] = "Окончание";$MESS ['TVL_F_ACTIVE'] = "Акт.";$MESS ['TVL_F_C_SORT'] = "Сорт.";$MESS ['TVL_F_TITLE'] = "Заголовок";
$MESS ['TVL_F_QUESTIONS'] = "Вопросов";
$MESS ['TVL_F_COUNTER'] = "Голосов";$MESS ['TVL_LAMP_green'] = "Активен";
$MESS ['TVL_LAMP_red'] = "Не активен";$MESS ['TVL_NEW_QUESTION'] = "Добавить вопрос";
$MESS ['TVL_ADD_VOTING'] = "Новое голосование";$MESS ['TVL_TB_SETTINGS'] = "Настройки";$MESS ['TVL_TB_SETTINGS_TITLE'] = "Настройки списка";$MESS ['TVL_TB_VIEWS'] = "Представление";
$MESS ['TVL_TB_VIEWS_TITLE'] = "Представление списка";$MESS ['TVL_PROTOCOL'] = "Протокол";$MESS ['TVL_PROTOCOL_TITLE'] = "Протокол голосования";
$MESS ['TVL_VOTING_NOT_FOUND'] = "Голосование не найдено";
?>