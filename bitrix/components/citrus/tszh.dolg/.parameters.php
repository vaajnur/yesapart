<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

$arGroups = array();
$rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", Array("ACTIVE" => "Y"));
while ($arGroup = $rsGroups->Fetch())
{
	$arGroups[$arGroup["ID"]] = $arGroup["NAME"];
}

$arReceiptTypes = array();
if (\Bitrix\Main\Loader::includeModule('citrus.tszh'))
{
	$arReceiptTypes = Citrus\Tszh\Types\ReceiptType::getTitles();
}

// ������ �������� ����������
$arTszhList = Array("" => "");
$rsTszhs = CTszh::GetList(Array("ID" => "ASC"), Array("ACTIVE" => "Y"));
while ($arTszh = $rsTszhs->Fetch())
{
	$arTszhList[$arTszh["ID"]] = "[{$arTszh["ID"]}] {$arTszh["NAME"]}";
}

// ������ �����
if ($arCurrentValues["TSZH_ID"])
{
	$rsHouse = \Citrus\Tszh\HouseTable::getList(array(
		'filter' => array(
			'@TSZH_ID' => $arCurrentValues["TSZH_ID"]
		)
	));

	while ($arHouse = $rsHouse->fetch())
	{
		$arHouseList[$arHouse['ID']] = '[' . $arHouse['ID'] . ']' . CTszhAccount::GetFullAddress($arHouse);
	}
}

$arAvailableFields = Array(
	"XML_ID" => GetMessage("FL_XML_ID"),
	"TIMESTAMP_X" => GetMessage("FL_TIMESTAMP_X"),
	"PERIOD" => GetMessage("FL_PERIOD_DATE"),
	"DEBT_BEG" => GetMessage("FL_DEBT_BEG"),
	"DEBT_END" => GetMessage("FL_DEBT_END"),
	"DEBT_END_WITHOUT_CHARGES" => GetMessage("FL_DEBT_END_WITHOUT_CHARGES"),
	"DISTRICT" => GetMessage("FL_DISTRICT"),
	"CITY" => GetMessage("FL_CITY"),
	"SETTLEMENT" => GetMessage("FL_SETTLEMENT"),
	"STREET" => GetMessage("FL_STREET"),
	"HOUSE" => GetMessage("FL_HOUSE"),
	"FLAT" => GetMessage("FL_FLAT"),
	"USER_FULL_ADDRESS" => GetMessage("FL_USER_FULL_ADDRESS"),
);

$arSummFields = Array(
	"DEBT_BEG" => GetMessage("FL_DEBT_BEG"),
	"DEBT_END" => GetMessage("FL_DEBT_END"),
	"DEBT_END_WITHOUT_CHARGES" => GetMessage("FL_DEBT_END_WITHOUT_CHARGES"),
);

$arDefaultFields = Array(
	"PERIOD_DATE",
	"USER_FULL_NAME",
	"USER_FULL_ADDRESS",
	"DEBT_END_WITHOUT_CHARGES",
);

$arComponentParameters = array(
	"GROUPS" => array(
		"ACCESS" => array(
			"NAME" => GetMessage("COMP_GROUP_ACCESS"),
			"SORT" => 700
		),
		"DATA" => array(
			"NAME" => GetMessage("COMP_DATA"),
			"SORT" => 500
		),
	),
	'PARAMETERS' => array(
		"MAX_COUNT" => array(
			"NAME" => GetMessage("COMP_MAX_COUNT"),
			"TYPE" => "STRING",
			"DEFAULT" => '15',
			"PARENT" => "DATA",
		),
		"MIN_DEBT" => array(
			"NAME" => GetMessage("COMP_MIN_DEBT"),
			"TYPE" => "STRING",
			"DEFAULT" => '1000',
			"PARENT" => "DATA",
		),
		"TSZH_ID" => array(
			"PARENT" => "DATA",
			"NAME" => GetMessage("COMP_TSZH_FILTER"),
			"TYPE" => "LIST",
			"MULTIPLE" => "N",
			"VALUES" => $arTszhList,
			"REFRESH" => "Y",
			"DEFAULT" => "",
		),
		"HOUSE_ID" => array(
			"PARENT" => "DATA",
			"NAME" => GetMessage("COMP_HOUSE_FILTER"),
			"TYPE" => "LIST",
			"SIZE" => 5,
			"MULTIPLE" => "Y",
			"VALUES" => $arHouseList,
		),
		"FILTER_NAME" => array(
			"NAME" => GetMessage("COMP_FILTER_NAME"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			"PARENT" => "DATA",
		),
		"GROUP_BY" => array(
			"NAME" => GetMessage("GROUP_BY"),
			"TYPE" => "LIST",
			"DEFAULT" => "",
			"VALUES" => array_merge(Array('' => GetMessage("FL_LIST_NOT_SELECTED")), $arAvailableFields),
			"PARENT" => "DATA",
			"REFRESH" => "Y",
		),
	),
);

$arReceiptTypeParams["RECEIPT_TYPE_ENABLE"] = array(
	"PARENT" => "DATA",
	"NAME" => GetMessage("COMP_RECEIPT_TYPE_ENABLE_FILTER"),
	"TYPE" => "CHECKBOX",
	"REFRESH" => "Y",
	"DEFAULT" => "",
);
if ($arCurrentValues["RECEIPT_TYPE_ENABLE"] === "Y")
{
	$arReceiptTypeParams["RECEIPT_TYPE"] = array(
		"PARENT" => "DATA",
		"NAME" => GetMessage("COMP_RECEIPT_TYPE_FILTER"),
		"TYPE" => "LIST",
		"MULTIPLE" => "Y",
		"VALUES" => $arReceiptTypes,
		"REFRESH" => "N",
		"DEFAULT" => "",
	);
}
$arComponentParameters['PARAMETERS'] = array_merge($arReceiptTypeParams, $arComponentParameters['PARAMETERS']);

if ($arCurrentValues["GROUP_BY"])
{
	$arComponentParameters['PARAMETERS'] = array_merge(
		$arComponentParameters['PARAMETERS'],
		Array(
			"GROUP_BY_SUMMARY" => array(
				"PARENT" => "DATA",
				"NAME" => GetMessage("COMP_GROUP_SUMMARY"),
				"TYPE" => "LIST",
				"REFRESH" => "N",
				"DEFAULT" => '',
				"VALUES" => Array(
					'' => GetMessage("COMP_GROUP_SUMMARY_F_NO"),
					"COUNT" => GetMessage("COMP_GROUP_SUMMARY_F_COUNT"),
					'SUM' => GetMessage("COMP_GROUP_SUMMARY_F_SUM"),
					'AVG' => GetMessage("COMP_GROUP_SUMMARY_F_AVG"),
					'MAX' => GetMessage("COMP_GROUP_SUMMARY_F_MAX"),
					'MIN' => GetMessage("COMP_GROUP_SUMMARY_F_MIN"),
				),
			),
			"GROUP_BY_SUMMARY_FIELD" => array(
				"PARENT" => "DATA",
				"NAME" => GetMessage("COMP_GROUP_SUMMARY_FIELD"),
				"TYPE" => "LIST",
				"ADDITIONAL_VALUES" => "N",
				"VALUES" => $arSummFields,
				"DEFAULT" => "DEBT_END_WITHOUT_CHARGES",
			),
		)
	);
}

$arComponentParameters['PARAMETERS'] = array_merge(
	$arComponentParameters['PARAMETERS'],
	Array(
		"COLS_COUNT" => array(
			"NAME" => GetMessage("COMP_COLS_COUNT"),
			"TYPE" => "STRING",
			"REFRESH" => "Y",
			"DEFAULT" => '4',
			"VALUES" => $arAvailableFields,
			"PARENT" => "DATA",
		),
		"ACCESS_FIELDS" => array(
			"PARENT" => "ACCESS",
			"NAME" => GetMessage("ACCESS_FIELDS"),
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"ADDITIONAL_VALUES" => "N",
			"VALUES" => $arAvailableFields,
			"DEFAULT" => Array(
				"XML_ID",
				"USER_LAST_NAME",
				"USER_NAME",
				"USER_SECOND_NAME",
				"USER_FULL_NAME",
				"USER_FULL_ADDRESS",
			),
		),
		"ACCESS_GROUPS" => array(
			"PARENT" => "ACCESS",
			"NAME" => GetMessage("ACCESS_GROUPS"),
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"ADDITIONAL_VALUES" => "N",
			"VALUES" => $arGroups,
			"DEFAULT" => Array(2),
		)
	)
);

$arCurrentValues["COLS_COUNT"] = IntVal($arCurrentValues["COLS_COUNT"]) > 0 ? IntVal($arCurrentValues["COLS_COUNT"]) : 4;
for ($i = 0; $i < $arCurrentValues["COLS_COUNT"]; $i++)
{
	$arComponentParameters["PARAMETERS"]["COL_$i"] = array(
		"NAME" => str_replace("#I#", $i + 1, str_replace("#N#", $i + 1, GetMessage("C_COMP_DOLG_COL"))),
		"TYPE" => "LIST",
		"DEFAULT" => array_key_exists($i, $arDefaultFields) ? $arDefaultFields[$i] : '',
		"VALUES" => $arAvailableFields,
		"PARENT" => "DATA",
	);
}

if (CModule::IncludeModule("iblock"))
{
	CIBlockParameters::AddPagerSettings($arComponentParameters, GetMessage("COMP_NAV_PAGER"), true, true);
}

?>