<?
$MESS["COMP_ERROR_TSZH_NOT_FOUND"] = "Не знайденоТСЖ для поточного сайту.";
$MESS["FL_TIMESTAMP_X"] = "Дата";
$MESS["FL_PERIOD_DATE"] = "Період";
$MESS["FL_USER_LAST_NAME"] = "Прізвище";
$MESS["FL_USER_NAME"] = "Ім'я";
$MESS["FL_USER_SECOND_NAME"] = "По-батькові";
$MESS["FL_DEBT_BEG"] = "Заборгованість на початок періоду";
$MESS["FL_DEBT_END"] = "Заборгованість на кінець періоду";
$MESS["FL_DEBT_END_WITHOUT_CHARGES"] = "Заборгованість на кінець періоду за вирахуванням нарахувань за період";
$MESS["FL_DISTRICT"] = "Район";
$MESS["FL_CITY"] = "Місто";
$MESS["FL_SETTLEMENT"] = "Населений пункт";
$MESS["FL_STREET"] = "Вулиця";
$MESS["FL_HOUSE"] = "Будинок";
$MESS["FL_FLAT"] = "Квартира";
$MESS["FL_USER_FULL_NAME"] = "ПІП";
$MESS["FL_USER_FULL_ADDRESS"] = "Адреса";
$MESS["COMP_COLS_COUNT"] = "Кількість стовпців таблиці";
$MESS["COMP_MAX_COUNT"] = "Максимальна кількість рядків, що виводяться";
$MESS["TSZH_MODULE_NOT_FOUND_ERROR"] = "Модуль 1С: Сайт ЖКГ не встановлений.";
$MESS["FL_XML_ID"] = "Номер о/р";
$MESS["COMP_GROUP_SUMMARY_F_COUNT"] = "кількість";
$MESS["COMP_GROUP_SUMMARY_F_SUM"] = "сума";
$MESS["COMP_GROUP_SUMMARY_F_AVG"] = "середнє";
$MESS["COMP_GROUP_SUMMARY_F_MAX"] = "максимальне";
$MESS["COMP_GROUP_SUMMARY_F_MIN"] = "мінімальне";
?>