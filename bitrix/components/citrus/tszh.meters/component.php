<?
/**
 * ������ ���ƻ
 * ��������� ������ ��������� (citrus:tszh.meters)
 * ������� c����� ��������� �������� ������������ � � ����� ������� ��������� ���������.
 * @package tszh
 */
/** @var CBitrixComponent $this */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
    die();
}

if (!CModule::IncludeModule('citrus.tszh'))
{
    ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));

    return;
}

if (!CTszhFunctionalityController::CheckEdition())
{
    return;
}

$arParams["DONT_CHECK_PREV_VALUE"] = $arParams["DONT_CHECK_PREV_VALUE"] == "Y";
$arParams["ALLOW_ZERO_VALUES"] = $arParams["ALLOW_ZERO_VALUES"] == "Y";
//$arParams["COUNT_COLUMN"] = intval($arParams["COUNT_COLUMN"]); //���������� �������� � ������� ���������
//var_dump($arParams["COUNT_COLUMN"]);
if ($_SERVER["REQUEST_METHOD"] == 'POST' && $_POST['action'] == 'set_meters' && check_bitrix_sessid())
{
    $this->ClearResultCache();
}

global $USER;
// ���� ������������ �� �����������, ������� ����� �����������
if (!$USER->IsAuthorized())
{
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

// ��������� ������ ���������� - ���������� � ������ ����������
$arResult = Array();
// ������� ������ � ������ �������� ������������
$rsUser = $USER->GetByID($USER->GetID());
$arResult['USER'] = $rsUser->GetNext();

// ���� ������������ �� ����������� � ������ ���������� ��ƻ
// ������� ��������� � �������� ������ ����������
if (!CTszh::IsTenant() && !$USER->IsAdmin())
{
    $this->AbortResultCache();
    $APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"));

    return;
}

// $arResult['ITEMS'] ����� ��������� ������ ��������� � �����������/�������� �����������
$arResult['ITEMS'] = Array();

// ������� ���� �������� ������������
$arResult['ACCOUNT'] = null;

$rsAccount = CTszhAccount::GetList(
	array("CURRENT" => "DESC"),
	array("USER_ID" => $USER->GetID())
);

while ($arAccount = $rsAccount->Fetch())
{
	if (is_null($arResult['ACCOUNT']) || ($arAccount["CURRENT"] == "Y"))
	{
		$arResult['ACCOUNT'] = $arAccount;
	}
}

if (!is_array($arResult['ACCOUNT']) && !$USER->IsAdmin())
{
    $this->AbortResultCache();
    $APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"));

    return;
}

// ��������� ������ �������� ������������
$obLastPeriod = CTszhAccountPeriod::GetList(
                Array("PERIOD_DATE" => "DESC", "PERIOD_ID" => "DESC"),
                Array(
            'ACCOUNT_ID'     => $arResult["ACCOUNT"]["ID"],
            "PERIOD_TSZH_ID" => $arResult["ACCOUNT"]["TSZH_ID"],
                ), false, Array('nTopCount' => 1)
);
$arResult["ACCOUNT_PERIOD"] = $obLastPeriod->GetNext();
$arResult["ACCOUNT_PERIOD"] = is_array($arResult["ACCOUNT_PERIOD"]) ? $arResult["ACCOUNT_PERIOD"] : false;
/* if (!is_array($arResult["ACCOUNT_PERIOD"])) {
  $this->AbortResultCache();
  $APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"));
  return;
  } */

// ��������� ������ ��������� �������� ������������
$dbResult = CTszhMeter::GetList(Array('SORT' => 'ASC', 'NAME' => 'ASC'),
                Array(
            "ACCOUNT_ID"  => $arResult['ACCOUNT']['ID'],
            "ACTIVE"      => "Y",
            "HOUSE_METER" => "N"
        ));
while ($arMeter = $dbResult->GetNext(true, false))
{
    $arResult['ITEMS'][] = $arMeter;
}
$timeMonth = $timeNextMonth = time();

// ��������� �� ������������ �������/�������������� ������� ��������� ��������� (�������� �� ������� ���� � ������, � ������� �������� ���� ���������)
$arResult['allow_edit_by_period'] = $USER->IsAdmin() ? true : CTszh::IsMeterValuesInputEnabled($arResult["ACCOUNT"]["TSZH_ID"]);

// ��������� �� ������������ �������/�������������� ������� ��������� ���������
$arResult['allow_edit'] = count($arResult['ITEMS']) > 0 && CTszhMeter::CanPostMeterValues() && $arResult['allow_edit_by_period'];

$arResult['ERROR_MESSAGES'] = '';

foreach ($arResult['ITEMS'] as $key => $arMeter)
{
    $rsMeterValue = CTszhMeterValue::GetList(
                    Array('TIMESTAMP_X' => 'DESC', "ID" => "DESC"),
                    Array(
                'METER_ID' => $arMeter['ID'],
                    ), false, Array('nTopCount' => 1)
    );
    $rsMeterValueBefore = CTszhMeterValue::GetList(
                    Array('TIMESTAMP_X' => 'DESC', "ID" => "DESC"),
                    Array(
                'METER_ID'          => $arMeter['ID'],
                '<TIMESTAMP_X'      => ConvertTimeStamp($timeMonth, "FULL"),
                'MODIFIED_BY_OWNER' => "N",
                    ), false, Array('nTopCount' => 1)
    );
    $arResult['ITEMS'][$key]['VALUE'] = $rsMeterValue->GetNext(true, false);
    $arResult['ITEMS'][$key]['PREV_VALUE'] = $rsMeterValueBefore->GetNext(true,
            false);
}

$arResult["WARNING"] = array();
$arResult["HIDDEN_FIELDS"] = array("action" => "set_meters");

// ���������� ��������� ��������� � �����
if ($_SERVER["REQUEST_METHOD"] == 'POST' && $_POST['action'] == 'set_meters' && check_bitrix_sessid())
{

    if ($arResult['allow_edit'])
    {

        // ID				- ID ������
        // METER_ID			- ID ��������
        // VALUE1			- ��������� �������� �� ������ 1
        // VALUE2			- ��������� �������� �� ������ 2
        // VALUE3			- ��������� �������� �� ������ 3
        // MODIFIED_BY		- ID ������������, ����������� ���������
        // TIMESTAMP_X		- ���� ��������� ���������
        //CTszhMeterValue::Add()

        $arUpdateValues = Array();

        foreach ($arResult['ITEMS'] as $key => $arMeter)
        {

            $id = $arMeter['ID'];

            $tariff_name = strlen($arMeter['SERVICE_NAME']) > 0 ? $arMeter['SERVICE_NAME'] : $arMeter['NAME'];
            $tariff_kod = $arMeter['ID'];
            unset($arResult['ERROR_MESSAGES']);
            unset($arResult["WARNING"]);
            for ($i = 0; $i < min($arMeter['VALUES_COUNT'], 3); $i++)
            {
                $val = $_POST['indiccur'.($i + 1)][$id];
                $prevVal = $arResult['ITEMS'][$key]['PREV_VALUE']['VALUE'.($i + 1)];
                try
                {
                    if (!is_numeric($val))
                    {
                        throw new Exception(GetMessage("CITRUS_TSZH_METER_INCORRECT_VALUE"),
                        0);
                    }
                    if (!($arParams["ALLOW_ZERO_VALUES"] ? $val >= 0 : $val > 0))
                    {
                        throw new Exception($arParams["ALLOW_ZERO_VALUES"] ? GetMessage("CITRUS_TSZH_METER_VALUE_MUST_BE_NOT_NEGATIVE") : GetMessage("CITRUS_TSZH_METER_VALUE_MUST_BE_POSITIVE"),
                        1);
                    }
                    if (!$arParams["DONT_CHECK_PREV_VALUE"] && $val < $prevVal)
                    {
                        throw new Exception(GetMessage("CITRUS_TSZH_METER_MUST_BE_LARGER"),
                        2);
                    }
                }
                catch (Exception $e)
                {
                    /** @var boolean $zeroCrossing true ���� ��������� ��������� ����� ���� ����������� ��������� ��������� � ����� �������� �������������� ������ ������, �������� ���� ����� ������������� */
                    $zeroCrossing = false;
                    /**
                     * ������������ ������� �������� ����� 0, ��� � � 1�.
                     * ����� ��������� ������� ��������� ������ ���������� ������ � ���� ������:
                     *
                     *    [�����������] > 0
                     *    � [���� ���������] / [���� ����� �����������] >= 0.9
                     *    � [��� ���������] / [���� ����� �����������] < 0.9
                     */
                    if ($e->getCode() > 0 && $arMeter["CAPACITY"] > 0)
                    {
                        $maxValue = doubleval(str_repeat('9',
                                        $arMeter["CAPACITY"]));
                        if ($prevVal / $maxValue >= 0.9 && $val / $maxValue < 0.9)
                        {
                            $zeroCrossing = true;
                        }
                    }

                    // ������ �� ���� ������� �������� � �������� ������ ����������
                    // ��� ��������� � ������������� ����������� �������� � ��� ��������� � ��� ���� ���������� ���������
                    // ����� ������� �������������� �� ���������
                    if ($zeroCrossing)
                    {
                        if (!$_REQUEST["warning_confirmed"])
                        {
                            $arResult["WARNING"][] = str_replace(
                                    Array('#METER_NAME#', '#N#'),
                                    Array($tariff_name, $i + 1),
                                    $e->getMessage()
                            );
                            $arResult["HIDDEN_FIELDS"]["warning_confirmed"] = 1;
                        }
                    }
                    else
                    {
                        $arResult['ERROR_MESSAGES'] .= str_replace(
                                Array('#METER_NAME#', '#N#'),
                                Array($tariff_name, $i + 1), $e->getMessage()
                        );
                        $arResult['ITEMS'][$key]['VALUE']['VALUE'.($i + 1)] = htmlspecialcharsbx($val);
                        continue;
                    }
                }
                $arResult['ITEMS'][$key]['VALUE']['VALUE'.($i + 1)] = floatval($val);
            }

            // ��������! � ������� � �������������� �� ����� ��������, � ��� ������������� �� �� ��� �� ���������!
            if ($_REQUEST["warning_confirmed"] && $arUpdateValues !== $_SESSION['citrus.tszh.meters.warning.values'])
            {
                $arResult['ERROR_MESSAGES'] .= GetMessage("CITRUS_TSZH_METERS_ERORR_CHANGED_CONFIRED_VALUES");
                unset($_SESSION['citrus.tszh.meters.warning.values']);
            }
            if ($arResult['ERROR_MESSAGES'])
            {
                ShowMessage($arResult['ERROR_MESSAGES']);
            }
            elseif (is_array($arResult["WARNING"]) && !empty($arResult["WARNING"]))
            {
                $_SESSION['citrus.tszh.meters.warning.values'] = $arUpdateValues;

                $arResult["WARNING"][] = GetMessage("CITRUS_TSZH_METER_VALUE_CAPACITY_WARNING");
                CJsCore::Init(Array("jquery"));

                ?>
                <div class="meters-component-error-block">
                    <?= ShowError(implode("",
                                    $arResult["WARNING"]))

                    ?>
                </div>
                <script>
                    $(function () {
                        var $metersForm = $('input[type=hidden][name=action][value=set_meters]').parents('form');
                        if ($metersForm && !$metersForm.data('version')) {
                            $metersForm.prepend('<input type="hidden" name="warning_confirmed" value="1">');
                        } else
                            $('.meters-component-error-block').remove();
                    })
                </script>
                <?
            }
            else
            {
				$arUpdateValue = Array(
					'METER_ID' => $id,
					'VALUE1'   => $arResult['ITEMS'][$key]['VALUE']['VALUE1'],
					'VALUE2'   => $arResult['ITEMS'][$key]['VALUE']['VALUE2'],
					'VALUE3'   => $arResult['ITEMS'][$key]['VALUE']['VALUE3'],
				);

                ShowMessage(Array(
                    'TYPE'    => 'OK',
                    'MESSAGE' => $tariff_name.GetMessage("TSZH_METER_VALUES_SAVED"),
                ));

                // ���������� ������� ��������� ��������� ������������
                CTszhMeterValue::Add($arUpdateValue);

                //CTszhMeters::Update($arResult['USER']['ID'], $arFields);
            }
        }
    }
}
use \Bitrix\Main\Application;

$app = Application::getInstance();
$request = $app->getContext()->getRequest();

// ������ ��� ������ �������
$page = $request->get("type");
$id = $request->get("id");
if ($page != 'history' || IntVal($id) <= 0)
{
    $page = '';
}

// ������ ��� �������� ����� �����������
$arResult["INPUT_URL"] = $APPLICATION->GetCurPage();
$arResult["HISTORY_URL"] = $APPLICATION->GetCurPageParam("type=history");
$arResult["CURRENT_PAGE"] = $page;

// �������� ������� ���������� ��������, ���� �� ������������ ������� �� ���������
if ($arParams["TABLE"] == "1")
{
    $arResult["SORT"] = $arParams["COUNT_COLUMN"];
}

// ����������� ������� ����������
$this->IncludeComponentTemplate();

