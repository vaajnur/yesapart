<?
$MESS["TSZH_DONT_CHECK_PREV_VALUE"] = "Дозволяти вводити показання менше попередніх";
$MESS["CITRUS_TSZH_METERS_ALLOW_ZERO_VALUES"] = "Дозволяти вводити нульові показання";
$MESS["CITRUS_TSZH_METERS_METERS_HISTORY"] = "Історія показань лічильників";
$MESS["CITRUS_TSZH_NAV_PAGER"] = "Показання";
$MESS["CITRUS_TSZH_MODIFIED_BY_OWNER"] = "Відображати показання ";
$MESS["CITRUS_ALL"] = "( усі)";
$MESS["CITRUS_MODIFIED_BY_OWNER_Y"] = "введені власником особового рахунку";
$MESS["CITRUS_MODIFIED_BY_OWNER_N"] = "заповнені іншими ( з 1С , адміністраторами )";
$MESS["CITRUS_TSZH_FILTER_NAME"] = "Ім'я змінної, що містить фільтр";
?>