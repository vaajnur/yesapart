<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

$arAvailableFields = Array(
	// �������� ��������
	"NAME" => GetMessage("CITRUS_TSZH_TABLE_DATA_NAME"),
	//�������� ������
	"SERVICE_NAME" => GetMessage("CITRUS_TSZH_TABLE_DATA_SERVICE_NAME"),
	// ��������� ����� ��������
	"NUM" => GetMessage("CITRUS_TSZH_TABLE_DATA_NUM"),
	//�����
	"VALUES_COUNT" => GetMessage("CITRUS_TSZH_TABLE_DATA_VALUES_COUNT"),
	// ���� ��������
	"VERIFICATION_DATE" => GetMessage("CITRUS_TSZH_TABLE_DATA_VERIFICATION"),
	// �������
	"METER_HISTORY" => GetMessage("CITRUS_TSZH_TABLE_DATA_HISTORY"),
);

$arComponentParameters = array(
	"GROUPS" => array(
		"METERS_HISTORY" => array(
			"NAME" => GetMessage("CITRUS_TSZH_METERS_METERS_HISTORY"),
			"SORT" => 700,
		),
		"TABLE_DATA" => array(
			"NAME" => GetMessage("CITRUS_TSZH_TABLE_DATA"),
			"SORT" => 600,
		),
	),
	"PARAMETERS" => array(
		"DONT_CHECK_PREV_VALUE" => Array(
			"NAME" => GetMessage("TSZH_DONT_CHECK_PREV_VALUE"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"ALLOW_ZERO_VALUES" => Array(
			"NAME" => GetMessage("CITRUS_TSZH_METERS_ALLOW_ZERO_VALUES"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"CACHE_TIME" => Array("DEFAULT" => 300),
		"MODIFIED_BY_OWNER" => Array(
			"PARENT" => "METERS_HISTORY",
			"NAME" => GetMessage("CITRUS_TSZH_MODIFIED_BY_OWNER"),
			"TYPE" => "LIST",
			"DEFAULT" => "Y",
			"VALUES" => Array(
				'' => GetMessage("CITRUS_ALL"),
				'Y' => GetMessage("CITRUS_MODIFIED_BY_OWNER_Y"),
				'N' => GetMessage("CITRUS_MODIFIED_BY_OWNER_N"),
			),
		),
		"COUNT_METERS_HISTORY" => Array(
			"PARENT" => "METERS_HISTORY",
			"NAME" => GetMessage("CITRUS_TSZH_COUNT_METERS_HISTORY"),
			"TYPE" => "LIST",
			"DEFAULT" => "6",
			"VALUES" => Array(
				"3" => 3,
				"6" => 6,
				"12" => 12,
			),
		),
		"FILTER_NAME" => array(
			"PARENT" => "METERS_HISTORY",
			"NAME" => GetMessage("CITRUS_TSZH_FILTER_NAME"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"TABLE" => array(
			"PARENT" => "TABLE_DATA",
			"NAME" => GetMessage("CITRUS_TSZH_TABLE_PARAM"),
			"REFRESH" => "Y",
			"TYPE" => "LIST",
			"SIZE" => 1,
			"ADDITIONAL_VALUES" => "N",
			"MULTIPLE" => "N",
			"DEFAULT" => "0",
			"VALUES" => array(
				'0' => GetMessage("CITRUS_TSZH_NO_SORT"),
				'1' => GetMessage("CITRUS_TSZH_MANUAL_SORT"),
			),
		),
	),
);

if ($arCurrentValues["TABLE"] == '1')
{
	$arComponentParameters["PARAMETERS"]["COUNT_COLUMN"] = array(
		"PARENT" => "TABLE_DATA",
		"NAME" => GetMessage("CITRUS_TSZH_TABLE_COUNT_COLUMN"),
		"TYPE" => "LIST",
		"SIZE" => 6,
		"ADDITIONAL_VALUES" => "N",
		"MULTIPLE" => "Y",
		"DEFAULT" => "0",
		"VALUES" => $arAvailableFields,
	);
}
?>
