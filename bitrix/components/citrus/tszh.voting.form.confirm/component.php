<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if (!CModule::IncludeModule("vdgb.tszhvote"))
{
    ShowError(GetMessage("VOTE_MODULE_IS_NOT_INSTALLED"));
    return;
}

//�����

if (!empty($arParams['form_id']))
    $arResult['form_id'] = $arParams['form_id'];
else
{
    ShowError(GetMessage("FORM_ID_EMPTY"));
    return;
}

$arResult['option']          = COption::GetOptionInt("vdgb.tszhvote",
                                                     "sendToWay", 0, FALSE);
$arResult['count_read_code'] = COption::GetOptionInt("vdgb.tszhvote",
                                                     "sendToWay.count", 1,
                                                     FALSE, FALSE);

// ������� ������������
$arUser = CUser::GetByID(CUser::GetID());
$arUser = $arUser->Fetch();
//��� ������������� ��������� ���, ���������� �� ������������������ �������� ����
$arResult['email'] = $arUser['EMAIL'];

//� ����������� �� ����� �������� ���������� ������   
if ($arResult['option'] == 1)
{
    $dummy = CTszhSubscribe::isDummyMail($arUser['EMAIL']);

    if (!$dummy)
        $arResult["contact"] = $arUser['EMAIL'];
}
else if ($arResult['option'] == 2)
{
    if (!empty($arUser['PERSONAL_PHONE']))
        $arResult["contact"] = $arUser['PERSONAL_PHONE'];
}
else
    $arResult["contact"] = false;


$this->IncludeComponentTemplate();

