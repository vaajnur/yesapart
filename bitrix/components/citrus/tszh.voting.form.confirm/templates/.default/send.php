<?

/*
 * ���������� ����������� ��������� �����
 */

require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");

//��������� ������� ����� 1 email/ 2 �������
$opt = COption::GetOptionInt("vdgb.tszhvote", "sendToWay", 1, FALSE);

/*
 * ��� �������������� � ������ �������
 */

function sendAdminAlert($contact, $code)
{
    $arEventFields = array(
        "SUBJECT" => 'Failed send to user',
        "TOEMAIL" => '#DEFAULT_EMAIL_FROM#',
        "CODE"    => $code.' TO: '.$contact,
    );

    return CEvent::SendImmediate('voteNot', SITE_ID, $arEventFields, "N", "");
}

//���������� ����������� ������
function check_phone($valid)
{
    $return = false;
    $len    = strlen($valid);

    if (($valid[0] == '+') && ($valid[1] == '7'))
        $return = true;
    if (($valid[0] == '8') || ($valid[0] == '7'))
        $return = true;

    $count = 0;
    for ($i = 0; $i < $len; $i++)
    {
        if (($valid[$i] >= '0') && ($valid[$i] <= '9'))
            $count++;
        else if (($i > 0 ) && (($valid[$i] != ' ') && ($valid[$i] != '-') ))
            $return = false;
    }

    if ($count != 11)
        $return = false;
    if ((strpos($valid, '  ') != false) || (strpos($valid, '--') != false))
        $return = false;
    if ((strpos($valid, ' -') != false) || (strpos($valid, '- ') != false))
        $return = false;

    return $return;
}

//����� ���������� 
function GetSendMessage($code, $to, $flag)
{
    $subject = '��� �������������';
    $message = '';

    if ($flag == 1)
    {
        $message = $code;
    }
    if ($flag == 2)
    {
        $message = $subject.': '.$code;
		//����� ��������� � UTF-8, ���� ������ ��������� �����
        if (!defined('BX_UTF') || BX_UTF !== true)
            $message = iconv('windows-1251', 'UTF-8', $message);
    }

    return $message;
}

//�������� �� ����������� �����
function SendMail($code, $mail)
{
    $arEventFields = array(
        "SUBJECT" => SITE_SERVER_NAME,
        "TOEMAIL" => $mail,
        "CODE"    => GetSendMessage($code, $mail, 1),
    );

//����������� �������� ���������
    $return = CEvent::SendImmediate('voteNot', SITE_ID, $arEventFields, "N", "");

    if ($return == false)
        sendAdminAlert($mail, $code);

//������ �������� � ���
    CEventLog::Log("WARNING", "VOTING_CONFIRM", "vdgb.tszhvote",
                   'vdgb.tszhvote.confirm',
                   "SEND_EMAIl: ".$return.'; TO: '.$mail.'; CODE:'.$code, false);

    return $return;
}

//�������� ��� ����� ������������
function SendSmsMsg($code, $phone)
{
    $server        = 'http://gateway.api.sc/rest/';
    $login         = COption::GetOptionString("vdgb.tszhvote",
                                              "sendToWay.login", "", FALSE);
    $password      = COption::GetOptionString("vdgb.tszhvote", "sendToWay.pass",
                                              "", FALSE);
    $sourceAddress = COption::GetOptionString("vdgb.tszhvote",
                                              "sendToWay.source", "", FALSE);
    $session       = GetSessionId_Post($server, $login, $password);

    $destinationAddress = preg_replace('~[^0-9]~', '', $phone);
    if ($destinationAddress[0] == '8')
        $destinationAddress[0] == '7';

    $data     = GetSendMessage($code, $phone, 2);
    $validity = 5; // ����� ����� � �������

    $send_sms = SendSms($server, $session, $sourceAddress, $destinationAddress,
                        $data, $validity, $sendDate='');

    $return = empty($send_sms['Code']);

    if ($return)
        $send_sms['Code'] = 'Y'; // ��� ������ � ���
    else
        sendAdminAlert($phone, $code); // � ������ ��������� ��������, ���������� ��������� ������ �� �����

    CEventLog::Log("WARNING", "VOTING_CONFIRM", "vdgb.tszhvote",
                   'vdgb.tszhvote.confirm',
                   "SEND_SMS: ".$send_sms['Code'].' '.$send_sms['Desc'].'; TO: '.$phone.'; CODE:'.$code,
                   false);


    return $return;
}

//==============================================================================
/*
 *  ��� ������ � �������� �� ������������
 */
// ������� ������������ � �������� post-������� �� ������ ����� cURL
function PostConnect($src, $href)
{
    $ch     = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER,
                array('Content-type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CRLF, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $src);
    curl_setopt($ch, CURLOPT_URL, $href);
    $result = curl_exec($ch);
    return $result;
    curl_close($ch);
}

//��������� ������
function GetSessionId_Post($server, $login, $password)
{
    $href   = $server.'Session/session.php';
    $src    = 'login='.$login.'&password='.$password;
    $result = PostConnect($src, $href);
    return json_decode($result, true);
}

//�������� ���
function SendSms($server, $session, $sourceAddress, $destinationAddress, $data,
        $validity, $sendDate = '')
{
    $href     = $server.'Send/SendSms/';
    if ($sendDate != '')
        $sendDate = '&sendDate='.$sendDate;
    $src      = 'sessionId='.$session.'&sourceAddress='.$sourceAddress.'&destinationAddress='.$destinationAddress.'&data='.$data.'&validity='.$validity.$sendDate;
    $result   = PostConnect($src, $href);
    return json_decode($result, true);
}

/*
 * ����� 
 */
//==============================================================================
//�������� ��������
if (isset($_POST['valid']) && $_POST['valid'] != '')
{
    if ($opt == 1)
        $result['valid'] = check_email($_POST['valid']);
    if ($opt == 2)
        $result['valid'] = check_phone($_POST['valid']);

    echo json_encode($result);
}

//��������
if ((isset($_POST['contact']) && $_POST['contact'] != '') && ($_POST['send'] != ''))
{
//���������� ��� � ���������� � ������
    $code             = mt_rand(1000, 9999);
    session_start();
    $_SESSION['CODE'] = md5($code);

// email/������� ����������
    $to = $_POST['contact'];

//� ����������� �� �����
    if ($opt == 1)
    {   //�������� mail
        $result['sendFlag'] = SendMail($code, $to);
    }
    if ($opt == 2)
    {   //�������� sms
        if ($_POST['smstoemail'] == 1) // ���� �� ���������� ��������� ���
            $result['sendFlag'] = SendMail($code, $to);
        else
            $result['sendFlag'] = SendSmsMsg($code, $to);
    }

    echo json_encode($result);
}

//�������� ����
if ((isset($_POST['code']) && $_POST['code'] != ''))
{   //��������� ��������� �� ������
    $code              = $_SESSION['CODE'];
//���������� ��������
    if (md5($_POST['code']) == $_SESSION['CODE'])
        $result['confirm'] = TRUE;
    else
        $result['confirm'] = FALSE;

    echo json_encode($result);
}
?>
