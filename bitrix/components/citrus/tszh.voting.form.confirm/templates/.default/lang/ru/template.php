<?

$MESS["CONFIRM_TITLE"]                   = "Подтвердите ваш выбор";
$MESS["CONFIRM_CONTACT_EMAIL"]           = "Введите ваш адрес электронной почты, на который будет отправлен код подверждения.";
$MESS["CONFIRM_CONTACT_PHONE"]           = "Введите ваш телефон, на который будет отправлен код подверждения.";
$MESS["CONFIRM_CANCEL"]                  = "Отмена";
$MESS["CONFIRM_SEND"]                    = "Получить код";
$MESS["CONFIRM_SEND_TRUE"]               = "Код отправлен!";
$MESS["CONFIRM_SEND_TRUE_COMMENT"] = "Код подтверждения отправлен.";
$MESS["CONFIRM_SEND_TRUE_INPUT"]         = "Введите, пожалуйста, полученный код ниже:";
$MESS["CONFIRM_SEND_CONFIRM"]            = "Подтвердить";
$MESS["CONFIRM_SEND_FALSE"]              = "Отправить новый код";
$MESS["CONFIRM_TRUE"]                    = "Спасибо!";
$MESS["CONFIRM_TRUE_VOTE"]               = "Ваш голос принят!";
$MESS["CONFIRM_CODE"]                    = "Код";
$MESS["CONFIRM_ADR"]                     = "Адрес";
$MESS["CONFIRM_FALSE"]                   = 'У Вас отключён JavaScript...';
$MESS["CONFIRM_EQ"]                      = "Например";
$MESS["CONFIRM_FLASE"]                   = "Ошибка при отправке сообщения.";
$MESS["CONFIRM_CONTACT"]                 = "Но мы уже об этом знаем.";
$MESS["CONFIRM_RE"]                      = "Попробуйте еще раз или дождитесь ответа администратора.";
$MESS["CONFIRM_SMSTOEMAIl"]              = "Отправить новый код на зарегистрированный почтовый ящик";
$MESS["CONFIRM_PHONE_VALID"]             = "+7 999 999-99-99, 8-999-999-99-99, 7 999 99 99 999 и т.п.";
$MESS["CONFIRM_RECODE"]                  = "Осталось попыток:";
$MESS["CONFIRM_ERROR_READ"]              = "Превышено количество попыток ввода кода.";
$MESS["CONFIRM_ERROR_INPUT"]             = "Ошибка";
?>
