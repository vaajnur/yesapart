<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
if (!CModule::IncludeModule("citrus.tszhpayment"))
{
	ShowError(GetMessage("TSZH_PAYMENT_MODULE_NOT_INSTALLED"));

	return;
}

$arTszhs = Array("0" => GetMessage("P_CHOOSE_TSZH"));
$rsTszhs = CTszh::GetList(Array("ID" => "ASC"), Array("ACTIVE" => "Y"));
while ($arTszh = $rsTszhs->Fetch())
{
	$arTszhs[$arTszh["ID"]] = "[{$arTszh["ID"]}] {$arTszh["NAME"]}";
}

$arPayment = array("0" => GetMessage("P_CHOOSE_SYSTEM"));
$rsPayment = CTszhPaySystem::GetList(Array("SORT" => "ASC"), $arFilter);


while ($arPayments = $rsPayment->GetNext())
{
	$arPayment[$arPayments["ID"]] = "[{$arPayments["ID"]}] {$arPayments["NAME"]}";
}

$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"AJAX_MODE" => array(),
		"VARIABLE_ALIASES" => Array(
			"payment" => Array("NAME" => GetMessage("CITRUS_TSZH_PAYMENT_ID")),
		),
		"SEF_MODE" => Array(
			"default" => array(
				"NAME" => GetMessage("CITRUS_TSZH_PAGE_DEFAULT"),
				"DEFAULT" => "",
				"VARIABLES" => array(),
			),
			"history" => array(
				"NAME" => GetMessage("CITRUS_TSZH_PAGE_HISTORY"),
				"DEFAULT" => "history/",
				"VARIABLES" => array(),
			),
			"do" => array(
				"NAME" => GetMessage("CITRUS_TSZH_PAGE_DO"),
				"DEFAULT" => "#payment#/",
				"VARIABLES" => array("payment"),
			),
			"make" => array(
				"NAME" => GetMessage("CITRUS_TSZH_PAGE_MAKE"),
				"DEFAULT" => "process/#payment#/",
				"VARIABLES" => array("payment"),
			),
		),
	),
);
?>
