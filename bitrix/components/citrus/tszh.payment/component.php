<?
/**
 * ������ ������ ���ƻ
 * ��������� ������ ����� (citrus:tszh.payment)
 * @package citrus.tszhpayment
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

$arParams["TSZH_ID"] = intval($arParams["TSZH_ID"]);

global $USER;

// ����������� ������ ���
if (!CModule::IncludeModule("citrus.tszh"))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_INSTALLED"));

	return;
}
if (!CModule::IncludeModule("citrus.tszhpayment"))
{
	ShowError(GetMessage("TSZH_PAYMENT_MODULE_NOT_INSTALLED"));

	return;
}

// ���� ������������ �� ����������� � ������ ���������� ��ƻ
if ($USER->IsAuthorized() && !CTszh::IsTenant() && !$USER->IsAdmin())
{
	$APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"), false, false, "N", false);

	return;
}

// ��������� ������ ���������� - ���������� � ������ ����������
$arResult = Array(
	"PAY_SYSTEMS" => Array(),
);

$arFilter = Array(
	"ACTIVE" => "Y",
	"LID" => SITE_ID,
);
if (IntVal($_REQUEST['payment']) > 0)
{
	$arPayment = CTszhPayment::GetByID(IntVal($_REQUEST['payment']));
	if ($arPayment)
	{
		$arFilter['ID'] = $arPayment["PAY_SYSTEM_ID"];
	}
}
elseif ($arParams["TSZH_ID"] > 0)
{
	$arFilter["TSZH_ID"] = $arParams["TSZH_ID"];
	unset($arFilter["LID"]);
}
elseif ($USER->IsAuthorized())
{
	$arAccount = CTszhAccount::GetByUserID($USER->GetID());
	if (is_array($arAccount))
	{
		$arFilter["TSZH_ID"] = $arAccount["TSZH_ID"];
	}
}
elseif (is_array($arParams["PAY_SYSTEM_ID"]))
{
	foreach ($arParams["PAY_SYSTEM_ID"] as $ps_id)
	{
		if ($ps_id != 0)
		{
			$arFilter['ID'][] = $ps_id;
		}
	}
}

// ��������� �������� ��� ���� ���������, ������� ���������� ��������
if (isset($_REQUEST['ptype']))
{
	$ptype = strtoupper($_REQUEST['ptype']);
}
else
{
	$ptype = 'MAIN';
}
if (isset($_REQUEST['tszh']))
{
	$tszh = (int)$_REQUEST['tszh'];
}
else
{
	$accLists = CTszhAccount::GetList(
		array(),
		array('USER_ID' => $USER->GetID()),
		false,
		false,
		array('CURRENT', 'USER_ID', 'TSZH_ID', 'XML_ID')
	);

	if ($accLists->SelectedRowsCount() > 1 && $ptype != 'OVERHAUL')
	{

		while ($acc = $accLists->GetNext())
		{
			if ($acc['CURRENT'] == 'Y')
			{
				$account = $acc;
				break;
			}
		}
	}
    elseif($accLists->SelectedRowsCount() > 1)
    {
        while ($acc = $accLists->GetNext())
        {
            $account = $acc;
            break;
        }

    }
    else{
        $account["TSZH_ID"] = 0;
    }
	$tszh = $account["TSZH_ID"];
}
$receipt_type = Citrus\Tszh\Types\ReceiptType::getConstants();
$receipt_type_titles = Citrus\Tszh\Types\ReceiptType::getTitles();
$arResult['is_overhaul'] = $receipt_type[$ptype] == Citrus\Tszh\Types\ReceiptType::OVERHAUL || $receipt_type[$ptype] == Citrus\Tszh\Types\ReceiptType::FINES_OVERHAUL;
$arResult['is_penalty'] = $receipt_type[$ptype] == Citrus\Tszh\Types\ReceiptType::FINES_MAIN || $receipt_type[$ptype] == Citrus\Tszh\Types\ReceiptType::FINES_OVERHAUL;
foreach ($receipt_type as $name => $type)
{
	$arSelectOptions[] = array(
		'url' => $APPLICATION->GetCurPageParam('ptype=' . $name, array('ptype', 'tszh', 'payment')),
		'selected' => (string)$ptype == (string)$name,
		'name' => $receipt_type_titles[$type],
	);
}
$arResult['receipt_types'] = $arSelectOptions;
$rsTszh = CTszh::GetList(array(), array(), false, false, array('ID', 'NAME', 'PAYMENT_RECEIPT_TYPES', 'ENABLE_CUSTOM_PAYMENT', 'RSCH'));
$arTszhList = array();
$arTszhReceiptTypesAvailable = array();
$needCustomPayment = true;
while ($arTszh = $rsTszh->Fetch())
{
	$arTszhReceiptTypes = array();
	if ($arTszh["ENABLE_CUSTOM_PAYMENT"] == "Y")
	{
		$arTszhReceiptTypes = unserialize($arTszh["PAYMENT_RECEIPT_TYPES"]);
		foreach ($arTszhReceiptTypes as $index_receipt => $value)
		{
			$arTszhReceiptTypesAvailable[$value] = $arResult["receipt_types"][$value];
		}
	}
	else
	{
		$needCustomPayment = false;
	}

	if (empty($arTszhReceiptTypes) || in_array($receipt_type[$ptype], $arTszhReceiptTypes))
	{
		if ($tszh <= 0)
		{
			$tszh = $arTszh['ID'];
		}

		$arTszhList[] = array(
			'ID' => $arTszh['ID'],
			'NAME' => $arTszh['NAME'],
			'RSCH' => $arTszh['RSCH'],
			'url' => $APPLICATION->GetCurPageParam('ptype=' . $ptype . '&tszh=' . $arTszh['ID'], array('ptype', 'tszh', 'payment')),
			'selected' => $tszh == $arTszh['ID'],
		);
	}
}
if ($needCustomPayment)
{
	$arResult["receipt_types"] = $arTszhReceiptTypesAvailable;
}
$arResult['tszh_list'] = $arTszhList;
if (isset($_REQUEST['ptype']))
{
	$arFilter['TSZH_ID'] = $tszh;
}

$rsPaySytems = CTszhPaySystem::GetList(Array("SORT" => "ASC"), $arFilter);
$usedPaySystems = array();
while ($arPaySystem = $rsPaySytems->GetNext())
{
	// �������� ������������ �� ��� �� �������������� ������������� (����� ����� ����������, ����� �� ����� ��������� ����������� � ������ � ���� �� ������������� ��)
	// ������ ����������� ����� ������� �� ����� ������ ��� �� �������������� �������������
	if (!$USER->IsAuthorized() && array_key_exists($arPaySystem['ACTION_FILE'], $usedPaySystems))
	{
		continue;
	}

	$arPaySystem['~DESCRIPTION'] = str_replace('#ID#', $USER->GetID(), $arPaySystem['~DESCRIPTION']);
	$arPaySystem['DESCRIPTION'] = $arPaySystem['~DESCRIPTION'];
	$arResult['PAY_SYSTEMS'][] = $arPaySystem;
	$usedPaySystems[$arPaySystem['ACTION_FILE']] = true;
}

// ����� ������� ��� ������� ������ � � ��� ������ ������� �������� ����, � � ����� ���������� (��� ��������� � ������� ������ ������ ������� ������)
// ������ �12567: ������ ������� ����������� � ������� - ������ �����
if (strpos(SITE_TEMPLATE_ID, 'tszh_two_') === 0)
{
	$APPLICATION->SetPageProperty("SHOW_TOP_RIGHT", "Y");
}

$this->IncludeComponentTemplate();
