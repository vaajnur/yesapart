<?
$MESS ['SUP_LIST_TICKETS_PER_PAGE'] = "Количество обращений на одной странице";
$MESS ['SUP_SET_PAGE_TITLE'] = "Устанавливать заголовок страницы";
$MESS ['SUP_EDIT_MESSAGES_PER_PAGE'] = "Количество сообщений на одной странице";
$MESS ['SUP_DESC_YES'] = "Да";
$MESS ['SUP_DESC_NO'] = "Нет";
$MESS ['SUP_TICKET_ID_DESC'] = "Идентификатор обращения";
$MESS ['SUP_TICKET_LIST_DESC'] = "Список обращений";
$MESS ['SUP_TICKET_EDIT_DESC'] = "Редактирование обращения";
$MESS ['SUP_SHOW_COUPON_FIELD'] = "Показывать поле ввода купона";
$MESS ['WZ_PROPERTY_VALUES'] = "Множественное свойство, в котором хранятся значения выпадающего списка";
$MESS ['WZ_INCLUDE_INTO_CHAIN'] = "Добавлять разделы мастера в навигационную цепочку";
$MESS ['WZ_PROPERTY'] = "Свойство, в котором хранится тип вопроса";
$MESS ['WZ_IBLOCK'] = "Информационный блок";
$MESS ['WZ_IBTYPE'] = "Тип инфоблока";
$MESS ['WZ_SHOW_RESULT'] = "Показать результат работы мастера";
$MESS ['WZ_TEMPLATE'] = "Шаблон мастера";
$MESS ['WZ_STANDARD'] = "Строгий";
$MESS ['WZ_DEFAULT'] = "Изящный";
$MESS ['WZ_SELECT_SECTIONS'] = "Список разделов для привязки";
$MESS ['SECTIONS_TO_CATEGORIES'] = "Включить привязки";
$MESS ['WZ_GRP_SECTIONS_TO_CATEGORIES'] = "Привязка разделов мастера к категориям техподдержки";
?>