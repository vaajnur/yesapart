<?
$MESS["SUP_LIST_TICKETS_PER_PAGE"] = "Кількість звернень на одній сторінці";
$MESS["SUP_SET_PAGE_TITLE"] = "Встановлювати заголовок сторінки";
$MESS["SUP_EDIT_MESSAGES_PER_PAGE"] = "Кількість звернень на одній сторінці";
$MESS["SUP_DESC_YES"] = "Так";
$MESS["SUP_DESC_NO"] = "Ні";
$MESS["SUP_TICKET_ID_DESC"] = "Ідентифікатор звернення";
$MESS["SUP_TICKET_LIST_DESC"] = "Список звернень";
$MESS["SUP_TICKET_EDIT_DESC"] = "Редагування звернення";
$MESS["WZ_PROPERTY_VALUES"] = "Множинна властивість, в якій зберігається значення випадаючого списку ";
$MESS["WZ_INCLUDE_INTO_CHAIN"] = "Додати розділи майстра до навігаційного ланцюжка";
$MESS["WZ_PROPERTY"] = "Властивість, в якій зберігається тип питання ";
$MESS["WZ_IBLOCK"] = "Інформаційний блок ";
$MESS["WZ_IBTYPE"] = "Тип інфоблоку";
$MESS["WZ_SHOW_RESULT"] = "Показати результат роботи майстра ";
$MESS["WZ_TEMPLATE"] = "Шаблон майстра";
$MESS["WZ_STANDARD"] = "Суворий";
$MESS["WZ_DEFAULT"] = "Витончений ";
$MESS["WZ_SELECT_SECTIONS"] = "Список розділів для прив'язки ";
$MESS["SECTIONS_TO_CATEGORIES"] = "включити прив'язки ";
$MESS["WZ_GRP_SECTIONS_TO_CATEGORIES"] = "Прив'язка розділів майстра до категорій техпідтримки ";
?>