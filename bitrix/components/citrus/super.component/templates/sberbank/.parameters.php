<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"PAYEE" => array(
		"NAME" => "������������ ���������� �������",
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"INN" => array(
		"NAME" => "��� ���������� �������",
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"KPP" => array(
		"NAME" => "��� ���������� �������",
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"ACCOUNT" => array(
		"NAME" => "����� ����� ���������� �������",
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"BANK" => array(
		"NAME" => "������������ ����� ���������� �������",
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"BIK" => array(
		"NAME" => "���",
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"BANK_ACCOUNT" => array(
		"NAME" => "����� ���./��. ����� ����������",
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
);
?>