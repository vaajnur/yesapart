<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arParams["DISPLAY_TOP_PAGER"] == "Y")
	echo '<div>' . $arResult["NAV_STRING"] . '</div>';

foreach ($arResult["VOTINGS"] as $arVoting)
{
	$bCanVote = CCitrusPollVote::CanVote($arVoting['ID'], $arVoting);
	
	if ($bCanVote)
	{
		?><?$APPLICATION->IncludeComponent("citrus:tszh.voting.form", "", array(
			"GROUP_ID" => $arParams["GROUP_ID"],
			"VOTING_ID" => $arVoting['ID']
			),
			($this->__component->__parent ? $this->__component->__parent : $component)
		);?><?
	}
	else
	{
		?><?$APPLICATION->IncludeComponent("citrus:tszh.voting.result", "", array(
			"GROUP_ID" => $arParams["GROUP_ID"],
			"VOTING_ID" => Array($arVoting['ID']),
			),
			($this->__component->__parent ? $this->__component->__parent : $component)
		);?><?
	}
	?><hr /><br /><?
}

if ($arParams["DISPLAY_BOTTOM_PAGER"] == "Y")
	echo '<div>' . $arResult["NAV_STRING"] . '</div>';


?>