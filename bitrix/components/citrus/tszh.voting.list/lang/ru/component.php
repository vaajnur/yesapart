<?$MESS ['VOTE_MODULE_IS_NOT_INSTALLED'] = "Модуль голосований не установлен";
$MESS ['CIEE_ELEMENT_NOT_FOUND'] = "Организация не найдена.";
$MESS ['CIEE_IBLOCK_MODULE_NOT_INSTALLED'] = "Модуль «Информационные блоки» не установлен.";$MESS ['CIEE_PORTAL_MODULE_NOT_INSTALLED'] = "Модуль «Портал ЖКХ» не установлен.";$MESS ['CIEE_IBLOCK_NOT_FOUND'] = "Инфоблок не найден.";
$MESS ['CIEE_F_ID'] = "ID";$MESS ['CIEE_F_NAME'] = "Название";
$MESS ['CIEE_F_CODE'] = "Символьный код";$MESS ['CIEE_F_IBLOCK_SECTION_ID'] = "Раздел";$MESS ['CIEE_F_ACTIVE'] = "Активность";$MESS ['CIEE_F_DATE_ACTIVE_FROM'] = "Дата начала активности";$MESS ['CIEE_F_DATE_ACTIVE_TO'] = "Дата окончания активности";$MESS ['CIEE_F_SORT'] = "Индекс сортировки";$MESS ['CIEE_F_PREVIEW_TEXT'] = "Описание для анонса";$MESS ['CIEE_F_DETAIL_TEXT'] = "Детальное описание";$MESS ['CIEE_F_PREVIEW_PICTURE'] = "Картинка для анонса";
$MESS ['CIEE_F_DETAIL_PICTURE'] = "Детальная картинка";?>