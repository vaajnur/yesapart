<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if (!CModule::IncludeModule("vdgb.tszhvote"))
{
	ShowError(GetMessage("VOTE_MODULE_IS_NOT_INSTALLED"));
	return;
}

$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]!="N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]!=="N";

$arParams["VOTES_COUNT"] = intval($arParams["VOTES_COUNT"]);
if($arParams["VOTES_COUNT"]<=0)
	$arParams["VOTES_COUNT"] = 10;

$arResult["VOTINGS"] = Array();

$arFilter = Array(
	"ACTIVE" => "Y",
);

$arNavParams = array(
	"nPageSize" => $arParams["VOTES_COUNT"],
	"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	"bShowAll" => $arParams["PAGER_SHOW_ALL"],
);
// for cache key
//$arNavigation = CDBResult::GetNavParams($arNavParams);
$rsVotings = CCitrusPoll::GetList(Array("DATE_BEGIN" => "DESC"), $arFilter, false, $arNavParams);
while ($arVoting = $rsVotings->GetNext(false))
{
	$arResult["VOTINGS"][] = $arVoting;
}
$arResult["NAV_STRING"] = $rsVotings->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
$arResult["NAV_RESULT"] = $rsVotings;

$this->IncludeComponentTemplate();
