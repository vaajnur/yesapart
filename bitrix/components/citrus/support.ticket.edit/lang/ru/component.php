<?
$MESS["SUP_NEW_TICKET_TITLE"]="Новая заявка";
$MESS["SUP_EDIT_TICKET_TITLE"]="Заявка номер ";
$MESS["MODULE_NOT_INSTALL"]="Модуль аварийно-диспетчерской службы не установлен.";
$MESS ['SUP_FORGOT_TITLE'] = "Вы забыли ввести поле \"Заголовок\"";
$MESS ['SUP_FORGOT_MESSAGE'] = "Вы забыли ввести поле \"Сообщение\"";
$MESS ['SUP_MAX_FILE_SIZE_EXCEEDING'] = "Ошибка! Невозможна загрузка файла \"#FILE_NAME#\". Возможно, превышен максимально допустимый размер файла.";
$MESS ['SUP_PAGES'] = "Сообщений";
$MESS ['SUP_ERROR'] = "Ошибка создания сообщения";
$MESS ['CTT_VIEWERS'] = "Кем просматривалось";
$MESS ['CTT_OWNER'] = "Автор обращения";
$MESS ['CTT_SOUREC'] = "Источник";
$MESS ['CTT_DATE_CREATE'] = "Дата создания";
$MESS ['CTT_CREATED'] = "Кем создано";
$MESS ['CTT_TIMESTAMP_X'] = "Дата изменения";
$MESS ['CTT_DATE_CLOSE'] = "Дата закрытия";
$MESS ['CTT_STATUS'] = "Статус";
$MESS ['CTT_CATEGORY'] = "Тип обращения";
$MESS ['CTT_TIME_TO_SOLVE'] = "Котрольная дата выполнения";
$MESS ['CTT_CRIT'] = "Критичность";
$MESS ['CTT_RESPONSIBLE'] = "Ответственный";
$MESS ['CTT_MESSAGE'] = "Сообщение";
$MESS ['CTT_CLOSE_TICKET'] = "Закрыть заявку";
$MESS ['CTT_CLIENT_TICKET_ADDED'] = "Ваша заявка передана диспетчеру. Заявке присвоен номер #NUMBER#";
$MESS ['CTT_CLIENT_TICKET_SAVED'] = "Заявка номер #NUMBER# сохранена";
$MESS ['CTT_TICKET_ADDED'] = "Заявка добавлена, присвоен номер #NUMBER#";
$MESS ['CTT_TICKET_SAVED'] = "Заявка номер #NUMBER# сохранена";
$MESS ['SUP_ERROR_MESSAGE'] = "Вы не ввели сообщение";
$MESS ['SUP_ERROR_TELEPHONE'] = "Некорректный номер телефона";
$MESS ['SUP_MESS_ADDED'] = "Сообщение добавлено";
$MESS ['SUP_DISPATCHER'] = "Диспетчер";

?>