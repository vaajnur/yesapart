<?
$MESS['SUP_EDIT_DEFAULT_TEMPLATE_PARAM_1_NAME'] = "ID обращения";
$MESS ['SUP_EDIT_DEFAULT_TEMPLATE_PARAM_2_NAME'] = "Страница списка обращений";
$MESS ['SUP_EDIT_MESSAGES_PER_PAGE'] = "Количество сообщений на одной странице";
$MESS ['SUP_DESC_YES'] = "Да";
$MESS ['SUP_DESC_NO'] = "Нет";
$MESS ['SUP_SET_PAGE_TITLE'] = "Устанавливать заголовок страницы";
$MESS ['SUP_SHOW_COUPON_FIELD'] = "Показывать поле ввода купона";
$MESS ['SUP_EDIT_FIELDS'] = "Поля, выводимые на редактирование";

$MESS ['TT_VIEWERS'] = "Кем просматривалось";
$MESS ['TT_OWNER'] = "Автор обращения";
$MESS ['TT_SOURCE'] = "Источник";
$MESS ['TT_DATE_CREATE'] = "Дата создания";
$MESS ['TT_CREATED'] = "Кем создано";
$MESS ['TT_TIMESTAMP_X'] = "Дата изменения";
$MESS ['TT_DATE_CLOSE'] = "Дата закрытия";
$MESS ['TT_STATUS'] = "Статус";
$MESS ['TT_CATEGORY'] = "Тип обращения";
$MESS ['TT_TIME_TO_SOLVE'] = "Котрольная дата выполнения";
$MESS ['TT_CRIT'] = "Критичность";
$MESS ['TT_RESP'] = "Ответственный";
$MESS ['TT_MESSAGE'] = "Сообщение";
$MESS ['TT_CLOSE_TICKET'] = "Закрыть заявку";

$MESS ['SUP_EDIT_TEMPLATE_ID'] = "Шаблон получения ID тикета";
$MESS ['SUP_EDIT_ORDER_DESC'] = "Сортировать сообщения в обратном порядке";

?>