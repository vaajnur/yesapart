<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

require_once($_SERVER["DOCUMENT_ROOT"].$componentPath."/functions.php");

if (!CModule::IncludeModule("citrus.tszhtickets"))
{
	ShowError(GetMessage("MODULE_NOT_INSTALL"));
	return;
}

//Permissions
if ( !($USER->IsAuthorized() && (CTszhTicket::IsSupportClient() || CTszhTicket::IsAdmin() || CTszhTicket::IsSupportTeam())) )
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if($arParams["SEF_MODE"] == "Y"){
    $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates(array(), $arParams["SEF_URL_TEMPLATES"]);

    $componentPage = CComponentEngine::ParseComponentPath(
        $arParams["SEF_FOLDER"],
        $arUrlTemplates,
        $arVariables
    );
    $ID = intval($arVariables["ID"]);
}
else
    $ID = intval($arParams["ID"]);

if($ID<0)
    return;

$arParams["SET_PAGE_TITLE"] = ($arParams["SET_PAGE_TITLE"] == "N" ? "N" : "Y" );
if ($arParams["SET_PAGE_TITLE"] == "Y")
{
    $title = GetMessage("SUP_EDIT_TICKET_TITLE").$ID;
    $APPLICATION->SetPageProperty('breadcrumb_title', $title);
    $APPLICATION->SetTitle($title);
}


$arShowFields = Array(
	'VIEWERS' => GetMessage("CTT_VIEWERS"),
	'OWNER' => GetMessage("CTT_OWNER"),
	'SOURCE' => GetMessage("CTT_SOURCE"),
	'DATE_CREATE' => GetMessage("CTT_DATE_CREATE"),
	'CREATED' => GetMessage("CTT_CREATED"),
	'TIMESTAMP_X' => GetMessage("CTT_TIMESTAMP_X"),
	'DATE_CLOSE' => GetMessage("CTT_DATE_CLOSE"),
	'STATUS' => GetMessage("CTT_STATUS"),
	'CATEGORY' => GetMessage("CTT_CATEGORY"),
	'TIME_TO_SOLVE' => GetMessage("CTT_TIME_TO_SOLVE"),
	'CRITICALITY' => GetMessage("CTT_CRIT"),
	'RESPONSIBLE' => GetMessage("CTT_RESPONSIBLE"),
);
if (!is_array($arParams['SHOW_FIELDS']) || count($arParams['SHOW_FIELDS']) <= 0) {
	$arParams['SHOW_FIELDS'] = array_keys($arShowFields);
}
$arr = Array();
foreach ($arParams['SHOW_FIELDS'] as $key=>$value) {
	$arr[$value] = 'Y';
}
$arParams['SHOW_FIELDS'] = $arr;


if (!is_array($arParams['EDIT_FIELDS'])) {
	$arParams['EDIT_FIELDS'] = Array();
}
$arRes = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("TSZH_TICKET", 0, LANGUAGE_ID);
$userProp = array();
if (!empty($arRes))
{
	foreach ($arRes as $key => $val)
		$userProp[$val["FIELD_NAME"]] = (strLen($val["EDIT_FORM_LABEL"]) > 0 ? $val["EDIT_FORM_LABEL"] : $val["FIELD_NAME"]);
}


$arEditFields = Array(
	'MESSAGE' => GetMessage("CTT_MESSAGE"),
	'CRITICALITY' => GetMessage("CTT_CRIT"),
	'CATEGORY' => GetMessage("CTT_CATEGORY"),
	'STATUS' => GetMessage("CTT_STATUS"),
	'RESPONSIBLE' => GetMessage("CTT_RESPONSIBLE"),
	'CLOSE' => GetMessage("CTT_CLOSE_TICKET"),
);

foreach ($arParams['EDIT_FIELDS'] as $key=>$strEditField) {
	if (substr($strEditField, 0, 3) == 'UF_') {
		if (!array_key_exists($strEditField, $userProp)) {
			unset($arParams['EDIT_FIELDS'][$key]);
		}
	} else {
		if (!array_key_exists($strEditField, $arEditFields)) {
			unset($arParams['EDIT_FIELDS'][$key]);
		}
	}
}

//Post
$strError = "";
$cookieName = str_replace(".","_", 'message'.$APPLICATION->GetCurPage(false));
$arParams["TICKET_EDIT_TEMPLATE"] = trim($arParams["TICKET_EDIT_TEMPLATE"]);
$arParams["TICKET_EDIT_TEMPLATE"] = (strlen($arParams["TICKET_EDIT_TEMPLATE"]) > 0 ? htmlspecialchars($arParams["TICKET_EDIT_TEMPLATE"]) : "ticket_edit.php?ID=#ID#");

if (($_REQUEST["save"]=="Y" || strlen($_REQUEST["apply"])>0) && $_SERVER["REQUEST_METHOD"]=="POST" && check_bitrix_sessid() /*&& (count($arParams['EDIT_FIELDS']) > 0)*/)
{
	$ID = intval($_REQUEST["ID"]);
    //if (strlen(trim($_REQUEST["MESSAGE"]))<=0)
      //  $strError .= GetMessage("SUP_ERROR_MESSAGE")."<br>";
	if ($ID <=0)
	{
		if (isset($_REQUEST["TITLE"]) && strlen(trim($_REQUEST["TITLE"]))<=0)
			$strError .= GetMessage("SUP_FORGOT_TITLE")."<br>";

		if (strlen(trim($_REQUEST["MESSAGE"]))<=0)
			$strError .= GetMessage("SUP_FORGOT_MESSAGE")."<br>";
    }

	$arFILES = array();
	if (is_array($_FILES) && count($_FILES)>0)
	{
		foreach ($_FILES as $key => $arFILE)
		{
			if (strlen($arFILE["name"])>0)
			{
				$arFILE["MODULE_ID"] = "support";
				$arFILES[] = $arFILE;
			}
		}
	}

	if (is_array($arFILES) && count($arFILES)>0)
	{
		$max_size = COption::GetOptionString("support", "SUPPORT_MAX_FILESIZE");
		$max_size = intval($max_size)*1024;

		foreach ($arFILES as $key => $arFILE)
		{
			if (intval($arFILE["size"])>$max_size || intval($arFILE["error"])>0)
				$strError .= str_replace("#FILE_NAME#", $arFILE["name"], GetMessage("SUP_MAX_FILE_SIZE_EXCEEDING"))."<br>";
		}
	}

	//$arParams["TICKET_LIST_URL"] = trim($arParams["TICKET_LIST_URL"]);
	//$arParams["TICKET_LIST_URL"] = (strlen($arParams["TICKET_LIST_URL"]) > 0 ? htmlspecialchars($arParams["TICKET_LIST_URL"]) : "ticket_list.php");

	if ($strError == "")
	{
		if ($_REQUEST["OPEN"]=="Y")
			$_REQUEST["CLOSE"]="N";
		if ($_REQUEST["CLOSE"]=="Y")
			$_REQUEST["OPEN"]="N";

		$arFields = array(
			'SITE_ID'					=> SITE_ID,
			'HIDDEN'					=> 'N',
			'PUBLIC_EDIT_URL'			=> $APPLICATION->GetCurPage(),
		);

		if (count($arParams['EDIT_FIELDS']) == 0 || in_array('CLOSE', $arParams['EDIT_FIELDS'])) {
			$arFields['CLOSE'] = $_REQUEST['CLOSE'];

			if ($arFields['CLOSE'] == 'Y') {
				$is_filtered = false;
				$rsStatus = CTszhTicketDictionary::GetList(($by = 's_id'), ($order = 'asc'), Array("TYPE" => 'S', 'SID' => 'closed', 'SITE' => SITE_ID), $is_filtered);
				if ($arStatus = $rsStatus->Fetch()) {
					$arFields['STATUS_ID'] = $arStatus['ID'];
				}
			}
		}
        if (count($arParams['EDIT_FIELDS']) == 0){
            $arDicFieldsEdit = ['CRITICALITY_ID'=>'CRITICALITY', 'CATEGORY_ID'=>'CRITICALITY'
                , 'MESSAGE'=>'MESSAGE', 'RESPONSIBLE_USER_ID'=>'RESPONSIBLE'];
            foreach ($arDicFieldsEdit as $key=>$value){
                $arFields[$key] = $_REQUEST[$key];
            }
        }
        else{
            if ($ID <= 0)
                $arFields['TITLE'] = $_REQUEST['TITLE'];

            if (in_array('CRITICALITY', $arParams['EDIT_FIELDS']))
                $arFields['CRITICALITY_ID'] = $_REQUEST['CRITICALITY_ID'];

            if (in_array('CATEGORY', $arParams['EDIT_FIELDS']))
                $arFields['CATEGORY_ID'] = $_REQUEST['CATEGORY_ID'];

            if (in_array('MESSAGE', $arParams['EDIT_FIELDS']))
                $arFields['MESSAGE'] = $_REQUEST['MESSAGE'];

            if (in_array('RESPONSIBLE', $arParams['EDIT_FIELDS']))
                $arFields['RESPONSIBLE_USER_ID'] = $_REQUEST['RESPONSIBLE_USER_ID'];

            if (in_array('STATUS', $arParams['EDIT_FIELDS']) && $_REQUEST["CLOSE"]!="Y") {
                $arFields['STATUS_ID'] = IntVal($_REQUEST['STATUS_ID']);
        }
			$is_filtered = false;
			$rsStatus = CTszhTicketDictionary::GetList(($by = 's_id'), ($order = 'asc'), Array("TYPE" => 'S', 'ID' => $arFields['STATUS_ID'], 'SITE' => SITE_ID), $is_filtered);
			if ($arStatus = $rsStatus->Fetch() && in_array($arStatus['SID'], Array('done', 'closed'))) {
				$arFields['CLOSE'] = 'Y';
			}
		}

		if (!(CTszhTicket::IsSupportTeam() || CTszhTicket::IsAdmin() || $arFields["CLOSE"] == 'Y')) {
			$arFields['STATUS_ID'] = CTszhTicketDictionary::GetDefault("S", SITE_ID);
			$arFields['CRITICALITY_ID'] = CTszhTicketDictionary::GetDefault("K", SITE_ID);
			//$arFields['CATEGORY_ID'] = CTszhTicketDictionary::GetDefault("C", SITE_ID);
		}

		$GLOBALS["USER_FIELD_MANAGER"]->EditFormAddFields("TSZH_TICKET", $arFields);
		foreach ($arFields as $key=>$value) {
			if (substr($key, 0, 3) == 'UF_' && !in_array($key, $arParams['EDIT_FIELDS'])) {
				unset($arFields[$key]);
			}
		}

		global $USER;
		// �������� ���� ��������� � ����� ������
		if (CModule::IncludeModule("citrus.tszh") && $USER->IsAuthorized() && $ID <= 0) {
			$arUserAccount = CTszhAccount::GetByUserID($USER->GetID());
			if (is_array($arUserAccount)) {
				$arFields['UF_FIO'] = CTszhAccount::GetFullName($USER->GetID());
				$arFields['UF_ACCOUNT'] = $arUserAccount['XML_ID'];
				$arFields['UF_ADDRESS_BUILDING'] = CTszhAccount::GetFullAddress($arUserAccount, false);
				$arFields['UF_ADDRESS_FLAT'] = $arUserAccount['FLAT'];
			}
		}

		$bNew = $ID <= 0;
		$ID = CTszhTicket::SetTicket($arFields, $ID, "Y", $NOTIFY = "Y");
		if (intval($ID)>0)
		{
		    if(isset($arParams["TICKET_LIST_URL"]) && $arParams["TICKET_LIST_URL"] != "" && isset($arParams["TICKET_EDIT_TEMPLATE"])){
                if (strlen($_REQUEST["save"])>0)
                {
                    $rsTmpTicket = CTszhTicket::GetByID($ID);
                    if ($arTmpTicket = $rsTmpTicket->Fetch()) {
                        if (CTszhTicket::IsSupportClient()) {
                            $_SESSION['citrus_support_message'] = str_replace('#NUMBER#', $arTmpTicket['NUMBER'], $bNew ? GetMessage("CTT_CLIENT_TICKET_ADDED") : GetMessage("CTT_CLIENT_TICKET_SAVED"));
                        } else {
                            $_SESSION['citrus_support_message'] = str_replace('#NUMBER#', $arTmpTicket['NUMBER'], $bNew ? GetMessage("CTT_TICKET_ADDED") : GetMessage("CTT_TICKET_SAVED"));
                        }
                    }
                    LocalRedirect($arParams["TICKET_LIST_URL"]);
                }
                elseif (strlen($_REQUEST["apply"])>0)
                {
                    LocalRedirect(
                        CComponentEngine::MakePathFromTemplate(
                            $arParams["TICKET_EDIT_TEMPLATE"],
                            Array(
                                "ID" => $ID
                            )
                        )
                    );
                }
            }
			else{
                $APPLICATION->set_cookie($cookieName, GetMessage("SUP_MESS_ADDED"), time()+3600);
                LocalRedirect($APPLICATION->GetCurPage(false));
            }
		}
		else
		{
			$ex = $APPLICATION->GetException();
			if ($ex)
			{
				$strError .= $ex->GetString() . '<br>';
			}
			else
			{
				$strError .= GetMessage('SUP_ERROR') . '<br>';
			}
		}
	}
}

$message = $APPLICATION->get_cookie($cookieName);
if($message!=""){
    $APPLICATION->set_cookie($cookieName, "", time()-60);
}


//Result array
$arResult = Array(
	"TICKET" => Array(),
    "MESSAGE" => $message,
    "MESSAGES" => Array(),
	"ONLINE" => Array(),
	"DICTIONARY" => Array(
		"CRITICALITY" => Array(),
		"CRITICALITY_DEFAULT" => "",
		"CATEGORY" => Array(),
		"CATEGORY_DEFAULT" => "",
		"STATUS" => Array(),
		"STATUS_DEFAULT" => "",
	),
	"ERROR_MESSAGE" => $strError,
	"REAL_FILE_PATH" => (strlen($_SERVER["REAL_FILE_PATH"]) > 0 ? htmlspecialchars($_SERVER["REAL_FILE_PATH"]) : htmlspecialchars($APPLICATION->GetCurPage())),
    "CURPAGE" => $APPLICATION->GetCurPage(),
    "NAV_STRING" => "",
	"NAV_RESULT" => null,
	"OPTIONS" => Array(
		"ONLINE_INTERVAL" => intval(COption::GetOptionString("support", "ONLINE_INTERVAL")),
		"MAX_FILESIZE" => intval(COption::GetOptionString("support", "SUPPORT_MAX_FILESIZE")),
	),
);

$arParams["ID"] = (intval($ID) > 0 ? intval($ID) : intval($_REQUEST["ID"]));
$rsTicket = CTszhTicket::GetByID($arParams["ID"], SITE_ID, $check_rights = "Y", $get_user_name = "N", $get_extra_names = "N");
$position = GetMessage("SUP_DISPATCHER");

if ($arTicket = $rsTicket->GetNext())
{
	//+Ticket and user names
	$arResult["TICKET"] = $arTicket +
	_GetUserInfo($arTicket["RESPONSIBLE_USER_ID"], "RESPONSIBLE") +
	_GetUserInfo($arTicket["OWNER_USER_ID"], "OWNER") +
	_GetUserInfo($arTicket["CREATED_USER_ID"], "CREATED") +
	_GetUserInfo($arTicket["MODIFIED_USER_ID"], "MODIFIED_BY");

    if($arTicket["STATUS_ID"] == null)
        $arTicket["STATUS_ID"] = array_keys(CTszhTicketDictionary::GetDropDownArray(SITE_ID)["S"])[0];
	//Dictionary table
	$arDictionary = Array(
		"C" => Array("CATEGORY", intval($arTicket["CATEGORY_ID"])),
		"K" => Array("CRITICALITY", intval($arTicket["CRITICALITY_ID"])),
		"S" => Array("STATUS", intval($arTicket["STATUS_ID"])),
		"M" => Array("MARK", intval($arTicket["MARK_ID"])),
		"SR" => Array("SOURCE", intval($arTicket["SOURCE_ID"]))
	);

	//+Ticket dictionary
	$arResult["TICKET"] += _GetDictionaryInfoEx($arDictionary);
    if($arTicket["CATEGORY_ID"] != null){
        $dbCategory = CTszhTicketDictionary::GetList(($by = 'id'), ($order = 'desc'), Array("ID" => $arResult['TICKET']['CATEGORY_ID']),($is_filtered = false));
        if ($arCategory = $dbCategory->Fetch()) {
            $timestamp = strtotime("+" . $arCategory['TIME_TO_SOLVE'] . ' hours', MakeTimeStamp($arTicket["DATE_CREATE"]));
            $arResult["TICKET"]["DATE_SOLVE"] = ConvertTimeStamp($timestamp, "SHORT");
        }
    }

	//Messages files
	$arMessagesFiles = Array();
	$rsFiles = CTszhTicket::GetFileList($v1="s_id", $v2="asc", array("TICKET_ID" => $arParams["ID"]));
	{
		while ($arFile = $rsFiles->Fetch())
		{
			$name = strlen($arFile["ORIGINAL_NAME"])>0 ? $arFile["ORIGINAL_NAME"] : $arFile["FILE_NAME"];
			if (strlen($arFile["EXTENSION_SUFFIX"]) > 0)
			{
				$suffix_length = strlen($arFile["EXTENSION_SUFFIX"]);
				$name = substr($name, 0, strlen($name)-$suffix_length);
			}
			$arMessagesFiles[$arFile["MESSAGE_ID"]][] = array("HASH" => $arFile["HASH"], "NAME" => htmlspecialchars($name), "FILE_SIZE" => $arFile["FILE_SIZE"]);
		}
	}

	//+Messages
	$arParams["MESSAGES_PER_PAGE"] = (intval($arParams["MESSAGES_PER_PAGE"]) <= 0 ? 20 : intval($arParams["MESSAGES_PER_PAGE"]));

	$arFilter = Array(
		"TICKET_ID" => $arParams["ID"],
		"TICKET_ID_EXACT_MATCH" => "Y",
		"IS_MESSAGE" => "Y"
	);

	CPageOption::SetOptionString("main", "nav_page_in_session", "N");
    $rsMessage = CTszhTicket::GetMessageList($by, $order=($arParams["ORDER_DESC"] == "Y" ? "desc" : ""), $arFilter, $is_filtered, $check_rights = "Y", $get_user_name = "N");
    //$rsMessage = CTszhTicket::GetMessageList($by, $order, $arFilter, $is_filtered, $check_rights = "Y", $get_user_name = "N");
	$rsMessage->NavStart($arParams["MESSAGES_PER_PAGE"]);


	$arResult["NAV_STRING"] = $rsMessage->GetPageNavString(GetMessage("SUP_PAGES"));
	$arResult["NAV_RESULT"] = $rsMessage;
    $idGroupDisp = CGroup::GetList ($by = "c_sort", $order = "asc", Array ("STRING_ID" => 'TSZH_SUPPORT_ADMINISTRATORS'))->Fetch()["ID"];
	while ($arMessage = $rsMessage->GetNext())
	{
		if (array_key_exists($arMessage["ID"], $arMessagesFiles))
			$arFiles["FILES"] = $arMessagesFiles[$arMessage["ID"]];
		else
			$arFiles["FILES"] = Array();

		$arMessage["MESSAGE"] =TxtToHTML(
			$arMessage["~MESSAGE"],
			$bMakeUrls = true,
			$iMaxStringLen = 70,
			$QUOTE_ENABLED = "Y",
			$NOT_CONVERT_AMPERSAND = "N",
			$CODE_ENABLED = "Y",
			$BIU_ENABLED ="Y",
			$quote_table_class		= "support-quote-table",
			$quote_head_class		= "support-quote-head",
			$quote_body_class		= "support-quote-body",
			$code_table_class		= "support-code-table",
			$code_head_class		= "support-code-head",
			$code_body_class		= "support-code-body",
			$code_textarea_class	= "support-code-textarea",
			$link_class					= ""
		);

		$arResult["MESSAGES"][] =
			$arMessage +
			$arFiles +
			_GetUserInfo($arMessage["OWNER_USER_ID"], "OWNER") +
			_GetUserInfo($arMessage["CREATED_USER_ID"], "CREATED") +
			_GetUserInfo($arMessage["MODIFIED_USER_ID"], "MODIFIED_BY") +
            array("POSITION"=> in_array($idGroupDisp, CUser::GetUserGroup($arMessage["OWNER_USER_ID"])) ? $position : "");
	}


	//Online
	CTszhTicket::UpdateOnline($arParams["ID"], $USER->GetID());
	$rsOnline = CTszhTicket::GetOnline($arParams["ID"]);
	while ($arOnline = $rsOnline->GetNext())
	{
		$arResult["ONLINE"][] = $arOnline;
	}

	$ticketSite = $arTicket["SITE_ID"];
	//$ticketSla = $arTicket["SLA_ID"];
}
else
{
	$ticketSite = SITE_ID;
//	$ticketSla = CTszhTicketSLA::GetForUser();
	$arResult["DICTIONARY"]["CRITICALITY_DEFAULT"] = CTszhTicketDictionary::GetDefault("K", $ticketSite);
	$arResult["DICTIONARY"]["CATEGORY_DEFAULT"] = CTszhTicketDictionary::GetDefault("C", $ticketSite);
	$arResult["DICTIONARY"]["STATUS_DEFAULT"] = CTszhTicketDictionary::GetDefault("S", $ticketSite);
}


//Mark, Category, Criticality dictionary list
$ticketDictionary = CTszhTicketDictionary::GetDropDownArray($ticketSite/*, $ticketSla*/);
$arResult['DICT'] = $ticketDictionary;
$arResult["DICTIONARY"]["CRITICALITY"] = _GetDropDownDictionary("K", $ticketDictionary);
$arResult["DICTIONARY"]["CATEGORY"] = _GetDropDownDictionary("C", $ticketDictionary);
$arResult["DICTIONARY"]["STATUS"] = _GetDropDownDictionary("S", $ticketDictionary);

if (CTszhTicket::IsAdmin()) {
	$rsSupportTeamList = CTszhTicket::GetSupportTeamList();
	$arSupportTeamList = Array();
	while ($arSupportTeamUser = $rsSupportTeamList->GetNext()) {
		$arSupportTeamList[$arSupportTeamUser['REFERENCE_ID']] = $arSupportTeamUser['REFERENCE'];
	}
	$arResult['DICTIONARY']['SUPPORT_TEAM'] = $arSupportTeamList;
}

unset($rsTicket);
unset($rsMessage);
unset($arMessagesFiles);
unset($ticketDictionary);


//Set Title
$arParams["SET_PAGE_TITLE"] = ($arParams["SET_PAGE_TITLE"] == "N" ? "N" : "Y" );

if ($arParams["SET_PAGE_TITLE"] == "Y")
{
	if (empty($arResult["TICKET"]))
		$APPLICATION->SetTitle(GetMessage("SUP_NEW_TICKET_TITLE"));
	else
		$APPLICATION->SetTitle(GetMessage("SUP_EDIT_TICKET_TITLE")." ".($arResult["TICKET"]["NUMBER"] > 0 ? $arResult["TICKET"]["NUMBER"] : $arResult["TICKET"]["ID"]));
}


// ********************* User properties ***************************************************
$arResult["USER_PROPERTIES"] = array("SHOW" => "N");
$arUserFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("TSZH_TICKET", $arParams["ID"], LANGUAGE_ID);
$arEditUserFields = Array();
foreach ($arParams['EDIT_FIELDS'] as $strField) {
	if (substr($strField, 0, 3) == 'UF_') {
		$arEditUserFields[] = $strField;
	}
}
if (count($arEditUserFields) > 0)
{
	foreach ($arUserFields as $FIELD_NAME => $arUserField)
	{
		if (!in_array($FIELD_NAME, $arEditUserFields))
			continue;
		$arUserField["EDIT_FORM_LABEL"] = strLen($arUserField["EDIT_FORM_LABEL"]) > 0 ? $arUserField["EDIT_FORM_LABEL"] : $arUserField["FIELD_NAME"];
		$arUserField["EDIT_FORM_LABEL"] = htmlspecialcharsEx($arUserField["EDIT_FORM_LABEL"]);
		$arUserField["~EDIT_FORM_LABEL"] = $arUserField["EDIT_FORM_LABEL"];
		$arResult["USER_PROPERTIES"]["DATA"][$FIELD_NAME] = $arUserField;
	}
}
if (!empty($arResult["USER_PROPERTIES"]["DATA"]))
	$arResult["USER_PROPERTIES"]["SHOW"] = "Y";
$arResult["bVarsFromForm"] = (strlen($strError) <= 0) ? false : true;
// ******************** /User properties ***************************************************

$this->IncludeComponentTemplate();

?>