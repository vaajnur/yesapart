<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arYesNo = Array(
    "Y" => GetMessage("SUP_DESC_YES"),
    "N" => GetMessage("SUP_DESC_NO"),
);
$arComponentParameters = array(
    "PARAMETERS" => array(
        "ID" => array(
            "NAME" => GetMessage("SUP_CHAT_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => ""
            ),
        "SEF_MODE" => Array(
            "TEMPLATE_ID" => Array(
                "NAME" => GetMessage("SUP_CHAT_TEMPLATE_ID"),
                "TYPE" => "STRING",
                "MULTIPLE" => "N",
                "DEFAULT" => "#ID#.php"
            ),
        ),
        "MESSAGES_PER_PAGE" => Array(
            "NAME" => GetMessage("SUP_CHAT_MESSAGES_PER_PAGE"),
            "TYPE" => "STRING",
            "PARENT" => "ADDITIONAL_SETTINGS",
            "MULTIPLE" => "N",
            "DEFAULT" => "20"
        ),
        "SET_PAGE_TITLE" => Array(
            "NAME"=>GetMessage("SUP_SET_PAGE_TITLE"),
            "TYPE"=>"LIST",
            "MULTIPLE"=>"N",
            "DEFAULT"=>"Y",
            "PARENT" => "ADDITIONAL_SETTINGS",
            "VALUES"=>$arYesNo,
            "ADDITIONAL_VALUES"=>"N"
        ),
    )
)
?>