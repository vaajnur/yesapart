<?
$MESS ['G_TICKETS_GREEN_ALT'] = "последний раз в обращение писали вы";
$MESS ['G_TICKETS_GREY_ALT'] = "обращение закрыто";
$MESS ['G_TICKETS_RED_ALT'] = "последний раз в обращение писал ваш оппонент";
$MESS ['G_TICKETS_TIMESTAMP_X'] = "Последнее изменение";
$MESS ['G_TICKETS_MESSAGES'] = "Всего сообщений";
$MESS ['G_TICKETS_STATUS'] = "Статус";
$MESS ['G_TICKETS_MODIFIED_BY'] = "Автор последнего сообщения";
$MESS ['G_TICKETS_RESPONSIBLE'] = "Ответственный";
$MESS ['G_TICKETS_LIST_EMPTY'] = "Список обращений пуст";
?>
