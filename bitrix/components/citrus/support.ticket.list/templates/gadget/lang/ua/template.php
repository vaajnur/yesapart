<?
$MESS["G_TICKETS_GREEN_ALT"] = "востаннє у зверненні писали ви ";
$MESS["G_TICKETS_GREY_ALT"] = "повідомлення закрито";
$MESS["G_TICKETS_RED_ALT"] = "востаннє у зверненні писав ваш опонент ";
$MESS["G_TICKETS_TIMESTAMP_X"] = "Останні зміни";
$MESS["G_TICKETS_MESSAGES"] = "Всього повідомлень";
$MESS["G_TICKETS_STATUS"] = "Статус";
$MESS["G_TICKETS_MODIFIED_BY"] = "Автор останього повідомлення";
$MESS["G_TICKETS_RESPONSIBLE"] = "Відповідальний";
$MESS["G_TICKETS_LIST_EMPTY"] = "Список звернень порожній";
?>