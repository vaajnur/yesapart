<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule("vdgb.tszhvote"))
{
	ShowError(GetMessage("VOTE_MODULE_IS_NOT_INSTALLED"));
	return;
}

$arParams["GROUP_ID"] = trim($arParams["GROUP_ID"]);
if (is_numeric($arParams["GROUP_ID"]))
{
	$arParams["GROUP_ID"] = IntVal($arParams["GROUP_ID"]);
}
else
{
	$arGroup = CCitrusPollGroup::GetList(
		array(),
		array("CODE" => $arParams["GROUP_ID"]), 
		false,
		array("nTopCount" => 1),
		array("ID")
	)->Fetch();
	$arParams["GROUP_ID"] = IntVal($arGroup["ID"]);
}

$arDefaultUrlTemplates404 = array(
	"vote" => "",
	"detail" => "#VOTING_ID#/",
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array(
	"GROUP_ID",
	"VOTING_ID",
);

if($arParams["SEF_MODE"] == "Y")
{
	$arVariables = array();

	$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
	$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

	$componentPage = CComponentEngine::ParseComponentPath(
		$arParams["SEF_FOLDER"],
		$arUrlTemplates,
		$arVariables
	);

	if(!$componentPage)
	{
		$componentPage = "vote";

		if($arParams["SET_STATUS_404"]==="Y")
		{
			$folder404 = str_replace("\\", "/", $arParams["SEF_FOLDER"]);
			if ($folder404 != "/")
				$folder404 = "/".trim($folder404, "/ \t\n\r\0\x0B")."/";
			if (substr($folder404, -1) == "/")
				$folder404 .= "index.php";

 			if($folder404 != $APPLICATION->GetCurPage(true))
				CHTTP::SetStatus("404 Not Found");
		}
	}

	CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

	$arResult = array(
		"FOLDER" => $arParams["SEF_FOLDER"],
		"URL_TEMPLATES" => $arUrlTemplates,
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases,
	);
}
else
{
	$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
	CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

	$componentPage = "";

	if(isset($arVariables["VOTING_ID"]) && intval($arVariables["VOTING_ID"]) > 0)
		$componentPage = "detail";
	else
		$componentPage = "vote";

	$arResult = array(
		"FOLDER" => "",
		"URL_TEMPLATES" => Array(
			"vote" => htmlspecialcharsbx($APPLICATION->GetCurPage()),
			"detail" => htmlspecialcharsbx($APPLICATION->GetCurPage()."?".$arVariableAliases["VOTING_ID"]."=#VOTING_ID#"),
		),
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases
	);
}

$this->IncludeComponentTemplate($componentPage);

?>