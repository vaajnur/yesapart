<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("WZ_NAME"),
	"DESCRIPTION" => GetMessage("WZ_DESC"),
	"ICON" => "/images/wizard.png",
	"SORT" => 200,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "citrus",
		"NAME" => GetMessage("C_CITRUS"),
		"CHILD" => array(
			"ID" => "tszh_support",
			"NAME" => GetMessage("C_TSZH_TICKETS"),
			"SORT" => 10,
		)
	),
);

?>
