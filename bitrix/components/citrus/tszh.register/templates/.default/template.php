<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

CJSCore::Init(array("jquery"));

global $arrResult;
$arrResult = $arResult;

$arFieldsByGroup = Array(
	Array(
		'TITLE' => GetMessage("REGISTER_GROUP_1"),
		'FIELDS' => Array(
			'LOGIN',
			'PASSWORD',
			'CONFIRM_PASSWORD',
		),
	),
	Array(
		'TITLE' => GetMessage("REGISTER_GROUP_2"),
		'FIELDS' => Array(
			'NAME',
			'SECOND_NAME',
			'LAST_NAME',
			'EMAIL',
			'PERSONAL_PHONE',
			'PERSONAL_CITY',
			'PERSONAL_STREET',
		),
		'PROPERTIES' => Array(
			"UF_ACCOUNT",
		),
	),
);


function ShowField($FIELD)
{
	global $arrResult, $APPLICATION;

	if (substr($FIELD, 0, strlen('UF_')) == 'UF_')
	{

		$arUserField = $arrResult['USER_PROPERTIES']['DATA'][$FIELD];
		$APPLICATION->IncludeComponent(
			"bitrix:system.field.edit",
			$arUserField["USER_TYPE"]["USER_TYPE_ID"],
			array(
				"bVarsFromForm" => $arrResult["bVarsFromForm"],
				"arUserField" => $arUserField,
				"form_name" => "regform",
			),
			null,
			array(
				"HIDE_ICONS" => "Y",
			)
		);
	}
	else
	{
		switch ($FIELD)
		{
			case "PASSWORD":
			case "CONFIRM_PASSWORD":
				?><input size="30" type="password" name="REGISTER[<?=$FIELD?>]" class="styled"><?
				break;

			case "PERSONAL_GENDER":
				?><select name="REGISTER[<?=$FIELD?>]" class="styled">
				<option value=""><?=GetMessage("USER_DONT_KNOW")?></option>
				<option value="M"<?=$arrResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_MALE")?></option>
				<option value="F"<?=$arrResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_FEMALE")?></option>
				</select><?
				break;

			case "PERSONAL_COUNTRY":
				?><select name="REGISTER[<?=$FIELD?>]" class="styled"><?
				foreach ($arrResult["COUNTRIES"]["reference_id"] as $key => $value)
				{
					?>
					<option value="<?=$value?>"<? if ($value == $arrResult["VALUES"][$FIELD]): ?> selected="selected"<? endif ?>><?=$arrResult["COUNTRIES"]["reference"][$key]?></option>
					<?
				}
				?></select><?
				break;

			case "PERSONAL_PHOTO":
			case "WORK_LOGO":
				?><input size="30" type="file" name="REGISTER_FILES_<?=$FIELD?>" class="styled"><?
				break;

			case "PERSONAL_NOTES":
			case "WORK_NOTES":
				?><textarea cols="30" rows="5" name="REGISTER[<?=$FIELD?>]" class="styled"><?=$arrResult["VALUES"][$FIELD]?></textarea><?
				break;
			case "PERSONAL_PHONE":
				?>
				<input type="text" id="customer_phone" value="<?=$arrResult["VALUES"][$FIELD]?>" size="30" class="styled"></br>
				<?// ����� ��� ����� � �������� ������� ��������
				?>
				<input type="hidden" id="phone_mask" checked=""><label id="descr" for="phone_mask"></label>
				<input type="hidden" id="phoneId" name="REGISTER[<?=$FIELD?>]" value="<?=$arrResult["VALUES"][$FIELD]?>">
				<?//����������� ����� �������� ����������� ������ (�������� ��� �����-���� ����� �����)
				?>
				<?
				break;
			default:
				if ($FIELD == "PERSONAL_BIRTHDAY"):?>
					<small><?=$arrResult["DATE_FORMAT"]?></small><br><?endif;
				?><input size="30" type="text" name="REGISTER[<?=$FIELD?>]" value="<?=$arrResult["VALUES"][$FIELD]?>" class="styled"><?
				if ($FIELD == "PERSONAL_BIRTHDAY")
				{
					$APPLICATION->IncludeComponent(
						'bitrix:main.calendar',
						'',
						array(
							'SHOW_INPUT' => 'N',
							'FORM_NAME' => 'regform',
							'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
							'SHOW_TIME' => 'N',
						),
						null,
						array("HIDE_ICONS" => "Y")
					);
				}
		}
	}

	if (strlen(GetMessage("REGISTER_FIELD_" . $FIELD . '_NOTE')) > 0)
	{
		echo '<small><br>' . GetMessage("REGISTER_FIELD_" . $FIELD . '_NOTE') . '</small>';
	}

	if (strlen($arrResult['ERRORS'][$FIELD]) > 0)
	{
		ShowError($arrResult["ERRORS"][$FIELD]);
		unset($arrResult['ERRORS'][$FIELD]);
	}
}

function ShowFieldTitle($FIELD)
{
	global $arrResult;

	$arCustomTitles = Array();

	if (substr($FIELD, 0, strlen('UF_')) == 'UF_')
	{
		$arUserField = $arrResult['USER_PROPERTIES']['DATA'][$FIELD];

		if (array_key_exists($FIELD, $arCustomTitles))
		{
			echo $arCustomTitles[$FIELD] . ':';
		}
		else
		{
			echo $arUserField["EDIT_FORM_LABEL"] . ':';
		}

		if ($arUserField["MANDATORY"] == "Y")
		{
			echo '<span class="required">*</span>';
		}
	}
	else
	{
		echo GetMessage("REGISTER_FIELD_" . $FIELD) . ':';
		if ($arrResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y")
		{
			echo '<span class="starrequired">*</span>';
		}
	}
}

if (count($arrResult["ERRORS"]) > 0)
{
	foreach ($arrResult["ERRORS"] as $key => $error)
	{
		if (intval($key) <= 0)
		{
			$arrResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);
		}
	}

	ShowError(GetMessage("REGISTER_FORM_SEND_ERROR"));
}
elseif ($arrResult["USE_EMAIL_CONFIRMATION"] === "Y")
{
	?><p><? echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p><?
}
?>

<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data" class="registration-form">
	<?
	if (strlen($arrResult["BACKURL"]) > 0)
	{
		echo '<input type="hidden" name="backurl" value="' . $arrResult["BACKURL"] . '">';
	}

	foreach ($arFieldsByGroup as $arFieldGroup)
	{
		?>
		<div class="bx-auth">
			<div class="bx-auth-title"><?=$arFieldGroup['TITLE']?></div>

			<table width="100%" class="bx-auth-table">
				<?
				if (is_array($arFieldGroup['FIELDS']))
				{
					foreach ($arFieldGroup['FIELDS'] as $key => $FIELD)
					{
						if (!in_array($FIELD, $arrResult['SHOW_FIELDS']))
						{
							continue;
						}
						?>
						<tr>
							<td class="field-name bx-auth-label">
								<?=ShowFieldTitle($FIELD)?>
							</td>

							<td><?
								ShowField($FIELD);

								if (strlen($arrResult['ERRORS'][$FIELD]) > 0)
								{
									ShowError($arrResult["ERRORS"][$FIELD]);
									unset($arrResult['ERRORS'][$FIELD]);
								}

								?>
							</td>
						</tr>
						<?
					}
				}

				if (is_array($arFieldGroup['PROPERTIES']))
				{
					foreach ($arFieldGroup['PROPERTIES'] as $key => $sProperty)
					{
						$arUserField = $arrResult['USER_PROPERTIES']['DATA'][$sProperty];
						if (!is_array($arUserField))
						{
							continue;
						}
						?>
						<tr>
							<td class="field-name bx-auth-label">
								<?=$arUserField["EDIT_FORM_LABEL"]?>:<? if ($arUserField["MANDATORY"] == "Y"): ?><span
										class="required">*</span><? endif; ?>
							</td>

							<td>
								<?
								$APPLICATION->IncludeComponent(
									"bitrix:system.field.edit",
									$arUserField["USER_TYPE"]["USER_TYPE_ID"],
									array(
										"bVarsFromForm" => $arrResult["bVarsFromForm"],
										"arUserField" => $arUserField,
										"form_name" => "regform",
									), null, array("HIDE_ICONS" => "Y")); ?>
								<?

								if (strlen($arrResult['ERRORS'][$FIELD]) > 0)
								{
									ShowError($arrResult["ERRORS"][$FIELD]);
									unset($arrResult['ERRORS'][$FIELD]);
								}

								?>
							</td>
						</tr>
						<?
					}
				}

				?>
			</table>
		</div>
		<?
	}

	/* CAPTCHA */
	if ($arrResult["USE_CAPTCHA"] == "Y")
	{
		?>
		<div class="bx-auth">
			<div class="bx-auth-title"><?=GetMessage("REGISTER_CAPTCHA_TITLE")?></div>

			<table width="100%" class="no-border bx-auth-table">
				<tr>
					<td></td>
					<td><input type="hidden" name="captcha_sid" value="<?
						echo $arResult["CAPTCHA_CODE"] ?>"/>
						<img src="/bitrix/tools/captcha.php?captcha_sid=<?
						echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA"></td>
				</tr>
				<tr>
					<td class="bx-auth-label"><?
						echo GetMessage("REGISTER_CAPTCHA_PROMT") ?>:
					</td>
					<td><input class="bx-auth-input styled" type="text" name="captcha_word" maxlength="50" value="" size="15"></td>
				</tr>
			</table>
		</div>
		<?
	}
	/* CAPTCHA */


	if (count($arrResult['ERRORS']) > 0)
	{
		ShowError(implode("<br>", $arrResult["ERRORS"]));
		?><br><?
	}

	?>
	<div class="bx-auth bx-auth-footer">
		<table width="100%" class="no-border bx-auth-table">
			<tr>
				<td class="bx-auth-label"></td>
				<td>
					<? if ($arResult["CONFIRM_TSZH"] == "Y")
					{
					?>

					<input name="confirm" class=confirm type="checkbox" required>
					<p><?=$arResult["TSZH_DATA"]["~CONFIRM_TEXT"]?><br>
						<? } ?>
						<?
						if (function_exists("TemplateShowButton"))
						{
							TemplateShowButton(array(
								"type" => "submit",
								"title" => GetMessage("AUTH_REGISTER_BUTTON"),
								"attr" => array(
									"name" => "register_submit_button",
									"value" => GetMessage("AUTH_REGISTER_BUTTON"),
									"onclick" => "getValuePhone()",
								),
							));
						}
						else
						{
							?>
							<button class="submit" type="submit" name="register_submit_button" value="1"
							        onclick="getValuePhone()"><?=GetMessage("AUTH_REGISTER_BUTTON")?></button>
							<?
						}
						?>
				</td>
			</tr>
		</table>
	</div>
	<?

	echo "</form>";
	?>
	<br>

	<p><? echo $arrResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; ?></p>

	<p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>

</form>
<script type="text/javascript">
    function getValuePhone() {
        document.getElementById('phoneId').value = document.getElementById('customer_phone').value;
    }
</script>
<script>
    var listCountries = $.masksSort($.masksLoad("<?= $this->GetFolder() ?>/json/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
    var maskOpts = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            showMaskOnHover: false,
            autoUnmask: true,
            clearMaskOnLostFocus: false
        },
        match: /[0-9]/,
        replace: '#',
        listKey: "mask"
    };

    var maskChangeWorld = function (maskObj, determined) {
        if (determined) {
            var hint = maskObj.name_ru;
            if (maskObj.desc_ru && maskObj.desc_ru != "") {
                hint += " (" + maskObj.desc_ru + ")";
            }
            $("#descr").html(hint);
        } else {
            $("#descr").html("");
        }
    }

    $('#phone_mask, input[name="mode"]').change(function () {
        $('#customer_phone').inputmasks($.extend(true, {}, maskOpts, {
            list: listCountries,
            onMaskChange: maskChangeWorld
        }));
    });

    $('#phone_mask').change();
</script>