<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponent $this
 */
use Citrus\Tszh\Types\ComponentType;
use Bitrix\Main\Application;
use Citrus\Tszh\Types\ReceiptType;
/** @var CBitrixComponent $this */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule("citrus.tszh"))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));
	return;
}

if (!CTszhFunctionalityController::CheckEdition())
	return;

global $USER, $DB;
$app = Application::getInstance();
$request = $app->getContext()->getRequest();
// ������ �� ���� ���������
$sheet_type = $request->get("type");

if ($sheet_type != 'overhaul')
{
	$arResult["SHEET_TYPE"] = ReceiptType::MAIN;
	$fType = array(ReceiptType::MAIN, ReceiptType::FINES_MAIN);
}
else
{
	$arResult["SHEET_TYPE"] = ReceiptType::OVERHAUL;
	$fType = array(ReceiptType::OVERHAUL, ReceiptType::FINES_OVERHAUL);

}
$arResult["MAIN_URL"] = $APPLICATION->GetCurPageParam("type=main", array("type"));
$arResult["OVERHAUL_URL"] = $APPLICATION->GetCurPageParam("type=overhaul", array("type"));
$acc = CTszhAccount::GetByUserID($USER->GetID());
$arResult['USER_TSZH'] = $tszh = CTszh::GetByID($acc['TSZH_ID']);
$this->IncludeComponentTemplate();
