<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("citrus.tszhpayment"))
{
	ShowError(GetMessage("MODULE_NOT_INSTALL"));
	return;
}

$arFilter = Array(
	"ACTIVE" => "Y",
	"LID" => SITE_ID,
	"ID" => $arParams["PAY_SYSTEM_ID"],
);
$rsPaySytems = CTszhPaySystem::GetList(Array("SORT" => "ASC"), $arFilter);
if ($arPaySystem = $rsPaySytems->GetNext())
{
	if (strlen($arPaySystem["ACTION_FILE"]) > 0)
	{
		$GLOBALS["TSZH_PAYMENT_CORRESPONDENCE"] = CTszhPaySystem::UnSerializeParams($arPaySystem["PARAMS"]);
		$pathToAction = $_SERVER["DOCUMENT_ROOT"].$arPaySystem["ACTION_FILE"];

		$pathToAction = str_replace("\\", "/", $pathToAction);
		while (substr($pathToAction, strlen($pathToAction) - 1, 1) == "/")
			$pathToAction = substr($pathToAction, 0, strlen($pathToAction) - 1);
		
		$APPLICATION->RestartBuffer();

		if (file_exists($pathToAction))
		{
			if (is_dir($pathToAction))
			{
				if (file_exists($pathToAction."/result_rec.php"))
					include($pathToAction."/result_rec.php");
			}
		}
		if(strlen($arPaySystem["ENCODING"]) > 0)
		{
			define("BX_TSZH_PAYMENT_ENCODING", $arPaySystem["ENCODING"]);
			if (!function_exists('TszhPaymentChangeEncoding')):
				AddEventHandler("main", "OnEndBufferContent", "TszhPaymentChangeEncoding");
				function TszhPaymentChangeEncoding(&$content)
				{
					global $APPLICATION;
					header("Content-Type: text/html; charset=".BX_TSZH_PAYMENT_ENCODING);
					$content = $APPLICATION->ConvertCharset($content, SITE_CHARSET, BX_TSZH_PAYMENT_ENCODING);
					$content = str_replace("charset=".SITE_CHARSET, "charset=".BX_TSZH_PAYMENT_ENCODING, $content);
				}
			endif;
		}
		die();

	}
}
?>