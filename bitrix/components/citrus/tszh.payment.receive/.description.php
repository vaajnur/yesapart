<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SOP_DEFAULT_TEMPLATE_NAME"),
	"DESCRIPTION" => GetMessage("SOP_DEFAULT_TEMPLATE_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"PATH" => array(
		"ID" => "tszh",
		"NAME" => GetMessage("COMPONENT_ROOT_SECTION"),
		"CHILD" => array(
			"ID" => "tszh_personal",
			"NAME" => GetMessage("COMPONENT_SECTION"),
			"SORT" => 10,
		)
	),
);
?>