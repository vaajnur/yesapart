<?php
namespace Vdgb\Documents;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Helper {
	const MODULE_ID = "vdgb.documents";
	const FILE_PATH = "vdgb.documents";

	/**
	 * ����� ������� ��� ���������������� �����
	 *
	 * @param array $arFields ������ � ��������� �����
	 * @return array
	 */
	public static function adminFilterParams($arFields)
	{
	    $ar = Array("find", "find_types");

	    foreach ($arFields as $arField) {
	        switch ($arField['type']) {
	            case 'int':
	            case 'float':
	            case 'date':
	            case 'datetime':
	                $ar[] = 'find_' . strtolower($arField['id']) . '_from';
	                $ar[] = 'find_' . strtolower($arField['id']) . '_to';
	                break;
	            case 'text':
	            case 'file':
	            case 'string':
	            default:
	                $ar[] = 'find_' . strtolower($arField['id']);
	                break;
	        }
	    }
	    return $ar;
	}

	/**
	 * ������������ ���������� ������� ��� ���������������� �����
	 *
	 * @param array $arFilter �������������� ������ � ��������
	 * @param array $arFields ������ � ��������� �����
	 * @return void
	 */
	function adminMakeFilter(&$arFilter, $arFields)
	{
	    global $DB;

	    if ($GLOBALS['find'] != "" && ToLower($GLOBALS['find_filter_types']) == "id") {
	        if (IntVal($GLOBALS['find']) > 0)
	            $arFilter["ID"] = IntVal($GLOBALS['find']);
	        elseif (IntVal($GLOBALS['find_id']) > 0)
	            $arFilter["ID"] = IntVal($GLOBALS['find_id']);
	    } else {
	        if (IntVal($GLOBALS['find_id']) > 0)
	            $arFilter["ID"] = IntVal($GLOBALS['find_id']);
	    }
	    if ($GLOBALS['find'] != "" && ToLower($GLOBALS['find_filter_types']) == "document_id") {
	    	$arFilter['%DOCUMENT.ORIGINAL_NAME'] = $GLOBALS['find'];
	    }
	    if ($GLOBALS['find'] != "" && ToLower($GLOBALS['find_filter_types']) == "entity_id") {
	    	$arFilter['ENTITY_ID'] = IntVal($GLOBALS['find']);
	    }
	    foreach ($arFields as $arField) {
	        $varName = 'find_' . ToLower($arField['id']);
	        $field = $arField['id'];

	        switch ($arField['type']) {
	            case 'int':
	            case 'float':
	                $val = &$GLOBALS[$varName];
	                if (isset($val) && strlen($val) > 0 && is_numeric($val))
	                    $arFilter[$field] = $arField['type'] == 'float' ? FloatVal($val) : IntVal($val);
	                else {
	                    $val = &$GLOBALS[$varName . '_from'];
	                    if (isset($val) && strlen($val) > 0 && is_numeric($val))
	                        $arFilter['>=' . $field] = $arField['type'] == 'float' ? FloatVal($val) : IntVal($val);
	                    $val = &$GLOBALS[$varName . '_to'];
	                    if (isset($val) && strlen($val) > 0 && is_numeric($val))
	                        $arFilter['<=' . $field] = $arField['type'] == 'float' ? FloatVal($val) : IntVal($val);
	                }
	                break;

	            case 'date':
	            case 'datetime':
	                $bFullDate = $arField['type'] == 'datetime';
	                $val = &$GLOBALS[$varName . '_from'];
	                if (isset($val) && strlen($val) > 0 && $DB->IsDate($val, false, false, $bFullDate ? "FULL" : "SHORT"))
	                    $arFilter['>=' . $field] = $val;
	                $val = &$GLOBALS[$varName . '_to'];
	                if (isset($val) && strlen($val) > 0 && $DB->IsDate($val, false, false, $bFullDate ? "FULL" : "SHORT"))
	                    $arFilter['<=' . $field] = $val;

	                $val = &$GLOBALS[$varName . '_from_DAYS_TO_BACK'];
	                if (isset($val) && strlen($val) > 0 && is_numeric($val))
	                    $arFilter['>=' . $field] = ConvertTimestamp($val > 0 ? strtotime('-' . IntVal($val) . ' days') : time());
	                break;
	            
	            case 'text':
	            case 'file':
	            case 'string':
	                $val = &$GLOBALS[$varName];
	                if (isset($val) && strlen(trim($val)) > 0)
	                    $arFilter['%' . $field] = $val;
	                break;

	            case 'list':
	            case 'checkbox':
	            default:
	                $val = &$GLOBALS[$varName];
	                if (isset($val) && strlen(trim($val)) > 0)
	                    $arFilter[$field] = $val;
	                break;
	        }
	    }
	}

	/**
	 * ����� ������� ��� ���������������� �����
	 *
	 * @param $arFields
	 * @param array $arFindTypes
	 * @internal param array $arFindType ������ � ��������� �����
	 * @return void
	 */
	public static function showAdminFilter($arFields, $arFindFilterTypes = array("ID" => "ID"))
	{
	    ?>
	    <tr>
	        <td><b><?= GetMessage("DOCUMENTS_HELPER_FIND") ?>:</b></td>
	        <td>
	            <input type="text" size="25" name="find" value="<? echo htmlspecialcharsbx($GLOBALS['find']) ?>"
	                   title="<?= GetMessage("DOCUMENTS_HELPER_FIND_TITLE") ?>">
	            <?
	            $arSelectBox = Array();
	            foreach ($arFindFilterTypes as $id => $val) {
	                $arSelectBox["REFERENCE"][] = $val;
	                $arSelectBox["REFERENCE_ID"][] = $id;
	            }
	            echo SelectBoxFromArray("find_filter_types", $arSelectBox, $GLOBALS['find_filter_types'], "", "");
	            ?>
	        </td>
	    </tr>
	    <?
	    foreach ($arFields as $arField) {
	        $fieldName = 'find_' . strtolower($arField['id']);
	        ?>
	        <tr>
	            <td><?= $arField['content'] ?></td>
	            <td>
	                <?
	                if (is_set($arField['filterHtml']))
	                    echo $arField['filterHtml'];
	                else
	                    switch ($arField['type']) {
	                        case 'int':
	                        case 'float':
	                            ?>
	                            <?= GetMessage("DOCUMENTS_HELPER_FILTER_FROM") ?>
	                            <input type="text" name="<?= $fieldName ?>_from" size="10"
	                                   value="<? echo htmlspecialcharsbx($GLOBALS[$fieldName . '_from']) ?>">
	                            <?= GetMessage("DOCUMENTS_HELPER_FILTER_TO") ?>
	                            <input type="text" name="<?= $fieldName ?>_to" size="10"
	                                   value="<? echo htmlspecialcharsbx($GLOBALS[$fieldName . '_to']) ?>">
	                            <?
	                            break;

	                        case 'list':
	                            $arItems = Array("REFERENCE_ID" => Array(''), "REFERENCE" => Array(GetMessage("DOCUMENTS_HELPER_FILTER_LIST_ALL")));
	                            if (is_array($arField['items'])) {
	                                foreach ($arField['items'] as $id => $val) {
	                                    $arItems["REFERENCE_ID"][] = $id;
	                                    $arItems["REFERENCE"][] = $val;
	                                }
	                            } elseif (is_object($arField['items'])) {
	                                while ($ar = $arField['items']->GetNext()) {
	                                    $arItems["REFERENCE_ID"][] = $ar['ID'];
	                                    $arItems["REFERENCE"][] = '[' . $ar['ID'] . '] ' . $ar['NAME'];
	                                }
	                            }
	                            echo SelectBoxFromArray($fieldName, $arItems, $GLOBALS[$fieldName], "", "");
	                            ?>

	                            <?
	                            break;

	                        case 'checkbox':
	                            $arItems = Array(
	                                "REFERENCE_ID" => Array('', 'Y', "N"),
	                                "REFERENCE" => Array(GetMessage("DOCUMENTS_HELPER_FILTER_LIST_ALL"), GetMessage("DOCUMENTS_HELPER_YES"), GetMessage("DOCUMENTS_HELPER_NO"))
	                            );
	                            echo SelectBoxFromArray($fieldName, $arItems, $GLOBALS[$fieldName], "", "");
	                            ?>

	                            <?
	                            break;

	                        case 'date':
	                        case 'datetime':
	                            echo CalendarPeriod($fieldName . '_from', htmlspecialcharsbx($GLOBALS[$fieldName . '_from']), $fieldName . '_to', htmlspecialcharsbx($GLOBALS[$fieldName . '_to']), "form_filter", "Y");
	                            break;

	                        case 'file':
	                        case 'string':
	                        default:
	                            ?>
	                                <input type="text" name="find_<?= strtolower($arField['id']) ?>" size="40"
	                                       value="<? echo htmlspecialchars($GLOBALS['find_' . strtolower($arField['id'])]) ?>">
	                            <?
	                            break;
	                    }
	                ?>
	            </td>
	        </tr>
	    <?
	    }
	}

	/**
	 * ������� ���������� �����
	 *
	 * @param  String $file ��� ��������� � ������� $_FILES
	 * @return Integer ���������� �� ������������ ����� ��� 0 � ������ ������ 
	 */
	public static function saveFile($file, $oldFile)
	{
		/*if (!isset($_FILES[$file]))
			return 0;
		$arParam = $_FILES[$file];
		$arParam['del'] = 'N';
		$arParam['MODULE_ID'] = self::MODULE_ID;
		return CFile::SaveFile(
			$arParam,
			self::FILE_PATH
		);*/
	} 
}