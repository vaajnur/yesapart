<?php
namespace Vdgb\Documents;

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\TextField;
use Bitrix\Main\Entity\ScalarField;
use Bitrix\Main\Entity\Validator;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\DB;

Loc::loadMessages(__FILE__);

class DocumentTable extends DataManager
{
    /**
     * @var array|null
     */
    private static $editableMap = null;

    public static function getTableName()
    {
        return 'document_table';
    }

    public static function getMap()
    {
        return array(
            new IntegerField('ID', array(
                'autocomplete' => true,
                'primary' => true,
                'title' => Loc::getMessage('ID'),
            )),
            new IntegerField('DOCUMENT_ID', array(
                'required' => true,
                'title' => Loc::getMessage('DOCUMENT_ID'),
                'validation' => function () {
                    return array(
                        function ($value) {
                            $arFile = \Bitrix\Main\FileTable::getList(array(
                                'filter' => array('ID' => $value, 'MODULE_ID' => Helper::MODULE_ID)
                            ))->fetch();
                            if (!empty($arFile)) {
                                return true;
                            } else {
                                return Loc::getMessage('ERR_DOCUMENT_FILE_NOT_EXISTS');
                            }
                        }
                    );
                },
            )),
            new IntegerField('ENTITY_ID', array(
                'required' => true,
                'title' => Loc::getMessage('ENTITY_ID'),
                'validation' => function () {
                    return array(
                        function ($value) {
                            if (is_numeric($value) && intval($value) > 0) {
                                return true;
                            } else {
                                return Loc::getMessage('ERR_DOCUMENT_ID_NEGATIVE');
                            }
                        }
                    );
                },
            )),
            new TextField('DOCUMENT_DESCRIPTION', array(
                'required' => true,
                'title' => Loc::getMessage('DOCUMENT_DESCRIPTION'),
            )),
            new IntegerField('SORT', array(
                'required' => false,
                'title' => Loc::getMessage('DOCUMENT_SORT'),
                'default_value' => 100
            )),
            new ReferenceField(
                'DOCUMENT',
                'Bitrix\Main\FileTable',
                array(
                    '=this.DOCUMENT_ID' => 'ref.ID',
                    '=ref.MODULE_ID' => new DB\SqlExpression('?s', Helper::MODULE_ID)
                ),
                array('join_type' => 'LEFT')
            )
        );
    }

    /**
     * ���������� ������ ������������� ����� ���������� ������ getMap()
     * @return array
     */
    public static function getEditableFields()
    {
        if (!isset(static::$editableMap))
        {
            static::$editableMap = array();
            $map = static::getMap();
            foreach ($map as $field)
            { 
                if ($field instanceof ReferenceField) {
                    continue;
                }
                static::$editableMap[$field->getColumnName()] = $field;
            }
        }

        return static::$editableMap;
    }

    /**
     * ����� ��������� ������ ��� ������ ������ � ���������������� �����
     *
     * @param array $arFilter
     * @return array
     */
    public static function getHeadersList($arFilter = array())
    {
        $defaultAlign = "left";
        $defaultVisible = true;

        $arFields = self::getEditableFields();
        /*$arFields['DOCUMENT_NAME'] = new StringField('DOCUMENT_NAME', 
            array('title' => Loc::getMessage('DOCUMENT_NAME'))
        );*/
        $arHeaders = array();

        foreach($arFields as $key => $field)
        {
            $arHeaders[] = array(
                "id"        => $key,
                "align"     => is_set($arFilter["align"][$key]) ? $arFilter["align"][$key] : $defaultAlign,
                "default"   => is_set($arFilter["visible"][$key]) ? $arFilter["visible"][$key] : $defaultVisible,
                "type"      => is_set($arFilter["type"][$key]) ? $arFilter["type"][$key] : $field->getDataType(),
                "content"   => $field->getTitle(),
                "sort"      => $key,
                "items"     => is_set($arFilter["items"][$key]) ? $arFilter["items"][$key] : array(),
            );
        }

        return $arHeaders;
    }

    /**
     * ����� ���������� ������ � ���������� �����
     *
     * @return mixed
     */
    public static function getTitles()
    {
        $map = static::getMap();
        foreach ($map as $fieldName => $fieldSettings)
        {
            $arTitles[$fieldName] = $fieldSettings->getTitle();
        }

        return $arTitles;
    }

    /**
     * ����� ���������� ���-�� ������� � ����������� �� ������
     * @param Array $filter ������ ��� getList
     * @return int ���-�� �������
     */
    public static function getCount($filter = array()) {
        $count = self::getList(array(
            'select' => array('CNT'),
            'filter' => $filter,
            'runtime' => array(
                new ExpressionField("CNT", "COUNT(*)")
            )
            ))->fetch();
        return intval($count['CNT']);
    }
}
