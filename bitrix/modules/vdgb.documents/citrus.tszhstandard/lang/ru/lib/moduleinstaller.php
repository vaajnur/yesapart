<?php
$MESS['TSZH_ERROR_MODULE_LOAD'] = 'Ошибка загрузка модуля из Marketplace: #ERROR#';
$MESS['TSZH_ERROR_MODULE_OBJECT'] = 'Ошибка при установке модуля, объект модуля не создан';
$MESS['TSZH_ERROR_MODULE_DO_INSTALL'] = 'Ошибка при установке модуля';

$MESS['TSZH_ERROR_MODULE_DB'] = 'Ошибка на этапе установки БД модуля.';
$MESS['TSZH_ERROR_MODULE_DB'] = 'Ошибка на этапе установки почтовых событий модуля.';
$MESS['TSZH_ERROR_MODULE_FILES'] = 'Ошибка копирования файлов модуля.';
$MESS['TSZH_ERROR_INSTALLING_MODULE'] = 'Установка модуля #MODULE_ID#: ';
$MESS['TSZH_AUDIT_TYPE_TSZH_MODULE_AUTOINSTALL_SUCCESS'] = 'Автоматическая установка модулей ЖКХ';
$MESS['TSZH_AUDIT_TYPE_TSZH_MODULE_AUTOINSTALL_ERROR'] = 'Ошибки во время выполнения автоматической установки модулей ЖКХ';
?>