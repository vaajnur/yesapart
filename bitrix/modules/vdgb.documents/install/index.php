<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Vdgb\Documents\DocumentTable;

if (class_exists('vdgb_documents'))
{
	return;
}

class vdgb_documents extends CModule
{
	/** @var string */
	var $MODULE_ID = 'vdgb.documents';

	/** @var string */
	public $MODULE_VERSION;

	/** @var string */
	public $MODULE_VERSION_DATE;

	/** @var string */
	public $MODULE_NAME;

	/** @var string */
	public $MODULE_DESCRIPTION;

	/** @var string */
	public $MODULE_GROUP_RIGHTS;

	/** @var string */
	public $PARTNER_NAME;

	/** @var string */
	public $PARTNER_URI;

	public function __construct()
	{
		$arModuleVersion = array();

		include(dirname(__FILE__) . "/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else
		{
			$this->MODULE_VERSION = '1.0.0';
			$this->MODULE_VERSION_DATE = '2017-08-30 16:00:00';
		}

		$this->MODULE_NAME = Loc::getMessage('VDGB_DOCUMENTS_MODULE_NAME');
		$this->MODULE_DESCRIPTION = Loc::getMessage('VDGB_DOCUMENTS_MODULE_DESCRIPTION');
		$this->MODULE_GROUP_RIGHTS = 'N';
		$this->PARTNER_NAME = Loc::getMessage('VDGB_DOCUMENTS_PARTNER_NAME');;
		$this->PARTNER_URI = Loc::getMessage('VDGB_DOCUMENTS_PARTNER_URI');;
	}

	public function doInstall()
	{
		$this->installFiles();
		$this->installDB();
	}

	public function doUninstall()
	{
		$this->uninstallDB();
		$this->unInstallFiles();
	}

	public function installDB()
	{
		ModuleManager::registerModule($this->MODULE_ID);

		if (Loader::includeModule($this->MODULE_ID))
		{
			DocumentTable::getEntity()->createDbTable();
		}
	}

	public function uninstallDB()
	{
		if (Loader::includeModule($this->MODULE_ID))
		{
			$connection = Application::getInstance()->getConnection();
			$connection->dropTable(DocumentTable::getTableName());
		}

		ModuleManager::unregisterModule($this->MODULE_ID);
	}

	public function installFiles()
	{
		CopyDirFiles(
			Application::getDocumentRoot() . "/bitrix/modules/" . $this->MODULE_ID . "/install/admin",
			Application::getDocumentRoot() . "/bitrix/admin",
			true,
			true
		);
		CopyDirFiles(
			Application::getDocumentRoot() . "/bitrix/modules/" . $this->MODULE_ID . "/install/components",
			Application::getDocumentRoot() . "/bitrix/components",
			true,
			true
		);
	}

	public function unInstallFiles()
	{
		DeleteDirFiles(
			Application::getDocumentRoot() . "/bitrix/modules/" . $this->MODULE_ID . "/install/admin",
			Application::getDocumentRoot() . "/bitrix/admin"
		);
		DeleteDirFiles(
			Application::getDocumentRoot() . "/bitrix/modules/" . $this->MODULE_ID . "/install/components",
			Application::getDocumentRoot() . "/bitrix/components"
		);
	}
}
