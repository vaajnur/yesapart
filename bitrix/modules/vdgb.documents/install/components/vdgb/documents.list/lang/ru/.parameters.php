<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$MESS['DOC_PARAM_ENTITY_ID'] = "Идентификатор";
$MESS['DOC_PARAM_SORT'] = "Поле для сортировки";
$MESS['DOC_PARAM_DESC'] = "Порядок сортировки";
$MESS['DOC_PARAM_DESC_ASC'] = "По возрастанию";
$MESS['DOC_PARAM_DESC_DESC'] = "По убыванию";
$MESS['DOC_PARAM_SHOW_DESCRIPTION'] = "Показывать описание";
$MESS['DOC_PARAM_CACHE_GROUPS'] = "Учитывать права доступа";
$MESS['DOC_PARAM_COUNT_PAGE'] = "Количество элементов на странице";
$MESS['DOC_PARAM_SHOW_FIELDS'] = "Выводимые поля";