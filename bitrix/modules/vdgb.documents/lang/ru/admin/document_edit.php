<?php
$MESS['DOC_EDIT_ACCESS_DENIED'] = "Доступ на редактирование документа запрещен";
$MESS['DOC_EDIT_TAB_ONE'] = "Документ";
$MESS['DOC_EDIT_TAB_ONE_TITLE'] = "Редактирование документа";
$MESS['DOC_EDIT_SAVE_ERROR'] = "Ошибка сохранения";
$MESS['DOC_EDIT_BACK'] = "Назад";
$MESS['DOC_EDIT_ADD'] = "Добавить";
$MESS['DOC_EDIT_DELETE'] = "Удалить";
$MESS['DOC_EDIT_CONFIRM_DELETE'] = "Вы уверены, что хотите удалить запись?";
$MESS['DOC_EDIT_CONFIRM_DELETE_FILE'] = "Вы уверены, что хотите удалить выбранный файл?";