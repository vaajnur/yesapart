<?php
$MESS['DOCUMENTS_ACCESS_DENIED'] = "Доступ запрещен";
$MESS["DOCUMENTS_DELETE_OK"] = "Записи успешно удалены";
$MESS["DOCUMENTS_DELETE_ERROR"] = "Ошибка при удалении записи.";
$MESS["DOCUMENTS_DELETE_ITEM_NOT_FOUND"] = "Запись с ID = #ID# не найдена.";
$MESS["DOCUMENTS_GROUP_SAVE_ERROR"] = "Ошибка при сохранении записи";
$MESS["DOCUMENTS_GROUP_SAVE_ERROR_ITEM_NOT_FOUND"] = "Запись с ID = #ID# не найдена.";
$MESS["DOCUMENTS_SETTINGS"] = "Индивидуальные настройки";
$MESS["DOCUMENTS_SETTINGS_LIST"] = "Список настроек";
$MESS["DOCUMENTS_YES"] = "Да";
$MESS["DOCUMENTS_NO"] = "Нет";

$MESS["DOCUMENTS_SETTINGS_UPDATE"] = "Изменить настройки для лицевого счета";
$MESS["DOCUMENTS_SETTINGS_UPDATE_TITLE"] = "Изменить настройки для лицевого счета";
$MESS["DOCUMENTS_SETTINGS_EDIT_TITLE"] = "Редактировать настройки";
$MESS["DOCUMENTS_CARS_LIST_TITLE"] = "Список автомобилей";
$MESS["DOCUMENTS_SETTINGS_GROUP_DELETE_TITLE"] = "Удалить";
$MESS["DOCUMENTS_SETTINGS_GROUP_DELETE_TITLE_CONFIRM"] = "Вы действительно хотите удалить выбранные настройки?";
$MESS["DOCUMENTS_SETTINGS_UPDATE"] = "Изменить настройки для лицевого счета";
$MESS["DOCUMENTS_SETTINGS_UPDATE_TITLE"] = "Изменить настройки для лицевого счета";
$MESS["DOCUMENTS_SETTINGS"] = "Настройки документов";
$MESS["DOCUMENTS_DEFAULT_SETTINGS_TITLE"] = "Настройки, которые действуют для всех лицевых счетов по умолчанию:";
$MESS["DOCUMENTS_DEFAULT_SETTINGS_DETAIL"] = "
    Количество постоянных пропусков - <b>PERMANENT_NUMBER</b> шт.<br/>
    Количество временных пропусков - <b>TEMPORARY_NUMBER</b> шт.<br/>
    Период блокировки постоянных пропусков - <b>PERMANENT_PERIOD</b> дн.<br/>
    Период блокировки временных номеров - <b>TEMPORARY_PERIOD</b> ч.<br/>
    Период пребывания для временных пропусков - <b>TEMPORARY_MAX_TIME</b> дн.<br/><br/>
    <a href='#URL#'>Изменить настройки по умолчанию</a>
    ";