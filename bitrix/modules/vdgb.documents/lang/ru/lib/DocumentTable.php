<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['ID'] = 'ID';
$MESS['DOCUMENT_NAME'] = 'Имя документа';
$MESS['DOCUMENT_ID'] = 'Документ';
$MESS['ENTITY_ID'] = 'Идентификатор дома';
$MESS['DOCUMENT_DESCRIPTION'] = 'Описание (выводится в публичной части)';
$MESS['DOCUMENT_SORT'] = 'Сортировка';
$MESS['ERR_DOCUMENT_ID_NEGATIVE'] = 'Идентификатор должен быть положительным и отличным от нуля.';
$MESS['ERR_DOCUMENT_FILE_NOT_EXISTS'] = 'Выбранного документа не существет.';
