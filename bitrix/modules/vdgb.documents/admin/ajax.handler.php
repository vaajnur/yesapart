<?php
use Vdgb\Documents\DocumentTable;
use Vdgb\Documents\Helper;


require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
CUtil::JSPostUnescape();
CUtil::decodeURIComponent($_FILES);
CModule::IncludeModule("vdgb.documents"); //� ���� ����� �������� � ����������� �� ����������� ���������� ������, ��� ��������� ������� ���� �����
IF (isset($_POST["add_element"]))
{
    if (!empty($_FILES))
    {
        $fId = CFile::SaveFile(array_merge($_FILES["IMAGE_ID"],array("MODULE_ID" => "vdgb.documents")),"/vdgb.documents");
    }
    $arFields = array(
        "DOCUMENT_ID" => $fId,
        "ENTITY_ID" => $_POST["entity_id"],
        "DOCUMENT_DESCRIPTION" => $_FILES["IMAGE_ID"]["name"],
    );
    $rsData = DocumentTable::add($arFields);
    if ($rsData->isSuccess())
    {
        $rsData = DocumentTable::getById($rsData->getId())->fetch();
        $rsData["DOCUMENT_ID"] = CFile::GetPath($rsData["DOCUMENT_ID"]);
    }
}
IF (isset($_POST['table_headers']))
{
    $rsData = DocumentTable::getHeadersList();
    unset ($rsData[1], $rsData[2]);
    $rsData = array_values($rsData);
}
IF (isset($_POST['table_data']))
{
    $rsData = DocumentTable::getList(
        array(
            'select' => array("ID","DOCUMENT_DESCRIPTION","DOCUMENT_ID","SORT"),
            'filter' => array("ENTITY_ID" => $_POST["entity_id"]),
        )
    )->fetchAll();
    if (is_array($rsData))
    {
        foreach($rsData as $rsDataIter => &$rsDataItem)
        {
            $rsDataItem["DOCUMENT_ID"] = CFile::GetPath($rsDataItem["DOCUMENT_ID"]);
        }
    }
}
IF (isset($_POST['delete_id']))
{
    $rsData = DocumentTable::getList(
        array(
            "select"=>array("DOCUMENT_ID"),
            "filter" => array("ID" => $_POST["delete_id"])
        )
    )->fetch();
    CFile::Delete($rsData["DOCUMENT_ID"]);
    $rsData = DocumentTable::Delete($_POST['delete_id']);
    if ($rsData->isSuccess())
    {
        $rsData = $_POST['delete_id'];
    }
}
$temp = \Bitrix\Main\Web\Json::encode($rsData, $options = null);
echo $temp;