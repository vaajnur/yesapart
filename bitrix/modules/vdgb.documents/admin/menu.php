<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$aMenu = array(
    array(
        'parent_menu' => 'global_menu_services',
        'sort' => 400,
        'text' => Loc::getMessage("DOCUMENTS_DOCUMENTS"),
        'title' => Loc::getMessage("DOCUMENTS_DOCUMENTS"),
        'url' => 'documents_list.php',
        'items_id' => 'menu_references',
        'icon' => 'adm-submenu-item-link-icon'
    ),
);

return $aMenu;
