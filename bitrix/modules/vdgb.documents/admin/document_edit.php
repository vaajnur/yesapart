<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\DB;
use Bitrix\Main\Entity;
use Vdgb\Documents\DocumentTable;
use Vdgb\Documents\Helper;

Loc::loadMessages(__FILE__);
Loader::includeModule('vdgb.documents');
global $APPLICATION;

// ������ �������� ����������
$application = Application::getInstance();
// ������ ���� ��������
$request = $application->getContext()->getRequest();
// ������������� �������������� ��������
$ID = intval($request->getQuery('ID'));
// ������ �����
$arFields = array();
// ��������� �� ������
$message = "";
// �������� url, � �������� ������� � ��������������
$backUrl = !is_null($request->get('backUrl')) && strlen($request->get('backUrl')) > 0 ? $request->get('backUrl') : 'documents_list.php?lang=' . LANG;

// ����� �� ������
$modulePermissions = $APPLICATION->GetGroupRight(Helper::MODULE_ID);
if ($modulePermissions <= "D")
{
	$APPLICATION->AuthForm(Loc::getMessage("DOC_EDIT_ACCESS_DENIED"));
}
// ������������� ����
$arEditableFields = DocumentTable::getEditableFields();
// ���������� � �����������
$obUploadDir = new Directory($application->getDocumentRoot() . "/upload/" . Helper::FILE_PATH);
if (!$obUploadDir->isExists())
{                        // ���� �� ���������, �� �������
	Directory::createDirectory($application->getDocumentRoot() . "/upload/" . Helper::FILE_PATH);
}
// ��������� ���������� ������
if (
	$REQUEST_METHOD == "POST"    // �������� ������ ������ ��������
	&&
	($request->get("save") != "" || $request->get("apply") != "") // �������� ������� ������ "���������" � "���������"
	&&
	$modulePermissions >= "W"    // �������� ������� ���� �� ������ ��� ������
	&&
	check_bitrix_sessid()        // �������� �������������� ������*/
)
{
	$arParams = $request->getPostList(); // �������� ������ ���������� ����� ����� ����

	foreach ($arEditableFields as $name => $obField)
	{
		if (
			!$obField->isAutocomplete()
			&&
			isset($arParams[$name])
			&&
			!empty($arParams[$name])
			||
			$obField->isRequired()
		)
		{
			$arFields[$name] = $arParams[$name];
		}
	}
	$arFile = $request->getFile('DOCUMENT_ID');
	// C��������� ������
	if (!empty($arFile['name']))
	{    // ���� ���� � ���������� ������������
		$arFile['del'] = 'N';
		$arFile['MODULE_ID'] = Helper::MODULE_ID;
		$fid = CFile::SaveFile($arFile, Helper::FILE_PATH);
		if ($fid > 0)
		{
			$arFields['DOCUMENT_ID'] = $fid;
		}
		else
		{
			unset($arFields['DOCUMENT_ID']);
		}
		unset($arFile);
	}
	else if (        // ���� ���� �� ��������� ��� �������
		isset($arParams['DOCUMENT_ID'])
		&&
		!empty($arParams['DOCUMENT_ID'])
		&&
		is_string($arParams['DOCUMENT_ID'])
	)
	{
		$file_path = $arParams['DOCUMENT_ID'];
		if (preg_match("/^https?:\\/\\//", $file_path))
		{
			$arFile = \CFile::MakeFileArray($file_path);
		}
		else
		{
			$io = \CBXVirtualIo::GetInstance();
			$normPath = $io->CombinePath("/", $file_path);
			$absPath = $io->CombinePath($_SERVER["DOCUMENT_ROOT"], $normPath);
			if ($io->ValidatePathString($absPath) && $io->FileExists($absPath))
			{
				$perm = $APPLICATION->GetFileAccessPermission($normPath);
				if ($perm >= "W")
				{
					$arFile = \CFile::MakeFileArray($io->GetPhysicalName($absPath));
				}
			}
		}
		if (!empty($arFile))
		{
			$arFile['del'] = 'N';
			$arFile['MODULE_ID'] = Helper::MODULE_ID;
			$fid = CFile::SaveFile($arFile, Helper::FILE_PATH);
			if ($fid > 0)
			{
				$arFields['DOCUMENT_ID'] = $fid;
			}
			else
			{
				unset($arFields['DOCUMENT_ID']);
			}
			unset($arFile);
		}
	}
	else if (isset($arParams['DOCUMENT_FILE_ID']) && intval($arParams['DOCUMENT_FILE_ID']) > 0)
	{
		$arFields['DOCUMENT_ID'] = intval($arParams['DOCUMENT_FILE_ID']);
	}
	if ($ID > 0)
	{
		$res = DocumentTable::update($ID, $arFields);
	}
	else
	{
		$res = DocumentTable::add($arFields);
	}
	if ($res->isSuccess())
	{
		$ID = $res->getId();
		// ���� ���� ������ ������ "���������" - ���������� ������� �� �����.
		if ($request->get('apply') != "")
		{
			$uri = new Bitrix\Main\Web\Uri($request->getRequestUri());
			$uri->addParams(array("ID" => $ID));
			$uri->addParams(array("mess" => "ok"));
			$uri->addParams(array("lang" => LANG));
			LocalRedirect($uri->getUri());
		}
		else
		{
			LocalRedirect($backUrl);
		}
	}
	else
	{
		$arErrors = $res->getErrorMessages();
		foreach ($arErrors as $err)
		{
			$message .= $err . "<br />";
		}
		unset($arErrors);
	}
}
// �������� �����
/*if(
    $REQUEST_METHOD == "POST" // �������� ������ ������ ��������
    &&
    $request->get("delete_file") != ""
    &&
    intval($request->get("DOCUMENT_FILE_ID")) > 0
)
{
	CFile::Delete(intval($request->get("DOCUMENT_FILE_ID")));
	LocalRedirect($request->getRequestUri());
}*/

// ���� �� ����������, �� �������� ������ �� �������
if ($ID > 0)
{
	$arFields = DocumentTable::getList(array(
		'filter' => array('ID' => $ID),
		'select' => array('*', 'DOCUMENT_NAME' => 'DOCUMENT.ORIGINAL_NAME'),
	))->fetch();
	if (empty($arFields))
	{
		unset($ID);
	}
}
else
{
	$arFields = $request->getPostList();
}
// ������ ���� ����������
/*$arFiles = Bitrix\Main\FileTable::getList(array(
	'filter' => array('MODULE_ID' => Helper::MODULE_ID)
	))->fetchAll();*/
?>
<?
/******************************************************************************************/
/**********************            ����� ������        ***************************************/
/******************************************************************************************/
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$arTabs = array(
	array(
		"DIV" => "edit_main",
		"TAB" => GetMessage("DOC_EDIT_TAB_ONE"),
		"ICON" => "main_user_edit",
		"TITLE" => GetMessage("DOC_EDIT_TAB_ONE_TITLE"),
	),
);
$tabControl = new CAdminTabControl("tabControl", $arTabs);

// ������������ ����������������� ����
$aMenu = array(
	array(
		"TEXT" => GetMessage("DOC_EDIT_BACK"),
		"TITLE" => GetMessage("DOC_EDIT_BACK"),
		"LINK" => $backUrl,
		"ICON" => "btn_list",
	),
);

if ($ID > 0)
{
	$aMenu[] = array("SEPARATOR" => "Y");
	$aMenu[] = array(
		"TEXT" => GetMessage("DOC_EDIT_ADD"),
		"TITLE" => GetMessage("DOC_EDIT_ADD"),
		"LINK" => "document_edit.php?lang=" . LANG,
		"ICON" => "btn_new",
	);
	$aMenu[] = array(
		"TEXT" => GetMessage("DOC_EDIT_DELETE"),
		"TITLE" => GetMessage("DOC_EDIT_DELETE"),
		"LINK" => "javascript:if(confirm('" . GetMessage("DOC_EDIT_CONFIRM_DELETE") . "'))window.location='documents_list.php?ID=" . $ID . "&action_button=delete&lang=" . LANG . "&" . bitrix_sessid_get() . "';",
		"ICON" => "btn_delete",
	);
}

// �������� ���������� ������ ����������������� ����
$context = new CAdminContextMenu($aMenu);
// ����� ����������������� ����
$context->Show();

CAdminMessage::ShowMessage($message);
?>
	<form method="post" action="<?=$request->getRequestUri()?>" enctype="multipart/form-data" name="document_form">
		<? echo bitrix_sessid_post(); ?>
		<input type="hidden" name="lang" value="<?=LANG?>">
		<? if ($ID > 0): ?>
			<input type="hidden" name="ID" value="<?=$ID?>">
		<? endif; ?>

		<?
		// ��������� ��������� ��������
		$tabControl->Begin();
		?>
		<?
		//********************
		// ������ �������� - ����� �������������� ����������
		//********************
		$tabControl->BeginNextTab();
		?>
		<? foreach ($arEditableFields as $fieldName => $obField): ?>
			<? if ($obField->isAutocomplete())
			{
				continue;
			} ?>
			<tr>
				<td width="40%">
					<? if ($obField->isRequired()): ?><b><? endif; ?>
						<? echo $obField->getTitle() ?>
						<? if ($obField->isRequired()): ?></b><? endif; ?>
				</td>
				<td width="60%">
					<? if ($fieldName == 'DOCUMENT_ID'): ?>
						<? /*select name="DOCUMENT_FILE_ID" style="height: 31px; border-radius: 5px 0px 0px 5px;">
						<? foreach ($arFiles as $file): ?>
							<option 
								value="<?echo $file['ID']?>" 
								<?echo ($arFields[$fieldName] == $file['ID']) ? "selected='selected'" : ""?>
								>
								<?echo $file['ORIGINAL_NAME'] ?>
							</option>
						<? endforeach ?>
					</select><button name="delete_file" 
									style="vertical-align: middle; padding: 0; background: none; border: none; outline:none" 
									value="delete_file"
									onclick="if(!confirm('<?echo GetMessage("DOC_EDIT_CONFIRM_DELETE_FILE")?>')) return false;">
						<span class="adm-table-btn-delete" title="������� ����"></span>
					</button*/ ?>
						<?=$arFields['DOCUMENT_NAME']?><br/><br/>
						<? //input type="file" name="<?echo $fieldName "/>?>
						<? echo CFileInput::Show($fieldName, 0, false, array(
								'upload' => true,
								'file_dialog' => true,
								'cloud' => false,
								'medialib' => false,
								'del' => false,
								'description' => false,
							)
						); ?>
						<input type="hidden" name="DOCUMENT_FILE_ID" value="<?=$arFields[$fieldName]?>">
					<? elseif ($fieldName == 'DOCUMENT_DESCRIPTION'): /* ���� �������� �� ������ ��������� �������� ����� */ ?>
						<textarea name="<? echo $fieldName ?>" cols="30" rows="3"><? if ($arFields[$fieldName] != "")
							{
								echo $arFields[$fieldName];
							}
							else
							{
								echo $arFields["DOCUMENT_NAME"];
							} ?></textarea>
					<? elseif ($fieldName == 'ENTITY_ID'): ?>
						<select name="<? echo $fieldName ?>">
							<? $rsHouses = \Citrus\Tszh\HouseTable::getList(array("select" => array("ID", "ADDRESS_USER_ENTERED")));
							$haveSelectedOption = false; /*���� �������� ��������� � ������ �� �����*/
							ob_start(); /*�������� ����������� ������, */
							while ($arHouses = $rsHouses->fetch()) /*���������� � ����� ������ ��������� <option> ��� ������ � select, ���� �������� �������� � ����*/
							{
								?>
								<option <? if ($arHouses["ID"] == $arFields["ENTITY_ID"])
								{
									echo "selected";
									$haveSelectedOption = true;
								} ?> value="<? echo $arHouses["ID"] ?>"><? echo "[" . $arHouses["ID"] . "] " . $arHouses["ADDRESS_USER_ENTERED"] ?></option>
								<?
							}
							$listOfOptions = ob_get_contents(); /*��������� ����� �� ������� ��������� <option> � ���������� � ������� �����*/
							ob_end_clean();
							if (!$haveSelectedOption)
								/* ���� �������� �� �������� �� � ������ ����: ���������� ����� � ������� ��������� ������ ��������� ������ �������
								� ����� ������ ��������� <option>*/
							{
								?>
								<option selected value="0"></option>
								<?
							};
							echo $listOfOptions;
							?>
						</select>
					<? else: ?>
						<input type="text" name="<? echo $fieldName ?>" value="<? echo $arFields[$fieldName] ?>"/>
					<? endif; ?>
				</td>
			</tr>
		<? endforeach ?>
		<?
		// ���������� ����� - ����� ������ ���������� ���������
		$tabControl->Buttons(
			array(
				"disabled" => ($modulePermissions < "W"),
				"back_url" => $backUrl,
			)
		);

		// ��������� ��������� ��������
		$tabControl->End();
		?>
	</form>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");