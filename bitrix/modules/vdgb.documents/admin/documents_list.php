<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Vdgb\Documents\DocumentTable;
use Vdgb\Documents\Helper;

Loc::loadMessages(__FILE__);
Loader::includeModule('vdgb.documents');

$modulePermissions = $APPLICATION->GetGroupRight(Helper::MODULE_ID);
// ���� ��� ���� �� ������ - �������� � ����� ����������� � ���������� �� ������
if ($modulePermissions <= "D") {
    $APPLICATION->AuthForm(Loc::getMessage("DOCUMENTS_ACCESS_DENIED"));
}
global $FilterArr, $lAdmin;
$app     = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();

$sTableID = 'tbl_documents'; // ID �������
$oSort = new \CAdminSorting($sTableID, "ID", "desc"); // ������ ����������
$lAdmin = new \CAdminList($sTableID, $oSort); // �������� ������ ������

// ��������� ��� �������
$arHeaders = DocumentTable::getHeadersList(array(
		"type" => array("DOCUMENT_ID" => "file")
	));

// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
	global $FilterArr, $lAdmin;
	foreach ($FilterArr as $value)
	{
		global $$value;
	}

	return true;
}

// ������ �������� �������
$FilterArr = Helper::adminFilterParams($arHeaders);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

// ���� ��� �������� ������� ���������, ���������� ���
$arFilter = array();
if (CheckFilter() && $request->get("del_filter") === null)
{
	// �������� ������ ���������� ��� ������� �� ������ �������� �������
	$bufHeaders = $arHeaders;
	Helper::adminMakeFilter($arFilter, $arHeaders);
	if (isset($arFilter['%DOCUMENT_ID'])) {
		$arFilter['%DOCUMENT.ORIGINAL_NAME'] = $arFilter['%DOCUMENT_ID'];
		unset($arFilter['%DOCUMENT_ID']);
	}
}
$arSearchFilter = array(
	"ID" => Loc::getMessage("DOCUMENTS_ID"),
	"DOCUMENT_ID" => Loc::getMessage("DOCUMENTS_DOCUMENT"),
	"ENTITY_ID" => Loc::getMessage("DOCUMENTS_ENTITY_ID")
	);

/**
 * ��������� �������� ��� ���������� ������
 */
// ���������� ����������������� ���������
if ($lAdmin->EditAction() && $modulePermissions >= "W" && check_bitrix_sessid())
{
	// ������� �� ������ ���������� ���������
	foreach ($FIELDS as $ID => $arFields)
	{
		if (!$lAdmin->IsUpdated($ID))
			continue;

		// �������� ��������� ������� ��������
		$ID = IntVal($ID);

		$arItem = DocumentTable::getById($ID)->fetch();
		if (is_array($arItem)) {
			/*$arFile = $request->getFile('DOCUMENT_ID');
			if (!empty($arFile['name'])) {
				$arFile['del'] = 'N';
				$arFile['MODULE_ID'] = Helper::MODULE_ID;
				$fid = CFile::SaveFile($arFile, Helper::FILE_PATH);
				if ($fid > 0) {
					$arFields['DOCUMENT_ID'] = $fid;
				} else {
					unset($arFields['DOCUMENT_ID']);
				}
				unset($arFile);
			}*/
			$result = DocumentTable::update($ID, $arFields);

			if (!$result->isSuccess())
			{
				if (is_array($arError = $result->getErrorMessages()))
				{
					foreach ($arError as $err)
					{
						$strError .= $err . "<br/>";
					}
				}

				$lAdmin->AddGroupError(Loc::getMessage("DOCUMENTS_GROUP_SAVE_ERROR") . " " . $strError, $ID);
			}
		}
		else
		{
			$lAdmin->AddGroupError(Loc::getMessage("DOCUMENTS_GROUP_SAVE_ERROR") . " " . Loc::getMessage("DOCUMENTS_GROUP_SAVE_ERROR_ITEM_NOT_FOUND", Array("#ID#" => $ID)), $ID);
		}
	}
}

// ��������� ��������� � ��������� ��������
if (($arID = $lAdmin->GroupAction()) && $modulePermissions >= "W" && check_bitrix_sessid())
{
	// ���� ������� "��� ���� ���������"
	if ($request->get('action_target') == 'selected')
	{
		$rsItems = DocumentTable::getList(array(
			'filter' => $arFilter,
			'select' => array('ID'),
		));

		$arID = Array();

		while ($arItem = $rsItems->fetch())
		{
			$arID[] = $arItem['ID'];
		}
	}

	@set_time_limit(0);

	// ������� �� ������ ���������
	foreach ($arID as $ID)
	{
		if (strlen($ID) <= 0)
			continue;

		$ID = IntVal($ID);

		// ��� ������� �������� �������� ��������� ��������
		switch ($request->get('action_button'))
		{
			// ��������
			case "delete":

				$arItem = DocumentTable::getById($ID)->fetch();

				if (is_array($arItem))
				{
					$result = DocumentTable::delete($ID);

					if ($result->isSuccess())
						$messageOK = Loc::getMessage("DOCUMENTS_DELETE_OK");
					else
						$lAdmin->AddGroupError(Loc::getMessage("VDOCUMENTS_DELETE_ERROR"), $ID);
				}
				else
					$lAdmin->AddGroupError(Loc::getMessage("DOCUMENTS_DELETE_ERROR") . " " . Loc::getMessage("DOCUMENTS_DELETE_ITEM_NOT_FOUND", Array("#ID#" => $ID)), $ID);
				break;
		}
	}
}

/**
 * ������� ��������� ������
 */
$lAdmin->AddHeaders($arHeaders);
// ������� ������� �� ���������
$arVisibleColumns = $_visibleColumns = $lAdmin->GetVisibleHeaderColumns();
$arVisibleColumns = array_unique(array_merge($arVisibleColumns, Array('ID')));
// ��������� ��� ������������ ���������
$arNavParams = \CAdminResult::GetNavParams(\CAdminResult::GetNavSize($sTableID));
$arNav = Array(
	'limit'  => $arNavParams['SIZEN'],
	'offset' => ($arNavParams['PAGEN'] - 1) * $arNavParams['SIZEN'],
);

$arSort   = Array();
$arSortBy = explode('|', $by);
foreach ($arSortBy as $sBy)
{
	$arSort[ $sBy ] = $order;
}

// ������ ���������� ��� �������� ��������������
// ��������� ����� option ��� select'a
// #SELECTED# - ��������� �� selected='selected'
/*$rsFiles = \Bitrix\Main\FileTable::getList(array(
	'filter' => array('MODULE_ID' => Helper::MODULE_ID),
	'select' => array('ID', 'ORIGINAL_NAME')
	)); 
$sOptionsFiles = "";
while ($arFile = $rsFiles->fetch()) {
	$sOptionsFiles .= "<option value='{$arFile['ID']}' #SELECTED#>{$arFile['ORIGINAL_NAME']}</option>";
}*/
// ������� ������ �������
$rsData = DocumentTable::getList(
	array(
		'select' => array("*", "DOCUMENT_NAME" => "DOCUMENT.ORIGINAL_NAME"),
		'filter' => $arFilter,
		'order'  => $arSort,
		'limit'  => $request->get('mode') != 'excel' && !$arNavParams['SHOW_ALL'] ? $arNav['limit'] : '',
		'offset' => $request->get('mode') != 'excel' && !$arNavParams['SHOW_ALL'] ? $arNav['offset'] : '',
	)
);
// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);
// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();
// ������ ���������� ���������, ��� ��� ����������� ����������� ���-�� ������� � �7
$rsData->NavRecordCount = DocumentTable::getCount($arFilter);
$rsData->NavPageSize = $arNavParams['SIZEN'];
$rsData->bShowAll = $arNavParams['SHOW_ALL'];
$rsData->NavPageCount = ceil($rsData->NavRecordCount / $rsData->NavPageSize);
$rsData->NavPageNomer = $arNavParams['PAGEN'];

/**
 * ���������� ������ � ������
 */

// ������ ������������ ��������� ��� ���������������� �����
$template_path = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/interface/navigation.php";

// ����� ������������� �������
$lAdmin->NavText($rsData->GetNavPrint(Loc::getMessage("DOCUMENTS_SETTINGS_LIST"), true, "", $template_path, array(
	'action',
	'sessid'
)));
while ($arRes = $rsData->NavNext(true, "f_"))
{
	// ������� ������. ��������� - ��������� ������ CAdminListRow
	$row = &$lAdmin->AddRow($f_ID, $arRes);
	// ����� �������� ����������� �������� ��� ��������� � �������������� ������
	foreach ($arHeaders as $arH)
	{
		if ($arH["id"] == 'ID') {
			$row->AddViewField($arH["id"], '<a href="document_edit.php?ID='.$f_ID.'&lang='.LANG.'">'.$arRes[$arH['id']].'</a>');
		} else if ($arH["type"] === 'file') {
			$row->AddViewField($arH["id"], $arRes['DOCUMENT_NAME']);
			/*$bufOptions = str_replace("value='{$arRes[$arH['id']]}' #SELECTED#", "value='{$arRes[$arH['id']]}' selected='selected'", $sOptionsFiles);
			$bufOptions = str_replace("#SELECTED", "", $bufOptions);
			$row->AddEditField($arH["id"], "<select name='FIELDS[{$f_ID}][{$arH['id']}]'>{$bufOptions}</select>"); */
			//$row->AddEditField($arH["id"], "<input type='file' name='{$arH['id']}' title='{$arRes['DOCUMENT_NAME']}'>");
		} else {
			$row->AddInputField($arH["id"], array("size"=>20));
		}
	}

	// ���������� ����������� ����
	$arActions = Array();

	// �������� ���� � ���������� ������ � ����������� ����
	if ($modulePermissions >= "U")
	{
		// �������������� ��������
		$arActions[] = array(
			"ICON"    => "edit",
			"DEFAULT" => true,
			"TEXT"    => Loc::getMessage("DOCUMENTS_SETTINGS_EDIT_TITLE"),
			"ACTION"  => $lAdmin->ActionRedirect("document_edit.php?ID=" . $f_ID . '&lang=' . LANG)
		);
		// ��������
		$arActions[] = array(
			"ICON"   => "delete",
			"TEXT"   => Loc::getMessage("DOCUMENTS_SETTINGS_GROUP_DELETE_TITLE"),
			"ACTION" => "if(confirm('" . Loc::getMessage("DOCUMENTS_SETTINGS_GROUP_DELETE_TITLE_CONFIRM") . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete")
		);
	}

	// ������� �����������
	$arActions[] = array("SEPARATOR" => true);

	// ���� ��������� ������� - �����������, �������� �����.
	if (is_set($arActions[ count($arActions) - 1 ], "SEPARATOR"))
		unset($arActions[ count($arActions) - 1 ]);

	// �������� ����������� ���� � ������
	$row->AddActions($arActions);
}

// ����� �������
$lAdmin->AddFooter(
	array(
		// ���-�� ���������
		array(
			"title" => Loc::getMessage("MAIN_ADMIN_LIST_SELECTED"),
			"value" => $rsData->SelectedRowsCount()
		),
		// ������� ��������� ���������
		array(
			"counter" => true,
			"title"   => Loc::getMessage("MAIN_ADMIN_LIST_CHECKED"),
			"value"   => "0"
		),
	)
);

// ��������� ��������
$lAdmin->AddGroupActionTable(Array(
	"delete" => Loc::getMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
));

/**
 * ���������������� ����
 */

// ���������� ���� �� ������ ������ - �������� ��������
$aContext = array(
	array(
		"TEXT"  => Loc::getMessage("DOCUMENTS_SETTINGS_UPDATE"),
		"LINK"  => "document_edit.php?lang=" . LANG,
		"TITLE" => Loc::getMessage("DOCUMENTS_SETTINGS_UPDATE_TITLE"),
		"ICON"  => "btn_green",
	),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

/**
 * ������ ������, ��������, ��������������
 */

if (($arID = $lAdmin->GroupAction()) && $modulePermissions >= "U" && isset($messageOK) && $request->get('action') == "delete")
{
	$message = new \CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// �������������� �����
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(Loc::getMessage("DOCUMENTS_SETTINGS"));

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';

/*echo $demoNotice;*/

if (isset($message) && $request->get("edit_form_del") !== null)
{
	$message = new \CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

/**
 * ����� �������
 */

$arFilterItems = Array();
foreach ($arHeaders as $arField)
{
	if ($arField['id'] != "ID")
		$arFilterItems[] = $arField['content'];
}

$oFilter = new \CAdminFilter(
	$sTableID . "_filter",
	$arFilterItems
);

?>
	<form name="form_filter" method="get" action="<? echo $APPLICATION->GetCurPage(); ?>">
		<?
		$oFilter->Begin();

		Helper::showAdminFilter($arHeaders, $arSearchFilter);

		$oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "form_filter"));
		$oFilter->End();
		?>
	</form>
<?
// ������� ������� ������ ���������
$lAdmin->DisplayList();

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';
