<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2017 vdgb-soft.ru
 * @date 28.08.2017 23:37
 */

$MESS['DATA_VALIDATION__VALIDATE_BIK__ERROR__EMPTY'] = 'БИК пуст';
$MESS['DATA_VALIDATION__VALIDATE_BIK__ERROR__ONLY_NUMBERS'] = 'БИК может состоять только из цифр';
$MESS['DATA_VALIDATION__VALIDATE_BIK__ERROR__COUNT_NUMBERS'] = 'БИК может состоять только из 9 цифр';

$MESS['DATA_VALIDATION__VALIDATE_INN__ERROR__EMPTY'] = 'ИНН пуст';
$MESS['DATA_VALIDATION__VALIDATE_INN__ERROR__ONLY_NUMBERS'] = 'ИНН может состоять только из цифр';
$MESS['DATA_VALIDATION__VALIDATE_INN__ERROR__COUNT_NUMBERS'] = 'ИНН может состоять только из 10 или 12 цифр';
$MESS['DATA_VALIDATION__VALIDATE_INN__ERROR__INCORRECT'] = 'ИНН указан неправильного формата';

$MESS['DATA_VALIDATION__VALIDATE_KPP__ERROR__EMPTY'] = 'КПП пуст';
$MESS['DATA_VALIDATION__VALIDATE_KPP__ERROR__COUNT_CHARS'] = 'КПП может состоять только из 9 знаков (цифр или заглавных букв латинского алфавита от A до Z)';
$MESS['DATA_VALIDATION__VALIDATE_KPP__ERROR__INCORRECT_FORMAT'] = 'Неправильный формат КПП';

$MESS['DATA_VALIDATION__VALIDATE_KS__ERROR__EMPTY'] = 'К/С пуст';
$MESS['DATA_VALIDATION__VALIDATE_KS__ERROR__ONLY_NUMBERS'] = 'К/С может состоять только из цифр';
$MESS['DATA_VALIDATION__VALIDATE_KS__ERROR__COUNT_NUMBERS'] = 'К/С может состоять только из 20 цифр';
$MESS['DATA_VALIDATION__VALIDATE_KS__ERROR__INCORRECT'] = 'К/С указан неправильного формата';

$MESS['DATA_VALIDATION__VALIDATE_OGRN__ERROR__EMPTY'] = 'ОГРН пуст';
$MESS['DATA_VALIDATION__VALIDATE_OGRN__ERROR__ONLY_NUMBERS'] = 'ОГРН может состоять только из цифр';
$MESS['DATA_VALIDATION__VALIDATE_OGRN__ERROR__COUNT_NUMBERS'] = 'ОГРН может состоять только из 13 цифр';
$MESS['DATA_VALIDATION__VALIDATE_OGRN__ERROR__INCORRECT'] = 'ОГРН указан неправильного формата';

$MESS['DATA_VALIDATION__VALIDATE_OGRNIP__ERROR__EMPTY'] = 'ОГРНИП пуст';
$MESS['DATA_VALIDATION__VALIDATE_OGRNIP__ERROR__ONLY_NUMBERS'] = 'ОГРНИП может состоять только из цифр';
$MESS['DATA_VALIDATION__VALIDATE_OGRNIP__ERROR__COUNT_NUMBERS'] = 'ОГРНИП может состоять только из 15 цифр';
$MESS['DATA_VALIDATION__VALIDATE_OGRNIP__ERROR__INCORRECT'] = 'ОГРНИП указан неправильного формата';

$MESS['DATA_VALIDATION__VALIDATE_RS__ERROR__EMPTY'] = 'Р/С пуст';
$MESS['DATA_VALIDATION__VALIDATE_RS__ERROR__ONLY_NUMBERS'] = 'Р/С может состоять только из цифр';
$MESS['DATA_VALIDATION__VALIDATE_RS__ERROR__COUNT_NUMBERS'] = 'Р/С может состоять только из 20 цифр';
$MESS['DATA_VALIDATION__VALIDATE_RS__ERROR__INCORRECT'] = 'Р/С указан неправильного формата';

$MESS['DATA_VALIDATION__VALIDATE_SNILS__ERROR__EMPTY'] = 'СНИЛС пуст';
$MESS['DATA_VALIDATION__VALIDATE_SNILS__ERROR__ONLY_NUMBERS'] = 'СНИЛС может состоять только из цифр';
$MESS['DATA_VALIDATION__VALIDATE_SNILS__ERROR__COUNT_NUMBERS'] = 'СНИЛС может состоять только из 11 цифр';
$MESS['DATA_VALIDATION__VALIDATE_SNILS__ERROR__INCORRECT'] = 'СНИЛС указан неправильного формата';