<?php
$MESS['OTR_DATAVALIDATION_MODULE_NAME'] = '1С-Рарус: Проверка данных организации';
$MESS['OTR_DATAVALIDATION_MODULE_DESCRIPTION'] = 'Модуль с помощью API позволяет проверять корректность данных БИК, ИНН, КПП, кор. счетов, расчетных счетов, ОГРН, ОГРНИП, СНИЛС для организаций и физ. лиц. в соответствии с правилами их формирования и проверкой контрольных сумм';
$MESS['PARTNER_NAME'] = '1С-Рарус Тиражные решения';
$MESS['PARTNER_URI'] = 'https://otr-soft.ru';