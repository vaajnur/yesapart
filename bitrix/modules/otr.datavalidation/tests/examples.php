<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2017 vdgb-soft.ru
 * @date 29.08.2017 0:32
 */

CModule::IncludeModule('otr.datavalidation');
$err_code = $err_msg = '';
\Otr\Tszh\DataValidation::validateKpp('123123', $err_msg, $err_code);
echo "<pre>"; var_dump($err_msg); echo "</pre>";
echo "<pre>"; var_dump($err_code); echo "</pre>";
CJSCore::Init('tszh_data_validation');
?>
	<script>
        var error = {
            code: null,
            message: null
        };
        console.log(BX.tszh.validateInn('1231231233', error));
        console.log(error);
	</script>
<?
