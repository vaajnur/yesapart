<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2017 vdgb-soft.ru
 * @date 28.08.2017 22:31
 */

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

if (ModuleManager::isModuleInstalled('otr.datavalidation'))
{
	CJSCore::RegisterExt('tszh_data_validation', array(
			'js' => '/bitrix/js/otr.datavalidation/tszh_data_validation.js',
			'lang' => BX_ROOT . '/modules/otr.datavalidation/lang/' . LANGUAGE_ID . '/lib/datavalidation.php',
		)
	);

	Loader::registerAutoLoadClasses('otr.datavalidation', array(
			'Otr\\Tszh\\DataValidation' => 'lib/datavalidation.php',
		)
	);

}