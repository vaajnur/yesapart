<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2017 vdgb-soft.ru
 * @date 29.08.2017 0:14
 */

namespace Otr\Tszh;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class DataValidation
{
	/**
	 * @var Singleton
	 */
	private static $instance;

	/**
	 * gets the instance via lazy initialization (created on first usage)
	 */
	public static function getInstance()
	{
		if (null === static::$instance) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	/**
	 * is not allowed to call from outside to prevent from creating multiple instances,
	 * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
	 */
	private function __construct()
	{
	}

	/**
	 * prevent the instance from being cloned (which would create a second instance of it)
	 */
	private function __clone()
	{
	}

	/**
	 * prevent from being unserialized (which would create a second instance of it)
	 */
	private function __wakeup()
	{
	}

	/**
	 * @param string $bik
	 * @param mixed $error_message
	 * @param mixed $error_code
	 *
	 * @return boolean
	 */
	public static function validateBik($bik, &$error_message = null, &$error_code = null)
	{
		$result = false;
		$bik = (string)$bik;
		if (!$bik)
		{
			$error_code = 1;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_BIK__ERROR__EMPTY');
		}
		elseif (preg_match('/[^0-9]/', $bik))
		{
			$error_code = 2;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_BIK__ERROR__ONLY_NUMBERS');
		}
		elseif (strlen($bik) !== 9)
		{
			$error_code = 3;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_BIK__ERROR__COUNT_NUMBERS');
		}
		else
		{
			$result = true;
		}

		return $result;
	}

	/**
	 * @param string $inn
	 * @param mixed $error_message
	 * @param mixed $error_code
	 *
	 * @return boolean
	 */
	public static function validateInn($inn, &$error_message = null, &$error_code = null)
	{
		$result = false;
		$inn = (string)$inn;
		if (!$inn)
		{
			$error_code = 1;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_INN__ERROR__EMPTY');
		}
		elseif (preg_match('/[^0-9]/', $inn))
		{
			$error_code = 2;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_INN__ERROR__ONLY_NUMBERS');
		}
		elseif (!in_array($inn_length = strlen($inn), [10, 12]))
		{
			$error_code = 3;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_INN__ERROR__COUNT_NUMBERS');
		}
		else
		{
			$check_digit = function ($inn, $coefficients) {
				$n = 0;
				foreach ($coefficients as $i => $k)
				{
					$n += $k * (int)$inn{$i};
				}

				return $n % 11 % 10;
			};
			switch ($inn_length)
			{
				case 10:
					$n10 = $check_digit($inn, [2, 4, 10, 3, 5, 9, 4, 6, 8]);
					if ($n10 === (int)$inn{9})
					{
						$result = true;
					}
					break;
				case 12:
					$n11 = $check_digit($inn, [7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);
					$n12 = $check_digit($inn, [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);
					if (($n11 === (int)$inn{10}) && ($n12 === (int)$inn{11}))
					{
						$result = true;
					}
					break;
			}
			if (!$result)
			{
				$error_code = 4;
				$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_INN__ERROR__INCORRECT');
			}
		}

		return $result;
	}

	/**
	 * @param string $kpp
	 * @param mixed $error_message
	 * @param mixed $error_code
	 *
	 * @return boolean
	 */
	public static function validateKpp($kpp, &$error_message = null, &$error_code = null)
	{
		$result = false;
		$kpp = (string)$kpp;
		if (!$kpp)
		{
			$error_code = 1;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_KPP__ERROR__EMPTY');
		}
		elseif (strlen($kpp) !== 9)
		{
			$error_code = 2;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_KPP__ERROR__COUNT_CHARS');
		}
		elseif (!preg_match('/^[0-9]{4}[0-9A-Z]{2}[0-9]{3}$/', $kpp))
		{
			$error_code = 3;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_KPP__ERROR__INCORRECT_FORMAT');
		}
		else
		{
			$result = true;
		}

		return $result;
	}

	/**
	 * @param string $ks
	 * @param string $bik
	 * @param mixed $error_message
	 * @param mixed $error_code
	 *
	 * @return boolean
	 */
	public static function validateKs($ks, $bik, &$error_message = null, &$error_code = null)
	{
		$result = false;
		if (self::validateBik($bik, $error_message, $error_code))
		{
			$ks = (string)$ks;
			if (!$ks)
			{
				$error_code = 1;
				$error_message =  Loc::getMessage('DATA_VALIDATION__VALIDATE_KS__ERROR__EMPTY');
			}
			elseif (preg_match('/[^0-9]/', $ks))
			{
				$error_code = 2;
				$error_message =  Loc::getMessage('DATA_VALIDATION__VALIDATE_KS__ERROR__ONLY_NUMBERS');
			}
			elseif (strlen($ks) !== 20)
			{
				$error_code = 3;
				$error_message =  Loc::getMessage('DATA_VALIDATION__VALIDATE_KS__ERROR__COUNT_NUMBERS');
			}
			else
			{
				$bik_ks = '0' . substr((string)$bik, -5, 2) . $ks;
				$checksum = 0;
				foreach ([7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1] as $i => $k)
				{
					$checksum += $k * ((int)$bik_ks{$i} % 10);
				}
				if ($checksum % 10 === 0)
				{
					$result = true;
				}
				else
				{
					$error_code = 4;
					$error_message =  Loc::getMessage('DATA_VALIDATION__VALIDATE_KS__ERROR__INCORRECT');
				}
			}
		}

		return $result;
	}

	/**
	 * @param string $ogrn
	 * @param mixed $error_message
	 * @param mixed $error_code
	 *
	 * @return boolean
	 */
	public static function validateOgrn($ogrn, &$error_message = null, &$error_code = null)
	{
		$result = false;
		$ogrn = (string)$ogrn;
		if (!$ogrn)
		{
			$error_code = 1;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_OGRN__ERROR__EMPTY');
		}
		elseif (preg_match('/[^0-9]/', $ogrn))
		{
			$error_code = 2;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_OGRN__ERROR__ONLY_NUMBERS');
		}
		elseif (strlen($ogrn) !== 13)
		{
			$error_code = 3;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_OGRN__ERROR__COUNT_NUMBERS');
		}
		else
		{
			$n13 = (int)substr(bcsub(substr($ogrn, 0, -1), bcmul(bcdiv(substr($ogrn, 0, -1), '11', 0), '11')), -1);
			if ($n13 === (int)$ogrn{12})
			{
				$result = true;
			}
			else
			{
				$error_code = 4;
				$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_OGRN__ERROR__INCORRECT');
			}
		}

		return $result;
	}

	/**
	 * @param string $ogrnip
	 * @param mixed $error_message
	 * @param mixed $error_code
	 *
	 * @return boolean
	 */
	public static function validateOgrnip($ogrnip, &$error_message = null, &$error_code = null)
	{
		$result = false;
		$ogrnip = (string)$ogrnip;
		if (!$ogrnip)
		{
			$error_code = 1;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_OGRNIP__ERROR__EMPTY');
		}
		elseif (preg_match('/[^0-9]/', $ogrnip))
		{
			$error_code = 2;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_OGRNIP__ERROR__ONLY_NUMBERS');
		}
		elseif (strlen($ogrnip) !== 15)
		{
			$error_code = 3;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_OGRNIP__ERROR__COUNT_NUMBERS');
		}
		else
		{
			$n15 = (int)substr(bcsub(substr($ogrnip, 0, -1), bcmul(bcdiv(substr($ogrnip, 0, -1), '13', 0), '13')), -1);
			if ($n15 === (int)$ogrnip{14})
			{
				$result = true;
			}
			else
			{
				$error_code = 4;
				$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_OGRNIP__ERROR__INCORRECT');
			}
		}

		return $result;
	}

	/**
	 * @param string $rs
	 * @param string $bik
	 * @param mixed $error_message
	 * @param mixed $error_code
	 *
	 * @return boolean
	 */
	public static function validateRs($rs, $bik, &$error_message = null, &$error_code = null)
	{
		$result = false;
		if (self::validateBik($bik, $error_message, $error_code))
		{
			$rs = (string)$rs;
			if (!$rs)
			{
				$error_code = 1;
				$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_RS__ERROR__EMPTY');
			}
			elseif (preg_match('/[^0-9]/', $rs))
			{
				$error_code = 2;
				$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_RS__ERROR__ONLY_NUMBERS');
			}
			elseif (strlen($rs) !== 20)
			{
				$error_code = 3;
				$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_RS__ERROR__COUNT_NUMBERS');
			}
			else
			{
				$bik_rs = substr((string)$bik, -3) . $rs;
				$checksum = 0;
				foreach ([7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1] as $i => $k)
				{
					$checksum += $k * ((int)$bik_rs{$i} % 10);
				}
				if ($checksum % 10 === 0)
				{
					$result = true;
				}
				else
				{
					$error_code = 4;
					$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_RS__ERROR__INCORRECT');
				}
			}
		}

		return $result;
	}

	/**
	 * @param string $snils
	 * @param mixed $error_message
	 * @param mixed $error_code
	 *
	 * @return boolean
	 */
	public static function validateSnils($snils, &$error_message = null, &$error_code = null)
	{
		$result = false;
		$snils = (string)$snils;
		if (!$snils)
		{
			$error_code = 1;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_SNILS__ERROR__EMPTY');
		}
		elseif (preg_match('/[^0-9]/', $snils))
		{
			$error_code = 2;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_SNILS__ERROR__ONLY_NUMBERS');
		}
		elseif (strlen($snils) !== 11)
		{
			$error_code = 3;
			$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_SNILS__ERROR__COUNT_NUMBERS');
		}
		else
		{
			$sum = 0;
			for ($i = 0; $i < 9; $i++)
			{
				$sum += (int)$snils{$i} * (9 - $i);
			}
			$check_digit = 0;
			if ($sum < 100)
			{
				$check_digit = $sum;
			}
			elseif ($sum > 101)
			{
				$check_digit = $sum % 101;
				if ($check_digit === 100)
				{
					$check_digit = 0;
				}
			}
			if ($check_digit === (int)substr($snils, -2))
			{
				$result = true;
			}
			else
			{
				$error_code = 4;
				$error_message = Loc::getMessage('DATA_VALIDATION__VALIDATE_SNILS__ERROR__INCORRECT');
			}
		}

		return $result;
	}
}