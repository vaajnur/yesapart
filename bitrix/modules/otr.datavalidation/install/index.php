<?php

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

if (class_exists('otr_datavalidation'))
{
	return;
}

/**
 * Class otr_datavalidation
 */
class otr_datavalidation extends CModule
{
	/** @var string */
	// public $MODULE_ID;
	var $MODULE_ID = 'otr.datavalidation';

	/** @var string */
	public $MODULE_VERSION;

	/** @var string */
	public $MODULE_VERSION_DATE;

	/** @var string */
	public $MODULE_NAME;

	/** @var string */
	public $MODULE_DESCRIPTION;

	/** @var string */
	public $MODULE_GROUP_RIGHTS;

	/** @var string */
	public $PARTNER_NAME;

	/** @var string */
	public $PARTNER_URI;

	public function __construct()
	{
		$arModuleVersion = array();

		include_once(__DIR__ . '/version.php');
		if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion['VERSION'];
			$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
		}
		else
		{
			$this->MODULE_VERSION = '1.0.0';
			$this->MODULE_VERSION_DATE = '2017-08-28 18:00:00';
		}

		$this->MODULE_ID = 'otr.datavalidation';
		$this->MODULE_NAME = Loc::getMessage('OTR_DATAVALIDATION_MODULE_NAME');
		$this->MODULE_DESCRIPTION = Loc::getMessage('OTR_DATAVALIDATION_MODULE_DESCRIPTION');
		$this->MODULE_GROUP_RIGHTS = 'N';
		$this->PARTNER_NAME = Loc::getMessage('PARTNER_NAME');;
		$this->PARTNER_URI = Loc::getMessage('PARTNER_URI');;
	}

	/**
	 * @return mixed|void
	 */
	public function DoInstall()
	{
		$this->installDB();
		$this->installFiles();
	}

	/**
	 * @return mixed|void
	 */
	public function DoUninstall()
	{
		$this->unInstallFiles();
		$this->uninstallDB();
	}

	/**
	 * @return bool|void
	 */
	public function installDB()
	{
		ModuleManager::registerModule($this->MODULE_ID);
	}

	public function uninstallDB()
	{
		ModuleManager::unregisterModule($this->MODULE_ID);
	}

	public function installFiles()
	{
		CopyDirFiles(Application::getDocumentRoot() . '/bitrix/modules/' . $this->MODULE_ID . '/install/js', Application::getDocumentRoot() . '/bitrix/js', true, true);
	}

	public function unInstallFiles()
	{
		DeleteDirFiles(Application::getDocumentRoot() . '/bitrix/modules/' . $this->MODULE_ID . '/install/js', Application::getDocumentRoot() . '/bitrix/js');
	}

}