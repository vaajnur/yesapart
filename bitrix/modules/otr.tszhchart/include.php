<?php

define("TSZH_CHART_MODULE_ID", 'otr.tszhchart');

CJSCore::RegisterExt('moment', Array(
    'js' => '/bitrix/js/otr.tszhchart/moment/moment.min.js'
));

CJSCore::RegisterExt('hammer', Array(
    'js' => '/bitrix/js/otr.tszhchart/hammer/hammer.min.js',
    'rel' =>   array()
));

CJSCore::RegisterExt('localeru', Array(
    'js' => '/bitrix/js/otr.tszhchart/moment/locale/ru.js',
    'lang' => '/bitrix/js/otr.tszhchart/moment/lang/ru/ru.php',
    'rel' =>   array()
));

CJSCore::RegisterExt('chart', Array(
    'js' => '/bitrix/js/otr.tszhchart/chart/Chart.js',
    'rel' =>   array('moment', 'hammer', 'localeru')
));

CJSCore::RegisterExt('chartjspluginzoom', Array(
    'js' => '/bitrix/js/otr.tszhchart/chartjs-plugin-zoom/chartjs-plugin-zoom.js',
    'rel' =>   array()
));

?>