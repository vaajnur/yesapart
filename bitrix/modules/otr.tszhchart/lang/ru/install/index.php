<?
$MESS["OTR_TSZH_CHART_MODULE_NAME"] = "Графики для 1С: Сайта ЖКХ";
$MESS["OTR_TSZH_CHART_MODULE_DESCRIPTION"] = "Модуль для вывода графиков в личном кабинете Сайта ЖКХ";
$MESS["CITRUS_TSZH_MODULE_PARTNER_NAME"] = "1С-Рарус Тиражные решения";
$MESS["CITRUS_TSZH_MODULE_PARTNER_URI"] = "https://otr-soft.ru/";
$MESS["TSZH_CHART_INST_TITLE"] = "Установка модуля «Графики для 1С: Сайта ЖКХ»";
$MESS["TSZH_CHART_UNINST_TITLE"] = "Удаление модуля «Графики для 1С: Сайта ЖКХ»";
$MESS["TSZH_ERROR_EDITION_NOT_FOUND"] = "Не определена редакция продукта 1С: Сайт ЖКХ";

$MESS['VDGB_ADD_CHART'] = "Доступен новый функционал <a href='/bitrix/admin/partner_modules.php?id=otr.tszhchart'>Графики для 1С: Сайта ЖКХ</a>";

