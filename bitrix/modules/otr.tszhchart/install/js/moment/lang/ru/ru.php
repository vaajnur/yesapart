<?
$MESS ['MONTHS_FORMAT'] = "января_февраля_марта_апреля_мая_июня_июля_августа_сентября_октября_ноября_декабря";
$MESS ['MONTHS_STANDALONE'] ="январь_февраль_март_апрель_май_июнь_июль_август_сентябрь_октябрь_ноябрь_декабрь";

$MESS ['MONTHSSHORT_FORMAT'] = "янв._февр._мар._апр._мая_июня_июля_авг._сент._окт._нояб._дек.";
$MESS ['MONTHSSHORT_STANDALONE'] = "янв._февр._март_апр._май_июнь_июль_авг._сент._окт._нояб._дек.";


$MESS ['BCL_MONITORING_INTERVAL_WEEK'] = "Неделя";
$MESS ['BCL_MONITORING_INTERVAL_MONTH'] = "Месяц";
$MESS ['BCL_MONITORING_INTERVAL_QUARTER'] = "Квартал";
$MESS ['BCL_MONITORING_INTERVAL_YEAR'] = "Год";
?>