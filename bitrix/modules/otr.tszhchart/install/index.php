<?
/**
 * ������ ���� �������
 * ���������/�������� ������
 * @package tszh
*/
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

global $DOCUMENT_ROOT, $MESS;

Loc::loadMessages(__FILE__);

if (class_exists("otr.tszhchart")) return;

Class otr_tszhchart extends CModule
{
	var $MODULE_ID = "otr.tszhchart";
    var $MODULE_VERSION; // ������ ������
    var $MODULE_VERSION_DATE; // ���� ���������� ������
    var $MODULE_NAME; // �������� ������
    var $MODULE_DESCRIPTION; // �������� ������
    var $MODULE_CSS;

	function otr_tszhchart()
	{
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        else
        {
            $this->MODULE_VERSION = "1.0.0";
            $this->MODULE_VERSION_DATE = "15.02.2018";
        }

        $this->MODULE_NAME = GetMessage("OTR_TSZH_CHART_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("OTR_TSZH_CHART_MODULE_DESCRIPTION");

        $this->PARTNER_NAME = GetMessage("CITRUS_TSZH_MODULE_PARTNER_NAME");
        $this->PARTNER_URI = GetMessage("CITRUS_TSZH_MODULE_PARTNER_URI");
	}

    public function InstallDB()
    {
        // �������� ������ ������ � ���� ������ � ������������� ������ � ��������
        ModuleManager::registerModule($this->MODULE_ID);
        return true;
    }

    public function UninstallDB()
    {
        // ������ �����������
        ModuleManager::unRegisterModule($this->MODULE_ID);
        return true;
    }

	// ����������� ������ ������
    function InstallFiles($arParams = Array())
    {
        CopyDirFiles(__DIR__ . "/components", $_SERVER['DOCUMENT_ROOT']."/bitrix/components", true, true);
        CopyDirFiles(__DIR__ . "/js/", $_SERVER['DOCUMENT_ROOT']."/bitrix/js/" . $this->MODULE_ID . "/", true, true);

        require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/install/wizard_sol/utils.php");

        $bReWriteAdditionalFiles = ($arParams["public_rewrite"] == "Y");

        if(array_key_exists("public_dir", $arParams) && strlen($arParams["public_dir"])) {

            if (array_key_exists("sites_checked", $arParams) && count($arParams["sites_checked"])) {

                $sites_checked = $arParams["sites_checked"];
                $menu_item_name = $arParams["menu_item_name"];
                $add_menu = $arParams["add_menu"];
                foreach ($sites_checked as $fSite) {
                    $rsSites = CSite::GetByID($fSite);
                    $site = $rsSites->Fetch();
                    CopyDirFiles(__DIR__ . "/public/ru/personal/chart", $_SERVER['DOCUMENT_ROOT'] . $site["DIR"] . $arParams["public_dir"]."/", $bReWriteAdditionalFiles, true);
                    if ((strlen($menu_item_name)>0) && ($add_menu)) {
                    WizardServices::AddMenuItem($site["DIR"] . "personal/.show_add.menu.php", Array($_REQUEST["menu_item_name"], $site["DIR"] . $arParams["public_dir"] . "/", Array(),  Array("class"=>"chart_utilities"), ""), false, -1);
                    WizardServices::AddMenuItem($site["DIR"] . "personal/.section.menu.php", Array($_REQUEST["menu_item_name"], $site["DIR"] . $arParams["public_dir"] . "/", Array(),  Array("class"=>"chart_utilities leftmenu__img-chart"), ""), false, -1);
                }

                }
            }
        }
        // ����������� � ������ ������ �����������
        if (tszhCheckMinEdition('business'))	{
            \CAdminNotify::DeleteByTag("add_chart");
            \CAdminNotify::add(
                array(
                    "MESSAGE" => Loc::GetMessage('VDGB_ADD_CHART'),
                    "TAG" => "add_chart",
                    "MODULE_ID" => $this->MODULE_ID,
                    "ENABLE_CLOSE" => "Y",
                ));
        }


        return true;
    }

	// �������� ������ ������
    function UnInstallFiles($arParams = Array())
    {
       // DeleteDirFilesEx("/bitrix/components/otr/tszh.chart");
        DeleteDirFilesEx("/bitrix/js/{$this->MODULE_ID}/");
        if (!$arParams["save_public_files"]) {
            $dbSites = CSite::GetList(($b = ""), ($o = ""), Array("ACTIVE" => "Y"));
            while ($site = $dbSites->Fetch())
            {
                $arSites[] = Array("DIR" => $site["DIR"]);
            }
            foreach ($arSites as $arSite) {
                DeleteDirFilesEx($arSite["DIR"]."personal/chart");
            }
        }
        \CAdminNotify::DeleteByTag("add_chart");
        return true;
    }

	// ��������� ������ (���������� ���������)
    function DoInstall()
    {

        if (!check_bitrix_sessid())
            return false;
        global $APPLICATION;

       if($this->InstallDB())
            {
                $this->InstallFiles(array(
                    "sites_checked" => false,
                    "public_dir" => false,
                    "public_rewrite" => false,
                    "add_menu" => false,
                    "menu_item_name" => false,
                ));
            }
            $GLOBALS["errors"] = $this->errors;
    }



	// �������� ������ (���������� ���������)
    function DoUninstall()
    {
        if (!check_bitrix_sessid())
            return false;
        global $APPLICATION;
        $step = IntVal($_REQUEST['step']);
        if ($step < 2) {
            $APPLICATION->IncludeAdminFile(GetMessage("TSZH_CHART_UNINST_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/uninst1.php");
        } elseif ($step == 2) {
            $this->UninstallDB();
            $this->UnInstallFiles(array(
                "save_public_files" => IntVal($_REQUEST["save_public_files"]),
            ));
            UnRegisterModule($this->MODULE_ID);
            $GLOBALS["errors"] = $this->errors;
            $APPLICATION->IncludeAdminFile(GetMessage("TSZH_CHART_UNINST_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/uninst2.php");
        }
    }

}
