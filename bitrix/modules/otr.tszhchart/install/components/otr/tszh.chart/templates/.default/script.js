var NEW_CHART_TYPE, speedData, options_array = {};
var inView = [];
var color = Chart.helpers.color;

window.onload = function () {
    if (document.getElementById('form_chart_rate_button')) {
        document.getElementById('form_chart_rate_button').onclick = function (e) {
            var e = e || event;
            var target = e.target || e.srcElement;
            if ((target.type == 'button') || (target.tagName == "SPAN") || (target.className == "textdiv")) {
                if (target.className == "textdiv") target = target.parentElement
                else {
                    if (target.tagName == "SPAN") target = target.parentElement;
                    if (target.tagName == "SPAN") target = target.parentElement;
                }
                var child = document.getElementById('form_chart_rate_button').children[0].childNodes;
                for (i = 0; i < child.length; i++)
                    if (child[i].type == 'button') child[i].querySelector("span").querySelector("span").className = "icon-btn-round-current current-white";
                document.getElementById(target.id).querySelector("span").querySelector("span").className = "icon-btn-round-current";

                arParams = JSON.parse($(target).attr("data"));
                chart_start();
            }
        }
    }
};

function generateColor() {
    return '#' + Math.floor(Math.random() * 16777215).toString(16)
}

function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();
    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();
    return ((((elemBottom >= docViewBottom) && (elemTop <= docViewBottom)) || ((elemBottom >= docViewTop) && (elemTop <= docViewTop))) || ((elemBottom <= docViewBottom) && (elemTop >= docViewTop)));
}

$(window).scroll(function () {
        for (i = 0; i < myChart.length-1; i++) {
            if (myChart[i]["options"]["animation"]["duration"]) {
                popCanvas_id = myChart[i]["canvas"]["id"]
                if (popCanvas_id) {
                    if (isScrolledIntoView(document.getElementById(popCanvas_id))) {
                        if (inView[i] === undefined) inView[i] = true;
                        if (inView[i]) continue;
                        inView[i] = true;
                        myChart[i].destroy();
                        myChart[i] = new Chart(popCanvas_id, {
                            type: myChart[i]["config"]["type"],
                            data: myChart[i]["config"]["data"],
                            options: myChart[i]["config"]["options"],
                        });
                    } else inView[i] = false;
                }
            }
        }
    }
);

function chart_start() {
    arParams.ARCHART_CAPTION = arParams.ARCHART_CAPTION.replace(/&quot;/g, '"');

    if (arParams.ARCHART_CAPTION) arParams.ARCHART_CAPTION = arParams.ARCHART_CAPTION.split('|');

    MAX_VALUES_COUNT = arParams.ARCHART_CAPTION.length;

    arParams.STACKED = arParams.CHART_TYPE === "CHART_SUMM";
    arParams.ELEMENTS_LINE_TENSION = arParams.CHART_TYPE === "CHART_RATE" ? 0.4 : 0;
    arParams.STEPPEDLINE = arParams.CHART_TYPE === "CHART_SUMM_SUMMPAYED";
    arParams.DISPLAY_PERIOD_XAXES = 12;
    arParams.DISPLAY_FORMATS_XAXES = arParams.CHART_TYPE === "CHART_RATE" ? "MMMM" : "MMMM YY";

    backgroundcolorColors = [];
    chartColors = [];
    if (arParams.CHART_COLORS) chartColors = arParams.CHART_COLORS.split(',');
    if (arParams.BACKGROUNDCOLOR_COLORS) backgroundcolorColors = arParams.BACKGROUNDCOLOR_COLORS.split(',');
    if ((chartColors.length < MAX_VALUES_COUNT ) || (backgroundcolorColors.length < MAX_VALUES_COUNT )) {
        for (i = 0; i < MAX_VALUES_COUNT; i++) {
            if (!chartColors[i]) chartColors[i] = generateColor();
            if (!backgroundcolorColors[i]) backgroundcolorColors[i] = chartColors[i];
        }
    }

    if (arParams.ARCHART_VAL) arParams.ARCHART_VAL = arParams.ARCHART_VAL.split('|');
    if (arParams.ARCHART_DATE) arParams.ARCHART_DATE = arParams.ARCHART_DATE.split('|');
    if (arParams.CHART_TYPE == "CHART_PIE") {
        if (arParams.ARCHART_DATE[0]) {
            ARCHART_DATE_YEAR = arParams.ARCHART_DATE[0].split(';');
            ARCHART_DATE_YEAR[0] = moment(moment(ARCHART_DATE_YEAR[0])).format(arParams.DISPLAY_FORMATS_XAXES);
            ARCHART_DATE_YEAR[1] = moment(moment(ARCHART_DATE_YEAR[1])).format(arParams.DISPLAY_FORMATS_XAXES);
            arParams.ARCHART_DATE[0] = ARCHART_DATE_YEAR.join(' - ');
        }
        if (arParams.ARCHART_DATE[1]) {
            arParams.ARCHART_DATE[1] = moment(moment(arParams.ARCHART_DATE[1])).format(arParams.DISPLAY_FORMATS_XAXES);
        }
    }

    NEW_CHART_TYPE = '';
    // ������� ������ ���� �������

    CHART_TYPE_MIX = false;
    if (arParams.CHART_TYPES) {
        arParams.CHART_TYPES = arParams.CHART_TYPES.split('|');
        if (arParams.CHART_TYPES.length > 1) {
            CHART_TYPE_MIX = true;
            arParams.CHART_TYPES.forEach(function (items, i) {// ���� ���� ��� ����� line
                    if (items != 'line') NEW_CHART_TYPE = items
                }
            );
        }
        else {
            NEW_CHART_TYPE = arParams.CHART_TYPES[0];
            arParams.CHART_TYPES = [];
            arParams.ARCHART_CAPTION.forEach(function (items, i) {
                    arParams.CHART_TYPES[i] = NEW_CHART_TYPE;
                }
            );
        }
        if (!NEW_CHART_TYPE) NEW_CHART_TYPE = 'line';
    }
    else {
        arParams.CHART_TYPES = [];
        arParams.ARCHART_CAPTION.forEach(function (items, i) {
                arParams.CHART_TYPES[i] = 'line';
            }
        );
        NEW_CHART_TYPE = 'line';
    }

// ------------------------------------------------- ����������� ������� ����, ��� � ���� �������� -------------------------------------------------------------------------------
    var timeFormat = 'YYYY-MM';// ������ ���������� �� ����������
    moment.locale('ru'); // ������������� �����������
    archart_alldate_timeformat = [];
    archart_caption_dates = [];

    if (arParams.CHART_TYPE == "CHART_RATE") {
        for (i = 0; i < 2; i++) {
            archart_caption_dates[i] = arParams.ARCHART_CAPTION[i].split(';');
            arParams.ARCHART_CAPTION[i] = moment(archart_caption_dates[i][0], timeFormat).format("MMMM YY") + ' - ' + moment(archart_caption_dates[i][1], timeFormat).format("MMMM YY");
        }
    }

    if (NEW_CHART_TYPE != 'pie') {
        arParams.ARCHART_DATE.forEach(function (items_array, j) {
                archart_date = items_array.split(';');
                archart_date_timeformat = [];
                if ((arParams.CHART_TYPES[j] == 'line') && (CHART_TYPE_MIX)) {
                    archart_date.forEach(function (items, i) {
                            archart_date_timeformat[i] = moment(items, timeFormat).add(15, 'day');
                        }
                    );
                }
                else {
                    archart_date.forEach(function (items, i) {
                            archart_date_timeformat[i] = moment(items, timeFormat);
                        }
                    );
                }
                archart_alldate_timeformat[j] = archart_date_timeformat;
            }
        );

        //������ ����������� �� ��������� ����� 12 ������� �� ������� ����
        display_period_on_chart_min_chartdata = moment.min(archart_date_timeformat); // ������� �� ������� ������� ������
        display_period_on_chart_min_param = moment().subtract(parseInt(arParams.DISPLAY_PERIOD_XAXES), 'month').startOf('month');        // ������� ������������ � ���������� ��� ��������������� �� ���������
        display_period_on_chart_min = moment(moment.max(display_period_on_chart_min_chartdata, display_period_on_chart_min_param)).subtract(15, 'day');
        display_period_on_chart_min_zoom = moment(moment.min(display_period_on_chart_min_chartdata, display_period_on_chart_min_param)).startOf('month').subtract(15, 'day');
        if (arParams.CHART_TYPE == "CHART_SUMM_SUMMPAYED") {
            display_period_on_chart_min_zoom = moment.min(moment(display_period_on_chart_min_chartdata).add(15, 'day'), display_period_on_chart_min_param);
        }

        //������ ����������� �� ��������� � ���� ������� ����
        display_period_on_chart_max = moment();

        var dataFirst = [], alldata = [], data_XY = [], data_XY_all = [];

        arParams.ARCHART_CAPTION.forEach(function (items, i) {
            data_XY_one = [];
            archart_val_item_array = arParams.ARCHART_VAL[i].split(';');
            data_XY = [];
            archart_val_item_array.forEach(function (val_item, k) {
                data_XY_one = [];
                data_XY_one = {
                    y: val_item,
                    x: archart_alldate_timeformat[i][k]
                };
                data_XY[k] = data_XY_one;
            });
            data_XY_all[i] = data_XY;

            dataFirst = {
                type: arParams.CHART_TYPES[i],
                steppedLine: arParams.STEPPEDLINE,
                label: items,
                data: data_XY_all[i],
                fill: true,
                borderColor: chartColors[i],
                backgroundColor: backgroundcolorColors[i],
                pointRadius: 3,
                pointHoverRadius: 5,
            };
            // ��������� ��� ������� � ������������ �������
            if ((i % 2 == 1) && (dataFirst.type == 'line')) dataFirst.borderDash = [5, 5];
            // ��������� ��� ��������� ���������
            if ((CHART_TYPE_MIX) && (dataFirst.type == 'line')) {
                dataFirst.backgroundColor = color(backgroundcolorColors[i]).alpha(0.3).rgbString();
                dataFirst.borderColor = color(chartColors[i]).alpha(0.3).rgbString();
                dataFirst.borderDash = [5, 5];
                dataFirst.pointRadius = 1;
                dataFirst.pointHoverRadius = 1;
            }
            else {
                if (dataFirst.type == 'line') {
                    dataFirst.backgroundColor = color(backgroundcolorColors[i]).alpha(0.6).rgbString();
                }
            }
            alldata[i] = dataFirst;
        });
    }
    else {
        //��������� ��� �������� ���������
        var dataFirst = [], alldata = [], chartColorsHover = [];

        chartColors.forEach(function (items, i) {
            chartColorsHover[i] = color(chartColors[i]).darken(0.3).rgbString()
        });

        arParams.ARCHART_VAL.forEach(function (items, i) {
            archart_val_item_array = items.split(';');
            dataFirst = {
                type: arParams.CHART_TYPES[i],
                label: arParams.ARCHART_CAPTION,
                data: archart_val_item_array,
                backgroundColor: chartColors,
                title: arParams.ARCHART_DATE[i],
                hoverBackgroundColor: chartColorsHover,
                borderWidth: 3
            };
            alldata[i] = dataFirst;
        });
    }
    speedData = {datasets: alldata};

// ===================================================== ���
    if (NEW_CHART_TYPE != 'pie') {
        options_array.scales = {
            xAxes: [{
                stacked: arParams.STACKED,
                type: "time",
                scaleLabel: {
                    display: true,
                    labelString: arParams.CITRUS_TSZH_PERIOD,
                    fontColor: "#000000",
                    fontFamily: "Roboto Light",
                    fontSize: 14,
                    padding: {top: 10, bottom: 25}
                },
                ticks: {
                    fontColor: "#000000",
                    fontFamily: "Roboto Light",
                }
            }],
            yAxes: [{
                stacked: arParams.STACKED,
                scaleLabel: {
                    display: true,
                    labelString: arParams.CITRUS_TSZH_CONSUMPTION,
                    fontColor: "#000000",
                    fontFamily: "Roboto Light",
                    fontSize: 14,
                    padding: {top: 25, bottom: 10}
                },
                ticks: {
                    fontColor: "#000000",
                    fontFamily: "Roboto Light",
                }
            }]
        }
    };
// ===================================================== ��� ���� �� ����� �������, ���
    if (NEW_CHART_TYPE == 'pie') {
        options_array = {cutoutPercentage: 40};
        speedData.labels = arParams.ARCHART_CAPTION;
        options_array.hover = {mode:'index', intersect:'true'};
    }
    else {
        options_array.scales.xAxes[0].time = {
            min: display_period_on_chart_min,
            max: display_period_on_chart_max,
            unit: "month",
            displayFormats: {
                day: arParams.DISPLAY_FORMATS_XAXES,
                month: arParams.DISPLAY_FORMATS_XAXES,
                year: "YYYY"
            }
        };
        options_array.elements = {line: {tension: arParams.ELEMENTS_LINE_TENSION}};
        if (arParams.CHART_ZOOM) {
            options_array.zoom = {
                enabled: true,
                mode: 'x',
                rangeMin: {x: display_period_on_chart_min_zoom.valueOf()},
                rangeMax: {x: display_period_on_chart_max.valueOf()}
            };
            options_array.pan = {
                enabled: true,
                mode: 'x',
                rangeMin: {x: display_period_on_chart_min_zoom.valueOf()},
                rangeMax: {x: display_period_on_chart_max.valueOf()}
            };
        }
    }
// ===================================================== �c��������� ���������
    options_array.tooltips = {
        backgroundColor: "#ffffff",
        titleFontColor: "#2291f1",
        titleFontSize: 12,
        titleFontFamily: "Roboto Light",
        bodyFontColor: "#2291f1",
        bodyFontSize: 12,
        bodyFontFamily: "Roboto Light",
        borderColor: "#2291f1",
        borderWidth: 1,
        mode: 'index',
        intersect: false,
        cornerRadius: 0,
        bodySpacing: 5,
        xPadding: 10,
        yPadding: 10,
        titleMarginBottom: 10,
        callbacks: {
            title: function (tooltipItems, data) {
                return moment(moment(tooltipItems[0].xLabel._i)).format(arParams.DISPLAY_FORMATS_XAXES);
            },
            label: function(tooltipItem, data) {
                var label = data.datasets[tooltipItem.datasetIndex].label || '';

                if (label) {
                    label += ': ';
                }
                label += tooltipItem.yLabel + arParams.CHART_TOOLTIPS_CURRENCY;
                return label;
            },
        }
    };

    if (arParams.CHART_TYPE == 'CHART_RATE') {
        options_array.tooltips.callbacks.label = function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';
                    if (label) {label += ': ';}
                    label += tooltipItem.yLabel + arParams.CHART_TOOLTIPS_CURRENCY_RATE;
                    return label;
                }
            };

    if (NEW_CHART_TYPE == 'pie') {
        options_array.tooltips.PIE_SUMM_LAST_YEAR = arParams.PIE_SUMM_LAST_YEAR;
        options_array.tooltips.PIE_SUMM_CURRENT = arParams.PIE_SUMM_CURRENT;

        options_array.tooltips.mode = 'index';
        options_array.tooltips.intersect = true;
        options_array.tooltips.callbacks = {
            title: function (tooltipItems, data) {
                return data.labels[tooltipItems[0].index];

            },
            label: function(tooltipItem, data) {
                var dataLabel;
                if (tooltipItem.datasetIndex == 0) dataLabel = this._options.PIE_SUMM_LAST_YEAR;
                if (tooltipItem.datasetIndex == 1) dataLabel = this._options.PIE_SUMM_CURRENT;

                    var result = data.datasets[tooltipItem.datasetIndex].data.reduce(function(sum, current) {
                    return sum + parseInt(current);
                }, 0);
                var value = ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + arParams.CHART_TOOLTIPS_CURRENCY + ' (' + Math.round(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]/result*100) + '%)';
                dataLabel += value;
                return dataLabel;
            }
        }
    };
// ===================================================== �������
    options_array.legend = {
        position: arParams.LEGEND_POSITION,
        paddingTop: 25,
        labels: {fontFamily: "Roboto Light", fontSize: 12, fontColor: "#000000"}
    };
    if (arParams.CHART_TYPE == "CHART_DEBT")  options_array.legend.display = false;
// ===================================================== ��������
    if (arParams.ANIMATION_EASING) options_array.animation = {
        duration: arParams.ANIMATION_DURATION,
        easing: arParams.ANIMATION_EASING
    };
    if (arParams.ANIMATION_DURATION == "0") options_array.animation = false;

// ===================================================== ����� Chart
    popCanvas = document.getElementById(arParams.CHART_ID).getContext("2d");
    chart_tr = false;
    for (i = 0; i < myChart.length; i++) {
        if (myChart[i].canvas.id == arParams.CHART_ID) {
            myChart[i].destroy();
            myChart[i] = new Chart(popCanvas, {
                type: NEW_CHART_TYPE,
                data: speedData,
                options: options_array,
            });
            chart_tr = true;
        }
    }
    if (!chart_tr) {
        myChart[myChart.length] = new Chart(popCanvas, {
            type: NEW_CHART_TYPE,
            data: speedData,
            options: options_array,
        });
    }

}

