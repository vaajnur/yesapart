<?
use Citrus\Tszh\Types\ReceiptType;
use Bitrix\Main\Loader;

global $USER, $APPLICATION;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
    die();
}

if ($_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]] == "Y") return;

if (!Loader::includeModule('citrus.tszh'))
{
    if(!isset($_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]]))
    {
        ShowError(GetMessage("TSZH_CHART_TSZH_MODULE_NOT_FOUND_ERROR"));
        $_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]] = "Y";
    }
    return;
}

if (($_SESSION["TSZH_CHART_MODULE_WARNING"][$_SESSION["BX_SESSION_COUNTER"]] != "CheckEditionDONE")  && ($_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]] != "Y"))
{
    if (!CTszhFunctionalityController::CheckEdition())
    {
        if ($_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]] = "Y")
        return;
    }
    else $_SESSION["TSZH_CHART_MODULE_WARNING"][$_SESSION["BX_SESSION_COUNTER"]] = "CheckEditionDONE";
}

if (!tszhCheckMinEdition('standard'))
{
    if(!isset($_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]]))
    {
        ShowError(GetMessage("TSZH_CHART_MODULE_NOT_INCLUDED"));
        $_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]] = "Y";
    }
    return;
}

if (!Loader::includeModule('otr.tszhchart'))
{
    if(!isset($_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]]))
    {
        ShowError(GetMessage("TSZH_CHART_MODULE_NOT_FOUND_ERROR"));
        $_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]] = "Y";
    }
    return;
}

if (!tszhCheckMinEdition('smallbusiness') && !Loader::includeModule('otr.tszhchartinst'))
{
    if(!isset($_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]]))
    {
        ShowError(GetMessage("TSZH_CHART_MODULE_NOT_INCLUDED"));
        $_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]] = "Y";
    }
    return;
}

if (($_SESSION["TSZH_CHART_INST_MODULE_WARNING"][$_SESSION["BX_SESSION_COUNTER"]] != "CheckEditionDONE") && ($_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]] != "Y"))
{
    if (Loader::includeModule('otr.tszhchartinst'))
    {
        if (!CTszhChartInstFunctionalityController::CheckEdition())
        {
            $_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]] = "Y";
            return;
        }
        else $_SESSION["TSZH_CHART_INST_MODULE_WARNING"][$_SESSION["BX_SESSION_COUNTER"]] = "CheckEditionDONE";
    }
}


// ���� ������������ �� ����������� � ������ ���������� ��ƻ
// ������� ��������� � �������� ������ ����������
if (!CTszh::IsTenant() && !$USER->IsAdmin()) {
    $this->AbortResultCache();
    $APPLICATION->AuthForm(GetMessage("CITRUS_TSZH_NOT_A_MEMBER"));
    $_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]] = "Y";
    return;
}

// set default value for missing parameters, simple param check
$componentParams = CComponentUtil::GetComponentProps($this->getName());
if (is_array($componentParams))
{
    foreach ($componentParams["PARAMETERS"] as $paramName => $paramArray)
    {
        if (!is_set($arParams, $paramName) && is_set($paramArray, "DEFAULT"))
        {
            $arParams[$paramName] = $paramArray["DEFAULT"];
        }

        $paramArray["TYPE"] = ToUpper(is_set($paramArray, "TYPE") ? $paramArray["TYPE"] : "STRING");
        switch ($paramArray["TYPE"])
        {
            case 'INT':
                $arParams[$paramName] = IntVal($arParams[$paramName]);
                break;

            case 'LIST':
                if (!array_key_exists($arParams[$paramName], $paramArray['VALUES']))
                {
                    $arParams[$paramName] = $paramArray["DEFAULT"];
                }
                break;

            case 'CHECKBOX':
                $arParams[$paramName] = ($arParams[$paramName] == (is_set($paramArray, 'VALUE') ? $paramArray['VALUE'] : 'Y'));
                break;

            default:
                // string etc.
                break;
        }
    }
}


$chartType = Array(
    "CHART_DEBT" => GetMessage("TSZH_CHART_DEBT"),
    "CHART_SUMM" => GetMessage("TSZH_CHART_SUMM"),
    "CHART_SUMM_SUMMPAYED" => GetMessage("TSZH_CHART_SUMM_SUMMPAYED"),
    "CHART_RATE" => GetMessage("TSZH_CHART_RATE"),
    "CHART_PIE" => GetMessage("TSZH_CHART_PIE"),
);


// ���� ������������ �� �����������, ������� ����� �����������
if (!$USER->IsAuthorized())
{
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

if (!CTszh::IsTenant() && !$USER->IsAdmin())
{
    $APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"));
    $_SESSION["TSZH_CHART_MODULE_ERROR"][$_SESSION["BX_SESSION_COUNTER"]] = "Y";
    return;
}

$arResult=[];
$arAccount = CTszhAccount::GetByUserID($USER->GetID());
if (!$arAccount)
{
    echo GetMessage("TSZH_NO_ACCOUNT").GetMessage("TSZH_GRAFIC").$chartType[$arParams["CHART_TYPE"]]."\"<br>";
    return;
}
else $arResult['ACCOUNT'] = $arAccount;


if ($arParams["CHART_TYPE"] != "CHART_RATE") {
    $arFilter = array(
        "ACCOUNT_ID" => $arAccount["ID"]
    );

// ������ �������� ��������

    $rsAccPeriod = CTszhAccountPeriod::GetList(array(), $arFilter);

    while ($arAccPeriod = $rsAccPeriod->GetNext()) {
        $arPeriodsIDs[] = $arAccPeriod["PERIOD_ID"];
    }

    if (!$arPeriodsIDs) {
        echo GetMessage("TSZH_NO_DATA").GetMessage("TSZH_GRAFIC").$chartType[$arParams["CHART_TYPE"]]."\"<br>";
        return;
    }

    $arFilter = array(
        "TSZH_ID" => $arAccount["TSZH_ID"],
        "ACTIVE" => "Y",
        "@ID" => $arPeriodsIDs,
    );



    if (($arParams["CHART_TYPE"] == "CHART_PIE") || ($arParams["CHART_TYPE"] == "CHART_SUMM")) {
        $timeStamp = mktime(0, 0, 0, date("m")-1, date("d","01"), date("Y") - 1);
        if(Loader::includeModule("iblock"))
        {
            $date = ToLower(CIBlockFormatProperties::DateFormat($DB->DateFormatToPHP('YYYY-MM-DD'), $timeStamp));
        }
        $arFilter [">DATE"] = $date;
    }

    $rsPeriods = CTszhPeriod::GetList(
        Array('DATE' => 'DESC', 'ID' => 'DESC'),
        $arFilter,
        false,
        false
    );

    $months = Array();
    /** @noinspection PhpAssignmentInConditionInspection */
    while ($arPeriod = $rsPeriods->GetNext()) {
        $arPeriod['DISPLAY_NAME'] = CTszh::ToUpperFirstChar(CTszhPeriod::Format($arPeriod['DATE']));
        if (isset($months)) {
            if (array_key_exists($arPeriod['MONTH'], $months)) {
                // ��� ��� ������ � ����� ������� - ������� ������� � ��������
                $arPeriod['DISPLAY_NAME'] .= " ({$months[$arPeriod['MONTH']]})";
            }
        }
        $arPeriod['DETAIL_PAGE_URL'] = 'javascript:void();';
        $months[$arPeriod['MONTH']] += 1;

        $rsAccountPeriod = CTszhAccountPeriod::GetList(
            Array(),
            Array(
                'ACCOUNT_ID' => $arAccount["ID"],
                'PERIOD_ID' => $arPeriod['ID'],
                '@TYPE' => $fType,
            )
        );

        $arPeriod['TOTAL_CHARGES'] = $arPeriod['TOTAL_PAYED'] = $arPeriod['CORRECTION'] = 0;
        $arPeriod['CHARGES'] = Array();

        while ($arPeriodTemp = $rsAccountPeriod->GetNext()) {
            $arPeriod['ACCOUNT_PERIOD'] = $arPeriodTemp;
            $dbCharge = CTszhCharge::GetList(
                Array("SORT" => "ASC", "ID" => "ASC"),
                Array(
                    'ACCOUNT_PERIOD_ID' => $arPeriod['ACCOUNT_PERIOD']["ID"],
                )
            );
            $arPeriod['TOTAL_DEBT_BEG'] += $arPeriodTemp['DEBT_BEG'];
            $arPeriod['TOTAL_DEBT_END'] += $arPeriodTemp['DEBT_END'];
            /** @noinspection PhpAssignmentInConditionInspection */
            while ($arCharge = $dbCharge->GetNext()) {
                if ($arPeriodTemp['TYPE'] == ReceiptType::FINES_MAIN || $arPeriodTemp['TYPE'] == ReceiptType::FINES_OVERHAUL) {
                    $arCharge['SERVICE_NAME'] .= ' ' . GetMessage('PERIOD_FINES');
                }

                if (($arCharge["COMPONENT"] == "N") &&  ($arCharge['SUMM'] != '0.00')) {
                    if ($arCharge["IS_INSURANCE"] != "Y") {
                        $arPeriod['TOTAL_CHARGES'] += $arCharge['SUMM'];
                    }
                    $arPeriod['TOTAL_PAYED'] += $arCharge['SUMM_PAYED'];
                    $arPeriod['CORRECTION'] += $arCharge['CORRECTION'];
                    $arPeriod['CHARGES'][] = $arCharge;
                }
            }
        }

        $arResult['PERIODS'][] = $arPeriod;

        unset($arCharge);
        unset($arPeriodTemp);
    }

    unset($arFilter);

}
/* --------------------------------------------------- ��� ���������  �������� �����������*/
if ($arParams["CHART_TYPE"] == "CHART_RATE") {
    // ������� ��� ��������
    $dbResult = CTszhMeter::GetList(Array('SORT' => 'ASC', 'NAME' => 'ASC'),
        Array(
            "ACCOUNT_ID" => $arResult['ACCOUNT']['ID'],
            "ACTIVE" => "Y",
            "HOUSE_METER" => "N"
        ));


    $key=0;
    while ($arMeters = $dbResult->GetNext(true, false)) {
        $arMeter = $arMeters;

        // ������� ��������� ������� ��������  - �� ��������� ��� � �� ��� ��
        $timeStamp = mktime(0, 0, 0, date("m")-1, date("d", "01"), date("Y")-1);
        $timeStamp1 = mktime(0, 0, 0, date("m"), date("d", "01"), date("Y")-1);
        $timeStamp2 =  mktime(0, 0, 0, date("m")-1, date("d", "01"), date("Y")-2);


        $chart_rate_dates = date("Y-m", mktime(0, 0, 0, date('m'), date('d'), date('Y')-1)).";". date("Y-m", mktime(0, 0, 0, date('m')-1, date('d'), date('Y')))."|". date("Y-m", mktime(0, 0, 0, date('m'), date('d'), date('Y')-2)).";". date("Y-m", mktime(0, 0, 0, date('m')-1, date('d'), date('Y')-1));

        for ($i = 1; $i <= 2; $i++) {
            if ($i == 1) {
                $arFilter = Array(
                    "METER_ID" => $arMeter['ID'],
                    ">TIMESTAMP_X" => date($DB->DateFormatToPHP(FORMAT_DATETIME), $timeStamp),
                );
            } else {
                $arFilter = Array(
                    "METER_ID" => $arMeter['ID'],
                    ">TIMESTAMP_X" => date($DB->DateFormatToPHP(FORMAT_DATETIME), $timeStamp2),
                    "<TIMESTAMP_X" => date($DB->DateFormatToPHP(FORMAT_DATETIME), $timeStamp1),
                );
            }

            $rsValues = CTszhMeterValue::GetList(Array('TIMESTAMP_X' => 'DESC', "ID" => "DESC"), $arFilter, false, false);
            while ($arValue = $rsValues->GetNext()) {
                $date = $arValue["TIMESTAMP_X"];
                if ($i == 2) {
                    $timeStamp4 = MakeTimeStamp($date, "DD.MM.YYYY HH:MI:SS");
                    $timeStamp4 = mktime(0, 0, 0, date("m", $timeStamp4), date("d", 01), date("Y", $timeStamp4) + 1);
                } else $timeStamp4 = MakeTimeStamp($date);
                if(CModule::IncludeModule("iblock")) {
                    $date = ToLower(CIBlockFormatProperties::DateFormat($DB->DateFormatToPHP('YYYY-MM'), $timeStamp4));
                }
                if ($i == 1) {
                    if (isset($arMeter['ROWS'])) {
                        if (!(array_key_exists($date, $arMeter['ROWS']))) {
                            $arMeter['ROWS'][$date] = $arValue; // ����������� ������ ������ ��������� ��������  � �������
                        }
                    }
                    else $arMeter['ROWS'][$date] = $arValue;
                } else {
                    if (isset($arMeter['ROWS_last_year'])) {
                        if (!(array_key_exists($date, $arMeter['ROWS_last_year']))) {
                            $arMeter['ROWS_last_year'][$date] = $arValue;
                        }
                    }
                    else $arMeter['ROWS_last_year'][$date] = $arValue;
                }
            }

        }
        if ($arMeter['ROWS']) $arResult['ITEMS'][] = $arMeter;
        $key++;
    }

    if (!$arResult['ITEMS']) {
        echo GetMessage("TSZH_NO_METER").GetMessage("TSZH_GRAFIC"). $chartType[$arParams["CHART_TYPE"]]."\"<br>";
        return;
    }
}

// ���������� ��������� ID
$arResult["popchart_id"] = "canvas-chart-".uniqid();


// ���������� ������ ��� ������

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$charges_service_name_array = [];
$charges_sna = [];
$charges_PIE = [];
$charges_PIE_last_month = [];

if ($arParams["CHART_TYPE"] != "CHART_RATE") {
    if ($arResult['PERIODS'] ) {
        foreach ($arResult['PERIODS'] as &$period) {
            if ($period["ONLY_DEBT"] != "Y" && is_array($period["CHARGES"]) && !empty($period["CHARGES"]) && !empty($period['TOTAL_CHARGES'])) {
                foreach ($period["CHARGES"] as $key => &$charge) {
                    if ((!in_array($charge["SERVICE_ID"], $charges_service_name_array)) && !empty($charge['SUMM2PAY']) && ($charge["COMPONENT"] == "N"))
                        if ($charge["DEBT_ONLY"] == "N")
                            $charges_service_name_array ["$charge[SERVICE_ID]"] = $charge["SERVICE_NAME"];
                }
            }
        }
    }
    if ($arParams["CHART_TYPE"] != "CHART_DEBT")
    if (empty($charges_service_name_array))
    {
        echo GetMessage("TSZH_SHEET_NO_DATA").GetMessage("TSZH_GRAFIC").$chartType[$arParams["CHART_TYPE"]]."\"<br>";
        return;
    }

    $chart1_CHARGES0 = '';
    $chart1_PAYED0 = '';
    $chart1_date0 = '';
    $chart1_date = '';
    $charges_val_array = [];
    $charges_date_array = [];

    $key_month = 0;

    if ($arParams["CHART_TYPE"] == "CHART_DEBT") {
        foreach ($arResult['PERIODS'] as &$period) {
            if ($period["ONLY_DEBT"] != "Y") {
                $chart1_date0 .= ';' . $period['MONTH'];
                $chart1_total_debt_end0 .= ';' . round($period['TOTAL_DEBT_END']);
            }
        }
    }
    else {
        foreach ($arResult['PERIODS'] as &$period) {
            if ($period["ONLY_DEBT"] != "Y" && is_array($period["CHARGES"]) && !empty($period["CHARGES"]) && !empty($period['TOTAL_CHARGES'])) {
                $chart1_CHARGES0 .= ';' . round($period['TOTAL_CHARGES']);
                $chart1_PAYED0 .= ';' . round($period['TOTAL_PAYED']);
                $chart1_date0 .= ';' . $period['MONTH'];
                $chart1_total_debt_end0 .= ';' . $period['TOTAL_DEBT_END'];

                foreach ($period['CHARGES'] as $key => $charge) {
                    if (($charge["COMPONENT"] == "N") && ($charge["DEBT_ONLY"] == "N")) {
                        if ($charge["SUMM2PAY"] == null) $charge["SUMM2PAY"] = "0.00";
                        $charges_val_array["$charge[SERVICE_ID]"] .= ';' . round($charge["SUMM2PAY"]);
                        $charges_date_array["$charge[SERVICE_ID]"] .= ';' . $period['MONTH'];
                        $charges_sna["$charge[SERVICE_ID]"] = true;
                        $charges_PIE["$charge[SERVICE_ID]"] = $charges_PIE["$charge[SERVICE_ID]"] + round($charge["SUMM2PAY"]);
                        if ($key_month == 0) $charges_PIE_last_month ["$charge[SERVICE_ID]"] = round($charge["SUMM2PAY"]);
                    }
                }

                if ($key_month == 0) {
                    $last_month = $period['MONTH'];
                    $key_month++;
                }
                $first_month = $period['MONTH'];

                if (isset($charge)) unset($charge);
                foreach ($charges_service_name_array as $key => $charge) {
                    if (!$charges_sna[$key]) {
                        $charges_val_array[$key] .= ';0.00';
                        $charges_date_array[$key] .= ';' . $period['MONTH'];
                    }
                }
                if (isset($charge)) unset($charge);
                foreach ($charges_sna as $key => $value) {
                    $charges_sna[$key] = false;
                }
            }
        }

    }
    if ($arParams["CHART_TYPE"] == "CHART_SUMM_SUMMPAYED") {
        $enddate = end(explode(';', $chart1_date0));
        $enddate1 = MakeTimeStamp($enddate."-01","YYYY-MM-DD");
        $enddate2 = mktime(0, 0, 0, date("m", $enddate1)-1, date("d", $enddate1 ), date("Y", $enddate1));
        $enddate = ToLower(CIBlockFormatProperties::DateFormat($DB->DateFormatToPHP('YYYY-MM'), $enddate2));
        $chart1_date0 = $chart1_date0.";".$enddate;

        $chart1_CHARGES0 = $chart1_CHARGES0.";".end(explode(';', $chart1_CHARGES0));

    }


    $chart1_date .= '|' . substr($chart1_date0, 1);
    $chart1_date = $chart1_date . $chart1_date;
    $chart_captions = GetMessage("TSZH_SHEET_SUMM") . '|' . GetMessage("TSZH_SHEET_SUMMPAYED");

}

// ===================================================== ��� ������� ���������� � ����� � ��� ������� �����
if (($arParams["CHART_TYPE"] == "CHART_SUMM_SUMMPAYED") || ($arParams["CHART_TYPE"] == "CHART_DEBT")) {

    $arParams["ARCHART_CAPTION"] = $chart_captions;
    $arParams["ARCHART_VAL"] = substr($chart1_CHARGES0,1) . '|' . substr($chart1_PAYED0, 1);
    $arParams["ARCHART_DATE"] = substr($chart1_date, 1);


    if ($arParams["CHART_TYPE"] == "CHART_DEBT") {
        $arParams["ARCHART_VAL"] = substr($chart1_total_debt_end0, 1);
        $arParams["ARCHART_CAPTION"] = GetMessage("TSZH_SHEET_DEBT");
    }
}

// ===================================================== ��� ������� ����������� �����  � ������� �������������
if (($arParams["CHART_TYPE"] == "CHART_SUMM") || ($arParams["CHART_TYPE"] == "CHART_PIE") ) {
    foreach ($charges_val_array as $key => $value) $charges_val_array[$key] = substr($value, 1);
    $arParams["ARCHART_VAL"] = implode('|', $charges_val_array);
    foreach ($charges_date_array as $key => $value) $charges_date_array[$key] = substr($value, 1);
    $arParams["ARCHART_DATE"] = implode('|', $charges_date_array);
    $arParams["ARCHART_CAPTION"] = implode('|', $charges_service_name_array);
}

// ===================================================== ������������� ��� ������� �������������
if ($arParams["CHART_TYPE"] == "CHART_PIE") {
    foreach ($charges_PIE_last_month as $key => $value) {
        if ($charges_PIE_last_month[$key] == "") $charges_PIE_last_month[$key] = "0.00";
    }
    $arParams["ARCHART_DATE"] = $first_month.";".$last_month."|".$last_month;
    $arParams["ARCHART_VAL"] = implode(';', $charges_PIE) . "|" . implode(';', $charges_PIE_last_month);

}

// ===================================================== ��� ������� ��������� �����������
if ($arParams["CHART_TYPE"] == "CHART_RATE") {
    $result_service_rate = [];
    $results_rate_ch = [];
    $results_rate_last_year_ch = [];
    foreach ($arResult['ITEMS'] as &$items) {
        $results_rate = [];
        $results_rate_last_year = [];

        if (isset($items["ROWS"])) $items["ROWS"] = array_reverse($items["ROWS"]);
        if (isset($items["ROWS_last_year"])) $items["ROWS_last_year"] = array_reverse($items["ROWS_last_year"]);

        for ($k = 1; $k <= 2; $k++) {
            if ($k == 1) $items_ROWS = $items["ROWS"];
            else $items_ROWS = $items["ROWS_last_year"];
            $MAX_VALUES_COUNT = $items["VALUES_COUNT"];
            for ($i = 1; $i <= $MAX_VALUES_COUNT; $i++) {
                $lastdata = 0;
                $item = 0;
                $periods = array();
                if (isset($items_ROWS)) {
                    foreach ($items_ROWS as $date => $arItem) {
                        if ($item == 0) $lastdata = $arItem["VALUE" . $i];
                        $timeStamp_items = mktime(0, 0, 0, date("m", strtotime($date)), date("d", "01"), date("Y", strtotime($date)));
                        if (strtotime($date) < mktime(0, 0, 0, date("m", $timeStamp) + 1, date("d", "01"), date("Y", $timeStamp))) {
                            $item++;
                            continue;
                        }

                        $periods[$item]['date'] = $date;
                        $periods[$item]['data'] = $arItem["VALUE" . $i] > 0 ? $arItem["VALUE" . $i] : "";
                        $periods[$item]['rate'] = $periods[$item]['data'] != "" ? $periods[$item]['data'] - $lastdata : 0;
                        if ($item == 0) $periods[$item]['rate'] = $periods[$item]['data'];
                        $lastdata = $periods[$item]['data'];
                        $item++;
                    }
                }

                krsort($periods);
                foreach ($periods as $key => $Item) { // ��������� ��������� �� �������
                    $results_rate[$Item["date"]][$k] = $results_rate[$Item["date"]][$k] + $Item["rate"];
                }
            }
        }
        $ifempty = false;
        foreach ($results_rate as $key => $Item) {
            for ($k = 1; $k <= 2; $k++) {
                if (empty($results_rate[$key][$k])) $results_rate[$key][$k] = 0;
                if ($results_rate[$key][$k]) $ifempty = true;
            }
        }

        if (!$ifempty) continue;

        $indexofservicename = array_search($items["SERVICE_NAME"], $charges_service_name_array);

        if ($indexofservicename === false) {
            if (!$results_rate_ch[count($charges_service_name_array)]) $results_rate_ch[count($charges_service_name_array)] = $results_rate;
            $charges_service_name_array [] = $items["SERVICE_NAME"];
        }
        else {
            foreach ($results_rate_ch[$indexofservicename] as $key => $Item) {
                for ($k = 1; $k <= 2; $k++) {
                    $results_rate_ch[$indexofservicename][$key][$k] = $results_rate_ch[$indexofservicename][$key][$k] + $results_rate[$key][$k];
                }
            }
        }
    }

    if ($results_rate_ch) {
        ksort($results_rate_ch);
        // ������� � ������ c �����������

        foreach ($results_rate_ch as $key => $period) {
            ksort($period);
            $chart_rate_year_date = "";
            $chart_rate_year_vals = "";
            $chart_rate_last_year_vals = "";
            ksort($period);
            foreach ($period as $k => $rate) {
                $chart_rate_year_date .= ';' . $k;
                $chart_rate_year_vals .= ';' . $rate[1];
                $chart_rate_last_year_vals .= ';' . $rate[2];
            }
            $chart_rate_vals[$key] = substr($chart_rate_year_vals, 1).'|' . substr($chart_rate_last_year_vals, 1);
            $chart_rate_date[$key] = substr($chart_rate_year_date, 1).'|' . substr($chart_rate_year_date, 1);
            $chart_rate_captions[$key] = $charges_service_name_array[$key];

        }
    }

    $arParams["ARCHART_CAPTION"] = $chart_rate_captions;
    $arParams["ARCHART_VAL"] = $chart_rate_vals;
    $arParams["ARCHART_DATE"] = $chart_rate_date;

 if (!empty($arParams["ARCHART_CAPTION"]))
    foreach ($arParams["ARCHART_CAPTION"] as $key => $charges_service_name) {
        $arParams["CHART_RATE"] [$key] ["ARCHART_VAL"] = $arParams["ARCHART_VAL"][$key];
        $arParams["CHART_RATE"] [$key] ["ARCHART_DATE"] = $arParams["ARCHART_DATE"][$key];
        $arParams["CHART_RATE"] [$key] ["ARCHART_CAPTION"] = $arParams["ARCHART_CAPTION"][$key];
        $arParams["CHART_RATE"] [$key] ["ARCHART_LEGEND"] = $chart_rate_dates;
    }
}


if (!(($arParams["ARCHART_VAL"]) && ($arParams["ARCHART_DATE"]) && ($arParams["ARCHART_CAPTION"])))
{
    echo GetMessage("TSZH_SHEET_NO_DATA").GetMessage("TSZH_GRAFIC").$chartType[$arParams["CHART_TYPE"]]."\"<br>";
    return;
}


// ����������� ������� ����������
$this->IncludeComponentTemplate();
