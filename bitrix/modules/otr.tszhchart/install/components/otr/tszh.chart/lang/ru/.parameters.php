<?
$MESS["TSZH_CHART_PARAMETERS"] = "Общие параметры графика";

$MESS["TSZH_CHART_TYPE"] = "Тип графика";
$MESS["TSZH_CHART_DEBT"] = "Задолженность";
$MESS["TSZH_CHART_SUMM"] = "Ежемесячные начисления в разрезе услуг";
$MESS["TSZH_CHART_SUMM_SUMMPAYED"] = "Ежемесячные начисления и платежи";
$MESS["TSZH_CHART_RATE"] = "Потребление услуг по приборам учета";
$MESS["TSZH_CHART_PIE"] = "Начисления в разрезе услуг за предыдущие 1 и 12 месяцев";

$MESS["TSZH_CHART_ZOOM"] = "Масштабирование";
$MESS["TSZH_CHART_COLORS"] = "Цвета графиков (для примера - #ffc312,#4cd137)";

$MESS["TSZH_CHART_OPTIONS_LEGEND"] = "Настройки легенды";
$MESS["TSZH_CHART_LEGEND_POSITION"] = "Расположение легенды";
$MESS["TSZH_CHART_LEGEND_POSITION_TOP"] = "Сверху";
$MESS["TSZH_CHART_LEGEND_POSITION_LEFT"] = "Слева";
$MESS["TSZH_CHART_LEGEND_POSITION_BOTTOM"] = "Снизу";
$MESS["TSZH_CHART_LEGEND_POSITION_RIGHT"] = "Справа";

$MESS["TSZH_CHART_OPTIONS_ANIMATION"] = "Настройки анимации";
$MESS["TSZH_CHART_ANIMATION_EASING"] = "Эффект анимации";
$MESS["TSZH_CHART_ANIMATION_DURATION"] = "Время, которое занимает анимация (в миллисекундах)";

?>