<?php
$files2delete = array (
  0 => '.idea/copyright/profiles_settings.xml',
  1 => '.idea/deployment.xml',
  2 => '.idea/encodings.xml',
  3 => '.idea/misc.xml',
  4 => '.idea/modules.xml',
  5 => '.idea/otr.sale.iml',
  6 => '.idea/workspace.xml',
);
$removeEmptySubFolders = function($path) use (&$removeEmptySubFolders)
{
	$empty=true;
	foreach (glob($path.DIRECTORY_SEPARATOR."*") as $file)
		$empty &= is_dir($file) && $removeEmptySubFolders($file);
	return $empty && rmdir($path);
};
$doc_root = $_SERVER["DOCUMENT_ROOT"];
$module_path = '/bitrix/modules/otr.sale/';
foreach ($files2delete as $file)
{
	if (file_exists($doc_root . $module_path . $file))
		DeleteDirFilesEx($module_path . $file);
	$folder = dirname($doc_root . $module_path . $file);
		if (file_exists($folder) && is_dir($folder))
		$removeEmptySubFolders($folder);
}