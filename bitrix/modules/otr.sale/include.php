<?
use Bitrix\Main\Loader;
define("SALE_DEBUG", false); // Debug

global $DBType;

IncludeModuleLangFile(__FILE__);

$GLOBALS["SALE_FIELD_TYPES"] = array(
	"TEXT" => GetMessage("SALE_TYPE_TEXT"),
	"CHECKBOX" => GetMessage("SALE_TYPE_CHECKBOX"),
	"SELECT" => GetMessage("SALE_TYPE_SELECT"),
	"MULTISELECT" => GetMessage("SALE_TYPE_MULTISELECT"),
	"TEXTAREA" => GetMessage("SALE_TYPE_TEXTAREA"),
	"LOCATION" => GetMessage("SALE_TYPE_LOCATION"),
	"RADIO" => GetMessage("SALE_TYPE_RADIO"),
	"FILE" => GetMessage("SALE_TYPE_FILE")
);



// Number of processed recurring records at one time
define("SALE_PROC_REC_NUM", 3);
// Number of recurring payment attempts
define("SALE_PROC_REC_ATTEMPTS", 3);
// Time between recurring payment attempts (in seconds)
define("SALE_PROC_REC_TIME", 43200);

define("SALE_PROC_REC_FREQUENCY", 7200);
// Owner ID base name used by CSale<etnity_name>ReportHelper clases for managing the reports.
define("SALE_REPORT_OWNER_ID", 'otr.sale');
//cache orders flag for real-time exhange with 1C
define("CACHED_b_sale_order", 3600*24);

global $SALE_TIME_PERIOD_TYPES;
$SALE_TIME_PERIOD_TYPES = array(
	"H" => GetMessage("I_PERIOD_HOUR"),
	"D" => GetMessage("I_PERIOD_DAY"),
	"W" => GetMessage("I_PERIOD_WEEK"),
	"M" => GetMessage("I_PERIOD_MONTH"),
	"Q" => GetMessage("I_PERIOD_QUART"),
	"S" => GetMessage("I_PERIOD_SEMIYEAR"),
	"Y" => GetMessage("I_PERIOD_YEAR")
);

define("SALE_VALUE_PRECISION", 4);
define("SALE_WEIGHT_PRECISION", 3);

define('BX_SALE_MENU_CATALOG_CLEAR', 'Y');

/*$GLOBALS["AVAILABLE_ORDER_FIELDS"] = array(
	"ID" => array("COLUMN_NAME" => "ID", "NAME" => GetMessage("SI_ORDER_ID"), "SELECT" => "ID,DATE_INSERT", "CUSTOM" => "Y", "SORT" => "ID"),
	"LID" => array("COLUMN_NAME" => GetMessage("SI_SITE"), "NAME" => GetMessage("SI_SITE"), "SELECT" => "LID", "CUSTOM" => "N", "SORT" => "LID"),
	"PERSON_TYPE" => array("COLUMN_NAME" => GetMessage("SI_PAYER_TYPE"), "NAME" => GetMessage("SI_PAYER_TYPE"), "SELECT" => "PERSON_TYPE_ID", "CUSTOM" => "Y", "SORT" => "PERSON_TYPE_ID"),
	"PAYED" => array("COLUMN_NAME" => GetMessage("SI_PAID"), "NAME" => GetMessage("SI_PAID_ORDER"), "SELECT" => "PAYED,DATE_PAYED,EMP_PAYED_ID", "CUSTOM" => "Y", "SORT" => "PAYED"),
	"PAY_VOUCHER_NUM" => array("COLUMN_NAME" => GetMessage("SI_NO_PP"), "NAME" => GetMessage("SI_NO_PP_DOC"), "SELECT" => "PAY_VOUCHER_NUM", "CUSTOM" => "N", "SORT" => "PAY_VOUCHER_NUM"),
	"PAY_VOUCHER_DATE" => array("COLUMN_NAME" => GetMessage("SI_DATE_PP"), "NAME" => GetMessage("SI_DATE_PP_DOC"), "SELECT" => "PAY_VOUCHER_DATE", "CUSTOM" => "N", "SORT" => "PAY_VOUCHER_DATE"),
	"DELIVERY_DOC_NUM" => array("COLUMN_NAME" => GetMessage("SI_DATE_PP_DELIVERY_DOC_NUM"), "NAME" => GetMessage("SI_DATE_PP_DOC_DELIVERY_DOC_NUM"), "SELECT" => "DELIVERY_DOC_NUM", "CUSTOM" => "N", "SORT" => "DELIVERY_DOC_NUM"),
	"DELIVERY_DOC_DATE" => array("COLUMN_NAME" => GetMessage("SI_DATE_PP_DELIVERY_DOC_DATE"), "NAME" => GetMessage("SI_DATE_PP_DOC_DELIVERY_DOC_DATE"), "SELECT" => "DELIVERY_DOC_DATE", "CUSTOM" => "N", "SORT" => "DELIVERY_DOC_DATE"),
	"CANCELED" => array("COLUMN_NAME" => GetMessage("SI_CANCELED"), "NAME" => GetMessage("SI_CANCELED_ORD"), "SELECT" => "CANCELED,DATE_CANCELED,EMP_CANCELED_ID", "CUSTOM" => "Y", "SORT" => "CANCELED"),
	"STATUS" => array("COLUMN_NAME" => GetMessage("SI_STATUS"), "NAME" => GetMessage("SI_STATUS_ORD"), "SELECT" => "STATUS_ID,DATE_STATUS,EMP_STATUS_ID", "CUSTOM" => "Y", "SORT" => "STATUS_ID"),
	"PRICE_DELIVERY" => array("COLUMN_NAME" => GetMessage("SI_DELIVERY"), "NAME" => GetMessage("SI_DELIVERY"), "SELECT" => "PRICE_DELIVERY,CURRENCY", "CUSTOM" => "Y", "SORT" => "PRICE_DELIVERY"),
	"ALLOW_DELIVERY" => array("COLUMN_NAME" => GetMessage("SI_ALLOW_DELIVERY"), "NAME" => GetMessage("SI_ALLOW_DELIVERY1"), "SELECT" => "ALLOW_DELIVERY,DATE_ALLOW_DELIVERY,EMP_ALLOW_DELIVERY_ID", "CUSTOM" => "Y", "SORT" => "ALLOW_DELIVERY"),
	"PRICE" => array("COLUMN_NAME" => GetMessage("SI_SUM"), "NAME" => GetMessage("SI_SUM_ORD"), "SELECT" => "PRICE,CURRENCY", "CUSTOM" => "Y", "SORT" => "PRICE"),
	"SUM_PAID" => array("COLUMN_NAME" => GetMessage("SI_SUM_PAID"), "NAME" => GetMessage("SI_SUM_PAID1"), "SELECT" => "SUM_PAID,CURRENCY", "CUSTOM" => "Y", "SORT" => "SUM_PAID"),
	"USER" => array("COLUMN_NAME" => GetMessage("SI_BUYER"), "NAME" => GetMessage("SI_BUYER"), "SELECT" => "USER_ID", "CUSTOM" => "Y", "SORT" => "USER_ID"),
	"PAY_SYSTEM" => array("COLUMN_NAME" => GetMessage("SI_PAY_SYS"), "NAME" => GetMessage("SI_PAY_SYS"), "SELECT" => "PAY_SYSTEM_ID", "CUSTOM" => "Y", "SORT" => "PAY_SYSTEM_ID"),
	"DELIVERY" => array("COLUMN_NAME" => GetMessage("SI_DELIVERY_SYS"), "NAME" => GetMessage("SI_DELIVERY_SYS"), "SELECT" => "DELIVERY_ID", "CUSTOM" => "Y", "SORT" => "DELIVERY_ID"),
	"DATE_UPDATE" => array("COLUMN_NAME" => GetMessage("SI_DATE_UPDATE"), "NAME" => GetMessage("SI_DATE_UPDATE"), "SELECT" => "DATE_UPDATE", "CUSTOM" => "N", "SORT" => "DATE_UPDATE"),
	"PS_STATUS" => array("COLUMN_NAME" => GetMessage("SI_PAYMENT_PS"), "NAME" => GetMessage("SI_PS_STATUS"), "SELECT" => "PS_STATUS,PS_RESPONSE_DATE", "CUSTOM" => "N", "SORT" => "PS_STATUS"),
	"PS_SUM" => array("COLUMN_NAME" => GetMessage("SI_PS_SUM"), "NAME" => GetMessage("SI_PS_SUM1"), "SELECT" => "PS_SUM,PS_CURRENCY", "CUSTOM" => "Y", "SORT" => "PS_SUM"),
	"TAX_VALUE" => array("COLUMN_NAME" => GetMessage("SI_TAX"), "NAME" => GetMessage("SI_TAX_SUM"), "SELECT" => "TAX_VALUE,CURRENCY", "CUSTOM" => "Y", "SORT" => "TAX_VALUE"),
	"BASKET" => array("COLUMN_NAME" => GetMessage("SI_ITEMS"), "NAME" => GetMessage("SI_ITEMS_ORD"), "SELECT" => "", "CUSTOM" => "Y", "SORT" => "")
);*/

CModule::AddAutoloadClasses(
	"otr.sale",
	array(
		"otr.sale" => "install/index.php",
		/*"CSaleDelivery" => $DBType."/delivery.php",
		"CSaleDeliveryHandler" => $DBType."/delivery_handler.php",
		"CSaleDeliveryHelper" => "general/delivery_helper.php",
		"CSaleDelivery2PaySystem" => "general/delivery_2_pay_system.php",
		"CSaleLocation" => $DBType."/location.php",
		"CSaleLocationGroup" => $DBType."/location_group.php",

		"CSaleBasket" => $DBType."/basket.php",
		"CSaleBasketHelper" => "general/basket_helper.php",
		"CSaleUser" => $DBType."/basket.php",

		"CSaleOrder" => $DBType."/order.php",
		"CSaleOrderPropsGroup" => $DBType."/order_props_group.php",
		"CSaleOrderPropsVariant" => $DBType."/order_props_variant.php",
		"CSaleOrderUserProps" => $DBType."/order_user_props.php",
		"CSaleOrderUserPropsValue" => $DBType."/order_user_props_value.php",
		"CSaleOrderTax" => $DBType."/order_tax.php",
		"CSaleOrderHelper" => "general/order_helper.php",

		"CSalePaySystem" => $DBType."/pay_system.php",
		"CSalePaySystemAction" => $DBType."/pay_system_action.php",
		"CSalePaySystemsHelper" => "general/pay_system_helper.php",
		"CSalePaySystemTarif" => "general/pay_system_tarif.php",

		"CSaleTax" => $DBType."/tax.php",
		"CSaleTaxRate" => $DBType."/tax_rate.php",

		"CSalePersonType" => $DBType."/person_type.php",
		"CSaleDiscount" => $DBType."/discount.php",
		"CSaleBasketDiscountConvert" => "general/step_operations.php",
		"CSaleDiscountReindex" => "general/step_operations.php",
		"CSaleDiscountConvertExt" => "general/step_operations.php",
		"CSaleUserAccount" => $DBType."/user.php",
		"CSaleUserTransact" => $DBType."/user_transact.php",
		"CSaleUserCards" => $DBType."/user_cards.php",
		"CSaleRecurring" => $DBType."/recurring.php",


		"CSaleLang" => $DBType."/settings.php",
		"CSaleGroupAccessToSite" => $DBType."/settings.php",
		"CSaleGroupAccessToFlag" => $DBType."/settings.php",

		"CSaleAuxiliary" => $DBType."/auxiliary.php",

		"CSaleAffiliate" => $DBType."/affiliate.php",
		"CSaleAffiliatePlan" => $DBType."/affiliate_plan.php",
		"CSaleAffiliatePlanSection" => $DBType."/affiliate_plan_section.php",
		"CSaleAffiliateTier" => $DBType."/affiliate_tier.php",
		"CSaleAffiliateTransact" => $DBType."/affiliate_transact.php",
		"CSaleExport" => "general/export.php", //"CSaleExport" => $DBType."/export.php",
		"CSaleOrderLoader" => "general/order_loader.php",

		"CSaleMeasure" => "general/measurement.php",
		"CSaleProduct" => $DBType."/product.php",

		"CSaleViewedProduct" => $DBType."/product.php",

		"CSaleHelper" => "general/helper.php",
		"CSaleMobileOrderUtils" => "general/mobile_order.php",
		"CSaleMobileOrderPull" => "general/mobile_order.php",
		"CSaleMobileOrderPush" => "general/mobile_order.php",
		"CSaleMobileOrderFilter" => "general/mobile_order.php",

		"CBaseSaleReportHelper" => "general/sale_report_helper.php",
		"CSaleReportSaleOrderHelper" => "general/sale_report_helper.php",
		"CSaleReportUserHelper" => "general/sale_report_helper.php",
		"CSaleReportSaleFuserHelper" => "general/sale_report_helper.php",

		"IBXSaleProductProvider" => "general/product_provider.php",
		"CSaleStoreBarcode" => $DBType."/store_barcode.php",

		"CSaleOrderChange" => $DBType."/order_change.php",
		"CSaleOrderChangeFormat" => "general/order_change.php",

		"\\Otr\\Sale\\Internals\\FuserTable" => "lib/internals/fuser.php",
		"\\Otr\\Sale\\FuserTable" => "lib/internals/fuser_old.php",
		"\\Otr\\Sale\\Fuser" => "lib/fuser.php",

		// begin lists
		'\Otr\Sale\Internals\Input\Manager' => 'lib/internals/input.php',
		'\Otr\Sale\Internals\Input\Base'    => 'lib/internals/input.php',
		'\Otr\Sale\Internals\Input\File'    => 'lib/internals/input.php',
		'\Otr\Sale\Internals\Input\StringInput'    => 'lib/internals/input.php',

		'\Otr\Sale\Internals\SiteCurrencyTable' => 'lib/internals/sitecurrency.php',

		'CSaleStatus'                                 => 'general/status.php',
		'\Otr\Sale\OrderStatus'                    => 'lib/status.php',
		'\Otr\Sale\DeliveryStatus'                 => 'lib/status.php',
		'\Otr\Sale\Internals\StatusTable'          => 'lib/internals/status.php',
		'\Otr\Sale\Internals\StatusLangTable'      => 'lib/internals/status_lang.php',
		'\Otr\Sale\Internals\StatusGroupTaskTable' => 'lib/internals/status_grouptask.php',
		'CSaleOrderProps'                                => 'general/order_props.php',
		'CSaleOrderPropsAdapter'                         => 'general/order_props.php',
		'CSaleOrderPropsValue'                           => 'general/order_props_values.php',
		'\Otr\Sale\PropertyValueCollection'           => 'lib/propertyvaluecollection.php',
		'\Otr\Sale\Internals\OrderPropsTable'         => 'lib/internals/orderprops.php',
		'\Otr\Sale\Internals\OrderPropsGroupTable'    => 'lib/internals/orderprops_group.php',
		'\Otr\Sale\Internals\OrderPropsValueTable'    => 'lib/internals/orderprops_value.php',
		'\Otr\Sale\Internals\OrderPropsVariantTable'  => 'lib/internals/orderprops_variant.php',
		'\Otr\Sale\Internals\OrderPropsRelationTable' => 'lib/internals/orderprops_relation.php',
		'\Otr\Sale\Internals\UserPropsValueTable'     => 'lib/internals/userpropsvalue.php',
		'\Otr\Sale\Internals\UserPropsTable'          => 'lib/internals/userprops.php',
		'\Otr\Sale\BusinessValue'                            => 'lib/businessvalue.php',
		'\Otr\Sale\IBusinessValueProvider'                   => 'lib/businessvalueproviderinterface.php',
		'\Otr\Sale\Internals\BusinessValueTable'             => 'lib/internals/businessvalue.php',
		'\Otr\Sale\Internals\BusinessValuePersonDomainTable' => 'lib/internals/businessvalue_persondomain.php',
		'\Otr\Sale\Internals\BusinessValueCode1CTable'       => 'lib/internals/businessvalue_code_1c.php',
		'\Otr\Sale\Internals\PaySystemActionTable' => 'lib/internals/paysystemaction.php',
		'\Otr\Sale\Internals\PaySystemInner' => 'lib/internals/paysysteminner.php',
		'\Otr\Sale\Internals\DeliveryPaySystemTable' => 'lib/internals/delivery_paysystem.php',
		'\Otr\Sale\UserMessageException' => 'lib/exception.php',
		// end lists


		"Otr\\Sale\\Internals\\DeliveryHandlerTable" => "lib/internals/deliveryhandler.php",
		"Otr\\Sale\\DeliveryHandlerTable" => "lib/internals/deliveryhandler_old.php",

		"\\Otr\\Sale\\Configuration" => "lib/configuration.php",
		"\\Otr\\Sale\\Order" => "lib/order.php",
		"\\Otr\\Sale\\PersonType" => "lib/persontype.php",

		"CSaleReportSaleGoodsHelper" => "general/sale_report_helper.php",
		"CSaleReportSaleProductHelper" => "general/sale_report_helper.php",

		"Otr\\Sale\\ProductTable" => "lib/internals/product_old.php",
		"\\Otr\\Sale\\ProductTable" => "lib/internals/product_old.php",
		"\\Otr\\Sale\\Internals\\ProductTable" => "lib/internals/product.php",

		"Otr\\Sale\\GoodsSectionTable" => "lib/internals/goodssection_old.php",
		"\\Otr\\Sale\\GoodsSectionTable" => "lib/internals/goodssection_old.php",
		"\\Otr\\Sale\\Internals\\GoodsSectionTable" => "lib/internals/goodssection.php",

		"Otr\\Sale\\SectionTable" => "lib/internals/section_old.php",
		"\\Otr\\Sale\\SectionTable" => "lib/internals/section_old.php",
		"\\Otr\\Sale\\Internals\\SectionTable" => "lib/internals/section.php",

		"\\Otr\\Sale\\Internals\\StoreProductTable" => "lib/internals/storeproduct.php",
		"\\Otr\\Sale\\StoreProductTable" => "lib/internals/storeproduct_old.php",



		"\\Otr\\Sale\\SalesZone" => "lib/saleszone.php",
		"Otr\\Sale\\Internals\\OrderDeliveryReqTable" => "lib/internals/orderdeliveryreq.php",
		"\\Otr\\Sale\\Internals\\OrderDeliveryReqTable" => "lib/internals/orderdeliveryreq.php",

		"Otr\\Sale\\SenderEventHandler" => "lib/senderconnector.php",
		"Otr\\Sale\\SenderConnectorBuyer" => "lib/senderconnector.php",

		"\\Otr\\Sale\\UserConsent" => "lib/userconsent.php",

		"\\Otr\\Sale\\Product2ProductTable" => "lib/internals/product2product_old.php",
		"\\Otr\\Sale\\Internals\\Product2ProductTable" => "lib/internals/product2product.php",

		"Otr\\Sale\\OrderProcessingTable" => "lib/internals/orderprocessing_old.php",
		"Otr\\Sale\\Internals\\OrderProcessingTable" => "lib/internals/orderprocessing.php",

		"\\Otr\\Sale\\OrderBase" => "lib/orderbase.php",
		"\\Otr\\Sale\\Internals\\Entity" => "lib/internals/entity.php",
		"\\Otr\\Sale\\Internals\\EntityCollection" => "lib/internals/entitycollection.php",
		"\\Otr\\Sale\\Internals\\CollectionBase" => "lib/internals/collectionbase.php",
		//"\\Otr\\Sale\\Order" => "lib/order.php",

		"\\Otr\\Sale\\Shipment" => "lib/shipment.php",
		"\\Otr\\Sale\\ShipmentCollection" => "lib/shipmentcollection.php",
		"\\Otr\\Sale\\ShipmentItemCollection" => "lib/shipmentitemcollection.php",
		"\\Otr\\Sale\\ShipmentItem" => "lib/shipmentitem.php",
		"\\Otr\\Sale\\ShipmentItemStoreCollection" => "lib/shipmentitemstorecollection.php",
		"\\Otr\\Sale\\ShipmentItemStore" => "lib/shipmentitemstore.php",

		"\\Otr\\Sale\\PaymentCollectionBase" => "lib/internals/paymentcollectionbase.php",
		"\\Otr\\Sale\\PaymentCollection" => "lib/paymentcollection.php",
		"\\Otr\\Sale\\Payment" => "lib/payment.php",
		"\\Otr\\Sale\\PaysystemService" => "lib/paysystemservice.php",
		"\\Otr\\Sale\\Internals\\Fields" => "lib/internals/fields.php",
		"\\Otr\\Sale\\Result" => "lib/result.php",
		"\\Otr\\Sale\\ResultError" => "lib/result.php",
		"\\Otr\\Sale\\ResultSerializable" => "lib/resultserializable.php",
		"\\Otr\\Sale\\EventActions" => "lib/eventactions.php",

		"\\Otr\\Sale\\Internals\\PaymentBase" => "lib/internals/paymentbase.php",
		"\\Otr\\Sale\\BasketBase" => "lib/basketbase.php",
		"\\Otr\\Sale\\BasketItemBase" => "lib/basketitembase.php",
		"\\Otr\\Sale\\Basket" => "lib/basket.php",

		"\\Otr\\Sale\\Internals\\BasketItemBase" => "lib/internals/basketitembase.php",
		"\\Otr\\Sale\\BasketItem" => "lib/basketitem.php",
		"\\Otr\\Sale\\BasketBundleCollection" => "lib/basketbundlecollection.php",

		"\\Otr\\Sale\\OrderProperties" => "lib/orderprops.php",
		"\\Otr\\Sale\\PropertyValue" => "lib/propertyvalue.php",

		"\\Otr\\Sale\\Compatible\\Internals\\EntityCompatibility" => "lib/compatible/internals/entitycompatibility.php",
		"\\Otr\\Sale\\Compatible\\OrderCompatibility" => "lib/compatible/ordercompatibility.php",
		"\\Otr\\Sale\\Compatible\\BasketCompatibility" => "lib/compatible/basketcompatibility.php",
		"\\Otr\\Sale\\Compatible\\EventCompatibility" => "lib/compatible/eventcompatibility.php",

		'\Otr\Sale\Compatible\OrderQuery'   => 'lib/compatible/compatible.php',
		'\Otr\Sale\Compatible\OrderQueryLocation'   => 'lib/compatible/compatible.php',
		'\Otr\Sale\Compatible\FetchAdapter' => 'lib/compatible/compatible.php',
		'\Otr\Sale\Compatible\Test'         => 'lib/compatible/compatible.php',

		"\\Otr\\Sale\\OrderUserProperties" => "lib/userprops.php",

		"\\Otr\\Sale\\BasketPropertiesCollection" => "lib/basketproperties.php",
		"\\Otr\\Sale\\BasketPropertyItem" => "lib/basketpropertiesitem.php",

		"\\Otr\\Sale\\Tax" => "lib/tax.php",

		"\\Otr\\Sale\\Internals\\OrderTable" => "lib/internals/order.php",
		"\\Otr\\Sale\\OrderTable" => "lib/internals/order_old.php",

		"\\Otr\\Sale\\Internals\\BasketTable" => "lib/internals/basket.php",
		"\\Otr\\Sale\\BasketTable" => "lib/internals/basket_old.php",

		"\\Otr\\Sale\\Internals\\ShipmentTable" => "lib/internals/shipment.php",
		"\\Otr\\Sale\\Internals\\ShipmentItemTable" => "lib/internals/shipmentitem.php",

		"\\Otr\\Sale\\Internals\\PaySystemServiceTable" => "lib/internals/paysystemservice.php",
		"\\Otr\\Sale\\Internals\\PaymentTable" => "lib/internals/payment.php",

		"\\Otr\\Sale\\Internals\\PaySystemTable" => "lib/internals/paysystem.php",
		"\\Otr\\Sale\\PaySystemTable" => "lib/internals/paysystem_old.php",


		"\\Otr\\Sale\\Internals\\ShipmentItemStoreTable" => "lib/internals/shipmentitemstore.php",
		"\\Otr\\Sale\\Internals\\ShipmentExtraService" => "lib/internals/shipmentextraservice.php",

		"\\Otr\\Sale\\Internals\\OrderUserPropertiesTable" => "lib/internals/userprops.php",

		"\\Otr\\Sale\\Internals\\CollectableEntity" => "lib/internals/collectableentity.php",

		"\\Otr\\Sale\\Provider" => "lib/provider.php",
		"\\Otr\\Sale\\ProviderBase" => "lib/providerbase.php",
		"\\Otr\\Sale\\OrderHistory" => "lib/orderhistory.php",

		"\\Otr\\Sale\\Internals\\ProviderBasketCollection" => "lib/providerbasketcollection.php",
		"\\Otr\\Sale\\Internals\\ProviderBasketItem" => "lib/providerbasketitem.php",
		"\\Otr\\Sale\\Internals\\BasketPropertyTable" => "lib/internals/basketproperties.php",
		"\\Otr\\Sale\\Internals\\CompanyTable" => "lib/internals/company.php",
		"\\Otr\\Sale\\Internals\\CompanyGroupTable" => "lib/internals/companygroup.php",
		"\\Otr\\Sale\\Internals\\CompanyResponsibleGroupTable" => "lib/internals/companyresponsiblegroup.php",

		"\\Otr\\Sale\\Internals\\PersonTypeTable" => "lib/internals/persontype.php",
		"\\Otr\\Sale\\PersonTypeTable" => "lib/internals/persontype_old.php",
		"\\Otr\\Sale\\Internals\\PersonTypeSiteTable" => "lib/internals/persontypesite.php",

		"\\Otr\\Sale\\Internals\\Pool" => "lib/internals/pool.php",
		"\\Otr\\Sale\\Internals\\UserBudgetPool" => "lib/internals/userbudgetpool.php",
		"\\Otr\\Sale\\Internals\\EventsPool" => "lib/internals/eventspool.php",
		"\\Otr\\Sale\\Internals\\Events" => "lib/internals/events.php",

		"\\Otr\\Sale\\PriceMaths" => "lib/pricemaths.php",
		"\\Otr\\Sale\\BasketComponentHelper" => "lib/basketcomponenthelper.php",
		"\\Otr\\Sale\\Registry" => "lib/registry.php",

		"IPaymentOrder" => "lib/internals/paymentinterface.php",
		"IShipmentOrder" => "lib/internals/shipmentinterface.php",
		"IEntityMarker" => "lib/internals/entitymarkerinterface.php",

		//archive
		"\\Otr\\Sale\\Internals\\OrderArchiveTable" => "lib/internals/orderarchive.php",
		"\\Otr\\Sale\\Internals\\BasketArchiveTable" => "lib/internals/basketarchive.php",
		"\\Otr\\Sale\\Archive\\Manager" => "lib/archive/manager.php",
		"\\Otr\\Sale\\Archive\\Recovery\\Base" => "lib/archive/recovery/base.php",
		"\\Otr\\Sale\\Archive\\Recovery\\Scheme" => "lib/archive/recovery/scheme.php",
		"\\Otr\\Sale\\Archive\\Recovery\\Version1" => "lib/archive/recovery/version1.php",


		"Otr\\Sale\\Tax\\RateTable" => "lib/tax/rate.php",

		////////////////////////////
		// new location 2.0
		////////////////////////////

		// data entities
		"Otr\\Sale\\Location\\LocationTable" => "lib/location/location.php",
		"Otr\\Sale\\Location\\Tree" => "lib/location/tree.php",
		"Otr\\Sale\\Location\\TypeTable" => "lib/location/type.php",
		"Otr\\Sale\\Location\\GroupTable" => "lib/location/group.php",
		"Otr\\Sale\\Location\\ExternalTable" => "lib/location/external.php",
		"Otr\\Sale\\Location\\ExternalServiceTable" => "lib/location/externalservice.php",

		// search
		"Otr\\Sale\\Location\\Search\\Finder" => "lib/location/search/finder.php",
		"Otr\\Sale\\Location\\Search\\WordTable" => "lib/location/search/word.php",
		"Otr\\Sale\\Location\\Search\\ChainTable" => "lib/location/search/chain.php",
		"Otr\\Sale\\Location\\Search\\SiteLinkTable" => "lib/location/search/sitelink.php",

		// lang entities
		"Otr\\Sale\\Location\\Name\\NameEntity" => "lib/location/name/nameentity.php",
		"Otr\\Sale\\Location\\Name\\LocationTable" => "lib/location/name/location.php",
		"Otr\\Sale\\Location\\Name\\TypeTable" => "lib/location/name/type.php",
		"Otr\\Sale\\Location\\Name\\GroupTable" => "lib/location/name/group.php",

		// connector from locations to other entities
		"Otr\\Sale\\Location\\Connector" => "lib/location/connector.php",

		// link entities
		"Otr\\Sale\\Location\\GroupLocationTable" => "lib/location/grouplocation.php",
		"Otr\\Sale\\Location\\SiteLocationTable" => "lib/location/sitelocation.php",
		"Otr\\Sale\\Location\\DefaultSiteTable" => "lib/location/defaultsite.php",

		// db util
		"Otr\\Sale\\Location\\DB\\CommonHelper" => "lib/location/db/commonhelper.php",
		"Otr\\Sale\\Location\\DB\\Helper" => "lib/location/db/".ToLower($DBType)."/helper.php",
		"Otr\\Sale\\Location\\DB\\BlockInserter" => "lib/location/db/blockinserter.php",

		// admin logic
		"Otr\\Sale\\Location\\Admin\\Helper" => "lib/location/admin/helper.php",
		"Otr\\Sale\\Location\\Admin\\NameHelper" => "lib/location/admin/namehelper.php",
		"Otr\\Sale\\Location\\Admin\\LocationHelper" => "lib/location/admin/locationhelper.php",
		"Otr\\Sale\\Location\\Admin\\TypeHelper" => "lib/location/admin/typehelper.php",
		"Otr\\Sale\\Location\\Admin\\GroupHelper" => "lib/location/admin/grouphelper.php",
		"Otr\\Sale\\Location\\Admin\\DefaultSiteHelper" => "lib/location/admin/defaultsitehelper.php",
		"Otr\\Sale\\Location\\Admin\\SiteLocationHelper" => "lib/location/admin/sitelocationhelper.php",
		"Otr\\Sale\\Location\\Admin\\ExternalServiceHelper" => "lib/location/admin/externalservicehelper.php",
		"Otr\\Sale\\Location\\Admin\\SearchHelper" => "lib/location/admin/searchhelper.php",


		// util
		"Otr\\Sale\\Location\\Util\\Process" => "lib/location/util/process.php",
		"Otr\\Sale\\Location\\Util\\CSVReader" => "lib/location/util/csvreader.php",
		"Otr\\Sale\\Location\\Util\\Assert" => "lib/location/util/assert.php",

		// processes for step-by-step actions
		"Otr\\Sale\\Location\\Import\\ImportProcess" => "lib/location/import/importprocess.php",
		"Otr\\Sale\\Location\\Search\\ReindexProcess" => "lib/location/search/reindexprocess.php",

		// exceptions
		"\\Otr\\Sale\\Location\\Tree\\NodeNotFoundException" => "lib/location/tree/exception.php",
		"\\Otr\\Sale\\Location\\Tree\\NodeIncorrectException" => "lib/location/tree/exception.php",

		// old
		"CSaleProxyAdminResult" => "general/proxyadminresult.php", // for admin
		"CSaleProxyResult" => "general/proxyresult.php", // for public
		// other
		"Otr\\Sale\\Location\\Migration\\CUpdaterLocationPro" => "lib/location/migration/migrate.php", // class of migrations

		////////////////////////////
		// linked entities
		////////////////////////////

		"Otr\\Sale\\Delivery\\DeliveryLocationTable" => "lib/delivery/deliverylocation.php",
		"Otr\\Sale\\Tax\\RateLocationTable" => "lib/tax/ratelocation.php",
		////////////////////////////

		"CSaleBasketFilter" => "general/sale_cond.php",
		"CSaleCondCtrl" => "general/sale_cond.php",
		"CSaleCondCtrlComplex" => "general/sale_cond.php",
		"CSaleCondCtrlGroup" => "general/sale_cond.php",
		"CSaleCondCtrlBasketGroup" => "general/sale_cond.php",
		"CSaleCondCtrlBasketFields" => "general/sale_cond.php",
		"CSaleCondCtrlBasketProps" => "general/sale_cond.php",
		"CSaleCondCtrlOrderFields" => "general/sale_cond.php",
		"CSaleCondCtrlCommon" => "general/sale_cond.php",
		"CSaleCondTree" => "general/sale_cond.php",
		"CSaleCondCtrlPastOrder" => "general/sale_cond.php",
		"CSaleCondCumulativeCtrl" => "general/sale_cond.php",
		"CSaleActionCtrl" => "general/sale_act.php",
		"CSaleActionCtrlGroup" => "general/sale_act.php",
		"CSaleActionCtrlAction" => "general/sale_act.php",
		"CSaleDiscountActionApply" => "general/sale_act.php",
		"CSaleActionCtrlDelivery" => "general/sale_act.php",
		"CSaleActionGift" => "general/sale_act.php",
		"CSaleActionGiftCtrlGroup" => "general/sale_act.php",
		"CSaleActionCtrlBasketGroup" => "general/sale_act.php",
		"CSaleActionCtrlSubGroup" => "general/sale_act.php",
		"CSaleActionCondCtrlBasketFields" => "general/sale_act.php",
		"CSaleActionTree" => "general/sale_act.php",
		"CSaleDiscountConvert" => "general/discount_convert.php",

		"CSalePdf" => "general/pdf.php",
		"CSaleYMHandler" => "general/ym_handler.php",
		"CSaleYMLocation" => "general/ym_location.php",

		"Otr\\Sale\\Delivery\\CalculationResult" => "lib/delivery/calculationresult.php",
		"Otr\\Sale\\Delivery\\Services\\Table" => "lib/delivery/services/table.php",
		"Otr\\Sale\\Delivery\\Restrictions\\Table" => "lib/delivery/restrictions/table.php",
		"Otr\\Sale\\Delivery\\Services\\Manager" => "lib/delivery/services/manager.php",
		"Otr\\Sale\\Delivery\\Restrictions\\Base" => "lib/delivery/restrictions/base.php",
		"Otr\\Sale\\Delivery\\Restrictions\\Manager" => "lib/delivery/restrictions/manager.php",
		"Otr\\Sale\\Delivery\\Services\\Base" => "lib/delivery/services/base.php",
		"Otr\\Sale\\Delivery\\Menu" => "lib/delivery/menu.php",
		"Otr\\Sale\\Delivery\\Services\\ObjectPool" => "lib/delivery/services/objectpool.php",

		'\Otr\Sale\TradingPlatformTable' => 'lib/internals/tradingplatform.php',
		'\Otr\Sale\TradingPlatform\Ebay\Policy' => 'lib/tradingplatform/ebay/policy.php',
		'\Otr\Sale\TradingPlatform\Helper' => 'lib/tradingplatform/helper.php',
		'\Otr\Sale\TradingPlatform\YMarket\YandexMarket' => 'lib/tradingplatform/ymarket/yandexmarket.php',
		'\Otr\Sale\TradingPlatform\Platform' => 'lib/tradingplatform/platform.php',
		'\Otr\Sale\TradingPlatform\Logger' => 'lib/tradingplatform/logger.php',

		'Otr\Sale\Internals\ShipmentExtraServiceTable' => 'lib/internals/shipmentextraservice.php',
		'Otr\Sale\Delivery\ExtraServices\Manager' => 'lib/delivery/extra_services/manager.php',
		'Otr\Sale\Delivery\ExtraServices\Base' => 'lib/delivery/extra_services/base.php',
		'Otr\Sale\Delivery\ExtraServices\Table' => 'lib/delivery/extra_services/table.php',
		'Otr\Sale\Delivery\Tracking\Manager' => 'lib/delivery/tracking/manager.php',
		'Otr\Sale\Delivery\Tracking\Table' => 'lib/delivery/tracking/table.php',
		'Otr\Sale\Delivery\ExternalLocationMap' => 'lib/delivery/externallocationmap.php',

		'Otr\Sale\Internals\ServiceRestrictionTable' => 'lib/internals/servicerestriction.php',
		'Otr\Sale\Services\Base\RestrictionManager' => 'lib/services/base/restrictionmanager.php',

		'\Otr\Sale\Compatible\DiscountCompatibility' => 'lib/compatible/discountcompatibility.php',
		'\Otr\Sale\Discount\Gift\Collection' => 'lib/discount/gift/collection.php',
		'\Otr\Sale\Discount\Gift\Gift' => 'lib/discount/gift/gift.php',
		'\Otr\Sale\Discount\Gift\Manager' => 'lib/discount/gift/manager.php',
		'\Otr\Sale\Discount\Gift\RelatedDataTable' => 'lib/discount/gift/relateddata.php',
		'\Otr\Sale\Discount\Actions' => 'lib/discount/actions.php',
		'\Otr\Sale\Internals\DiscountTable' => 'lib/internals/discount.php',
		'\Otr\Sale\Internals\DiscountCouponTable' => 'lib/internals/discountcoupon.php',
		'\Otr\Sale\Internals\DiscountEntitiesTable' => 'lib/internals/discountentities.php',
		'\Otr\Sale\Internals\DiscountGroupTable' => 'lib/internals/discountgroup.php',
		'\Otr\Sale\Internals\DiscountModuleTable' => 'lib/internals/discountmodule.php',
		'\Otr\sale\Internals\OrderDiscountTable' => 'lib/internals/orderdiscount.php',
		'\Otr\Sale\Internals\OrderDiscountDataTable' => 'lib/internals/orderdiscount.php',
		'\Otr\sale\Internals\OrderCouponsTable' => 'lib/internals/orderdiscount.php',
		'\Otr\sale\Internals\OrderModulesTable' => 'lib/internals/orderdiscount.php',
		'\Otr\sale\Internals\OrderRoundTable' => 'lib/internals/orderround.php',
		'\Otr\sale\Internals\OrderRulesTable' => 'lib/internals/orderdiscount.php',
		'\Otr\Sale\Internals\OrderRulesDescrTable' => 'lib/internals/orderdiscount.php',
		'\Otr\Sale\Internals\AccountNumberGenerator' => 'lib/internals/accountnumber.php',
		'\Otr\Sale\Discount' => 'lib/discount.php',
		'\Otr\Sale\DiscountCouponsManager' => 'lib/discountcoupon.php',
		'\Otr\Sale\OrderDiscountManager' => 'lib/orderdiscount.php',

		'\Otr\Sale\Services\Base\RestClient' => 'lib/services/base/restclient.php',
		'\Otr\Sale\PaySystem\Service' => 'lib/paysystem/service.php',
		'\Otr\Sale\PaySystem\Manager' => 'lib/paysystem/manager.php',
		'\Otr\Sale\PaySystem\BaseServiceHandler' => 'lib/paysystem/baseservicehandler.php',
		'\Otr\Sale\PaySystem\ServiceHandler' => 'lib/paysystem/servicehandler.php',
		'\Otr\Sale\PaySystem\IRefund' => 'lib/paysystem/irefund.php',
		'\Otr\Sale\PaySystem\IRequested' => 'lib/paysystem/irequested.php',
		'\Otr\Sale\PaySystem\IRefundExtended' => 'lib/paysystem/irefundextended.php',
		'\Otr\Sale\PaySystem\Cert' => 'lib/paysystem/cert.php',
		'\Otr\Sale\PaySystem\IPayable' => 'lib/paysystem/ipayable.php',
		'\Otr\Sale\PaySystem\ICheckable' => 'lib/paysystem/icheckable.php',
		'\Otr\Sale\PaySystem\IPrePayable' => 'lib/paysystem/iprepayable.php',
		'\Otr\Sale\PaySystem\CompatibilityHandler' => 'lib/paysystem/compatibilityhandler.php',
		'\Otr\Sale\PaySystem\IHold' => 'lib/paysystem/ihold.php',
		'\Otr\Sale\Internals\PaymentLogTable' => 'lib/internals/paymentlog.php',
		'\Otr\Sale\Services\PaySystem\Restrictions\Manager' => 'lib/services/paysystem/restrictions/manager.php',
		'\Otr\Sale\Services\Base\Restriction' => 'lib/services/base/restriction.php',
		'\Otr\Sale\Services\Base\RestrictionManager' => 'lib/services/base/restrictionmanager.php',
		'\Otr\sale\Internals\YandexSettingsTable' => 'lib/internals/yandexsettings.php',

		'\Otr\Sale\Services\Company\Manager' => 'lib/services/company/manager.php',*/

		'\Otr\Sale\Services\Base\Restriction' => 'lib/services/base/restriction.php',
		'\Otr\Sale\Services\Base\RestrictionManager' => 'lib/services/base/restrictionmanager.php',
		'Otr\Sale\Result' => 'lib/result.php',
		'\Otr\Sale\Cashbox\Internals\Pool' => 'lib/cashbox/internals/pool.php',
		'\Otr\Sale\Cashbox\Internals\CashboxTable' => 'lib/cashbox/internals/cashbox.php',
		'\Otr\Sale\Cashbox\Internals\CashboxCheckTable' => 'lib/cashbox/internals/cashboxcheck.php',
		'\Otr\Sale\Cashbox\Internals\Check2CashboxTable' => 'lib/cashbox/internals/check2cashbox.php',
		'\Otr\Sale\Cashbox\Internals\CashboxZReportTable' => 'lib/cashbox/internals/cashboxzreport.php',
		'\Otr\Sale\Cashbox\Internals\CashboxErrLogTable' => 'lib/cashbox/internals/cashboxerrlog.php',
		'\Otr\Sale\Cashbox\Cashbox' => 'lib/cashbox/cashbox.php',
		'\Otr\Sale\Cashbox\SellCheck' => 'lib/cashbox/sellcheck.php',
		'\Otr\Sale\Cashbox\SellReturnCashCheck' => 'lib/cashbox/sellreturncashcheck.php',
		'\Otr\Sale\Cashbox\SellReturnCheck' => 'lib/cashbox/sellreturncheck.php',
		'\Otr\Sale\Cashbox\CashboxAtolFarm' => 'lib/cashbox/cashboxatolfarm.php',
		'\Otr\Sale\Cashbox\Check' => 'lib/cashbox/check.php',
		'\Otr\Sale\Cashbox\Manager' => 'lib/cashbox/manager.php',
		'\Otr\Sale\Cashbox\CheckManager' => 'lib/cashbox/checkmanager.php',
		'\Otr\Sale\Cashbox\IPrintImmediately' => 'lib/cashbox/iprintimmediately.php',
		'\Otr\Sale\Cashbox\Restrictions\Manager' => 'lib/cashbox/restrictions/manager.php',
		'Otr\Sale\Cashbox\ICheckable'=>'lib/cashbox/icheckable.php',
		'\Otr\Sale\Cashbox\AdvancePaymentCheck' => 'lib/cashbox/advancepaymentcheck.php',
		'\Otr\Sale\Cashbox\AdvanceReturnCashCheck' => 'lib/cashbox/advancereturncashcheck.php',
		'Otr\Sale\Cashbox\AdvanceReturnCheck'=>'lib/cashbox/advancereturncheck.php',

		/*'\Otr\Sale\Notify' => 'lib/notify.php',

		'CAdminSaleList' => 'general/admin_lib.php',
		'\Otr\Sale\Helpers\Admin\SkuProps' => 'lib/helpers/admin/skuprops.php',
		'\Otr\Sale\Helpers\Admin\Product' => 'lib/helpers/admin/product.php',
		'\Otr\Sale\Helpers\Order' => 'lib/helpers/order.php',
		'\Otr\Sale\Location\Comparator\Replacement' => 'lib/location/comparator/ru/replacement.php',
		'\Otr\Sale\Location\Comparator\TmpTable' => 'lib/location/comparator/tmptable.php',
		'\Otr\Sale\Location\Comparator' => 'lib/location/comparator.php',
		'\Otr\Sale\Location\Comparator\MapResult' => 'lib/location/comparator/mapresult.php',
		'\Otr\Sale\Location\Comparator\Mapper' => 'lib/location/comparator/mapper.php',

		'\Otr\Sale\Exchange\EntityCollisionType' => '/lib/exchange/entitycollisiontype.php',
		'\Otr\Sale\Exchange\EntityType' => '/lib/exchange/entitytype.php',
		'\Otr\Sale\Exchange\OneC\ImportCollision' => '/lib/exchange/onec/importcollision.php',
		'\Otr\Sale\Exchange\OneC\CollisionOrder' => '/lib/exchange/onec/importcollision.php',
		'\Otr\Sale\Exchange\OneC\CollisionShipment' => '/lib/exchange/onec/importcollision.php',
		'\Otr\Sale\Exchange\OneC\CollisionPayment' => '/lib/exchange/onec/importcollision.php',
		'\Otr\Sale\Exchange\OneC\CollisionProfile' => '/lib/exchange/onec/importcollision.php',
		'\Otr\Sale\Exchange\OneC\DocumentImportFactory'=> '/lib/exchange/onec/documentimportfactory.php',
		'\Otr\Sale\Exchange\OneC\DocumentImport'=> '/lib/exchange/onec/documentimport.php',
		'\Otr\Sale\Exchange\OneC\OrderDocument'=> '/lib/exchange/onec/orderdocument.php',
		'\Otr\Sale\Exchange\OneC\PaymentDocument'=> '/lib/exchange/onec/paymentdocument.php',
		'\Otr\Sale\Exchange\OneC\PaymentCashDocument'=> '/lib/exchange/onec/paymentdocument.php',
		'\Otr\Sale\Exchange\OneC\PaymentCashLessDocument'=> '/lib/exchange/onec/paymentdocument.php',
		'\Otr\Sale\Exchange\OneC\PaymentCardDocument'=> '/lib/exchange/onec/paymentdocument.php',
		'\Otr\Sale\Exchange\OneC\ShipmentDocument'=> '/lib/exchange/onec/shipmentdocument.php',
		'\Otr\Sale\Exchange\OneC\UserProfileDocument'=> '/lib/exchange/onec/userprofiledocument.php',
		'\Otr\Sale\Exchange\OneC\Converter'=> '/lib/exchange/onec/converter.php',
		'\Otr\Sale\Exchange\OneC\ConverterDocumentOrder' => '/lib/exchange/onec/converterdocument.php',
		'\Otr\Sale\Exchange\OneC\ConverterDocumentShipment' => '/lib/exchange/onec/converterdocument.php',
		'\Otr\Sale\Exchange\OneC\ConverterDocumentPayment' => '/lib/exchange/onec/converterdocument.php',
		'\Otr\Sale\Exchange\OneC\ConverterDocumentProfile' => '/lib/exchange/onec/converterdocument.php',
		'\Otr\Sale\Exchange\OneC\ImportCriterionBase' => '/lib/exchange/onec/importcriterion.php',
		'\Otr\Sale\Exchange\OneC\ImportCriterionOneCCml2' => '/lib/exchange/onec/importcriterion.php',
		'\Otr\Sale\Exchange\OneC\CriterionOrder' => '/lib/exchange/onec/importcriterion.php',
		'\Otr\Sale\Exchange\OneC\CriterionShipment' => '/lib/exchange/onec/importcriterion.php',
		'\Otr\Sale\Exchange\OneC\CriterionPayment' => '/lib/exchange/onec/importcriterion.php',
		'\Otr\Sale\Exchange\OneC\CriterionProfile' => '/lib/exchange/onec/importcriterion.php',
		'\Otr\Sale\Exchange\OneC\ImportSettings' => '/lib/exchange/onec/importsettings.php',
		'\Otr\Sale\Exchange\Entity\EntityImportLoader'=> '/lib/exchange/entity/entityimportloader.php',
		'\Otr\Sale\Exchange\Entity\OrderImportLoader'=> '/lib/exchange/entity/entityimportloader.php',
		'\Otr\Sale\Exchange\Entity\PaymentImportLoader'=> '/lib/exchange/entity/entityimportloader.php',
		'\Otr\Sale\Exchange\Entity\ShipmentImportLoader'=> '/lib/exchange/entity/entityimportloader.php',
		'\Otr\Sale\Exchange\Entity\UserProfileImportLoader'=> '/lib/exchange/entity/entityimportloader.php',
		'\Otr\Sale\Exchange\EntityImportFactory'=> '/lib/exchange/entity/entityimportfactory.php',
		'\Otr\Sale\Exchange\ImportBase'=> '/lib/exchange/importbase.php',
		'\Otr\Sale\Exchange\Entity\EntityImport'=> '/lib/exchange/entity/entityimport.php',
		'\Otr\Sale\Exchange\Entity\OrderImport'=> '/lib/exchange/entity/orderimport.php',
		'\Otr\Sale\Exchange\Entity\PaymentImport'=> '/lib/exchange/entity/paymentimport.php',
		'\Otr\Sale\Exchange\Entity\PaymentCashLessImport'=> '/lib/exchange/entity/paymentimport.php',
		'\Otr\Sale\Exchange\Entity\PaymentCardImport'=> '/lib/exchange/entity/paymentimport.php',
		'\Otr\Sale\Exchange\Entity\PaymentCashImport'=> '/lib/exchange/entity/paymentimport.php',
		'\Otr\Sale\Exchange\Entity\ShipmentImport'=> '/lib/exchange/entity/shipmentimport.php',
		'\Otr\Sale\Exchange\Entity\UserImportBase'=> '/lib/exchange/entity/userimportbase.php',
		'\Otr\Sale\Exchange\Entity\UserProfileImport'=> '/lib/exchange/entity/userprofileimport.php',
		'\Otr\Sale\Exchange\ImportPattern'=> '/lib/exchange/importpattern.php',
		'\Otr\Sale\Exchange\ImportOneCPackage'=> '/lib/exchange/importonecpackage.php',

		'\Otr\Sale\Location\GeoIp' => '/lib/location/geoip.php'*/
	)
);

/*class_alias('Otr\Sale\TradingPlatform\YMarket\YandexMarket', 'Bitrix\Sale\TradingPlatform\YandexMarket');*/

/*$psConverted = \Bitrix\Main\Config\Option::get('main', '~sale_paysystem_converted');
if ($psConverted == '')
{
	CAdminNotify::Add(
		array(
			"MESSAGE" => GetMessage("SALE_PAYSYSTEM_CONVERT_ERROR", array('#LANG#' => LANGUAGE_ID)),
			"TAG" => "SALE_PAYSYSTEM_CONVERT_ERROR",
			"MODULE_ID" => "otr.sale",
			"ENABLE_CLOSE" => "Y",
			"PUBLIC_SECTION" => "N"
		)
	);
}*/

/*function GetBasketListSimple($bSkipFUserInit = true)
{
	$fUserID = (int)CSaleBasket::GetBasketUserID($bSkipFUserInit);
	if ($fUserID > 0)
		return CSaleBasket::GetList(
			array("NAME" => "ASC"),
			array("FUSER_ID" => $fUserID, "LID" => SITE_ID, "ORDER_ID" => "NULL")
		);
	else
		return False;
}

function GetBasketList($bSkipFUserInit = true)
{
	$fUserID = (int)CSaleBasket::GetBasketUserID($bSkipFUserInit);
	$arRes = array();
	if ($fUserID > 0)
	{
		$basketID = array();
		$db_res = CSaleBasket::GetList(
			array(),
			array("FUSER_ID" => $fUserID, "LID" => SITE_ID, "ORDER_ID" => false),
			false,
			false,
			array('ID', 'CALLBACK_FUNC', 'PRODUCT_PROVIDER_CLASS', 'MODULE', 'PRODUCT_ID', 'QUANTITY', 'NOTES')
		);
		while ($res = $db_res->Fetch())
		{
			$res['CALLBACK_FUNC'] = (string)$res['CALLBACK_FUNC'];
			$res['PRODUCT_PROVIDER_CLASS'] = (string)$res['PRODUCT_PROVIDER_CLASS'];
			if ($res['CALLBACK_FUNC'] != '' || $res['PRODUCT_PROVIDER_CLASS'] != '')
				CSaleBasket::UpdatePrice($res["ID"], $res["CALLBACK_FUNC"], $res["MODULE"], $res["PRODUCT_ID"], $res["QUANTITY"], 'N', $res["PRODUCT_PROVIDER_CLASS"], $res['NOTES']);
			$basketID[] = $res['ID'];
		}
		unset($res, $db_res);
		if (!empty($basketID))
		{
			$basketIterator = CSaleBasket::GetList(
				array('NAME' => 'ASC'),
				array('ID' => $basketID)
			);
			while ($basket = $basketIterator->GetNext())
				$arRes[] = $basket;
			unset($basket, $basketIterator);
		}
		unset($basketID);
	}
	return $arRes;
}*/

/*function SaleFormatCurrency($fSum, $strCurrency, $OnlyValue = false, $withoutFormat = false)
{
	if ($withoutFormat === true)
	{
		if ($fSum === '')
			return '';

		$currencyFormat = CCurrencyLang::GetFormatDescription($strCurrency);
		if ($currencyFormat === false)
		{
			$currencyFormat = CCurrencyLang::GetDefaultValues();
		}

		$intDecimals = $currencyFormat['DECIMALS'];
		if (round($fSum, $currencyFormat["DECIMALS"]) == round($fSum, 0))
			$intDecimals = 0;

		return number_format($fSum, $intDecimals, '.','');
	}

	return CCurrencyLang::CurrencyFormat($fSum, $strCurrency, !($OnlyValue === true));
}*/

/*function AutoPayOrders($ORDER_ID)
{
	$ORDER_ID = (int)$ORDER_ID;
	if ($ORDER_ID <= 0)
		return false;

	$arOrder = CSaleOrder::GetByID($ORDER_ID);
	if (!$arOrder)
		return false;
	if ($arOrder["PS_STATUS"] != "Y")
		return false;
	if ($arOrder["PAYED"] != "N")
		return false;

	if ($arOrder["CURRENCY"] == $arOrder["PS_CURRENCY"]
		&& DoubleVal($arOrder["PRICE"]) == DoubleVal($arOrder["PS_SUM"]))
	{
		if (CSaleOrder::PayOrder($arOrder["ID"], "Y", true, false))
			return true;
	}

	return false;
}

function CurrencyModuleUnInstallSales()
{
	$GLOBALS["APPLICATION"]->ThrowException(GetMessage("SALE_INCLUDE_CURRENCY"), "SALE_DEPENDES_CURRENCY");
	return false;
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/ru/include.php"))
	include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/ru/include.php");

function PayUserAccountDeliveryOrderCallbacks($productID, $userID, $bPaid, $orderID, $quantity = 1)
{
	global $DB;

	$productID = IntVal($productID);
	$userID = IntVal($userID);
	$bPaid = ($bPaid ? True : False);
	$orderID = IntVal($orderID);

	if ($userID <= 0)
		return False;

	if ($orderID <= 0)
		return False;

	if (!($arOrder = CSaleOrder::GetByID($orderID)))
		return False;

	$baseLangCurrency = CSaleLang::GetLangCurrency($arOrder["LID"]);
	$arAmount = unserialize(COption::GetOptionString("otr.sale", "pay_amount", 'a:4:{i:1;a:2:{s:6:"AMOUNT";s:2:"10";s:8:"CURRENCY";s:3:"EUR";}i:2;a:2:{s:6:"AMOUNT";s:2:"20";s:8:"CURRENCY";s:3:"EUR";}i:3;a:2:{s:6:"AMOUNT";s:2:"30";s:8:"CURRENCY";s:3:"EUR";}i:4;a:2:{s:6:"AMOUNT";s:2:"40";s:8:"CURRENCY";s:3:"EUR";}}'));
	if (!array_key_exists($productID, $arAmount))
		return False;

	$currentPrice = $arAmount[$productID]["AMOUNT"] * $quantity;
	$currentCurrency = $arAmount[$productID]["CURRENCY"];
	if ($arAmount[$productID]["CURRENCY"] != $baseLangCurrency)
	{
		$currentPrice = CCurrencyRates::ConvertCurrency($arAmount[$productID]["AMOUNT"], $arAmount[$productID]["CURRENCY"], $baseLangCurrency) * $quantity;
		$currentCurrency = $baseLangCurrency;
	}

	if (!CSaleUserAccount::UpdateAccount($userID, ($bPaid ? $currentPrice : -$currentPrice), $currentCurrency, "MANUAL", $orderID, "Payment to user account"))
		return False;

	return True;
}*/

/*
* Formats user name. Used everywhere in 'otr.sale' module
*
*/
/*function GetFormatedUserNames($userId, $bEnableId = true, $createEditLink = true)
{
	static $formattedUsersName = array();
	static $siteNameFormat = '';

	$result = (!is_array($userId)) ? '' : array();
	$newUsers = array();

	if (is_array($userId))
	{
		foreach ($userId as $id)
		{
			if (!isset($formattedUsersName[$id]))
				$newUsers[] = $id;
		}
	}
	else if(!isset($formattedUsersName[$userId]))
	{
		$newUsers[] = $userId;
	}

	if (count($newUsers) > 0)
	{
		$resUsers = \Bitrix\Main\UserTable::getList(
			array(
				'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'LOGIN', 'EMAIL'),
				'filter' => array('ID' => $newUsers)
			)
		);
		while ($arUser = $resUsers->Fetch())
		{
			if (strlen($siteNameFormat) == 0)
				$siteNameFormat = CSite::GetNameFormat(false);
			$formattedUsersName[$arUser['ID']] = CUser::FormatName($siteNameFormat, $arUser, true, true);
		}
	}

	if (is_array($userId))
	{
		foreach ($userId as $uId)
		{
			$formatted = '';
			if ($bEnableId)
				$formatted = '[<a href="/bitrix/admin/user_edit.php?ID='.$uId.'&lang='.LANGUAGE_ID.'">'.$uId.'</a>] ';

			if (CBXFeatures::IsFeatureEnabled('SaleAccounts') && !$createEditLink)
				$formatted .= '<a href="/bitrix/admin/sale_buyers_profile.php?USER_ID='.$uId.'&lang='.LANGUAGE_ID.'">';
			else
				$formatted .= '<a href="/bitrix/admin/user_edit.php?ID='.$uId.'&lang='.LANGUAGE_ID.'">';
			$formatted .= $formattedUsersName[$uId];

			$formatted .= '</a>';

			$result[$uId] = $formatted;
		}
	}
	else
	{
		if ($bEnableId)
			$result .= '[<a href="/bitrix/admin/user_edit.php?ID='.$userId.'&lang='.LANGUAGE_ID.'">'.$userId.'</a>] ';

		if (CBXFeatures::IsFeatureEnabled('SaleAccounts') && !$createEditLink)
			$result .= '<a href="/bitrix/admin/sale_buyers_profile.php?USER_ID='.$userId.'&lang='.LANGUAGE_ID.'">';
		else
			$result .= '<a href="/bitrix/admin/user_edit.php?ID='.$userId.'&lang='.LANGUAGE_ID.'">';

		$result .= $formattedUsersName[$userId];

		$result .= '</a>';
	}

	return $result;
}

/*
 * Updates basket item arrays with information about measures from catalog
 * Basically adds MEASURE_TEXT field with the measure name to each basket item array
 *
 * @param array $arBasketItems - array of basket items' arrays
 * @return array|bool
 */
/*function getMeasuress($arBasketItems)
{
	static $measures = array();
	$newMeasure = array();
	if (Loader::includeModule('catalog'))
	{
		$arDefaultMeasure = CCatalogMeasure::getDefaultMeasure(true, true);
		$arElementId = array();
		$basketLinks = array();
		foreach ($arBasketItems as $keyBasket => $arItem)
		{
			if (isset($arItem['MEASURE_NAME']) && strlen($arItem['MEASURE_NAME']) > 0)
			{
				$measureText = $arItem['MEASURE_NAME'];
				$measureCode = intval($arItem['MEASURE_CODE']);
			}
			else
			{
				$productID = (int)$arItem["PRODUCT_ID"];
				if (!isset($basketLinks[$productID]))
					$basketLinks[$productID] = array();
				$basketLinks[$productID][] = $keyBasket;
				$arElementId[] = $productID;

				$measureText = $arDefaultMeasure['~SYMBOL_RUS'];
				$measureCode = 0;
			}

			$arBasketItems[$keyBasket]['MEASURE_TEXT'] = $measureText;
			$arBasketItems[$keyBasket]['MEASURE'] = $measureCode;
		}
		unset($productID, $keyBasket, $arItem);

		if (!empty($arElementId))
		{
			$arBasket2Measure = array();
			$dbres = CCatalogProduct::GetList(
				array(),
				array("ID" => $arElementId),
				false,
				false,
				array("ID", "MEASURE")
			);
			while ($arRes = $dbres->Fetch())
			{
				$arRes['ID'] = (int)$arRes['ID'];
				$arRes['MEASURE'] = (int)$arRes['MEASURE'];
				if ($arRes['MEASURE'] <= 0)
					continue;
				if (!isset($arBasket2Measure[$arRes['MEASURE']]))
					$arBasket2Measure[$arRes['MEASURE']] = array();
				$arBasket2Measure[$arRes['MEASURE']][] = $arRes['ID'];

				if (!isset($measures[$arRes['MEASURE']]) && !in_array($arRes['MEASURE'], $newMeasure))
					$newMeasure[] = $arRes['MEASURE'];
			}
			unset($arRes, $dbres);

			if (!empty($newMeasure))
			{
				$dbMeasure = CCatalogMeasure::GetList(
					array(),
					array("ID" => array_values($newMeasure)),
					false,
					false,
					array('ID', 'SYMBOL_RUS', 'CODE')
				);
				while ($arMeasure = $dbMeasure->Fetch())
					$measures[$arMeasure['ID']] = $arMeasure;
			}

			foreach ($arBasket2Measure as $measureId => $productIds)
			{
				if (!isset($measures[$measureId]))
					continue;
				foreach ($productIds as $productId)
				{
					if (isset($basketLinks[$productId]) && !empty($basketLinks[$productId]))
					{
						foreach ($basketLinks[$productId] as $keyBasket)
						{
							$arBasketItems[$keyBasket]['MEASURE_TEXT'] = $measures[$measureId]['SYMBOL_RUS'];
							$arBasketItems[$keyBasket]['MEASURE'] = $measures[$measureId]['ID'];
						}
					}
				}
			}
		}
	}
	return $arBasketItems;
}*/

/*
 * Updates basket items' arrays with information about ratio from catalog
 * Basically adds MEASURE_RATIO field with the ratio coefficient to each basket item array
 *
 * @param array $arBasketItems - array of basket items' arrays
 * @return mixed
 */
/*function getRatios($arBasketItems)
{
	if (Loader::includeModule('catalog'))
	{
		static $cacheRatio = array();
		$map = array();
		$arElementId = array();
		foreach ($arBasketItems as $key => $arItem)
		{
			if (
				(isset($arBasketItems[$key]['MEASURE_RATIO_VALUE']) && (float)$arBasketItems[$key]['MEASURE_RATIO_VALUE'] > 0)
				&& (isset($arBasketItems[$key]['MEASURE_RATIO_ID']) && (int)$arBasketItems[$key]['MEASURE_RATIO_ID'] > 0)
			)
				continue;

			$hash = md5((!empty($arItem['PRODUCT_PROVIDER_CLASS']) ? $arItem['PRODUCT_PROVIDER_CLASS']: "")."|".(!empty($arItem['MODULE']) ? $arItem['MODULE']: "")."|".$arItem["PRODUCT_ID"]);

			if (isset($cacheRatio[$hash]))
			{
				$arBasketItems[$key]["MEASURE_RATIO"] = $cacheRatio[$hash]['RATIO']; // old key
				$arBasketItems[$key]["MEASURE_RATIO_ID"] = $cacheRatio[$hash]["ID"];
				$arBasketItems[$key]["MEASURE_RATIO_VALUE"] = $cacheRatio[$hash]["RATIO"];
			}
			else
			{
				$arElementId[$arItem["PRODUCT_ID"]] = $arItem["PRODUCT_ID"];
			}

			if (!isset($map[$arItem["PRODUCT_ID"]]))
			{
				$map[$arItem["PRODUCT_ID"]] = array();
			}

			$map[$arItem["PRODUCT_ID"]][] = $key;
		}

		if (!empty($arElementId))
		{
			$dbRatio = \Otr\Catalog\MeasureRatioTable::getList(array(
				'select' => array('*'),
				'filter' => array('@PRODUCT_ID' => $arElementId, '=IS_DEFAULT' => 'Y')
			));
			while ($arRatio = $dbRatio->fetch())
			{
				if (empty($map[$arRatio["PRODUCT_ID"]]))
					continue;

				foreach ($map[$arRatio["PRODUCT_ID"]] as $key)
				{
					$arBasketItems[$key]["MEASURE_RATIO"] = $arRatio["RATIO"]; // old key
					$arBasketItems[$key]["MEASURE_RATIO_ID"] = $arRatio["ID"];
					$arBasketItems[$key]["MEASURE_RATIO_VALUE"] = $arRatio["RATIO"];

					$itemData = $arBasketItems[$key];

					$hash = md5((!empty($itemData['PRODUCT_PROVIDER_CLASS']) ? $itemData['PRODUCT_PROVIDER_CLASS']: "")."|".(!empty($itemData['MODULE']) ? $itemData['MODULE']: "")."|".$itemData["PRODUCT_ID"]);

					$cacheRatio[$hash] = $arRatio;
				}
				unset($key);
			}
			unset($arRatio, $dbRatio);
		}
		unset($arElementId, $map);
	}
	return $arBasketItems;
}*/

/*
 * Creates an array of iblock properties for the elements with certain IDs
 *
 * @param array $arElementId - array of element id
 * @param array $arSelect - properties to select
 * @return array - array of properties' values in the form of array("ELEMENT_ID" => array of props)
 */
/*function getProductPropss($arElementId, $arSelect)
{
	if (!Loader::includeModule("iblock"))
		return array();

	if (empty($arElementId))
		return array();

	$arSelect = array_filter($arSelect, 'checkProductPropCode');
	foreach (array_keys($arSelect) as $index)
	{
		if (substr($arSelect[$index], 0, 9) === 'PROPERTY_')
		{
			if (substr($arSelect[$index], -6) === '_VALUE')
				$arSelect[$index] = substr($arSelect[$index], 0, -6);
		}
	}
	unset($index);

	$arProductData = array();
	$arElementData = array();
	$res = CIBlockElement::GetList(
		array(),
		array("=ID" => array_unique($arElementId)),
		false,
		false,
		array("ID", "IBLOCK_ID")
	);
	while ($arElement = $res->Fetch())
		$arElementData[$arElement["IBLOCK_ID"]][] = $arElement["ID"]; // two getlists are used to support 1 and 2 type of iblock properties

	foreach ($arElementData as $iblockId => $arElemId) // todo: possible performance bottleneck
	{
		$res = CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => $iblockId, "=ID" => $arElemId),
			false,
			false,
			$arSelect
		);
		while ($arElement = $res->GetNext())
		{
			$id = $arElement["ID"];
			foreach ($arElement as $key => $value)
			{
				if (!isset($arProductData[$id]))
					$arProductData[$id] = array();

				if (isset($arProductData[$id][$key])
					&& !is_array($arProductData[$id][$key])
					&& !in_array($value, explode(", ", $arProductData[$id][$key]))
				) // if we have multiple property value
				{
					$arProductData[$id][$key] .= ", ".$value;
				}
				elseif (empty($arProductData[$id][$key]))
				{
					$arProductData[$id][$key] = $value;
				}
			}
		}
	}

	return $arProductData;
}*/

/*function checkProductPropCodes($selectItem)
{
	return ($selectItem !== null && $selectItem !== '' && $selectItem !== 'PROPERTY_');
}

function updateBasketOffersPropss($oldProps, $newProps)
{
	if (!is_array($oldProps) || !is_array($newProps))
		return false;

	$result = array();
	if (empty($newProps))
		return $oldProps;
	if (empty($oldProps))
		return $newProps;
	foreach ($oldProps as &$oldValue)
	{
		$found = false;
		$key = false;
		$propId = (isset($oldValue['CODE']) ? (string)$oldValue['CODE'] : '').':'.$oldValue['NAME'];
		foreach ($newProps as $newKey => $newValue)
		{
			$newId = (isset($newValue['CODE']) ? (string)$newValue['CODE'] : '').':'.$newValue['NAME'];
			if ($newId == $propId)
			{
				$key = $newKey;
				$found = true;
				break;
			}
		}
		if ($found)
		{
			$oldValue['VALUE'] = $newProps[$key]['VALUE'];
			unset($newProps[$key]);
		}
		$result[] = $oldValue;
	}
	unset($oldValue);
	if (!empty($newProps))
	{
		foreach ($newProps as &$newValue)
		{
			$result[] = $newValue;
		}
		unset($newValue);
	}
	return $result;*/
//}
