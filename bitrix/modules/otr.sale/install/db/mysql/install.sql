
create table if not exists b_tszh_kkm_model (
	ID INT(11) NOT NULL AUTO_INCREMENT,
	NAME varchar(255) NOT NULL,
	SETTINGS text NULL,
	PRIMARY KEY (ID)
);

create table if not exists b_tszh_cashbox (
	ID INT(11) NOT NULL AUTO_INCREMENT,
	NAME varchar(255) NOT NULL,
	HANDLER varchar(255) NOT NULL,
	DATE_CREATE datetime NOT NULL,
	DATE_LAST_CHECK datetime NULL,
	SORT int default 100,
	ACTIVE char(1) not null default 'Y',
	USE_OFFLINE char(1) not null default 'N',
	ENABLED char(1) not null default 'N',
	KKM_ID INT(11) NULL,
	OFD varchar(255) NULL,
	OFD_SETTINGS text NULL,
	NUMBER_KKM varchar(64) NULL,
	SETTINGS text NULL,
	EMAIL varchar(255) NOT NULL,
	PRIMARY KEY (ID)
);



create table if not exists b_tszh_cashbox_err_log (
	ID INT(11) NOT NULL AUTO_INCREMENT,
	CASHBOX_ID INT(11) NULL,
	DATE_INSERT datetime NOT NULL,
	MESSAGE TEXT,
  PRIMARY KEY (ID)
);

create table if not exists b_tszh_cashbox_check (
	ID INT(11) NOT NULL AUTO_INCREMENT,
	CASHBOX_ID INT(11) NULL,
	EXTERNAL_UUID varchar(100) NULL,
	PAYMENT_ID INT(11) NULL,
	SHIPMENT_ID INT(11) NULL,
	CNT_FAIL_PRINT INT default 0,
	ORDER_ID INT(11) NULL,
	DATE_CREATE datetime NOT NULL,
	DATE_PRINT_START datetime NULL,
	DATE_PRINT_END datetime NULL,
	SUM decimal(18, 4) NULL,
	CURRENCY char(3) NULL,
	STATUS char(1) not null default 'N',
	TYPE varchar(255) not null,
	ENTITY_REGISTRY_TYPE varchar(255) not null,
	LINK_PARAMS text NULL,
	PRIMARY KEY (ID),
	INDEX IX_SALE_CHECK_ORDER_ID (ORDER_ID),
	INDEX IX_SALE_CHECK_PAYMENT_ID (PAYMENT_ID),
	INDEX IX_SALE_CHECK_SHIPMENT_ID (SHIPMENT_ID),
	INDEX IX_SALE_CHECK_STATUS (STATUS)
);

create table if not exists b_tszh_check2cashbox(
	ID INT(11) NOT NULL AUTO_INCREMENT,
	CHECK_ID INT(11) NOT NULL,
	CASHBOX_ID INT(11) NOT NULL,
	PRIMARY KEY (ID),
	UNIQUE IX_SALE_CHECK2CB_UNI(CHECK_ID, CASHBOX_ID)
);

create table if not exists b_tszh_cashbox_z_report (
	ID INT(11) NOT NULL AUTO_INCREMENT,
	CASHBOX_ID INT(11) NOT NULL,
	DATE_CREATE datetime NOT NULL,
	DATE_PRINT_START datetime NULL,
	DATE_PRINT_END datetime NULL,
	CASH_SUM decimal(18, 4) NULL,
	CASHLESS_SUM decimal(18, 4) NULL,
	CUMULATIVE_SUM decimal(18, 4) NULL,
	RETURNED_SUM decimal(18, 4) NULL,
	STATUS char(1) not null default 'N',
	CNT_FAIL_PRINT INT default 0,
	LINK_PARAMS text NULL,
	CURRENCY char(3) NULL,
  PRIMARY KEY (ID),
	INDEX IX_SALE_Z_REPORT_CASHBOX_ID (CASHBOX_ID)
);

create table if not exists b_tszh_cashbox_connect (
	HASH VARCHAR(100) NOT NULL,
	ACTIVE char(1) not null default 'Y',
	DATE_CREATE datetime NOT NULL,
	PRIMARY KEY (HASH)
);