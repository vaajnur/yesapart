<?
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang."/lang/", "/install.php"));



Class otr_sale extends CModule
{
	var $MODULE_ID = "otr.sale";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";

	function otr_sale()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else
		{
			//укажите собственные значения
			$this->MODULE_VERSION = 1.0;
			$this->MODULE_VERSION_DATE = "2018-02-05 09:00:00";
			$this->MODULE_NAME = GetMessage("SALE_INSTALL_NAME");
			$this->MODULE_DESCRIPTION = GetMessage("SALE_INSTALL_DESCRIPTION");
		}

		$this->MODULE_NAME = GetMessage("SALE_INSTALL_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("SALE_INSTALL_DESCRIPTION");


		$this->PARTNER_NAME = GetMessage("SALE_INSTALL_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("SALE_INSTALL_PARTNER_URL");
	}

	function DoInstall()
	{
		global $APPLICATION, $step;

		$this->InstallFiles();
		if($this->InstallDB())
			$this->InstallEvents();
		$GLOBALS["errors"] = $this->errors;


	}

	function DoUninstall()
	{
		global $APPLICATION, $step;

		$this->UnInstallFiles();
		if($_REQUEST["saveemails"] != "Y")
			$this->UnInstallEvents();

		$this->UnInstallDB(array(
			"savedata" => $_REQUEST["savedata"],
		));

		$GLOBALS["errors"] = $this->errors;

	}

	function GetModuleRightList()
	{
		$arr = array(
			"reference_id" => array("D",/* "R",*/ "P", "U", "W"),
			"reference" => array(
				"[D] ".GetMessage("SINS_PERM_D"),
				//"[R] ".GetMessage("SINS_PERM_R"),
				"[P] ".GetMessage("SINS_PERM_P"),
				"[U] ".GetMessage("SINS_PERM_U"),
				"[W] ".GetMessage("SINS_PERM_W")
			)
		);
		return $arr;
	}

	function InstallDB()
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;

		$clearInstall = false;

		if(!$DB->Query("SELECT 'x' FROM b_sale_basket", true))
		{
			$clearInstall = true;
			$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/install/db/".$DBType."/install.sql");
		}
		if($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("", $this->errors));
			return false;
		}

		ModuleManager::registerModule('otr.sale');







		return true;
	}

	function UnInstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;



		ModuleManager::unRegisterModule('otr.sale');

		return true;
	}

	function InstallEvents()
	{
		global $DB;
		include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/install/events.php");
		return true;
	}

	function UnInstallEvents()
	{
		global $DB;

		$statusMes = Array();
		$dbStatus = $DB->Query("SELECT * FROM b_sale_status", true);

		if($dbStatus)
		{
			while($arStatus = $dbStatus->Fetch())
			{
				$statusMes[] = "SALE_STATUS_CHANGED_".$arStatus["ID"];
			}
		}

		$statusMes[] = "SALE_NEW_ORDER";
		$statusMes[] = "SALE_ORDER_CANCEL";
		$statusMes[] = "SALE_ORDER_PAID";
		$statusMes[] = "SALE_ORDER_DELIVERY";
		$statusMes[] = "SALE_RECURRING_CANCEL";
		$statusMes[] = "SALE_STATUS_CHANGED";
		$statusMes[] = "SALE_ORDER_REMIND_PAYMENT";
		$statusMes[] = "SALE_NEW_ORDER_RECURRING";
		$statusMes[] = "SALE_ORDER_TRACKING_NUMBER";
		$statusMes[] = "SALE_SUBSCRIBE_PRODUCT";
		$statusMes[] = "SALE_CHECK_PRINT";
		$statusMes[] = "SALE_ORDER_SHIPMENT_STATUS_CHANGED";

		$eventType = new CEventType;
		$eventM = new CEventMessage;
		foreach($statusMes as $v)
		{
			$eventType->Delete($v);
			$dbEvent = CEventMessage::GetList($b="ID", $order="ASC", Array("EVENT_NAME" => $v));
			while($arEvent = $dbEvent->Fetch())
			{
				$eventM->Delete($arEvent["ID"]);
			}
		}

		return true;
	}

	function InstallFiles()
	{
		if($_ENV["COMPUTERNAME"]!='BX')
		{
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true, true);
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/install/panel", $_SERVER["DOCUMENT_ROOT"]."/bitrix/panel", true, true);
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/install/images",  $_SERVER["DOCUMENT_ROOT"]."/bitrix/images/sale", true, true);
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/install/themes", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes", true, true);

			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/install/tools", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools", true, true);
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/install/wizards", $_SERVER["DOCUMENT_ROOT"]."/bitrix/wizards", true, true);
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/install/js", $_SERVER["DOCUMENT_ROOT"]."/bitrix/js", true, true);

			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/install/css", $_SERVER["DOCUMENT_ROOT"]."/bitrix/css", true, true);
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/install/fonts", $_SERVER["DOCUMENT_ROOT"]."/bitrix/fonts", true, true);
		}
		return true;
	}

	function UnInstallFiles()
	{
		if ($_ENV["COMPUTERNAME"]!='BX')
		{
			DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
			DeleteDirFilesEx("/bitrix/js/otr.sale/");//javascript
			DeleteDirFilesEx("/bitrix/css/otr.sale/");//javascript
			DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/install/themes/.default/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default");//css
			DeleteDirFilesEx("/bitrix/themes/.default/icons/otr.sale/");//icons
			DeleteDirFilesEx("/bitrix/images/otr.sale/");//images
			DeleteDirFilesEx("/bitrix/panel/otr.sale/");
			DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/install/tools/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools");//tools

		}

		return true;
	}
	/*
		function OnGetTableSchema()
		{
			return array(
				'otr.sale' => array(
					'b_sale_discount' => array(
						'ID' => array(
							'b_sale_discount_coupon' => 'DISCOUNT_ID',
							'b_sale_discount_group' => 'DISCOUNT_ID',
							'b_sale_discount_module' => 'DISCOUNT_ID',
							'b_sale_discount_entities' => 'DISCOUNT_ID',
							'b_sale_order_discount' => 'DISCOUNT_ID',
						),
					),
					'b_sale_order_discount' => array(
						'ID' => array(
							'b_sale_order_coupons' => 'ORDER_DISCOUNT_ID',
							'b_sale_order_modules' => 'ORDER_DISCOUNT_ID',
							'b_sale_order_rules' => 'ORDER_DISCOUNT_ID',
							'b_sale_order_rules_descr' => 'ORDER_DISCOUNT_ID',
						),
					),
					'b_sale_order' => array(
						'ID' => array(
							'b_sale_order_coupons' => 'ORDER_ID',
							'b_sale_order_rules' => 'ORDER_ID',
							'b_sale_order_discount_data' => 'ORDER_ID',
							'b_sale_order_rules_descr' => 'ORDER_ID',
						),
					),
					'b_sale_discount_coupon' => array(
						'ID' => array(
							'b_sale_order_coupons' => 'COUPON_ID',
							'b_sale_order_rules' => 'COUPON_ID',
						),
					),
					'b_sale_order_rules' => array(
						'ID' => array(
							'b_sale_order_rules_descr' => 'RULE_ID',
						),
					),
				),
				'main' => array(
					'b_group' => array(
						'ID' => array(
							'b_sale_discount_group' => 'GROUP_ID',
						)
					),
					'b_user' => array(
						'ID' => array(
							'b_sale_discount' => 'MODIFIED_BY',
							'b_sale_discount^' => 'CREATED_BY',
							'b_sale_discount_coupon' => 'USER_ID',
							'b_sale_discount_coupon^' => 'MODIFIED_BY',
						)
					),
				),
			);
		}*/
}
