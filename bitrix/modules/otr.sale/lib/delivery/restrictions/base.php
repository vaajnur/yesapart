<?php
namespace Otr\Sale\Delivery\Restrictions;

use Otr\Sale\Delivery;
use Otr\Sale\Services;

/**
 * Class Base.
 * Base class for delivery services restrictions.
 * @package Otr\Sale\Delivery
 */
class Base extends Services\Base\Restriction
{

}