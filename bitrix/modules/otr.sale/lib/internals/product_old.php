<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage sale
 * @copyright 2001-2012 Bitrix
 *
 * @ignore
 * @see \Bitrix\Catalog\ProductTable
 */
namespace Otr\Sale;

use Otr\Sale\Internals;

/**
 * @deprecated
 * Class ProductTable
 * @package Otr\Sale
 *
 * @ignore
 * @see \Bitrix\Catalog\ProductTable
 */
class ProductTable extends Internals\ProductTable
{

}