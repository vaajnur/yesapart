<?php
namespace Otr\Sale;

use Otr\Sale\Internals;

/**
 * @deprecated
 * Class TradingPlatformTable
 * @package Otr\Sale
 */
class TradingPlatformTable extends Internals\TradingPlatformTable
{
}