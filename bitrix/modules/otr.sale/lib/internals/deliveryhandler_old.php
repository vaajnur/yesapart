<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage sale
 * @copyright 2001-2012 Bitrix
 */
namespace Otr\Sale;

use Otr\Sale\Internals;

/**
 * @deprecated
 * Class DeliveryHandlerTable
 * @package Otr\Sale
 */
class DeliveryHandlerTable extends Internals\DeliveryHandlerTable
{

}
