<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage sale
 * @copyright 2001-2012 Bitrix
 *
 * @ignore
 * @see \Bitrix\Iblock\SectionElementTable
 */

namespace Otr\Sale;

use Otr\Sale\Internals;

/**
 * @deprecated
 * Class GoodsSectionTable
 * @package Otr\Sale
 *
 * @ignore
 * @see \Bitrix\Iblock\SectionElementTable
 */
class GoodsSectionTable extends Internals\GoodsSectionTable
{

}