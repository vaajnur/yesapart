<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage sale
 * @copyright 2001-2012 Bitrix
 *
 * @ignore
 * @see \Bitrix\Catalog\StoreProductTable
 */
namespace Otr\Sale;

use Otr\Sale\Internals;

/**
 * @deprecated
 * Class StoreProductTable
 * @package Otr\Sale
 */
class StoreProductTable extends Internals\StoreProductTable
{

}