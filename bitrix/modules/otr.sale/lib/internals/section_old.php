<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage sale
 * @copyright 2001-2012 Bitrix
 *
 * @ignore
 * @see \Bitrix\Catalog\SectionTable
 */
namespace Otr\Sale;

use Otr\Sale\Internals;

/**
 * @deprecated
 * Class SectionTable
 * @package Otr\Sale
 *
 * @ignore
 * @see \Bitrix\Catalog\SectionTable
 */
class SectionTable extends Internals\SectionTable
{

}