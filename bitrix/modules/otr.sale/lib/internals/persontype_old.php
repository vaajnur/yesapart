<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage sale
 * @copyright 2001-2012 Bitrix
 */
namespace Otr\Sale;

use Otr\Sale\Internals;

/**
 * @deprecated
 * Class PersonTypeTable
 * @package Otr\Sale
 */
class PersonTypeTable extends Internals\PersonTypeTable
{

}
