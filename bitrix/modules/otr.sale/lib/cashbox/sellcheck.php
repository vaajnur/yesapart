<?php

namespace Otr\Sale\Cashbox;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhpayment/include.php");

use Bitrix\Main;
use Otr\Sale\Payment;
use Otr\Sale\PaySystem;
use Otr\Sale\Shipment;
use Otr\Sale\ShipmentItem;



Main\Localization\Loc::loadMessages(__FILE__);

/**
 * Class SellCheck
 * @package Otr\Sale\Cashbox
 */
class SellCheck extends Check
{
	/**
	 * @return string
	 */
	public static function getType()
	{

		return 'sell';
	}

	/**
	 * @throws Main\NotImplementedException
	 * @return string
	 */
	public static function getCalculatedSign()
	{
		return static::CALCULATED_SIGN_INCOME;
	}

	/**
	 * @return string
	 */
	public static function getName()
	{
		return Main\Localization\Loc::getMessage('SALE_CASHBOX_SELL_NAME');
	}

	/**
	 * @return array
	 */
	public function getDataForCheck()
	{

		$payd = \CTszhPayment::GetList(array("ID" => "DESC"), array("ID" => $this->getField('PAYMENT_ID')), false, false, array());
		while($pay = $payd->GetNext())
		{
			$acc = \CTszhAccount::GetByID($ID = $pay['ACCOUNT_ID']);
			$items = \Citrus\Tszhpayment\PaymentServicesTable::getList(array(
					'filter' => array(
						'PAYMENT_ID' => $pay['ID']
					),
				)

			)->fetchAll();
			$itemsSumm = array();
			foreach ($items as $eachItemSumm)
			{
				$itemsSumm['totals'] += $eachItemSumm['SUMM_PAYED'];

			}



			if($items &&$pay['SUMM']==strval($itemsSumm["totals"])){
				$result_tmp['items'] = array();
				foreach ($items as $eachItem){
					$itemSingle = array(
						'name' => $eachItem['SERVICE_NAME'],
						'base_price' => floatval($eachItem['SUMM_PAYED']),//$product['SUMM'],
						'price' =>floatval($eachItem['SUMM_PAYED']),//$product['SUMM'],
						'sum' => floatval($eachItem['SUMM_PAYED']),//$pay['SUMM'],
						'quantity' => '1',
					);
					$result_tmp[]=$itemSingle;

				}
			}
			else{
				$result_tmp['items'] =
					array(
						'name' => GetMessage('TSZH_PRODUCT_NAME').$acc['XML_ID'],
						'base_price' => floatval($pay['SUMM']),//$product['SUMM'],
						'price' =>floatval($pay['SUMM']),//$product['SUMM'],
						'sum' => floatval($pay['SUMM']),//$pay['SUMM'],
						'quantity' => '1',

					);
			}

			$result = array(
				'type' => static::getType(),
				'unique_id' => $this->getField('ID'),
				'payments' => array('type' => 1, 'value' => floatval($pay['SUMM'])),
				'items' =>$result_tmp,
				'date_create' => new Main\Type\DateTime(),
			);
			$result['total_sum'] = floatval($pay['SUMM']);

			$user = \CUser::GetByID($ID = $acc['USER_ID'])->GetNext();

			$result['client_email'] = ($pay['EMAIL']?$pay['EMAIL']:$user['EMAIL']);
		}

		$order = null;


		if ($entities)
		{
			$entitiesData = $entities;

			foreach ($entitiesData['PAYMENT'] as $payment)
			{
				$result['payments'][] = array(
					'is_cash' => 'N',
					'sum' => $pay['~SUMM']
				);
			}

			foreach ($entitiesData['PAYMENT'] as $product)
			{
				$item = array(
					'name' => "����",//GetMessage('TSZH_PRODUCT_NAME'),
					'base_price' => floatval($pay['~SUMM']),//$product['SUMM'],
					'price' =>floatval($pay['~SUMM']),//$product['SUMM'],
					'sum' => $pay['~SUMM'],//$pay['SUMM'],
					'quantity' => '1',
					//'vat' => $product['VAT']
				);

				if ($product['DISCOUNT'])
				{
					$item['discount'] = array(
						'discount' => 0,//$product['DISCOUNT']['PRICE'],
						'discount_type' => $product['DISCOUNT']['TYPE'],
					);
				}

				$result['items'][] = $item;

			}

			/*foreach ($entitiesData['DELIVERY'] as $delivery)
			{
				$item = array(
					'name' => $delivery['NAME'],
					'base_price' => $delivery['BASE_PRICE'],
					'price' => $delivery['PRICE'],
					'sum' => $delivery['SUM'],
					'quantity' => $delivery['QUANTITY'],
					'vat' => $delivery['VAT']
				);

				if ($delivery['DISCOUNT'])
				{
					$item['discount'] = array(
						'discount' => $delivery['DISCOUNT']['PRICE'],
						'discount_type' => $delivery['DISCOUNT']['TYPE'],
					);
				}

				$result['items'][] = $item;
			}*/

			if (isset($entitiesData['BUYER']))
			{
				if (isset($entitiesData['BUYER']['EMAIL']))
					$result['client_email'] = $entitiesData['BUYER']['EMAIL'];

				if (isset($entitiesData['BUYER']['PHONE']))
					$result['client_email'] = $entitiesData['BUYER']['PHONE'];
			}
			$pays = \CTszhPayment::GetList(array("ID"=>"DESC"), array(), false, array("nTopCount"=>1,), array())->GetNext();
			$result['total_sum'] =floatval($pays['~SUMM']);//$result['items']['price'];//$entitiesData['TOTAL_SUM'];
		}


		return $result;
	}

}