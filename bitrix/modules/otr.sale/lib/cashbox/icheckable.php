<?php

namespace Otr\Sale\Cashbox;

use Otr\Sale\Result;

interface ICheckable
{
	/**
	 * @param Check $check
	 * @return Result
	 */
	public function check(Check $check);
}

