<?php

namespace Otr\Sale\Cashbox;
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhpayment/include.php");
use Bitrix\Main;
use Bitrix\Main\Localization;
use Otr\Sale\Result;
use Bitrix\Catalog;
use Otr\Sale\Cashbox\Internals\CashboxTable;

Localization\Loc::loadMessages(__FILE__);

/**
 * Class CashboxAtolFarm
 * @package Otr\Sale\Cashbox
 */
class CashboxAtolFarmV4 extends CashboxAtolFarm
{
	const OPERATION_CHECK_REGISTRY = 'registry';
	const OPERATION_CHECK_CHECK = 'check';
	const REQUEST_TYPE_GET = 'get';
	const REQUEST_TYPE_POST = 'post';
	const TOKEN_OPTION_NAME = 'atol_access_token';
	const SERVICE_URL = 'https://online.atol.ru/possystem/v4';
	const RESPONSE_HTTP_CODE_401 = 401;
	const RESPONSE_HTTP_CODE_200 = 200;

	/**
	 * @param Check $check
	 * @return array
	 */
	public function buildCheckQuery(Check $check)
	{
		$data = $check->getDataForCheck();

		/** @var Main\Type\DateTime $dateTime */
		//$dateTime = $data['date_create'];
		$checkList = \Otr\Sale\Cashbox\Internals\CashboxCheckTable::getList(
			array(
				'order' => array('ID' => 'DESC'),
				//'select' => array('ID', 'ORDER_ID'),
				'limit' => 1,

			)
		);
		while ($checkcahbox = $checkList->fetchAll())
		{
			$check2cahbox = $checkcahbox;
		}
		$payd = \CTszhPayment::GetList(array("ID" => "DESC"), array("ID" => $check2cahbox['0']["PAYMENT_ID"]), false, false, array())->GetNext();
		$acc = \CTszhAccount::GetByID($ID = $payd['ACCOUNT_ID']);
		$user = \CUser::GetByID($ID = $acc['USER_ID'])->GetNext();
		/** @var Main\Type\DateTime $dateTime */
		$dateTime = $payd["~DATE_UPDATE"];
		//------------------------------------------------------------------
		$serviceEmail = $this->getValueFromSettings('SERVICE', 'EMAIL');
		if (!$serviceEmail)
		{
			$serviceEmail = static::getDefaultServiceEmail();
		}

		$result = array(
			'timestamp' => $dateTime,
			'external_id' => static::buildUuid(static::UUID_TYPE_CHECK, $check2cahbox['0']["ID"]),
			'service' => array(
				'callback_url' => $this->getCallbackUrl(),
			),
			'receipt' => array(
				'client' => array(),
				'company' => array(
					'email' => $serviceEmail,
					'sno' => $this->getValueFromSettings('TAX', 'SNO'),
					'inn' => $this->getValueFromSettings('SERVICE', 'INN'),
					'payment_address' => $this->getValueFromSettings('SERVICE', 'P_ADDRESS'),
				),
				'payments' => array(),
				'items' => array(),
				'total' => (float)$payd['SUMM']
			)
		);

		$email = $user['EMAIL'] ?: '';

		$phone = \NormalizePhone($user['PERSONAL_PHONE']);
		if (is_string($phone))
		{
			if ($phone[0] === '7')
				$phone = substr($phone, 1);
		}
		else
		{
			$phone = '';
		}

		$clientInfo = $this->getValueFromSettings('CLIENT', 'INFO');
		if ($clientInfo === 'PHONE')
		{
			$result['receipt']['client'] = ['phone' => $phone];
		}
		elseif ($clientInfo === 'EMAIL')
		{
			$result['receipt']['client'] = ['email' => $email];
		}
		else
		{
			$result['receipt']['client'] = [
				'email' => $email,
				'phone' => $phone,
			];
		}
		//------------------------------------------------------------------
		$paymentTypeMap = $this->getPaymentTypeMap();

		$result['receipt']['payments'][] = array(
			'type' => $paymentTypeMap["cashless"],//(int)$this->getValueFromSettings('PAYMENT_TYPE', $payment['type']),
			'sum' => (float)$payd['SUMM']
		);



		$checkTypeMap = $this->getCheckTypeMap();
		$paymentObjectMap = $this->getPaymentObjectMap();

		$vat = $this->getValueFromSettings('VAT', $item['vat']);
		if ($vat === null)
			$vat = $this->getValueFromSettings('VAT', 'NOT_VAT');
		$quant = '1';
		$dbService = \Citrus\Tszhpayment\PaymentServicesTable::getList(
			[
				'select' => ['GUID', 'SERVICE_NAME', 'SUMM_PAYED'],
				'filter' => ['PAYMENT_ID' => $payd['ID']],
				'order' => ['ID' => 'asc'],
			]
		);
		$rsService = \Citrus\Tszhpayment\PaymentServicesTable::getList(
			[
				'select' => ['GUID', 'SERVICE_NAME', 'SUMM_PAYED'],
				'filter' => ['PAYMENT_ID' => $payd['ID']],
				'order' => ['ID' => 'asc'],
			]
		);
		$service_list=array();
		while ($services = $rsService->fetch())
		{
			$service_list[] = $services;
		}
		$service_count=count($service_list);

		if($service_count>0){
			while ($service = $dbService->fetch())
			{
				$result['receipt']['items'][] = array(
					'name' => $service['SERVICE_NAME'],//Localization\Loc::getMessage('SALE_CASHBOX_ATOL_FARM_ITEM_NAME') . $acc['XML_ID'],
					'price' => (float)$service['SUMM_PAYED'],
					'sum' => (float)$service['SUMM_PAYED'],
					'quantity' => (float)$quant,
					'payment_method' => $checkTypeMap[$check::getType()],
					'payment_object' => $paymentObjectMap["payment"],
					'vat' => array(
						'type' => $vat
					),
				);
			}
		}
		else
		{
			$result['receipt']['items'][] = array(
				'name' => Localization\Loc::getMessage('SALE_CASHBOX_ATOL_FARM_ITEM_NAME'). $acc['XML_ID'],
				'price' => (float)$payd['SUMM'],
				'sum' => (float)$payd['SUMM'],
				'quantity' => (float)$quant,
				'payment_method' => $checkTypeMap[$check::getType()],
				'payment_object' => $paymentObjectMap["payment"],
				'vat' => array(
					'type' => $vat
				),
			);
		}
		return $result;
	}


	/**
	 * @return array
	 */
	private function getPaymentObjectMap()
	{
		return [
			Check::PAYMENT_OBJECT_COMMODITY => 'commodity',
			Check::PAYMENT_OBJECT_SERVICE => 'service',
			Check::PAYMENT_OBJECT_JOB => 'job',
			Check::PAYMENT_OBJECT_EXCISE => 'excise',
			Check::PAYMENT_OBJECT_PAYMENT => 'payment',
		];
	}

	/**
	 * @return array
	 */
	private function getPaymentTypeMap()
	{
		return array(
			Check::PAYMENT_TYPE_CASH => 4,
			Check::PAYMENT_TYPE_CASHLESS => 1,
			Check::PAYMENT_TYPE_ADVANCE => 2,
			Check::PAYMENT_TYPE_CREDIT => 3,
		);
	}

	/**
	 * @return string
	 */
	private static function getDefaultServiceEmail()
	{
		return Main\Config\Option::get('main', 'email_from');
	}

	/**
	 * @return string
	 */
	public static function getName()
	{
		return Localization\Loc::getMessage('SALE_CASHBOX_ATOL_FARM_V4_TITLE');
	}

	/**
	 * @return array
	 */
	protected function getCheckTypeMap()
	{
		return array(
			SellCheck::getType() => 'full_payment',
			SellReturnCashCheck::getType() => 'full_payment',
			SellReturnCheck::getType() => 'full_payment',
			AdvancePaymentCheck::getType() => 'advance',
			AdvanceReturnCashCheck::getType() => 'advance',
			AdvanceReturnCheck::getType() => 'advance',
			/*PrepaymentCheck::getType() => 'prepayment',
			PrepaymentReturnCheck::getType() => 'prepayment',
			PrepaymentReturnCashCheck::getType() => 'prepayment',
			FullPrepaymentCheck::getType() => 'full_prepayment',
			FullPrepaymentReturnCheck::getType() => 'full_prepayment',
			FullPrepaymentReturnCashCheck::getType() => 'full_prepayment',
			CreditCheck::getType() => 'credit',
			CreditReturnCheck::getType() => 'credit',
			CreditPaymentCheck::getType() => 'credit_payment',*/
		);
	}

	/**
	 * @param $operation
	 * @param $token
	 * @param array $queryData
	 * @return string
	 * @throws Main\SystemException
	 */
	protected function getUrl($operation, $token, array $queryData = array())
	{
		$groupCode = $this->getField('NUMBER_KKM');

		if ($operation === static::OPERATION_CHECK_REGISTRY)
		{
			return static::SERVICE_URL.'/'.$groupCode.'/'.$queryData['CHECK_TYPE'].'?token='.$token;
		}
		elseif ($operation === static::OPERATION_CHECK_CHECK)
		{
			return static::SERVICE_URL.'/'.$groupCode.'/report/'.$queryData['EXTERNAL_UUID'].'?token='.$token;
		}

		throw new Main\SystemException();
	}

	/**
	 * @param int $modelId
	 * @return array
	 */
	public static function getSettings($modelId = 0)
	{
		$settings = parent::getSettings($modelId);
		unset($settings['PAYMENT_TYPE']);

		$settings['SERVICE']['ITEMS']['EMAIL'] = array(
			'TYPE' => 'STRING',
			'LABEL' => Localization\Loc::getMessage('SALE_CASHBOX_ATOL_FARM_SETTINGS_SERVICE_EMAIL_LABEL'),
			'VALUE' => static::getDefaultServiceEmail()
		);

		return $settings;
	}

	/**
	 * @param array $checkData
	 * @return Result
	 */
	protected function validate(array $checkData)
	{
		$result = new Result();

		if ($checkData['receipt']['client']['email'] === '' && $checkData['receipt']['client']['phone'] === '')
		{
			$result->addError(new Main\Error(Localization\Loc::getMessage('SALE_CASHBOX_ATOL_ERR_EMPTY_PHONE_EMAIL')));
		}

		foreach ($checkData['receipt']['items'] as $item)
		{
			if ($item['vat'] === null)
			{
				$result->addError(new Main\Error(Localization\Loc::getMessage('SALE_CASHBOX_ATOL_ERR_EMPTY_TAX')));
				break;
			}
		}

		return $result;
	}

	/**
	 * @return bool
	 */
	public static function isSupportedFFD105()
	{
		return true;
	}
}
