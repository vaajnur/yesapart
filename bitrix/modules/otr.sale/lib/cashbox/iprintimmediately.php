<?php

namespace Otr\Sale\Cashbox;

use Otr\Sale\Result;

interface IPrintImmediately
{
	/**
	 * @param Check $check
	 * @return Result
	 */
	public function printImmediately(Check $check);
}

