<?php

namespace Otr\Sale\Cashbox;

use Bitrix\Main\NotImplementedException;
use Bitrix\Main\Localization\Loc;

use Otr\Sale\Result;

abstract class Ofd
{
	protected $testMode = false;

	/**
	 * @return array
	 */
	public static function getHandlerList()
	{
		return array(
			'\Otr\Sale\Cashbox\FirstOfd' => FirstOfd::getName(),
			'\Otr\Sale\Cashbox\PlatformaOfd' => PlatformaOfd::getName(),
			'\Otr\Sale\Cashbox\YarusOfd' => YarusOfd::getName(),
			'\Otr\Sale\Cashbox\TaxcomOfd' => TaxcomOfd::getName(),
			'\Otr\Sale\Cashbox\OfdruOfd' => OfdruOfd::getName(),
			'\Otr\Sale\Cashbox\TenzorOfd' => TenzorOfd::getName(),
			'\Otr\Sale\Cashbox\ConturOfd' => ConturOfd::getName(),
		);
	}

	/**
	 * @param $handler
	 * @param bool $testMode
	 * @return null
	 */
	public static function create($handler, $testMode = false)
	{
		if (class_exists($handler))
			return new $handler($testMode);

		return null;
	}

	/**
	 * Ofd constructor.
	 * @param $testMode
	 */
	private function __construct($testMode)
	{
		$this->testMode = $testMode;
	}

	/**
	 * @return string
	 */
	protected function getUrl()
	{
		return '';
	}

	/**
	 * @return array
	 */
	protected function getLinkParamsMap()
	{
		return array();
	}

	/**
	 * @param $data
	 * @return string
	 */
	public function generateCheckLink($data)
	{
		$queryParams = array();

		$map = $this->getLinkParamsMap();
		foreach ($map as $queryKey => $checkKey)
		{
			if ($data[$checkKey])
				$queryParams[] = $queryKey.'='.$data[$checkKey];
		}

		if (empty($queryParams))
			return '';

		$url = $this->getUrl();
		return $url.implode('&', $queryParams);
	}

	/**
	 * @throws NotImplementedException
	 * @return string
	 */
	public static function getName()
	{
		throw new NotImplementedException();
	}
	public static function getSettings()
	{
		return array(
			'OFD_MODE' => array(
				'LABEL' => Loc::getMessage('SALE_CASHBOX_OFD_SETTINGS'),
				'ITEMS' => array(
					'IS_TEST' => array(
						'TYPE' => 'Y/N',
						'LABEL' => Loc::getMessage('SALE_CASHBOX_OFD_TEST_MODE'),
						'VALUE' => 'N'
					)
				)
			)
		);
	}

	/**
	 * @param $settings
	 * @return Result
	 */
	public static function validateSettings($settings)
	{
		return new Result();
	}

	/**
	 * @param $name
	 * @param $code
	 * @return mixed
	 */
	public function getValueFromSettings($name, $code = null)
	{
		$map = $this->cashbox->getField('OFD_SETTINGS');
		if (isset($map[$name]))
		{
			if (is_array($map[$name]))
			{
				if (isset($map[$name][$code]))
					return $map[$name][$code];
			}
			else
			{
				return $map[$name];
			}
		}

		return null;
	}

	/**
	 * @return bool
	 */
	protected function isTestMode()
	{
		return $this->getValueFromSettings('OFD_MODE', 'IS_TEST') === 'Y';
	}

}