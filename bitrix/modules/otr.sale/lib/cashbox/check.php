<?php

namespace Otr\Sale\Cashbox;
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhpayment/include.php");

use Bitrix\Main;
use Otr\Sale\Cashbox\Internals\CashboxCheckTable;
use Otr\Sale\Cashbox\Internals\Check2CashboxTable;
use Otr\Sale\Internals\CollectableEntity;

use Otr\Sale\PaymentCollection;


/**
 * Class Check
 * @package Otr\Sale\Cashbox
 */
abstract class Check
{
    const EVENT_ON_CHECK_PREPARE_DATA = 'OnSaleCheckPrepareData';

    const PARAM_FISCAL_DOC_NUMBER = 'fiscal_doc_number';
    const PARAM_FISCAL_DOC_ATTR = 'fiscal_doc_attribute';
    const PARAM_FISCAL_RECEIPT_NUMBER = 'fiscal_receipt_number';
    const PARAM_FN_NUMBER = 'fn_number';
    const PARAM_SHIFT_NUMBER = 'shift_number';
    const PARAM_REG_NUMBER_KKT = 'reg_number_kkt';
    const PARAM_DOC_TIME = 'doc_time';
    const PARAM_DOC_SUM = 'doc_sum';
    const PARAM_CALCULATION_ATTR = 'calculation_attribute';

	const CALCULATED_SIGN_INCOME = 'income';
	const CALCULATED_SIGN_CONSUMPTION = 'consumption';

	const SHIPMENT_TYPE_NONE = '';
	const PAYMENT_TYPE_CASH = 'cash';
	const PAYMENT_TYPE_ADVANCE = 'advance';
	const PAYMENT_TYPE_CASHLESS = 'cashless';
	const PAYMENT_TYPE_CREDIT = 'credit';

	const PAYMENT_OBJECT_COMMODITY = 'commodity';
	const PAYMENT_OBJECT_EXCISE = 'excise';
	const PAYMENT_OBJECT_JOB = 'job';
	const PAYMENT_OBJECT_SERVICE = 'service';
	const PAYMENT_OBJECT_PAYMENT = 'payment';

	const SUPPORTED_ENTITY_TYPE_PAYMENT = 'payment';
	const SUPPORTED_ENTITY_TYPE_SHIPMENT = 'shipment';
	const SUPPORTED_ENTITY_TYPE_ALL = 'all';
	const SUPPORTED_ENTITY_TYPE_NONE = 'none';

    /** @var array $fields */
    private $fields = array();

    /** @var array $cashboxList */
    private $cashboxList = array();

    /** @var CollectableEntity[] $entities */
    private $entities = array();

    /**
     * @throws Main\NotImplementedException
     * @return string
     */
    public static function getType()
    {
        throw new Main\NotImplementedException();
    }

    /**
     * @throws Main\NotImplementedException
     * @return string
     */
    public static function getCalculatedSign()
    {
        throw new Main\NotImplementedException();
    }

    /**
     * @throws Main\NotImplementedException
     * @return string
     */
    public static function getName()
    {
        throw new Main\NotImplementedException();
    }

    /**
     * @param string $handler
     * @return null|Check
     */
    public static function create($handler)
    {
        if (class_exists($handler))
            return new $handler();

        return null;
    }

    /**
     * Check constructor.
     */
    private function __construct() {}

    /**
     * @param $name
     * @return mixed
     */
    public function getField($name)
    {
        return $this->fields[$name];
    }

    /**
     * @param $name
     * @param $value
     */
    public function setField($name, $value)
    {
        $this->fields[$name] = $value;
    }
	public function setEntities( $entities)//array
	{
		$this->entities = $entities;

		$orderId = null;
		$entityRegistryType = null;

		foreach ($this->entities as $entity)
		{
			if ($entity instanceof CTszhPayment)
			{
				$this->fields['PAYMENT_ID'] = $entity->getId();
				$this->fields['SUM'] = $entity->getField('SUMM');
				$this->fields['CURRENCY'] = $entity->getField('CURRENCY');
			}
			/*elseif ($entity instanceof Shipment)
			{
				$this->fields['SHIPMENT_ID'] = $entity->getId();

				if (!$this->fields['CURRENCY'])
					$this->fields['CURRENCY'] = $entity->getParentOrder()->getCurrency();

				if ($this->fields['SUM'] <= 0)
				{
					$this->fields['SUM'] = $entity->getPrice();
					$shipmentItemCollection = $entity->getShipmentItemCollection();

					foreach ($shipmentItemCollection as $item)
					{
						$basketItem = $item->getBasketItem();
						$this->fields['SUM'] += PriceMaths::roundPrecision($item->getQuantity() * $basketItem->getPrice());
					}
				}
			}*/
			/*else
			{
				throw new Main\ArgumentTypeException('entities');
			}*/

			if ($entityRegistryType === null)
			{
				$entityRegistryType = 'ORDER';//$entity->getRegistryType();
			}
			elseif ($entityRegistryType !== 'ORDER'/*$entity->getRegistryType()*/)
			{
				throw new Main\ArgumentTypeException('entities');
			}

			/** @var PaymentCollection|ShipmentCollection $collection */
			//$collection = $entity->getCollection();
			$colOrderId = $entities["ID"];//$collection->getOrder()->getId();

			if ($orderId === null)
			{
				$orderId = $colOrderId;
			}
			elseif ($orderId != $colOrderId)
			{
				throw new Main\ArgumentTypeException('entities');
			}
		}

		$this->fields['ORDER_ID'] = $orderId;
		$this->fields['ENTITY_REGISTRY_TYPE'] = $entityRegistryType;
	}
    /**
     * @param array $cashboxList
     */
    public function setAvailableCashbox(array $cashboxList)
    {
        $this->cashboxList = $cashboxList;
    }

    /**
     * @param CollectableEntity[] $entities
     * @throws Main\ArgumentTypeException
     */


    /*������� ������*/

    public function getPayment()
    {
        $payment = array();//\CTszhPayment::GetByID($ID="ID");
        $cashboxId = null;
        $cashbox = \Otr\Sale\Cashbox\Internals\CashboxTable::getList(
            array(
                'select' => array('ID'),
                'filter' => array(
                    '=ACTIVE' => 'Y',
                )
            )

        );
        while ($cashboxIds = $cashbox->fetchAll()) {
            $cashboxId = $cashboxIds;
        }
        $paymentID = array();
        $payID = \Otr\Sale\Cashbox\Internals\CashboxCheckTable::getList(
            array(
                'select'=> array(
                    "ORDER_ID",
                    "ID"
                ),
                'filter'=>array(
                    //'STATUS'=>'N',
                    'CASHBOX_ID'=>$cashboxId["0"]["ID"],
                ),
                'order'=>array(
                    'ID'=>'DESC',
                ),
                //'limit'=> 10,
            )
        );
        while ($payment = $payID ->fetchAll()){
            $paymentID = $payment;
        }
        foreach ($paymentID as $payIDs) {
            $payd = \CTszhPayment::GetList(array("ID" => "DESC"), array("ID" => $payIDs["ORDER_ID"]), false, false, array());
            while($paym = $payd ->GetNext()){
                $pays[] = $paym;
            }

        }

        while ($paydata = $pays->GetNext())
        {
            $payment['PAYMENT'] = $paydata;
        }
        $this ->fields['ID']=$payment['PAYMENT']['ID'];
        $this ->fields['SUMM']=$payment['PAYMENT']['SUMM'];
        $this ->fields['CURRENCY']=$payment['PAYMENT']['CURRENCY'];
        $orderId = null;
        if($orderId===null) {
            $orderId = $payment['PAYMENT']['ID'];
        }
        if ($this->fields['ID'] > 0)
        {
            if ($this->fields['ID'] > 0)
            {
                $orderId = $this->fields['ID'];
            }
            else
            {
                $dbRes = \CTszhPayment::GetByID($ID=$this->fields['ID']);
                $data = $dbRes;
                $orderId = $data['ID'];
            }


        }

    }

    /**
     * @return Main\Entity\AddResult|Main\Entity\UpdateResult
     */
    public function save()
    {
        if ((int)$this->fields['ID'] > 0)
            return CashboxCheckTable::update($this->fields['ID'], $this->fields);

        $this->fields['TYPE'] = static::getType();
        $this->fields['DATE_CREATE'] = new Main\Type\DateTime();

        $result = CashboxCheckTable::add($this->fields);
        $checkId = $result->getId();
        $this->fields['ID'] = $checkId;
        foreach ($this->cashboxList as $cashbox)
            Check2CashboxTable::add(array('CHECK_ID' => $checkId, 'CASHBOX_ID' => $cashbox['ID']));

        return $result;
    }

    /**
     * @param $cashboxId
     */
    public function linkCashbox($cashboxId)
    {
        $this->fields['CASHBOX_ID'] = $cashboxId;
    }

    /**
     * @param $settings
     */
    public function init($settings)
    {
        $this->fields = $settings;
    }

    /**
     * @return array
     */
   // abstract public function getDataForCheck();
	public function getDataForCheck()
	{
		$result = array(
			'type' => static::getType(),
			'unique_id' => $this->getField('ID'),
			'items' => array(),
			'date_create' => new Main\Type\DateTime()
		);

		$entitiesData = $this->extractData();

		if ($entitiesData)
		{
			if (isset($entitiesData['ORDER']))
			{
				$result['order'] = $entitiesData['ORDER'];
			}

			foreach ($entitiesData['PAYMENTS'] as $payment)
			{
				$result['payments'][] = array(
					'entity' => $payment['ENTITY'],
					'type' => $payment['TYPE'],
					'is_cash' => $payment['IS_CASH'],
					'sum' => $payment['SUM']
				);
			}

			if (isset($entitiesData['PRODUCTS']))
			{
				foreach ($entitiesData['PRODUCTS'] as $product)
				{
					$item = array(
						'entity' => $product['ENTITY'],
						'name' => $product['NAME'],
						'base_price' => $product['BASE_PRICE'],
						'price' => $product['PRICE'],
						'sum' => $product['SUM'],
						'quantity' => $product['QUANTITY'],
						'vat' => $product['VAT'],
						'payment_object' => $product['PAYMENT_OBJECT'],
					);

					if ($product['DISCOUNT'])
					{
						$item['discount'] = array(
							'discount' => $product['DISCOUNT']['PRICE'],
							'discount_type' => $product['DISCOUNT']['TYPE'],
						);
					}

					$result['items'][] = $item;
				}
			}

			if (isset($entitiesData['DELIVERY']))
			{
				foreach ($entitiesData['DELIVERY'] as $delivery)
				{
					$item = array(
						'entity' => $delivery['ENTITY'],
						'name' => $delivery['NAME'],
						'base_price' => $delivery['BASE_PRICE'],
						'price' => $delivery['PRICE'],
						'sum' => $delivery['SUM'],
						'quantity' => $delivery['QUANTITY'],
						'vat' => $delivery['VAT'],
						'payment_object' => $delivery['PAYMENT_OBJECT'],
					);

					if ($delivery['DISCOUNT'])
					{
						$item['discount'] = array(
							'discount' => $delivery['DISCOUNT']['PRICE'],
							'discount_type' => $delivery['DISCOUNT']['TYPE'],
						);
					}

					$result['items'][] = $item;
				}
			}

			if (isset($entitiesData['BUYER']))
			{
				if (isset($entitiesData['BUYER']['EMAIL']))
					$result['client_email'] = $entitiesData['BUYER']['EMAIL'];

				if (isset($entitiesData['BUYER']['PHONE']))
					$result['client_phone'] = $entitiesData['BUYER']['PHONE'];
			}

			$result['total_sum'] = $entitiesData['TOTAL_SUM'];
		}

		return $result;
	}
    /**
     * @param array $entities
     * @return array
     */
    public function extractDataFromEntities()
    {
        static $psList = array();
        $result = array();
        $order = null;
        $totalSum = 0;

        foreach ($entities as $entity)
        {
            if ($order === null)
                $order = \CTszhPayment::GetByID($ID="ID");;

            if ($entity instanceof \CTszhPayment)
            {
                if (!isset($psList))
                    $psList = \CTszhPaySystem::GetByID($ID = $order['PAY_SYSTEM']['ID']);

                $paySystem = $psList;

                $result['PAYMENTS'][] = array(
                    'IS_CASH' => 'N',
                    'SUM' => $order['PAYMENT']['SUMM']
                );

                $totalSum += $order['PAYMENT']['SUMM'];
            }

        }

        if ($order !== null)
        {
            $result['BUYER'] = array();

            $properties = $order;



            $email = $properties['PAYMENT']['USER_EMAIL'];
            if ($email)
                $result['BUYER']['EMAIL'] = $email;
            $phone = $propertie['PAYMENT']['USER_PHONE'];
            if ($phone)
                $result['BUYER']['PHONE'] = $phone;
        }

        $result['TOTAL_SUM'] = $totalSum;

        $event = new Main\Event('otr.sale', static::EVENT_ON_CHECK_PREPARE_DATA, array($result));
        $event->send();

        if ($event->getResults())
        {
            /** @var Main\EventResult $eventResult */
            foreach ($event->getResults() as $eventResult)
            {
                if ($eventResult->getType() !== Main\EventResult::ERROR)
                {
                    $result = $eventResult->getParameters();
                }
            }
        }

        return $result;
    }


}