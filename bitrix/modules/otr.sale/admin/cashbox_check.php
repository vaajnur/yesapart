<?
//namespace Otr\Sale\Cashbox;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
CModule::IncludeModule('citrus.tszhpayment');
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhpayment/include.php");
require_once ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/otr.sale/include.php");


use Bitrix\Main\Localization\Loc;
use Otr\Sale\Cashbox\Internals;
use Bitrix\Main\Page;
use Otr\Sale\Shipment;
use Otr\Sale\Internals\StatusTable;

function RarusFormatCurrency($fSum, $strCurrency, $OnlyValue = false, $withoutFormat = false)
{
	if ($withoutFormat === true)
	{
		if ($fSum === '')
			return '';

		$currencyFormat = CCurrencyLang::GetFormatDescription($strCurrency);
		if ($currencyFormat === false)
		{
			$currencyFormat = CCurrencyLang::GetDefaultValues();
		}

		$intDecimals = $currencyFormat['DECIMALS'];
		if (round($fSum, $currencyFormat["DECIMALS"]) == round($fSum, 0))
			$intDecimals = 0;

		return number_format($fSum, $intDecimals, '.','');
	}

	//return CCurrencyLang::CurrencyFormat($fSum, $strCurrency, !($OnlyValue === true));
}
$saleModulePermissions = $APPLICATION->GetGroupRight("otr.sale");
if ($saleModulePermissions < "W")
	$APPLICATION->AuthForm(GetMessage("SALE_ACCESS_DENIED"));



IncludeModuleLangFile(__FILE__);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/otr.sale/prolog.php");
Page\Asset::getInstance()->addJs("/bitrix/js/otr.sale/cashbox.js");
\Bitrix\Main\Loader::includeModule('otr.sale');

$tableId = "tbl_sale_cashbox_check";
$instance = \Bitrix\Main\Application::getInstance();
$context = $instance->getContext();
$request = $context->getRequest();

$oSort = new CAdminSorting($tableId, "ID", "asc");
$lAdmin = new CAdminList($tableId, $oSort);

$arFilterFields = array(
	'filter_cashbox_id'
);
if (($ids = $lAdmin->GroupAction()) && $saleModulePermissions >= "W")
{
	foreach ($ids as $id)
	{
		if (empty($id))
			continue;

		if ($_REQUEST['action'] === 'delete')
		{
			$check = Internals\CashboxCheckTable::getRowById($id);
			if ($check['STATUS'] == 'E' || $check['STATUS'] == 'N')
			{
				Internals\CashboxCheckTable::delete($id);
			}
			else
			{
				$lAdmin->AddGroupError(Loc::getMessage('SALE_CHECK_DELETE_ERR_INCORRECT_STATUS'), $id);
			}
		}
	}
}

$lAdmin->InitFilter($arFilterFields);

$filter = array();

if (strlen($filter_cashbox_id) > 0 && $filter_cashbox_id != "NOT_REF")
	$filter["CASHBOX_ID"] = trim($filter_cashbox_id);

if (strlen($filter_date_create_from)>0)
{
	$filter[">=DATE_CREATE"] = trim($filter_date_create_from);
}
elseif($set_filter!="Y" && $del_filter != "Y")
{
	$filter_date_create_from_FILTER_PERIOD = 'day';
	$filter_date_create_from_FILTER_DIRECTION = 'current';
	$filter[">=DATE_CREATE"] = new \Bitrix\Main\Type\Date();
}

if (strlen($filter_date_create_to)>0)
{
	if($arDate = ParseDateTime($filter_date_create_to, CSite::GetDateFormat("FULL", SITE_ID)))
	{
		if(strlen($filter_date_create_to) < 11)
		{
			$arDate["HH"] = 23;
			$arDate["MI"] = 59;
			$arDate["SS"] = 59;
		}

		$filter_date_create_to = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID)), mktime($arDate["HH"], $arDate["MI"], $arDate["SS"], $arDate["MM"], $arDate["DD"], $arDate["YYYY"]));
		$filter["<=DATE_CREATE"] = $filter_date_create_to;
	}
	else
	{
		$filter_date_create_to = "";
	}
}

if (strlen($filter_cashbox_id) > 0 && $filter_cashbox_id != "NOT_REF")
	$filter["CASHBOX_ID"] = trim($filter_cashbox_id);


if((int)($filter_order_id_from)>0) $filter[">=ORDER_ID"] = (int)($filter_order_id_from);
if((int)($filter_order_id_to)>0) $filter["<=ORDER_ID"] = (int)($filter_order_id_to);
if((int)($filter_id_from)>0) $filter[">=ID"] = (int)($filter_id_from);
if((int)($filter_id_to)>0) $filter["<=ID"] = (int)($filter_id_to);

if(isset($filter_check_status) && is_array($filter_check_status) && count($filter_check_status) > 0)
{
	$countFilter = count($filter_check_status);
	for ($i = 0; $i < $countFilter; $i++)
	{
		$filter_check_status[$i] = trim($filter_check_status[$i]);
		if(strlen($filter_check_status[$i]) > 0)
			$filter["=STATUS"][] = $filter_check_status[$i];
	}
}

$navyParams = array();

if ($del_filter !== 'Y')
{
	$params = array(
		'filter' => $filter
	);
}

if (isset($by))
{
	$order = isset($order) ? $order : "ASC";
	$params['order'] = array($by => $order);
}
$navyParams = CDBResult::GetNavParams(CAdminResult::GetNavSize($tableId));

if ($navyParams['SHOW_ALL'])
{
	$usePageNavigation = false;
}
else
{
	$navyParams['PAGEN'] = (int)$navyParams['PAGEN'];
	$navyParams['SIZEN'] = (int)$navyParams['SIZEN'];
}



if ($usePageNavigation)
{
	$params['limit'] = $navyParams['SIZEN'];
	$params['offset'] = $navyParams['SIZEN']*($navyParams['PAGEN']-1);
}

$totalPages = 0;

if ($usePageNavigation)
{
	$countQuery = new \Bitrix\Main\Entity\Query(Internals\CashboxCheckTable::getEntity());
	$countQuery->addSelect(new \Bitrix\Main\Entity\ExpressionField('CNT', 'COUNT(1)'));
	$countQuery->setFilter($params['filter']);

	foreach ($params['runtime'] as $key => $field)
		$countQuery->registerRuntimeField($key, clone $field);

	$totalCount = $countQuery->setLimit(null)->setOffset(null)->exec()->fetch();
	unset($countQuery);
	$totalCount = (int)$totalCount['CNT'];

	if ($totalCount > 0)
	{
		$totalPages = ceil($totalCount/$navyParams['SIZEN']);

		if ($navyParams['PAGEN'] > $totalPages)
			$navyParams['PAGEN'] = $totalPages;

		$params['limit'] = $navyParams['SIZEN'];
		$params['offset'] = $navyParams['SIZEN']*($navyParams['PAGEN']-1);
	}
	else
	{
		$navyParams['PAGEN'] = 1;
		$params['limit'] = $navyParams['SIZEN'];
		$params['offset'] = 0;
	}
}

$dbResultList = new CAdminResult(Internals\CashboxCheckTable::getList($params), $tableId);

if ($usePageNavigation)
{
	$dbResultList->NavStart($params['limit'], $navyParams['SHOW_ALL'], $navyParams['PAGEN']);
	$dbResultList->NavRecordCount = $totalCount;
	$dbResultList->NavPageCount = $totalPages;
	$dbResultList->NavPageNomer = $navyParams['PAGEN'];
}
else
{
	$dbResultList->NavStart();
}

$headers = array(
	array("id" => "ID", "content" => GetMessage("SALE_CASHBOX_ID"), "sort" => "ID", "default" => true),
	array("id" => "ORDER_ID", "content" => GetMessage("SALE_CASHBOX_ORDER_ID"), "sort" => "ORDER_ID", "default" => true),
	array("id" => "CASHBOX_ID", "content" => GetMessage("SALE_CASHBOX_CASHBOX_ID"), "sort" => "CASHBOX_ID", "default" => true),
	array("id" => "DATE_CREATE", "content" => GetMessage("SALE_CASHBOX_DATE_CREATE"), "sort" => "DATE_CREATE", "default" => true),
	array("id" => "SUM", "content" => GetMessage("SALE_CASHBOX_SUM"), "sort" => "SUM", "default" => true),
	array("id" => "LINK_PARAMS", "content" => GetMessage("SALE_CASHBOX_LINK"), "default" => true),
	array("id" => "STATUS", "content" => GetMessage("SALE_CASHBOX_STATUS"), "sort" => "STATUS", "default" => true),
	array("id" => "PAYMENT", "content" => GetMessage("SALE_CASHBOX_PAYMENT_DESCR"), "sort" => "PAYMENT_ID", "default" => true),
	//array("id" => "SHIPMENT", "content" => GetMessage("SALE_CASHBOX_SHIPMENT_DESCR"), "sort" => "SHIPMENT_ID", "default" => true),
	array("id" => "PAYMENT_ID", "content" => GetMessage("SALE_CASHBOX_PAYMENT_ID"), "sort" => "PAYMENT_ID", "default" => false),
	array("id" => "SHIPMENT_ID", "content" => GetMessage("SALE_CASHBOX_SHIPMENT_ID"), "sort" => "SHIPMENT_ID", "default" => false),
);

$lAdmin->NavText($dbResultList->GetNavPrint(GetMessage("group_admin_nav")));

$lAdmin->AddHeaders($headers);

$visibleHeaders = $lAdmin->GetVisibleHeaderColumns();
$cashboxList = array();
$dbRes = Internals\CashboxTable::getList();
while ($item = $dbRes->fetch())
	$cashboxList[$item['ID']] = $item;

$tempResult = clone($dbResultList);
$paymentIdList = array();
$shipmentIdList = array();
$shipmentStatuses = array();
$paymentRows = array();
$shipmentRows = array();
while ($check = $tempResult->Fetch())
{
	$paymentIdList[] = $check['PAYMENT_ID'];
	$shipmentIdList[] = $check['SHIPMENT_ID'];
}
$paymentIdList = array_unique($paymentIdList);
$shipmentIdList = array_unique($shipmentIdList);
unset($tempResult);	

$paymentData = CTszhPayment::getList(
	array(
		'select' => array('ID', 'ORDER_ID', 'PAY_SYSTEM_NAME', 'PAYED', 'PS_STATUS', 'SUM', 'CURRENCY'),
		'filter' => array('=ID' => $paymentIdList)
	)
);	

while ($payment = $paymentData->fetch())
{
	$psys = CTszhPaySystem::GetByID($ID = $payment["PAY_SYSTEM_ID"]) ;
	//echo "<pre>"; var_dump($payment); echo "</pre>";
	$linkId = '[<a href="/bitrix/admin/tszh_payments_edit.php?ID='.$payment['ID'].'&lang='.LANGUAGE_ID.'">'.$payment["ID"].'</a>]';
	$paymentRows[$payment['ID']] = $linkId.','.htmlspecialcharsbx($psys["NAME"]).','.
		($payment["PAYED"] == "Y" ? Loc::getMessage("SALE_CHECK_PAYMENTS_PAID") :  Loc::getMessage("SALE_CHECK_PAYMENTS_UNPAID")).", ".
		//(strlen($payment["PS_STATUS"]) > 0 ? Loc::getMessage("SALE_CASHBOX_STATUS").": ".htmlspecialcharsbx($payment["PS_STATUS"]).", " : "").
	                               '<span style="white-space:nowrap;">'.CTszhPayment::FormatCurrency($payment["SUMM"], $payment["CURRENCY"]).'</span>';
}

while ($check = $dbResultList->Fetch())
{
//	$row =& $lAdmin->AddRow($check['ID'], $check, "sale_cashbox_check_edit.php?ID=".$check['ID']."&lang=".LANG, GetMessage("SALE_EDIT_DESCR"));
	$row =& $lAdmin->AddRow($check['ID'], $check, false, GetMessage("SALE_EDIT_DESCR"));

//	$row->AddField("ID", "<a href=\"sale_cashbox_check_edit.php?ID=".$check['ID']."&lang=".LANG."\">".$check['ID']."</a>");
	$row->AddField("ID", $check['ID']);
	$row->AddField("ORDER_ID",  "<a href=\"tszh_payments_edit.php?ID=".(int)$check['ORDER_ID']."&lang=".LANG."\">".(int)$check['ORDER_ID']."</a>");
	$row->AddField("PAYMENT_ID",  "<a href=\"tszh_payments_edit.php?ID=".(int)$check['ORDER_ID']."&lang=".LANG."\">".(int)$check['ORDER_ID']."</a>");
	$row->AddField("PAYMENT",  $paymentRows[(int)$check['PAYMENT_ID']]);
	//$row->AddField("SHIPMENT_ID",  "<a href=\"sale_order_shipment_edit.php?order_id=".(int)$check['ORDER_ID']."&shipment_id=".(int)$check['SHIPMENT_ID']."&lang=".LANG."\">".(int)$check['SHIPMENT_ID']."</a>");
	//$row->AddField("SHIPMENT",  $shipmentRows[(int)$check['SHIPMENT_ID']]);
	$row->AddField("DATE_CREATE", $check['DATE_CREATE']);
	$row->AddField("SUM", CTszhPayment::FormatCurrency($check['SUM'], $check['CURRENCY']));
	$row->AddField("CASHBOX_ID", htmlspecialcharsbx($cashboxList[$check['CASHBOX_ID']]['NAME']));

	$checkLink = '';
	if ($check['CASHBOX_ID'] > 0)
	{
		$cashbox = \Otr\Sale\Cashbox\Manager::getObjectById($check['CASHBOX_ID']);
		//$cashbox = \Bitrix\Sale\Cashbox\Manager::getObjectById($check['CASHBOX_ID']); --- ����������������� ��� �������� �������. ���������������� ������ ����.
		if ($cashbox && is_array($check['LINK_PARAMS']))
		{
			$link = $cashbox->getCheckLink($check['LINK_PARAMS']);
			if ($link)
				$checkLink = '<a href="'.$link.'" target="_blank">'.Loc::getMessage('SALE_CHECK_LOOK').'</a>';
		}
	}
	$row->AddField("LINK_PARAMS", $checkLink);
	$row->AddField("STATUS", Loc::getMessage('SALE_CASHBOX_STATUS_'.$check['STATUS']));
	if ($check['STATUS'] === 'E' || $check['STATUS'] == 'N')
	{
		$arActions = array(
			array(
				"ICON" => "delete",
				"TEXT" => GetMessage("SALE_CHECK_DELETE"),
				"ACTION" => "if(confirm('".Loc::getMessage('SALE_CHECK_DELETE_CONFIRM', array('#CHECK_ID#' => $check['ID']))."')) ".$lAdmin->ActionDoGroup($check["ID"], "delete")
			)
		);
		$row->AddActions($arActions);
	}
}

$lAdmin->AddFooter(
	array(
		array(
			"title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"),
			"value" => $dbResultList->SelectedRowsCount()
		),
		array(
			"counter" => true,
			"title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"),
			"value" => "0"
		),
	)
);

$dbRes = Internals\CashboxTable::getList(array('filter' => array('=ACTIVE' => 'Y', '=ENABLED' => 'Y')));
if ($saleModulePermissions == "W" && $dbRes->fetch())
{
	$aContext = array(
		array(
			"TEXT" => GetMessage("SALE_CASHBOX_ADD_NEW"),
			"TITLE" => GetMessage("SALE_CASHBOX_ADD_NEW"),
			"LINK" => "#",
			"ICON" => "btn_new",
			'ONCLICK' => "BX.Otr.Sale.Cashbox.showCreateCheckWindow()"
		)
	);
	//$lAdmin->AddAdminContextMenu($aContext);
}

$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("SALE_CASHBOX_CHECK_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<form name="find_form" method="GET" action="<?echo $APPLICATION->GetCurPage()?>?">
<?
$oFilter = new CAdminFilter(
	$tableId."_filter",
	array(
		"filter_cashbox_id" => Loc::getMessage("SALE_F_CASHBOX"),
		"filter_cashbox_id" => Loc::getMessage("SALE_CHECK_ID"),
		"filter_check_create" => Loc::getMessage("SALE_CHECK_CREATE"),
		"filter_order_id" => Loc::getMessage("SALE_F_ORDER_ID"),
		"filter_check_status" => Loc::getMessage("SALE_CASHBOX_STATUS"),
	)
);

$oFilter->Begin();
?>
	<tr>
		<td><?echo GetMessage("SALE_F_CASHBOX")?>:</td>
		<td>
			<select name="filter_cashbox_id">
				<option value="NOT_REF">(<?echo GetMessage("SALE_ALL")?>)</option>
				<?
					$dbRes = Internals\CashboxTable::getList();
					while ($item = $dbRes->fetch()): ?>
						<option value="<?=$item['ID']?>"><?= htmlspecialcharsbx($item['NAME']);?></option>
					<?endwhile;?>
			</select>
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("SALE_CHECK_ID");?>:</td>
		<td>
			<script type="text/javascript">
				function filter_id_from_change()
				{
					if(document.find_form.filter_id_to.value.length<=0)
					{
						document.find_form.filter_id_to.value = document.find_form.filter_id_from.value;
					}
				}
			</script>
			<?echo Loc::getMessage("SALE_F_FROM");?>
			<input type="text" name="filter_id_from" onchange="filter_id_from_change()" value="<?echo ((int)($filter_id_from)>0)?(int)($filter_id_from):""?>" size="10">
			<?echo Loc::getMessage("SALE_F_TO");?>
			<input type="text" name="filter_id_to" value="<?echo ((int)($filter_id_to)>0)?(int)($filter_id_to):""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?=GetMessage("SALE_F_CHECK_CREATE");?>:</td>
		<td>
			<?=CalendarPeriod("filter_date_create_from", htmlspecialcharsbx($filter_date_create_from), "filter_date_create_to", htmlspecialcharsbx($filter_date_create_to), "find_form", "Y")?>
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("SALE_F_ORDER_ID");?>:</td>
		<td>
			<script type="text/javascript">
				function filter_order_id_from_change()
				{
					if(document.find_form.filter_order_id_to.value.length<=0)
					{
						document.find_form.filter_order_id_to.value = document.find_form.filter_order_id_from.value;
					}
				}
			</script>
			<?echo Loc::getMessage("SALE_F_FROM");?>
			<input type="text" name="filter_order_id_from" onchange="filter_order_id_from_change()" value="<?echo ((int)($filter_order_id_from)>0)?(int)($filter_order_id_from):""?>" size="10">
			<?echo Loc::getMessage("SALE_F_TO");?>
			<input type="text" name="filter_order_id_to" value="<?echo ((int)($filter_order_id_to)>0)?(int)($filter_order_id_to):""?>" size="10">
		</td>
	</tr>
	<tr>
		<td valign="top"><?echo Loc::getMessage("SALE_CASHBOX_STATUS")?>:</td>
		<td valign="top">
			<select name="filter_check_status[]" multiple size="3">
				<?
					$statusesList = array('N','P','Y', 'E');
					foreach($statusesList as  $statusCode)
					{
						?>
						<option value="<?= htmlspecialcharsbx($statusCode) ?>"<?if(is_array($filter_check_status) && in_array($statusCode, $filter_check_status)) echo " selected"?>>
							<?= Loc::getMessage('SALE_CASHBOX_STATUS_'.$statusCode)?>
						</option>
						<?
					}
				?>
			</select>
		</td>
	</tr>
<?
$oFilter->Buttons(
	array(
		"table_id" => $tableId,
		"url" => $APPLICATION->GetCurPage(),
		"form" => "find_form"
	)
);
$oFilter->End();
?>
</form>
<script language="JavaScript">
	BX.message(
		{
			CASHBOX_CREATE_WINDOW_NOT_SELECT: '<?=Loc::getMessage("CASHBOX_CREATE_WINDOW_NOT_SELECT")?>',
			CASHBOX_CREATE_WINDOW_TITLE: '<?=Loc::getMessage("CASHBOX_CREATE_WINDOW_TITLE")?>'
		}
	);
</script>
<?
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");

?>