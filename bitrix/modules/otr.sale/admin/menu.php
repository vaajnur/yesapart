<?
//if(CModule::IncludeModule("otr.sale")) {
    IncludeModuleLangFile(__FILE__);

// $module_rights = $APPLICATION->GetGroupRight("citrus.tszhpayment");
    $module_rights = $APPLICATION->GetGroupRight("citrus.tszh");

//global $adminMenu;

    if ($module_rights > "D") {


        /* CASHBOX Begin*/

        $aMenu = array(
            "parent_menu" => "global_menu_services",
            "section" => "otr.sale",
            "sort" => 12,
            "text" => GetMessage("SALE_CASHBOX"),
            "title" => GetMessage("SALE_CASHBOX"),
            "icon" => "crm-cashbox-icon",
            "url" => "tszh_cashbox.php?lang=" . LANGUAGE_ID,
            "page_icon" => "citrus_sale_page_icon_crm",
            "items_id" => "menu_citrus_sale_cashbox",
            "items" => Array(


                Array(
                    "text" => GetMessage("SALE_CASHBOX_LIST"),
                    "title" => GetMessage("SALE_CASHBOX_LIST"),
                    "url" => "tszh_cashbox_list.php?lang=" . LANGUAGE_ID,
                    "more_url" => array(
                        "cashbox_edit.php"
                    ),
                ),

                Array(
                    "text" => GetMessage("SALE_CASHBOX_CHECK"),
                    "title" => GetMessage("SALE_CASHBOX_CHECK"),
                    "url" => "tszh_cashbox_check.php?lang=" . LANGUAGE_ID,
                    "more_url" => array(
                        "cashbox_check_edit.php"
                    ),
                ),

                Array(
                    "text" => GetMessage("SALE_CASHBOX_ZREPORT"),
                    "title" => GetMessage("SALE_CASHBOX_ZREPORT"),
                    "url" => "tszh_cashbox_zreport.php?lang=" . LANGUAGE_ID,
                    "more_url" => array(),
                ),
            )
        );
        return $aMenu;
    }
    /* CASHBOX End*/


    return false;
//}