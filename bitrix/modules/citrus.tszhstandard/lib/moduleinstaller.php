<?php

namespace Citrus\Tszh;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Citrus\Tszh\ModuleInstallException;

require_once __DIR__ . '/moduleinstallexception.php';

if (!class_exists('Citrus\Tszh\ModuleInstaller'))
{
	class ModuleInstaller
	{
		private static $strError = '';

		private static function &_GetModuleObject($moduleID)
		{
			if (!class_exists('CModule'))
			{
				global $DB, $DBType, $DBHost, $DBLogin, $DBPassword, $DBName, $DBDebug, $DBDebugToFile, $APPLICATION, $USER, $DBSQLServerType;
				require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include.php');
			}

			$installFile = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $moduleID . '/install/index.php';
			if (!file_exists($installFile))
			{
				return false;
			}
			include_once($installFile);

			$moduleIDTmp = str_replace('.', '_', $moduleID);
			if (!class_exists($moduleIDTmp))
			{
				return false;
			}

			return new $moduleIDTmp;
		}

		public static function LoadAndInstallModule($selectedModule)
		{
			global $APPLICATION;
			require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/update_client_partner.php');

			$module =& self::_GetModuleObject($selectedModule);

			if (!self::isModuleDownloaded($module))
			{
				if (!\CUpdateClientPartner::LoadModuleNoDemand($selectedModule, self::$strError, $bStable = 'Y', LANGUAGE_ID))
				{
					throw new ModuleInstallException(Loc::getMessage('TSZH_ERROR_MODULE_LOAD', array('#ERROR#' => self::$strError)));
				}
			}

			if (!ModuleManager::isModuleInstalled($selectedModule))
			{
				$module =& self::_GetModuleObject($selectedModule);

				if (!self::isModuleDownloaded($module))
				{
					throw new ModuleInstallException(Loc::getMessage('TSZH_ERROR_MODULE_OBJECT'));
				}

				try
				{
					$module->InstallDB();

					if ($ex = $APPLICATION->GetException())
					{
						throw new ModuleInstallException(Loc::getMessage('TSZH_ERROR_MODULE_DB') . $ex->GetString());
					}
				}
				catch (\Exception $e)
				{
					throw new ModuleInstallException(Loc::getMessage('TSZH_ERROR_MODULE_DB') . $e->getMessage());
				}

				try
				{
					$module->InstallEvents();

					if ($ex = $APPLICATION->GetException())
					{
						throw new ModuleInstallException(Loc::getMessage('TSZH_ERROR_MODULE_EVENTS') . $ex->GetString());
					}
				}
				catch (\Exception $e)
				{
					throw new ModuleInstallException(Loc::getMessage('TSZH_ERROR_MODULE_EVENTS') . $e->getMessage());
				}

				try
				{
					$module->InstallFiles();

					if ($ex = $APPLICATION->GetException())
					{
						throw new ModuleInstallException(Loc::getMessage('TSZH_ERROR_MODULE_FILES') . $ex->GetString());
					}
				}
				catch (\Exception $e)
				{
					throw new ModuleInstallException(Loc::getMessage('TSZH_ERROR_MODULE_FILES') . $e->getMessage());
				}
			}
		}

		/**
		 * @param $module
		 *
		 * @return bool
		 */
		protected static function isModuleDownloaded($module)
		{
			return is_object($module);
		}

		public function OnEventLogGetAuditTypesHandler()
		{
			return array(
				'TSZH_MODULE_AUTOINSTALL_SUCCESS' => '[TSZH_MODULE_AUTOINSTALL_SUCCESS] ' . Loc::getMessage('TSZH_AUDIT_TYPE_TSZH_MODULE_AUTOINSTALL_SUCCESS'),
				'TSZH_MODULE_AUTOINSTALL_ERROR' => '[TSZH_MODULE_AUTOINSTALL_ERROR] ' . Loc::getMessage('TSZH_AUDIT_TYPE_TSZH_MODULE_AUTOINSTALL_ERROR'),
			);
		}
	}
}