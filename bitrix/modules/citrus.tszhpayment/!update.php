<?php
$files2delete = array (
  0 => 'install/components/citrus/tszh.payment.do/templates/.default/script.min.js',
  1 => 'install/components/citrus/tszh.payment.do/templates/orchid_default/script.min.js',
);
$removeEmptySubFolders = function($path) use (&$removeEmptySubFolders)
{
	$empty=true;
	foreach (glob($path.DIRECTORY_SEPARATOR."*") as $file)
		$empty &= is_dir($file) && $removeEmptySubFolders($file);
	return $empty && rmdir($path);
};
$doc_root = $_SERVER["DOCUMENT_ROOT"];
$module_path = '/bitrix/modules/citrus.tszhpayment/';
foreach ($files2delete as $file)
{
	if (file_exists($doc_root . $module_path . $file))
		DeleteDirFilesEx($module_path . $file);
	$folder = dirname($doc_root . $module_path . $file);
		if (file_exists($folder) && is_dir($folder))
		$removeEmptySubFolders($folder);
}