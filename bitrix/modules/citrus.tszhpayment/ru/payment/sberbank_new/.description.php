<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?
$psTitle = "Квитанция Сбербанка";
$psDescription = "Квитанция на перевод денег через Сбербанк";

$arPSCorrespondence = array(
		"COMPANY_NAME" => array(
				"NAME" => "Наименование получателя платежа",
				"DESCR" => "",
				"VALUE" => "NAME",
				"TYPE" => "TSZH"
			),
		"INN" => array(
				"NAME" => "ИНН получателя платежа",
				"DESCR" => "",
				"VALUE" => "INN",
				"TYPE" => "TSZH"
			),
		"KPP" => array(
				"NAME" => "КПП получателя платежа",
				"DESCR" => "",
				"VALUE" => "KPP",
				"TYPE" => "TSZH"
			),
		"SETTLEMENT_ACCOUNT" => array(
				"NAME" => "Номер счета получателя платежа",
				"DESCR" => "",
				"VALUE" => "RSCH",
				"TYPE" => "TSZH"
			),
		"BANK_NAME" => array(
				"NAME" => "Наименование банка",
				"DESCR" => "",
				"VALUE" => "BANK",
				"TYPE" => "TSZH"
			),
		"BANK_BIC" => array(
				"NAME" => "Банковские реквизиты",
				"DESCR" => "",
				"VALUE" => "BIK",
				"TYPE" => "TSZH"
			),
		"BANK_COR_ACCOUNT" => array(
				"NAME" => "Номер кор./сч. банка получателя платежа",
				"DESCR" => "",
				"VALUE" => "KSCH",
				"TYPE" => "TSZH"
			),
		"ORDER_ID" => array(
				"NAME" => "Номер платежа",
				"DESCR" => "",
				"VALUE" => "ID",
				"TYPE" => "PAYMENT"
			),
		"DATE_INSERT" => array(
				"NAME" => "Дата создания платежа",
				"DESCR" => "",
				"VALUE" => "DATE_INSERT",
				"TYPE" => "PAYMENT"
			),
		"PAYER_CONTACT_PERSON" => array(
				"NAME" => "Ф.И.О. плательщика",
				"DESCR" => "",
				"VALUE" => "FULL_NAME",
				"TYPE" => "USER"
			),
		"PAYER_ZIP_CODE" => array(
				"NAME" => "Почтовый индекс плательщика",
				"DESCR" => "",
				"VALUE" => "",
				"TYPE" => ""
			),
		"PAYER_COUNTRY" => array(
				"NAME" => "Страна плательщика",
				"DESCR" => "",
				"VALUE" => "",
				"TYPE" => ""
			),
		"PAYER_CITY" => array(
				"NAME" => "Город плательщика",
				"DESCR" => "",
				"VALUE" => "",
				"TYPE" => ""
			),
		"PAYER_ADDRESS_FACT" => array(
				"NAME" => "Адрес плательщика",
				"DESCR" => "",
				"VALUE" => "FULL_ADDRESS",
				"TYPE" => "ACCOUNT"
			),
		"PAYER_ACCOUNT_NUMBER" => array(
				"NAME" => "Номер лицевого счета плательщика",
				"DESCR" => "",
				"VALUE" => "XML_ID",
				"TYPE" => "ACCOUNT"
			),
		"SHOULD_PAY" => array(
				"NAME" => "Сумма к оплате",
				"DESCR" => "",
				"VALUE" => "SHOULD_PAY",
				"TYPE" => "PAYMENT"
			),
	);
?>