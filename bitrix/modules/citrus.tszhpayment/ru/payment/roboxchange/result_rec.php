<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$resultMessage = "FAIL";

$inv_id = IntVal($_REQUEST["InvId"]);
if (IntVal($inv_id) > 0 && CModule::IncludeModule("citrus.tszhpayment"))
{
	$bCorrectPayment = True;

	$out_summ = $_REQUEST["OutSum"];
	$crc = $_REQUEST["SignatureValue"];
	
	if (!($arOrder = CTszhPayment::GetByID(IntVal($inv_id))))
		$bCorrectPayment = False;

	if ($bCorrectPayment)
		CTszhPaySystem::InitParamArrays($arOrder);

	$mrh_pass2 =  CTszhPaySystem::GetParamValue("ShopPassword2");
	$strCheck = md5($out_summ.":".$inv_id.":".$mrh_pass2);

	if ($bCorrectPayment && strtoupper($crc) != strtoupper($strCheck))
		$bCorrectPayment = False;
	
	if($bCorrectPayment)
	{
		$arFields = array(
				"PS_STATUS" => "Y",
				"PS_STATUS_CODE" => "-",
				"PS_STATUS_DESCRIPTION" => $strPS_STATUS_DESCRIPTION,
				"PS_STATUS_MESSAGE" => $strPS_STATUS_MESSAGE,
				"SUMM_PAYED" => $out_summ,
				"PS_CURRENCY" => "",
				"PS_RESPONSE_DATE" => Date(CDatabase::DateFormatToPHP(CLang::GetDateFormat("FULL", LANG))),
			);

		// You can comment this code if you want PAYED flag not to be set automatically
		if (DoubleVal($arOrder["SUMM"]) == DoubleVal($out_summ))
		{
			CTszhPayment::Pay($arOrder["ID"], "Y");
		}

		if (CTszhPayment::Update($arOrder["ID"], $arFields))
			$resultMessage = "OK".$arOrder["ID"];
	}
}

$APPLICATION->RestartBuffer();
echo $resultMessage;
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");	
?>