<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?
include(GetLangFileName(dirname(__FILE__)."/", "/payment.php"));
$mrh_login = CTszhPaySystem::GetParamValue("ShopLogin");
$mrh_pass1 =  CTszhPaySystem::GetParamValue("ShopPassword");
$inv_id = CTszhPaySystem::GetParamValue("OrderId"); 
$inv_desc =  CTszhPaySystem::GetParamValue("OrderDescr");
$user_mail = CTszhPaySystem::GetParamValue("EMAIL_USER");
$out_summ = number_format(CTszhPaySystem::GetParamValue("SHOULD_PAY"), 2, ".", ""); 
$crc = md5($mrh_login.":".$out_summ.":".$inv_id.":".$mrh_pass1);

$bTest = CTszhPaySystem::GetParamValue('TestMode') != '';

?>
<form action="<?=($bTest ? 'http://test.robokassa.ru/Index.aspx': 'https://merchant.roboxchange.com/Index.aspx')?>" method="post" target="_blank">
<font class="tablebodytext">
<?=GetMessage("PYM_TITLE")?><br>
<?=GetMessage("PYM_ORDER")?> <?echo $inv_id."  ".CTszhPaySystem::GetParamValue("DATE_INSERT")?><br>
<?=GetMessage("PYM_TO_PAY")?> <b><?echo CTszhPayment::FormatCurrency(CTszhPaySystem::GetParamValue("SHOULD_PAY"), CTszhPaySystem::GetParamValue("CURRENCY"))?></b>
<p>
<input type="hidden" name="FinalStep" value="1">
<input type="hidden" name="MrchLogin" value="<?=$mrh_login?>">
<input type="hidden" name="OutSum" value="<?=$out_summ?>">
<input type="hidden" name="InvId" value="<?=$inv_id?>">
<input type="hidden" name="Desc" value="<?=$inv_desc?>">
<input type="hidden" name="SignatureValue" value="<?=$crc?>">
<input type="hidden" name="Email" value="<?=$user_mail?>">
<input type="submit" name="Submit" value="<?=GetMessage("PYM_BUTTON")?>">
</p>
</font>
</form>
<p align=\"justify\"><font class=\"tablebodytext\"><?=GetMessage("PYM_WARN")?></p>