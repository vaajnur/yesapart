<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?
include(GetLangFileName(dirname(__FILE__)."/", "/payment.php"));

$psTitle = GetMessage("SPCP_DTITLE");
$psDescription = GetMessage("SPCP_DDESCR");

$arPSCorrespondence = array(
		"ShopLogin" => array(
				"NAME" => GetMessage("ShopLogin"),
				"DESCR" => GetMessage("ShopLogin_DESCR"),
				"VALUE" => "",
				"TYPE" => ""
			),
		"ShopPassword" => array(
				"NAME" => GetMessage("ShopPassword"),
				"DESCR" => GetMessage("ShopPassword_DESCR"),
				"VALUE" => "",
				"TYPE" => ""
			),
		"ShopPassword2" => array(
				"NAME" => GetMessage("ShopPassword2"),
				"DESCR" => GetMessage("ShopPassword_DESCR2"),
				"VALUE" => "",
				"TYPE" => ""
			),
		"TestMode" => array(
				"NAME" => GetMessage("TestMode"),
				"DESCR" => GetMessage("TestMode_DESCR"),
				"VALUE" => "1",
				"TYPE" => ""
			),
		"OrderId" => array(
				"NAME" => GetMessage("OrderId"),
				"DESCR" => GetMessage("OrderId_DESCR"),
				"VALUE" => "ID",
				"TYPE" => "PAYMENT" 
		),
		"OrderDescr" => array(
				"NAME" => GetMessage("OrderDescr"),
				"DESCR" => GetMessage("OrderDescr_DESCR"),
				"VALUE" => "",
				"TYPE" => ""
			),
		"SHOULD_PAY" => array(
				"NAME" => GetMessage("SHOULD_PAY"),
				"DESCR" => GetMessage("SHOULD_PAY_DESCR"),
				"VALUE" => "SHOULD_PAY",
				"TYPE" => "PAYMENT" 
		),
		"CURRENCY" => array(
				"NAME" => GetMessage("CURRENCY"),
				"DESCR" => GetMessage("CURRENCY_DESCR"),
				"VALUE" => "CURRENCY",
				"TYPE" => "PAYMENT" 
		),
		"DATE_INSERT" => array(
				"NAME" => GetMessage("DATE_INSERT"),
				"DESCR" => GetMessage("DATE_INSERT_DESCR"),
				"VALUE" => "DATE_INSERT",
				"TYPE" => "PAYMENT" 
		),
		"EMAIL_USER" => array(
                "NAME" => GetMessage("EMAIL_USER"),
                "DESCR" => GetMessage("EMAIL_USER_DESCR"),
				"VALUE" => "EMAIL",
				"TYPE" => "USER" 
		),
	);
?>
