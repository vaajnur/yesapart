<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Citrus\Tszhpayment\PaymentPayerIdTable;

include(GetLangFileName(dirname(__FILE__) . "/", "/payment.php"));

$orderId = IntVal($_REQUEST['orderId']);
$gatewayParams = CTszhPaymentGatewayMoneta::getInstance()->getParams($orderId);
if (!$gatewayParams)
{
	$ex = $APPLICATION->GetException();
	ShowError(GetMessage("CITRUS_TSZHPAYMENT_MONETARU_ERROR") . (is_object($ex) ? $ex->GetString() : ''));
	echo GetMessage("CITRUS_TSZHPAYMENT_MONETARU_ERROR_NOTICE", Array("#SITE_DIR#" => SITE_DIR));

	return;
}

$bCheckRequest = is_set($_REQUEST, 'MNT_COMMAND') && $_REQUEST["MNT_COMMAND"] == 'CHECK';

CModule::IncludeModule("citrus.tszhpayment");

$arRequiredFields = Array(
	'MNT_ID',
	'MNT_TRANSACTION_ID',
	'MNT_AMOUNT',
	'MNT_CURRENCY_CODE',
	'MNT_TEST_MODE',
	'MNT_SIGNATURE',
	'orderId',
);
if (!$bCheckRequest)
{
	$arRequiredFields[] = 'MNT_OPERATION_ID';
}

$bHasRequiredFields = true;
foreach ($arRequiredFields as $field)
{
	$bHasRequiredFields = $bHasRequiredFields && is_set($_REQUEST, $field);
	if (!$bHasRequiredFields)
	{
		break;
	}
}
function TszhPaymentChangeEncoding(&$content)
{
	// do nothing
}

$resultCode = 500; // Ошибка обработки. Автоматическая отправка уведомлений будет остановлена. Необходимо связаться с группой поддержки MONETA.RU.
if ($bHasRequiredFields && IntVal($_REQUEST['orderId']) > 0)
{
	$bCorrectPayment = true;
	$errorMessage = '';
	$rsPayment = CTszhPayment::GetList(array(), array("ID" => intval($_REQUEST['orderId'])), false, false, array("*"));
	if (!($arPayment = $rsPayment->Fetch()))
	{
		$errorMessage = Loc::getMessage("TSZH_PAYMENT_MONETA_RESULT_ORDER_NUMBER") . intval($_REQUEST['orderId']) . Loc::getMessage("TSZH_PAYMENT_MONETA_RESULT_ORDER_NOT_FOUND");
		$bCorrectPayment = false;
	}
	if ($bCorrectPayment)
	{
		CTszhPaySystem::InitParamArrays($arPayment, $arPayment["ID"]);
	}

	$_REQUEST['MNT_SUBSCRIBER_ID'] = isset($_REQUEST['MNT_SUBSCRIBER_ID']) ? $_REQUEST['MNT_SUBSCRIBER_ID'] : '';
	$calculatedSignature = md5(
		($bCheckRequest ? $_REQUEST["MNT_COMMAND"] : '') . $_REQUEST['MNT_ID'] . $_REQUEST['MNT_TRANSACTION_ID'] . $_REQUEST['MNT_OPERATION_ID'] .
		$_REQUEST['MNT_AMOUNT'] . $_REQUEST['MNT_CURRENCY_CODE'] . $_REQUEST['MNT_SUBSCRIBER_ID'] . $_REQUEST['MNT_TEST_MODE'] . $gatewayParams['integrityCode']
	);
	if ($_REQUEST['MNT_SIGNATURE'] != $calculatedSignature)
	{
		$errorMessage = "Не корректная подпись запроса";
		$bCorrectPayment = false;
	}
	if (isset($arPayment['ACCOUNT_ID']) && $arPayment['ACCOUNT_ID'] != $_REQUEST['MNT_SUBSCRIBER_ID'])
	{
		$errorMessage = Loc::getMessage("TSZH_PAYMENT_MONETA_RESULT_ORDER_PAYER_SPECIFIED_INCORRECTLY");
		$bCorrectPayment = false;
	}
	if ($bCorrectPayment)
	{
		if ($bCheckRequest)
		{
			if (!is_set($_REQUEST, "MNT_AMOUNT") || !$_REQUEST['MNT_AMOUNT'])
			{
				$resultCode = 100; // Ответ содержит сумму заказа для оплаты. Данным кодом следует отвечать, когда в параметрах проверочного запроса не был указан параметр MNT_AMOUNT.
				$description = Loc::getMessage("TSZH_PAYMENT_MONETA_RESULT_ORDER_AMOUNT_SPECIFIED");
			}
			elseif ($arPayment["PAYED"] == "Y")
			{
				$resultCode = 200; //  Заказ оплачен. Уведомление об оплате магазину доставлено.
				$description = Loc::getMessage("TSZH_PAYMENT_MONETA_RESULT_ORDER_PAID");

			}
			else
			{
				$resultCode = 402; // Заказ создан и готов к оплате. Уведомление об оплате магазину не доставлено.
				$description = Loc::getMessage("TSZH_PAYMENT_MONETA_RESULT_ORDER_READY");
			}
			$arTszh = $GLOBALS["TSZH_PAYMENT_PARAMS"]["TSZH"];
			$arAccountName = explode(' ', $GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]['NAME']);
			$arAttributes = Array(
				"tszh.bankName" => $arTszh['~BANK'],
				"tszh.bankBik" => $arTszh['BIK'],
				"tszh.rsch" => $arTszh['RSCH'],
				"tszh.orgInn" => $arTszh['INN'],
				"tszh.orgName" => $arTszh['~NAME'],
				"tszh.account" => $GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]['XML_ID'],
				"tszh.lastName" => $arAccountName[0],
				"tszh.firstName" => $arAccountName[1],
				"tszh.middleName" => $arAccountName[2],
				"tszh.fullAddress" => $GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]['FULL_ADDRESS'],
			);
			$arAttrs = Array(
				array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_PAYER_DATA"), 'VALUE' => ""),
				array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_LS"), 'VALUE' => $GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]['XML_ID']),
				array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_FIO"), 'VALUE' => $arAccountName[0].' '.$arAccountName[1].' '.$arAccountName[2]),
				array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_ADDRESS"), 'VALUE' => $GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]['FULL_ADDRESS']),
				array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_PAYMENT_DATA"), 'VALUE' => ""),
				array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_NAME"), 'VALUE' => $arTszh['~NAME']),
				array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_INN"), 'VALUE' => $arTszh['INN']),
				array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_BANK"), 'VALUE' => $arTszh['~BANK']),
				array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_BIK"), 'VALUE' => $arTszh['BIK']),
				array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_RSCH"), 'VALUE' => $arTszh['RSCH'])
			);
			$is_budget = isset($arTszh['IS_BUDGET']) && $arTszh['IS_BUDGET'] == 'Y';
			if ($is_budget)
			{
				$arAttributes['WIREKPP'] = isset($arTszh['KPP']) ? $arTszh['KPP'] : '';
				$arAttributes['WIREKBK'] = isset($arTszh['KBK']) ? $arTszh['KBK'] : '';
				$arAttributes['WIREOKTMO'] = isset($arTszh['OKTMO']) ? $arTszh['OKTMO'] : '';
				$arAttributes['WIREALTPAYERIDENTIFIER'] = PaymentPayerIdTable::getAltPayerIdentifier($arPayment['ID']);
			}

			$resultSignature = md5($resultCode . $_REQUEST["MNT_ID"] . $_REQUEST["MNT_TRANSACTION_ID"] . $gatewayParams['integrityCode']);
			ob_start();
			echo '<?xml version="1.0" encoding="' . SITE_CHARSET . '"?>';
			?>

			<MNT_RESPONSE>
			<MNT_ID><?=$_REQUEST["MNT_ID"]?></MNT_ID>
			<MNT_TRANSACTION_ID><?=$_REQUEST["MNT_TRANSACTION_ID"]?></MNT_TRANSACTION_ID>
			<MNT_RESULT_CODE><?=htmlspecialcharsbx($resultCode)?></MNT_RESULT_CODE>
			<MNT_DESCRIPTION><?=htmlspecialcharsbx($description)?></MNT_DESCRIPTION>
			<MNT_AMOUNT><?=number_format($arPayment["SUMM"], 2, '.', '')?></MNT_AMOUNT>
			<MNT_SIGNATURE><?=$resultSignature?></MNT_SIGNATURE>
			<MNT_ATTRIBUTES>
				<?
				foreach ($arAttributes as $key => $value)
				{
					?>
					<ATTRIBUTE>
						<KEY><?=htmlspecialcharsbx($key)?></KEY>
						<VALUE><?=htmlspecialcharsbx($value)?></VALUE>
					</ATTRIBUTE>
					<?
				}
				?>
			</MNT_ATTRIBUTES>
			<MNT_RECEIPT>
				<?
				foreach ($arAttrs as $values)
				{
					?>
					<ATTRIBUTE>
						<KEY><?=htmlspecialcharsbx($values['KEY'])?></KEY>
						<VALUE><?=htmlspecialcharsbx($values['VALUE'])?></VALUE>
					</ATTRIBUTE>
					<?
				}
				?>
			</MNT_RECEIPT>
			</MNT_RESPONSE><?

			$response = ob_get_contents();
			ob_end_clean();
			/*if (!defined("BX_UTF"))
				$response = iconv(SITE_CHARSET, 'utf-8', $response);*/
			header("Content-Type: application/xml; charset=" . SITE_CHARSET);
			echo $response;

			return;
		}
		else
		{
			$arFields = array(
				"PSYS_STATUS" => "Y",
				"PSYS_STATUS_CODE" => "-",
				"PSYS_STATUS_DESCR" => GetMessage("CITRUS_TSZHPAYMENT_STATUS_DESCR", Array(
					"#MNT_TRANSACTION_ID#" => htmlspecialcharsbx($_REQUEST['MNT_TRANSACTION_ID']),
					"#MNT_OPERATION_ID#" => htmlspecialcharsbx($_REQUEST['MNT_OPERATION_ID']),
				)),
				"PSYS_STATUS_RESPONSE" => '',
				"PSYS_OPERATION_ID" => htmlspecialcharsbx($_REQUEST['MNT_OPERATION_ID']),
				"PSYS_FEE" => 0,
				"SUMM_PAYED" => $_REQUEST['MNT_AMOUNT'],
				"PSYS_STATUS_DATE" => ConvertTimeStamp(false, "FULL"),
				//"USER_ID" => $arPayment["USER_ID"]
			);

			// определение комиссии с получателя платежа
			$info = CTszhPaymentGatewayMoneta::getInstance()->getOperationInfo($_REQUEST['MNT_OPERATION_ID']);
			$sourceAmount = $targetAmount = false;
			foreach ($info['operation']['attribute'] as $a)
			{
				if ($a['key'] == 'sourceamountfee' && $a['value'] < 0)
				{
					$arFields["PSYS_FEE"] = abs($a['value']);
				}
			}

			// You can comment this code if you want PAYED flag not to be set automatically
			if ($arPayment["SUMM"] >= $_REQUEST['MNT_AMOUNT'])
			{
				$arFields["PAYED"] = "Y";
				$arFields["DATE_PAYED"] = ConvertTimeStamp(false, "FULL");
				$arFields["EMP_PAYED_ID"] = false;
			}

			if (CTszhPayment::Update($arPayment["ID"], $arFields))
			{
				$resultCode = 200;
				if (ModuleManager::isModuleInstalled('otr.sale'))
				{
					if ($arPayment['PAYED']!="Y")
					{
						CTszhPayment::SetCheck($ID = $arPayment["ID"]);
					}
				}
			}
		}
	}
}
$arTszh = $GLOBALS["TSZH_PAYMENT_PARAMS"]["TSZH"];
$arAccountName = explode(' ', $GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]['NAME']);

$arAttributes = Array(
	array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_PAYER_DATA"), 'VALUE' => ""),
	array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_LS"), 'VALUE' => $GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]['XML_ID']),
	array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_FIO"), 'VALUE' => $arAccountName[0].' '.$arAccountName[1].' '.$arAccountName[2]),
	array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_ADDRESS"), 'VALUE' => $GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]['FULL_ADDRESS']),
	array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_PAYMENT_DATA"), 'VALUE' => ""),
	array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_NAME"), 'VALUE' => $arTszh['~NAME']),
	array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_INN"), 'VALUE' => $arTszh['INN']),
	array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_BANK"), 'VALUE' => $arTszh['~BANK']),
	array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_BIK"), 'VALUE' => $arTszh['BIK']),
	array('KEY' => GetMessage("CITRUS_TSZHPAYMENT_RSCH"), 'VALUE' => $arTszh['RSCH'])
);

$resultSignature = md5($resultCode . $_REQUEST["MNT_ID"] . $_REQUEST["MNT_TRANSACTION_ID"] . $gatewayParams['integrityCode']);

ob_start();
echo '<?xml version="1.0" encoding="' . SITE_CHARSET . '"?>';
?>
	<MNT_RESPONSE>
		<MNT_ID><?=$_REQUEST["MNT_ID"]?></MNT_ID>
		<MNT_TRANSACTION_ID><?=$_REQUEST["MNT_TRANSACTION_ID"]?></MNT_TRANSACTION_ID>
		<MNT_RESULT_CODE><?=$resultCode?></MNT_RESULT_CODE>
		<MNT_SIGNATURE><?=$resultSignature?></MNT_SIGNATURE>
		<!-- <?=htmlspecialcharsbx($errorMessage)?> -->
		<MNT_RECEIPT>

			<?
			foreach ($arAttributes as $values)
			{
				?>
				<ATTRIBUTE>
					<KEY><?=htmlspecialcharsbx($values['KEY'])?></KEY>
					<VALUE><?=htmlspecialcharsbx($values['VALUE'])?></VALUE>
				</ATTRIBUTE>
				<?
			}
			?>

		</MNT_RECEIPT>
	</MNT_RESPONSE>
<?
$response = ob_get_contents();
ob_end_clean();

header("Content-Type: application/xml; charset=" . SITE_CHARSET);
/*if (!defined("BX_UTF"))
	$response = iconv(SITE_CHARSET, 'utf-8', $response);*/
echo $response;

