<?

use Citrus\Tszhpayment\PaymentPayerIdTable;
use Citrus\Tszhpayment\PaymentServicesTable;

include(GetLangFileName(dirname(__FILE__) . "/", "/payment.php"));

// отлючим конвертацию кодировки в компоненте оплаты: обрабатываем кодировку самостоятельно
$arResult["PAY_SYSTEM"]["ENCODING"] = '';

if (is_set($_GET, 'terms'))
{
	$search = Array("NAME", "INN", "KPP", "BANK", "BIK", "RSCH");
	$replace = Array();
	foreach ($search as &$field)
	{
		$replace[$field] = $GLOBALS["TSZH_PAYMENT_PARAMS"]["TSZH"][$field];
		$field = "#$field#";
	}
	unset($field);

	ob_start();
	require('terms.inc.php');
	$html = ob_get_contents();
	ob_end_flush();

	$APPLICATION->RestartBuffer();
	$html = str_replace($search, $replace, $html);
	echo $html;
	die();
}

$paymentId = $GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["ID"];

$gateway = CTszhPaymentGatewayMoneta::getInstance();
$gatewayParams = $gateway->getParams($paymentId);
if (!$gatewayParams)
{
	$ex = $APPLICATION->GetException();
	ShowError(GetMessage("CITRUS_TSZHPAYMENT_MONETARU_ERROR") . (is_object($ex) ? $ex->GetString() : ''));
	echo GetMessage("CITRUS_TSZHPAYMENT_MONETARU_ERROR_NOTICE", Array("#SITE_DIR#" => SITE_DIR));

	return;
}
$gatewayParams["demo"] = false;
if ($gatewayParams["demo"])
{
	ShowNote(GetMessage("CITRUS_TSZHPAYMENT_MONETARU_DEMO_NOTICE"));

	return;
}
if (CTszh::isDirty($GLOBALS["TSZH_PAYMENT_PARAMS"]["TSZH"]["ID"]))
{
	ShowNote(GetMessage("CITRUS_TSZHPAYMENT_MONETARU_FIELDS_NOTICE"));

	return;
}

$IS_BUDGET = $GLOBALS["TSZH_PAYMENT_PARAMS"]["TSZH"]["IS_BUDGET"];
if ($IS_BUDGET)
{
	$addInfo = PaymentPayerIdTable::getByPrimary($GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["ID"])->fetch();
	$arDocType = PaymentPayerIdTable::getDocumentTypeList();
	$arNationality = PaymentPayerIdTable::getNationalityList();
}

$MNT_TEST_MODE = $gatewayParams["demo"] ? "1" : "0";
$MNT_PAYMENT_SERVER = $MNT_TEST_MODE ? 'demo.moneta.ru' : 'www.payanyway.ru';
$MNT_ID = $gatewayParams['accountId'];
$MNT_TRANSACTION_ID = $paymentId . date('Ymdhis') . rand(0, 99999);
$MNT_CURRENCY_CODE = $GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["CURRENCY"];
$MNT_AMOUNT = number_format($GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["SHOULD_PAY"], 2, ".", "");
$MNT_SUBSCRIBER_ID = $GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]["ID"];
$MNT_SUBSCRIBER_ID = $MNT_SUBSCRIBER_ID ? $MNT_SUBSCRIBER_ID : '';
$MNT_SIGNATURE = md5($MNT_ID . $MNT_TRANSACTION_ID . $MNT_AMOUNT . $MNT_CURRENCY_CODE . $MNT_SUBSCRIBER_ID . $MNT_TEST_MODE . $gatewayParams['integrityCode']);

$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
$currentUrl = $protocol . $_SERVER['HTTP_HOST'];

$MNT_SUCCESS_URL = CTszhPaySystem::GetParamValue("MNT_SUCCESS_URL");
$MNT_SUCCESS_URL = empty($MNT_SUCCESS_URL) ? $currentUrl . '/personal/payment/?success' : $MNT_SUCCESS_URL . '&id=' . $paymentId;
$MNT_FAIL_URL = CTszhPaySystem::GetParamValue("MNT_FAIL_URL");
$MNT_FAIL_URL = empty($MNT_FAIL_URL) ? $currentUrl . '/personal/payment/?fail' : $MNT_FAIL_URL . '&id=' . $paymentId;

$ORDER_ID = $GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["ID"];

$paymentDescription = htmlspecialcharsbx(GetMessage("CITRUS_TSZHPAYMENT_MONETARU_PAYMENT_DESCRIPTION", Array("#ACCOUNT_NUMBER#" => $GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]["XML_ID"])));
$GLOBALS["TSZH_PAYMENT_PARAMS"]["TSZH"]["DESCRIPTION"] = $paymentDescription;
$services = PaymentServicesTable::getList(array(

	'filter'  => array('PAYMENT_ID'=>$paymentId),

))->fetchAll();
?>
<style>
	.citrustszh-moneta-payment-info {
		margin: 1em 0;
		padding: 0;
		line-height: 1.3;
		color: #333;
		font-size: 1.2em;
		display: table;
	}
	.citrustszh-moneta-payment-info > div {
		display: table-row;
	}
	.citrustszh-moneta-payment-info > div > div {
		display: table-cell;
		text-align: left;
		padding: 0 0 .3em 0;
		vertical-align: top;
	}
	.citrustszh-moneta-payment-info > div > div:first-child {
		padding-right: 8px;
		width: 150px;
		text-align: right;
		font-weight: bold;
		white-space: normal;
	}
	.citrustszh-moneta-payment-info > div > div:first-child:after {
		content: ': ';
		font-weight: normal;
	}
	@media screen and (max-width: 499px), screen and (min-width: 500px) and (max-width: 767px) and (orientation: portrait) {
		.citrustszh-moneta-payment-info > div > div:first-child {
			text-align: left;
			padding-bottom: 1px;
		}
		.citrustszh-moneta-payment-info > div > div {
			display: block;
		}
	}
	.citrustszh-moneta-payment-info small {
		font-size: 80%;
		color: #666;
		display: block;
	}
	.citrustszh-moneta-payment-info-total td,
	.citrustszh-moneta-payment-info-total th {
		line-height: 20px;
	}
	.citrustszh-moneta-payment-info-total td {
		font-size: 120%;
	}
	.services{
		background-color:#F0F2F5;
		padding-bottom: 30px;
		padding-top: 30px;

	}
</style>
<form action="https://<?=$MNT_PAYMENT_SERVER?>/assistant.htm" method="post">
	<h3>Информация о платеже</h3>
	<div class="citrustszh-moneta-payment-info">
		<?
		$paymentDetails = Array("NAME", "INN", "KPP", "BANK", "BIK", "RSCH");
		foreach ($paymentDetails as $field)
		{
			$value = $GLOBALS["TSZH_PAYMENT_PARAMS"]["TSZH"][$field];
			?>
			<div>
				<div><?=GetMessage("CITRUS_TSZHPAYMENT_MONETARU_PAYMENT_DETAILS_F_" . $field)?></div>
				<div><?=$GLOBALS["TSZH_PAYMENT_PARAMS"]["TSZH"][$field]?></div>
			</div>
			<?
		}
		?>
		<?if($services){?>
		<div class="services">
			<div style="width:100%; text-align:center;"><?=GetMessage("TSZH_PAYMENT_SBR_SERVICE_DESCRIPTION")?></div>
		<? foreach ($services as $ServiceName):
		?>
		<div>
			<?if($ServiceName['SUMM_PAYED']>0){?>
				<div style="width:50%; text-align:right; padding-right: 8px;"><?=$ServiceName['SERVICE_NAME']?>:</div>
				<div><?=CTszhPayment::FormatCurrency($ServiceName['SUMM_PAYED'], $CURRENCY_ABBR)?></div>
			<?}?>
		</div>

		<?endforeach?>
		</div>
		<? } ?>
		<div>
			<div>Лицевой счет</div>
			<div><?=$GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]["XML_ID"]?></div>
		</div>
		<div>
			<div>Сумма</div>
			<div><?=CTszhPayment::FormatCurrency($MNT_AMOUNT, $MNT_CURRENCY_CODE)?>
				<small>Комиссия <?=$gatewayParams['paymentFee']?>%</small>
			</div>
		</div>
		<div class="citrustszh-moneta-payment-info-total">
			<div>Итого</div>
			<div><?=CTszhPayment::FormatCurrency($MNT_AMOUNT * (1 + $gatewayParams['paymentFee'] / 100), $MNT_CURRENCY_CODE)?></div>
		</div>
		<?php
		if ($IS_BUDGET) :
		?>
			<div class="citrustszh-moneta-payment-info-total">
				<div>Документ</div>
				<div><?= $arDocType[$addInfo['DOCUMENT_TYPE']] ?></div>
			</div>
			<div class="citrustszh-moneta-payment-info-total">
				<div>Серия и номер документа</div>
				<div><?= $addInfo['DOCUMENT_NUMBER'] ?></div>
			</div>
			<div class="citrustszh-moneta-payment-info-total">
				<div>Гражданство</div>
				<div><?= $arNationality[$addInfo['NATIONALITY']] ?></div>
			</div>
		<?php
		endif;
		?>
	</div>
	<p><?=GetMessage("CITRUS_TSZHPAYMENT_MONETA_TERMS", Array("#URL#" => $APPLICATION->GetCurPageParam("terms=1", Array('terms'))))?></p>
    <p><?=GetMessage("CITRUS_TSZHPAYMENT_MONETA_CONFIRM")?></p>
	<p>
		<input type="hidden" name="MNT_ID" value="<?=$MNT_ID?>">
		<input type="hidden" name="MNT_TRANSACTION_ID" value="<?=$MNT_TRANSACTION_ID?>">
		<input type="hidden" name="MNT_CURRENCY_CODE" value="<?=$MNT_CURRENCY_CODE?>">
		<input type="hidden" name="MNT_AMOUNT" value="<?=$MNT_AMOUNT?>">
		<input type="hidden" name="MNT_TEST_MODE" value="<?=$MNT_TEST_MODE?>">
		<?
		if ($MNT_SUBSCRIBER_ID)
		{
			?><input type="hidden" name="MNT_SUBSCRIBER_ID" value="<?=$MNT_SUBSCRIBER_ID?>"><?
		}
		?>
		<input type="hidden" name="MNT_SIGNATURE" value="<?=$MNT_SIGNATURE?>">
		<input type="hidden" name="MNT_SUCCESS_URL" value="<?=$MNT_SUCCESS_URL?>">
		<input type="hidden" name="MNT_FAIL_URL" value="<?=$MNT_FAIL_URL?>">
		<input type="hidden" name="MNT_DESCRIPTION" value="<?=CTszhPaymentGatewayMoneta::paramTo($paymentDescription)?>">
		<input type="hidden" name="MNT_CUSTOM1" value="<?=($gatewayParams['key'] . '|' . $GLOBALS["TSZH_PAYMENT_PARAMS"]["TSZH"]["ID"])?>">
		<?
		$validMethods = $gateway->getPaymentMethods();

		$unitId = CTszhPaySystem::GetParamValue("unitId");
		if (!array_key_exists($unitId, $validMethods))
		{
			$unitId = array_shift(array_keys($validMethods));
		}

		$limitIds = CTszhPaySystem::GetParamValue("limitIds");
		$limitIds = is_array($limitIds) ? $limitIds : array();
		foreach ($limitIds as $key => $limitId)
		{
			if (!array_key_exists($limitId, $validMethods))
			{
				unset($limitIds[$key]);
			}
		}

		?>
		<input type="hidden" name="paymentSystem.unitId" value="<?=$unitId?>">
		<?
		if (!empty($limitIds))
		{
			?><input type="hidden" name="paymentSystem.limitIds" value="<?=implode(',', $limitIds)?>"><?
		}
		?>
		<input type="hidden" name="orderId" value="<?=$ORDER_ID?>">
		<input type="submit" value="<?=GetMessage("CITRUS_TSZHPAYMENT_MONETARU_BUTTON")?>">
	</p>
	<p><?=GetMessage("CITRUS_TSZHPAYMENT_SECURE_NOTICE")?></p>
</form>
