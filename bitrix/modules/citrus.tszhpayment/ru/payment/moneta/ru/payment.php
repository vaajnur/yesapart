<?
global $MESS;
$MESS['CITRUS_TSZHPAYMENT_MONETARU_TITLE'] = 'Монета.Ру';

$MESS['CITRUS_TSZHPAYMENT_MONETARU_ORDER'] = 'Платеж №';
$MESS["CITRUS_TSZHPAYMENT_MONETARU_PAYMENT_DESCRIPTION"] = "за услуги ЖКХ по л/с № #ACCOUNT_NUMBER#";
$MESS['CITRUS_TSZHPAYMENT_MONETARU_TO_PAY'] = 'Сумма к зачислению:';
$MESS['CITRUS_TSZHPAYMENT_MONETARU_BUTTON'] = 'Оплатить';
$MESS["CITRUS_TSZHPAYMENT_STATUS_DESCR"] = "Номер операции: #MNT_OPERATION_ID#, внутренний идентификатор заказа: #MNT_TRANSACTION_ID#";

$MESS["CITRUS_TSZHPAYMENT_MONETARU_ERROR"] = "Произошла ошибка при взаимодействии с сервисом оплаты: ";
$MESS["CITRUS_TSZHPAYMENT_MONETARU_ERROR_NOTICE"] = "<p>Пожалуйста, повторите попытку оплаты позже. Если ошибка повторится, <a href=\"#SITE_DIR#contacts/\" target='_blank'>обратитесь к администрации сайта</a>.</p>";
$MESS["CITRUS_TSZHPAYMENT_MONETARU_DEMO_NOTICE"] = "Оплата через сервис Монета.ру недоступна в пробной версии 1С:Сайт ЖКХ.<br>Возможность оплаты появится после регистрации коммерческой версии продукта.";
$MESS["CITRUS_TSZHPAYMENT_MONETARU_FIELDS_NOTICE"] = "Оплата через сервис Монета.ру временно недоступна.<br>Пожалуйста, обратитесь к администрации сайта.";
$MESS["CITRUS_TSZHPAYMENT_SECURE_NOTICE"] = "<p>Безопасность платежей со&nbsp;счетов банковских карт обеспечивается при помощи передачи данных по&nbsp;защищенному соединению HTTPS, а&nbsp;также может обеспечиваться двухфакторной аутентификацией по&nbsp;технологии <nobr>3-D</nobr> Secure.</p><p>В&nbsp;соответствии с&nbsp;Федеральным Законом &laquo;О&nbsp;защите прав потребителей&raquo; в&nbsp;случае, если вам была оказана услуга или реализован товар ненадлежащего качества, платеж может быть возвращен на&nbsp;счёт банковской карты, с&nbsp;которой производилась оплата.</p>";

$MESS["CITRUS_TSZHPAYMENT_MONETA_TERMS"] = "Зачисление средств на ваш лицевой счет в течение 3-х рабочих дней. <a href=\"#URL#\" target=\"_blank\">Условия оплаты услуг</a>";

$MESS["CITRUS_TSZHPAYMENT_MONETARU_PAYMENT_DETAILS_F_NAME"] = "Получатель";
$MESS["CITRUS_TSZHPAYMENT_MONETARU_PAYMENT_DETAILS_F_INN"] = "ИНН";
$MESS["CITRUS_TSZHPAYMENT_MONETARU_PAYMENT_DETAILS_F_KPP"] = "КПП";
$MESS["CITRUS_TSZHPAYMENT_MONETARU_PAYMENT_DETAILS_F_BANK"] = "Банк";
$MESS["CITRUS_TSZHPAYMENT_MONETARU_PAYMENT_DETAILS_F_BIK"] = "БИК банка";
$MESS["CITRUS_TSZHPAYMENT_MONETARU_PAYMENT_DETAILS_F_RSCH"] = "Расчетный счет";

$MESS["CITRUS_TSZHPAYMENT_TERMS_UNAUTH"] = "<div style=\"margin-top: -1em;\">Зачисление средств на ваш лицевой счет в течение 3-х рабочих дней. <a href=\"/bitrix/modules/citrus.tszhpayment/ru/payment/moneta/terms.php\" target=\"_blank\">Условия оплаты услуг</a></div>";
$MESS["CITRUS_TSZHPAYMENT_MONETA_CONFIRM"] = "Нажимая кнопку \"оплатить\" вы соглашаетесь с оплатой с помощью сервиса Монета.ру ";

$MESS['CITRUS_TSZHPAYMENT_PAYER_DATA'] = 'Данные плательщика';
$MESS['CITRUS_TSZHPAYMENT_LS'] = 'Лицевой счет';
$MESS['CITRUS_TSZHPAYMENT_FIO'] = 'Ф.И.О';
$MESS['CITRUS_TSZHPAYMENT_ADDRESS'] = 'Адрес проживания';

$MESS['CITRUS_TSZHPAYMENT_PAYMENT_DATA'] = 'Данные получателя платежа';
$MESS['CITRUS_TSZHPAYMENT_NAME'] = 'Название организации';
$MESS['CITRUS_TSZHPAYMENT_INN'] = 'ИНН';
$MESS['CITRUS_TSZHPAYMENT_BANK'] = 'Банк';
$MESS['CITRUS_TSZHPAYMENT_BIK'] = 'Бик';
$MESS['CITRUS_TSZHPAYMENT_RSCH'] = 'Расчетный счет';
$MESS["TSZH_PAYMENT_MONETA_SERVICE_DESCRIPTION"] = "Расшифровка платежа";