<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule("citrus.tszh") || !CModule::IncludeModule("citrus.tszhpayment"))
	throw new Exception("Не установлены необходимые модули");

include(GetLangFileName(dirname(__FILE__)."/", "/payment.php"));

$editionModule = CTszhFunctionalityController::GetEdition();
$gateway = CTszhPaymentGatewayMoneta::getInstance();

$psTitle = GetMessage("CITRUS_TSZHPAYMENT_MONETARU_TITLE");
$psDescription = GetMessage("CITRUS_TSZHPAYMENT_MONETARU_DESC");
$arPSCorrespondence = array(
	"unitId" => array(
		"NAME" => "Метод оплаты по умолчанию",
		"DESCR" => "Выбранный метод будет выбираться по умолчанию после перехода к оплате на сайте сервиса",
		"OPTION" => array(
			"type" => "list",
			"multiple" => false,
			"items" => $gateway->getPaymentMethods(),
		),
	),
	"limitIds" => array(
		"NAME" => "Ограничить методы оплаты",
		"DESCR" => "В списке вариантов оплаты на сайте сервиса будут отображаться только выбрынные здесь методы оплаты",
		"OPTION" => array(
			"type" => "list",
			"multiple" => true,
			"items" => $gateway->getPaymentMethods(),
		),
	),
);