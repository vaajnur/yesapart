<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (!CModule::IncludeModule('citrus.tszhpayment'))
{
	$bCorrectPayment = false;
	$code = "200";
	$techMessage = "Не подключен модуль оплаты (citrus.tszhpayment).";
}
else{
	$bCorrectPayment = true;
	$code = "0";
	
}
$orderIsPaid = $_POST["orderIsPaid"];
$action = $_POST["action"];
$scid = $_POST["scid"];
$orderID = $_POST["orderID"];
$sum = $_POST["orderSumAmount"];
$shopID = $_POST["shopID"];
$invoiceID = $_POST["invoiceID"];
$pass = $_POST["pass"];
$orderCreatedDatetime = $_POST["orderCreatedDatetime"];
$paymentType = $_POST["paymentType"];
$customerNumber = IntVal($_POST["customerNumber"]);
$md5 = $_POST["md5"];
$paymentDateTime = $_POST["paymentDateTime"];

//var_dump($sum); die;
$bCorrectPayment = true;
if(!($arPayment = CTszhPayment::GetByID($orderID)))
{
	$bCorrectPayment = false;
	$code = "200"; //неверные параметры
	$techMessage = "ID платежа неизвестен.";
}
else{
	$bCorrectPayment = true;
	$code = "0"; //верные параметры
	//$techMessage = "ID платежа неизвестен.";
}

if ($bCorrectPayment)
	CTszhPaySystem::InitParamArrays($arPayment);

if ($bCorrectPayment && (!$action || $scid != CTszhPaySystem::GetParamValue("SCID")))
{
	$bCorrectPayment = false;
	$code = "200"; 
	$techMessage = "Неизвестен тип запроса или номер витрины.";
}
if ($bCorrectPayment && ($action = 'checkOrder' || $scid = CTszhPaySystem::GetParamValue("SCID")))
{
	$bCorrectPayment = true;
	$code = "0"; 
	
}
//var_dump($arPayment["SUMM"]); die;
if($bCorrectPayment)
{
	$arFields = array(
		'PSYS_STATUS' => '',
		'PSYS_STATUS_DESCR' => '',
		'PSYS_STATUS_RESPONSE' => print_r($_POST, 1),
		'PSYS_STATUS_DATE' => ConvertTimeStamp(time(), "FULL"),
	);

	// You can comment this code if you want PAYED flag not to be set automatically
	if (DoubleVal($arPayment["SUMM"]) == DoubleVal($sum))
	{
		if($arPayment["PAYED"] == "Y")
			$code = "0";
		else
		{
			$arFields["SUMM_PAYED"] = DoubleVal($sum);
			if (CTszhPayment::Pay($arPayment["ID"], "Y"))
				$code = "0";
			else
			{
				$code = "1000";
				$techMessage = "Ошибка оплаты.";
			}
		}
	}
	else
	{
		$code = "200"; //неверные параметры
		$techMessage = "Сумма платежа не верна.";
	}

	/*if(*/CTszhPayment::Update($arPayment["ID"], $arFields);//)
/*		if(strlen($techMessage)<=0 && strlen($code)<=0)
			$code = "0";*/
}

$action = $_POST["action"];
$APPLICATION->RestartBuffer();

$dateISO = date("c");
header("Content-Type: text/xml");
header("Pragma: no-cache");
$text = "<"."?xml version=\"1.0\" encoding=\"windows-1251\"?".">\n";
//$text .= "<response performedDatetime=\"".$dateISO."\">";
if(strlen($techMessage) > 0)
	$text .= "<" . $action . "Response  performedDatetime=\"".$dateISO."\" code=\"" . $code . "\"  invoiceId=\"" . $invoiceId . "\"  shopId=\"" . $shopId . "\" techMessage=\"" . $techMessage . "\"/>";
else
	$text .= "<" . $action . "Response  performedDatetime=\"".$dateISO."\" code=\"" . $code . "\"  invoiceId=\"" . $invoiceId . "\"  shopId=\"" . $shopId . "\" />";
//$text .= "</response>";
echo $text;
die();
?>