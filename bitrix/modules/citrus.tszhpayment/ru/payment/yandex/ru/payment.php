<?
$MESS["SPCP_DTITLE"] = "Яндекс.Деньги";
$MESS["SPCP_DDESCR"] = "Работа через Центр Приема Платежей <a href=\"http://money.yandex.ru\" target=\"_blank\">http://money.yandex.ru</a>";

$MESS["SHOP_NAME"] = "Наименование магазина";
$MESS["SHOP_NAME_DESCR"] = "";
$MESS["SHOP_INN"] = "ИНН магазина";
$MESS["SHOP_INN_DESCR"] = "";
$MESS["SHOP_KPP"] = "КПП магазина";
$MESS["SHOP_KPP_DESCR"] = "";
$MESS["BANK_ACCOUNT"] = "Расчетный счет магазина";
$MESS["BANK_ACCOUNT_DESCR"] = "";
$MESS["BANK_NAME"] = "Намиенование банка магазина";
$MESS["BANK_NAME_DESCR"] = "";
$MESS["BANK_BIK"] = "БИК банка";
$MESS["BANK_BIK_DESCR"] = "";
$MESS["CPPID"] = "Идентификатор магазина";
$MESS["SCID"] = "Номер витрины магазина в ЦПП";
$MESS["SERVER"] = "Адрес платежного сервера";
$MESS["LOG"] = "Логин";
$MESS["PASS"] = "Пароль";
$MESS["SCID_DESCR"] = "";
$MESS["ORDER_ID"] = "Номер платежа";
$MESS["ORDER_ID_DESCR"] = "";
$MESS["ORDER_DATE"] = "Дата создания платежа";
$MESS["ORDER_DATE_DESCR"] = "";
$MESS["SHOULD_PAY"] = "Сумма платежа";
$MESS["SHOULD_PAY_DESCR"] = "Сумма к оплате";
$MESS["ORDER_DESCR"] = "Описание платежа";
$MESS["ORDER_DESCR_DESCR"] = "";
$MESS["ORDER_DESCR_VALUE"] = "за услуги ЖКХ по л/с № #ACCOUNT_NUMBER# за #PREV_MONTH#";
$MESS["CUSTOMER_FIO"] = "ФИО владельца лицевого счета";
$MESS["CUSTOMER_FIO_DESCR"] = "";
$MESS["CUSTOMER_ADDRESS"] = "Адрес владельца лицевого счета";
$MESS["CUSTOMER_ADDRESS_DESCR"] = "";
$MESS["NOTIFY_URL"] = "URL приема уведомлений об оплате";
$MESS["NOTIFY_URL_DESCR"] = "Адрес станицы сайта, на которую Яндекс.Деньги будут отправлять уведомления об оплате";
$MESS["SUCESS_URL"] = "URL возврата в магазин при успешном осуществлении платежа";
$MESS["SUCESS_URL_DESCR"] = "Адрес станицы сайта, на которую будет перенаправлен пользователь после успешного осуществления платежа";

$MESS["PYM_F_ORDER_DESCR"] = "Описание платежа";

$MESS["PYM_ERROR_SERVER_NAME"] = "В настройках текущего сайта не указан URL сервера.";

$MESS["PYM_TITLE"] = "Вы хотите оплатить через систему <b>Яндекс.Деньги</b>";
$MESS["PYM_ORDER"] = "Платеж №";
$MESS["PYM_FROM"] = "от";
$MESS["PYM_TO_PAY"] = "Сумма к оплате:";
$MESS["PYM_BUTTON"] = "Оплатить";

$MESS["P_M_01"] = "январь";
$MESS["P_M_02"] = "февраль";
$MESS["P_M_03"] = "март";
$MESS["P_M_04"] = "апрель";
$MESS["P_M_05"] = "май";
$MESS["P_M_06"] = "июнь";
$MESS["P_M_07"] = "июль";
$MESS["P_M_08"] = "август";
$MESS["P_M_09"] = "сентябрь";
$MESS["P_M_10"] = "октябрь";
$MESS["P_M_11"] = "ноябрь";
$MESS["P_M_12"] = "декабрь";
?>