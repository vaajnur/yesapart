<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?
__IncludeLang(GetLangFileName(dirname(__FILE__)."/", "/payment.php"));

$psTitle = GetMessage("SPCP_DTITLE");
$psDescription = GetMessage("SPCP_DDESCR");

$arPSCorrespondence = array(
		"SHOP_NAME" => array(
				"NAME" => GetMessage("SHOP_NAME"),
				"DESCR" => GetMessage("SHOP_NAME_DESCR"),
				"VALUE" => "NAME",
				"TYPE" => "TSZH"
			),
		"SHOP_INN" => array(
				"NAME" => GetMessage("SHOP_INN"),
				"DESCR" => GetMessage("SHOP_INN_DESCR"),
				"VALUE" => "INN",
				"TYPE" => "TSZH"
			),
		"SHOP_KPP" => array(
				"NAME" => GetMessage("SHOP_KPP"),
				"DESCR" => GetMessage("SHOP_KPP_DESCR"),
				"VALUE" => "KPP",
				"TYPE" => "TSZH"
			),
		"BANK_ACCOUNT" => array(
				"NAME" => GetMessage("BANK_ACCOUNT"),
				"DESCR" => GetMessage("BANK_ACCOUNT_DESCR"),
				"VALUE" => "RSCH",
				"TYPE" => "TSZH"
			),
		"BANK_NAME" => array(
				"NAME" => GetMessage("BANK_NAME"),
				"DESCR" => GetMessage("BANK_NAME_DESCR"),
				"VALUE" => "BANK",
				"TYPE" => "TSZH"
			),
		"BANK_BIK" => array(
				"NAME" => GetMessage("BANK_BIK"),
				"DESCR" => GetMessage("BANK_BIK_DESCR"),
				"VALUE" => "BIK",
				"TYPE" => "TSZH"
			),
		"SCID" => array(
				"NAME" => GetMessage("SCID"),
				"DESCR" => GetMessage("SCID_DESCR"),
				"VALUE" => "5512",
				"TYPE" => ""
			),
		"CPPID" => array(
            "NAME" => GetMessage("CPPID"),
            "DESCR" => GetMessage("CPPID_DESCR"),
            "VALUE" => "CPPID",
            "TYPE" => ""
        ),
		"SERVER" => array(
            "NAME" => GetMessage("SERVER"),
            "DESCR" => GetMessage("SERVER_DESCR"),
            "VALUE" => "SERVER",
            "TYPE" => ""
        ),
        "LOG" => array(
            "NAME" => GetMessage("LOG"),
            "DESCR" => GetMessage("LOG_DESCR"),
            "VALUE" => "LOG",
            "TYPE" => ""
        ),
        "PASS" => array(
            "NAME" => GetMessage("PASS"),
            "DESCR" => GetMessage("PASS_DESCR"),
            "VALUE" => "PASS",
            "TYPE" => ""
        ),
		"ORDER_ID" => array(
				"NAME" => GetMessage("ORDER_ID"),
				"DESCR" => GetMessage("ORDER_ID_DESCR"),
				"VALUE" => "ID",
				"TYPE" => "PAYMENT"
			),
		"ORDER_DATE" => array(
				"NAME" => GetMessage("ORDER_DATE"),
				"DESCR" => GetMessage("ORDER_DATE_DESCR"),
				"VALUE" => "DATE_INSERT",
				"TYPE" => "PAYMENT"
			),
		"SHOULD_PAY" => array(
				"NAME" => GetMessage("SHOULD_PAY"),
				"DESCR" => GetMessage("SHOULD_PAY_DESCR"),
				"VALUE" => "SHOULD_PAY",
				"TYPE" => "PAYMENT"
			),
		"ORDER_DESCR" => array(
				"NAME" => GetMessage("ORDER_DESCR"),
				"DESCR" => GetMessage("ORDER_DESCR_DESCR"),
				"VALUE" => GetMessage("ORDER_DESCR_VALUE"),
				"TYPE" => ""
			),
		"CUSTOMER_FIO" => array(
				"NAME" => GetMessage("CUSTOMER_FIO"),
				"DESCR" => GetMessage("CUSTOMER_FIO_DESCR"),
				"VALUE" => "NAME",
				"TYPE" => "ACCOUNT"
			),
		"CUSTOMER_ADDRESS" => array(
				"NAME" => GetMessage("CUSTOMER_ADDRESS"),
				"DESCR" => GetMessage("CUSTOMER_ADDRESS_DESCR"),
				"VALUE" => "FULL_ADDRESS",
				"TYPE" => "ACCOUNT"
			),
/*		"NOTIFY_URL" => array(
				"NAME" => GetMessage("NOTIFY_URL"),
				"DESCR" => GetMessage("NOTIFY_URL_DESCR"),
				"VALUE" => "",
				"TYPE" => ""
			),*/
		"SUCESS_URL" => array(
				"NAME" => GetMessage("SUCESS_URL"),
				"DESCR" => GetMessage("SUCESS_URL_DESCR"),
				"VALUE" => "",
				"TYPE" => ""
			),
	);
?>