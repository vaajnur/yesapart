<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
__IncludeLang(GetLangFileName(dirname(__FILE__)."/", "/".basename(__FILE__)));

$funcHtmlspecialcharsArray = function ($ar)
{
	$arResult = array();

	foreach($ar as $key => $value)
		$arResult[$key] = htmlspecialchars($value);

	return $arResult;
};

$Sum = CTszhPaySystem::GetParamValue("SHOULD_PAY");
$scid = CTszhPaySystem::GetParamValue("SCID");
$pass = CTszhPaySystem::GetParamValue("PASS");
$serv = CTszhPaySystem::GetParamValue("SERVER");
$shopid = CTszhPaySystem::GetParamValue("CPPID");
$orderID = CTszhPaySystem::GetParamValue("ORDER_ID");
$orderDate = CTszhPaySystem::GetParamValue("ORDER_DATE");
$Sum = number_format($Sum, 2, '.', '');


$tszhName = htmlspecialchars(CTszhPaySystem::GetParamValue("SHOP_NAME"));
$tszhINN = htmlspecialchars(CTszhPaySystem::GetParamValue("SHOP_INN"));
$tszhKPP = htmlspecialchars(CTszhPaySystem::GetParamValue("SHOP_KPP"));
$tszhBankAccount = htmlspecialchars(CTszhPaySystem::GetParamValue("BANK_ACCOUNT"));
$tszhBankName = htmlspecialchars(CTszhPaySystem::GetParamValue("BANK_NAME"));
$tszhBankBIK = htmlspecialchars(CTszhPaySystem::GetParamValue("BANK_BIK"));

$customerFIO = htmlspecialchars(CTszhPaySystem::GetParamValue("CUSTOMER_FIO"));
$customerAddress = htmlspecialchars(CTszhPaySystem::GetParamValue("CUSTOMER_ADDRESS"));

$successURL = htmlspecialchars(CTszhPaySystem::GetParamValue("SUCESS_URL"));

if (is_array($GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]) && !empty($GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]))
	$arPayment = $GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"];
else
	$arPayment = CTszhPayment::GetByID($orderID);

$paySystemID = $arPayment["PAY_SYSTEM_ID"];

$arSite = CSite::GetByID($arPayment["LID"])->Fetch();
$serverName = $arSite['SERVER_NAME'];
if (!$serverName)
{
	$serverName = COption::GetOptionString('main', 'server_name', $_SERVER['HTTP_HOST']);
	if (!$serverName)
	{
		ShowError(GetMessage('PYM_ERROR_SERVER_NAME'));
		return;
	}
}
$notifyURL = htmlspecialchars(/*CTszhPaySystem::GetParamValue("NOTIFY_URL")*/"http://{$serverName}/payment_receive.php");

if (is_array($GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]) && !empty($GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]))
	$arAccount = $GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"];
else
	$arAccount = CTszhAccount::GetByID($arPayment["ACCOUNT_ID"]);
$arAccount = $funcHtmlspecialcharsArray($arAccount);

$paymentDescr = str_replace('#ACCOUNT_NUMBER#', $GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]["XML_ID"], CTszhPaySystem::GetParamValue("ORDER_DESCR"));

$arPeriods = array();
for ($i=-1; $i<2; $i++)
{
	$ts = strtotime("{$i} month");
	$arPeriods[] = GetMessage('P_M_' . date('m', $ts)) . ' ' . date('Y', $ts);
}

$paymentDescr = str_replace(array('#PREV_MONTH#', '#CUR_MONTH#', '#NEXT_MONTH#'), $arPeriods, $paymentDescr);
?>
<script type="text/javascript">
	function __YandexMoneyOnSubmit()
	{
		document.__YandexMoneyForm.payment_purpose.value = document.__YandexMoneyForm.narrative.value;
		return true;
	}
</script>

<form name="__YandexMoneyForm" action="<?=$serv?>" method="post" target="_blank" onsubmit="return __YandexMoneyOnSubmit();">

<font class="tablebodytext">
<?=GetMessage("PYM_TITLE")?><br><br>
<?=GetMessage("PYM_ORDER")?> <?=$orderID?> <?=GetMessage("PYM_FROM")?> <?=$orderDate?><br>
<?=GetMessage("PYM_TO_PAY")?> <b><?=CTszhPayment::FormatCurrency(CTszhPaySystem::GetParamValue("SHOULD_PAY"), $arPayment["CURRENCY"])?></b>

<p>
<label for="narrative"><?=GetMessage("PYM_F_ORDER_DESCR")?>:</label> <input type="text" name="narrative" id="narrative" value="<?=$paymentDescr?>" size="50"><br>

<input type="hidden" name="scid" value="<?=$scid?>">
<input type="hidden" name="shopid" value="<?=$shopid?>">
<input type="hidden" name="pass" value="<?=$pass?>">

<input type="hidden" name="supplierName" value="<?=$tszhName?>">
<input type="hidden" name="CustName" value="<?=$tszhName?>">

<input type="hidden" name="supplierInn" value="<?=$tszhINN?>">
<input type="hidden" name="CustINN" value="<?=$tszhINN?>">

<input type="hidden" name="CustKPP" value="<?=$tszhKPP?>">

<input type="hidden" name="supplierBankAccount" value="<?=$tszhBankAccount?>">
<input type="hidden" name="CustAccount" value="<?=$tszhBankAccount?>">

<input type="hidden" name="supplierBankName" value="<?=$tszhBankName?>">

<?/*<input type="hidden" name="BankINN" value="<?=$arTszh["INN"]?>">*/?>

<input type="hidden" name="supplierBankBik" value="<?=$tszhBankBIK?>">
<input type="hidden" name="BankBIK" value="<?=$tszhBankBIK?>">

<input type="hidden" name="payerName" value="<?=$customerFIO?>">
<input type="hidden" name="payerAddress" value="<?=$customerAddress?>">

<input type="hidden" name="payment_purpose" value="">

<input type="hidden" name="netSum" value="<?=$Sum?>">
<input type="hidden" name="sum" value="<?=$Sum?>">

<input type="hidden" name="budgetDocNumber" value="0">
<?//<input type="hidden" name="WDRegistry_15" value="30">?>

<input type="hidden" name="orderID" value="<?=$orderID?>">

<input type="hidden" name="notifyUrl" value="<?=$notifyURL?>">
<input type="hidden" name="shopSuccessURL" value="<?=$successURL?>">

<input type="hidden" name="paySystemID" value="<?=$paySystemID?>">

<input type="submit" value="<?=GetMessage("PYM_BUTTON")?>">

</p>
</font>

<p><font class="tablebodytext"><?=GetMessage("PYM_WARN")?></font></p>
</form>
