<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?
include(GetLangFileName(dirname(__FILE__)."/", "/assist.php"));
//return url
//price format
$SERVER_NAME_tmp = "";
if (defined("SITE_SERVER_NAME"))
	$SERVER_NAME_tmp = SITE_SERVER_NAME;
if (strlen($SERVER_NAME_tmp)<=0)
	$SERVER_NAME_tmp = COption::GetOptionString("main", "server_name", "");

$dateInsert = (strlen(CTszhPaySystem::GetParamValue("DATE_INSERT")) > 0) ? CTszhPaySystem::GetParamValue("DATE_INSERT") : $GLOBALS["SALE_INPUT_PARAMS"]["PAYMENT"]["DATE_INSERT"];
$orderID = (strlen(CTszhPaySystem::GetParamValue("ORDER_ID")) > 0) ? CTszhPaySystem::GetParamValue("ORDER_ID") : $GLOBALS["SALE_INPUT_PARAMS"]["PAYMENT"]["ID"];
$shouldPay = (strlen(CTszhPaySystem::GetParamValue("SHOULD_PAY")) > 0) ? CTszhPaySystem::GetParamValue("SHOULD_PAY") : $GLOBALS["SALE_INPUT_PARAMS"]["PAYMENT"]["SHOULD_PAY"];
$currency = (strlen(CTszhPaySystem::GetParamValue("CURRENCY")) > 0) ? CTszhPaySystem::GetParamValue("CURRENCY") : $GLOBALS["SALE_INPUT_PARAMS"]["PAYMENT"]["CURRENCY"];
$sucUrl = (strlen(CTszhPaySystem::GetParamValue("SUCCESS_URL")) > 0) ? CTszhPaySystem::GetParamValue("SUCCESS_URL") : "http://".$SERVER_NAME_tmp;
$failUrl = (strlen(CTszhPaySystem::GetParamValue("FAIL_URL")) > 0) ? CTszhPaySystem::GetParamValue("FAIL_URL") : "http://".$SERVER_NAME_tmp;

$bDemo = strlen(CTszhPaySystem::GetParamValue("DEMO")) > 0;
$url = 'https://payments.paysecure.ru/pay/order.cfm'; 

?>
<FORM ACTION="<?=$url?>" METHOD="POST" target="_blank">
<font class="tablebodytext">
<?echo GetMessage("SASP_PROMT")?><br>
<?echo GetMessage("SASP_ACCOUNT_NO")?> <?= $orderID.GetMessage("SASP_ORDER_FROM").$dateInsert ?><br>
<?echo GetMessage("SASP_ORDER_SUM")?> <b><?echo CTszhPayment::FormatCurrency($shouldPay, $currency) ?></b><br>
<br>
<INPUT TYPE="HIDDEN" NAME="Merchant_ID" VALUE="<?= (CTszhPaySystem::GetParamValue("SHOP_IDP")) ?>">
<INPUT TYPE="HIDDEN" NAME="OrderNumber" VALUE="<?= $orderID ?>">
<INPUT TYPE="HIDDEN" NAME="Delay" VALUE="0">
<?if ($bDemo):?><input type="hidden" name="TestMode" value="1" /><?endif;?>
<INPUT TYPE="HIDDEN" NAME="OrderComment" VALUE="Invoice <?= $orderID." (".$dateInsert.")" ?>">
<INPUT TYPE="HIDDEN" NAME="OrderAmount" VALUE="<?= (str_replace(",", ".", $shouldPay)) ?>">
<INPUT TYPE="HIDDEN" NAME="OrderCurrency" VALUE="<?=(($currency == "RUB") ? "RUR" :($currency)) ?>">
<INPUT TYPE="HIDDEN" NAME="LastName" VALUE="<?= (CTszhPaySystem::GetParamValue("LAST_NAME")) ?>">
<INPUT TYPE="HIDDEN" NAME="FirstName" VALUE="<?= (CTszhPaySystem::GetParamValue("FIRST_NAME")) ?>">
<INPUT TYPE="HIDDEN" NAME="MiddleName" VALUE="<?= (CTszhPaySystem::GetParamValue("MIDDLE_NAME")) ?>">
<INPUT TYPE="HIDDEN" NAME="Email" VALUE="<?= (CTszhPaySystem::GetParamValue("EMAIL")) ?>">
<INPUT TYPE="HIDDEN" NAME="Address" VALUE="<?= (CTszhPaySystem::GetParamValue("ADDRESS")) ?>">
<INPUT TYPE="HIDDEN" NAME="MobilePhone" VALUE="<?= (CTszhPaySystem::GetParamValue("PHONE")) ?>">
<INPUT TYPE="HIDDEN" NAME="URL_RETURN_OK" VALUE="<?= (CTszhPaySystem::GetParamValue("SUCCESS_URL")) ?>">
<INPUT TYPE="HIDDEN" NAME="URL_RETURN_NO" VALUE="<?= (CTszhPaySystem::GetParamValue("FAIL_URL")) ?>">
<?/*<INPUT TYPE="HIDDEN" NAME="Language" VALUE="0">*/?>

<INPUT TYPE="SUBMIT" NAME="Submit" VALUE="<?echo GetMessage("SASP_ACTION")?>">
</font>
</form>

<p align="justify"><font class="tablebodytext"><b><?echo GetMessage("SASP_NOTES_TITLE")?></b></font></p>
<p align="justify"><font class="tablebodytext"><?echo GetMessage("SASP_NOTES")?></font></p>
<p align="justify"><font class="tablebodytext"><b><?echo GetMessage("SASP_NOTES_TITLE1")?></b></font></p>
<p align="justify"><font class="tablebodytext"><?echo GetMessage("SASP_NOTES1")?></font></p>
