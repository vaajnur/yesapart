<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><form id="pay" name="pay" method="POST" action="https://merchant.webmoney.ru/lmi/payment.asp">
	<input type="hidden" name="LMI_PAYMENT_AMOUNT" value="<?= CTszhPaySystem::GetParamValue("SHOULD_PAY") ?>">
	<input type="hidden" name="LMI_PAYMENT_DESC" value="Платеж <?= CTszhPaySystem::GetParamValue("ORDER_ID") ?> от <?= htmlspecialchars(CTszhPaySystem::GetParamValue("DATE_INSERT")) ?>">
	<input type="hidden" name="LMI_PAYMENT_NO" value="<?= CTszhPaySystem::GetParamValue("ORDER_ID") ?>">
	<input type="hidden" name="LMI_PAYEE_PURSE" value="<?= htmlspecialchars(CTszhPaySystem::GetParamValue("SHOP_ACCT")) ?>">
	<input type="hidden" name="LMI_SIM_MODE" value="<?= htmlspecialchars(CTszhPaySystem::GetParamValue("TEST_MODE")) ?>">
	<input type="hidden" name="LMI_RESULT_URL" value="<?=CTszhPaySystem::GetParamValue("RESULT_URL")?>">
	<input type="hidden" name="LMI_SUCCESS_URL" value="<?=CTszhPaySystem::GetParamValue("SUCCESS_URL")?>">
	<input type="hidden" name="LMI_FAIL_URL" value="<?=CTszhPaySystem::GetParamValue("FAIL_URL")?>">
	<input type="hidden" name="LMI_SUCCESS_METHOD" value="1">
	<input type="hidden" name="LMI_FAIL_METHOD" value="1">
	<input type="submit" value="Оплатить">
</form>