<?

$MESS ['TSZH_PAYMENT_ERROR_NO_ID'] = "Неверно указан ID платежа";
$MESS ['TSZH_PAYMENT_ERROR_WRONG_USER_ID'] = "ID пользователя указан неверно, либо пользователь не существует";
$MESS ['TSZH_PAYMENT_ERROR_WRONG_ACCOUNT_ID'] = "ID лицевого счета указан неверно, либо лицевой счет не существует";
$MESS ["TSZH_PAYMENT_ERROR_WRONG_TSZH_ID"] = "ID объекта управления указано не верно";
$MESS ['TSZH_PAYMENT_ERROR_NO_LID'] = "Не указана привязка к сайту";
$MESS ['TSZH_PAYMENT_ERROR_LID_DOESNT_EXISTS'] = "Указанный сайт не существует";
$MESS ['TSZH_PAYMENT_ERROR_NO_SUMM'] = "Не указана сумма к оплате";
$MESS ['TSZH_PAYMENT_ERROR_WRONG_SUMM'] = "Сумма к оплате указана неверно";
$MESS ['TSZH_PAYMENT_ERROR_WRONG_SUMM_PAYED'] = "Оплаченная сумма указана неверно";
$MESS ['TSZH_PAYMENT_ERROR_NO_CURRENCY'] = "Не указана валюта";
$MESS ['TSZH_PAYMENT_ERROR_NO_PAY_SYSTEM'] = "Не указана платежная система";
$MESS ['TSZH_PAYMENT_ERROR_PAY_SYSTEM_DOESNT_EXISTS'] = "Указанная платежная система не существует";
$MESS ['TSZH_PAYMENT_ERROR_WRONG_ID'] = "Неверно указан ID платежа";

$MESS ['TSZH_PAYMENT_NO_PAYMENT_ID'] = "Не указан ID платежа";
$MESS ['TSZH_PAYMENT_NOT_FOUND'] = "Платеж не найден";
$MESS ['TSZH_PAYMENT_DUB_PAY'] = "Платеж уже оплачен";

$MESS ['TSZH_PAYMENT_RUB_ABBR'] = "руб.";
$MESS ["TSZH_PAYMENT_UAH_ABBR"] = "грн.";

?>