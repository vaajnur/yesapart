<?
$MESS["SKGPS_EMPTY_NAME"]="Не установлено название платежной системы";
$MESS["SKGPS_EMPTY_CURRENCY"]="Не установлена валюта платежной системы";
$MESS["SKGPS_NO_SITE"]="Сайт с кодом #ID# не найден";
$MESS["SKGPS_NO_TSZH"]="Объект управления с кодом #ID# не найден";

$MESS["SKGPSA_NO_CODE"]="Не установлен код платежной системы";
$MESS["SKGPSA_NO_ID_TYPE"]="Не установлен код типа плательщика";
$MESS["SKGPSA_NO_PS"]="Платежная система с кодом #ID# не найдена";
$MESS["SKGPSA_NO_PERS_TYPE"]="Тип плательщика с кодом #ID# не найден";

$MESS["SKGPSA_YANDEX"] = "Яндекс.Деньги";
$MESS["SKGPSA_YANDEX_DESC"] = '<p><img src="/bitrix/images/citrus.tszhpayment/yandex_money.png" alt="Яндекс.Деньги" width="137" height="60">&nbsp;<img src="/bitrix/images/citrus.tszhpayment/cards.jpg" alt="Банковские карты" width="313" height="50"></p>
<p>Оплата банковской картой или&nbsp;Яндекс.Деньгами через&nbsp;<a href="https://money.yandex.ru/legal/payee.xml" target="_blank" rel="nofollow">сервис приема платежей Яндекса</a>.</p>
<p>Комиссия: 2%, но не&nbsp;менее 30&nbsp;рублей.<br>Срок зачисления: 2—3 рабочих дня.</p>';
$MESS["SKGPSA_YANDEX_ORDER_DESCR"] = "за услуги ЖКХ по л/с № #ACCOUNT_NUMBER# за #PREV_MONTH#";

$MESS["SKGPSA_MONETA"] = "Монета.ру";
$MESS["SKGPSA_MONETA_DESC"] = "<p><img src=\"/bitrix/images/citrus.tszhpayment/visa.png\" width=\"80\" height=\"49\"><img src=\"/bitrix/images/citrus.tszhpayment/mastercard.png\" width=\"80\" height=\"49\"><img src=\"/bitrix/images/citrus.tszhpayment/mir.png\" height=\"49\"></p><p style=\"font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 12px; line-height: 16px;\">Вы можете оплатить услуги <a title=\"Жилищно-коммунальное хозяйство\" lang=\"ru\" >ЖКХ с помощью банковских карт MasterCard, Visa, МИР любых банков и электронных денежных средств.<br>Перевод денежных средств осуществляется</a> <a href=\"http://moneta.ru/\" target=\"_blank\">НКО &laquo;МОНЕТА&raquo; (ООО)</a> (лицензия ЦБ РФ № <nobr>3508-К</nobr>).</p>";

$MESS["CITRUS_TSZHPAYMENT_EXPORT"] = "Выгрузка платежей";

$MESS ["TSZH_PAYMENT_MONETA_FOOTER_TITLE"] = "Мы принимаем банковские карты";
$MESS ["TSZH_PAYMENT_MONETA_FOOTER_TEXT"] = "Сервис оплаты предоставлен<br>НКО «МОНЕТА» (ООО)";

?>