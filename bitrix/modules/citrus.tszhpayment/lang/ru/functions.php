<?
$MESS["MUT_HEADER"] = "Сравнение способов подключения оплат";
$MESS["MUT_VARIANTS"] = "Варианты подключения";
$MESS["MUT_SCHEME1"] = "1 схема";
$MESS["MUT_SCHEME2"] = "2 схема";
$MESS["MUT_DOGOVOR"] = "Заключение договора";
$MESS["MUT_PERSONAL_AREA"] = "Личный кабинет";
$MESS["MUT_COMMISSION"] = "Комиссия";
$MESS["MUT_COMMISSION_PAYEE"] = "Берется с получателя";
$MESS["MUT_COMMISSION_PAYER"] = "Берется с плательщика";
$MESS["MUT_TRANSFER"] = "Перевод денег на р/с";
$MESS["MUT_BY_TEMPLATE"] = "Настраивается по шаблону";
$MESS["MUT_DAILY"] = "Ежедневно";
$MESS["MUT_PAY_WAYS"] = "Способы оплаты";
$MESS["MUT_PAY_SYSTEMS"] = "VISA, Mastercard, Яндекс-<br>деньги, Монета";
$MESS["MUT_PAY_OTHERS"] = "и другие";
$MESS["MUT_PAY_WAYS_20"] = "(20+ способов оплаты)";
$MESS["MUT_ACTIVITY"] = "Активность";
$MESS["CITRUS_TSZH_PAYMENT_MONETA_OFFER"] = "Подтверждаю свое согласие на&nbsp;присоединении к&nbsp;<a href=\"https://moneta.ru/info/public/merchants/b2boffer.pdf\" target=\"_blank\">Договору НКО &laquo;МОНЕТА&raquo; (ООО)</a>, с&nbsp;<a href=\"https://www.moneta.ru/info/public/merchants/b2btariffs.pdf\" target=\"_blank\">Тарифами</a> и&nbsp;<a href=\"#\" style=\"text-decoration: none; border-bottom: 1px dashed #2770ba\" onclick=\"monetaProcedurePopup.show(); return false;\">Порядком действий</a> ознакомлен и&nbsp;согласен";
$MESS["CITRUS_TSZH_PAYMENT_MONETA_PROCEDURE"] = "
<p><strong>Порядок действий</strong></p>
<ol>
	<li><p>На&nbsp;указанный Вами при установке <nobr>e-mail</nobr> (<span class=\"moneta-email\">#EMAIL#</span>) будет направлено письмо с&nbsp;реквизитами доступа в&nbsp;Личный кабинет НКО &laquo;МОНЕТА&raquo; (ООО). Войдите в&nbsp;Личный кабинет и&nbsp;заполните все требуемые поля. После первого входа рекомендуем изменить пароль.</p></li>
	<li><p>После заполнения и&nbsp;подтверждения всех обязательных полей в&nbsp;формах Личного кабинета&nbsp;&mdash; будет автоматически сформировано Заявление о&nbsp;присоединении к&nbsp;условиям договора. Скачайте Заявление в&nbsp;разделе &laquo;Документы&raquo;, распечатайте, подпишите и&nbsp;отправьте в&nbsp;наш адрес &laquo;Заявление о&nbsp;присоединении&raquo; по&nbsp;адресу: <i>424020, Российская Федерация, Республика Марий Эл, г.&nbsp;<nobr>Йошкар-Ола</nobr>, ул.&nbsp;Анциферова, дом 7, корпус &laquo;А&raquo; для НКО &laquo;МОНЕТА&raquo; (ООО)</i>, телефон для курьерской службы <nobr>(8362) 45-43-83</nobr>.</p></li>
	<li><p>По&nbsp;умолчанию доступны следующие способы: <i>банковские карты</i>, <i>Монета.ру</i>, <i>Яндекс.Деньги</i>. Если в&nbsp;течение 30 календарных дней не&nbsp;будет получено письменное подтверждение согласия с&nbsp;условиями Договора НКО &laquo;МОНЕТА&raquo; (ООО), он&nbsp;может быть расторгнут.</p></li>
</ol>
<p><small>Примечание: Денежные средства перечисляются на&nbsp;ваш банковский счет не&nbsp;позднее первого рабочего дня, следующего за&nbsp;днем получения НКО &laquo;МОНЕТА&raquo; (ООО) распоряжения Плательщика. Вознаграждение за&nbsp;осуществление Переводов указано в&nbsp;Тарифах. В&nbsp;случае вашего присоединения к&nbsp;Договору НКО &laquo;МОНЕТА&raquo; (ООО): комиссия НКО &laquo;МОНЕТА&raquo; (ООО) удерживается из&nbsp;суммы, подлежащей перечислению на&nbsp;ваш расчетный счет; комиссия с&nbsp;Плательщика не&nbsp;удерживается.<br><br>Расторжение Договора с НКО «МОНЕТА» (ООО) возможно путем направления соответствующего письменного уведомления.</small></p>
";
$MESS["CITRUS_TSZH_NEED_EMAIL"] = "<span style=\"color: #000; font-weight: bold;\">укажите email!</span>";
$MESS["CITRUS_TSZH_PAYMENT_CLOSE"] = "Закрыть";

$MESS["MUT_DEFAULT"] = "По умолчанию";

$MESS["MUT_MONETA_DEMO"] = "Платёжный сервис \"НКО \"МОНЕТА\" (ООО) будет доступен только после приобретения продукта «1С: Сайт ЖКХ».";
?>