<?
$MESS["CITRUS_TSZHPAYMENT_RECEIVE_TITLE"] = "Получение оплаты";
$MESS["CITRUS_TSZHPAYMENT_RECEIVE_TEXT"] = "Вызывается при получении подтверждения платежа от платежной системы, либо установки галочки «Оплачен» вручную для платежных систем, которые не поддерживают уведомления о принятой оплате

#ID# — Номер платежа
#DATE_INSERT# — Дата создания платежа
#TSZH# — Объект управления (организация)
#PAYSYSTEM# — Способ оплаты
#ACCOUNT# — Номер лицевого счета
#SUMM# — Сумма платежа

* могут использоваться любые поля платежа

";

$MESS["CITRUS_TSZHPAYMENT_RECEIVE_SUBJECT1"] = "#SITE_NAME#: Платеж №#ID# на сумму #SUMM# подтвержден";
$MESS["CITRUS_TSZHPAYMENT_RECEIVE_MESSAGE1"] = "Информационное сообщение сайта #SITE_NAME# (http://#SERVER_NAME#)
------------------------------------------

Платеж на сумму #SUMM# подтвержден

Дата платежа: #DATE_INSERT#
Организация: #TSZH#
Способ оплаты: #PAYSYSTEM#
Номер лицевого счета: #ACCOUNT#
Адрес: #FULL_ADDRESS#
Оплачено: #SUMM_PAYED#

Подробная информация: http://#SERVER_NAME#/bitrix/admin/tszh_payments_edit.php?ID=#ID#&lang=ru

Сообщение сгенерировано автоматически.";
$MESS["CITRUS_TSZHPAYMENT_RECEIVE_SUBJECT2"] = "#SITE_NAME#: Ваш платеж №#ID# на сумму #SUMM# принят";
$MESS["CITRUS_TSZHPAYMENT_RECEIVE_MESSAGE2"] = "Информационное сообщение сайта #SITE_NAME# (http://#SERVER_NAME#)
------------------------------------------

Платеж на сумму #SUMM# подтвержден

Дата платежа: #DATE_INSERT#
Организация: #TSZH#
Способ оплаты: #PAYSYSTEM#
Номер лицевого счета: #ACCOUNT#
Адрес: #FULL_ADDRESS#
Оплачено: #SUMM_PAYED#

Сообщение сгенерировано автоматически.";

?>