<?
$MESS['C_TSZH_PAYMENT_MODULE_NAME'] = "1С: Сайт ЖКХ. Оплата";
$MESS['C_TSZH_PAYMENT_MODULE_DESCRIPTION'] = "Модуль оплаты для сайта ТСЖ позволяет организовать прием оплаты за коммунальные услуги на сайте ТСЖ";
$MESS['C_MODULE_PARTNER_NAME'] = "1С-Рарус Тиражные решения";
$MESS['C_MODULE_PARTNER_URI'] = "http://otr-soft.ru/";
$MESS ['TSZH_PAYMENT_INSTALL_TITLE'] = "Установка модуля «1С: Сайт ЖКХ. Оплата»";
$MESS ['CITRUS_TSZHPAYMENT_ERROR_INSTALLING_EVENT_TYPES'] = "Произошла ошибка при установке типов почтовых событий. ";
$MESS ['CITRUS_TSZHPAYMENT_ERROR_INSTALLING_EVENT_MESSAGES'] = "Произошла ошибка при установке почтовых шаблонов. ";
?>