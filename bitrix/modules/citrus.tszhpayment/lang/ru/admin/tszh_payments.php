<?

$MESS ['TSZH_PAYMENTS_ERROR_DELETE'] = "Ошибка при удалении платежа";
$MESS ['TSZH_PAYMENTS_ERROR_UPDATE'] = "Ошибка при изменении платежа";
$MESS ['TSZH_PAYMENTS_NAV'] = "Платежи";

$MESS ['TSZH_PAYMENT_ID'] = "ID";
$MESS ['TSZH_PAYMENT_LID'] = "Сайт";
$MESS ['TSZH_PAYMENT_ALL'] = "все";
$MESS ['TSZH_PAYMENT_PAYED'] = "Оплачено";
$MESS ['TSZH_PAYMENT_DATE_PAYED'] = "Дата оплаты";
$MESS ['TSZH_PAYMENT_EMP_PAYED_ID'] = "Сотрудник, подтвердивший платеж";
$MESS ['TSZH_PAYMENT_SUMM'] = "Сумма";
$MESS ['TSZH_PAYMENT_SUMM_PAYED'] = "Оплаченная сумма";
$MESS ['TSZH_PAYMENT_CURRENCY'] = "Валюта";
$MESS ['TSZH_PAYMENT_USER_ID'] = "Пользователь";
$MESS ['TSZH_PAYMENT_ACCOUNT_ID'] = "Лицевой счет";
$MESS ['TSZH_PAYMENT_PAY_SYSTEM_ID'] = "Платежная система";
$MESS ['TSZH_PAYMENT_PSYS_STATUS'] = "Код статуса";
$MESS ['TSZH_PAYMENT_PSYS_STATUS_DESCR'] = "Описание статуса";
$MESS ['TSZH_PAYMENT_PSYS_STATUS_RESPONSE'] = "Ответ на запрос статуса";
$MESS ['TSZH_PAYMENT_PSYS_STATUS_DATE'] = "Дата получения статуса";
$MESS ['TSZH_PAYMENT_INSURANCE_INCLUDED'] = "Включено добровольное страхование";
$MESS ['TSZH_PAYMENT_DATE_INSERT'] = "Создано";
$MESS ['TSZH_PAYMENT_DATE_UPDATE'] = "Изменено";

$MESS ['TSZH_PAYMENT_IS_OVERHAUL'] = "Кап. ремонт";
$MESS ['TSZH_PAYMENT_IS_PENALTY'] = "Пени";

$MESS ['TSZH_PAYMENT_YES'] = "Да";
$MESS ['TSZH_PAYMENT_NO'] = "Нет";
$MESS ['TSZH_PAYMENT_FROM'] = "с";
$MESS ['TSZH_PAYMENT_TO'] = "по";

$MESS ['TSZH_PAYMENTS_EDIT_DESCR'] = "Изменить платеж";
$MESS ['TSZH_PAYMENTS_EDIT'] = "Изменить";
$MESS ['TSZH_PAYMENTS_DELETE'] = "Удалить";
$MESS ['TSZH_PAYMENTS_DELETE_DESCR'] = "Удалить запись о платеже";
$MESS ['TSZH_PAYMENTS_CONFIRM_DEL_MESSAGE'] = "Вы действительно хотите удалить платеж?";

$MESS ['MAIN_WRONG_DATE_PAYED_FROM'] = "Введите в фильтре правильную дату оплаты \"c\"";
$MESS ['MAIN_WRONG_DATE_PAYED_TILL'] = "Введите в фильтре правильную дату оплаты \"по\"";
$MESS ['MAIN_WRONG_DATE_PAYED_FROM_TILL'] = "Дата оплаты \"по\" должна быть больше чем дата \"с\"";
$MESS ['MAIN_WRONG_DATE_INSERT_FROM'] = "Введите в фильтре правильную дату создания \"c\"";
$MESS ['MAIN_WRONG_DATE_INSERT_TILL'] = "Введите в фильтре правильную дату создания \"по\"";
$MESS ['MAIN_WRONG_DATE_INSERT_FROM_TILL'] = "Дата создания \"по\" должна быть больше чем дата \"с\"";
$MESS ['MAIN_WRONG_DATE_UPDATE_FROM'] = "Введите в фильтре правильную дату изменения \"c\"";
$MESS ['MAIN_WRONG_DATE_UPDATE_TILL'] = "Введите в фильтре правильную дату изменения \"по\"";
$MESS ['MAIN_WRONG_DATE_UPDATE_FROM_TILL'] = "Дата изменения \"по\" должна быть больше чем дата \"с\"";
$MESS ['MAIN_WRONG_PSYS_STATUS_DATE_FROM'] = "Введите в фильтре правильную дату получения статуса \"c\"";
$MESS ['MAIN_WRONG_PSYS_STATUS_DATE_TILL'] = "Введите в фильтре правильную дату получения статуса \"по\"";
$MESS ['MAIN_WRONG_PSYS_STATUS_DATE_FROM_TILL'] = "Дата получения статуса \"по\" должна быть больше чем дата \"с\"";

$MESS ['TSZH_PAYMENTS_SECTION_TITLE'] = "Платежи";

$MESS ['TSZH_PAYMENTS_WRONG_SUMMS'] = "Сумма платежа \"по\" должна быть не меньше суммы \"с\"";
$MESS ['TSZH_PAYMENTS_WRONG_SUMM_PAYEDS'] = "Оплаченная сумма \"по\" должна быть не меньше суммы \"с\"";
$MESS["CITRUS_TSZH_ACCOUNT_NUMBER"] = "№";

?>