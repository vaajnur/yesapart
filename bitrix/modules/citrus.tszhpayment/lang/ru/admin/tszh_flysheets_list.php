<?
$MESS ['TSZH_FLYSHEET_ID'] = "ID";
$MESS ['DESCRIPTION'] = "Описание";
$MESS ['IMG_SRC'] = "Листовка";
$MESS ['TSZH_PAYMENTS_SECTION_TITLE'] = "Рекламные листовки";
$MESS ['FLYSHEET_ADD_NEW_GENERATOR'] = "Генератор листовок Онлайн";
$MESS ['FLYSHEET_ADD_NEW'] = "Добавить";
$MESS ['FLYSHEET_ADD_NEW'] = "Добавить новую листовку";
$MESS ['FLYSHEET_ADD_NEW_ALT'] = "Добавить новую листовку вручную";
$MESS ['FLYSHEET_ADD_NEW_GENERATOR_ALT'] = "На сайте разработчика вы можете подготовить листовку с вашими данными";
$MESS ['FLYSHEET_ADD_NEW_GENERATOR_ALT_CLOSE'] = "закрыть";
$MESS ['TSZH_FLYSHEETS_DELETE'] = "Удалить";
$MESS ['TSZH_FLYSHEETS_EDIT'] = "Редактировать";
$MESS ['TSZH_DELETE_ERROR'] = "Ошибка удаления";
?>