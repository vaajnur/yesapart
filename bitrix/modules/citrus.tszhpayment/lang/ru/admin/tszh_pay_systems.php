<?

$MESS ['TSZH_PAYMENT_DELETE_ERROR'] = "Ошибка удаления платежной системы. Возможно по этой платежной системе уже были сделаны заказы.<br>";
$MESS ['TSZH_PAYMENT_SECTION_TITLE'] = "Платежные системы";
$MESS ['TSZH_PAYMENT_F_FILTER'] = "Фильтр";
$MESS ['TSZH_PAYMENT_F_LANG'] = "Сайт";
$MESS ['TSZH_PAYMENT_F_TSZH_ID'] = "Объект управления";
$MESS ['TSZH_PAYMENT_ALL'] = "все";
$MESS ['TSZH_PAYMENT_F_CURRENCY'] = "Валюта";
$MESS ['TSZH_PAYMENT_F_ACTIVE'] = "Активность";
$MESS ['TSZH_PAYMENT_F_NAME'] = "Название";
$MESS ['TSZH_PAYMENT_F_SORT'] = "Сортировка";
$MESS ['TSZH_PAYMENT_F_SUBMIT'] = "Установить";
$MESS ['TSZH_PAYMENT_F_DEL'] = "Сбросить";
$MESS ['TSZH_PAYMENT_ADD'] = "Добавить";
$MESS ['TSZH_PAYMENT_NAME'] = "Название";
$MESS ['TSZH_PAYMENT_LID'] = "Сайт";
$MESS ['TSZH_PAYMENT_TSZH_ID'] = "Объект управления";
$MESS ['TSZH_PAYMENT_H_CURRENCY'] = "Валюта";
$MESS ['TSZH_PAYMENT_ACTIVE'] = "Акт.";
$MESS ['TSZH_PAYMENT_SORT'] = "Сорт.";
$MESS ['TSZH_PAYMENT_ACTION'] = "Действия";
$MESS ['TSZH_PAYMENT_EDIT'] = "Изменить";
$MESS ['TSZH_PAYMENT_CONFIRM_DEL_MESSAGE'] = "Вы уверены, что хотите удалить платежную систему? Платежные системы, к которым привязаны заказы, удалить невозможно.";
$MESS ['TSZH_PAYMENT_DELETE'] = "Удалить";
$MESS ['TSZH_PAYMENT_PRLIST'] = "Платежные системы";
$MESS ['TSZH_PAYMENT_YES'] = "Да";
$MESS ['TSZH_PAYMENT_NO'] = "Нет";
$MESS ['TSZH_PAYMENT_EDIT_DESCR'] = "Изменить параметры платежной системы";
$MESS ['TSZH_PAYMENT_DELETE_DESCR'] = "Удалить платежную систему";
$MESS ['SPS_ADD_NEW'] = "Добавить &gt;&gt;";
$MESS ['SPS_YES'] = "да";
$MESS ['SPS_NO'] = "нет";
$MESS ['SPSAN_ERROR_DELETE'] = "Ошибка удаления записи";
$MESS ['SPSAN_ERROR_UPDATE'] = "Ошибка изменения записи";
$MESS ['SPSAN_ADD_NEW'] = "Добавить платежную систему";
$MESS ['SPSAN_ADD_NEW_ALT'] = "Нажмите для добавления новой платежной системы";

$MESS ['TSZH_PAYMENT_TSZH_EDIT_TITLE'] = "Перейти к редактированию объекта управления";
?>