<?
$MESS["CITRUS_TSZHPAYMENTS_CITRUS_TSZH_MODULE_NOT_FOUND"] = "Модуль «1С:Сайт ЖКХ» не найден";
$MESS ['TE_ERROR_EXPORT'] = "При выгрузке произошла ошибка.";
$MESS ['TE_PAGE_TITLE'] = "Выгрузка платежей";
$MESS ['TE_TAB1'] = "Настройки выгрузки";
$MESS ['TE_TAB1_TITLE'] = "Настройки выгрузки";
$MESS ['TE_TAB2'] = "Выгрузка";
$MESS ['TE_TAB2_TITLE'] = "Выгрузка";
$MESS ['TE_EXPORT_DONE'] = "Выгрузка завершена";
$MESS ['TE_DOWNLOAD_XML'] = "Скачать выгруженный файл";
$MESS ['TE_NEXT_BTN'] = "Выгрузить &gt;&gt;";
$MESS ['TE_DELETE_FILE_BTN'] = "Удалить выгруженный файл";
$MESS ['TSZH_EXPORT_NO_TSZH_SELECTED'] = "Не выбран объект управления";
$MESS ['TSZH_EXPORT_TSZH'] = "Объект управления";
$MESS ['TSZH_EXPORT_STEP_TIME'] = "Длительность шага в секундах<br />(0 - выполнять экспорт за один шаг)";
$MESS ['TSZH_EXPORT_PROGRESS'] = "Выгрузка платежей.<br />Выгружено: #CNT# из #TOTAL#...";
$MESS ['TSZH_EXPORT_FORMAT'] = "Формат";
$MESS ['TSZH_EXPORT_FORMAT_XML_DESC'] = "Выгрузка файла в формате XML, для последующей загрузки в 1C: Учет в управляющих компаниях ЖКХ, ТСЖ и ЖСК.";
$MESS["TSZH_EXPORT_PERIOD"] = "Выгружать платежи за период:";
$MESS["TSZH_EXPORT_ERROR_DATE_1"] = "Неверный формат первой даты!";
$MESS["TSZH_EXPORT_ERROR_DATE_2"] = "Неверный формат второй даты!";
$MESS["TSZH_EXPORT_ERROR_DATE_3"] = "Первая дата больше второй!";

?>