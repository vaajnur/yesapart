<?

$MESS ['TSZH_PAYMENT_EDIT'] = "Платеж";
$MESS ['TSZH_PAYMENT_EDIT_TITLE'] = "Изменение платежа";
$MESS ['TSZH_PAYMENT_EDIT_PAGE_TITLE'] = "Изменение платежа #ID#";
$MESS ['TSZH_PAYMENT_EDIT_ERROR_ACCESS'] = "Недостаточно прав для изменения платежей";
$MESS ['TSZH_PAYMENT_EDIT_ERROR_NO_ID'] = "Не указан ID платежа";
$MESS ['TSZH_PAYMENT_EDIT_ERROR_UPDATE'] = "При сохранении платежа произошла ошибка";
$MESS ['TSZH_PAYMENT_EDIT_NOT_FOUND'] = "Платеж не найден";
$MESS ['TSZH_PAYMENT_EDIT_ERRORS'] = "Произошли следующие ошибки";

$MESS ['TSZH_PAYMENT_LIST'] = "Список платежей";
$MESS ['TSZH_PAYMENT_LIST_TITLE'] = "Перейти к списку платежей";

$MESS ['TSZH_PAYMENT_DELETE'] = "Удалить платеж";
$MESS ['TSZH_PAYMENT_DELETE_CONFIRM'] = "Вы действительно хотите удалить платеж?";

$MESS ['TSZH_PAYMENT_EDIT_SETTING'] = "Настроить";
$MESS ['TSZH_PAYMENT_EDIT_SETTING_TITLE'] = "Настроить поля формы";

$MESS["TSZH_PAYMENT_EDIT_TSZH_ID"] = "Объект управления";
$MESS ['TSZH_PAYMENT_EDIT_USER_ID'] = "Пользователь";
$MESS["TSZH_PAYMENT_EDIT_USER_UNAUTH"] = "(оплата без авторизации на сайте)";
$MESS ['TSZH_PAYMENT_EDIT_ACCOUNT_ID'] = "Лицевой счет";
$MESS ['TSZH_PAYMENT_EDIT_PAYED'] = "Оплачено";
$MESS ['TSZH_PAYMENT_EDIT_DATE_PAYED'] = "Дата оплаты";
$MESS ['TSZH_PAYMENT_EDIT_EMP_PAYED_ID'] = "Сотрудник, подтвердивший платеж";
$MESS ['TSZH_PAYMENT_SERVICES_SUMM'] = "Оплачиваемые услуги:";
$MESS ['TSZH_PAYMENT_EDIT_SUMM'] = "Итого к оплате";
$MESS ['TSZH_PAYMENT_EDIT_SUMM_PAYED'] = "Оплаченная сумма";
$MESS ['TSZH_PAYMENT_EDIT_CURRENCY'] = "Валюта";
$MESS ['TSZH_PAYMENT_EDIT_PAY_SYSTEM'] = "Платежная система";
$MESS ['TSZH_PAYMENT_AUTO_EMP_PAYED_ID'] = "(платежная система)";

$MESS ['TSZH_PAYMENT_EDIT_IS_OVERHAUL'] = "Кап. ремонт";
$MESS ['TSZH_PAYMENT_EDIT_IS_PENALTY'] = "Пени";
$MESS ['TSZH_PAYMENT_EDIT_IS_INSURANCE_INCLUDED'] = "Включены оплаты по добровольному страхованию";

$MESS ['TSZH_PAYMENT_EDIT_PSYS_STATUS'] = "Код статуса";
$MESS ['TSZH_PAYMENT_EDIT_PSYS_STATUS_DESCR'] = "Описание статуса";
$MESS ['TSZH_PAYMENT_EDIT_PSYS_STATUS_RESPONSE'] = "Ответ на запрос статуса";
$MESS ['TSZH_PAYMENT_EDIT_PSYS_STATUS_DATE'] = "Дата получения статуса";

$MESS ['TSZH_PAYMENT_EDIT_USER_FIELDS'] = "Дополнительные поля";

$MESS ['P_ORDER_PS_STATUS'] = "Статус платежной системы";
$MESS ['P_ORDER_PS_STATUS_CODE'] = "Код статуса платежной системы";
$MESS ['P_ORDER_PS_STATUS_DESCRIPTION'] = "Описание статуса платежной системы";
$MESS ['P_ORDER_PS_STATUS_MESSAGE'] = "Сообщение платежной системы";
$MESS ['P_ORDER_PS_SUM'] = "Сумма платежной системы";
$MESS ['P_ORDER_PS_CURRENCY'] = "Валюта платежной системы";
$MESS ['P_ORDER_PS_RESPONSE_DATE'] = "Дата опроса платежной системы";
$MESS ['P_ORDER_PS_STATUS_UPDATE'] = "Получить";
$MESS["TSZH_PAYMENT_EDIT_PSYS_FEE"] = "Комиссия платежной системы";
$MESS["TSZH_PAYMENT_EDIT_PSYS_OPERATION_ID"] = "Номер транзакции в платежной системе";


$MESS ['TSZH_PAYMENT_NO_PAYMENT'] = "Платеж не найден.";
$MESS ['SOD_NO_PS_SCRIPT'] = "Не найден скрипт получения результатов работы платежной системы";
$MESS ['SOD_CANT_PAY'] = "Не удалось оплатить заказ с кодом ##ID#";
$MESS ['ERROR_CONNECT_PAY_SYS'] = "Ошибка подключения к платежной системе.";

$MESS ['SOD_OK_PS'] = "Информация по оплате заказа успешно запрошена у платежной системы";

$MESS ['TSZH_PAYMENT_EDIT_C_ADDRESS'] = "Адрес";
$MESS ['TSZH_PAYMENT_EDIT_C_COMMENTS'] = "Комментрии (показания счетчиков)";
$MESS["CITRUS_TSZHPAYMENT_FROM_PS"] = "(подтверждение оплаты из платежной системы)";

?>