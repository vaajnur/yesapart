<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 11.05.2018 10:33
 */

$MESS["PAYMENT_ENTITY_ID_FIELD"] = "ID";
$MESS["PAYMENT_ENTITY_LID_FIELD"] = "Сайт";
$MESS["PAYMENT_ENTITY_PAYED_FIELD"] = "Оплачено";
$MESS["PAYMENT_ENTITY_DATE_PAYED_FIELD"] = "Дата оплаты";
$MESS["PAYMENT_ENTITY_EMP_PAYED_ID_FIELD"] = "Сотрудник, подтвердивший платеж";
$MESS["PAYMENT_ENTITY_SUMM_FIELD"] = "Сумма к оплате";
$MESS["PAYMENT_ENTITY_SUMM_PAYED_FIELD"] = "Оплаченная сумма";
$MESS["PAYMENT_ENTITY_CURRENCY_FIELD"] = "Валюта";
$MESS["PAYMENT_ENTITY_USER_ID_FIELD"] = "Пользователь";
$MESS["PAYMENT_ENTITY_PAY_SYSTEM_ID_FIELD"] = "Платежная система";
$MESS["PAYMENT_ENTITY_DATE_INSERT_FIELD"] = "Создано";
$MESS["PAYMENT_ENTITY_DATE_UPDATE_FIELD"] = "Изменено";
$MESS["PAYMENT_ENTITY_ACCOUNT_ID_FIELD"] = "Лицевой счет";
$MESS["PAYMENT_ENTITY_PSYS_STATUS_FIELD"] = "Код статуса";
$MESS["PAYMENT_ENTITY_PSYS_STATUS_DESCR_FIELD"] = "Описание статуса";
$MESS["PAYMENT_ENTITY_PSYS_STATUS_RESPONSE_FIELD"] = "Ответ на запрос статуса";
$MESS["PAYMENT_ENTITY_PSYS_STATUS_DATE_FIELD"] = "Дата получения статуса";
$MESS["PAYMENT_ENTITY_PSYS_OPERATION_ID_FIELD"] = "Номер транзакции в платежной системе";
$MESS["PAYMENT_ENTITY_PSYS_FEE_FIELD"] = "Комиссия платежной системы";
$MESS["PAYMENT_ENTITY_TSZH_ID_FIELD"] = "Объект управления";
$MESS["PAYMENT_ENTITY_C_PAYEE_NAME_FIELD"] = "ФИО плательщика";
$MESS["PAYMENT_ENTITY_C_ADDRESS_FIELD"] = "Адрес";
$MESS["PAYMENT_ENTITY_EMAIL_FIELD"] = "E-MAIL";
$MESS["PAYMENT_ENTITY_C_ACCOUNT_FIELD"] = "Лицевой счет";
$MESS["PAYMENT_ENTITY_C_COMMENTS_FIELD"] = "Комментрии (показания счетчиков)";
$MESS["PAYMENT_ENTITY_INSURANCE_INCLUDED_FIELD"] = "Включены оплаты по добровольному страхованию";
$MESS["PAYMENT_ENTITY_IS_OVERHAUL_FIELD"] = "Кап. ремонт";
$MESS["PAYMENT_ENTITY_IS_PENALTY_FIELD"] = "Пени";