<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 10.05.2018 14:24
 */
$MESS["PAYMENT_PAYER_ID_ENTITY_PAYMENT_ID_FIELD"] = "ID платежа";
$MESS["PAYMENT_PAYER_ID_ENTITY_ACCOUNT_ID_FIELD"] = "ID лицевого счета";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_FIELD"] = "Тип документа";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_NUMBER_FIELD"] = "Серия и номер документа";
$MESS["PAYMENT_PAYER_ID_ENTITY_NATIONALITY_FIELD"] = "Гражданство";

$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_01"] = "Паспорт гражданина Российской Федерации";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_02"] = "Свидетельство органов ЗАГС, органа исполнительной власти или органа местного самоуправления о рождении гражданина";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_03"] = "Паспорт моряка (удостоверение личности моряка)";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_04"] = "Удостоверение личности военнослужащего";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_05"] = "Военный билет военнослужащего";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_06"] = "Временное удостоверение личности гражданина Российской Федерации";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_07"] = "Справка об освобождении из мест лишения свободы";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_08"] = "Паспорт иностранного гражданина либо иной документ, установленный федеральным законом или признаваемый в соответствии с международным договором Российской Федерации в качестве документа, удостоверяющего личность иностранного гражданина";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_09"] = "Вид на жительство";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_10"] = "Разрешение на временное проживание (для лиц без гражданства)";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_11"] = "Удостоверение беженца";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_12"] = "Миграционная карта";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_13"] = "Паспорт гражданина СССР";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_14"] = "CНИЛС";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_21"] = "ИНН";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_22"] = "Водительское удостоверение";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_23"] = "Зарезервировано";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_24"] = "Свидетельство о регистрации транспортного средства в органах Министерства внутренних дел Российской Федерации";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_25"] = "Охотничий билет";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_26"] = "Разрешение на хранение и ношение охотничьего оружия";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_27"] = "Номер мобильного телефона";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_28"] = "Паспорт гражданина Российской Федерации, являющийся основным документом, удостоверяющим личность гражданина Российской Федерации за пределами территории Российской Федерации, в том числе содержащий электронный носитель информации";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_29"] = "Свидетельство о предоставлении временного убежища на территории Российской Федерации";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_30"] = "Свидетельство о рассмотрении ходатайства по существу";

$MESS["PAYMENT_PAYER_ID_ENTITY_NATIONALITY_643"] = "Гражданин РФ";
$MESS["PAYMENT_PAYER_ID_ENTITY_NATIONALITY_999"] = "Без гражданства";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_NUMBER_REGEXP_ERROR"] = "В поле \"Серия и номер документа\" должны быть указаны только цифры и заглавные буквы латинского алфавита";
$MESS["PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_ERROR_TYPE"] = "Указанный тип документа не поддерживается";