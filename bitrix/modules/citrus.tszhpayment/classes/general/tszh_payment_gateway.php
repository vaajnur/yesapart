<?
IncludeModuleLangFile(__FILE__);

// require_once("BaseJsonRpcClient.php");

class CTszhPaymentGateway
{
	const SERVER_URL = 'https://otr-soft.ru/jkh-service/';

	protected $key;
	protected static $instance;

	protected function __construct()
	{
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/update_client_partner.php");
		$this->key = md5("BITRIX".CUpdateClientPartner::GetLicenseKey()."LICENCE");

		static::$instance = $this;
	}

	// singleton pattern
	final private function __clone() {}
	final public static function &getInstance()
	{
		return isset(static::$instance) ? static::$instance : new static();
	}

	public function registerCopy($siteId)
	{
		// TODO rewrite to own registerCopy
		if (function_exists('tszhSendUsage'))
			tszhSendUsage($siteId);
	}

	public function registerObject($id)
	{
		$fields = CTszh::GetList(Array(), Array("ID" => $id), false, Array('nTopCount' => 1), Array("*", "UF_*"))->Fetch();

		self::registerCopy($fields["SITE_ID"]);

		$arSite = CSite::GetByID($fields["SITE_ID"])->GetNext();
		$serverName = empty($arSite["SERVER_NAME"]) ? COption::GetOptionString("main", "server_name", $_SERVER['HTTP_HOST']) : $arSite["SERVER_NAME"].$arSite["DIR"];
		$siteUrl = (CMain::IsHTTPS() ? "https" : "http") . '://' . $serverName;
		$fields['SITE_URL'] = $siteUrl;
		$fields['DIRTY'] = CTszh::isDirty($id);
		if (!strlen($fields['EMAIL']) || !check_email($fields["EMAIL"]))
			$fields['EMAIL'] = COption::GetOptionString("main", "email_from", false);

		// TODO re-register as demo flag changes
		$client = new BaseJsonRpcClient(self::SERVER_URL);
		$result = $client->registerObject($this->key, $id, $fields, $this->isDemo(), false);
		if ($result->HasError())
		{
			$this->showError($result);
		}
	}

	public function deleteObject($id)
	{
		$client = new BaseJsonRpcClient(self::SERVER_URL);
		$result = $client->deleteObject($this->key, $id);
		if ($result->HasError())
		{
			$this->showError($result);
		}
	}

	public function registerPayment($id, $paymentFields = false)
	{
		$paymentFields = CTszhPayment::GetList(Array(), Array("ID" => $id), false, false, Array("*", "UF_*"))->Fetch();
		$paymentSystem = CTszhPaySystem::GetByID($paymentFields["PAY_SYSTEM_ID"]);
		foreach ($paymentFields as $key => $value)
		{
			if ((stripos($key, "ACCOUNT_") === 0 || stripos($key, "PS_") === 0) && $key !== 'ACCOUNT_TSZH_ID')
				unset($paymentFields[$key]);
		}
		if (isset($paymentFields["TSZH_ID"]))
		{
			$paymentFields["ACCOUNT_TSZH_ID"] = $paymentFields["TSZH_ID"];
		}
		unset($paymentSystem['PARAMS']);

		$client = new BaseJsonRpcClient(self::SERVER_URL);
		$result = $client->registerPayment($this->key, $id, $paymentFields, $paymentSystem, $this->isDemo());
		if ($result->HasError())
		{
			$this->showError($result);

			return false;
		}
		return true;
	}

	public function deletePayment($id)
	{
		$client = new BaseJsonRpcClient(self::SERVER_URL);
		$result = $client->deletePayment($this->key, $id);
		if ($result->HasError())
		{
			$this->showError($result);
		}
	}

	public function registerMonetaProfile($tszh_id, $profile = 'no_offer')
	{
		$client = new BaseJsonRpcClient(self::SERVER_URL);
		$isDemo = $this::isDemo();
		$isDirty = CTszh::isDirty($tszh_id);
		$result = $client->registerMonetaProfile($this->key, $tszh_id, $isDemo, $isDirty, $profile);

		if ($result->HasError())
		{
			$this->showError($result);
			return false;
		}

		return $result->Result;
	}

	public function getParams($id, $paymentFields = false)
	{
		// to be overriden in descendants
		return Array();
	}

	public static function isDemo()
	{
		if (!CModule::IncludeModule("citrus.tszh"))
		{
			return true;
		}
		$editionModule = CTszhFunctionalityController::GetEdition($edition);
		return (CModule::IncludeModuleEx($editionModule) !== MODULE_INSTALLED) || (\Bitrix\Main\Config\Option::get('main', 'update_devsrv') == 'Y');
	}

	public static function paramTo($value)
	{
		return defined("BX_UTF") ? $value : $GLOBALS['APPLICATION']->ConvertCharsetArray($value, SITE_CHARSET, "utf-8");
	}

	public static function paramFrom($value)
	{
		return defined("BX_UTF") ? $value : $GLOBALS['APPLICATION']->ConvertCharsetArray($value, "utf-8", SITE_CHARSET);
	}

	protected function showError($res)
	{
		$GLOBALS["APPLICATION"]->ThrowException($res->Error['message'] . (isset($res->Error['data']) ? ' (' . $res->Error['data'] . ')' : ''));
	}
}

class CTszhPaymentGatewayMoneta extends CTszhPaymentGateway
{
	public function getParams($id, $paymentFields = false)
	{
		$client = new BaseJsonRpcClient(self::SERVER_URL);
		$result = $client->getParams($this->key, $id);
		if ($result->HasError())
		{
			$this->showError($result);
			return false;
		}
		return array_merge(array('key' => $this->key), $result->Result);
	}

	public function getOperationInfo($opearationId)
	{
		$client = new BaseJsonRpcClient(self::SERVER_URL);
		$result = $client->getOperationInfo($this->key, $opearationId);
		if ($result->HasError())
		{
			$this->showError($result);
			return false;
		}
		return $result->Result;
	}

	/**
	 * @return array|bool
	 */
	public function getPaymentMethods()
	{
		$client = new BaseJsonRpcClient(self::SERVER_URL);
		$result = $client->getPaymentMethods();
		if ($result->HasError())
		{
			$this->showError($result);
			return false;
		}
		return $result->Result;
	}

	public function getMinPaymentSum()
	{
		return 10;
	}

	public function saveAddInfo($arFields)
	{
		$client = new BaseJsonRpcClient(self::SERVER_URL);
		$result = $client->saveAddInfo($this->key, $arFields);

		if ($result->HasError())
		{
			$this->showError($result);
			return false;
		}
		return true;
	}

	public function getAddInfo($tszh_id)
	{
		$client = new BaseJsonRpcClient(self::SERVER_URL);
		$result = $client->getAddInfo($this->key, $tszh_id);

		if ($result->HasError())
		{
			$this->showError($result);
			return false;
		}
		return $result->Result;
	}
}
?>