<?
/**
 * ����� ��� �������� ��������
 * Class CTszhPaymentsExport
 */
class CTszhPaymentsExport
{
	/**
	 * <b>�������� �������� � XML-���� ��� 1�:���� � ����������� ���������</b><br>
	 * ����� ���� ���������, ���� $NS['stepTime'] > 0. � ����� ������ ����� ��������� ��������� �������, ������� ������ ��������� �� �������� �������� ���� ��������. ����� ����� ����� ��������� � ���� �� ����������� (�� ��������� ����) �� ��� ���, ���� $NS['progress']['remaining'] > 0<br>
	 * <br>
	 * <i>�������� ������ <b>$NS</b></i>:<br>
	 *		filename � * ��� ����� (������������ ����� �����), ���� ������ ���� ��������� ������<br>
	 *		stepTime � ������������ ����� ������ ������ ���� (� ��������)<br>
	 * 		tszh � * ������������� ������ � ������ ������� ����������, ������� �� �������� ������ ���� ���������<br>
	 * * �������� �����, ������� ����������� ������ ��� �������<br>
	 *<br>
	 * @param array $arFilter ������ ��� ������ �������� (� ��� �����������, ��������, id ������ ���������� � ���� ������)
	 * @param int $lastID ��������� ������������ ������ � ����������� ����
	 * @param array $NS ������, ��� ����������� ���������� �������� � ����� ��������� ����� ������ ��������
	 * @return int ID ��������� ������������ ������
	 * @throws Exception
	 */
	public static function ProcessExport($arFilter, $lastID, &$NS)
	{
		$startTime = microtime(1);
		@set_time_limit(0);
		@ignore_user_abort(true);

		if (!is_array($NS))
			throw new Exception("\$NS must be an array");
		if (!is_set($NS, 'filename'))
			throw new Exception("\$NS must contain filename key");
		if (!is_set($NS, 'tszh') || !is_array($NS['tszh']))
			throw new Exception("\$NS must contain tszh key");
		if (!is_array($arFilter))
			throw new Exception("\$arFilter parameter not specified");
		// ��������� ������ ������� ����� ������ � payu
		/*$arFilter["@PS_ACTION_FILE"] = array(
			"/bitrix/modules/citrus.tszhpayment/ru/payment/moneta",
			"/bitrix/modules/citrus.tszhpayment/ru/payment/payu",
			"/bitrix/php_interface/include/citrus.tszhpayment/payment/payu",
		);*/
		$arFilter["!ACCOUNT_ID"] = false;
		$arFilter["!PS_ACTION_FILE"] = false;


		$f = fopen($_SERVER['DOCUMENT_ROOT'] . '/'. $NS['filename'], 'a');
		// ������ ���
		if (!$lastID)
		{
			$inn = $NS['tszh']['INN'];

			// ���������� ���� ������ ���������� �������
			$lastPayment = CTszhPayment::GetList(Array("DATE_PAYED" => "DESC"), $arFilter, false, array('nTopCount' => 1), Array("ID", "DATE_PAYED"))->Fetch();
			$date = is_array($lastPayment) ? $lastPayment["DATE_PAYED"] : ConvertTimeStamp(false, "FULL");
			fputs($f, "<?xml version=\"1.0\" encoding=\"" . SITE_CHARSET . "\"?>\n<payments filetype=\"payments\" inn=\"$inn\" filedate=\"$date\" version=\"" . TSZH_EXCHANGE_CURRENT_VERSION  . "\">\n");

			$count = CTszhPayment::GetList(array(), $arFilter, Array("COUNT" => "ID"))->Fetch();
			$NS['progress'] = Array(
				'total' => $count["ID"],
				'done' => 0,
				'remaining' => $count["ID"],
			);
			$lastID = 0;
		}

		$arrFilter = array_merge($arFilter, Array(">ID" => $lastID));
		$dbPayments = CTszhPayment::GetList(Array("ID" => "ASC"), $arrFilter, false, false, Array("ID", "DATE_PAYED", "SUMM", "ACCOUNT_XML_ID", "ACCOUNT_EXTERNAL_ID", "PSYS_FEE", "PS_ACTION_FILE", "C_ACCOUNT", "INSURANCE_INCLUDED", "UF_*", "IS_OVERHAUL", "IS_PENALTY"));
		while ($payment = $dbPayments->Fetch())
		{
			$lastID = $payment["ID"];
			$attr = Array(
				"id" => $payment["ID"],
				"date" => $payment["DATE_PAYED"],
				"summ" => $payment["SUMM"],
				"account" => $payment["C_ACCOUNT"] ? $payment["C_ACCOUNT"] : $payment["ACCOUNT_XML_ID"],
				"ps" => basename($payment["PS_ACTION_FILE"]),
			);
            if (!empty($payment["INSURANCE_INCLUDED"])) $attr["insurance_included"] = $payment["INSURANCE_INCLUDED"];
            if (!empty($payment["IS_OVERHAUL"]))
            {
            	$attr["is_overhaul"] = $payment["IS_OVERHAUL"] == 'Y' ? 1 : 0;
            }
            if (!empty($payment["IS_PENALTY"]))
            {
            	$attr["is_penalty"] = $payment["IS_PENALTY"] == 'Y' ? 1 : 0;
            }
			if (!empty($payment["ACCOUNT_EXTERNAL_ID"])) $attr["kod_ls"] = $payment["ACCOUNT_EXTERNAL_ID"];
			if (is_set($payment, "PSYS_FEE") && $payment["PSYS_FEE"] > 0) $attr["fee"] = $payment["PSYS_FEE"];
			foreach ($payment as $field => $value)
				if (substr($field, 0, 3) == "UF_" && !empty($value))
					$attr[ToLower($field)] = $value;
			$paymentString = "\t<payment";
			foreach ($attr as $k=>$v)
				$paymentString .= " $k=\"$v\"";
			$paymentString .= " />\n";
			fputs($f, $paymentString);

			$NS['progress']['done']++;
			$NS['progress']['remaining']--;
			if ($NS['stepTime'] && microtime(1) - $startTime >= $NS['stepTime'])
				break;
		}
		if ($NS['progress']['remaining'] <= 0)
		{
			$NS['progress']['remaining'] = 0;
			fputs($f, "</payments>\n");
		}
		fclose($f);
		return $lastID;
	}
}
?>