<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

IncludeModuleLangFile(__FILE__);

class CTszhPaySystem
{
	static $paymentBase = null;

	public static function GetByID($ID)
	{
		global $DB;

		$ID = IntVal($ID);
		$strSql =
			"SELECT * ".
				"FROM b_tszh_pay_system ".
				"WHERE ID = ".$ID."";
		$db_res = $DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);
		if ($res = $db_res->Fetch()) {
			return $res;
		}
		return false;
	}

	public static function CheckFields($ACTION, &$arFields)
	{
		global $APPLICATION;

		if ((is_set($arFields, "NAME") || $ACTION=="ADD") && strlen($arFields["NAME"]) <= 0)
		{
			$APPLICATION->ThrowException(GetMessage("SKGPS_EMPTY_NAME"), "ERROR_NO_NAME");
			return false;
		}

		if (is_set($arFields, "LID") && $ACTION!="ADD")
			UnSet($arFields["LID"]);

		if ((is_set($arFields, "CURRENCY") || $ACTION=="ADD") && strlen($arFields["CURRENCY"])<=0)
		{
			$APPLICATION->ThrowException(GetMessage("SKGPS_EMPTY_CURRENCY"), "ERROR_NO_CURRENCY");
			return false;
		}

		if (is_set($arFields, "LID"))
		{
			$dbSite = CSite::GetByID($arFields["LID"]);
			if (!$dbSite->Fetch())
			{
				$APPLICATION->ThrowException(str_replace("#ID#", $arFields["LID"], GetMessage("SKGPS_NO_SITE")), "ERROR_NO_SITE");
				return false;
			}
		}

		if ((is_set($arFields, "TSZH_ID") || $ACTION=="ADD"))
		{
			$arTszh = CTszh::GetByID($arFields["TSZH_ID"]);
			if (!is_array($arTszh) || empty($arTszh))
			{
				$APPLICATION->ThrowException(str_replace("#ID#", $arFields["TSZH_ID"], GetMessage("SKGPS_NO_TSZH")), "ERROR_NO_TSZH");
				return false;
			}
		}

		if (is_set($arFields, "ACTIVE") && $arFields["ACTIVE"]!="Y")
			$arFields["ACTIVE"] = "N";
		if (is_set($arFields, "SORT") && IntVal($arFields["SORT"])<=0)
			$arFields["SORT"] = 100;

		if (is_set($arFields, "NEW_WINDOW") && $arFields["NEW_WINDOW"] != "Y")
			$arFields["NEW_WINDOW"] = "N";
		if (is_set($arFields, "HAVE_PAYMENT") && $arFields["HAVE_PAYMENT"] != "Y")
			$arFields["HAVE_PAYMENT"] = "N";
		if (is_set($arFields, "HAVE_ACTION") && $arFields["HAVE_ACTION"] != "Y")
			$arFields["HAVE_ACTION"] = "N";
		if (is_set($arFields, "HAVE_RESULT") && $arFields["HAVE_RESULT"] != "Y")
			$arFields["HAVE_RESULT"] = "N";
		if (is_set($arFields, "HAVE_PREPAY") && $arFields["HAVE_PREPAY"] != "Y")
			$arFields["HAVE_PREPAY"] = "N";
		if (is_set($arFields, "HAVE_RESULT_RECEIVE") && $arFields["HAVE_RESULT_RECEIVE"] != "Y")
			$arFields["HAVE_RESULT_RECEIVE"] = "N";
		if (is_set($arFields, "ENCODING") && strlen($arFields["ENCODING"]) <= 0)
			unset($arFields["ENCODING"]);

		return True;
	}

	public static function Delete($ID)
	{
		global $DB;
		$ID = IntVal($ID);
		return $DB->Query("DELETE FROM b_tszh_pay_system WHERE ID = ".$ID."", true);
	}

	public static function SerializeParams($arParams)
	{
		return serialize($arParams);
	}

	public static function UnSerializeParams($strParams)
	{
		$arParams = unserialize($strParams);

		if (!is_array($arParams))
			$arParams = array();

		return $arParams;
	}

	public static function GetParamValue($key)
	{
		if (
			isset($_REQUEST["TSZH_PAYMENT_CORRESPONDENCE"]) || array_key_exists("TSZH_PAYMENT_CORRESPONDENCE", $_REQUEST)
			|| isset($_POST["TSZH_PAYMENT_CORRESPONDENCE"]) || array_key_exists("TSZH_PAYMENT_CORRESPONDENCE", $_POST)
			|| isset($_GET["TSZH_PAYMENT_CORRESPONDENCE"]) || array_key_exists("TSZH_PAYMENT_CORRESPONDENCE", $_GET)
			|| isset($_SESSION["TSZH_PAYMENT_CORRESPONDENCE"]) || array_key_exists("TSZH_PAYMENT_CORRESPONDENCE", $_SESSION)
			|| isset($_COOKIE["TSZH_PAYMENT_CORRESPONDENCE"]) || array_key_exists("TSZH_PAYMENT_CORRESPONDENCE", $_COOKIE)
			|| isset($_SERVER["TSZH_PAYMENT_CORRESPONDENCE"]) || array_key_exists("TSZH_PAYMENT_CORRESPONDENCE", $_SERVER)
			|| isset($_ENV["TSZH_PAYMENT_CORRESPONDENCE"]) || array_key_exists("TSZH_PAYMENT_CORRESPONDENCE", $_ENV)
			|| isset($_FILES["TSZH_PAYMENT_CORRESPONDENCE"]) || array_key_exists("TSZH_PAYMENT_CORRESPONDENCE", $_FILES)
			|| isset($_REQUEST["TSZH_PAYMENT_PARAMS"]) || array_key_exists("TSZH_PAYMENT_PARAMS", $_REQUEST)
			|| isset($_POST["TSZH_PAYMENT_PARAMS"]) || array_key_exists("TSZH_PAYMENT_PARAMS", $_POST)
			|| isset($_GET["TSZH_PAYMENT_PARAMS"]) || array_key_exists("TSZH_PAYMENT_PARAMS", $_GET)
			|| isset($_SESSION["TSZH_PAYMENT_PARAMS"]) || array_key_exists("TSZH_PAYMENT_PARAMS", $_SESSION)
			|| isset($_COOKIE["TSZH_PAYMENT_PARAMS"]) || array_key_exists("TSZH_PAYMENT_PARAMS", $_COOKIE)
			|| isset($_SERVER["TSZH_PAYMENT_PARAMS"]) || array_key_exists("TSZH_PAYMENT_PARAMS", $_SERVER)
			|| isset($_ENV["TSZH_PAYMENT_PARAMS"]) || array_key_exists("TSZH_PAYMENT_PARAMS", $_ENV)
			|| isset($_FILES["TSZH_PAYMENT_PARAMS"]) || array_key_exists("TSZH_PAYMENT_PARAMS", $_FILES)
		)
		{
			return False;
		}

		if (!isset($GLOBALS["TSZH_PAYMENT_CORRESPONDENCE"]) || !is_array($GLOBALS["TSZH_PAYMENT_CORRESPONDENCE"]))
			return False;

		if (!array_key_exists($key, $GLOBALS["TSZH_PAYMENT_CORRESPONDENCE"]))
			return False;

		$type = $GLOBALS["TSZH_PAYMENT_CORRESPONDENCE"][$key]["TYPE"];
		$value = $GLOBALS["TSZH_PAYMENT_CORRESPONDENCE"][$key]["VALUE"];
		if (strlen($type) > 0)
		{
			if (isset($GLOBALS["TSZH_PAYMENT_PARAMS"])
				&& is_array($GLOBALS["TSZH_PAYMENT_PARAMS"])
				&& array_key_exists($type, $GLOBALS["TSZH_PAYMENT_PARAMS"])
				&& is_array($GLOBALS["TSZH_PAYMENT_PARAMS"][$type])
				&& array_key_exists($value, $GLOBALS["TSZH_PAYMENT_PARAMS"][$type]))
			{
				$res = $GLOBALS["TSZH_PAYMENT_PARAMS"][$type][$value];
			}
			else
			{
				$res = False;
			}
		}
		else
		{
			/*
			if ((substr($value, 0, 1) == "=") && (strlen($value) > 1))
				eval("\$res=".substr($value, 1).";");
			else*/
			$res = $value;
		}
		return $res;
	}

	public static function InitParamArrays($arPayment, $psParams = "")
	{
		$GLOBALS["TSZH_PAYMENT_PARAMS"] = array();
		$GLOBALS["TSZH_PAYMENT_CORRESPONDENCE"] = array();

		if (count($arPayment) > 0)
			$GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"] = $arPayment;

		$GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["SHOULD_PAY"] = DoubleVal($GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["SUMM"]) - DoubleVal($GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["SUMM_PAID"]);

		$arDateInsert = explode(" ", $GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["DATE_INSERT"]);
		if (is_array($arDateInsert) && count($arDateInsert) > 0)
			$GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["DATE_INSERT_DATE"] = $arDateInsert[0];
		else
			$GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["DATE_INSERT_DATE"] = $GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["DATE_INSERT"];

		$userID = IntVal($GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["USER_ID"]);
		if ($userID > 0) {
			$dbUser = CUser::GetByID($userID);
			if ($arUser = $dbUser->GetNext()) {
				$GLOBALS["TSZH_PAYMENT_PARAMS"]["USER"] = $arUser;
				$GLOBALS["TSZH_PAYMENT_PARAMS"]["USER"]["FULL_NAME"] = CTszhAccount::GetFullName($userID);
			}
		}

		if (is_set($arPayment, "ACCOUNT_ID") && ($accountId = $arPayment["ACCOUNT_ID"]) > 0)
		{
			$GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"] = CTszhAccount::GetByID($accountId);
			if (is_array($GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]))
				$GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]["FULL_ADDRESS"] = CTszhAccount::GetFullAddress($GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]);
		}
		else
		{
			$GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"] = CTszhAccount::GetByUserID($userID);
			if (is_array($GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]))
				$GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]["FULL_ADDRESS"] = CTszhAccount::GetFullAddress($GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]);
		}

		$tszhID = isset($GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["TSZH_ID"]) ? $GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["TSZH_ID"] : $GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]["TSZH_ID"];
		$GLOBALS["TSZH_PAYMENT_PARAMS"]["TSZH"] = CTszh::GetByID($tszhID);

		// ������ ������������ ��� �����������
		if (isset($arPayment["C_ACCOUNT"]))
		{
			if (!is_array($GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"]))
				$GLOBALS["TSZH_PAYMENT_PARAMS"]["ACCOUNT"] = Array(
					"XML_ID" => $arPayment["C_ACCOUNT"],
					"NAME" => $arPayment["C_PAYEE_NAME"],
					"FULL_ADDRESS" => $arPayment["C_ADDRESS"],
				);
			$GLOBALS["TSZH_PAYMENT_PARAMS"]["USER"] = Array(
				"FULL_NAME" => $arPayment["C_PAYEE_NAME"],
			);
		}

		if (strlen($psParams) <= 0)
		{
			$dbPaySystem = CTszhPaySystem::GetList(
				array(),
				array(
					"ID" => IntVal($GLOBALS["TSZH_PAYMENT_PARAMS"]["PAYMENT"]["PAY_SYSTEM_ID"]),
				),
				false,
				false,
				array("PARAMS")
			);

			if ($arPaySystem = $dbPaySystem->Fetch()) {
				$psParams = $arPaySystem["PARAMS"];
			}
		}
		$GLOBALS["TSZH_PAYMENT_CORRESPONDENCE"] = CTszhPaySystem::UnSerializeParams($psParams);
	}


	public static function IncludePrePaySystem($fileName, $bDoPayAction, &$arPaySysResult, &$strPaySysError, &$strPaySysWarning, $BASE_LANG_CURRENCY = False, $ORDER_PRICE = 0.0, $TAX_PRICE = 0.0, $DISCOUNT_PRICE = 0.0, $DELIVERY_PRICE = 0.0)
	{
		$strPaySysError = "";
		$strPaySysWarning = "";

		$arPaySysResult = array(
			"PS_STATUS" => false,
			"PS_STATUS_CODE" => false,
			"PS_STATUS_DESCRIPTION" => false,
			"PS_STATUS_MESSAGE" => false,
			"PS_SUM" => false,
			"PS_CURRENCY" => false,
			"PS_RESPONSE_DATE" => false,
			"USER_CARD_TYPE" => false,
			"USER_CARD_NUM" => false,
			"USER_CARD_EXP_MONTH" => false,
			"USER_CARD_EXP_YEAR" => false,
			"USER_CARD_CODE" => false
		);

		if ($BASE_LANG_CURRENCY === false)
			$BASE_LANG_CURRENCY = "RUB";

		include($fileName);
	}

	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		// FIELDS -->
		$arFields = array(
			"ID" => array("FIELD" => "TPS.ID", "TYPE" => "int"),
			"LID" => array("FIELD" => "TPS.LID", "TYPE" => "string"),
			"TSZH_ID" => array("FIELD" => "TPS.TSZH_ID", "TYPE" => "int"),
			"CURRENCY" => array("FIELD" => "TPS.CURRENCY", "TYPE" => "string"),
			"NAME" => array("FIELD" => "TPS.NAME", "TYPE" => "string"),
			"ACTIVE" => array("FIELD" => "TPS.ACTIVE", "TYPE" => "char"),
			"SORT" => array("FIELD" => "TPS.SORT", "TYPE" => "int"),
			"DESCRIPTION" => array("FIELD" => "TPS.DESCRIPTION", "TYPE" => "string"),

			"ACTION_FILE" => array("FIELD" => "TPS.ACTION_FILE", "TYPE" => "string"),
			"RESULT_FILE" => array("FIELD" => "TPS.RESULT_FILE", "TYPE" => "string"),
			"NEW_WINDOW" => array("FIELD" => "TPS.NEW_WINDOW", "TYPE" => "char"),
			"PARAMS" => array("FIELD" => "TPS.PARAMS", "TYPE" => "string"),
			"HAVE_PAYMENT" => array("FIELD" => "TPS.HAVE_PAYMENT", "TYPE" => "char"),
			"HAVE_ACTION" => array("FIELD" => "TPS.HAVE_ACTION", "TYPE" => "char"),
			"HAVE_RESULT" => array("FIELD" => "TPS.HAVE_RESULT", "TYPE" => "char"),
			"HAVE_PREPAY" => array("FIELD" => "TPS.HAVE_PREPAY", "TYPE" => "char"),
			"HAVE_RESULT_RECEIVE" => array("FIELD" => "TPS.HAVE_RESULT_RECEIVE", "TYPE" => "char"),
			"ENCODING" => array("FIELD" => "TPS.ENCODING", "TYPE" => "string"),
		);
		// <-- FIELDS
		if (count($arSelectFields) <= 0)
			$arSelectFields = array_keys($arFields);


		return CTszh::GetListMakeQuery('b_tszh_pay_system TPS', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
	}

	public static function GetSelectBoxList($arOrder = array(), $arFilter = array())
	{
		// FIELDS -->
		$arFields = array(
			"REFERENCE_ID" => array("FIELD" => "TPS.ID", "TYPE" => "int"),
			"REFERENCE" => array("FIELD" => "CONCAT('[', TPS.ID, '] ', TPS.NAME)", "TYPE" => "string"),
		);
		// <-- FIELDS
		$arSelectFields = array_keys($arFields);

		return CTszh::GetListMakeQuery('b_tszh_pay_system TPS', $arFields, $arOrder, $arFilter, false, false, $arSelectFields);
	}


	public static function Add($arFields)
	{
		global $DB;

		if (!CTszhPaySystem::CheckFields("ADD", $arFields))
			return false;

		if (array_key_exists('DESCRIPTION', $arFields)) {
			$arFields['~DESCRIPTION'] = "'" . $arFields['DESCRIPTION'] . "'";
//			unset($arFields['DESCRIPTION']);
		}

		$arInsert = $DB->PrepareInsert("b_tszh_pay_system", $arFields);

		$strSql =
			"INSERT INTO b_tszh_pay_system(".$arInsert[0].") ".
				"VALUES(".$arInsert[1].")";
		$DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);

		$ID = IntVal($DB->LastID());

		return $ID;
	}

	public static function Update($ID, $arFields)
	{
		global $DB;

		$ID = IntVal($ID);
		if (!CTszhPaySystem::CheckFields("UPDATE", $arFields))
			return false;

		if (array_key_exists('DESCRIPTION', $arFields)) {
			$arFields['~DESCRIPTION'] = "'" . $arFields['DESCRIPTION'] . "'";
//			unset($arFields['DESCRIPTION']);
		}

		$strUpdate = $DB->PrepareUpdate("b_tszh_pay_system", $arFields);
		$strSql = "UPDATE b_tszh_pay_system SET ".$strUpdate." WHERE ID = ".$ID."";
		$DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);

		return $ID;
	}

	public static function AddYandex($arFields, $bForceRewrite = false)
	{
		$siteID = SITE_ID;
		if (strlen($arFields["LID"]) > 0)
			$siteID = $arFields["LID"];

		if (!is_set($arFields, "TSZH_ID") || $arFields['TSZH_ID'] <= 0)
			return false;

		$rsPS = self::GetList(Array(), Array("TSZH_ID" => $arFields["TSZH_ID"], "ACTION_FILE" => '/bitrix/modules/citrus.tszhpayment/ru/payment/yandex'), false, false, Array("ID"));
		$arPS = $rsPS ? $rsPS->Fetch() : false;

		$arDefaultFields = Array(
			'LID' => $siteID,
			'CURRENCY' => 'RUB',
			'NAME' => GetMessage("SKGPSA_YANDEX"),
			'SORT' => '100',
			'ACTIVE' => 'Y',
			'DESCRIPTION' => GetMessage("SKGPSA_YANDEX_DESC"),
			'ACTION_FILE' => '/bitrix/modules/citrus.tszhpayment/ru/payment/yandex',
			'RESULT_FILE' => NULL,
			'NEW_WINDOW' => 'N',
			'PARAMS' => serialize(array(
				'SHOP_NAME' => array ('TYPE' => 'TSZH', 'VALUE' => 'NAME'),
				'SHOP_INN' => array ('TYPE' => 'TSZH', 'VALUE' => 'INN'),
				'SHOP_KPP' => array ('TYPE' => 'TSZH', 'VALUE' => 'KPP'),
				'BANK_ACCOUNT' => array ('TYPE' => 'TSZH', 'VALUE' => 'RSCH'),
				'BANK_NAME' => array ('TYPE' => 'TSZH', 'VALUE' => 'BANK'),
				'BANK_BIK' => array ('TYPE' => 'TSZH', 'VALUE' => 'BIK'),
				'SCID' => array ('TYPE' => '', 'VALUE' => '5512'),
				'ORDER_ID' => array ('TYPE' => 'PAYMENT', 'VALUE' => 'ID'),
				'ORDER_DATE' => array ('TYPE' => 'PAYMENT', 'VALUE' => 'DATE_INSERT'),
				'SHOULD_PAY' => array ('TYPE' => 'PAYMENT', 'VALUE' => 'SHOULD_PAY'),
				'ORDER_DESCR' => array ('TYPE' => '', 'VALUE' => GetMessage('SKGPSA_YANDEX_ORDER_DESCR')),
				'CUSTOMER_FIO' => array ('TYPE' => 'ACCOUNT', 'VALUE' => 'NAME'),
				'CUSTOMER_ADDRESS' => array ('TYPE' => 'ACCOUNT', 'VALUE' => 'FULL_ADDRESS'),
//				'NOTIFY_URL' => array ('TYPE' => '', 'VALUE' => ''),
				'SUCESS_URL' => array ('TYPE' => '', 'VALUE' => ''),
			)),
			'HAVE_PAYMENT' => 'Y',
			'HAVE_ACTION' => 'N',
			'HAVE_RESULT' => 'N',
			'HAVE_PREPAY' => 'N',
			'HAVE_RESULT_RECEIVE' => 'Y',
			'ENCODING' => 'windows-1251',
		);

		// �� ����� ������ ��������� ���� ��� ��� ������������ ��
		if ($arPS && !$bForceRewrite)
		{
			unset($arDefaultFields["ACTIVE"]);
			unset($arDefaultFields["PARAMS"]);
			unset($arDefaultFields["NAME"]);
			unset($arDefaultFields["DESCRIPTION"]);
		}

		$arFields = array_merge($arDefaultFields, $arFields);
		if ($arPS)
		{
			$bResult = self::Update($arPS["ID"], $arFields);
			return $bResult ? $arPS['ID'] : false;
		}
		else
			return self::Add($arFields);
	}

	public static function AddMoneta($arFields, $bForceRewrite = false, $bEnabled = true)
	{
		$siteID = SITE_ID;
		if (strlen($arFields["LID"]) > 0)
			$siteID = $arFields["LID"];

		if (!is_set($arFields, "TSZH_ID") || $arFields['TSZH_ID'] <= 0)
			return false;

		$rsPS = self::GetList(Array(), Array("TSZH_ID" => $arFields["TSZH_ID"], "ACTION_FILE" => '/bitrix/modules/citrus.tszhpayment/ru/payment/moneta'), false, false, Array("ID"));
		$arPS = $rsPS ? $rsPS->Fetch() : false;

		$arDefaultFields = Array(
			'LID' => $siteID,
			'CURRENCY' => 'RUB',
			'NAME' => GetMessage("SKGPSA_MONETA"),
			'SORT' => '50',
			'ACTIVE' => 'Y',
			'DESCRIPTION' => GetMessage("SKGPSA_MONETA_DESC"),
			'ACTION_FILE' => '/bitrix/modules/citrus.tszhpayment/ru/payment/moneta',
			'RESULT_FILE' => NULL,
			'NEW_WINDOW' => 'N',
			'PARAMS' => serialize(array()),
			'HAVE_PAYMENT' => 'Y',
			'HAVE_ACTION' => 'N',
			'HAVE_RESULT' => 'N',
			'HAVE_PREPAY' => 'N',
			'HAVE_RESULT_RECEIVE' => 'Y',
			'ENCODING' => 'windows-1251',
		);

		// �� ����� ������ ��������� ���� ��� ��� ������������ ��
		if ($arPS && !$bForceRewrite)
		{
			unset($arDefaultFields["ACTIVE"]);
			unset($arDefaultFields["PARAMS"]);
			unset($arDefaultFields["NAME"]);
			unset($arDefaultFields["DESCRIPTION"]);
		}

		$arFields = array_merge($arDefaultFields, $arFields);
		$arFields["ACTIVE"] = $bEnabled ? "Y" : "N";
		if ($arPS)
		{
			$bResult = self::Update($arPS["ID"], $arFields);
			return $bResult ? $arPS['ID'] : false;
		}
		elseif ($bEnabled)
			return self::Add($arFields);
	}

	/**
	 * ���������� ��� ���������� � ��������� ������� ���������� � ������ citrus.tszh ��� �������� ��� ���������� �������� ������ ����� ��������� ������ ������.������
	 * @param int $ID ID ������� ���������� (CTszh)
	 * @param array $arFields ���� ������� ����������
	 * @return bool
	 */
	public static function OnAfterTszhAdd($ID, $arFields)
	{
		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		CTszhPaymentGateway::getInstance()->registerObject($ID);
		self::fillPredefinedPaySystems($ID);
		return true;
	}

	/**
	 * ���������� ����� �������� ������� ���������� ��� �������� ��������� ������ ����� ������� ����������
	 * @param int $ID ID ������� ����������
	 * @return bool
	 */
	public static function OnAfterTszhDelete($ID)
	{
		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		CTszhPaymentGateway::getInstance()->deleteObject($ID);

		$rsPaySystems = self::GetList(Array(), Array("TSZH_ID" => $ID), false, false, Array("ID"));
		while ($arPaySystem = $rsPaySystems->Fetch())
			self::Delete($arPaySystem["ID"]);

		return true;
	}

	/**
	 * ���������� ��� ��������� ������ ��� �������� ����������������� ��������� ������ (������.������) � ���� �������� ���������� ������ citrus.tszh
	 * @param bool|int $tszhID ID ������� ����������, ���� �� ������ � �� ����������� ��� ���� ������������ �������� ����������
	 * @param bool $bForceRewrite ������������� �������������� ���� ����������� �� (������������ ��� ����������)
	 */
	public static function fillPredefinedPaySystems($tszhID = false, $bForceRewrite = false)
	{
		if (CModule::IncludeModule("citrus.tszh"))
		{
			$arFilter = Array();
			if ($tszhID !== false)
				$arFilter["ID"] = IntVal($tszhID);
			$rsTszh = CTszh::GetList(Array(), $arFilter, false, false, Array("ID", "SITE_ID", "MONETA_ENABLED"));
			while ($arTszh = $rsTszh->Fetch())
			{
				$arPaySytemFields = array(
					'TSZH_ID' => $arTszh['ID'],
					'LID' => $arTszh['SITE_ID'],
				);
				//self::AddYandex($arPaySytemFields, $bForceRewrite);
				self::AddMoneta($arPaySytemFields, $bForceRewrite, $arTszh["MONETA_ENABLED"] == "Y");
			}
		}
	}

	/**
	 * @param string $siteID ID ����� (���� �� ������ -- �������)
	 * @return bool true, ���� �� ������� ����� �������� ������ ����� ������ ��� �������� (���)
	 */
	public static function monetaEnabled($siteID = SITE_ID)
	{
		$rsPS = self::GetList(Array(), Array("LID" => $siteID, "ACTIVE" => "Y", "ACTION_FILE" => '/bitrix/modules/citrus.tszhpayment/ru/payment/moneta'), false, false, Array("ID"));
		return is_object($rsPS) ? $rsPS->SelectedRowsCount() > 0 : false;
	}

	/**
	 * ��������� ������� �������� ��������� ������ �� ��������� �����.
	 * ���� ������ �������� $actionFile, ����������� ����� ������ ��������� ������� � ��������� ������������
	 *
	 * @param string $siteID ID ����� (���� �� ������ -- �������)
	 * @param string|bool $actionFile ��� ����������� (����� � ������������ � ������ ������)
	 * @return bool true, ���� �� ������� ����� �������� ������ ����� ������ ��� �������� (���)
	 */
	public static function hasActivePaySystems($siteID = SITE_ID, $actionFile = false)
	{
		$filter = Array("LID" => $siteID, "ACTIVE" => "Y");
		if ($actionFile)
			$filter["ACTION_FILE"] = '/bitrix/modules/citrus.tszhpayment/ru/payment/' . $actionFile;
		$rsPS = self::GetList(Array(), $filter, false, false, Array("ID"));
		return is_object($rsPS) ? $rsPS->SelectedRowsCount() > 0 : false;
	}

	/**
	 * @param string $path ������������� ��������� ���� (������������ ����� �����) � �������� �������� ��� ���������� ������
	 */
	public static function setPaymentBase($path)
	{
		self::$paymentBase = $path;
	}

	/**
	 * ���������� ����������� ������ �� ������� ����� � ��������� ���� � ���
	 * @param bool $siteID ���� (�� ��������� � �������)
	 * @param bool $checkMoneta ���������, �������� �� ������ ����� ������ ������.�� � ���������� ���� ������ ���� ��� �������
	 * @param bool $forceAuth �������� � ������ ��������, ��������� �� �������� ������ ������������ ����������� ������������ (��� ������ �� ��������� �� �����)
	 * @param string $addParams �������� � ������ ��������� ��������� (GET)
	 * @throws Exception
	 * @return string|bool ���� � �������� ������, false � ������, ���� ������ ����������
	 */
	public static function getPaymentPath($siteID = false, $checkMoneta = true, $forceAuth = false, $addParams = '')
	{
		global $APPLICATION;

		if (false === $siteID)
		{
			if (defined("ADMIN_SECTION"))
				throw new Exception("\$siteID parameter must be specified explicitly in administrative section.");
			$siteID = SITE_ID;
			$siteDir = SITE_DIR;
		}
		else
		{
			$arSite = CSite::GetByID($siteID)->Fetch();
			$serverName = is_array($arSite) && !empty($arSite["SERVER_NAME"]) ? $arSite["SERVER_NAME"] : (COption::GetOptionString('main', 'server_name', $_SERVER['HTTP_HOST']));
			$siteDir = is_array($arSite) && !empty($arSite["DIR"]) ? $arSite["DIR"] : "/";
			$siteDir = ($APPLICATION->IsHTTPS() ? 'https://' : 'http://') . $serverName . $siteDir;
		}

		// TODO ����������� ��������� ���� � �������� ������ � ������ �������� ���������� (� ������� ���� ���� �� ����� ������������ ��������� ��� �������� ���������)
		if (isset(self::$paymentBase))
			$path = self::$paymentBase;
		else
			$path = $siteDir . COption::GetOptionString('citrus.tszh', "payment_path", "personal/payment/", $siteID);
		if ($forceAuth || $addParams)
		{
			$u = parse_url($path);
			if ($forceAuth)
				$u['query'] .= (strlen($u['query']) ? '&' : '') . 'needauth=1';
			if ($addParams)
				$u['query'] .= (strlen($u['query']) ? '&' : '') . $addParams;
			$path = $u['path'] . '?' . $u['query'];
		}
		return self::hasActivePaySystems($siteID, $checkMoneta ? "moneta" : false) ? $path : false;
	}

	public static function getPaymentLogo($siteID = false)
	{
		$result = '';
		$paymentPath = self::getPaymentPath($siteID, false);
		if ($paymentPath)
		{
			ob_start();
			?>
			<a href="<?=$paymentPath?>" style="*display: inline; display: inline-block; padding: 3px 7px 0 7px;">
				<img src="/bitrix/images/citrus.tszhpayment/visa.png" alt="<?=GetMessage("TSZH_PAYMENT_MONETA_FOOTER_TITLE")?>" title="<?=GetMessage("TSZH_PAYMENT_MONETA_FOOTER_TITLE")?>" height="25">
				<img src="/bitrix/images/citrus.tszhpayment/mastercard.png" alt="<?=GetMessage("TSZH_PAYMENT_MONETA_FOOTER_TITLE")?>" title="<?=GetMessage("TSZH_PAYMENT_MONETA_FOOTER_TITLE")?>" height="25">
				<img src="/bitrix/images/citrus.tszhpayment/mir.png" alt="<?=GetMessage("TSZH_PAYMENT_MONETA_FOOTER_TITLE")?>" title="<?=GetMessage("TSZH_PAYMENT_MONETA_FOOTER_TITLE")?>" height="25">
			</a>
			<a href="http://www.moneta.ru/" style="*display: inline; display: inline-block; line-height: 1.3; vertical-align: top; margin: 2px 0 0 5px;" target="_blank" rel="nofollow"><?=GetMessage("TSZH_PAYMENT_MONETA_FOOTER_TEXT")?></a>
			<?
			$result = ob_get_contents();
			ob_end_clean();
		}
		return $result;
	}

	/**
	 * ���������� �������� �������� �������� � ���� ������ citrus.tszh
	 * @param array $aGlobalMenu
	 * @param array $aModuleMenu
	 */
	public static function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
	{
		foreach($aModuleMenu as $k => &$v)
		{
			if($v['parent_menu']=='global_menu_services' && $v['section']=='citrus.tszh' && is_array($v['items']))
			{
				foreach ($v['items'] as &$item)
				{
					if ($item['items_id'] == 'menu_tszh_import_export')
					{
						$item['items'][] = Array(
							'text' => GetMessage('CITRUS_TSZHPAYMENT_EXPORT'),
							'title' => GetMessage('CITRUS_TSZHPAYMENT_EXPORT'),
							'url' => 'tszh_payments_export.php?lang='.LANGUAGE_ID,
							'more_url' => array('tszh_payments_export.php'),
						);
					}
				}
				break;
			}
		}
		unset($v);
	}
}

?>