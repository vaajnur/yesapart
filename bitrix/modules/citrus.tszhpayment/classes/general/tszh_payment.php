<? use Bitrix\Main\ModuleManager;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)die();

IncludeModuleLangFile(__FILE__);

if (ModuleManager::isModuleInstalled('otr.sale'))
{
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/otr.sale/include.php");
}

class CTszhPayment {

	// �������� ������ � ��������� ��������, ���� ������������ ���������������� ���� �������� ������; false, ���� �� ������������
	const USER_FIELD_ENTITY = 'TSZH_PAYMENT';

	public static function Add($arFields) {
		global $DB,$APPLICATION;

		if(!CTszhPayment::CheckFields("ADD", $arFields))
			return false;

		$ID = $DB->Add("b_tszh_payment", $arFields);
		if (self::USER_FIELD_ENTITY && $ID > 0)
			$GLOBALS['USER_FIELD_MANAGER']->Update(self::USER_FIELD_ENTITY, $ID, $arFields);

		if ($ID > 0)
		{
			if (!CTszhPaymentGateway::getInstance()->registerPayment($ID, $arFields))
			{
				self::logError($APPLICATION->GetException());
				self::Delete($ID);
				return false;
			}
		}

		return $ID;
	}

	public static function Update($ID, $arFields) {
		global $DB, $APPLICATION;

		$ID = IntVal($ID);
		if ($ID <= 0) {
			$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_NO_ID"), "TSZH_PAYMENT_NO_ID");
			return false;
		}

		if(!CTszhPayment::CheckFields('UPDATE', $arFields))
			return false;

		$wasPayed = self::isPayed($ID);

		$strUpdate = $DB->PrepareUpdate("b_tszh_payment", $arFields);
		$strSql =
			"UPDATE b_tszh_payment SET ".
				$strUpdate.
			" WHERE ID=".$ID;
		$bSuccess = $DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);
		if (self::USER_FIELD_ENTITY && $bSuccess)
			$GLOBALS['USER_FIELD_MANAGER']->Update(self::USER_FIELD_ENTITY, $ID, $arFields);

		if ($bSuccess)
		{
			if ($arFields["PAYED"] == "Y" && !$wasPayed)
			{
				$arPaymentFields = self::GetList(array(), array("ID" => $ID), false, false, Array("*", "UF_*"))->GetNext();
				if ($arPaymentFields["USER_EMAIL"])
				{
					$arPaymentFields["MAIL"] = $arPaymentFields["USER_EMAIL"];
					$arPaymentFields["TSZH"] = $arPaymentFields["TSZH_NAME"];
					$arPaymentFields["PAYSYSTEM"] = $arPaymentFields["PS_NAME"];
					$arPaymentFields["ACCOUNT"] = $arPaymentFields["ACCOUNT_ID"] ? $arPaymentFields["ACCOUNT_XML_ID"] : $arPaymentFields["C_ACCOUNT"];
					$arPaymentFields["FULL_ADDRESS"] = $arPaymentFields["ACCOUNT_ADDRESS_FULL"] ? $arPaymentFields["ACCOUNT_ADDRESS_FULL"] : $arPaymentFields["C_ADDRESS"];
					$arPaymentFields["SUMM"] = self::FormatCurrency($arPaymentFields["SUMM"], $arPaymentFields["CURRENCY"]);
					$arPaymentFields["SUMM_PAYED"] = self::FormatCurrency($arPaymentFields["SUMM_PAYED"], $arPaymentFields["CURRENCY"]);
					CEvent::SendImmediate("CITRUS_TSZHPAYMENT_RECEIVE", $arPaymentFields["LID"], $arPaymentFields);
				}
			}
			if (!CTszhPaymentGateway::getInstance()->registerPayment($ID, $arFields))
			{
				self::logError($APPLICATION->GetException(), $ID);
			}
		}

		return $bSuccess;
	}
    public static function SetCheck($ID)
    {

        global $DB;
        $ID = IntVal($ID);


        $arFields = self::GetList(array(), array("ID" => $ID), false, false, Array("*", "UF_*"))->GetNext();

        $cashboxId = null;
        $cashbox = \Otr\Sale\Cashbox\Internals\CashboxTable::getList(
            array(
                'select' => array('ID'),
                'filter' => array(
                    'ACTIVE' => 'Y',
                )
            )

        );
        while ($cashboxIds = $cashbox->fetchAll()) {
            $cashboxId = $cashboxIds;
        }

        $arCheckFields = self::GetList(array(), array("ID" => $ID), false, false, Array("*", "UF_*"))->GetNext();
        //���� ��� ����
        $arFieldsList = array(
            'CASHBOX_ID' => $cashboxId["0"]["ID"],
            'PAYMENT_ID' => $ID,
            'SHIPMENT_ID' => '2',
            'ORDER_ID' => $ID,
            'DATE_CREATE' => $arCheckFields["DATE_INSERT"],
            'SUM' => $arCheckFields["SUMM"],
            'CURRENCY' => $arCheckFields["CURRENCY"],
            'TYPE' => "sell",
            'STATUS'=> "N",

        );
        //<-���� ��� ����


        if($cashboxId > 0) {
            $arInsert = $DB->PrepareInsert("b_sale_cashbox_check", $arFieldsList);
            $strSql =
                "INSERT INTO b_sale_cashbox_check(" . $arInsert['0'] . ") VALUES(" . $arInsert['1'] . ")";;
            $bSuccess = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

            $check = \Otr\Sale\Cashbox\Internals\CashboxCheckTable::getList(
                array(
                    'order' => array('ID' => 'DESC'),
                    'select' => array('ID'),
                    'limit' => 1,

                )
            );
            while ($checkcahbox = $check->fetchAll()) {
                $check2cahbox = $checkcahbox;
            }

            $arFieldList = array(
                'CASHBOX_ID' => $cashboxId["0"]["ID"],
                'CHECK_ID' => $check2cahbox['0']['ID'],

            );
            $brInsert = $DB->PrepareInsert("b_sale_check2cashbox", $arFieldList);
            $strSql =
                "INSERT INTO b_sale_check2cashbox(" . $brInsert['0'] . ") VALUES(" . $brInsert['1'] . ")";;
            $dSuccess = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
	        // ��������� ������ �������� ������
	        if($info = CModule::CreateModuleObject('otr.sale')){
		        $testVersion = '1.1.0';
		        if(CheckVersion($info->MODULE_VERSION, $testVersion)){
			        if ($dSuccess)
			        {
				        \Otr\Sale\Cashbox\CheckManager::addByType($arFieldList, 'sell');
			        }
		        }

	        }
        }

    }
	/**
	 * ���������� true, ���� � ������� $ID ���������� ������� ������. False � ���� ������
	 * @param int $ID ID �������
	 * @return bool
	 */
	public static function isPayed($ID)
	{
		global $DB;
		return $DB->Query("select 'x' from b_tszh_payment where ID = " . IntVal($ID) . " and PAYED='Y';")->Fetch() ? true : false;
	}

	protected function CheckFields($strOperation, &$arFields) {
		global $APPLICATION;

		unset($arFields["ID"]);
		unset($arFields["DATE_PAYED"]);
		unset($arFields["EMP_PAYED_ID"]);
		unset($arFields["DATE_UPDATE"]);
		unset($arFields["DATE_INSERT"]);

		if (isset($arFields['TSZH_ID'])) {
			$arFields["TSZH_ID"] = IntVal($arFields["TSZH_ID"]);
			$arTszh = CTszh::GetByID($arFields["TSZH_ID"]);
			if (!is_array($arTszh)) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_TSZH_ID"), "TSZH_PAYMENT_ERROR_WRONG_TSZH_ID");
				return false;
			}
		}

		if (isset($arFields['ACCOUNT_ID'])) {
			$arFields["ACCOUNT_ID"] = IntVal($arFields["ACCOUNT_ID"]);
			$arAccount = CTszhAccount::GetByID($arFields["ACCOUNT_ID"]);
			if (!is_array($arAccount)) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_ACCOUNT_ID"), "TSZH_PAYMENT_ERROR_WRONG_ACCOUNT_ID");
				return false;
			}
		}

		if (isset($arFields['USER_ID'])) {
			$arFields["USER_ID"] = IntVal($arFields["USER_ID"]);
			$arUser = CUser::GetByID($arFields["USER_ID"])->Fetch();
			if (!is_array($arUser)) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_USER_ID"), "TSZH_PAYMENT_ERROR_WRONG_USER_ID");
				return false;
			}
		}

		if ($strOperation == 'ADD' || isset($arFields["LID"])) {
			if (!isset($arFields['LID']) || strlen(trim($arFields['LID'])) <= 0) {
				$arFields["LID"] = SITE_ID;
				//$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_NO_LID"), "TSZH_PAYMENT_ERROR_NO_LID");
				//return false;
			}
			$rsSite = CSite::GetByID($arFields['LID']);
			$arSite = $rsSite->Fetch();
			if (!is_array($arSite)) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_LID_DOESNT_EXISTS"), "TSZH_PAYMENT_ERROR_LID_DOESNT_EXISTS");
				return false;
			}
		}

		if ($strOperation == "ADD" || array_key_exists("PAYED", $arFields)) {
			$arFields["PAYED"] = $arFields["PAYED"] == "Y" ? "Y" : "N";
			if ($arFields['PAYED'] == "Y") {
				$arFields["DATE_PAYED"] = ConvertTimeStamp(time(), "FULL");
				global $USER;
				$arFields["EMP_PAYED_ID"] = ( IntVal($USER->GetID())>0 ? IntVal($USER->GetID()) : false );
				//$arFields["EMP_PAYED_ID"] = $USER->GetID();
			} else {
				$arFields["DATE_PAYED"] = false;
				$arFields["EMP_PAYED_ID"] = false;
			}
		}

		if ($strOperation == "ADD" || isset($arFields["SUMM"])) {
			if (!isset($arFields["SUMM"])) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_NO_SUMM"), "TSZH_PAYMENT_ERROR_NO_SUMM");
				return false;
			}
			if (!is_numeric($arFields["SUMM"])) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_SUMM"), "TSZH_PAYMENT_ERROR_WRONG_SUMM");
				return false;
			}
			$arFields["SUMM"] = FloatVal($arFields["SUMM"]);
			if ($arFields["SUMM"] <= 0) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_SUMM"), "TSZH_PAYMENT_ERROR_WRONG_SUMM");
				return false;
			}
		}

		if (isset($arFields["SUMM_PAYED"])) {
			if (!is_numeric($arFields["SUMM_PAYED"])) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_SUMM_PAYED"), "TSZH_PAYMENT_ERROR_WRONG_SUMM_PAYED");
				return false;
			}
			$arFields["SUMM_PAYED"] = FloatVal($arFields["SUMM_PAYED"]);
			if ($arFields["SUMM_PAYED"] < 0) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_SUMM_PAYED"), "TSZH_PAYMENT_ERROR_WRONG_SUMM_PAYED");
				return false;
			}
		}

		if ($strOperation == "ADD" || isset($arFields["CURRENCY"])) {
			if (!isset($arFields["CURRENCY"]) || strlen(trim($arFields["CURRENCY"])) <= 0) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_NO_CURRENCY"), "TSZH_PAYMENT_ERROR_NO_CURRENCY");
				return false;
			}
		}

		if ($strOperation == 'ADD' || isset($arFields["PAY_SYSTEM_ID"])) {
			if (!isset($arFields["PAY_SYSTEM_ID"]) || IntVal($arFields["PAY_SYSTEM_ID"]) <= 0) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_NO_PAY_SYSTEM"), "TSZH_PAYMENT_ERROR_NO_PAY_SYSTEM");
				return false;
			}
			$arPaySystem = CTszhPaySystem::GetByID($arFields["PAY_SYSTEM_ID"]);
			if (!is_array($arPaySystem)) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_PAY_SYSTEM_DOESNT_EXISTS"), "TSZH_PAYMENT_ERROR_PAY_SYSTEM_DOESNT_EXISTS");
				return false;
			}
		}

		if ($strOperation == 'ADD') {
			$arFields["DATE_INSERT"] = ConvertTimeStamp(time(), "FULL");
		}
		$arFields["DATE_UPDATE"] = ConvertTimeStamp(time(), "FULL");

		return true;
	}

	/**
	 * @param array $arOrder
	 * @param array $arFilter
	 * @param bool|array $arGroupBy
	 * @param bool|array $arNavStartParams
	 * @param array $arSelectFields
	 * @return bool|CDBResult
	 */
	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		// FIELDS -->
		$arFields = array(
			"ID" => array("FIELD" => "TP.ID", "TYPE" => "int"),
			"LID" => array("FIELD" => "TP.LID", "TYPE" => "string"),
			"PAYED" => array("FIELD" => "TP.PAYED", "TYPE" => "char"),
			"DATE_PAYED" => array("FIELD" => "TP.DATE_PAYED", "TYPE" => "datetime"),
			"EMP_PAYED_ID" => array("FIELD" => "TP.EMP_PAYED_ID", "TYPE" => "int"),
			"SUMM" => array("FIELD" => "TP.SUMM", "TYPE" => "double"),
			"SUMM_PAYED" => array("FIELD" => "TP.SUMM_PAYED", "TYPE" => "double"),
			"CURRENCY" => array("FIELD" => "TP.CURRENCY", "TYPE" => "string"),
			"TSZH_ID" => array("FIELD" => "TP.TSZH_ID", "TYPE" => "int"),
			"USER_ID" => array("FIELD" => "TP.USER_ID", "TYPE" => "int"),
			"PAY_SYSTEM_ID" => array("FIELD" => "TP.PAY_SYSTEM_ID", "TYPE" => "int"),
			"ACCOUNT_ID" => array("FIELD" => "TP.ACCOUNT_ID", "TYPE" => "int"),
			"EMAIL"=> array("FIELD" => "TP.EMAIL", "TYPE" => "string"),
			"PSYS_STATUS" => array("FIELD" => "TP.PSYS_STATUS", "TYPE" => "string"),
			"PSYS_STATUS_DESCR" => array("FIELD" => "TP.PSYS_STATUS_DESCR", "TYPE" => "string"),
			"PSYS_STATUS_RESPONSE" => array("FIELD" => "TP.PSYS_STATUS_RESPONSE", "TYPE" => "string"),
			"PSYS_STATUS_DATE" => array("FIELD" => "TP.PSYS_STATUS_DATE", "TYPE" => "datetime"),
			"PSYS_OPERATION_ID" => array("FIELD" => "TP.PSYS_OPERATION_ID", "TYPE" => "string"),
			"PSYS_FEE" => array("FIELD" => "TP.PSYS_FEE", "TYPE" => "double"),
            "DATE_INSERT" => array("FIELD" => "TP.DATE_INSERT", "TYPE" => "datetime"),
            "DATE_UPDATE" => array("FIELD" => "TP.DATE_UPDATE", "TYPE" => "datetime"),
            "INSURANCE_INCLUDED" => array("FIELD" => "TP.INSURANCE_INCLUDED", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh T ON (TP.TSZH_ID = T.ID)"),
			"IS_OVERHAUL" => array("FIELD" => "TP.IS_OVERHAUL", "TYPE" => "string"),
			"IS_PENALTY" => array("FIELD" => "TP.IS_PENALTY", "TYPE" => "string"),

            "C_PAYEE_NAME" => array("FIELD" => "TP.C_PAYEE_NAME", "TYPE" => "string"),
            "C_ADDRESS" => array("FIELD" => "TP.C_ADDRESS", "TYPE" => "string"),
            "C_ACCOUNT" => array("FIELD" => "TP.C_ACCOUNT", "TYPE" => "string"),
            "C_COMMENTS" => array("FIELD" => "TP.C_COMMENTS", "TYPE" => "string"),

            "PS_LID" => array("FIELD" => "TPS.LID", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_CURRENCY" => array("FIELD" => "TPS.CURRENCY", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_NAME" => array("FIELD" => "TPS.NAME", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_ACTIVE" => array("FIELD" => "TPS.ACTIVE", "TYPE" => "char", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_SORT" => array("FIELD" => "TPS.SORT", "TYPE" => "int", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_DESCRIPTION" => array("FIELD" => "TPS.DESCRIPTION", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),

            "PS_ACTION_FILE" => array("FIELD" => "TPS.ACTION_FILE", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_RESULT_FILE" => array("FIELD" => "TPS.RESULT_FILE", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_NEW_WINDOW" => array("FIELD" => "TPS.NEW_WINDOW", "TYPE" => "char", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_PARAMS" => array("FIELD" => "TPS.PARAMS", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_HAVE_PAYMENT" => array("FIELD" => "TPS.HAVE_PAYMENT", "TYPE" => "char", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_HAVE_ACTION" => array("FIELD" => "TPS.HAVE_ACTION", "TYPE" => "char", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_HAVE_RESULT" => array("FIELD" => "TPS.HAVE_RESULT", "TYPE" => "char", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_HAVE_PREPAY" => array("FIELD" => "TPS.HAVE_PREPAY", "TYPE" => "char", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_HAVE_RESULT_RECEIVE" => array("FIELD" => "TPS.HAVE_RESULT_RECEIVE", "TYPE" => "char", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),
            "PS_ENCODING" => array("FIELD" => "TPS.ENCODING", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_pay_system TPS ON (TP.PAY_SYSTEM_ID=TPS.ID)"),

            "ACCOUNT_XML_ID" => Array("FIELD" => "UA.XML_ID", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_TSZH_ID" => Array("FIELD" => "UA.TSZH_ID", "TYPE" => "int", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_EXTERNAL_ID" => Array("FIELD" => "UA.EXTERNAL_ID", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_NAME" => Array("FIELD" => "UA.NAME", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_CITY" => Array("FIELD" => "UA.CITY", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_DISTRICT" => Array("FIELD" => "UA.DISTRICT", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_REGION" => Array("FIELD" => "UA.REGION", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_SETTLEMENT" => Array("FIELD" => "UA.SETTLEMENT", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_STREET" => Array("FIELD" => "UA.STREET", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_HOUSE" => Array("FIELD" => "UA.HOUSE", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_FLAT" => Array("FIELD" => "UA.FLAT", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_AREA" => Array("FIELD" => "UA.AREA", "TYPE" => "double", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_LIVING_AREA" => Array("FIELD" => "UA.LIVING_AREA", "TYPE" => "double", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_PEOPLE" => Array("FIELD" => "UA.PEOPLE", "TYPE" => "int", "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),
            "ACCOUNT_ADDRESS_FULL" => Array("FIELD" => "CONCAT(UA.CITY,' ',UA.STREET,' ',UA.HOUSE,' ',UA.FLAT)", "TYPE" => "string", "CONCAT" => true, "FROM" => "LEFT JOIN b_tszh_accounts UA ON (TP.ACCOUNT_ID = UA.ID)"),

            "USER_EMAIL" => Array("FIELD" => "U.EMAIL", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (TP.USER_ID = U.ID)"),
            "TSZH_NAME" => array("FIELD" => "T.NAME", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh T ON (TP.TSZH_ID = T.ID)"),

		);
		// <-- FIELDS

        /**
         * ��������� ������ ������ ���
         */
        if($info = CModule::CreateModuleObject('citrus.tszh'))
        {
            $testVersion = '14.6.5';
            if(CheckVersion($testVersion, $info->MODULE_VERSION)){}
            else
            {
                $arFields["ACCOUNT_CITY"] = Array("FIELD" => "HS.CITY", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house HS ON (UA.HOUSE_ID = HS.ID)");
                $arFields["ACCOUNT_DISTRICT"] = Array("FIELD" => "HS.DISTRICT", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house HS ON (UA.HOUSE_ID = HS.ID)");
                $arFields["ACCOUNT_REGION"] = Array("FIELD" => "HS.REGION", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house HS ON (UA.HOUSE_ID = HS.ID)");
                $arFields["ACCOUNT_SETTLEMENT"] = Array("FIELD" => "HS.SETTLEMENT", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house HS ON (UA.HOUSE_ID = HS.ID)");
                $arFields["ACCOUNT_STREET"] = Array("FIELD" => "HS.STREET", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house HS ON (UA.HOUSE_ID = HS.ID)");
                $arFields["ACCOUNT_HOUSE"] = Array("FIELD" => "HS.HOUSE", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house HS ON (UA.HOUSE_ID = HS.ID)");
                $arFields["ACCOUNT_ADDRESS_FULL"] = Array("FIELD" => "CONCAT(HS.CITY,' ',HS.STREET,' ',HS.HOUSE,' ',UA.FLAT)", "TYPE" => "string", "CONCAT" => true, "FROM" => "LEFT JOIN b_tszh_house HS ON (UA.HOUSE_ID = HS.ID)");
            }
        }

		if (count($arSelectFields) <= 0) {
			$arSelectFields = Array("ID", "LID", "PAYED", "DATE_PAYED", "EMP_PAYED_ID", "SUMM", "SUMM_PAYED", "CURRENCY", "TSZH_ID", "USER_ID", "PAY_SYSTEM_ID", "ACCOUNT_ID", "PSYS_STATUS", "PSYS_STATUS_DESCR", "PSYS_STATUS_RESPONSE", "PSYS_STATUS_DATE", "DATE_INSERT", "DATE_UPDATE", "C_ADDRESS", "C_PAYEE_NAME", "C_ACCOUNT", "C_COMMENTS", "INSURANCE_INCLUDED");
		} elseif (($idx = array_search('*', $arSelectFields)) !== false) {
			unset($arSelectFields[$idx]);
			$arSelectFields = array_merge(array_keys($arFields), $arSelectFields);
		}

		if (self::USER_FIELD_ENTITY)
		{
			$obUserFieldsSql = new CUserTypeSQL;
			$obUserFieldsSql->SetEntity(self::USER_FIELD_ENTITY, "TP.ID");
			$obUserFieldsSql->SetSelect($arSelectFields);
			$obUserFieldsSql->SetFilter($arFilter);
			$obUserFieldsSql->SetOrder($arOrder);
			
			$dbRes = CTszh::GetListMakeQuery('b_tszh_payment TP', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields,  $obUserFieldsSql, "TP.ID");
		}
		else
			$dbRes = CTszh::GetListMakeQuery('b_tszh_payment TP', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

		if (self::USER_FIELD_ENTITY && is_object($dbRes))
			$dbRes->SetUserFields($GLOBALS['USER_FIELD_MANAGER']->GetUserFields(self::USER_FIELD_ENTITY));

		return $dbRes;
	}

	public static function Delete($ID) {
		global $DB, $APPLICATION;

		$ID = intval($ID);
		if ($ID <= 0) {
			$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_ID"), "TSZH_PAYMENT_ERROR_WRONG_ID");
			return false;
		}

		$DB->StartTransaction();

		$strSql = "DELETE FROM b_tszh_payment WHERE ID=".$ID;
		$DB->Query($strSql, false, "FILE: ".__FILE__."<br> LINE: ".__LINE__);

		if (self::USER_FIELD_ENTITY)
			$GLOBALS['USER_FIELD_MANAGER']->Delete(self::USER_FIELD_ENTITY, $ID);

		CTszhPaymentGateway::getInstance()->deletePayment($ID);

		$DB->Commit();

		return true;
	}

	public static function GetByID($ID)
	{
		global $APPLICATION;

		$ID = IntVal($ID);
		if ($ID <= 0) {
			$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_ID"), "TSZH_PAYMENT_ERROR_WRONG_ID");
			return false;
		}

		$rsPayment = CTszhPayment::GetList(Array(), Array("ID" => $ID), false, Array('nTopCount' => 1));
		return $rsPayment->Fetch();
	}

	public static function Pay($ID, $val)
	{
		$ID = IntVal($ID);
		$val = (($val != "Y") ? "N" : "Y");

		if ($ID <= 0) {
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("TSZH_PAYMENT_NO_PAYMENT_ID"), "NO_PAYMENT_ID");
			return False;
		}

		$arPayment = CTszhPayment::GetByID($ID);
		if (!$arPayment) {
			$GLOBALS["APPLICATION"]->ThrowException(str_replace("#ID#", $ID, GetMessage("TSZH_PAYMENT_NOT_FOUND")), "PAYMENT_NOT_FOUND");
			return False;
		}

		if ($arPayment["PAYED"] == $val) {
			$GLOBALS["APPLICATION"]->ThrowException(str_replace("#ID#", $ID, GetMessage("TSZH_PAYMENT_DUB_PAY")), "ALREADY_FLAG");
			return False;
		}

		$arFields = array("PAYED" => $val);
		$res = CTszhPayment::Update($ID, $arFields);

		return $res;
	}

	public static function FormatCurrency($shouldPay, $currency)
	{
		if (function_exists('CurrencyFormat'))
			return CurrencyFormat($shouldPay, $currency);
			
		$arCurrencyNames = Array(
			"RUB" => "%.2f " . GetMessage('TSZH_PAYMENT_RUB_ABBR'),
			"UAH" => "%.2f " . GetMessage('TSZH_PAYMENT_UAH_ABBR'),
		);
		if (array_key_exists($currency, $arCurrencyNames))
			return sprintf($arCurrencyNames[$currency], $shouldPay);
		else
			return sprintf("%.2f", $shouldPay) . ' ' . $currency;
	}

	/**
	 * ��������� ������ �� ������ � ������ �������
	 * @param CApplicationException|string $message
	 * @param bool $ID
	 */
	protected static function logError($message, $ID = false)
	{
		CEventLog::Add(Array(
			"SEVERITY" => "ERROR",
			"AUDIT_TYPE_ID" => "PAYMENT",
			"MODULE_ID" => "citrus.tszhpayment",
			"ITEM_ID" => $ID,
			"DESCRIPTION" => nl2br(is_object($message) ? $message->GetString() : $message),
		));
	}
}

?>