<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

IncludeModuleLangFile(__FILE__);

class CTszhFlysheets {

	// �������� ������ � ��������� ��������, ���� ������������ ���������������� ���� �������� ������; false, ���� �� ������������
	const USER_FIELD_ENTITY = 'TSZH_FLYSHEETS';

	public static function Add($arFields) {
		global $DB,$APPLICATION;

		if(!CTszhF::CheckFields("ADD", $arFields))
			return false;

		$ID = $DB->Add("b_tszh_flysheets", $arFields);
		if (self::USER_FIELD_ENTITY && $ID > 0)
			$GLOBALS['USER_FIELD_MANAGER']->Update(self::USER_FIELD_ENTITY, $ID, $arFields);

		if ($ID > 0)
		{
			if (!CTszhPaymentGateway::getInstance()->registerPayment($ID, $arFields))
			{
				self::logError($APPLICATION->GetException());
				self::Delete($ID);
				return false;
			}
		}

		return $ID;
	}

	public static function Update($ID, $arFields) {
		global $DB, $APPLICATION;

		$ID = IntVal($ID);
		if ($ID <= 0) {
			$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_NO_ID"), "TSZH_PAYMENT_NO_ID");
			return false;
		}

		if(!CTszhFlysheets::CheckFields('UPDATE', $arFields))
			return false;

		$wasPayed = self::isPayed($ID);

		$strUpdate = $DB->PrepareUpdate("b_tszh_flysheets", $arFields);
		$strSql =
			"UPDATE b_tszh_flysheets SET ".
				$strUpdate.
			" WHERE ID=".$ID;
		$bSuccess = $DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);
		if (self::USER_FIELD_ENTITY && $bSuccess)
			$GLOBALS['USER_FIELD_MANAGER']->Update(self::USER_FIELD_ENTITY, $ID, $arFields);

		if ($bSuccess)
		{
			if ($arFields["PAYED"] == "Y" && !$wasPayed)
			{
				$arPaymentFields = self::GetList(array(), array("ID" => $ID), false, false, Array("*", "UF_*"))->GetNext();
				if ($arPaymentFields["USER_EMAIL"])
				{
					$arPaymentFields["MAIL"] = $arPaymentFields["USER_EMAIL"];
					$arPaymentFields["TSZH"] = $arPaymentFields["TSZH_NAME"];
					$arPaymentFields["PAYSYSTEM"] = $arPaymentFields["PS_NAME"];
					$arPaymentFields["ACCOUNT"] = $arPaymentFields["ACCOUNT_ID"] ? $arPaymentFields["ACCOUNT_XML_ID"] : $arPaymentFields["C_ACCOUNT"];
					$arPaymentFields["FULL_ADDRESS"] = $arPaymentFields["ACCOUNT_ADDRESS_FULL"] ? $arPaymentFields["ACCOUNT_ADDRESS_FULL"] : $arPaymentFields["C_ADDRESS"];
					$arPaymentFields["SUMM"] = self::FormatCurrency($arPaymentFields["SUMM"], $arPaymentFields["CURRENCY"]);
					$arPaymentFields["SUMM_PAYED"] = self::FormatCurrency($arPaymentFields["SUMM_PAYED"], $arPaymentFields["CURRENCY"]);
					CEvent::SendImmediate("CITRUS_TSZHPAYMENT_RECEIVE", $arPaymentFields["LID"], $arPaymentFields);
				}
			}
			if (!CTszhPaymentGateway::getInstance()->registerPayment($ID, $arFields))
			{
				self::logError($APPLICATION->GetException(), $ID);
			}
		}

		return $bSuccess;
	}

	/**
	 * ���������� true, ���� � ������� $ID ���������� ������� ������. False � ���� ������
	 * @param int $ID ID �������
	 * @return bool
	 */
	public static function isPayed($ID)
	{
		global $DB;
		return $DB->Query("select 'x' from b_tszh_payment where ID = " . IntVal($ID) . " and PAYED='Y';")->Fetch() ? true : false;
	}

	protected function CheckFields($strOperation, &$arFields) {
		global $APPLICATION;

		unset($arFields["ID"]);
		unset($arFields["DATE_PAYED"]);
		unset($arFields["EMP_PAYED_ID"]);
		unset($arFields["DATE_UPDATE"]);
		unset($arFields["DATE_INSERT"]);

		if (isset($arFields['TSZH_ID'])) {
			$arFields["TSZH_ID"] = IntVal($arFields["TSZH_ID"]);
			$arTszh = CTszh::GetByID($arFields["TSZH_ID"]);
			if (!is_array($arTszh)) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_TSZH_ID"), "TSZH_PAYMENT_ERROR_WRONG_TSZH_ID");
				return false;
			}
		}

		if (isset($arFields['ACCOUNT_ID'])) {
			$arFields["ACCOUNT_ID"] = IntVal($arFields["ACCOUNT_ID"]);
			$arAccount = CTszhAccount::GetByID($arFields["ACCOUNT_ID"]);
			if (!is_array($arAccount)) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_ACCOUNT_ID"), "TSZH_PAYMENT_ERROR_WRONG_ACCOUNT_ID");
				return false;
			}
		}

		if (isset($arFields['USER_ID'])) {
			$arFields["USER_ID"] = IntVal($arFields["USER_ID"]);
			$arUser = CUser::GetByID($arFields["USER_ID"])->Fetch();
			if (!is_array($arUser)) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_USER_ID"), "TSZH_PAYMENT_ERROR_WRONG_USER_ID");
				return false;
			}
		}

		if ($strOperation == 'ADD' || isset($arFields["LID"])) {
			if (!isset($arFields['LID']) || strlen(trim($arFields['LID'])) <= 0) {
				$arFields["LID"] = SITE_ID;
				//$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_NO_LID"), "TSZH_PAYMENT_ERROR_NO_LID");
				//return false;
			}
			$rsSite = CSite::GetByID($arFields['LID']);
			$arSite = $rsSite->Fetch();
			if (!is_array($arSite)) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_LID_DOESNT_EXISTS"), "TSZH_PAYMENT_ERROR_LID_DOESNT_EXISTS");
				return false;
			}
		}

		if ($strOperation == "ADD" || array_key_exists("PAYED", $arFields)) {
			$arFields["PAYED"] = $arFields["PAYED"] == "Y" ? "Y" : "N";
			if ($arFields['PAYED'] == "Y") {
				$arFields["DATE_PAYED"] = ConvertTimeStamp(time(), "FULL");
				global $USER;
				$arFields["EMP_PAYED_ID"] = ( IntVal($USER->GetID())>0 ? IntVal($USER->GetID()) : false );
				//$arFields["EMP_PAYED_ID"] = $USER->GetID();
			} else {
				$arFields["DATE_PAYED"] = false;
				$arFields["EMP_PAYED_ID"] = false;
			}
		}

		if ($strOperation == "ADD" || isset($arFields["SUMM"])) {
			if (!isset($arFields["SUMM"])) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_NO_SUMM"), "TSZH_PAYMENT_ERROR_NO_SUMM");
				return false;
			}
			if (!is_numeric($arFields["SUMM"])) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_SUMM"), "TSZH_PAYMENT_ERROR_WRONG_SUMM");
				return false;
			}
			$arFields["SUMM"] = FloatVal($arFields["SUMM"]);
			if ($arFields["SUMM"] <= 0) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_SUMM"), "TSZH_PAYMENT_ERROR_WRONG_SUMM");
				return false;
			}
		}

		if (isset($arFields["SUMM_PAYED"])) {
			if (!is_numeric($arFields["SUMM_PAYED"])) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_SUMM_PAYED"), "TSZH_PAYMENT_ERROR_WRONG_SUMM_PAYED");
				return false;
			}
			$arFields["SUMM_PAYED"] = FloatVal($arFields["SUMM_PAYED"]);
			if ($arFields["SUMM_PAYED"] < 0) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_SUMM_PAYED"), "TSZH_PAYMENT_ERROR_WRONG_SUMM_PAYED");
				return false;
			}
		}

		if ($strOperation == "ADD" || isset($arFields["CURRENCY"])) {
			if (!isset($arFields["CURRENCY"]) || strlen(trim($arFields["CURRENCY"])) <= 0) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_NO_CURRENCY"), "TSZH_PAYMENT_ERROR_NO_CURRENCY");
				return false;
			}
		}

		if ($strOperation == 'ADD' || isset($arFields["PAY_SYSTEM_ID"])) {
			if (!isset($arFields["PAY_SYSTEM_ID"]) || IntVal($arFields["PAY_SYSTEM_ID"]) <= 0) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_NO_PAY_SYSTEM"), "TSZH_PAYMENT_ERROR_NO_PAY_SYSTEM");
				return false;
			}
			$arPaySystem = CTszhPaySystem::GetByID($arFields["PAY_SYSTEM_ID"]);
			if (!is_array($arPaySystem)) {
				$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_PAY_SYSTEM_DOESNT_EXISTS"), "TSZH_PAYMENT_ERROR_PAY_SYSTEM_DOESNT_EXISTS");
				return false;
			}
		}

		if ($strOperation == 'ADD') {
			$arFields["DATE_INSERT"] = ConvertTimeStamp(time(), "FULL");
		}
		$arFields["DATE_UPDATE"] = ConvertTimeStamp(time(), "FULL");

		return true;
	}

	/**
	 * @param array $arOrder
	 * @param array $arFilter
	 * @param bool|array $arGroupBy
	 * @param bool|array $arNavStartParams
	 * @param array $arSelectFields
	 * @return bool|CDBResult
	 */
	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		// FIELDS -->
		$arFields = array(
			"ID" => array("FIELD" => "TF.ID", "TYPE" => "int"),
            "DESCRIPTION" => array("FIELD" => "TF.DESCRIPTION", "TYPE" => "string" ),
            "IMG_SRC" => array("FIELD" => "TF.IMG_SRC", "TYPE" => "string" ),
		);
		// <-- FIELDS

        /**
         * ��������� ������ ������ ���
         */
        if($info = CModule::CreateModuleObject('citrus.tszh'))
        {
            $testVersion = '14.6.5';
        }

		if (count($arSelectFields) <= 0) {
			$arSelectFields = Array("ID");
		} elseif (($idx = array_search('*', $arSelectFields)) !== false) {
			unset($arSelectFields[$idx]);
			$arSelectFields = array_merge(array_keys($arFields), $arSelectFields);
		}

		if (self::USER_FIELD_ENTITY)
		{
			$obUserFieldsSql = new CUserTypeSQL;
			$obUserFieldsSql->SetEntity(self::USER_FIELD_ENTITY, "TF.ID");
			$obUserFieldsSql->SetSelect($arSelectFields);
			$obUserFieldsSql->SetFilter($arFilter);
			$obUserFieldsSql->SetOrder($arOrder);
			
			$dbRes = CTszh::GetListMakeQuery('b_tszh_flysheets TF', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields,  $obUserFieldsSql, "TP.ID");
		}
		else
			$dbRes = CTszh::GetListMakeQuery('b_tszh_flysheets TF', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

		if (self::USER_FIELD_ENTITY && is_object($dbRes))
			$dbRes->SetUserFields($GLOBALS['USER_FIELD_MANAGER']->GetUserFields(self::USER_FIELD_ENTITY));

		return $dbRes;
	}

	public static function Delete($ID) {
		global $DB, $APPLICATION;

		$ID = intval($ID);
		if ($ID <= 0) {
			$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_ID"), "TSZH_PAYMENT_ERROR_WRONG_ID");
			return false;
		}

		$DB->StartTransaction();

		$strSql = "DELETE FROM b_tszh_payment WHERE ID=".$ID;
		$DB->Query($strSql, false, "FILE: ".__FILE__."<br> LINE: ".__LINE__);

		if (self::USER_FIELD_ENTITY)
			$GLOBALS['USER_FIELD_MANAGER']->Delete(self::USER_FIELD_ENTITY, $ID);

		CTszhPaymentGateway::getInstance()->deletePayment($ID);

		$DB->Commit();

		return true;
	}

	public static function GetByID($ID)
	{
		global $APPLICATION;

		$ID = IntVal($ID);
		if ($ID <= 0) {
			$APPLICATION->ThrowException(GetMessage("TSZH_PAYMENT_ERROR_WRONG_ID"), "TSZH_PAYMENT_ERROR_WRONG_ID");
			return false;
		}

		$rsPayment = CTszhFlysheets::GetList(Array(), Array("ID" => $ID), false, Array('nTopCount' => 1));
		return $rsPayment->Fetch();
	}

	public static function Pay($ID, $val)
	{
		$ID = IntVal($ID);
		$val = (($val != "Y") ? "N" : "Y");

		if ($ID <= 0) {
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("TSZH_PAYMENT_NO_PAYMENT_ID"), "NO_PAYMENT_ID");
			return False;
		}

		$arFlysheet = CTszhFlysheets::GetByID($ID);
		if (!$arFlysheet) {
			$GLOBALS["APPLICATION"]->ThrowException(str_replace("#ID#", $ID, GetMessage("TSZH_PAYMENT_NOT_FOUND")), "PAYMENT_NOT_FOUND");
			return False;
		}

		if ($arFlysheet["PAYED"] == $val) {
			$GLOBALS["APPLICATION"]->ThrowException(str_replace("#ID#", $ID, GetMessage("TSZH_PAYMENT_DUB_PAY")), "ALREADY_FLAG");
			return False;
		}

		$arFields = array("PAYED" => $val);
		$res = CTszhFlysheets::Update($ID, $arFields);

		return $res;
	}

	public static function FormatCurrency($shouldPay, $currency)
	{
		if (function_exists('CurrencyFormat'))
			return CurrencyFormat($shouldPay, $currency);
			
		$arCurrencyNames = Array(
			"RUB" => "%.2f " . GetMessage('TSZH_PAYMENT_RUB_ABBR'),
			"UAH" => "%.2f " . GetMessage('TSZH_PAYMENT_UAH_ABBR'),
		);
		if (array_key_exists($currency, $arCurrencyNames))
			return sprintf($arCurrencyNames[$currency], $shouldPay);
		else
			return sprintf("%.2f", $shouldPay) . ' ' . $currency;
	}

	/**
	 * ��������� ������ �� ������ � ������ �������
	 * @param CApplicationException|string $message
	 * @param bool $ID
	 */
	protected static function logError($message, $ID = false)
	{
		CEventLog::Add(Array(
			"SEVERITY" => "ERROR",
			"AUDIT_TYPE_ID" => "PAYMENT",
			"MODULE_ID" => "citrus.tszhpayment",
			"ITEM_ID" => $ID,
			"DESCRIPTION" => nl2br(is_object($message) ? $message->GetString() : $message),
		));
	}
    /**
     * ���������� ����������� ������ ��� ������ "��������������� �������� ������" � �������������� �������
     * @param array $items ������ ��������� ����
     */

    public static function MyOnAdminContextMenuShow(&$items)
    {
        //add custom button to the index page toolbar
        if($GLOBALS["APPLICATION"]->GetCurPage(true) == "/bitrix/admin/tszh_flysheets_list.php") {
            $GLOBALS["APPLICATION"]->SetAdditionalCSS("/bitrix/themes/.default/citrus.tszhpayment.admin.css");
        }
    }
    public static function ShowBannerHtml()
    {
        echo "<a href='tszh_flysheets_list.php?lang=" . LANG . "'>"
             ."<img style='margin: 0 10px 10px 0; display: block; float: left; min-width: 670px; width: calc(100% - 690px)' src='/bitrix/images/citrus.tszhpayment/banners/banner_flysheets_admin.gif'/>"
             ."</a>"
             ."<style>.adm-filter-wrap:after{display:block;content:'';clear:both;}</style>";
    }
}

?>