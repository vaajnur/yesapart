CREATE TABLE `b_tszh_pay_system` (
  `ID` int(11) NOT NULL auto_increment,
  `LID` char(2) NOT NULL,
  `TSZH_ID` int(11) NOT NULL,
  `CURRENCY` char(3) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `ACTIVE` char(1) NOT NULL default 'Y',
  `SORT` int(11) NOT NULL default '100',
  `DESCRIPTION` text,
  `ACTION_FILE` varchar(255) default NULL,
  `RESULT_FILE` varchar(255) default NULL,
  `NEW_WINDOW` char(1) NOT NULL default 'Y',
  `PARAMS` text,
  `HAVE_PAYMENT` char(1) NOT NULL default 'N',
  `HAVE_ACTION` char(1) NOT NULL default 'N',
  `HAVE_RESULT` char(1) NOT NULL default 'N',
  `HAVE_PREPAY` char(1) NOT NULL default 'N',
  `HAVE_RESULT_RECEIVE` char(1) NOT NULL default 'N',
  `ENCODING` varchar(45) default NULL,
  PRIMARY KEY  (`ID`),
  KEY `IXS_PAY_SYSTEM_LID` (`LID`),
  KEY `IXS_ACTIVE` (`ACTIVE`)
);

CREATE TABLE `b_tszh_payment` (
  `ID` int(11) NOT NULL auto_increment,
  `LID` char(2) NOT NULL,
  `PAYED` char(1) NOT NULL default 'N',
  `DATE_PAYED` datetime default NULL,
  `EMP_PAYED_ID` int(11) default NULL,
  `SUMM` decimal(18,2) NOT NULL,
  `SUMM_PAYED` decimal(18,2) NOT NULL,
  `CURRENCY` char(3) NOT NULL,
  `TSZH_ID` INT(11) NULL ,
  `USER_ID` int(11) NULL,
  `PAY_SYSTEM_ID` int(11) default NULL,
  `ACCOUNT_ID` int(11) NULL,
  `EMAIL` varchar(255) default NULL,
  `PSYS_STATUS` varchar(255) NULL,
  `PSYS_STATUS_DESCR` varchar(255) NULL,
  `PSYS_STATUS_RESPONSE` text NULL,
  `PSYS_STATUS_DATE` datetime NULL,
  `PSYS_OPERATION_ID` varchar(255) NULL,
  `PSYS_FEE` decimal(18,2) NULL,
  `DATE_INSERT` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `C_PAYEE_NAME` VARCHAR(100) NULL ,
  `C_ADDRESS` VARCHAR(255) NULL ,
  `C_ACCOUNT` VARCHAR(50) NULL ,
  `C_COMMENTS` VARCHAR(255) NULL ,
  `INSURANCE_INCLUDED` int(1) NOT NULL default 0,
  `IS_OVERHAUL` CHAR(1) NULL,
  `IS_PENALTY` CHAR(1) NULL,
  PRIMARY KEY  (`ID`),
  KEY `IXS_PAYMENT_USER_ID` (`USER_ID`),
  KEY `IXS_PAYMENT_TSZH_ID` (`TSZH_ID`),
  KEY `IXS_PAYMENT_PAYED` (`PAYED`)
);

CREATE TABLE `b_tszh_payment_services` (
  `ID` int(11) NOT NULL auto_increment,
  `GUID` varchar(500) default NULL,
  `SERVICE_NAME` varchar(500) default NULL,
  `SUMM_PAYED` varchar(255) default NULL,
  `PAYMENT_ID` int(11) NULL,
  PRIMARY KEY  (`ID`)
);

CREATE TABLE `b_tszh_flysheets` (
  `ID` int(11) NOT NULL auto_increment,
  `DESCRIPTION` varchar(255) NULL,
  `FILE_ID` int(11) NULL,
  PRIMARY KEY  (`ID`)
);

CREATE TABLE `b_tszh_payment_payer_id` (
  `PAYMENT_ID` int(11) NOT NULL UNIQUE,
  `ACCOUNT_ID` int(11) NULL,
  `DOCUMENT_TYPE` varchar(2) NOT NULL,
  `DOCUMENT_NUMBER` varchar(20) NOT NULL,
  `NATIONALITY` int(3) NOT NULL,
  PRIMARY KEY (`PAYMENT_ID`)
);