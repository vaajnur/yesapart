<?

$MESS["P_CHOOSE_TSZH"] = "(Выберите объект управления)";
$MESS["P_CHOOSE_SYSTEM"] = "(Выберите систему оплаты)";
$MESS["P_TSZH"] = "Объект управления";
$MESS["P_SYS"] = "Система оплаты";
$MESS["CITRUS_TSZH_PAYMENT_ID"] = "Имя переменной, содержащей ID платежа";
$MESS["CITRUS_TSZH_PAGE_DEFAULT"] = "Страница выбора способа оплаты";
$MESS["CITRUS_TSZH_PAGE_HISTORY"] = "Страница истории платежей";
$MESS["CITRUS_TSZH_PAGE_DO"] = "Страница подтверждения платежа";
$MESS["CITRUS_TSZH_PAGE_MAKE"] = "Страница совершения платежа";

?>