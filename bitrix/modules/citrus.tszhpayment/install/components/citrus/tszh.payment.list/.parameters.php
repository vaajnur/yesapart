<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	'PARAMETERS' => array(
		"MAX_COUNT" => array(
			"NAME" => GetMessage("COMP_MAX_COUNT"),
			"TYPE" => "STRING",
			"DEFAULT" => '15',
		),
	),
);

if (CModule::IncludeModule("iblock"))
	CIBlockParameters::AddPagerSettings($arComponentParameters, GetMessage("COMP_NAV_PAGER"), true, true);

?>