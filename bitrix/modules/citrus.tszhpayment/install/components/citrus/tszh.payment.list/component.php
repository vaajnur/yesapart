<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule('citrus.tszh'))
{
	ShowError(GetMessage("CITRUS_TSZH_MODULE_NOT_FOUND"));
	return;
}

if (!CModule::IncludeModule('citrus.tszhpayment'))
{
	ShowError(GetMessage("CITRUS_TSZHPAYMENT_MODULE_NOT_FOUND"));
	return;
}

if (!CTszhFunctionalityController::CheckEdition())
	return;

$arParams["MAX_COUNT"] = IntVal($arParams["MAX_COUNT"]) > 0 ? IntVal($arParams["MAX_COUNT"]) : 15;

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]!="N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]!=="N";

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	$arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
	if(!is_array($arrFilter))
		$arrFilter = array();
}

if($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"])
{
	$arNavParams = array(
		"nPageSize" => $arParams["MAX_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
}
else
{
	$arNavParams = array(
		"nTopCount" => $arParams["MAX_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

$arResult = Array(
	"FIELDS" => Array(
		"ID" => Array(
			"CODE" => "ID",
			"TYPE" => "int",
			"TITLE" => "����� �������",
		),
		"DATE_INSERT" => Array(
			"CODE" => "DATE_INSERT",
			"TITLE" => "����",
		),
		"PS_NAME" => Array(
			"CODE" => "PS_NAME",
			"TITLE" => "������ ������",
		),
		"SUMM" => Array(
			"CODE" => "SUMM",
			"TYPE" => "float",
			"TITLE" => "�����",
		),
		"STATUS" => Array(
			"CODE" => "STATUS",
			"TITLE" => "������",
		),
	),
);

global $USER;
// ���� ������������ �� �����������, ������� ����� �����������
if (!$USER->IsAuthorized())
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$arResult['ACCOUNT'] = CTszhAccount::GetByUserID($USER->GetID());
if (empty($arResult['ACCOUNT']))
	$APPLICATION->AuthForm(GetMessage("CITRUS_TSZH_ACCOUNT_NOT_FOUND"));

$arFilter = Array(
	"ACCOUNT_ID" => $arResult['ACCOUNT']['ID'],
);
$arOrder = Array("ID" => "DESC");

$rsPayments = CTszhPayment::GetList(
	// sort
	$arOrder,
	// filter
	array_merge($arFilter, $arrFilter),
	// group
	false,
	$arNavParams,
	Array('*', 'UF_*')
);
while ($arPayment = $rsPayments->GetNext())
{
	if ($arPayment["PAYED"] == "Y")
		$arPayment["STATUS"] = "�����������";
	else
	{
		$paymentUrl = str_replace("#ID#", $arPayment['ID'], SITE_DIR . 'personal/payment/?payment=#ID#');
		$arPayment["STATUS"] = "�� �������<br>" . "<span style=\"margin-left: -.3em;\">(</span><a href=\"$paymentUrl\">��������� ������</a>)";
	}

	$arResult["PAYMENTS"][] = $arPayment;
}
$arResult["NAV_STRING"] = $rsPayments->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);

$this->IncludeComponentTemplate();

?>