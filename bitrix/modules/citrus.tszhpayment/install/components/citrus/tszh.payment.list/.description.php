<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("CITRUS_TSZHPAYMENT_COMP_NAME"),
	"DESCRIPTION" => GetMessage("CITRUS_TSZHPAYMENT_COMP_DESC"),
	"ICON" => "/images/icon.gif",
	"PATH" => array(
		"ID" => "citrus.tszh",
		"NAME" => GetMessage("COMPONENT_ROOT_SECTION"),
	),
);
?>
