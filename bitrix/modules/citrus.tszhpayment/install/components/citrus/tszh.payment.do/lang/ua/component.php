<?
$MESS["TSZH_MODULE_NOT_INSTALLED"] = "Модуль «ТВЖ» не встановлено";
$MESS["TSZH_PAYMENT_MODULE_NOT_INSTALLED"] = "Модуль оплати «ТВЖ» не встановлено";
$MESS["TSZH_PAYMENT_DO_AMOUNT_ERROR"] = "Невірно вказана сума для оплати";
$MESS["TSZH_PAYMENT_DO_ERROR_MINIMUM_SUMM"] = "Мінімальна сума для оплати — #SUMM#";
$MESS["TSZH_PAYMENT_SYSTEM_DOESNT_EXISTS"] = "Зазначена платіжна система не існує";
$MESS["TSZH_PAYMENT_DO_ERROR_ADDING_PAYMENT"] = "Сталася помилка при додаванні платежу ";
$MESS["TSZH_PAYMENT_CURRENCY_TITLE"] = "грн.";
$MESS["TSZH_PAYMENT_CREATED"] = "Платіж додано. ";
?>