<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = Array(
	"PARAMETERS" => Array(
		"PAY_SYSTEM" => Array(
			"NAME" => GetMessage("TSZH_PAYMENT_DO_PAY_SYSTEM"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "0",
			"COLS" => 5,
		),
		"MAKE_PAYMENT_URL" => Array(
			"NAME" => GetMessage("TSZH_PAYMENT_DO_MAKE_PAYMENT_URL"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "/personal/payment/make/#ID#/",
			"COLS" => 40,
		),
		"MINIMUM_SUMM" => Array(
			"NAME" => GetMessage("TSZH_PAYMENT_DO_MINIMUM_SUMM"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "0",
			"COLS" => 5,
		)
	)
);
?>