<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("citrus.tszhpayment"))
	return;
	
$arPaySys = Array();
$arPaySys = Array("0" => GetMessage("SOPR_CHOOSE_PC"));
$dbPaySystem = CTszhPaySystem::GetList($arOrder = Array("SORT"=>"ASC", "NAME"=>"ASC"), Array("ACTIVE"=>"Y", "HAVE_RESULT_RECEIVE"=>"Y"));
while($arPaySystem = $dbPaySystem->Fetch())
{
	$arPaySys[$arPaySystem["ID"]] = $arPaySystem["NAME"];
}
$arComponentParameters = Array(
	"PARAMETERS" => Array(
		"PAY_SYSTEM_ID" => Array(
			"NAME" => GetMessage("SOPR_PC"),
			"TYPE" => "LIST", 
			"MULTIPLE"=>"N",
			"VALUES" => $arPaySys,
			"COLS"=>25, 
			"ADDITIONAL_VALUES"=>"N",
			"PARENT" => "BASE",
			"REFRESH" => "Y",
		),
	)
);
?>