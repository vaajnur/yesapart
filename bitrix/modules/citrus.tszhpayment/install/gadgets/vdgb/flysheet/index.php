<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

use Bitrix\Main\Localization\Loc;

$APPLICATION->SetAdditionalCSS('/bitrix/gadgets/vdgb/flysheet/styles.css');

$logoFile = "/bitrix/gadgets/bitrix/bitrix24/images/logo-" . LANGUAGE_ID . ".png";
if (!file_exists($_SERVER["DOCUMENT_ROOT"] . $logoFile))
{
	$logoFile = "/bitrix/gadgets/bitrix/bitrix24/images/logo-en.png";
}
?>
<div class="bx-gadgets-back-wrap">
	<div class="bx-gadgets-back-left">
		<img class="bx-gagets-image" src="/bitrix/gadgets/vdgb/flysheet/images/flysheet-left.gif">
	</div>
	<div class="bx-gagets-right-top-text"><?=Loc::getMessage("GADGET_TSZH_FLYSHEET_TEXT")?></div>
	<div class="bx-gadgets-back-right">
		<div class="bx-gagets-right-top-text-wrapper bx-gadgets-back-img">
			<a href="/bitrix/admin/tszh_flysheets_list.php?lang=<?= LANG ?>">
				<img src="/bitrix/gadgets/vdgb/flysheet/images/flysheet-right.gif">
			</a>
		</div>
	</div>
	<div style="clear: both"></div>
</div>
<div class="bx-gadget-shield"></div>

