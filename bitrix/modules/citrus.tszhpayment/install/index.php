<?
/**
 * ������ ������ ���ƻ
 * ���������/�������� ������
 * @package tszh
 */

global $DOCUMENT_ROOT, $MESS;
IncludeModuleLangFile(__FILE__);

if (class_exists("citrus_tszhpayment"))
{
	return;
}

Class citrus_tszhpayment extends CModule
{
	var $MODULE_ID = "citrus.tszhpayment"; // ID ������
	var $MODULE_VERSION; // ������ ������
	var $MODULE_VERSION_DATE; // ���� ���������� ������
	var $MODULE_NAME; // �������� ������
	var $MODULE_DESCRIPTION; // �������� ������
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";

	public $params = array();

	function citrus_tszhpayment()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path . "/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else
		{
			$this->MODULE_VERSION = "9.0.0";
			$this->MODULE_VERSION_DATE = "23.08.2010";
		}

		$this->MODULE_NAME = GetMessage("C_TSZH_PAYMENT_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("C_TSZH_PAYMENT_MODULE_DESCRIPTION");

		$this->PARTNER_NAME = GetMessage("C_MODULE_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("C_MODULE_PARTNER_URI");
	}

	protected function setParams($arParams)
	{
		$this->params = $arParams;

		return $this;
	}

	protected function getParams($method)
	{
		return array_key_exists($method, $this->params) ? $this->params[$method] : array();
	}

	// �������� ������ ������ � ���� ������ � ������������� ������ � ��������
	function InstallDB()
	{
		global $DB, $APPLICATION;
		$this->errors = false;

		// �������� ������ � ��, ���� ��� ��� �� ����������
		if (!$DB->Query("SELECT 'x' FROM b_tszh_pay_system WHERE 1=0", true))
		{
			$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/db/install.sql");
		}

		// ���� ��������� ������ - ������� ��
		if ($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("", $this->errors));

			return false;
		}
		// ����� - �������������� ������ � ������� �������
		else
		{
			RegisterModule($this->MODULE_ID);

			RegisterModuleDependences("citrus.tszh", "OnAfterTszhAdd", $this->MODULE_ID, "CTszhPaySystem", "OnAfterTszhAdd");
			RegisterModuleDependences("citrus.tszh", "OnAfterTszhUpdate", $this->MODULE_ID, "CTszhPaySystem", "OnAfterTszhAdd");
			RegisterModuleDependences("citrus.tszh", "OnAfterTszhDelete", $this->MODULE_ID, "CTszhPaySystem", "OnAfterTszhDelete");
			RegisterModuleDependences("main", "OnBuildGlobalMenu", $this->MODULE_ID, "CTszhPaySystem", "OnBuildGlobalMenu");
			RegisterModuleDependences("main","OnAdminContextMenuShow", $this->MODULE_ID, "CTszhFlysheets", "MyOnAdminContextMenuShow");

			// ������� ������, ������� �������� ����������������� ��������� ������� ��� ������������ �������� ���������� (���� ����� ��� ���)
			CAgent::AddAgent("CTszhPaySystem::fillPredefinedPaySystems();", $this->MODULE_ID, "Y", 1);

			return true;
		}
	}

	// �������� ������ ������ �� �� � ������ ����������� ������
	function UnInstallDB()
	{
		global $DB;

		$params = $this->getParams(__FUNCTION__);

		UnRegisterModuleDependences("citrus.tszh", "OnAfterTszhAdd", $this->MODULE_ID, "CTszhPaySystem", "OnAfterTszhAdd");
		UnRegisterModuleDependences("citrus.tszh", "OnAfterTszhUpdate", $this->MODULE_ID, "CTszhPaySystem", "OnAfterTszhAdd");
		UnRegisterModuleDependences("citrus.tszh", "OnAfterTszhDelete", $this->MODULE_ID, "CTszhPaySystem", "OnAfterTszhDelete");
		UnRegisterModuleDependences("main", "OnBuildGlobalMenu", $this->MODULE_ID, "CTszhPaySystem", "OnBuildGlobalMenu");
        UnRegisterModuleDependences("main","OnAdminContextMenuShow", $this->MODULE_ID, "CTszhFlysheets", "MyOnAdminContextMenuShow");

		// ������ �����������
		UnRegisterModule($this->MODULE_ID);

		// ������ ������� ������ �� ��, ���� �� �������� �������� ������ �� ���� �������� ������� ���������� ��������
		if (!$params['save_tables'])
		{
			$DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/db/uninstall.sql");
		}

		return true;
	}

	// ���������� ������ ����� � �������� �������� �� ���������
	function __GetEventTypes()
	{
		return array(
			'CITRUS_TSZHPAYMENT_RECEIVE' => Array(
				Array(
					"SUBJECT" => GetMessage('CITRUS_TSZHPAYMENT_RECEIVE_SUBJECT1'),
					"MESSAGE" => GetMessage('CITRUS_TSZHPAYMENT_RECEIVE_MESSAGE1'),
				),
				Array(
					"EMAIL_TO" => "#MAIL#",
					"SUBJECT" => GetMessage('CITRUS_TSZHPAYMENT_RECEIVE_SUBJECT2'),
					"MESSAGE" => GetMessage('CITRUS_TSZHPAYMENT_RECEIVE_MESSAGE2'),
				),
			),
		);

	}

	// ��������/���������� ����� � �������� �������� ���������
	function __InstallEvents()
	{
		global $APPLICATION;

		// ������ ���� ������, ��������������� �� ������
		$arSites = array();
		$rsSites = CSite::GetList($by, $order);
		while ($arSite = $rsSites->Fetch())
		{
			if (!in_array($arSite["LANGUAGE_ID"], Array('ru', 'ua')))
			{
				continue;
			}
			$arSites[$arSite["LANGUAGE_ID"]][] = $arSite["LID"];
		}

		// �������� ����� �������� ������� � �������� �������� ��-��������� ��� ���� ������
		$rsLanguages = CLanguage::GetList($b = "", $o = "");
		$obEventType = new CEventType();
		$obEventMessage = new CEventMessage();
		while ($arLang = $rsLanguages->Fetch())
		{
			if (!in_array($arLang["LID"], Array('ru', 'ua')))
			{
				continue;
			}

			// ����������� �������� ��������� ��� ������� �����
			IncludeModuleLangFile(dirname(__FILE__) . '/events.php', $arLang["LID"]);
			$arEventTypes = self::__GetEventTypes();

			foreach ($arEventTypes as $strEventName => $arEventTemplates)
			{
				$arEventTypeFields = Array(
					"LID" => $arLang["LID"],
					"EVENT_NAME" => $strEventName,
					"NAME" => GetMessage($strEventName . '_TITLE'),
					"DESCRIPTION" => GetMessage($strEventName . '_TEXT'),
				);
				$arEventType = CEventType::GetList(Array("EVENT_NAME" => $strEventName, 'LID' => $arLang['LID']))->Fetch();
				if (is_array($arEventType))
				{
					$bSuccess = $obEventType->Update(Array("ID" => $arEventType["ID"]), $arEventTypeFields);
				}
				else
				{
					$bSuccess = $obEventType->Add($arEventTypeFields) > 0;
					if ($bSuccess)
					{
						// ��������/���������� �������� �������� ��� ���� ������ ����� �����
						if (array_key_exists($arLang["LID"], $arSites) && count($arSites[$arLang["LID"]]) > 0)
						{
							foreach ($arEventTemplates as $arTemplate)
							{
								$arTemplate['EVENT_NAME'] = $strEventName;
								$arTemplate['LID'] = $arSites[$arLang["LID"]];

								if (!array_key_exists('EMAIL_FROM', $arTemplate))
								{
									$arTemplate['EMAIL_FROM'] = '#DEFAULT_EMAIL_FROM#';
								}
								if (!array_key_exists('EMAIL_TO', $arTemplate))
								{
									$arTemplate['EMAIL_TO'] = '#DEFAULT_EMAIL_FROM#';
								}
								if (!array_key_exists('BODY_TYPE', $arTemplate))
								{
									$arTemplate['BODY_TYPE'] = 'text';
								}
								if (!array_key_exists('ACTIVE', $arTemplate))
								{
									$arTemplate['ACTIVE'] = 'Y';
								}

								$bSuccess = $obEventMessage->Add($arTemplate) > 0;

								if (!$bSuccess)
								{
									$APPLICATION->ThrowException(GetMessage("CITRUS_TSZHPAYMENT_ERROR_INSTALLING_EVENT_MESSAGES") . $obEventType->LAST_ERROR);

									return false;
								}
							}
						}
					}
				}

				if (!$bSuccess)
				{
					$APPLICATION->ThrowException(GetMessage("CITRUS_TSZHPAYMENT_ERROR_INSTALLING_EVENT_TYPES") . $obEventType->LAST_ERROR);

					return false;
				}


			}

		}

		return true;
	}

	// ������� ������ ������������� ���������� ������� (���)�, ���� ��� �� ���� ������� �����
	function InstallEvents()
	{
		return self::__InstallEvents();
	}

	function UnInstallEvents()
	{
		return true;
	}

    function InstallGadgets($gadgets_id)
    {
        $gdid = $gadgets_id."@".rand();

        $arUserOptions = array();

        $arUserOptions = CUserOptions::GetOption("intranet", "~gadgets_admin_index", false, false);
        $bGdInstalled = false;
        foreach($arUserOptions[0]["GADGETS"] as $tempid=>$tempgadget)
        {
            if (!(strcasecmp(substr($tempid,0,8), $gadgets_id )))
            {
                $bGdInstalled = true;
            }
        }
        if (!$bGdInstalled)
        {
            foreach($arUserOptions[0]["GADGETS"] as $tempid=>$tempgadget)
                if($tempgadget["COLUMN"]==0)
                    $arUserOptions[0]["GADGETS"][$tempid]["ROW"]++;

            $arUserOptions[0]["GADGETS"][$gdid] = Array("COLUMN"=>0, "ROW"=>0, 'HIDE' => 'N');

            CUserOptions::SetOption("intranet", "~gadgets_admin_index", $arUserOptions);
        }
        return true;
    }
    function UninstallGadgets($gadgets_id)
    {
        $arUserOptions = array();
        $arUserOptions = CUserOptions::GetOption("intranet", "~gadgets_admin_index", false, false);
        foreach($arUserOptions[0]["GADGETS"] as $tempid=>$tempgadget)
        {
            if (!(strcasecmp(substr($tempid,0,8), $gadgets_id )))
            {
                unset($arUserOptions[0]["GADGETS"][$tempid]);
            }
        }
        CUserOptions::SetOption("intranet", "~gadgets_admin_index", $arUserOptions);
    }

	// ����������� ������ ������
	function InstallFiles()
	{
		$arParams = $this->getParams(__FUNCTION__);

		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/themes/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/themes/", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/images/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/images/" . $this->MODULE_ID . '/', true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/{$this->MODULE_ID}/install/gadgets/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/gadgets/", true, true);

		$bReWriteAdditionalFiles = ($arParams["public_rewrite"] == "Y");

		if (array_key_exists("public_dir", $arParams) && strlen($arParams["public_dir"]))
		{
			$rsSite = CSite::GetList($by = "sort", $order = "asc");
			while ($site = $rsSite->Fetch())
			{
				$arPaths = array(
					array(
						'from' => $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/" . $this->MODULE_ID . "/install/public/ru/personal/payment/",
						'to' => $site['ABS_DOC_ROOT'] . $site["DIR"] . $arParams["public_dir"] . "/",
					),
					array(
						'from' => $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/" . $this->MODULE_ID . "/install/public/ru/payment_receive.php",
						'to' => $site['ABS_DOC_ROOT'] . $site["DIR"] . "/payment_receive.php",
					),
				);

				foreach ($arPaths as $arPath)
				{
					CopyDirFiles($arPath['from'], $arPath['to'], $bReWriteAdditionalFiles);
				}
			}
		}
		return true;
	}

	// �������� ������ ������
	function UnInstallFiles()
	{
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components");
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/images", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/images/" . $this->MODULE_ID);
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/{$this->MODULE_ID}/install/gadgets", $_SERVER["DOCUMENT_ROOT"]."/bitrix/gadgets");
		DeleteDirFilesEx("/bitrix/themes/.default/icons/{$this->MODULE_ID}/");//icons
		DeleteDirFilesEx("/bitrix/themes/.default/start_menu/{$this->MODULE_ID}/");//icons

		return true;
	}


	function DoInstall()
	{
		if (!check_bitrix_sessid())
		{
			return false;
		}

		global $APPLICATION, $step;
		$step = IntVal($step);
		if ($step < 2)
		{
			$APPLICATION->IncludeAdminFile(GetMessage("TSZH_PAYMENT_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/step1.php");
		}
		elseif ($step == 2)
		{
			if ($this->InstallDB())
			{
				$this->setParams(array(
					'InstallFiles' => array(
						"public_dir" => $_REQUEST["public_dir"],
						"public_rewrite" => $_REQUEST["public_rewrite"],
					)
				));
				$this->InstallEvents();
				$this->InstallFiles();
                if (!$this->InstallGadgets('FLYSHEET'))
                    throw new Exception(GetMessage("C_ERROR_INSTALL_GADGET"));
			}
			$GLOBALS["errors"] = $this->errors;
			$APPLICATION->IncludeAdminFile(GetMessage("TSZH_PAYMENT_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/step2.php");
		}
	}

	// �������� ������ (���������� ���������)
	function DoUninstall()
	{
		global $APPLICATION;

		if (!check_bitrix_sessid())
		{
			return false;
		}

		$step = IntVal($_REQUEST['step']);
		if ($step < 2)
		{
			$APPLICATION->IncludeAdminFile(GetMessage("inst_uninst_title"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/uninst1.php");
		}
		elseif ($step == 2)
		{
			$this->setParams(array(
				'UnInstallDB' => array(
					"save_tables" => IntVal($_REQUEST["save_tables"]),
				)
			));
			$this->UnInstallDB();

			$this->UnInstallFiles();
            $this->UnInstallGadgets("FLYSHEET");
			$this->UnInstallEvents();
			$GLOBALS["errors"] = $this->errors;
			$APPLICATION->IncludeAdminFile(GetMessage("inst_uninst_title"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/uninst2.php");
		}

		//$this->UnInstallDB();
		//$this->UnInstallEvents();
		//$this->UnInstallFiles();
	}
}

?>