<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

$tszhPaymentModulePermissions = $APPLICATION->GetGroupRight("citrus.tszhpayment");
if ($tszhPaymentModulePermissions == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

IncludeModuleLangFile(__FILE__);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszhpayment/include.php");

$ID = IntVal($ID);
$LID = Trim($LID);

ClearVars();
ClearVars("f_");

CJSCore::Init(array("jquery"));

if (($ID>0) && ($arPaySystem = CTszhPaySystem::GetByID($ID)))
{
	$LID = $arPaySystem["LID"];
}
elseif (strlen($LID)>0)
{
	$dbLang = CLang::GetByID($LID);
	$ID = 0;
	if($arLang = $dbLang->Fetch())
		$LID = $arLang["ID"];
	else
		LocalRedirect("tszh_pay_systems.php?lang=".LANG.GetFilterParams("filter_", false));
}
else
	LocalRedirect("tszh_pay_systems.php?lang=".LANG.GetFilterParams("filter_", false));

$path2SystemPSFiles = "/bitrix/modules/citrus.tszhpayment/ru/payment/";
$path2UserPSFiles = COption::GetOptionString("citrus.tszhpayment", "path2user_ps_files", BX_PERSONAL_ROOT."/php_interface/include/citrus.tszhpayment/payment/");
CheckDirPath($_SERVER["DOCUMENT_ROOT"].$path2UserPSFiles);

$bFilemanModuleInst = False;
if (IsModuleInstalled("fileman"))
	$bFilemanModuleInst = True;

$errorMessage = "";
$bInitVars = false;

if ($_SERVER["REQUEST_METHOD"] == "POST"
	&& (strlen($save) > 0 || strlen($apply) > 0)
	&& $tszhPaymentModulePermissions >= "W"
	&& check_bitrix_sessid())
{
	$LID = Trim($LID);
	if (strlen($LID) <= 0)
		$errorMessage .= GetMessage("ERROR_NO_LANG")."<br>";

	$TSZH_ID = intval($TSZH_ID);
	if ($TSZH_ID <= 0)
		$errorMessage .= GetMessage("ERROR_NO_TSZH")."<br>";

	$NAME = Trim($NAME);
	if (strlen($NAME) <= 0)
		$errorMessage .= GetMessage("ERROR_NO_NAME")."<br>";

	$CURRENCY = Trim($CURRENCY);
	if (strlen($CURRENCY) <= 0)
		$errorMessage .= GetMessage("ERROR_NO_CURRENCY")."<br>";

	$ACTIVE = (($ACTIVE == "Y") ? "Y" : "N");
	$SORT = ((IntVal($SORT) > 0) ? IntVal($SORT) : 100);


	$ACTION_FILE = Trim($ACTION_FILE);
	if (strlen($ACTION_FILE) <= 0)
		$errorMessage .= GetMessage("TPS_EMPTY_SCRIPT").".<br>";

	if (strlen($ACTION_FILE) > 0)
	{
		$ACTION_FILE = str_replace("\\", "/", $ACTION_FILE);
		while (substr($ACTION_FILE, strlen($ACTION_FILE) - 1, 1) == "/")
			$ACTION_FILE = substr($ACTION_FILE, 0, strlen($ACTION_FILE) - 1);

		$pathToAction = $_SERVER["DOCUMENT_ROOT"].$ACTION_FILE;
		if (!file_exists($pathToAction))
			$errorMessage .= GetMessage("TPS_NO_SCRIPT").".<br>";
	}

	if (strlen($errorMessage) <= 0)
	{
		$arFields = array(
			"LID" => $LID,
			"TSZH_ID" => $TSZH_ID,
			"CURRENCY" => $CURRENCY,
			"NAME" => $NAME,
			"ACTIVE" => $ACTIVE,
			"SORT" => $SORT,
			"DESCRIPTION" => $DESCRIPTION,
			"ENCODING" => $ENCODING,
		);

		$arParams = array();

		if (strlen($PS_ACTION_FIELDS_LIST) > 0) {
			$arActFields = explode(",", $PS_ACTION_FIELDS_LIST);
			foreach ($arActFields as &$field)
			{
				$field = Trim($field);

				$typeTmp = ${"TYPE_".$field};
				$valueTmp = ${"VALUE1_".$field};
				if (strlen($typeTmp) <= 0)
					$valueTmp = ${"VALUE2_".$field};

				$arParams[$field] = array(
						"TYPE" => $typeTmp,
						"VALUE" => $valueTmp
					);
			}
			unset($field);
		}

		$arFields = array_merge($arFields, array(
			"ACTION_FILE" => $ACTION_FILE,
			"NEW_WINDOW" => ( ($NEW_WINDOW=="Y") ? "Y" : "N" ),
			"PARAMS" => CTszhPaySystem::SerializeParams($arParams),
			"HAVE_PREPAY" => "N",
			"HAVE_RESULT" => "N",
			"HAVE_ACTION" => "N",
			"HAVE_PAYMENT" => "N",
			"HAVE_RESULT_RECEIVE" => "N",
			"ENCODING" => $ENCODING,
		));


		$pathToAction = $_SERVER["DOCUMENT_ROOT"].$ACTION_FILE;
		$pathToAction = str_replace("\\", "/", $pathToAction);
		while (substr($pathToAction, strlen($pathToAction) - 1, 1) == "/")
			$pathToAction = substr($pathToAction, 0, strlen($pathToAction) - 1);

		if (file_exists($pathToAction)) {
			if (is_dir($pathToAction)) {
				if (file_exists($pathToAction."/pre_payment.php"))
					$arFields["HAVE_PREPAY"] = "Y";
				if (file_exists($pathToAction."/result.php"))
					$arFields["HAVE_RESULT"] = "Y";
				if (file_exists($pathToAction."/action.php"))
					$arFields["HAVE_ACTION"] = "Y";
				if (file_exists($pathToAction."/payment.php"))
					$arFields["HAVE_PAYMENT"] = "Y";
				if (file_exists($pathToAction."/result_rec.php"))
					$arFields["HAVE_RESULT_RECEIVE"] = "Y";
			} else {
				$arFields["HAVE_PAYMENT"] = "Y";
			}
		}

		if ($ID > 0) {
			if (!CTszhPaySystem::Update($ID, $arFields))
			{
				if ($ex = $APPLICATION->GetException())
					$errorMessage .= $ex->GetString().".<br>";
				else
					$errorMessage .= GetMessage("ERROR_EDIT_PAY_SYS").".<br>";
			}
		} else {
			$ID = CTszhPaySystem::Add($arFields);
			if ($ID <= 0) {
				if ($ex = $APPLICATION->GetException())
					$errorMessage .= $ex->GetString().".<br>";
				else
					$errorMessage .= GetMessage("ERROR_ADD_PAY_SYS").".<br>";
			}
		}

	}

	if (strlen($errorMessage) > 0)
		$bInitVars = True;

	if (strlen($save) > 0 && strlen($errorMessage) <= 0)
		LocalRedirect("tszh_pay_systems.php?lang=".LANG.GetFilterParams("filter_", false));
	elseif (strlen($errorMessage) <= 0)
		LocalRedirect("tszh_pay_systems_edit.php?ID={$ID}&lang=".LANG.GetFilterParams("filter_", false));
}

list($arTszhs, $arTszhSite) = citrusTszhPaymentGetTszhList();

if ($ID > 0) {
	$dbPaySystem = CTszhPaySystem::GetList(array(), array("ID" => $ID));
	$arPaySystem = $dbPaySystem->ExtractFields("str_");
	$str_DESCRIPTION = htmlspecialcharsBack($arPaySystem['DESCRIPTION']);
	$arPaySystem['DESCRIPTION'] = htmlspecialcharsBack($arPaySystem['DESCRIPTION']);
} else {
	$arPaySystemDefault = Array(
		"ACTIVE" => Y,
		"ENCODING" => SITE_CHARSET,
		"SORT" => 500,
	);

	$rsSort = CTszhPaySystem::GetList(Array("SORT" => "DESC"), Array("LID" => $LID), false, Array('nTopCount' => 1), Array("ID", "SORT"));
	if ($arSort = $rsSort->Fetch()) {
		$arPaySystemDefault['SORT'] = $arSort['SORT'] + 100;
	}

	foreach ($arPaySystemDefault as $code => $value) {
		${"str_" . $code} = $value;
	}
}

//echo '<pre>' . htmlspecialchars(print_r($arPaySystem, true)) . '</pre>';

if ($bInitVars)
{
	$DB->InitTableVarsForEdit("b_tszh_pay_system", "", "str_");
}

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszhpayment/prolog.php");

$APPLICATION->SetTitle(($ID > 0) ? str_replace("#ID#", $ID, GetMessage("TSZH_PAYMENT_EDIT_RECORD")) : GetMessage("TSZH_PAYMENT_NEW_RECORD"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

/*********************************************************************/
/********************  BODY  *****************************************/
/*********************************************************************/
?>

<?
$aMenu = array(
		array(
				"TEXT" => GetMessage("TSZH_PAYMENT_S_2FLIST"),
				"LINK" => "/bitrix/admin/tszh_pay_systems.php?lang=".LANG.GetFilterParams("filter_"),
				"ICON" => "btn_list"
			)
	);

if ($ID > 0 && $tszhPaymentModulePermissions >= "W")
{
	$aMenu[] = array("SEPARATOR" => "Y");

	$aMenu[] = array(
			"TEXT" => GetMessage("TSZH_PAYMENT_S_NEW_PAYSYS"),
			"LINK" => "/bitrix/admin/tszh_pay_systems_edit.php?lang=".LANG."&LID=".UrlEncode($LID).GetFilterParams("filter_"),
			"ICON" => "btn_new"
		);

	$aMenu[] = array(
			"TEXT" => GetMessage("TSZH_PAYMENT_S_DELETE_PAYSYS"),
			"LINK" => "javascript:if(confirm('".GetMessage("TSZH_PAYMENT_S_DELETE_PAYSYS_CONFIRM")."')) window.location='/bitrix/admin/tszh_pay_systems.php?action=delete&ID[]=".$ID."&lang=".LANG."&".bitrix_sessid_get()."#tb';",
			"WARNING" => "Y",
			"ICON" => "btn_delete"
		);
}
$context = new CAdminContextMenu($aMenu);
$context->Show();
?>

<?if(strlen($errorMessage)>0)
	echo CAdminMessage::ShowMessage(Array("DETAILS"=>$errorMessage, "TYPE"=>"ERROR", "MESSAGE"=>GetMessage("TSZH_PAYMENT_S_ERROR"), "HTML"=>true));?>

<script language="JavaScript">
<!--
function SetActLinkText(flag)
{
	var paySysActSwitch = document.getElementById("pay_sys_switch");
	if (flag)
	{
		paySysActSwitch.innerHTML = "<br><?= GetMessage("TPS_HIDE_PROPS") ?>";
	}
	else
	{
		paySysActSwitch.innerHTML = "<?= GetMessage("TPS_SHOW_PROPS") ?>";
	}
}

function ShowHideStatus()
{
	var paySysActDIV = document.getElementById("pay_sys_act");
	eval("var flag = paySysActVisible;");
	if (flag)
	{
		paySysActDIV.style["display"] = "none";
		eval("paySysActVisible = false;");
	}
	else
	{
		paySysActDIV.style["display"] = "block";
		eval("paySysActVisible = true;");
	}
	SetActLinkText(!flag);
}

function ActionFileChange(exist)
{
	var paySysActDIV = document.getElementById("pay_sys_act");
	paySysActDIV.style["backgroundColor"] = "#F1F1F1";
	paySysActDIV.innerHTML = '<font color="#FF0000"><?= GetMessage("TPS_WAIT") ?></font>';

	var oActionFile = document.forms['pay_sys_form'].elements["ACTION_FILE"];
	var psAction = oActionFile[oActionFile.selectedIndex].value;

	var curDescr = "";
	for (i = 0; i < arActionPaths.length; i++)
	{
		if (arActionPaths[i] == psAction)
		{
			curDescr = arActionDescrs[i];
			break;
		}
	}
	var paySysActDescrDIV = document.getElementById("pay_sys_act_descr");
	paySysActDescrDIV.innerHTML = '' + curDescr + '';

	window.frames["hidden_action_frame"].location.replace('/bitrix/admin/tszh_pay_systems_get.php?lang=<?= htmlspecialchars(LANG) ?>&file='+escape(psAction)+'&id=<?=$ID?>');
}


var arUserFieldsList = new Array("ID", "FULL_NAME", "LOGIN", "NAME", "SECOND_NAME", "LAST_NAME", "EMAIL", "LID", "PERSONAL_PROFESSION", "PERSONAL_WWW", "PERSONAL_ICQ", "PERSONAL_GENDER", "PERSONAL_FAX", "PERSONAL_MOBILE", "PERSONAL_STREET", "PERSONAL_MAILBOX", "PERSONAL_CITY", "PERSONAL_STATE", "PERSONAL_ZIP", "PERSONAL_COUNTRY", "WORK_COMPANY", "WORK_DEPARTMENT", "WORK_POSITION", "WORK_WWW", "WORK_PHONE", "WORK_FAX", "WORK_STREET", "WORK_MAILBOX", "WORK_CITY", "WORK_STATE", "WORK_ZIP", "WORK_COUNTRY");
var arUserFieldsNameList = new Array("<?= GetMessage("TPS_USER_ID") ?>", "<?=GetMessage("TPAF_FULL_NAME")?>", "<?= GetMessage("TPS_USER_LOGIN") ?>", "<?= GetMessage("TPS_USER_NAME") ?>", "<?= GetMessage("TPS_USER_SECOND_NAME") ?>", "<?= GetMessage("TPS_USER_LAST_NAME") ?>", "EMail", "<?= GetMessage("TPS_USER_SITE") ?>", "<?= GetMessage("TPS_USER_PROF") ?>", "<?= GetMessage("TPS_USER_WEB") ?>", "<?= GetMessage("TPS_USER_ICQ") ?>", "<?= GetMessage("TPS_USER_SEX") ?>", "<?= GetMessage("TPS_USER_FAX") ?>", "<?= GetMessage("TPS_USER_PHONE") ?>", "<?= GetMessage("TPS_USER_ADDRESS") ?>", "<?= GetMessage("TPS_USER_POST") ?>", "<?= GetMessage("TPS_USER_CITY") ?>", "<?= GetMessage("TPS_USER_STATE") ?>", "<?= GetMessage("TPS_USER_ZIP") ?>", "<?= GetMessage("TPS_USER_COUNTRY") ?>", "<?= GetMessage("TPS_USER_COMPANY") ?>", "<?= GetMessage("TPS_USER_DEPT") ?>", "<?= GetMessage("TPS_USER_DOL") ?>", "<?= GetMessage("TPS_USER_COM_WEB") ?>", "<?= GetMessage("TPS_USER_COM_PHONE") ?>", "<?= GetMessage("TPS_USER_COM_FAX") ?>", "<?= GetMessage("TPS_USER_COM_ADDRESS") ?>", "<?= GetMessage("TPS_USER_COM_POST") ?>", "<?= GetMessage("TPS_USER_COM_CITY") ?>", "<?= GetMessage("TPS_USER_COM_STATE") ?>", "<?= GetMessage("TPS_USER_COM_ZIP") ?>", "<?= GetMessage("TPS_USER_COM_COUNTRY") ?>");

var arPaymentFieldsList = new Array("ID", "DATE_INSERT", "DATE_INSERT_DATE", "SHOULD_PAY", "CURRENCY", "LID");
var arPaymentFieldsNameList = new Array("<?= GetMessage("TPS_ORDER_ID") ?>", "<?= GetMessage("TPS_ORDER_DATETIME") ?>", "<?= GetMessage("TPS_ORDER_DATE") ?>", "<?= GetMessage("TPS_ORDER_PRICE") ?>", "<?= GetMessage("TPS_ORDER_CURRENCY") ?>", "<?= GetMessage("TPS_ORDER_SITE") ?>");

var arAccountFieldsList = new Array("ID", "XML_ID", "NAME", "CITY", "DISTRICT", "REGION", "SETTLEMENT", "STREET", "HOUSE", "FLAT", "AREA", "LIVING_AREA", "PEOPLE", "FULL_ADDRESS");
var arAccountFieldsNameList = new Array("<?=GetMessage("TPAF_ID")?>", "<?=GetMessage("TPAF_XML_ID")?>", "<?=GetMessage("TPAF_FULL_NAME")?>", "<?=GetMessage("TPAF_CITY")?>", "<?=GetMessage("TPAF_DISTRICT")?>", "<?=GetMessage("TPAF_REGION")?>", "<?=GetMessage("TPAF_SETTLEMENT")?>", "<?=GetMessage("TPAF_STREET")?>", "<?=GetMessage("TPAF_HOUSE")?>", "<?=GetMessage("TPAF_FLAT")?>", "<?=GetMessage("TPAF_AREA")?>", "<?=GetMessage("TPAF_LIVING_AREA")?>", "<?=GetMessage("TPAF_PEOPLE")?>", "<?=GetMessage("TPAF_FULL_ADDRESS")?>");

var arTszhFieldsList = new Array("ID", "CODE", "SITE_ID", "NAME", "ADDRESS", "PHONE", "PHONE_DISP", "INN", "KPP", "RSCH", "BANK", "KSCH", "BIK");
var arTszhFieldsNameList = new Array("<?=GetMessage("TPTF_ID")?>", "<?=GetMessage("TPTF_CODE")?>", "<?=GetMessage("TPTF_SITE_ID")?>", "<?=GetMessage("TPTF_NAME")?>", "<?=GetMessage("TPTF_ADDRESS")?>", "<?=GetMessage("TPTF_PHONE")?>", "<?=GetMessage("TPTF_PHONE_DISP")?>", "<?=GetMessage("TPTF_INN")?>", "<?=GetMessage("TPTF_KPP")?>", "<?=GetMessage("TPTF_RSCH")?>", "<?=GetMessage("TPTF_BANK")?>", "<?=GetMessage("TPTF_KSCH")?>", "<?=GetMessage("TPTF_BIK")?>");

function PropertyTypeChange(pkey, cVal)
{
	var oType = document.forms["pay_sys_form"].elements["TYPE_" + pkey];
	var oValue1 = document.forms["pay_sys_form"].elements["VALUE1_" + pkey];
	var oValue2 = document.forms["pay_sys_form"].elements["VALUE2_" + pkey];

	// special params
	if (!oValue1) {
		eval("var cur_val = param_" + pkey + "_value;");
		$("#VALUE2_" + pkey).val(cur_val);
		return;
	}

	eval("var cur_type = ''; if (typeof(param_" + pkey + "_type) == 'string') cur_type = param_" + pkey + "_type;");
	eval("var cur_val = ''; if (typeof(param_" + pkey + "_value) == 'string') cur_val = param_" + pkey + "_value;");

	var value1_length = oValue1.length;
	while (value1_length > 0)
	{
		value1_length--;
		oValue1.options[value1_length] = null;
	}
	value1_length = 0;

	var typeVal = oType[oType.selectedIndex].value;
	if (typeVal == "USER")
	{
		oValue2.style["display"] = "none";
		oValue1.style["display"] = "block";

		for (i = 0; i < arUserFieldsList.length; i++)
		{
			var newoption = new Option(arUserFieldsNameList[i], arUserFieldsList[i], false, false);
			oValue1.options[value1_length] = newoption;

			if ((typeVal == cur_type && cur_val == arUserFieldsList[i]) || cVal == arUserFieldsList[i])
				oValue1.selectedIndex = value1_length;

			value1_length++;
		}
	}
	else if (typeVal == "PAYMENT")
	{
		oValue2.style["display"] = "none";
		oValue1.style["display"] = "block";

		for (i = 0; i < arPaymentFieldsList.length; i++)
		{
			var newoption = new Option(arPaymentFieldsNameList[i], arPaymentFieldsList[i], false, false);
			oValue1.options[value1_length] = newoption;

			if ((typeVal == cur_type && cur_val == arPaymentFieldsList[i]) || cVal == arPaymentFieldsList[i])
				oValue1.selectedIndex = value1_length;

			value1_length++;
		}
	}
	else if (typeVal == "ACCOUNT")
	{
		oValue2.style["display"] = "none";
		oValue1.style["display"] = "block";

		for (i = 0; i < arAccountFieldsList.length; i++)
		{
			var newoption = new Option(arAccountFieldsNameList[i], arAccountFieldsList[i], false, false);
			oValue1.options[value1_length] = newoption;

			if ((typeVal == cur_type && cur_val == arAccountFieldsList[i]) || cVal == arAccountFieldsList[i])
				oValue1.selectedIndex = value1_length;

			value1_length++;
		}
	}
	else if (typeVal == "TSZH")
	{
		oValue2.style["display"] = "none";
		oValue1.style["display"] = "block";

		for (i = 0; i < arTszhFieldsList.length; i++)
		{
			var newoption = new Option(arTszhFieldsNameList[i], arTszhFieldsList[i], false, false);
			oValue1.options[value1_length] = newoption;

			if ((typeVal == cur_type && cur_val == arTszhFieldsList[i]) || cVal == arTszhFieldsList[i])
				oValue1.selectedIndex = value1_length;

			value1_length++;
		}
	}
	else
	{
		oValue1.style["display"] = "none";
		oValue2.style["display"] = "block";

		oValue2.value = cur_val;
	}
}

function InitActionProps(pkey)
{
	var oType = document.forms["pay_sys_form"].elements["TYPE_" + pkey];
	eval("var cur_type = ''; if (typeof(param_" + pkey + "_type) == 'string') cur_type = param_" + pkey + "_type;");

	if (oType.options) {
		for (i = 0; i < oType.options.length; i++) {
			if (oType.options[i].value == cur_type) {
				oType.selectedIndex = i;
				break;
			}
		}
	}
	PropertyTypeChange(pkey);
}
//-->
</script>

<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?" name="pay_sys_form">
<?echo GetFilterHiddens("filter_");?>
<input type="hidden" name="Update" value="Y">
<input type="hidden" name="lang" value="<?echo LANG ?>">
<input type="hidden" name="ID" value="<?echo $ID ?>">
<input type="hidden" name="LID" value="<?= htmlspecialchars($LID) ?>">
<?=bitrix_sessid_post()?>

<?
$aTabs = array(
		array("DIV" => "edit1", "TAB" => GetMessage("TSZH_PAYMENT_S_TAB_PAYSYS"), "ICON" => "tszhpayment", "TITLE" => GetMessage("TSZH_PAYMENT_S_TAB_PAYSYS_DESCR"))
	);

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
?>

<?
$tabControl->BeginNextTab();
?>

	<?if ($ID>0):?>
		<tr>
			<td width="40%">ID:</td>
			<td width="60%"><?= $ID ?></td>
		</tr>
	<?endif;?>

	<tr>
		<td width="40%"><?= GetMessage("F_LANG") ?>:</td>
		<td width="60%"><?= $LID ?></td>
	</tr>
	<tr>
		<td width="40%"><span class="required">*</span><?= GetMessage("F_TSZH_ID") ?>:</td>
		<td width="60%">
			<select name="TSZH_ID">
<?				foreach ($arTszhs as $tszhID => $tszhName):?>
					<option value="<?=$tszhID?>"<?if ($str_TSZH_ID == $tszhID) echo " selected"?>><?="[{$tszhID}] {$tszhName}"?></option>
				<?endforeach?>
			</select>
		</td>
	</tr>
<?if (CModule::IncludeModule("currency")):?>
	<tr>
		<td width="40%"><span class="required">*</span><?= GetMessage("F_CURRENCY") ?>:</td>
		<td width="60%">
			<?= CCurrency::SelectBox("CURRENCY", $str_CURRENCY, "", True, "", "")?>
		</td>
	</tr>
<?else:?>
	<tr>
		<td width="40%"><span class="required">*</span><?= GetMessage("F_CURRENCY") ?>:</td>
		<td width="60%">
			<input type="text" name="CURRENCY" value="<?= $str_CURRENCY ?>" size="10">
		</td>
	</tr>
<?endif;?>
	<tr>
		<td width="40%"><span class="required">*</span><?= GetMessage("F_NAME") ?>:</td>
		<td width="60%">
			<input type="text" name="NAME" value="<?= $str_NAME ?>" size="40">
		</td>
	</tr>
	<tr>
		<td width="40%"><?= GetMessage("F_ACTIVE");?>:</td>
		<td width="60%">
			<input type="checkbox" name="ACTIVE" value="Y" <?if ($str_ACTIVE == "Y") echo "checked"?>>
		</td>
	</tr>
	<tr>
		<td width="40%"><?= GetMessage("F_SORT");?>:</td>
		<td width="60%">
			<input type="text" name="SORT" value="<?= $str_SORT ?>" size="5">
		</td>
	</tr>
	<tr>
		<td width="40%" valign="top"><?= GetMessage("F_DESCRIPTION");?>:</td>
		<td width="60%" valign="top">
<?
if (CModule::IncludeModule("fileman")):
		$LHE = new CLightHTMLEditor;
		$LHE->Show(array(
			'id' => 'DESCRIPTION',
			'width' => '100%',
			'height' => '200px',
			'inputName' => "DESCRIPTION",
			'content' => $str_DESCRIPTION,
			'bUseFileDialogs' => false,
			'bFloatingToolbar' => true,
			'bArisingToolbar' => true,
			'toolbarConfig' => array(
				'Bold', 'Italic', 'Underline', 'RemoveFormat',
				'CreateLink', 'DeleteLink', 'Image', 'Video',
//				'BackColor', 'ForeColor',
				'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
				'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
				'StyleList', 'HeaderList',
//				'FontList', 'FontSizeList',
			),
		));
else:
?>
		<textarea rows="5" cols="40" name="DESCRIPTION"><?=$str_DESCRIPTION?></textarea>
<?
endif;
?>
		</td>
	</tr>
	<?
	// Get arrays of PS actions
	$arUserPSActions = array();
	$arSystemPSActions = array();


	function LocalGetPSActionDescr($fileName)
	{
		$psTitle = "";
		$psDescription = "";

		if (file_exists($fileName) && is_file($fileName))
			include($fileName);

		return array($psTitle, $psDescription);
	}


	function LocalGetPSActionDescr_old($fileName)
	{
		if (!file_exists($fileName))
			return false;

		$handle = fopen($fileName, "r");
		$contents = fread($handle, filesize($fileName));
		fclose($handle);

		$rep_title = "";
		$rep_descr = "";

		$arMatches = array();
		if (preg_match("#<title_".LANGUAGE_ID."[^>]*>([^<]*?)</title_".LANGUAGE_ID."[\s]*>#i", $contents, $arMatches))
		{
			$arMatches[1] = Trim($arMatches[1]);
			if (strlen($arMatches[1])>0) $rep_title = $arMatches[1];
		}
		if (strlen($rep_title)<=0
			&& preg_match("#<title[^>]*>([^<]*?)</title[\s]*>#i", $contents, $arMatches))
		{
			$arMatches[1] = Trim($arMatches[1]);
			if (strlen($arMatches[1])>0) $rep_title = $arMatches[1];
		}
		if (strlen($rep_title)<=0)
			$rep_title = basename($fileName, ".php");

		$arMatches = array();
		if (preg_match("#<description_".LANGUAGE_ID."[^>]*>([^<]*?)</description_".LANGUAGE_ID."[\s]*>#i", $contents, $arMatches))
		{
			$arMatches[1] = Trim($arMatches[1]);
			if (strlen($arMatches[1])>0) $rep_descr = $arMatches[1];
		}
		if (strlen($rep_descr)<=0
			&& preg_match("#<description[^>]*>([^<]*?)</description[\s]*>#i", $contents, $arMatches))
		{
			$arMatches[1] = Trim($arMatches[1]);
			if (strlen($arMatches[1])>0) $rep_descr = $arMatches[1];
		}

		return array($rep_title, $rep_descr);
	}


	$handle = @opendir($_SERVER["DOCUMENT_ROOT"].$path2UserPSFiles);
	if ($handle)
	{
		while (false !== ($dir = readdir($handle)))
		{
			if ($dir == "." || $dir == "..")
				continue;

			if (is_dir($_SERVER["DOCUMENT_ROOT"].$path2UserPSFiles.$dir))
			{
				$newFormat = "Y";
				list($title, $description) = LocalGetPSActionDescr($_SERVER["DOCUMENT_ROOT"].$path2UserPSFiles.$dir."/.description.php");
				if (strlen($title) <= 0)
					$title = $dir;
				else
					$title .= " (".$dir.")";
			}
			elseif (is_file($_SERVER["DOCUMENT_ROOT"].$path2UserPSFiles.$dir))
			{
				$newFormat = "N";
				list($title, $description) = LocalGetPSActionDescr_old($_SERVER["DOCUMENT_ROOT"].$path2UserPSFiles.$dir);
				if (strlen($title) <= 0)
					$title = $dir;
				else
					$title .= " (".$dir.")";
			}

			$arUserPSActions[] = array(
					"PATH" => $path2UserPSFiles.$dir,
					"TITLE" => $title,
					"DESCRIPTION" => $description,
					"NEW_FORMAT" => $newFormat
				);
		}
		@closedir($handle);
	}

	$handle = @opendir($_SERVER["DOCUMENT_ROOT"].$path2SystemPSFiles);
	if ($handle)
	{
		while (false !== ($dir = readdir($handle)))
		{
			if ($dir == "." || $dir == "..")
				continue;

			if (is_dir($_SERVER["DOCUMENT_ROOT"].$path2SystemPSFiles.$dir))
			{
				$newFormat = "Y";
				list($title, $description) = LocalGetPSActionDescr($_SERVER["DOCUMENT_ROOT"].$path2SystemPSFiles.$dir."/.description.php");
				if (strlen($title) <= 0)
					$title = $dir;
				else
					$title .= " (".$dir.")";
			}
			elseif (is_file($_SERVER["DOCUMENT_ROOT"].$path2SystemPSFiles.$dir))
			{
				$newFormat = "N";
				list($title, $description) = LocalGetPSActionDescr_old($_SERVER["DOCUMENT_ROOT"].$path2SystemPSFiles.$dir);
				if (strlen($title) <= 0)
					$title = $dir;
				else
					$title .= " (".$dir.")";
			}

			$arSystemPSActions[] = array(
					"PATH" => $path2SystemPSFiles.$dir,
					"TITLE" => $title,
					"DESCRIPTION" => $description,
					"NEW_FORMAT" => $newFormat
				);
		}
		@closedir($handle);
	}
	?>

	<?
/*	for ($tpInd = 0; $tpInd < count($arPersonTypeList); $tpInd++)
	{
		$arPersonType = $arPersonTypeList[$tpInd];
		?>

		<script language="JavaScript">
		<!--
		arPropFieldsList[<?= $arPersonType["ID"] ?>] = new Array();
		arPropFieldsNameList[<?= $arPersonType["ID"] ?>] = new Array();
		<?
		$dbOrderProps = CSaleOrderProps::GetList(
				array("SORT" => "ASC", "NAME" => "ASC"),
				array("PERSON_TYPE_ID" => $arPersonType["ID"]),
				false,
				false,
				array("ID", "CODE", "NAME", "TYPE", "SORT")
			);
		$i = -1;
		while ($arOrderProps = $dbOrderProps->Fetch())
		{
			$i++;
			?>
			arPropFieldsList[<?= $arPersonType["ID"] ?>][<?= $i ?>] = '<?= str_replace("'", "\'", ((strlen($arOrderProps["CODE"])>0) ? $arOrderProps["CODE"] : $arOrderProps["ID"])) ?>';
			arPropFieldsNameList[<?= $arPersonType["ID"] ?>][<?= $i ?>] = '<?= str_replace("'", "\'", $arOrderProps["NAME"]) ?>';
			<?
			if ($arOrderProps["TYPE"] == "LOCATION")
			{
				$i++;
				?>
				arPropFieldsList[<?= $arPersonType["ID"] ?>][<?= $i ?>] = '<?= str_replace("'", "\'", ((strlen($arOrderProps["CODE"])>0) ? $arOrderProps["CODE"] : $arOrderProps["ID"])."_COUNTRY") ?>';
				arPropFieldsNameList[<?= $arPersonType["ID"] ?>][<?= $i ?>] = '<?= str_replace("'", "\'", $arOrderProps["NAME"]." (".GetMessage("TPS_JCOUNTRY").")") ?>';
				<?

				$i++;
				?>
				arPropFieldsList[<?= $arPersonType["ID"] ?>][<?= $i ?>] = '<?= str_replace("'", "\'", ((strlen($arOrderProps["CODE"])>0) ? $arOrderProps["CODE"] : $arOrderProps["ID"])."_CITY") ?>';
				arPropFieldsNameList[<?= $arPersonType["ID"] ?>][<?= $i ?>] = '<?= str_replace("'", "\'", $arOrderProps["NAME"]." (".GetMessage("TPS_JCITY").")") ?>';
				<?
			}
		}
		?>
		//-->
		</script>
		<?*/
		//unset($str_ID); unset($str_NAME); unset($str_ACTION_FILE); unset($str_NEW_WINDOW);

		?>
		<tr>
			<td width="40%" valign="top"><span class="required">*</span><?= GetMessage("TPS_ACT_FILE") ?>:</td>
			<td width="60%" valign="top">
				<?
				$str_ACTION_FILE = str_replace("\\", "/", $str_ACTION_FILE);
				while (strpos($str_ACTION_FILE, "//") !== false)
					$str_ACTION_FILE = str_replace("//", "/", $str_ACTION_FILE);
				while (substr($str_ACTION_FILE, strlen($str_ACTION_FILE) - 1, 1) == "/")
					$str_ACTION_FILE = substr($str_ACTION_FILE, 0, strlen($str_ACTION_FILE) - 1);
				?>
				<script language="JavaScript">
				<!--
				arActionPaths = new Array();
				arActionDescrs = new Array();
				<?
				for ($i = 0; $i < count($arUserPSActions); $i++)
				{
					?>
					arActionPaths[<?= $i ?>] = '<?= $arUserPSActions[$i]["PATH"] ?>';
					arActionDescrs[<?= $i ?>] = '<?= str_replace("'", "\'", preg_replace("/[\s\n\r]+/", " ", $arUserPSActions[$i]["DESCRIPTION"])) ?>';
					<?
				}
				for ($i = 0; $i < count($arSystemPSActions); $i++)
				{
					?>
					arActionPaths[<?= $i + count($arUserPSActions) ?>] = '<?= $arSystemPSActions[$i]["PATH"] ?>';
					arActionDescrs[<?= $i + count($arUserPSActions) ?>] = '<?= str_replace("'", "\'", preg_replace("/[\s\n\r]+/", " ", $arSystemPSActions[$i]["DESCRIPTION"])) ?>';
					<?
				}
				?>
				//-->
				</script>
				<select name="ACTION_FILE" OnChange="ActionFileChange('Y')">
					<option value=""><?= GetMessage("TPS_NO_ACT_FILE") ?></option>
					<option value="">---- <?= GetMessage("TPS_ACT_USER") ?> ----</option>
					<?
					for ($i = 0; $i < count($arUserPSActions); $i++)
					{
						?><option value="<?= htmlspecialchars($arUserPSActions[$i]["PATH"]) ?>"<?if ($str_ACTION_FILE == $arUserPSActions[$i]["PATH"]) echo " selected";?>><?= htmlspecialcharsEx($arUserPSActions[$i]["TITLE"]) ?></option><?
					}
					?>
					<option value="">---- <?= GetMessage("TPS_ACT_SYSTEM") ?> ----</option>
					<?
					for ($i = 0; $i < count($arSystemPSActions); $i++)
					{
						?><option value="<?= htmlspecialchars($arSystemPSActions[$i]["PATH"]) ?>"<?if ($str_ACTION_FILE == $arSystemPSActions[$i]["PATH"]) echo " selected";?>><?= htmlspecialcharsEx($arSystemPSActions[$i]["TITLE"]) ?></option><?
					}
					?>
				</select>
				<div id="pay_sys_act_descr"></div>
			</td>
		</tr>
		<tr>
			<td width="40%"><?= GetMessage("TPS_NEW_WINDOW") ?>:</td>
			<td width="60%">
				<input type="checkbox" name="NEW_WINDOW" value="Y"<?if ($str_NEW_WINDOW == "Y") echo " checked";?>>
			</td>
		</tr>
		<tr>
			<td width="40%"><?= GetMessage("TPS_ENCODING") ?>:</td>
			<td width="60%">
				<input type="text" name="ENCODING" value="<?= $str_ENCODING ?>" size="20">
			</td>
		</tr>
		<tr>
			<td valign="top" align="center" colspan="2">
				<script language="JavaScript">
				<!--
				var paySysActVisible = true;
				<?
				if ($bInitVars)
				{
					$arActFields = explode(",", ${"PS_ACTION_FIELDS_LIST"});
					foreach ($arActFields as &$field)
					{
						$field = Trim($field);
						$valueTmp = ${"VALUE1_".$field};
						if (strlen($typeTmp) <= 0)
							$valueTmp = ${"VALUE2_".$field};
						?>
						var param_<?=$field?>_type = '<?=CUtil::JSEscape(${"TYPE_".$field})?>';
						var param_<?=$field?>_value = '<?=CUtil::JSEscape($valueTmp)?>';
						<?
					}
					unset($field);
				}
				else
				{
					$arCorrespondence = CTszhPaySystem::UnSerializeParams($arPaySystem["PARAMS"]);
					foreach ($arCorrespondence as $key => $value)
					{
						$paramValueJs = is_array($value["VALUE"]) || is_object($value["VALUE"]) ? CUtil::PhpToJSObject($value["VALUE"]) : "'" . CUtil::JSEscape($value["VALUE"]) . "'";
						?>
						var param_<?= $key ?>_type = '<?=CUtil::JSEscape($value["TYPE"])?>';
						var param_<?= $key ?>_value = <?=$paramValueJs?>;
						<?
					}
				}
				?>
				//-->
				</script>
				<div id="pay_sys_act" style="display: block; background-color: #E4EDF3;">
				</div>
				<a href="javascript:ShowHideStatus();" id="pay_sys_switch"><br><?= GetMessage("TPS_HIDE_PROPS") ?></a><iframe style="width:0px; height:0px; border: 0px" name="hidden_action_frame" src="" width="0" height="0"></iframe>
			</td>
		</tr>

		<tr>
			<td valign="top" align="right" colspan="2">
				<script language="JavaScript">
				<!--
				ActionFileChange();
				//-->
				</script>
				<input type="hidden" name="PS_ACTION_FIELDS_LIST" value="">
			</td>
		</tr>


		<?
	//}
	?>
<?
$tabControl->EndTab();

$tabControl->Buttons(
		array(
				"disabled" => ($tszhPaymentModulePermissions < "W"),
				"back_url" => "/bitrix/admin/tszh_pay_systems.php?lang=".LANG.GetFilterParams("filter_")
			)
	);
?>

<?
$tabControl->End();
?>

</form>

<?echo BeginNote();?>
<span class="required">*</span> <?echo GetMessage("REQUIRED_FIELDS")?>
<?echo EndNote(); ?>

<?require($DOCUMENT_ROOT."/bitrix/modules/main/include/epilog_admin.php");?>