<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhpayment/include.php");

$tszhPaymentModulePermissions = $APPLICATION->GetGroupRight("citrus.tszhpayment");
if ($tszhPaymentModulePermissions == "D")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

IncludeModuleLangFile(__FILE__);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhpayment/prolog.php");

$UF_ENTITY = "TSZH_PAYMENT";

$sTableID = "tbl_tszh_payments";

$oSort = new CAdminSorting($sTableID, "ID", "asc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = array(
	"filter_id",
	"filter_lid",
	"filter_user_id",
	"filter_summ1",
	"filter_summ2",
	"filter_summ_payed1",
	"filter_summ_payed2",
	"filter_currency",
	"filter_payed",
	"filter_date_payed1",
	"filter_date_payed2",
	"filter_emp_payed_id",
	"filter_pay_system_id",
	"filter_account_id",
	"filter_insurance_included",
	"filter_psys_status",
	"filter_psys_status_descr",
	"filter_psys_status_response",
	"filter_psys_status_date1",
	"filter_psys_status_date2",
	"filter_date_insert1",
	"filter_date_insert2",
	"filter_date_update1",
	"filter_date_update2",
	"filter_is_overhaul",
	"filter_is_penalty",
);

$USER_FIELD_MANAGER->AdminListAddFilterFields($UF_ENTITY, $arFilterFields);

$lAdmin->InitFilter($arFilterFields);

//������������� ������� ������� ��� GetList
function CheckFilter($FilterArr) // �������� ��������� �����
{
	global $strError;
	foreach ($FilterArr as $f)
		global $$f;

	$str = "";

	if (strlen(trim($filter_summ1)) > 0 && strlen(trim($filter_summ2)) > 0)
	{
		if (FloatVal($filter_summ1) > $filter_summ2)
		{
			$str .= GetMessage("TSZH_PAYMENTS_WRONG_SUMMS") . "<br />";
		}
	}

	if (strlen(trim($filter_summ_payed1)) > 0 && strlen(trim($filter_summ_payed2)) > 0)
	{
		if (FloatVal($filter_summ_payed1) > $filter_summ_payed2)
		{
			$str .= GetMessage("TSZH_PAYMENTS_WRONG_SUMM_PAYEDS") . "<br />";
		}
	}

	if (strlen(trim($filter_date_payed1)) > 0 || strlen(trim($filter_date_payed2)) > 0)
	{
		$date_1_ok = false;
		$date1_stm = MkDateTime(FmtDate($filter_date_payed1, "D.M.Y"), "d.m.Y");
		$date2_stm = MkDateTime(FmtDate($filter_date_payed2, "D.M.Y") . " 23:59", "d.m.Y H:i");
		if (!$date1_stm && strlen(trim($filter_date_payed1)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_PAYED_FROM") . "<br />";
		}
		else
		{
			$date_1_ok = true;
		}
		if (!$date2_stm && strlen(trim($filter_date_payed2)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_PAYED_TILL") . "<br />";
		}
		elseif ($date_1_ok && $date2_stm <= $date1_stm && strlen($date2_stm) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_PAYED_FROM_TILL") . "<br />";
		}
		$filter_date_payed2 = FmtDate($filter_date_payed2, "D.M.Y") . " 23:59";
	}

	if (strlen(trim($filter_date_insert1)) > 0 || strlen(trim($filter_date_insert2)) > 0)
	{
		$date_1_ok = false;
		$date1_stm = MkDateTime(FmtDate($filter_date_insert1, "D.M.Y"), "d.m.Y");
		$date2_stm = MkDateTime(FmtDate($filter_date_insert2, "D.M.Y") . " 23:59", "d.m.Y H:i");
		if (!$date1_stm && strlen(trim($filter_date_insert1)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_INSERT_FROM") . "<br />";
		}
		else $date_1_ok = true;
		if (!$date2_stm && strlen(trim($filter_date_insert2)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_INSERT_TILL") . "<br />";
		}
		elseif ($date_1_ok && $date2_stm <= $date1_stm && strlen($date2_stm) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_INSERT_FROM_TILL") . "<br />";
		}
		$filter_date_insert2 = FmtDate($filter_date_insert2, "D.M.Y") . " 23:59";
	}
	if (strlen(trim($filter_date_update1)) > 0 || strlen(trim($filter_date_update2)) > 0)
	{
		$date_1_ok = false;
		$date1_stm = MkDateTime(FmtDate($filter_date_update1, "D.M.Y"), "d.m.Y");
		$date2_stm = MkDateTime(FmtDate($filter_date_update2, "D.M.Y") . " 23:59", "d.m.Y H:i");
		if (!$date1_stm && strlen(trim($filter_date_update1)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_UPDATE_FROM") . "<br />";
		}
		else $date_1_ok = true;
		if (!$date2_stm && strlen(trim($filter_date_update2)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_UPDATE_TILL") . "<br />";
		}
		elseif ($date_1_ok && $date2_stm <= $date1_stm && strlen($date2_stm) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_UPDATE_FROM_TILL") . "<br />";
		}
		$filter_date_update2 = FmtDate($filter_date_update2, "D.M.Y") . " 23:59";
	}

	if (strlen(trim($filter_psys_status_date1)) > 0 || strlen(trim($filter_psys_status_date2)) > 0)
	{
		$date_1_ok = false;
		$date1_stm = MkDateTime(FmtDate($filter_psys_status_date1, "D.M.Y"), "d.m.Y");
		$date2_stm = MkDateTime(FmtDate($filter_psys_status_date2, "D.M.Y") . " 23:59", "d.m.Y H:i");
		if (!$date1_stm && strlen(trim($filter_psys_status_date1)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_PSYS_STATUS_DATE_FROM") . "<br />";
		}
		else $date_1_ok = true;
		if (!$date2_stm && strlen(trim($filter_psys_status_date2)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_PSYS_STATUS_DATE_TILL") . "<br />";
		}
		elseif ($date_1_ok && $date2_stm <= $date1_stm && strlen($date2_stm) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_PSYS_STATUS_DATE_FROM_TILL") . "<br />";
		}
	}

	$strError .= $str;
	if (strlen($str) > 0)
	{
		global $lAdmin;
		$lAdmin->AddFilterError($str);
		return false;
	}

	return true;
}

$arFilter = array();

if (CheckFilter($arFilterFields))
{
	if (strlen($filter_id) > 0)
	{
		$arFilter["ID"] = IntVal(Trim($filter_id));
	}
	if (strlen($filter_lid) > 0 && $filter_lid != "NOT_REF")
	{
		$arFilter["LID"] = Trim($filter_lang);
	}
	if (strlen($filter_user_id) > 0)
	{
		$arFilter["USER_ID"] = Trim($filter_user_id);
	}
	if (strlen($filter_summ1) > 0)
	{
		$arFilter[">=SUMM"] = FloatVal(Trim($filter_summ1));
	}
	if (strlen($filter_summ2) > 0)
	{
		$arFilter["<=SUMM"] = FloatVal(Trim($filter_summ2));
	}
	if (strlen($filter_summ_payed1) > 0)
	{
		$arFilter[">=SUMM_PAYED"] = FloatVal(Trim($filter_summ_payed1));
	}
	if (strlen($filter_summ_payed2) > 0)
	{
		$arFilter["<=SUMM_PAYED"] = FloatVal(Trim($filter_summ_payed2));
	}
	if (strlen($filter_currency) > 0)
	{
		$arFilter["CURRENCY"] = Trim($filter_currency);
	}
	if (strlen($filter_is_overhaul) > 0 && $filter_is_overhaul != "NOT_REF")
	{
		$arFilter["IS_OVERHAUL"] = ($filter_is_overhaul == "Y" ? "Y" : "N");
	}
	if (strlen($filter_is_penalty) > 0 && $filter_is_penalty != "NOT_REF")
	{
		$arFilter["IS_PENALTY"] = ($filter_is_penalty == "Y" ? "Y" : "N");
	}
	if (strlen($filter_payed) > 0 && $filter_payed != "NOT_REF")
	{
		$arFilter["PAYED"] = ($filter_payed == "Y" ? "Y" : "N");
	}
	if (strlen($filter_date_payed1) > 0)
	{
		$arFilter[">=DATE_PAYED"] = Trim($filter_date_payed1);
	}
	if (strlen($filter_date_payed2) > 0)
	{
		$arFilter["<=DATE_PAYED"] = Trim($filter_date_payed2);
	}
	if (strlen($filter_emp_payed_id) > 0)
	{
		$arFilter["EMP_PAYED_ID"] = Trim($filter_emp_payed_id);
	}
	if (strlen($filter_pay_system_id) > 0 && $filter_pay_system_id != "NOT_REF")
	{
		$arFilter["PAY_SYSTEM_ID"] = Trim($filter_pay_system_id);
	}
	if (strlen($filter_account_id) > 0)
	{
		$arFilter["ACCOUNT_ID"] = Trim($filter_account_id);
	}
	if (strlen($filter_psys_status) > 0)
	{
		$arFilter["%PSYS_STATUS"] = Trim($filter_psys_status);
	}
	if (strlen($filter_psys_status_descr) > 0)
	{
		$arFilter["%PSYS_STATUS_DESCR"] = Trim($filter_psys_status_descr);
	}
	if (strlen($filter_psys_status_response) > 0)
	{
		$arFilter["%PSYS_STATUS_RESPONSE"] = Trim($filter_psys_status_response);
	}
	if (strlen($filter_psys_status_date1) > 0)
	{
		$arFilter[">=PSYS_STATUS_DATE"] = Trim($filter_psys_status_date1);
	}
	if (strlen($filter_psys_status_date2) > 0)
	{
		$arFilter["<=PSYS_STATUS_DATE"] = Trim($filter_psys_status_date2);
	}
	if (strlen($filter_date_insert1) > 0)
	{
		$arFilter[">=DATE_INSERT"] = Trim($filter_date_insert1);
	}
	if (strlen($filter_date_insert2) > 0)
	{
		$arFilter["<=DATE_INSERT"] = Trim($filter_date_insert2);
	}
	if (strlen($filter_date_update1) > 0)
	{
		$arFilter[">=DATE_UPDATE"] = Trim($filter_date_update1);
	}
	if (strlen($filter_date_update2) > 0)
	{
		$arFilter["<=DATE_UPDATE"] = Trim($filter_date_update2);
	}

	$USER_FIELD_MANAGER->AdminListAddFilter($UF_ENTITY, $arFilter);
}

if (($arID = $lAdmin->GroupAction()) && $tszhPaymentModulePermissions >= "W")
{
	if ($_REQUEST['action_target'] == 'selected')
	{
		$arID = Array();
		$dbResultList = CTszhPayment::GetList(
			array($by => $order),
			$arFilter,
			false,
			false,
			array("ID")
		);
		while ($arResult = $dbResultList->Fetch())
		{
			$arID[] = $arResult['ID'];
		}
	}

	foreach ($arID as $ID)
	{
		if (strlen($ID) <= 0)
		{
			continue;
		}

		switch ($_REQUEST['action'])
		{
			case "delete":
				@set_time_limit(0);

				$DB->StartTransaction();

				if (!CTszhPayment::Delete($ID))
				{
					$DB->Rollback();

					if ($ex = $APPLICATION->GetException())
					{
						$lAdmin->AddGroupError($ex->GetString(), $ID);
					}
					else
					{
						$lAdmin->AddGroupError(GetMessage("TSZH_PAYMENTS_ERROR_DELETE"), $ID);
					}
				}

				$DB->Commit();

				break;

			case "activate":
			case "deactivate":

				$arFields = array(
					"ACTIVE" => (($_REQUEST['action'] == "activate") ? "Y" : "N")
				);

				if (!CTszhPayment::Update($ID, $arFields))
				{
					if ($ex = $APPLICATION->GetException())
					{
						$lAdmin->AddGroupError($ex->GetString(), $ID);
					}
					else
					{
						$lAdmin->AddGroupError(GetMessage("TSZH_PAYMENTS_ERROR_UPDATE"), $ID);
					}
				}

				break;
		}
	}
}

$dbResultList = CTszhPayment::GetList(
	array($by => $order),
	$arFilter,
	false,
	false,
	array('*', 'UF_*')
);

$dbResultList = new CAdminResult($dbResultList, $sTableID);
$dbResultList->NavStart();

$lAdmin->NavText($dbResultList->GetNavPrint(GetMessage("TSZH_PAYMENTS_NAV")));

$arAdminListHeaders = array(
	array("id" => "ID", "content" => "ID", "sort" => "ID", "default" => true),
	array("id" => "LID", "content" => GetMessage('TSZH_PAYMENT_LID'), "sort" => "LID", "default" => false),

	array("id" => "USER_ID", "content" => GetMessage('TSZH_PAYMENT_USER_ID'), "sort" => "USER_ID", "default" => true),
	array("id" => "ACCOUNT_ID", "content" => GetMessage('TSZH_PAYMENT_ACCOUNT_ID'), "sort" => "ACCOUNT_ID", "default" => true),

	array("id" => "SUMM", "content" => GetMessage('TSZH_PAYMENT_SUMM'), "sort" => "SUMM", "default" => true, "align" => "right"),
	array("id" => "SUMM_PAYED", "content" => GetMessage('TSZH_PAYMENT_SUMM_PAYED'), "sort" => "SUMM_PAYED", "default" => false, "align" => "right"),
	array("id" => "CURRENCY", "content" => GetMessage("TSZH_PAYMENT_CURRENCY"), "sort" => "CURRENCY", "default" => true),

	array("id" => "PAYED", "content" => GetMessage('TSZH_PAYMENT_PAYED'), "sort" => "PAYED", "default" => true),
	array("id" => "DATE_PAYED", "content" => GetMessage('TSZH_PAYMENT_DATE_PAYED'), "sort" => "DATE_PAYED", "default" => false),
	array("id" => "EMP_PAYED_ID", "content" => GetMessage('TSZH_PAYMENT_EMP_PAYED_ID'), "sort" => "EMP_PAYED_ID", "default" => false),

	array("id" => "PAY_SYSTEM_ID", "content" => GetMessage('TSZH_PAYMENT_PAY_SYSTEM_ID'), "sort" => "PAY_SYSTEM_ID", "default" => true),

	array("id" => "PSYS_STATUS", "content" => GetMessage('TSZH_PAYMENT_PSYS_STATUS'), "sort" => "PSYS_STATUS", "default" => false),
	array("id" => "PSYS_STATUS_DESCR", "content" => GetMessage('TSZH_PAYMENT_PSYS_STATUS_DESCR'), "sort" => "PSYS_STATUS_DESCR", "default" => false),
	array("id" => "PSYS_STATUS_RESPONSE", "content" => GetMessage('TSZH_PAYMENT_PSYS_STATUS_RESPONSE'), "sort" => "PSYS_STATUS_RESPONSE", "default" => false),
	array("id" => "PSYS_STATUS_DATE", "content" => GetMessage('TSZH_PAYMENT_PSYS_STATUS_DATE'), "sort" => "INSURANCE_INCLUDED", "default" => false),
    array("id" => "INSURANCE_INCLUDED", "content" => GetMessage('TSZH_PAYMENT_INSURANCE_INCLUDED'), "sort" => "PSYS_STATUS_DATE", "default" => false),

	array("id" => "DATE_INSERT", "content" => GetMessage('TSZH_PAYMENT_DATE_INSERT'), "sort" => "DATE_INSERT", "default" => false),
	array("id" => "DATE_UPDATE", "content" => GetMessage('TSZH_PAYMENT_DATE_UPDATE'), "sort" => "DATE_UPDATE", "default" => false),

	array("id" => "IS_OVERHAUL", "content" => GetMessage('TSZH_PAYMENT_IS_OVERHAUL'), "sort" => "IS_OVERHAUL", "default" => true),
	array("id" => "IS_PENALTY", "content" => GetMessage('TSZH_PAYMENT_IS_PENALTY'), "sort" => "IS_PENALTY", "default" => true),
);

$USER_FIELD_MANAGER->AdminListAddHeaders($UF_ENTITY, $arAdminListHeaders);

$lAdmin->AddHeaders($arAdminListHeaders);

$arVisibleColumns = $lAdmin->GetVisibleHeaderColumns();

while ($arPayment = $dbResultList->NavNext(true, "f_"))
{
	$row =& $lAdmin->AddRow($f_ID, $arPayment, "tszh_payments_edit.php?ID=" . $f_ID . "&lang=" . LANG . GetFilterParams("filter_"), GetMessage("TSZH_PAYMENTS_EDIT_DESCR"));

	$row->AddViewField("ID", $f_ID);
	$row->AddViewField("LID", $f_LID);

	$row->AddViewField("CURRENCY", $f_CURRENCY);

	$row->AddViewField('PAYED', $arPayment["PAYED"] == 'Y' ? GetMessage("TSZH_PAYMENT_YES") : GetMessage("TSZH_PAYMENT_NO"));
	$row->AddViewField('DATE_PAYED', $arPayment["DATE_PAYED"]);
	$row->AddViewField('EMP_PAYED_ID', "[<a href=\"/bitrix/admin/user_edit.php?ID={$arPayment["EMP_PAYED_ID"]}&lang=" . LANG . "\">{$arPayment["EMP_PAYED_ID"]}</a>]&nbsp;" . CTszhAccount::GetFullName($arPayment["EMP_PAYED_ID"]));

	$row->AddViewField('SUMM', number_format($arPayment["SUMM"], 2, ',', ''));
	$row->AddViewField('SUMM_PAYED', number_format($arPayment["SUMM_PAYED"], 2, ',', ''));

	if (isset($arPayment["USER_ID"]))
	{
		$row->AddViewField('USER_ID', "[<a href=\"/bitrix/admin/user_edit.php?ID={$arPayment["USER_ID"]}&lang=" . LANG . "\">{$arPayment["USER_ID"]}</a>]&nbsp;" . CTszhAccount::GetFullName($arPayment["USER_ID"]));
	}

	if (isset($arPayment["ACCOUNT_ID"]) && intval($arPayment["ACCOUNT_ID"] > 0))
	{
		$arAccount = CTszhAccount::GetByID($arPayment['ACCOUNT_ID']);
		$row->AddViewField('ACCOUNT_ID', "[<a href=\"/bitrix/admin/tszh_account_edit.php?ID={$arPayment["ACCOUNT_ID"]}&lang=" . LANG . "\">{$arPayment["ACCOUNT_ID"]}</a>]&nbsp;" . CTszhAccount::GetFullName($arAccount));
	}
	elseif (isset($arPayment["C_ACCOUNT"]))
	{
		$row->AddViewField('ACCOUNT_ID', GetMessage("CITRUS_TSZH_ACCOUNT_NUMBER") . "{$arPayment["C_ACCOUNT"]}, {$arPayment["C_PAYEE_NAME"]}");
	}
	else
	{
		$row->AddViewField('ACCOUNT_ID', '');
	}

	$row->AddViewField('PAY_SYSTEM_ID', "[<a href=\"/bitrix/admin/tszh_pay_systems_edit.php?ID={$arPayment["PAY_SYSTEM_ID"]}&lang=" . LANG . "\">{$arPayment["PAY_SYSTEM_ID"]}</a>]&nbsp;" . $arPayment["PS_NAME"]);

	$row->AddViewField('PSYS_STATUS', $arPayment["PSYS_STATUS"]);
	$row->AddViewField('PSYS_STATUS_DESCR', $arPayment["PSYS_STATUS_DESCR"]);
	$row->AddViewField('PSYS_STATUS_RESPONSE', htmlspecialchars($arPayment["PSYS_STATUS_RESPONSE"]));
	$row->AddViewField('PSYS_STATUS_DATE', $arPayment["PSYS_STATUS_DATE"]);
    $row->AddViewField('INSURANCE_INCLUDED', ($arPayment["INSURANCE_INCLUDED"] > 0) ? GetMessage("TSZH_PAYMENT_YES") : GetMessage("TSZH_PAYMENT_NO"));

	$row->AddViewField('DATE_INSERT', $arPayment["DATE_INSERT"]);
	$row->AddViewField('DATE_UPDATE', $arPayment["DATE_UPDATE"]);

	$row->AddViewField('IS_OVERHAUL', $arPayment["IS_OVERHAUL"] == 'Y' ? GetMessage("TSZH_PAYMENT_YES") : GetMessage("TSZH_PAYMENT_NO"));
	$row->AddViewField('IS_PENALTY', $arPayment["IS_PENALTY"] == 'Y' ? GetMessage("TSZH_PAYMENT_YES") : GetMessage("TSZH_PAYMENT_NO"));

	$USER_FIELD_MANAGER->AddUserFields($UF_ENTITY, $arPayment, $row);

	$arActions = Array();
	$arActions[] = array("ICON" => "edit", "TEXT" => GetMessage("TSZH_PAYMENTS_EDIT"), "TITLE" => GetMessage("TSZH_PAYMENTS_EDIT_DESCR"), "ACTION" => $lAdmin->ActionRedirect("tszh_payments_edit.php?ID=" . $f_ID . "&lang=" . LANG . GetFilterParams("filter_") . ""), "DEFAULT" => true);
	if ($tszhPaymentModulePermissions >= "W")
	{
		$arActions[] = array("SEPARATOR" => true);
		$arActions[] = array("ICON" => "delete", "TEXT" => GetMessage("TSZH_PAYMENTS_DELETE"), "TITLE" => GetMessage("TSZH_PAYMENTS_DELETE_DESCR"), "ACTION" => "if(confirm('" . GetMessage('TSZH_PAYMENTS_CONFIRM_DEL_MESSAGE') . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete"));
	}

	$row->AddActions($arActions);
}

$lAdmin->AddFooter(
	array(
		array(
			"title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"),
			"value" => $dbResultList->SelectedRowsCount()
		),
		array(
			"counter" => true,
			"title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"),
			"value" => "0"
		),
	)
);

$lAdmin->AddGroupActionTable(
	array(
		"delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"),
		"activate" => GetMessage("MAIN_ADMIN_LIST_ACTIVATE"),
		"deactivate" => GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"),
	)
);

if ($tszhPaymentModulePermissions == "W")
{
	$arDDMenu = array();

	/*
	$arDDMenu[] = array(
		"TEXT" => "<b>".GetMessage("SOPAN_4NEW_PROMT")."</b>",
		"ACTION" => false
	);
	*/

	$aContext = array(/*		array(
			"TEXT" => GetMessage("SPSAN_ADD_NEW"),
			"TITLE" => GetMessage("SPSAN_ADD_NEW_ALT"),
			"ICON" => "btn_new",
			"MENU" => $arDDMenu
		),*/
	);
	$lAdmin->AddAdminContextMenu($aContext);
}

$lAdmin->CheckListMode();


/****************************************************************************/
/***********  MAIN PAGE  ****************************************************/
/****************************************************************************/
$APPLICATION->SetTitle(GetMessage("TSZH_PAYMENTS_SECTION_TITLE"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>

	<form name="find_form" method="GET" action="<? echo $APPLICATION->GetCurPage() ?>?"> <?=CTszhFlysheets::ShowBannerHtml();?>
		<?
		$arFindFields = array(
			GetMessage("TSZH_PAYMENT_LID"),
			GetMessage("TSZH_PAYMENT_USER_ID"),
			GetMessage("TSZH_PAYMENT_ACCOUNT_ID"),
			GetMessage("TSZH_PAYMENT_SUMM"),
			GetMessage("TSZH_PAYMENT_SUMM_PAYED"),
			GetMessage("TSZH_PAYMENT_CURRENCY"),
			GetMessage("TSZH_PAYMENT_PAYED"),
			GetMessage("TSZH_PAYMENT_DATE_PAYED"),
			GetMessage("TSZH_PAYMENT_EMP_PAYED_ID"),
			GetMessage("TSZH_PAYMENT_PAY_SYSTEM_ID"),
			GetMessage("TSZH_PAYMENT_PSYS_STATUS"),
			GetMessage("TSZH_PAYMENT_PSYS_STATUS_DESCR"),
			GetMessage("TSZH_PAYMENT_PSYS_STATUS_RESPONSE"),
			GetMessage("TSZH_PAYMENT_PSYS_STATUS_DATE"),
			GetMessage("TSZH_PAYMENT_DATE_INSERT"),
			GetMessage("TSZH_PAYMENT_DATE_UPDATE"),
			GetMessage("TSZH_PAYMENT_IS_OVERHAUL"),
			GetMessage("TSZH_PAYMENT_IS_PENALTY"),
		);

		$USER_FIELD_MANAGER->AddFindFields($UF_ENTITY, $arFindFields);
		$oFilter = new CAdminFilter(
			$sTableID . "_filter",
			$arFindFields
		);

		$oFilter->Begin();
		?>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_ID") ?></td>
			<td><input type="text" name="filter_id" size="21" value="<? echo htmlspecialchars($filter_id) ?>"/></td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_LID"); ?>:</td>
			<td>
				<? echo CLang::SelectBox("filter_lid", $filter_lid, "(" . GetMessage("TSZH_PAYMENT_ALL") . ")") ?>
			</td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_USER_ID"); ?>:</td>
			<td><?= FindUserID('filter_user_id', $filter_user_id, '', 'find_form'); ?></td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_ACCOUNT_ID"); ?>:</td>
			<td><?= tszhLookup('filter_account_id', htmlspecialchars($filter_account_id), Array("type" => 'account', "formName" => 'find_form')); ?></td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_SUMM"); ?>:</td>
			<td>
				<?= GetMessage("TSZH_PAYMENT_FROM") ?>
				<input type="text" name="filter_summ1" size="10" value="<? echo htmlspecialchars($filter_summ1) ?>"/>
				<?= GetMessage("TSZH_PAYMENT_TO") ?>
				<input type="text" name="filter_summ2" size="10" value="<? echo htmlspecialchars($filter_summ1) ?>"/>
			</td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_SUMM_PAYED"); ?>:</td>
			<td>
				<?= GetMessage("TSZH_PAYMENT_FROM") ?>
				<input type="text" name="filter_summ_payed1" size="10"
				       value="<? echo htmlspecialchars($filter_summ_payed1) ?>"/>
				<?= GetMessage("TSZH_PAYMENT_TO") ?>
				<input type="text" name="filter_summ_payed2" size="10"
				       value="<? echo htmlspecialchars($filter_summ_payed1) ?>"/>
			</td>
		</tr>
		<? if (CModule::IncludeModule("currency")): ?>
			<tr>
				<td><? echo GetMessage("TSZH_PAYMENT_CURRENCY") ?>:</td>
				<td>
					<? echo CCurrency::SelectBox("filter_currency", $filter_currency, "(" . GetMessage("TSZH_PAYMENT_ALL") . ")", True, "", "") ?>
				</td>
			</tr>
		<? else: ?>
			<tr>
				<td><? echo GetMessage("TSZH_PAYMENT_CURRENCY") ?>:</td>
				<td>
					<input type="text" name="filter_currency" value="<?= $filter_currency ?>" size="10">
				</td>
			</tr>
		<? endif; ?>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_PAYED") ?>:</td>
			<td>
				<select name="filter_payed">
					<option value="">(<? echo GetMessage("TSZH_PAYMENT_ALL") ?>)</option>
					<option
						value="Y"<? if ($filter_payed == "Y") echo " selected" ?>><? echo GetMessage("TSZH_PAYMENT_YES") ?></option>
					<option
						value="N"<? if ($filter_payed == "N") echo " selected" ?>><? echo GetMessage("TSZH_PAYMENT_NO") ?></option>
				</select>
			</td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_DATE_PAYED") . " (" . CLang::GetDateFormat("SHORT") . "):" ?></td>
			<td><? echo CalendarPeriod("filter_date_payed1", htmlspecialchars($filter_date_payed1), "filter_date_payed2", htmlspecialchars($filter_date_payed2), "find_form", "Y") ?></td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_EMP_PAYED_ID"); ?>:</td>
			<td><?= FindUserID('filter_emp_payed_id', $filter_emp_payed_id, '', 'find_form'); ?></td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_PAY_SYSTEM_ID"); ?>:</td>
			<td><?

				echo SelectBox('filter_pay_system_id', CTszhPaySystem::GetSelectBoxList(Array("SORT" => "ASC")), "(" . GetMessage("TSZH_PAYMENT_ALL") . ")", $find_pay_system_id);

				?></td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_PSYS_STATUS") ?>:</td>
			<td>
				<input type="text" name="filter_psys_status" value="<?= $filter_psys_status ?>" size="40">
			</td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_PSYS_STATUS_DESCR") ?>:</td>
			<td>
				<input type="text" name="filter_psys_status_descr" value="<?= $filter_psys_status_descr ?>" size="40">
			</td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_PSYS_STATUS_RESPONSE") ?>:</td>
			<td>
				<input type="text" name="filter_psys_status_response" value="<?= $filter_psys_status_response ?>"
				       size="40">
			</td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_PSYS_STATUS_DATE") . " (" . CLang::GetDateFormat("SHORT") . "):" ?></td>
			<td><? echo CalendarPeriod("filter_psys_status_date1", htmlspecialchars($filter_psys_status_date1), "filter_psys_status_date2", htmlspecialchars($filter_psys_status_date2), "find_form", "Y") ?></td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_DATE_INSERT") . " (" . CLang::GetDateFormat("SHORT") . "):" ?></td>
			<td><? echo CalendarPeriod("filter_date_insert1", htmlspecialchars($filter_date_insert1), "filter_date_insert2", htmlspecialchars($filter_date_insert2), "find_form", "Y") ?></td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_DATE_UPDATE") . " (" . CLang::GetDateFormat("SHORT") . "):" ?></td>
			<td><? echo CalendarPeriod("filter_date_update1", htmlspecialchars($filter_date_update1), "filter_date_update2", htmlspecialchars($filter_date_update2), "find_form", "Y") ?></td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_IS_OVERHAUL") ?>:</td>
			<td>
				<select name="filter_is_overhaul">
					<option value="">(<? echo GetMessage("TSZH_PAYMENT_ALL") ?>)</option>
					<option
							value="Y"<? if ($filter_is_overhaul == "Y") echo " selected" ?>><? echo GetMessage("TSZH_PAYMENT_YES") ?></option>
					<option
							value="N"<? if ($filter_is_overhaul == "N") echo " selected" ?>><? echo GetMessage("TSZH_PAYMENT_NO") ?></option>
				</select>
			</td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_IS_PENALTY") ?>:</td>
			<td>
				<select name="filter_is_penalty">
					<option value="">(<? echo GetMessage("TSZH_PAYMENT_ALL") ?>)</option>
					<option
							value="Y"<? if ($filter_is_penalty == "Y") echo " selected" ?>><? echo GetMessage("TSZH_PAYMENT_YES") ?></option>
					<option
							value="N"<? if ($filter_is_penalty == "N") echo " selected" ?>><? echo GetMessage("TSZH_PAYMENT_NO") ?></option>
				</select>
			</td>
		</tr>
		<?
		$USER_FIELD_MANAGER->AdminListShowFilter($UF_ENTITY);

		$oFilter->Buttons(
			array(
				"table_id" => $sTableID,
				"url" => $APPLICATION->GetCurPage(),
				"form" => "find_form"
			)
		);
		$oFilter->End();
		?>
	</form>

<?
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>