<?

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhpayment/include.php");

use Citrus\Tszhpayment\FlysheetsTable;

CModule::IncludeModule("citrus.tszhpayment");
CJSCore::Init(array("popup", 'window'));
$tszhPaymentModulePermissions = $APPLICATION->GetGroupRight("citrus.tszhpayment");

if ($tszhPaymentModulePermissions == "D")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

IncludeModuleLangFile(__FILE__);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhpayment/prolog.php"); ?>
<?

$UF_ENTITY = "TSZH_FLYSHEETS";

$sTableID = "tbl_tszh_flysheets";

list($arTszhs, $arTszhSites) = citrusTszhPaymentGetTszhList();

$oSort = new CAdminSorting($sTableID, "ID", "asc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = array(
	"filter_id",
);

$USER_FIELD_MANAGER->AdminListAddFilterFields($UF_ENTITY, $arFilterFields);

$lAdmin->InitFilter($arFilterFields);

//������������� ������� ������� ��� GetList
function CheckFilter($FilterArr) // �������� ��������� �����
{
	global $strError;
	foreach ($FilterArr as $f)
	{
		global $$f;
	}

	$str = "";

	if (strlen(trim($filter_summ1)) > 0 && strlen(trim($filter_summ2)) > 0)
	{
		if (FloatVal($filter_summ1) > $filter_summ2)
		{
			$str .= GetMessage("TSZH_PAYMENTS_WRONG_SUMMS") . "<br />";
		}
	}

	if (strlen(trim($filter_summ_payed1)) > 0 && strlen(trim($filter_summ_payed2)) > 0)
	{
		if (FloatVal($filter_summ_payed1) > $filter_summ_payed2)
		{
			$str .= GetMessage("TSZH_PAYMENTS_WRONG_SUMM_PAYEDS") . "<br />";
		}
	}

	if (strlen(trim($filter_date_payed1)) > 0 || strlen(trim($filter_date_payed2)) > 0)
	{
		$date_1_ok = false;
		$date1_stm = MkDateTime(FmtDate($filter_date_payed1, "D.M.Y"), "d.m.Y");
		$date2_stm = MkDateTime(FmtDate($filter_date_payed2, "D.M.Y") . " 23:59", "d.m.Y H:i");
		if (!$date1_stm && strlen(trim($filter_date_payed1)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_PAYED_FROM") . "<br />";
		}
		else
		{
			$date_1_ok = true;
		}
		if (!$date2_stm && strlen(trim($filter_date_payed2)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_PAYED_TILL") . "<br />";
		}
		elseif ($date_1_ok && $date2_stm <= $date1_stm && strlen($date2_stm) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_PAYED_FROM_TILL") . "<br />";
		}
		$filter_date_payed2 = FmtDate($filter_date_payed2, "D.M.Y") . " 23:59";
	}

	if (strlen(trim($filter_date_insert1)) > 0 || strlen(trim($filter_date_insert2)) > 0)
	{
		$date_1_ok = false;
		$date1_stm = MkDateTime(FmtDate($filter_date_insert1, "D.M.Y"), "d.m.Y");
		$date2_stm = MkDateTime(FmtDate($filter_date_insert2, "D.M.Y") . " 23:59", "d.m.Y H:i");
		if (!$date1_stm && strlen(trim($filter_date_insert1)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_INSERT_FROM") . "<br />";
		}
		else
		{
			$date_1_ok = true;
		}
		if (!$date2_stm && strlen(trim($filter_date_insert2)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_INSERT_TILL") . "<br />";
		}
		elseif ($date_1_ok && $date2_stm <= $date1_stm && strlen($date2_stm) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_INSERT_FROM_TILL") . "<br />";
		}
		$filter_date_insert2 = FmtDate($filter_date_insert2, "D.M.Y") . " 23:59";
	}
	if (strlen(trim($filter_date_update1)) > 0 || strlen(trim($filter_date_update2)) > 0)
	{
		$date_1_ok = false;
		$date1_stm = MkDateTime(FmtDate($filter_date_update1, "D.M.Y"), "d.m.Y");
		$date2_stm = MkDateTime(FmtDate($filter_date_update2, "D.M.Y") . " 23:59", "d.m.Y H:i");
		if (!$date1_stm && strlen(trim($filter_date_update1)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_UPDATE_FROM") . "<br />";
		}
		else
		{
			$date_1_ok = true;
		}
		if (!$date2_stm && strlen(trim($filter_date_update2)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_UPDATE_TILL") . "<br />";
		}
		elseif ($date_1_ok && $date2_stm <= $date1_stm && strlen($date2_stm) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_DATE_UPDATE_FROM_TILL") . "<br />";
		}
		$filter_date_update2 = FmtDate($filter_date_update2, "D.M.Y") . " 23:59";
	}

	if (strlen(trim($filter_psys_status_date1)) > 0 || strlen(trim($filter_psys_status_date2)) > 0)
	{
		$date_1_ok = false;
		$date1_stm = MkDateTime(FmtDate($filter_psys_status_date1, "D.M.Y"), "d.m.Y");
		$date2_stm = MkDateTime(FmtDate($filter_psys_status_date2, "D.M.Y") . " 23:59", "d.m.Y H:i");
		if (!$date1_stm && strlen(trim($filter_psys_status_date1)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_PSYS_STATUS_DATE_FROM") . "<br />";
		}
		else
		{
			$date_1_ok = true;
		}
		if (!$date2_stm && strlen(trim($filter_psys_status_date2)) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_PSYS_STATUS_DATE_TILL") . "<br />";
		}
		elseif ($date_1_ok && $date2_stm <= $date1_stm && strlen($date2_stm) > 0)
		{
			$str .= GetMessage("MAIN_WRONG_PSYS_STATUS_DATE_FROM_TILL") . "<br />";
		}
	}

	$strError .= $str;
	if (strlen($str) > 0)
	{
		global $lAdmin;
		$lAdmin->AddFilterError($str);

		return false;
	}

	return true;
}

$arFilter = array();

if (CheckFilter($arFilterFields))
{
	if (strlen($filter_id) > 0)
	{
		$arFilter["ID"] = IntVal(Trim($filter_id));
	}

	$USER_FIELD_MANAGER->AdminListAddFilter($UF_ENTITY, $arFilter);
}

if (($arID = $lAdmin->GroupAction()) && $tszhPaymentModulePermissions >= "W")
{
	if ($_REQUEST['action_target'] == 'selected')
	{
		$arID = Array();
		$dbResultList = FlysheetsTable::GetList(array("ORDER" => array($by => $order), "FILTER" => $arFilter, "SELECT" => array("ID")));
		while ($arResult = $dbResultList->Fetch())
		{
			$arID[] = $arResult['ID'];
		}
	}

	@set_time_limit(0);

	foreach ($arID as $ID)
	{
		if (strlen($ID) <= 0)
		{
			continue;
		}

		switch ($_REQUEST['action'])
		{
			//��������
			case "delete":
				$arItem = FlysheetsTable::getByID($ID)->fetch();
				if (is_array($arItem))
				{
					if (FlysheetsTable::delete(IntVal($ID)))
					{
						CFile::Delete($arItem["FILE_ID"]);
						$messageOK = GetMessage("qroup_del_ok");
					}
					else
					{
						$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR"), $ID);
					}
				}
				else
				{
					$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR") . " " . GetMessage("TSZH_DELETE_ITEM_NOT_FOUND"), $ID);
				}
				break;
		}
	}
}

$dbResultList = FlysheetsTable::GetList(
	array("select" => Array("*"))
);

$dbResultList = new CAdminResult($dbResultList, $sTableID);
$dbResultList->NavStart();

$lAdmin->NavText($dbResultList->GetNavPrint(GetMessage("TSZH_PAYMENTS_NAV")));

$arAdminListHeaders = array(
	array("id" => "ID", "content" => "ID", "sort" => "ID", "default" => true),
	array("id" => "DESCRIPTION", "content" => GetMessage("DESCRIPTION"), "sort" => "DESCRIPTION", "default" => true),
	array("id" => "FILE_ID", "content" => GetMessage("IMG_SRC"), "sort" => "FILE_ID", "default" => true),
);

$USER_FIELD_MANAGER->AdminListAddHeaders($UF_ENTITY, $arAdminListHeaders);

$lAdmin->AddHeaders($arAdminListHeaders);

$arVisibleColumns = $lAdmin->GetVisibleHeaderColumns();

while ($arFlysheets = $dbResultList->NavNext(true, "f_"))
{
	$row =& $lAdmin->AddRow($f_ID, $arFlysheets, "tszh_flysheets_list.php", GetMessage("TSZH_PAYMENTS_EDIT_DESCR"));

	$row->AddViewField("ID", $f_ID);

	if (isset($arFlysheets["FILE_ID"]))
	{
		$row->AddViewField('FILE_ID', "<img alt=\"�������� ��� ���� ����� �����������\" style=\"cursor: pointer; height:200px;\" src=\"" . CFile::GetPath($arFlysheets['FILE_ID']) . "\" onclick=\"var printwindow1 = window.open('" . CFile::GetPath($arFlysheets['FILE_ID']) . "'); printwindow1.print()\";>");
	}


	$USER_FIELD_MANAGER->AddUserFields($UF_ENTITY, $arFlysheets, $row);

	$arActions = Array();
	$arActions[] = array(
		"ICON" => "edit",
		"TEXT" => GetMessage("TSZH_FLYSHEETS_EDIT"),
		"TITLE" => GetMessage("TSZH_FLYSHEETS_EDIT_DESCR"),
		"ACTION" => $lAdmin->ActionRedirect("tszh_flysheets_edit.php?ID=" . $f_ID . "&lang=" . LANG . GetFilterParams("filter_") . ""),
		"DEFAULT" => true,
	);
	if ($tszhPaymentModulePermissions >= "W")
	{
		$arActions[] = array("SEPARATOR" => true);
		$arActions[] = array(
			"ICON" => "delete",
			"TEXT" => GetMessage("TSZH_FLYSHEETS_DELETE"),
			"TITLE" => GetMessage("TSZH_FLYSHEETS_DELETE_DESCR"),
			"ACTION" => "if(confirm('" . GetMessage('TSZH_PAYMENTS_CONFIRM_DEL_MESSAGE') . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete"),
		);
	}

	$row->AddActions($arActions);
}

$lAdmin->AddFooter(
	array(
		array(
			"title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"),
			"value" => $dbResultList->SelectedRowsCount(),
		),
		array(
			"counter" => true,
			"title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"),
			"value" => "0",
		),
	)
);

$lAdmin->AddGroupActionTable(
	array(
		"delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"),
	)
);

if ($tszhPaymentModulePermissions == "W")
{
	$arDDMenu = array();
	$arDDMenu[] = array(
			"TEXT" => GetMessage("FLYSHEET_ADD_NEW"),
			"ACTION" => "window.open('https://vgkh.ru/flysheets/index.php?back_url_site=" . $_SERVER['SERVER_NAME'] . "/bitrix/admin/tszh_flysheets_edit.php','_blank');",
	);
	$rsTszh = CTszh::GetList(array(), array());
	while ($arTszh = $rsTszh->Fetch())
	{
		// SITE_CHARSET to charset vgkh.ru
		$org_name = urlencode($APPLICATION->ConvertCharset(htmlspecialcharsbx($arTszh["NAME"]), SITE_CHARSET, 'cp1251'));
		$contacts = urlencode(htmlspecialcharsbx($arTszh["PHONE_DISP"]));
		$org_site = urlencode(htmlspecialcharsbx("http://" . $_SERVER["SERVER_NAME"] . "/"));

		$arDDMenu[] = array(
			"TEXT" => "[" . htmlspecialcharsbx($arTszh["SITE_ID"]) . "] " . htmlspecialcharsbx($arTszh["NAME"]),
			"ACTION" => "window.open('https://vgkh.ru/flysheets/index.php?org_name=" . $org_name . "&contacts=" . $contacts . "&org_site=" . $org_site . "&back_url_site=" . $_SERVER['SERVER_NAME'] . "/bitrix/admin/tszh_flysheets_edit.php','_blank');",
		);
	}


	$aContext = array(
		array(
			"TEXT" => GetMessage("FLYSHEET_ADD_NEW"),
			"TITLE" => GetMessage("FLYSHEET_ADD_NEW_ALT"),
			"LINK" => "/bitrix/admin/tszh_flysheets_edit.php",
			"ICON" => "btn_new",
		),
		array(
			"HTML" => "<a href=\"javascript:void(0)\" hidefocus=\"true\" onclick=\"this.blur();BX.adminShowMenu(this, "
			          . CAdminPopup::PhpToJavaScript($arDDMenu)
			          . ", {active_class: 'adm-btn-generator-active'}); return false;\" class='adm-btn-generator adm-btn' title='" . GetMessage("FLYSHEET_ADD_NEW_GENERATOR_ALT") . "' target='_blank' >"
			          . GetMessage("FLYSHEET_ADD_NEW_GENERATOR")
			          . "</a>"
			          . "<div id='generator-tooltip' class='generator-tooltip-wrapper'>"
			          . "<div class='generator-triangle-topleft'></div>"
			          . "<div class='generator-tooltip'>" . GetMessage("FLYSHEET_ADD_NEW_GENERATOR_ALT")
			          . "</div>"
			          . "<div onclick=\"document.getElementById('generator-tooltip').style.display ='none';\" class='generator-tooltip-close'>" . GetMessage("FLYSHEET_ADD_NEW_GENERATOR_ALT_CLOSE"). "</div>"
			          . "</div>",
			"MENU" => $arDDMenu,
		),
	);
	$lAdmin->AddAdminContextMenu($aContext);
}

$lAdmin->CheckListMode();


/****************************************************************************/
/***********  MAIN PAGE  ****************************************************/
/****************************************************************************/
$APPLICATION->SetTitle(GetMessage("TSZH_PAYMENTS_SECTION_TITLE"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>

	<form name="find_form" method="GET" action="<? echo $APPLICATION->GetCurPage() ?>?">
		<?
		$arFindFields = array(
			GetMessage("TSZH_FLYSHEET_ID"),
		);

		$USER_FIELD_MANAGER->AddFindFields($UF_ENTITY, $arFindFields);
		$oFilter = new CAdminFilter(
			$sTableID . "_filter",
			$arFindFields
		);

		$oFilter->Begin();
		?>
		<tr>
			<td><? echo GetMessage("TSZH_FLYSHEET_ID") ?></td>
			<td><input type="text" name="filter_id" size="21" value="<? echo htmlspecialchars($filter_id) ?>"/></td>
		</tr>
		<?
		$USER_FIELD_MANAGER->AdminListShowFilter($UF_ENTITY);

		$oFilter->Buttons(
			array(
				"table_id" => $sTableID,
				"url" => $APPLICATION->GetCurPage(),
				"form" => "find_form",
			)
		);
		$oFilter->End();
		?>
		<? /*if (CModule::IncludeModule("citrus.tszhpayment")):*/ ?><!--
            <style>
                .adm-filter-wrap
                {
                    float: left;
                }
            </style>
            <div style="padding: 5px; width: 100%">
                <a href="tszh_flysheets.php"><img src="/images/banners/banner_flysheets.gif"></a>
            </div>
        --><? /*endif;*/ ?>
	</form>

<?
$lAdmin->DisplayList();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

?>