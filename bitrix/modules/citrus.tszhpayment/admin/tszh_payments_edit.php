<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

$tszhPaymentModulePermissions = $APPLICATION->GetGroupRight("citrus.tszhpayment");
if ($tszhPaymentModulePermissions == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

IncludeModuleLangFile(__FILE__);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszhpayment/include.php");
use Citrus\Tszhpayment\PaymentServicesTable;
$ID = IntVal($ID);
$LID = Trim($LID);

$bFatalError = false;

ClearVars();
ClearVars("str_");

$strRedirect = BX_ROOT."/admin/tszh_payments_edit.php?lang=".LANG;
$strRedirectList = BX_ROOT."/admin/tszh_payments.php?lang=".LANG;

$errorMessage = "";
$bVarsFromForm = false;
$message = null;

$ID = IntVal($ID);

$aTabs = array(
		array("DIV" => "edit1", "TAB" => GetMessage("TSZH_PAYMENT_EDIT"), "ICON" => "citrus.tszhpayment", "TITLE" => GetMessage("TSZH_PAYMENT_EDIT_TITLE"))
	);

$UF_ENTITY = "TSZH_PAYMENT";

$tabControl = new CAdminForm("paymentEdit", $aTabs);

/****************/
if(	$tszhPaymentModulePermissions >= "U"
	&& check_bitrix_sessid()
	&& empty($dontsave)
)
{
	if ($_REQUEST['action'] == "ps_update"
		&& $_SERVER["REQUEST_METHOD"] == "GET")
	{
		$arPayment = CTszhPayment::GetList(
			Array(),
			Array("ID" => $ID),
			false,
			Array("nTopCount" => 1),
			Array("*")
		)->Fetch();
		if (!$arPayment)
			$errorMessage .= GetMessage("TSZH_PAYMENT_NO_PAYMENT")."<br>";

		if (strlen($errorMessage) <= 0)
		{
			$psResultFile = "";

			$psActionPath = $_SERVER["DOCUMENT_ROOT"].$arPayment["PS_ACTION_FILE"];
			$psActionPath = str_replace("\\", "/", $psActionPath);
			while (substr($psActionPath, strlen($psActionPath) - 1, 1) == "/")
				$psActionPath = substr($psActionPath, 0, strlen($psActionPath) - 1);

			if (file_exists($psActionPath) && is_dir($psActionPath))
			{
				if (file_exists($psActionPath."/result.php") && is_file($psActionPath."/result.php"))
					$psResultFile = $psActionPath."/result.php";
			}
			elseif (strlen($arPayment["PS_RESULT_FILE"]) > 0)
			{
				if (file_exists($_SERVER["DOCUMENT_ROOT"].$arPayment["PS_RESULT_FILE"])
					&& is_file($_SERVER["DOCUMENT_ROOT"].$arPayment["PS_RESULT_FILE"]))
					$psResultFile = $_SERVER["DOCUMENT_ROOT"].$arPayment["PS_RESULT_FILE"];
			}

			if (strlen($psResultFile) <= 0)
				$errorMessage .= GetMessage("SOD_NO_PS_SCRIPT").". ";
		}
		
		if (strlen($errorMessage) <= 0)
		{
			$ORDER_ID = $ID;
			CTszhPaySystem::InitParamArrays($arPayment);
			if (!include($psResultFile))
			{
				$errorMessage .= GetMessage("ERROR_CONNECT_PAY_SYS").". ";
			}
		}

		if (strlen($errorMessage) <= 0)
		{
			$arPayment = CTszhPayment::GetList(
				Array(),
				Array("ID" => $ID),
				false,
				Array("nTopCount" => 1),
				Array("*")
			)->Fetch();
			if (!$arPayment)
				$errorMessage .= GetMessage("TSZH_PAYMENT_NO_PAYMENT")."<br>";
		}
		
		if (strlen($errorMessage) <= 0)
		{
			if ($arPayment["PS_STATUS"] == "Y" && $arPayment["PAYED"] == "N")
			{
				if ($arPayment["CURRENCY"] == $arPayment["PS_CURRENCY"]
					&& DoubleVal($arPayment["PRICE"]) == DoubleVal($arPayment["PS_SUM"]))
				{
					$arFields = Array(
						"PAYED" => "Y",
						"SUMM_PAYED" => $arPayment["PS_SUM"],
					);
			
					$bResult = CTszhPayment::Update($arPayment["ID"], $arFields);
					
					if (!$bResult)
					{
						if ($ex = $APPLICATION->GetException())
							$errorMessage .= $ex->GetString();
						else
							$errorMessage .= str_replace("#ID#", $ORDER_ID, GetMessage("SOD_CANT_PAY")).". ";
					}
				}
			}
		}

		if (strlen($errorMessage) <= 0)
			LocalRedirect("tszh_payments_edit.php?ID=".$ID."&result=ok_ps&lang=".LANG.GetFilterParams("filter_", false));
	}
}
elseif (!empty($dontsave))
{
	//CSaleOrder::UnLock($ID);
	LocalRedirect("tszh_payments.php?lang=".LANG.GetFilterParams("filter_", false));
}
/****************/

if ($REQUEST_METHOD=="POST" && strlen($Update)>0 && check_bitrix_sessid())
{
	if ($tszhPaymentModulePermissions < "W")
		$errorMessage .= GetMessage("TSZH_PAYMENT_EDIT_ERROR_ACCESS") . ".<br />";

	$ID = IntVal($ID);
	if ($ID < 0)
		$errorMessage .= GetMessage("TSZH_PAYMENT_EDIT_ERROR_NO_ID") . ".<br />";

	if (strlen($errorMessage) <= 0)
	{
		$arFields = Array(
			"PAYED" => $PAYED == "Y" ? "Y" : "N",
			"SUMM" => $SUMM,
			"SUMM_PAYED" => $SUMM_PAYED,
			"CURRENCY" => $CURRENCY,
			"IS_OVERHAUL" => $IS_OVERHAUL == "Y" ? "Y" : "N",
			"IS_PENALTY" => $IS_PENALTY == "Y" ? "Y" : "N",
            "INSURANCE_INCLUDED" => $INSURANCE_INCLUDED == 1 ? 1 : 0,
		);

		$USER_FIELD_MANAGER->EditFormAddFields($UF_ENTITY, $arFields);
		$bResult = CTszhPayment::Update($ID, $arFields);

		if (!$bResult) {
			if ($ex = $APPLICATION->GetException())
				$errorMessage .= $ex->GetString().".<br />";
			else
				$errorMessage .= GetMessage("TSZH_PAYMENT_EDIT_ERROR_UPDATE") . ".<br />";
		}
	}

	if (strlen($errorMessage) <= 0) {
		if (strlen($apply) <= 0)
			LocalRedirect($strRedirectList . GetFilterParams("filter_", false));
		else
			LocalRedirect($strRedirect."&ID=".$ID."&".$tabControl->ActiveTabParam());
	} else {
		$bVarsFromForm = true;
	}
}

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszhpayment/prolog.php");

$APPLICATION->SetTitle(($ID > 0) ? str_replace('#ID#', '�' . $ID, GetMessage("TSZH_PAYMENT_EDIT_PAGE_TITLE")) : str_replace('#ID#', '', GetMessage("TSZH_PAYMENT_EDIT_PAGE_TITLE")));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");


$rsPayment = CTszhPayment::GetList(
	Array(),
	Array("ID" => $ID),
	false,
	Array("nTopCount" => 1),
	Array("*")
);

if (!$arPayment = $rsPayment->ExtractFields("str_"))
{
	$errorMessage .= GetMessage("TSZH_PAYMENT_EDIT_NOT_FOUND") . ".<br />";
	$bFatalError = true;
}

//echo '<pre>' . htmlspecialchars(print_r($arPayment, true)) . '</pre>';

if ($bVarsFromForm) {
	$DB->InitTableVarsForEdit("b_tszh_payment", "", "str_");
}


if(strlen($errorMessage)>0)
	echo CAdminMessage::ShowMessage(Array("DETAILS"=>$errorMessage, "TYPE"=>"ERROR", "MESSAGE"=>GetMessage("TSZH_PAYMENT_EDIT_ERRORS") . ':', "HTML"=>true));
if (strlen($result) > 0)
{
	$okMessage = "";

	if ($result == "ok_ps")
		$okMessage = GetMessage("SOD_OK_PS");

	CAdminMessage::ShowNote($okMessage);
}



if ($bFatalError) {
	require_once ($DOCUMENT_ROOT.BX_ROOT."/modules/main/include/epilog_admin.php");
	return;
}

/**
 *   CAdminForm()
**/

$aMenu = array(
		array(
				"TEXT" => GetMessage("TSZH_PAYMENT_LIST"),
				"LINK" => "/bitrix/admin/tszh_payments.php?lang=".LANG.GetFilterParams("filter_"),
				"ICON"	=> "btn_list",
				"TITLE" => GetMessage("TSZH_PAYMENT_LIST_TITLE"),
			)
	);

if ($ID > 0 && $tszhPaymentModulePermissions >= "W")
{
	$aMenu[] = array("SEPARATOR" => "Y");

	$aMenu[] = array(
			"TEXT" => GetMessage("TSZH_PAYMENT_DELETE"),
			"LINK" => "javascript:if(confirm('" . GetMessage("TSZH_PAYMENT_DELETE_CONFIRM") . "')) window.location='/bitrix/admin/tszh_payments.php?ID=".$ID."&action=delete&lang=".LANG."&".bitrix_sessid_get()."#tb';",
			"WARNING" => "Y",
			"ICON"	=> "btn_delete"
		);
}
if(!empty($aMenu))
	$aMenu[] = array("SEPARATOR"=>"Y");
$link = DeleteParam(array("mode"));
$link = $GLOBALS["APPLICATION"]->GetCurPage()."?mode=settings".($link <> ""? "&".$link:"");
$aMenu[] = array(
	"TEXT"=> GetMessage("TSZH_PAYMENT_EDIT_SETTING"),
	"TITLE"=>GetMessage("TSZH_PAYMENT_EDIT_SETTING_TITLE"),
	"LINK"=>"javascript:".$tabControl->GetName().".ShowSettings('".htmlspecialchars(CUtil::addslashes($link))."')",
	"ICON"=>"btn_settings",
);

$context = new CAdminContextMenu($aMenu);
$context->Show();

if(method_exists($USER_FIELD_MANAGER, 'showscript')) {
	$tabControl->BeginPrologContent();
	echo $USER_FIELD_MANAGER->ShowScript();
	$tabControl->EndPrologContent();
}

// ============ FORM PROLOG ==================
$tabControl->BeginEpilogContent();
echo GetFilterHiddens("filter_");
?>
<input type="hidden" name="Update" value="Y" />
<input type="hidden" name="lang" value="<?echo LANG ?>" />
<input type="hidden" name="ID" value="<?echo $ID ?>" />
<?
echo bitrix_sessid_post();
$tabControl->EndEpilogContent();
// ===========================================

$tabControl->Begin(array(
	"FORM_ACTION" => $APPLICATION->GetCurPage(),
	"FORM_ATTRIBUTES" => "method=\"POST\" enctype=\"multipart/form-data\"",
));
$tabControl->BeginNextFormTab();

	if ($str_ID > 0) {
		$tabControl->AddViewField("ID", "ID:", $str_ID);
	}

	if (isset($arPayment['TSZH_ID']) && $arPayment["TSZH_ID"] > 0)
	{
		$arTszh = CTszh::GetByID($arPayment['TSZH_ID']);
		$html = "[<a href=\"/bitrix/admin/tszh_edit.php?ID={$arPayment["TSZH_ID"]}&lang=" . LANG . "\">{$arPayment["TSZH_ID"]}</a>]&nbsp;" . $arTszh["NAME"];
		$tabControl->AddViewField("TSZH_ID", GetMessage("TSZH_PAYMENT_EDIT_TSZH_ID") . ':', $html);
	}

  	if (isset($arPayment["ACCOUNT_ID"]) && intval($arPayment["ACCOUNT_ID"]) > 0)
	{
		$arAccount = CTszhAccount::GetByID($arPayment['ACCOUNT_ID']);
		$htmlAccountID = "[<a href=\"/bitrix/admin/tszh_account_edit.php?ID={$arPayment["ACCOUNT_ID"]}&lang=" . LANG . "\">{$arPayment["ACCOUNT_ID"]}</a>]&nbsp;" . CTszhAccount::GetFullName($arAccount);
		$tabControl->AddViewField("ACCOUNT_ID", GetMessage("TSZH_PAYMENT_EDIT_ACCOUNT_ID") . ':', $htmlAccountID);
	}
	elseif (isset($arPayment["C_ACCOUNT"]))
	{
		$tabControl->AddViewField("ACCOUNT_ID", GetMessage("TSZH_PAYMENT_EDIT_ACCOUNT_ID") . ':', $arPayment["C_ACCOUNT"]);
	}

	if (isset($arPayment['USER_ID']))
	{
		$sUserFullName = CTszhAccount::GetFullName($arPayment["USER_ID"]);
		$html = "[<a href=\"/bitrix/admin/user_edit.php?ID={$arPayment["USER_ID"]}&lang=" . LANG . "\">{$arPayment["USER_ID"]}</a>]&nbsp;" . $sUserFullName;
	}
	else
		$html = GetMessage("TSZH_PAYMENT_EDIT_USER_UNAUTH");
	$tabControl->AddViewField("USER_ID", GetMessage("TSZH_PAYMENT_EDIT_USER_ID") . ':', $html);

	$htmlPaySytem = "[<a href=\"/bitrix/admin/tszh_pay_systems_edit.php?ID={$arPayment["PAY_SYSTEM_ID"]}&lang=" . LANG . "\">{$arPayment["PAY_SYSTEM_ID"]}</a>]&nbsp;" . $arPayment["PS_NAME"];
	$tabControl->AddViewField("PAY_SYSTEM_ID", GetMessage("TSZH_PAYMENT_EDIT_PAY_SYSTEM") . ':', $htmlPaySytem);

	if (isset($arPayment["C_ADDRESS"]))
		$tabControl->AddViewField("C_ADDRESS", GetMessage("TSZH_PAYMENT_EDIT_C_ADDRESS") . ':', $arPayment["C_ADDRESS"]);
	if (isset($arPayment["C_COMMENTS"]))
		$tabControl->AddViewField("C_COMMENTS", GetMessage("TSZH_PAYMENT_EDIT_C_COMMENTS") . ':', nl2br($arPayment["C_COMMENTS"]));

	$tabControl->AddCheckBoxField("PAYED", GetMessage("TSZH_PAYMENT_EDIT_PAYED") . ':', false, 'Y', $str_PAYED == 'Y');

	if ($arPayment['PAYED'] == 'Y') {
		$tabControl->AddViewField("DATE_PAYED", GetMessage("TSZH_PAYMENT_EDIT_DATE_PAYED") . ':', $arPayment["DATE_PAYED"]);

		if ($arPayment["EMP_PAYED_ID"] > 0) {
			$strUserFullName = CTszhAccount::GetFullName($arPayment["EMP_PAYED_ID"]);
			$tmpHTML = "[<a href=\"/bitrix/admin/user_edit.php?lang=" . LANG . "&amp;ID={$arPayment["EMP_PAYED_ID"]}\">{$arPayment["EMP_PAYED_ID"]}</a>]&nbsp;{$strUserFullName}";
		} else {
			$tmpHTML = GetMessage("CITRUS_TSZHPAYMENT_FROM_PS");
		}
		$tabControl->AddViewField("EMP_PAYED_ID", GetMessage("TSZH_PAYMENT_EDIT_EMP_PAYED_ID") . ':', $tmpHTML);
	}
$arServiceList = PaymentServicesTable::getList(

	array(
		'filter'=>array(
			'PAYMENT_ID'=>$ID
		)

	)
)->fetchAll();

$tabControl->BeginCustomField("SERVICES_TITLE", GetMessage("TSZH_PAYMENT_SERVICES_SUMM"));
?>
	<tr>
		<td colspan="2" style="text-align: center; font-weight: bold; padding-right: 20px;"><?echo GetMessage("TSZH_PAYMENT_SERVICES_SUMM")?></td>
	</tr>
<?foreach($arServiceList as $arService){?>
	<tr>
		<td><?=$arService['SERVICE_NAME']?>:</td>
		<td><?=$arService['SUMM_PAYED']?></td>
	</tr>
<?}
$tabControl->EndCustomField("SERVICES_TITLE", '');
	$tabControl->AddEditField("SUMM", GetMessage("TSZH_PAYMENT_EDIT_SUMM") . ':', true, array("size"=>30, "maxlength"=>50), $str_SUMM);
	$tabControl->AddEditField("SUMM_PAYED", GetMessage("TSZH_PAYMENT_EDIT_SUMM_PAYED") . ':', true, array("size"=>30, "maxlength"=>50), $str_SUMM_PAYED);

	if (strlen($arPayment["PS_STATUS"]) > 0)
	{
		$tabControl->BeginCustomField("ORDER_PS_STATUS", GetMessage("P_ORDER_PS_STATUS"));
		?>
		<tr>
			<td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
			<td width="60%">
				<?
				echo (($arPayment["PS_STATUS"]=="Y") ? "OK" : "Error");

				if ($arPaySys["PSA_HAVE_RESULT"] == "Y" || strlen($arPaySys["PSA_RESULT_FILE"]) > 0)
				{
					?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="tszh_payments_edit.php?ID=<?= $ID ?>&����action=ps_update&amp;lang=<?= LANG ?><?echo GetFilterParams("filter_")?>&amp;<?= bitrix_sessid_get() ?>"><?echo GetMessage("P_ORDER_PS_STATUS_UPDATE") ?> &gt;&gt;</a>
					<?
				}
				?>
			</td>
		</tr>
		<?
		$tabControl->EndCustomField("ORDER_PS_STATUS", '');

		$tabControl->BeginCustomField("ORDER_PS_STATUS", GetMessage("P_ORDER_PS_STATUS"));
		?>
		<tr>
			<td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
			<td width="60%"><?echo $arPayment["PS_STATUS_CODE"] ;?></td>
		</tr>
		<?
		$tabControl->EndCustomField("ORDER_PS_STATUS", '');

		$tabControl->BeginCustomField("ORDER_PS_STATUS_DESCRIPTION", GetMessage("P_ORDER_PS_STATUS_DESCRIPTION"));
		?>
		<tr>
			<td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
			<td width="60%"><?echo $arPayment["PS_STATUS_DESCRIPTION"] ;?></td>
		</tr>
		<?
		$tabControl->EndCustomField("ORDER_PS_STATUS_DESCRIPTION", '');

		$tabControl->BeginCustomField("ORDER_PS_STATUS_MESSAGE", GetMessage("P_ORDER_PS_STATUS_MESSAGE"));
		?>
		<tr>
			<td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
			<td width="60%"><?echo $arPayment["PS_STATUS_MESSAGE"] ;?></td>
		</tr>
		<?
		$tabControl->EndCustomField("ORDER_PS_STATUS_MESSAGE", '');

		$tabControl->BeginCustomField("ORDER_PS_SUM", GetMessage("P_ORDER_PS_SUM"));
		?>
		<tr>
			<td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
			<td width="60%"><?echo $arPayment["PS_SUM"] ;?></td>
		</tr>
		<?
		$tabControl->EndCustomField("ORDER_PS_SUM", '');

		$tabControl->BeginCustomField("ORDER_PS_CURRENCY", GetMessage("P_ORDER_PS_CURRENCY"));
		?>
		<tr>
			<td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
			<td width="60%"><?echo $arPayment["PS_CURRENCY"] ;?></td>
		</tr>
		<?
		$tabControl->EndCustomField("ORDER_PS_CURRENCY", '');

		$tabControl->BeginCustomField("ORDER_PS_RESPONSE_DATE", GetMessage("P_ORDER_PS_RESPONSE_DATE"));
		?>
		<tr>
			<td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
			<td width="60%"><?echo $arPayment["PS_RESPONSE_DATE"]; ?></td>
		</tr>
		<?
		$tabControl->EndCustomField("ORDER_PS_RESPONSE_DATE", '');
	}
	elseif ($arPayment["PS_HAVE_RESULT"] == "Y" || strlen($arPayment["PS_RESULT_FILE"]) > 0)
	{
		$tabControl->BeginCustomField("ORDER_PS_STATUS", GetMessage("P_P_ORDER_PS_STATUS"));
		?>
		<tr>
			<td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
			<td width="60%"><a href="tszh_payments_edit.php?ID=<?= $ID ?>&amp;action=ps_update&amp;lang=<?= LANG ?><?= GetFilterParams("filter_") ?>&<?= bitrix_sessid_get() ?>"><?= GetMessage("P_ORDER_PS_STATUS_UPDATE") ?> &gt;&gt;</a></td>
		</tr>
		<?
		$tabControl->EndCustomField("ORDER_PS_STATUS", '');
	}

	if (CModule::IncludeModule("currency")) {
		$tabControl->BeginCustomField("CURRENCY", GetMessage("TSZH_PAYMENT_EDIT_CURRENCY") . ':', true);
?>
	<tr>
		<td width="40%"><span class="required">*</span><?=GetMessage("TSZH_PAYMENT_EDIT_CURRENCY")?>:</td>
		<td width="60%"><?=CCurrency::SelectBox("CURRENCY", $str_CURRENCY, "", true, "", "")?>
		</td>
	</tr>
<?
		$tabControl->EndCustomField("CURRENCY", '<input type="hidden" name="CURRENCY" value="'.$str_CURRENCY.'">');
	} else {
		$tabControl->AddEditField("CURRENCY", GetMessage("TSZH_PAYMENT_EDIT_CURRENCY") . ':', true, array("size"=>30, "maxlength"=>50), $str_CURRENCY);
	}

	$tabControl->AddCheckBoxField("IS_OVERHAUL", GetMessage("TSZH_PAYMENT_EDIT_IS_OVERHAUL") . ':', false, 'Y', $str_IS_OVERHAUL == 'Y');
	$tabControl->AddCheckBoxField("IS_PENALTY", GetMessage("TSZH_PAYMENT_EDIT_IS_PENALTY") . ':', false, 'Y', $str_IS_PENALTY == 'Y');
    $tabControl->AddCheckBoxField("INSURANCE_INCLUDED", GetMessage("TSZH_PAYMENT_EDIT_IS_INSURANCE_INCLUDED") . ':', false, 1, $str_INSURANCE_INCLUDED == 1);

	$tabControl->AddViewField("PSYS_OPERATION_ID", GetMessage("TSZH_PAYMENT_EDIT_PSYS_OPERATION_ID") . ':', $str_PSYS_OPERATION_ID);
	$tabControl->AddViewField("PSYS_STATUS", GetMessage("TSZH_PAYMENT_EDIT_PSYS_STATUS") . ':', $str_PSYS_STATUS);
	$tabControl->AddViewField("PSYS_STATUS_DESCR", GetMessage("TSZH_PAYMENT_EDIT_PSYS_STATUS_DESCR") . ':', $str_PSYS_STATUS_DESCR);
	$tabControl->AddViewField("PSYS_STATUS_RESPONSE", GetMessage("TSZH_PAYMENT_EDIT_PSYS_STATUS_RESPONSE") . ':', $str_PSYS_STATUS_RESPONSE);
	$tabControl->AddViewField("PSYS_STATUS_DATE", GetMessage("TSZH_PAYMENT_EDIT_PSYS_STATUS_DATE") . ':', $str_PSYS_STATUS_DATE);
	$tabControl->AddViewField("PSYS_STATUS_RESPONSE", GetMessage("TSZH_PAYMENT_EDIT_PSYS_STATUS_RESPONSE") . ':', $str_PSYS_STATUS_RESPONSE);
	$tabControl->AddViewField("PSYS_FEE", GetMessage("TSZH_PAYMENT_EDIT_PSYS_FEE") . ':', $str_PSYS_FEE);

	// ������� ������ � ����������������� ������, ���� �� ��� ���� ���� ��� � ������������ ���� ����� �� ���������� �����
	if(
		(count($USER_FIELD_MANAGER->GetUserFields($UF_ENTITY)) > 0) ||
		($USER_FIELD_MANAGER->GetRights($UF_ENTITY) >= "W")
	)
	{
		$tabControl->AddSection('USER_FIELDS', GetMessage("TSZH_PAYMENT_EDIT_USER_FIELDS"));
		$tabControl->ShowUserFields($UF_ENTITY, $str_ID, $bVarsFromForm);
	}

	$tabControl->Buttons(Array(
		"disabled" => ($tszhPaymentModulePermissions < "W"),
		"back_url" => $strRedirectList,
	));


	$tabControl->Show();

	$tabControl->ShowWarnings($tabControl->GetName(), $message);

if(!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):?>
<?echo BeginNote();?>
<span class="required">*</span> <?echo GetMessage("REQUIRED_FIELDS")?>
<?echo EndNote(); ?>
<?endif;?>
<?require_once ($DOCUMENT_ROOT.BX_ROOT."/modules/main/include/epilog_admin.php");?>