<?
IncludeModuleLangFile(__FILE__);

$module_rights = $APPLICATION->GetGroupRight("citrus.tszhpayment");

if($module_rights != "D")
{

	$aMenu = array(
		"parent_menu" => "global_menu_services",
		"section" => "citrus.tszhpayment",
		"sort" => 11,
		"text" => GetMessage("TSZH_PAYMENT_MENU_SECT"),
		"title" => GetMessage("TSZH_PAYMENT_MENU_SECT_TITLE"),
		"icon" => "citrus_tszhpayment_menu_icon",
		"page_icon" => "citrus_tszhpayment_page_icon",
		"items_id" => "menu_citrus_tszhpayment",
		"items" => Array(
			Array(
				"text" => GetMessage("TSZH_PAYMENT_PAY_SYSTEMS"),
				"url" => "tszh_pay_systems.php?lang=" . LANG,
				"more_url" => array('tszh_pay_systems_edit.php'),
				"title" => GetMessage("TSZH_PAYMENT_PAY_SYSTEMS"),
			),
			Array(
				"text" => GetMessage("TSZH_PAYMENT_PAYMENTS"),
				"url" => "tszh_payments.php?lang=" . LANG,
				"more_url" => array('tszh_payments_edit.php'),
				"title" => GetMessage("TSZH_PAYMENT_PAYMENTS"),
			),
            Array(
                "text" => GetMessage("TSZH_PAYMENT_FLYSHEETS"),
                "url" => "tszh_flysheets_list.php?lang=" . LANG,
                "more_url" => array('tszh_flysheets_edit.php'),
                "title" => GetMessage("TSZH_PAYMENT_FLYSHEETS"),
            ),
		)
	);

	return $aMenu;
}

return false;
?>