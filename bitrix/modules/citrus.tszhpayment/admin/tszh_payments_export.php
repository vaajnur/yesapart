<?
// ��������� ��� ����������� �����:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������

if (!CModule::IncludeModule("citrus.tszh"))
{
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
	ShowError(GetMessage("CITRUS_TSZHPAYMENTS_CITRUS_TSZH_MODULE_NOT_FOUND"));
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
	return;
}

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszhpayment/include.php"); // ������������� ������
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszhpayment/prolog.php"); // ������ ������

// ��������� �������� ����
IncludeModuleLangFile(__FILE__);

set_time_limit(0);

$step = IntVal($_REQUEST['step']);
if ($step < 1 || $step > 2 || !check_bitrix_sessid()) {
	$step = 1;
}

// �������� ����� ������� �������� ������������ �� ������ 1�:���� ����������� �������� ���, ��� � ���
$moduleRights = $APPLICATION->GetGroupRight("citrus.tszh");
if ($moduleRights < "U")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$strError = false;

// �������� ����� ��������
if ($_REQUEST['delete_btn'] && check_bitrix_sessid())
{
	if (strlen($_SESSION['citrus.tszhpayments.export']['FILENAME']) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'] . $_SESSION['citrus.tszhpayments.export']['FILENAME']))
	{
		unlink($_SERVER['DOCUMENT_ROOT'] . $_SESSION['citrus.tszhpayments.export']['FILENAME']);
		unset($_SESSION['citrus.tszhpayments.export']);
	}
}

// ��������� � ���������� ������
if ($step > 1 && check_bitrix_sessid()) {

	if ($step == 2) {

		$lastID = isset($_REQUEST['lastID']) && IntVal($_REQUEST['lastID']) > 0 ? IntVal($_REQUEST['lastID']) : 0;
		if ($lastID <= 0)
		{
			$tszhExportFormat = 'xml';
			$_SESSION['citrus.tszhpayments.export'] = Array(
				'filename' => "/upload/payments_export_" . date('Y-m-d_Hms') . ".{$tszhExportFormat}",
				'stepTime' => isset($_REQUEST['step_time']) && IntVal($_REQUEST['step_time']) > 0 ? IntVal($_REQUEST['step_time']) : 25,
				'tszh' => CTszh::GetByID($_REQUEST["tszhID"]),
				'format' => $tszhExportFormat,
			);

			$dateFrom = trim($_REQUEST['tszhExportDateFrom']);
			$dateTo = trim($_REQUEST['tszhExportDateTo']);
			if (strlen($dateFrom) > 0 || strlen($dateTo) > 0)
			{
				CheckFilterDates($dateFrom, $dateTo, $date1_wrong, $date2_wrong, $date2_less);
				if ($date1_wrong=="Y") $strError .= GetMessage("TSZH_EXPORT_ERROR_DATE_1");
				if ($date2_wrong=="Y") $strError .= GetMessage("TSZH_EXPORT_ERROR_DATE_2");
				if ($date2_less=="Y") $strError .= GetMessage("TSZH_EXPORT_ERROR_DATE_3");
				
				if (strlen($strError) <= 0)
				{
					if (strlen($dateFrom) > 0)
						$_SESSION['citrus.tszhpayments.export']['DATE_FROM'] = $dateFrom;
					if (strlen($dateTo) > 0)
						$_SESSION['citrus.tszhpayments.export']['DATE_TO'] = $dateTo;
				}
			}
		}

		$NS = &$_SESSION['citrus.tszhpayments.export'];

		if (strlen($strError) <= 0)
		{
			$obExport = new CTszhPaymentsExport();
			$arFilter = Array(
				"TSZH_ID" => $tszhID,
				"PAYED" => "Y",
			);
			if ($_SESSION['citrus.tszhpayments.export']['DATE_FROM'])
				$arFilter[">=DATE_PAYED"] = $_SESSION['citrus.tszhpayments.export']['DATE_FROM'];
			if ($_SESSION['citrus.tszhpayments.export']['DATE_TO'])
			{
				$dateTo = $_SESSION['citrus.tszhpayments.export']['DATE_TO'];
				$arDateTo = ParseDateTime($dateTo);
				if (!isset($arDateTo["HH"]) && !isset($arDateTo["H"]) && !isset($arDateTo["GG"]) && !isset($arDateTo["G"]))
				{
					$dateTo = ConvertTimeStamp(
						AddToTimeStamp(array("HH" => 23, "MI" => 59, "SS" => 59), MakeTimeStamp($dateTo)), 
						"FULL"
					);
				}
				$arFilter["<=DATE_PAYED"] = $dateTo;
			}
				
			try
			{
				$lastID = $obExport->ProcessExport($arFilter, $lastID, $NS);
			}
			catch (Exception $e)
			{
				$strError = $ex->getMessage();
			}
			if ($NS["progress"]["remaining"])
			{
				$href = $APPLICATION->GetCurPageParam("lastID=$lastID&step=$step&" . bitrix_sessid_get(), Array('tszhID', "lastID", 'step', 'step_time', 'sessid'));
				?>
				<!doctype html>
				<html>
				<meta http-equiv="REFRESH" content="0;url=<?=$href?>">
				</html>
				<body>
					<?
					echo GetMessage("TSZH_EXPORT_PROGRESS", array('#CNT#' => $NS['progress']['done'], '#TOTAL#' => $NS['progress']['total']));
					?>
				</body>
				<?
				return;
			}
		}

		if (strlen($strError) > 0)
			$step = 1;
	}
}
?>
<?

$APPLICATION->SetTitle(GetMessage("TE_PAGE_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php"); // ������ ����� ������

echo $demoNotice;

//==========================================================================================
//==========================================================================================


CAdminMessage::ShowMessage($strError);

?>
<form method="POST" action="<?echo $sDocPath?>?lang=<?echo LANG ?>" enctype="multipart/form-data" name="dataload" id="dataload">
<?=bitrix_sessid_post()?>
<?

// ����� ��������
//==========================================================================================
$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("TE_TAB1"), "ICON" => "iblock", "TITLE" => GetMessage("TE_TAB1_TITLE")),
	array("DIV" => "edit2", "TAB" => GetMessage("TE_TAB2"), "ICON" => "iblock", "TITLE" => GetMessage("TE_TAB2_TITLE")),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs, false);
$tabControl->Begin();
?>

<?
$tabControl->BeginNextTab();

if ($step == 1) {?>
	<tr>
		<td><?echo GetMessage("TSZH_EXPORT_TSZH");?>:</td>
		<td>
			<select name="tszhID" id="tszhID">
				<?
				$dbTszh = CTszh::GetList(Array("NAME" => "ASC"));
				while ($arTszh = $dbTszh->Fetch())
				{
					?><option value="<?= $arTszh["ID"] ?>"<?if ($arTszh["ID"] == CUserOptions::GetOption('citrus.tszh.import', 'TszhID', false)) echo " selected";?>>[<?= htmlspecialcharsex($arTszh["ID"]) ?>]&nbsp;<?= htmlspecialcharsex($arTszh["NAME"]) ?></option><?
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td><?echo GetMessage("TSZH_EXPORT_STEP_TIME");?>:</td>
		<td>
			<input name="step_time" value="25" type="text" />
		</td>
	</tr>
	<tr><?
			$dateFrom = trim($_REQUEST['tszhExportDateFrom']);
			$dateTo = trim($_REQUEST['tszhExportDateTo']);
		?>
		<td><label for="tszhExportDateFrom"> <?=GetMessage("TSZH_EXPORT_PERIOD")?></label></td>
		<td>
			<?=CalendarPeriod("tszhExportDateFrom", $dateFrom, "tszhExportDateTo", $dateTo, "dataload", "N", 'id="tszhExportPeriod"')?>
		</td>
	</tr>
<?
}

$tabControl->EndTab();
?>


<?
$tabControl->BeginNextTab();
$filename = htmlspecialcharsbx($_SESSION['citrus.tszhpayments.export']['filename']);
if ($step == 2) {?>
	<tr>
		<td>
			<?=GetMessage("TE_EXPORT_DONE")?>.<br />
			<a href="<?=$filename?>" target="_blank" download="<?=basename($filename)?>"><?=GetMessage("TE_DOWNLOAD_XML")?></a>
		</td>
	</tr>
<?
}

$tabControl->EndTab();
?>

<?
$tabControl->Buttons();
?>
<input type="hidden" name="step" value="<?=$step+1?>" />
<?
if ($step < 2)
{
	?>
	<input type="submit" name="submit_btn" value="<?=GetMessage("TE_NEXT_BTN")?>" class="adm-btn-save" />
	<?
}
else
{
	?>
	<input type="submit" name="delete_btn" value="<?=GetMessage("TE_DELETE_FILE_BTN")?>" />
	<?
}
?>

<?
$tabControl->End();
?>
</form>

<script language="JavaScript">
<!--
<?if ($step < 2):?>
tabControl.SelectTab("edit1");
tabControl.DisableTab("edit2");
<?elseif ($step >= 2):?>
tabControl.SelectTab("edit2");
tabControl.DisableTab("edit1");
<?endif;?>
//-->
</script>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>