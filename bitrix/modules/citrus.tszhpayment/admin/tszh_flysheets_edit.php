<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/include.php");

use Citrus\Tszh\HouseTable;
use Citrus\Tszhpayment\FlysheetsTable;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Loader,
	Bitrix\Main,
	Bitrix\Iblock,
	Bitrix\Catalog;

Loader::includeModule('iblock');
CModule::IncludeModule("citrus.tszhpayment");

$modulePermissions = $APPLICATION->GetGroupRight("citrus.tszh");
if ($modulePermissions <= "D")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
if (isset($_GET["flysheet_src"])) {
    $file_array = CFile::MakeFileArray($_GET["flysheet_src"]);
    echo $file_Array;
    $file_id = CFile::SaveFile($file_array,"flysheets/");
    $file_path = CFile::GetPath($file_id);
}

IncludeModuleLangFile(__FILE__);

$bFatalError = false;

ClearVars();
ClearVars("str_");

$strRedirect = BX_ROOT . "/admin/tszh_flysheets_edit.php?lang=" . LANG . GetFilterParams("find_", false);
$strRedirectList = BX_ROOT . "/admin/tszh_flysheets_list.php?lang=" . LANG . GetFilterParams("find_", false);

$errorMessage = "";
$bVarsFromForm = false;
$message = null;

$ID = IntVal($_REQUEST['ID']);
$EntityFields = FlysheetsTable::getFieldNames();

$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("TSZH_FLYSHEETS_EDIT"), "ICON" => "citrus.tszh", "TITLE" => GetMessage("TSZH_FLYSHEETS_TITLE"))
);

// ���� ���������� ������ ����������� ���������, �� ������� �������������� �������

$UF_ENTITY = "TSZH_FLYSHEETS";

$tabControl = new CAdminForm(basename(__FILE__, '.php'), $aTabs);

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['Update']) && check_bitrix_sessid())
{
	if ($modulePermissions < "W")
	{
		$errorMessage .= GetMessage("ACCESS_DENIED") . ".<br />";
	}

	$ID = IntVal($ID);
	if ($ID < 0)
	{
		$errorMessage .= GetMessage("TSZH_GROUP_SAVE_ERROR") . ' ' . GetMessage("TSZH_GROUP_SAVE_ERROR_ITEM_NOT_FOUND") . "<br />";
	}

	if (strlen($errorMessage) <= 0)
	{
		$fieldValues = $ufValues = array();
		array_map(function ($fieldName) use (&$fieldValues)
		{
			if (isset($_POST[$fieldName]))
			{
				$fieldValues[$fieldName] = $_POST[$fieldName];
			}
		}, $EntityFields);

		$USER_FIELD_MANAGER->EditFormAddFields($UF_ENTITY, $ufValues);
		if (isset($_FILES))
        {
            if($_FILES["FILE_ID"]["error"] != 4)
            {
                $file_array = CFile::MakeFileArray($_FILES["FILE_ID"]["tmp_name"]);
                $file_Array["name"] = $_FILES["FILE_ID"]["name"];
                $file_id = CFile::SaveFile($file_array,"flysheets/");
                $file_path = CFile::GetPath($file_id);
                $fieldValues["FILE_ID"] = $file_id;
            }
        }

		if ($ID > 0)
		{
			$result = FlysheetsTable::update($ID, $fieldValues);
		}
		else
		{
			$result = FlysheetsTable::add($fieldValues);
			if ($result->isSuccess())
			{
				$ID = $result->getId();
			}
		}

		if (!$result->isSuccess())
		{
			$errorMessage .= GetMessage("TSZH_GROUP_SAVE_ERROR") . '<br>' . implode('<br>', $result->getErrorMessages());
		}
		else
		{
			$USER_FIELD_MANAGER->Update($UF_ENTITY, $ID, $ufValues);
		}
	}

	if (strlen($errorMessage) <= 0)
	{
		if (isset($_REQUEST["apply"]) && strlen($_REQUEST["apply"]))
		{
			LocalRedirect($strRedirect . "&ID=" . $ID . "&" . $tabControl->ActiveTabParam());
		}
		else
		{
			LocalRedirect($strRedirectList);
		}
	}
	else
	{
		$bVarsFromForm = true;
	}
}

$fieldValues = array();
if ($ID > 0)
{

		$arSelect = array("*");
    	$db = FlysheetsTable::getList(
		array(
			"filter" => array("ID" => $ID),
			"limit" => 1,
			"select" => $arSelect,
		)
	);

	if (!$fieldValues = $db->fetch())
	{
		$errorMessage .= GetMessage("TSZH_EDIT_ITEM_NOT_FOUND") . ".<br />";
		$bFatalError = true;
	}
}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/prolog.php");

$APPLICATION->SetTitle(($ID > 0) ? str_replace(array('#STREET#','#HOUSE#'),array($fieldValues["STREET"], $fieldValues["HOUSE"]), GetMessage("TSZH_EDIT_PAGE_TITLE")) : GetMessage("TSZH_ADD_PAGE_TITLE"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

if ($bVarsFromForm)
{
	array_map(function ($fieldName) use ($fieldValues)
	{
		if (isset($_POST[$fieldName]))
		{
			$fieldValues[$fieldName] = $_POST[$fieldName];
		}
	}, $entityFields);
}


if (strlen($errorMessage) > 0)
{
	CAdminMessage::ShowMessage(Array(
		"DETAILS" => $errorMessage,
		"TYPE" => "ERROR",
		"MESSAGE" => GetMessage("TSZH_EDIT_ERRORS") . ':',
		"HTML" => true
	));
}

if ($bFatalError)
{
	require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_admin.php");

	return;
}

/**
 *   CAdminForm()
 **/

$aMenu = array(
	array(
		"TEXT" => GetMessage("TSZH_ITEMS_LIST"),
		"LINK" => $strRedirectList,
		"ICON" => "btn_list",
		"TITLE" => GetMessage("TSZH_ITEMS_LIST_TITLE"),
	)
);

if ($ID > 0 && $modulePermissions >= "W")
{
	$aMenu[] = array("SEPARATOR" => "Y");

	$aMenu[] = array(
		"TEXT" => GetMessage("TSZH_GROUP_DELETE_TITLE"),
		"LINK" => "javascript:if(confirm('" . GetMessage("TSZH_GROUP_DELETE_CONFIRM") . "')) window.location='{$strRedirectList}&ID=" . $ID . "&edit_form_del=1&" . bitrix_sessid_get() . "#tb';",
		"WARNING" => "Y",
		"ICON" => "btn_delete",
	);
}
if (!empty($aMenu))
{
	$aMenu[] = array("SEPARATOR" => "Y");
}

$link = DeleteParam(array("mode"));
$link = $APPLICATION->GetCurPage() . "?mode=settings" . ($link <> "" ? "&" . $link : "");
$aMenu[] = array(
	"TEXT" => GetMessage("TSZH_EDIT_SETTING"),
	"TITLE" => GetMessage("TSZH_EDIT_SETTING_TITLE"),
	"LINK" => "javascript:" . $tabControl->GetName() . ".ShowSettings('" . htmlspecialcharsbx(CUtil::addslashes($link)) . "')",
	"ICON" => "btn_settings",
);

$context = new CAdminContextMenu($aMenu);
$context->Show();

$tabControl->Begin(array(
	"FORM_ACTION" => $APPLICATION->GetCurPage(),
	"FORM_ATTRIBUTES" => "method=\"POST\" enctype=\"multipart/form-data\"",
));
$tabControl->BeginNextFormTab();

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = array();

$fieldsMap = FlysheetsTable::getEditableFields();

foreach ($fieldsMap as $fieldName => $fieldSettings)
{
	$fieldTitle = $fieldSettings['title'] . ':';
	switch ($fieldName)
	{
		case 'ID':
			$tabControl->AddViewField($fieldName, $fieldTitle, $fieldValues[$fieldName]);
			break;
		case 'FILE_ID':
			$tabControl->BeginCustomField("FLYSHEETS_IMG", GetMessage("TSZH_FLYSHEETS_IMG_SRC"));
			?>
			<tr id="tr_FILE_ID_UPLOADED">
				<td width="40%"><?=GetMessage("TSZH_FLYSHEETS_IMG_SRC")?></td>
				<td align="left">
					<?
                    \Bitrix\Main\Loader::includeModule("fileman");
					echo CFileInput::Show($fieldName, (isset($file_id) ? $file_id : $fieldValues[$fieldName]), array(
						"IMAGE" => "Y",
						"PATH" => "Y",
						"FILE_SIZE" => "Y",
						"DIMENSIONS" => "Y",
						"IMAGE_POPUP" => "Y",
					), array(
						'upload' => true,
						'medialib' => true,
						'file_dialog' => true,
						'cloud' => true,
						'del' => false,
						'description' => false,
					));
					?>
				</td>
			</tr>
			<?
            $tabControl->EndCustomField("FLYSHEETS_IMG", "");
			break;
		default:
			$tabControl->AddEditField($fieldName, $fieldTitle, $fieldSettings['requied'], array(
				"size" => 40,
				"maxlength" => 255
			), $fieldValues[$fieldName]);
			break;
	}
}

// ============ FORM PROLOG ==================
$tabControl->BeginEpilogContent();
echo GetFilterHiddens("find_");
?>
	<input type="hidden" name="Update" value="Y"/>
	<input type="hidden" name="lang" value="<?=LANG?>"/>
	<input type="hidden" name="ID" value="<?=$ID?>"/>
    <input type="hidden" name="FILE_ID" value="<?=(isset($file_id) ? $file_id : $fieldValues["FILE_ID"])?>"/>
	<? if (CModule::IncludeModule('vdgb.tszhepasport')): ?>
		<input type="hidden" name="EP_ID" value="<?=$EP_ID?>"/>
	<? endif; ?>
<?
echo bitrix_sessid_post();
$tabControl->EndEpilogContent();
// ===========================================

// ����� ���������������� �����
if (
	(count($USER_FIELD_MANAGER->GetUserFields($UF_ENTITY)) > 0) ||
	($USER_FIELD_MANAGER->GetRights($UF_ENTITY) >= "W")
)
{
	$tabControl->AddSection('USER_FIELDS', GetMessage("TSZH_USER_FIELDS"));
	$tabControl->ShowUserFields($UF_ENTITY, $ID, $bVarsFromForm);
}

$tabControl->Buttons(Array(
	"disabled" => ($modulePermissions < "W"),
	"back_url" => $strRedirectList,
));

$tabControl->Show();

if (method_exists($USER_FIELD_MANAGER, 'showscript'))
{
	$tabControl->BeginPrologContent();
	echo $USER_FIELD_MANAGER->ShowScript();
	$tabControl->EndPrologContent();
}

$tabControl->ShowWarnings($tabControl->GetName(), $message);

if (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):?>
	<? echo BeginNote(); ?>
	<span class="required">*</span> <? echo GetMessage("REQUIRED_FIELDS") ?>
	<? echo EndNote(); ?>
<? endif; ?>
<? require_once($DOCUMENT_ROOT . BX_ROOT . "/modules/main/include/epilog_admin.php"); ?>