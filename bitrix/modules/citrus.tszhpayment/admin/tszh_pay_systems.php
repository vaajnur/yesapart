<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhpayment/include.php");

$tszhPaymentModulePermissions = $APPLICATION->GetGroupRight("citrus.tszhpayment");
if ($tszhPaymentModulePermissions == "D")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

IncludeModuleLangFile(__FILE__);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhpayment/prolog.php");

$sTableID = "tbl_tszh_pay_systems";

list($arTszhs, $arTszhSites) = citrusTszhPaymentGetTszhList();

$oSort = new CAdminSorting($sTableID, "ID", "asc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = array(
	"filter_lang",
	"filter_tszh",
	"filter_currency",
	"filter_active",
	"filter_name",
	"filter_sort",
);

$lAdmin->InitFilter($arFilterFields);

$arFilter = array();

if (strlen($filter_lang) > 0 && $filter_lang != "NOT_REF") $arFilter["LID"] = Trim($filter_lang);
if (strlen($filter_tszh) > 0) $arFilter["TSZH_ID"] = Trim($filter_tszh);
if (strlen($filter_currency) > 0) $arFilter["CURRENCY"] = Trim($filter_currency);
if (strlen($filter_active) > 0) $arFilter["ACTIVE"] = Trim($filter_active);
if (strlen($filter_name) > 0) $arFilter["%NAME"] = Trim($filter_name);
if (strlen($filter_sort) > 0) $arFilter["SORT"] = Trim($filter_sort);

if (($arID = $lAdmin->GroupAction()) && $tszhPaymentModulePermissions >= "W")
{
	if ($_REQUEST['action_target'] == 'selected')
	{
		$arID = Array();
		$dbResultList = CTszhPaySystem::GetList(
			array($by => $order),
			$arFilter,
			false,
			false,
			array("ID")
		);
		while ($arResult = $dbResultList->Fetch())
			$arID[] = $arResult['ID'];
	}

	foreach ($arID as $ID)
	{
		if (strlen($ID) <= 0)
		{
			continue;
		}

		switch ($_REQUEST['action'])
		{
			case "delete":
				@set_time_limit(0);

				$DB->StartTransaction();

				if (!CTszhPaySystem::Delete($ID))
				{
					$DB->Rollback();

					if ($ex = $APPLICATION->GetException())
					{
						$lAdmin->AddGroupError($ex->GetString(), $ID);
					}
					else
					{
						$lAdmin->AddGroupError(GetMessage("SPSAN_ERROR_DELETE"), $ID);
					}
				}

				$DB->Commit();

				break;

			case "activate":
			case "deactivate":

				$arFields = array(
					"ACTIVE" => (($_REQUEST['action'] == "activate") ? "Y" : "N")
				);

				if (!CTszhPaySystem::Update($ID, $arFields))
				{
					if ($ex = $APPLICATION->GetException())
					{
						$lAdmin->AddGroupError($ex->GetString(), $ID);
					}
					else
					{
						$lAdmin->AddGroupError(GetMessage("SPSAN_ERROR_UPDATE"), $ID);
					}
				}

				break;
		}
	}
}

$dbResultList = CTszhPaySystem::GetList(
	array($by => $order),
	$arFilter,
	false,
	false,
	array("ID", "LID", "TSZH_ID", "CURRENCY", "NAME", "ACTIVE", "SORT", "DESCRIPTION")
);

$dbResultList = new CAdminResult($dbResultList, $sTableID);
$dbResultList->NavStart();

$lAdmin->NavText($dbResultList->GetNavPrint(GetMessage("TSZH_PAYMENT_PRLIST")));

$lAdmin->AddHeaders(array(
	array("id" => "ID", "content" => "ID", "sort" => "id", "default" => true),
	array("id" => "NAME", "content" => GetMessage("TSZH_PAYMENT_NAME"), "sort" => "NAME", "default" => true),
	array("id" => "LID", "content" => GetMessage('TSZH_PAYMENT_LID'), "sort" => "LID", "default" => true),
	array("id" => "TSZH_ID", "content" => GetMessage('TSZH_PAYMENT_TSZH_ID'), "sort" => "TSZH_ID", "default" => true),
	array("id" => "CURRENCY", "content" => GetMessage("TSZH_PAYMENT_H_CURRENCY"), "sort" => "CURRENCY", "default" => true),
	array("id" => "ACTIVE", "content" => GetMessage("TSZH_PAYMENT_ACTIVE"), "sort" => "ACTIVE", "default" => true),
	array("id" => "SORT", "content" => GetMessage("TSZH_PAYMENT_SORT"), "sort" => "SORT", "default" => true),
));

$arVisibleColumns = $lAdmin->GetVisibleHeaderColumns();

while ($arCCard = $dbResultList->NavNext(true, "f_"))
{
	$row =& $lAdmin->AddRow($f_ID, $arCCard, "tszh_pay_systems_edit.php?ID=" . $f_ID . "&lang=" . LANG . GetFilterParams("filter_"), GetMessage("TSZH_PAYMENT_EDIT_DESCR"));

	$row->AddField("ID", $f_ID);
	$row->AddField("NAME", $f_NAME);
	$row->AddField("LID", $f_LID);
	$a = '<a title="' . GetMessage('TSZH_PAYMENT_TSZH_EDIT_TITLE') . '" href="tszh_edit.php?ID=' . $f_TSZH_ID . '&lang=' . LANG . "\">{$f_TSZH_ID}</a>";
	$row->AddField("TSZH_ID", "[{$a}] {$arTszhs[$f_TSZH_ID]}");
	$row->AddField("CURRENCY", $f_CURRENCY);
	$row->AddField("ACTIVE", (($f_ACTIVE == "Y") ? GetMessage("SPS_YES") : GetMessage("SPS_NO")));
	$row->AddField("SORT", $f_SORT);

	$arActions = Array();
	$arActions[] = array("ICON" => "edit", "TEXT" => GetMessage("TSZH_PAYMENT_EDIT"), "TITLE" => GetMessage("TSZH_PAYMENT_EDIT_DESCR"), "ACTION" => $lAdmin->ActionRedirect("tszh_pay_systems_edit.php?ID=" . $f_ID . "&lang=" . LANG . GetFilterParams("filter_") . ""), "DEFAULT" => true);
	if ($tszhPaymentModulePermissions >= "W")
	{
		$arActions[] = array("SEPARATOR" => true);
		$arActions[] = array("ICON" => "delete", "TEXT" => GetMessage("TSZH_PAYMENT_DELETE"), "TITLE" => GetMessage("TSZH_PAYMENT_DELETE_DESCR"), "ACTION" => "if(confirm('" . GetMessage('TSZH_PAYMENT_CONFIRM_DEL_MESSAGE') . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete"));
	}

	$row->AddActions($arActions);
}

$lAdmin->AddFooter(
	array(
		array(
			"title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"),
			"value" => $dbResultList->SelectedRowsCount()
		),
		array(
			"counter" => true,
			"title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"),
			"value" => "0"
		),
	)
);

$lAdmin->AddGroupActionTable(
	array(
		"delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"),
		"activate" => GetMessage("MAIN_ADMIN_LIST_ACTIVATE"),
		"deactivate" => GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"),
	)
);

if ($tszhPaymentModulePermissions == "W")
{
	$arDDMenu = array();

	/*
	$arDDMenu[] = array(
		"TEXT" => "<b>".GetMessage("SOPAN_4NEW_PROMT")."</b>",
		"ACTION" => false
	);
	*/

	$dbRes = CLang::GetList($by1 = "sort", $order1 = "asc");
	while (($arRes = $dbRes->Fetch()))
	{
		if(in_array($arRes["ID"], $arTszhSites))
		{
			$arDDMenu[] = array(
				"TEXT" => "[" . $arRes["LID"] . "] " . $arRes["NAME"],
				"ACTION" => "window.location = 'tszh_pay_systems_edit.php?lang=" . LANG . "&LID=" . $arRes["LID"] . "';"
			);
		}
	}

	$aContext = array(
		array(
			"TEXT" => GetMessage("SPSAN_ADD_NEW"),
			"TITLE" => GetMessage("SPSAN_ADD_NEW_ALT"),
			"ICON" => "btn_new",
			"MENU" => $arDDMenu
		),
	);
	$lAdmin->AddAdminContextMenu($aContext);
}

$lAdmin->CheckListMode();


/****************************************************************************/
/***********  MAIN PAGE  ****************************************************/
/****************************************************************************/
$APPLICATION->SetTitle(GetMessage("TSZH_PAYMENT_SECTION_TITLE"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>

    <form name="find_form" method="GET" action="<? echo $APPLICATION->GetCurPage() ?>?"><?=CTszhFlysheets::ShowBannerHtml("/bitrix/images/citrus.tszhpayment/banners/banner_flysheets_admin.gif");?>
        <?
		$oFilter = new CAdminFilter(
			$sTableID . "_filter",
			array(
				GetMessage("TSZH_PAYMENT_F_CURRENCY"),
				GetMessage("TSZH_PAYMENT_F_ACTIVE"),
				GetMessage("TSZH_PAYMENT_F_NAME"),
				GetMessage("TSZH_PAYMENT_F_SORT"),
			)
		);

		$oFilter->Begin();
		?>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_F_LANG"); ?>:</td>
			<td>
				<? echo CLang::SelectBox("filter_lang", $filter_lang, "(" . GetMessage("TSZH_PAYMENT_ALL") . ")") ?>
			</td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_F_TSZH_ID"); ?>:</td>
			<td>
				<select name="filter_tszh_id">
					<option value="">(<? echo GetMessage("TSZH_PAYMENT_ALL") ?>)</option>
					<? foreach ($arTszhs as $tszhID => $tszhName): ?>
						<option
							value="<?= $tszhID ?>"<? if ($filter_tszh_id == $tszhID) echo " selected" ?>><?= "[{$tszhID}] {$tszhName}" ?></option>
					<? endforeach ?>
				</select>
			</td>
		</tr>
		<? if (CModule::IncludeModule("currency")): ?>
			<tr>
				<td><? echo GetMessage("TSZH_PAYMENT_F_CURRENCY") ?>:</td>
				<td>
					<? echo CCurrency::SelectBox("filter_currency", $filter_currency, "(" . GetMessage("TSZH_PAYMENT_ALL") . ")", True, "", "") ?>
				</td>
			</tr>
		<? else: ?>
			<tr>
				<td><? echo GetMessage("TSZH_PAYMENT_F_CURRENCY") ?>:</td>
				<td>
					<input type="text" name="filter_currency" value="<?= $filter_currency ?>" size="10">
				</td>
			</tr>
		<? endif; ?>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_F_ACTIVE") ?>:</td>
			<td>
				<select name="filter_active">
					<option value="">(<? echo GetMessage("TSZH_PAYMENT_ALL") ?>)</option>
					<option
						value="Y"<? if ($filter_active == "Y") echo " selected" ?>><? echo GetMessage("TSZH_PAYMENT_YES") ?></option>
					<option
						value="N"<? if ($filter_active == "N") echo " selected" ?>><? echo GetMessage("TSZH_PAYMENT_NO") ?></option>
				</select>
			</td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_F_NAME") ?>:</td>
			<td>
				<input type="text" name="filter_name" value="<?= $filter_name ?>" size="40">
			</td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_PAYMENT_F_SORT") ?>:</td>
			<td>
				<input type="text" name="filter_sort" value="<?= $filter_sort ?>" size="20">
			</td>
		</tr>
		<?
		$oFilter->Buttons(
			array(
				"table_id" => $sTableID,
				"url" => $APPLICATION->GetCurPage(),
				"form" => "find_form"
			)
		);
		$oFilter->End();
		?>
	</form>

<?
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>