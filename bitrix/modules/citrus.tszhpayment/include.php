<?
global $DBType;
if ($GLOBALS['DBType'] != 'mysql') { return false; }

$tszhDBType = 'general';

define("TSZH_PAYMENT_MODULE_ID", 'citrus.tszhpayment');


if (!CModule::IncludeModule("citrus.tszh")) {
	return false;
}

// ���������� �������� ������
IncludeModuleLangFile(__file__);


CModule::AddAutoloadClasses(
	TSZH_PAYMENT_MODULE_ID,
	array(
		"CTszhPaySystem" => "classes/".$tszhDBType."/tszh_pay_system.php",
		"CTszhPayment" => "classes/".$tszhDBType."/tszh_payment.php",
		"CTszhPaymentGateway" => "classes/".$tszhDBType."/tszh_payment_gateway.php",
		"CTszhPaymentGatewayMoneta" => "classes/".$tszhDBType."/tszh_payment_gateway.php",
		"CTszhPaymentsExport" => "classes/".$tszhDBType."/tszh_payments_export.php",
        "CTszhFlysheets" => "classes/".$tszhDBType."/tszh_flysheets.php",
		'\Citrus\Tszhpayment\PaymentServicesTable'=>'lib/paymentservices.php',
	)
);

if (!class_exists('BaseJsonRpcClient'))
{
	\Bitrix\Main\Loader::registerAutoLoadClasses(TSZH_PAYMENT_MODULE_ID, array(
		"BaseJsonRpcClient" => "classes/".$tszhDBType."/BaseJsonRpcClient.php"
	));
}

include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszhpayment/functions.php");
?>
