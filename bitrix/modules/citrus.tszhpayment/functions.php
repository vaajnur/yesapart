<?IncludeModuleLangFile(__FILE__);

function citrusTszhPaymentGetTszhList()
{
	global $USER;

	$arResult = array();

	// �������� ���� ��� �������
	$arTszhFilter = $arTszhRight = $arListFilter = array();
	if(CModule::IncludeModule("vdgb.portaltszh") && defined("TSZH_PORTAL") && TSZH_PORTAL === true && !$USER->IsAdmin())
	{
		global $USER;
		$arGroups = $USER->GetUserGroupArray();

		$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
			array(
				"filter" => array("@GROUP_ID" => $arGroups)
			)
		);

		while($arPerms = $rsPerms->fetch())
		{
			if($arPerms["PERMS"] >= "R")
			{
				$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
			}
		}

		$arTszhFilter["@ID"] = $arListFilter["@TSZH_ID"] = array_keys($arTszhRight);
	}

	$rsTszh = CTszh::GetList(
		array('ID' => 'ASC'),
		$arTszhFilter,
		false,
		false,
		array("ID", "NAME", "SITE_ID")
	);
	while ($arTszh = $rsTszh->GetNext())
	{
		$arResult[$arTszh["ID"]] = $arTszh["NAME"];
		$arResultSites[] = $arTszh["SITE_ID"];
	}

	return array($arResult, array_unique($arResultSites), $arTszhFilter);
}

function tszhPaymentMonetaUsageTable($monetaEnabled = "N", $monetaOffer = "N", $radioName = "MONETA_SCHEME", $email = false, CWizardStep $wizardStep = null, $wizardReqMonetaSchemeErrorID = "REQ_MONETA_SCHEME")
{
	$result = '';

	$demoFlag = CTszhPaymentGateway::isDemo();

	if ($wizardStep)
	{
		$result .= '<link href="'.CUtil::GetAdditionalFileURL('/bitrix/js/main/core/css/core.css').'" type="text/css" rel="stylesheet" />'."\r\n";
		$result .= '<script type="text/javascript" src="'.CUtil::GetAdditionalFileURL('/bitrix/js/main/core/core.js').'"></script>'."\r\n";

		$arWizardErrors = $wizardStep->GetErrors();
		$reqMonetaSchemeErrorFlag = false;
		foreach ($arWizardErrors as $arError)
		{
			if (isset($arError[1]) && $arError[1] == $wizardReqMonetaSchemeErrorID)
			{
				$reqMonetaSchemeErrorFlag = true;
				break;
			}
		}

	}
	$result .= CUtil::InitJSCore(array('ajax', 'popup'), true);

	$result .= '<style type="text/css">';
	if ($wizardStep)
		$result .= '
			font.notetext {
				color: #008000;
			}
			.notetext {
				font-family: Arial;
				font-size: 10pt;
				font-weight: bold;
			}
		';
	$result .=
		'#moneta-usage-table {
			width: 600px;
			margin: 0 auto !important;
			border-collapse: collapse;
			border: none;
			border-spacing: 0;
		}
		#moneta-usage-table td,
		#moneta-usage-table th {
			width: 33.33%;
			padding: .3em;
			color: #000;
			text-align: center;
			border: 2px solid #000;
			background-color: #F5F9F9;
		}
		#moneta-usage-table th {
			font-size: 115%;
			line-height: 115%;
			padding: .5em;
		}
		#moneta-usage-table td.scheme1 {
			background-color: #ff5705;
		}
		#moneta-usage-table label.scheme,
		#moneta-usage-table label.scheme input {
			cursor: pointer;
		}
		#moneta-usage-table label.scheme input[disabled] {
			opacity: 1;
		}
	</style>
	';

	$result .= '<div id="moneta-procedure" style="display: none; max-width: 500px;">' . GetMessage("CITRUS_TSZH_PAYMENT_MONETA_PROCEDURE", Array("#EMAIL#" => $email === false ? GetMessage("CITRUS_TSZH_NEED_EMAIL") : $email)) . '</div>';

	$result .= "<script>
		var monetaProcedurePopup = new BX.PopupWindow(
			'moneta-procedure',
			null,
			{
				content: BX('moneta-procedure'),
				closeIcon: {right: '20px', top: '10px'},
				closeByEsc: true,
				zIndex: 0,
				offsetLeft: 140,
				offsetTop: 0,
				draggable: {restrict: false},
				autoHide: true,
				buttons: [
					new BX.PopupWindowButton({
						text: '" . GetMessage("CITRUS_TSZH_PAYMENT_CLOSE") . "',
						className: 'webform-button-link-cancel',
						events: {click: function(){
							this.popupWindow.close();
						}}
					})
				]
			}
		);
	</script>
	";

	$monetaOfferOnClick = "this.checked ? BX('{$radioName}1').checked='checked' : BX('{$radioName}2').checked='checked';";
	$monetaScheme2OnClick = "if (this.checked) BX('MONETA_OFFER').checked=false;";
	if ($demoFlag)
	{
		if (defined("ADMIN_SECTION") && !$wizardStep)
		{
			$clickMessage = "TSZH_MODULE_DEMO_NOTICE_ADMIN";

			$arMessage = Array(
				"DETAILS"	=> GetMessage("TSZH_MODULE_DEMO_NOTICE_ADMIN"),
				"TYPE"		=> "OK",
				"MESSAGE"	=> GetMessage("MUT_MONETA_DEMO"),
				"HTML"		=> true,
			);
			$result .= CAdminMessage::ShowMessage($arMessage);
		}
		else
		{
			$clickMessage = "TSZH_MODULE_DEMO_NOTICE";

			ob_start();
			ShowNote(GetMessage("MUT_MONETA_DEMO"));
			$html = ob_get_contents();
			ob_end_clean();
			$result .= $html . GetMessage("TSZH_MODULE_DEMO_NOTICE");
		}

		$monetaOfferOnClick = "this.checked=false; alert(monetaAlertMessage);";
		$result .= "<script>
			var monetaAlertMessage = '" . CUtil::JSEscape(GetMessage("MUT_MONETA_DEMO") . "\n\n" . strip_tags(preg_replace('#<br\s*/?>#', "\n", GetMessage($clickMessage)))) . "';
		</script>";
	}

	$result .= '<table id="moneta-usage-table">
		<tr>
			<th colspan="3">' . GetMessage("MUT_HEADER") . '</td>
		</tr>
		<tr>
			<td>
				' . GetMessage("MUT_VARIANTS") . '
			</td>
			<td class="scheme1">
				<input type="hidden" name="' . ($wizardStep ? '__wiz_' : '') . $radioName . '" value="' . (!$reqMonetaSchemeErrorFlag && $monetaOffer == "Y" ? '1' : (!$reqMonetaSchemeErrorFlag && $monetaOffer != "Y" && $monetaEnabled=="Y" ? '2' : '')) . '" />
				<label class="scheme" for="' . $radioName . '1">' .
					($wizardStep
					 ? $wizardStep->ShowRadioField($radioName, "1", array("id" => "{$radioName}1") + (!$reqMonetaSchemeErrorFlag && $monetaOffer == "Y" ? array("checked" => "checked", "disabled" => "disabled") : array()) + (!$reqMonetaSchemeErrorFlag && ($monetaOffer == "Y" || $monetaEnabled == "Y") ? array("disabled" => "disabled") : array()))
						: '<input type="radio" id="' . $radioName . '1" name="' . $radioName . '" value="1" ' . ($monetaOffer == "Y" ? 'checked="checked"' : '') . ' ' . ($monetaOffer == "Y" || $monetaEnabled == "Y" ? 'disabled="disabled"' : '') . ' />'
					)
					. ' ' . GetMessage("MUT_SCHEME1") .
				'</label>
			</td>
			<td>
				<label class="scheme" for="' . $radioName . '2">' .
					($wizardStep
					 ? $wizardStep->ShowRadioField($radioName, "2", array("id" => "{$radioName}2", "onclick" => $monetaScheme2OnClick) + (!$reqMonetaSchemeErrorFlag && $monetaEnabled=="Y" && $monetaOffer!="Y" ? array("checked" => "checked") : array()) + (!$reqMonetaSchemeErrorFlag && ($monetaOffer == "Y" || $monetaEnabled == "Y") ? array("disabled" => "disabled") : array()))
						: '<input type="radio" id="' . $radioName . '2" name="' . $radioName . '" value="2" ' . ($monetaEnabled=="Y" && $monetaOffer!="Y" ? 'checked="checked"' : '') . ' ' . ($monetaOffer == "Y" || $monetaEnabled == "Y" ? 'disabled="disabled"' : '') . ' onclick="' . $monetaScheme2OnClick . '" />'
					)
					. ' ' . GetMessage("MUT_SCHEME2") .
				'</label>
			</td>
		</tr>
		<tr>
			<td>
				' . GetMessage("MUT_DOGOVOR") . '
			</td>
			<td class="scheme1">
				+
			</td>
			<td>
				&ndash;
			</td>
		</tr>
		<tr>
			<td>
				' . GetMessage("MUT_PERSONAL_AREA") . '
			</td>
			<td class="scheme1">
				+
			</td>
			<td>
				&ndash;
			</td>
		</tr>
		<tr>
			<td>
				' . GetMessage("MUT_COMMISSION") . '
			</td>
			<td class="scheme1">
				' . GetMessage("MUT_COMMISSION_PAYEE") . '
			</td>
			<td>
				' . GetMessage("MUT_COMMISSION_PAYER") . '
			</td>
		</tr>
		<tr>
			<td>
				' . GetMessage("MUT_TRANSFER") . '
			</td>
			<td class="scheme1">
				' . GetMessage("MUT_BY_TEMPLATE") . '
			</td>
			<td>
				' . GetMessage("MUT_DAILY") . '
			</td>
		</tr>
		<tr>
			<td>
				' . GetMessage("MUT_PAY_WAYS") . '
			</td>
			<td class="scheme1">
				' . GetMessage("MUT_PAY_SYSTEMS") . ' ' . GetMessage("MUT_PAY_OTHERS") . '
				<br>
				<strong>' . GetMessage("MUT_PAY_WAYS_20") . '</strong>
			</td>
			<td>
				' . GetMessage("MUT_PAY_SYSTEMS") . '
			</td>
		</tr>
		<tr>
			<td>
				' . GetMessage("MUT_ACTIVITY") . '
			</td>
			<td class="scheme1">
				<input type="hidden" name="MONETA_OFFER" value="' . ($monetaOffer == "Y" ? 'Y' : '') . '" />
				<label for="moneta_offer_checkbox" title="">' . 
					($wizardStep
						? $wizardStep->ShowCheckboxField("MONETA_OFFER", "Y", (array("id" => "MONETA_OFFER", "onclick" => $monetaOfferOnClick) + ($monetaOffer == "Y" || $monetaEnabled=="Y" ? array("disabled" => "disabled") : array())))
						: '<input id="MONETA_OFFER" type="checkbox" name="MONETA_OFFER" value="Y" ' . ($monetaOffer == "Y" ? 'checked="checked"' : '') . ($monetaOffer == "Y" || $monetaEnabled == "Y" ? 'disabled="disabled"' : '') . ' onclick="' . $monetaOfferOnClick . '" />'
					)
					. GetMessage("CITRUS_TSZH_PAYMENT_MONETA_OFFER") . '
				</label>'
				.
				(strlen(trim($email)) <= 0 || !check_email($email) ? '<p>' . GetMessage("CITRUS_TSZH_NEED_EMAIL") . '</p>' : '')
				. '
			</td>
			<td>
				'. GetMessage("MUT_DEFAULT") . '
			</td>
		</tr>
	</table>
	';

	if ($wizardStep)
		return $result;
	else
		echo $result;
}

function tszhMonetaDemoNotice(CWizardStep $wizardStep = null)
{
	$result = '';

	if (!$wizardStep)
	{
		// todo �������� ����������� ����� � ������������ ���������� ������� ��� ���������������� �������
		$result .=
			'<div style="margin: 10px auto; max-width: 600px; border: 1px dashed #e7d1d7; background-color: #fff2f4; padding: 20px 20px 20px 70px; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAYAAAByDd+UAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA+tpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNSAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDE3LTExLTI5VDExOjIxOjA1KzAzOjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAxNy0xMS0yOVQxMToyMToxMSswMzowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxNy0xMS0yOVQxMToyMToxMSswMzowMCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDFEOTIzOTdENERFMTFFNzhDQTdBOUZFNjA1MTRCQkMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDFEOTIzOThENERFMTFFNzhDQTdBOUZFNjA1MTRCQkMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0MUQ5MjM5NUQ0REUxMUU3OENBN0E5RkU2MDUxNEJCQyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0MUQ5MjM5NkQ0REUxMUU3OENBN0E5RkU2MDUxNEJCQyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Psw23A4AAAMFSURBVHjarJbJTxRREMare1YQCYqCgDdcEuPFC8aowcREA3jwYLygHj144eD2F3hzORkvniTxSoQYl5gQDZNBjF4gjlExMSwNgs0y43T39OZXQzeyzPJ6pJIvNUlPv19XvXpVT6IypnQdj8CdgTqhY1ArVOs9TkPfoCT0Anrd9DxhllpPKgGKwV2DbkJNJGYKdBd6CLAhDASsDe4JdJAqM466G9APGx/IBWDdcEP/AWPbDyW8tYpHiD9cguulrbUriLR3E9BLI0cW2WIgF9EJQEdWgYDF4UahfSIrNPa/I8lxaOb8KVHoOHQYUN3fwx5R2Epe8J2hEKk7GsgEWMBaPQbJiC4Kf12UFT9wCDwpD61rP00TWZ1+Gzlyy796g1kcYQe0u5LNsdPpvF8yLZr8o5FmlYx2FzcPBnYFgVjTk6u/s8q/36brkqLrNKcb5LhF480DjwYCZpbJ9RbUpiY2PU9bNtKsUca0C73eJnsbGsgkxyYXxZItAMynGt/zyzBI0XSy1kfbysBtQYGuJK8UThnTbCe/t4s5yy+qGrmSYvGrVMS4jNRcjqaRZj5CDFQDR1jBRxoOikozlhj4tZIoXTc41iV3nIGJ4AcQFYg0VUAcDsM9C9Jp2GbRQ7m7BLWs7fTL3oRIBX25PhalPbEYhUTPr+OOP/j8442MDs6bcUcUFK6pzU+Lhr5BCjW3UHN1FcXk8hW7bFr3B9S07R+Lp9DbQNNClmkhZ+KM5agJ0O3h4rEatjPS8/HL440DeC/cJ5FGbqLEZ9BFTK9SOcLGqjiat4W9NWltC0dfVZNzC+23Uj/H1t1pkFruxOc4+nLACAAt1XGKy/LqGZvCwQ5LIUQbh5d8WCa1mLnswwre2hDpEbgBqEWkAcxjOnDD9m1nNEq1kTCphqFMZo2LHcnRIZFrIs+uR9AFkS3lXqmuOZeA9tVFw1eRtXnhi7AHPgl32xvSJfsuhq8zq+uDC4Z5r3N47GWxDijUgQHmm/dZb3byOKv3HnEf/g69h14hIqXcWn8FGABBH0RsDE7ewwAAAABJRU5ErkJggg==); background-repeat: no-repeat; background-position: 20px 20px;">
				<div style="font-size: 14px; color: #e74c3c; text-align: left;">' . GetMessage("MUT_MONETA_DEMO") . '</div>
				<div style="clear: both; width: 100%;"></div>
				<div>' . GetMessage("TSZH_MODULE_DEMO_NOTICE_WIZARD") . '</div>
		</div>';
	}
	else
	{
		$result .=
			'<div style="max-width: 600px; border: 1px dashed #e7d1d7; background-color: #fff2f4; padding: 20px 20px 20px 70px; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAYAAAByDd+UAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA+tpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNSAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDE3LTExLTI5VDExOjIxOjA1KzAzOjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAxNy0xMS0yOVQxMToyMToxMSswMzowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxNy0xMS0yOVQxMToyMToxMSswMzowMCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDFEOTIzOTdENERFMTFFNzhDQTdBOUZFNjA1MTRCQkMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDFEOTIzOThENERFMTFFNzhDQTdBOUZFNjA1MTRCQkMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0MUQ5MjM5NUQ0REUxMUU3OENBN0E5RkU2MDUxNEJCQyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0MUQ5MjM5NkQ0REUxMUU3OENBN0E5RkU2MDUxNEJCQyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Psw23A4AAAMFSURBVHjarJbJTxRREMare1YQCYqCgDdcEuPFC8aowcREA3jwYLygHj144eD2F3hzORkvniTxSoQYl5gQDZNBjF4gjlExMSwNgs0y43T39OZXQzeyzPJ6pJIvNUlPv19XvXpVT6IypnQdj8CdgTqhY1ArVOs9TkPfoCT0Anrd9DxhllpPKgGKwV2DbkJNJGYKdBd6CLAhDASsDe4JdJAqM466G9APGx/IBWDdcEP/AWPbDyW8tYpHiD9cguulrbUriLR3E9BLI0cW2WIgF9EJQEdWgYDF4UahfSIrNPa/I8lxaOb8KVHoOHQYUN3fwx5R2Epe8J2hEKk7GsgEWMBaPQbJiC4Kf12UFT9wCDwpD61rP00TWZ1+Gzlyy796g1kcYQe0u5LNsdPpvF8yLZr8o5FmlYx2FzcPBnYFgVjTk6u/s8q/36brkqLrNKcb5LhF480DjwYCZpbJ9RbUpiY2PU9bNtKsUca0C73eJnsbGsgkxyYXxZItAMynGt/zyzBI0XSy1kfbysBtQYGuJK8UThnTbCe/t4s5yy+qGrmSYvGrVMS4jNRcjqaRZj5CDFQDR1jBRxoOikozlhj4tZIoXTc41iV3nIGJ4AcQFYg0VUAcDsM9C9Jp2GbRQ7m7BLWs7fTL3oRIBX25PhalPbEYhUTPr+OOP/j8442MDs6bcUcUFK6pzU+Lhr5BCjW3UHN1FcXk8hW7bFr3B9S07R+Lp9DbQNNClmkhZ+KM5agJ0O3h4rEatjPS8/HL440DeC/cJ5FGbqLEZ9BFTK9SOcLGqjiat4W9NWltC0dfVZNzC+23Uj/H1t1pkFruxOc4+nLACAAt1XGKy/LqGZvCwQ5LIUQbh5d8WCa1mLnswwre2hDpEbgBqEWkAcxjOnDD9m1nNEq1kTCphqFMZo2LHcnRIZFrIs+uR9AFkS3lXqmuOZeA9tVFw1eRtXnhi7AHPgl32xvSJfsuhq8zq+uDC4Z5r3N47GWxDijUgQHmm/dZb3byOKv3HnEf/g69h14hIqXcWn8FGABBH0RsDE7ewwAAAABJRU5ErkJggg==); background-repeat: no-repeat; background-position: 20px 20px;">
				<div style="font-size: 14px; color: #e74c3c;">' . GetMessage("MUT_MONETA_DEMO") . '</div>
				<div style="clear: both; width: 100%;"></div>
				<div>' . GetMessage("TSZH_MODULE_DEMO_NOTICE_WIZARD") . '</div>
		</div>';
	}

	return $result;
}