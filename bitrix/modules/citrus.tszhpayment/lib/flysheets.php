<?php

namespace Citrus\Tszhpayment;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;
use \Citrus\Tszhpayment\Entity\DataManager;

Loc::loadMessages(__FILE__);

/**
 * Class HouseTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> TSZH_ID int mandatory
 * <li> AREA double optional
 * <li> FLATS_AREA double optional
 * <li> COMMON_AREA double optional
 * <li> REGION string(50) optional
 * <li> DISTRICT string(50) optional
 * <li> CITY string(50) optional
 * <li> SETTLEMENT string(50) optional
 * <li> STREET string(50) optional
 * <li> HOUSE string(50) optional
 * </ul>
 *
 * @package Bitrix\Tszh
 **/
class FlysheetsTable extends DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_flysheets';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{

		$sqlHelper = Application::getConnection()->getSqlHelper();

		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('FLYSHEETS_ENTITY_ID_FIELD'),
			),
			'DESCRIPTION' => array(
				'data_type' => 'string',
				'required' => false,
				'title' => Loc::getMessage('FLYSHEETS_ENTITY_DESCRIPTION_FIELD'),
			),
			'FILE_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('FLYSHEETS_ENTITY_FILE_ID_FIELD'),
			),
		);
	}

	/**
	 * Returns validators for REGION field.
	 *
	 * @return array
	 */
	public static function validateRegion()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for DISTRICT field.
	 *
	 * @return array
	 */
	public static function validateDistrict()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for CITY field.
	 *
	 * @return array
	 */
	public static function validateCity()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for SETTLEMENT field.
	 *
	 * @return array
	 */
	public static function validateSettlement()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for STREET field.
	 *
	 * @return array
	 */
	public static function validateStreet()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for HOUSE field.
	 *
	 * @return array
	 */
	public static function validateHouse()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}

    /**
     * Returns validators for BANK field.
     *
     * @return array
     */
    public static function validateBank()
    {
        return array(
            new Entity\Validator\Length(null, 100),
        );
    }

    /**
     * Returns validators for BIK field.
     *
     * @return array
     */
    public static function validateBIK()
    {
        return array(
            new Entity\Validator\Length(null, 16),
        );
    }

    /**
     * Returns validators for RS field.
     *
     * @return array
     */
    public static function validateRS()
    {
        return array(
            new Entity\Validator\Length(null, 24),
        );
    }

    /**
     * Returns validators for KS field.
     *
     * @return array
     */
    public static function validateKS()
    {
        return array(
            new Entity\Validator\Length(null, 24),
        );
    }

    /**
     * Returns validators for OVERHAUL_BANK field.
     *
     * @return array
     */
    public static function validateOVERHAUL_Bank()
    {
        return array(
            new Entity\Validator\Length(null, 100),
        );
    }

    /**
     * Returns validators for OVERHAUL_BIK field.
     *
     * @return array
     */
    public static function validateOVERHAUL_BIK()
    {
        return array(
            new Entity\Validator\Length(null, 16),
        );
    }

    /**
     * Returns validators for OVERHAUL_RS field.
     *
     * @return array
     */
    public static function validateOVERHAUL_RS()
    {
        return array(
            new Entity\Validator\Length(null, 24),
        );
    }

    /**
     * Returns validators for OVERHAUL_KS field.
     *
     * @return array
     */
    public static function validateOVERHAUL_KS()
    {
        return array(
            new Entity\Validator\Length(null, 24),
        );
    }

    /**
     * Returns validators for ZIP field.
     *
     * @return array
     */
    public static function validateZIP()
    {
        return array(
            new Entity\Validator\Length(null, 20),
        );
    }
}