<?php

namespace Citrus\Tszhpayment;

use Bitrix\Main,
	Bitrix\Main\Entity,
	Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Query\Join;

Loc::loadMessages(__FILE__);

/**
 * Class PaymentServicesTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> GUID string(500) optional
 * <li> SERVICE_NAME string(500) optional
 * <li> SUMM_PAYED string(255) optional
 * </ul>
 *
 * @package Bitrix\Tszh
 **/
class PaymentServicesTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_payment_services';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */


	public static function getMap()
	{
		return array(
			new Entity\IntegerField(
				'ID',
				array(
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('PAYMENT_SERVICES_ENTITY_ID_FIELD'),
				)
			),
			new Entity\StringField(
				'GUID',
				array(
					'required' => true,
					'validation' => array(__CLASS__, 'validateGuid'),
					'title' => Loc::getMessage('PAYMENT_SERVICES_ENTITY_GUID_FIELD'),
				)
			),
			new Entity\StringField(
				'SERVICE_NAME',
				array(
					'required' => true,
					'validation' => array(__CLASS__, 'validateServiceName'),
					'title' => Loc::getMessage('PAYMENT_SERVICES_ENTITY_SERVICE_NAME_FIELD'),
				)
			),
			new Entity\StringField(
				'SUMM_PAYED',
				array(
					'required' => true,
					'validation' => array(__CLASS__, 'validateSummPayed'),
					'title' => Loc::getMessage('PAYMENT_SERVICES_ENTITY_SUMM_PAYED_FIELD'),
				)
			),
			new Entity\IntegerField(
				'PAYMENT_ID',
				array(
					'required' => false,
					'title' => Loc::getMessage('PAYMENT_SERVICES_ENTITY_PAYMENT_ID_FIELD'),
				)
			),
			/*new Entity\ReferenceField(
				'PAYMENT_ID',
				'Citrus\Tszhpayment\PaymentTable',
				array(
					'=this.PAYMENT_ID' => 'ref.ID',
				)
			),*/
			(new Entity\ReferenceField(
				'PAYMENT',
				PaymentTable::class,
				Join::on('this.PAYMENT_ID', 'ref.ID')
			)
			)
				->configureJoinType('inner'),

		);
	}


	/**
	 * Returns validators for GUID field.
	 *
	 * @return array
	 */
	public static function validateGuid()
	{
		return array(
			new Main\Entity\Validator\Length(null, 500),
		);
	}

	/**
	 * Returns validators for SERVICE_NAME field.
	 *
	 * @return array
	 */
	public static function validateServiceName()
	{
		return array(
			new Main\Entity\Validator\Length(null, 500),
		);
	}

	/**
	 * Returns validators for SUMM_PAYED field.
	 *
	 * @return array
	 */
	public static function validateSummPayed()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}
}
