<?php

namespace Citrus\Tszhpayment;

use Bitrix\Main,
	Bitrix\Main\Entity,
	Bitrix\Main\Localization\Loc;

use Bitrix\Main\ORM\Fields\Relations\OneToMany;

Loc::loadMessages(__FILE__);

/**
 * Class PaySystemTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> LID string(2) mandatory
 * <li> TSZH_ID int mandatory
 * <li> CURRENCY string(3) mandatory
 * <li> NAME string(255) mandatory
 * <li> ACTIVE bool optional default 'Y'
 * <li> SORT int optional default 100
 * <li> DESCRIPTION string optional
 * <li> ACTION_FILE string(255) optional
 * <li> RESULT_FILE string(255) optional
 * <li> NEW_WINDOW bool optional default 'Y'
 * <li> PARAMS string optional
 * <li> HAVE_PAYMENT bool optional default 'N'
 * <li> HAVE_ACTION bool optional default 'N'
 * <li> HAVE_RESULT bool optional default 'N'
 * <li> HAVE_PREPAY bool optional default 'N'
 * <li> HAVE_RESULT_RECEIVE bool optional default 'N'
 * <li> ENCODING string(45) optional
 * </ul>
 *
 * @package Bitrix\Tszh
 **/
class PaySystemTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_pay_system';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_ID_FIELD'),
			),
			'LID' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateLid'),
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_LID_FIELD'),
			),
			'TSZH_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_TSZH_ID_FIELD'),
			),
			'CURRENCY' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateCurrency'),
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_CURRENCY_FIELD'),
			),
			'NAME' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateName'),
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_NAME_FIELD'),
			),
			'ACTIVE' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_ACTIVE_FIELD'),
			),
			'SORT' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_SORT_FIELD'),
			),
			'DESCRIPTION' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_DESCRIPTION_FIELD'),
			),
			'ACTION_FILE' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateActionFile'),
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_ACTION_FILE_FIELD'),
			),
			'RESULT_FILE' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateResultFile'),
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_RESULT_FILE_FIELD'),
			),
			'NEW_WINDOW' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_NEW_WINDOW_FIELD'),
			),
			'PARAMS' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_PARAMS_FIELD'),
			),
			'HAVE_PAYMENT' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_HAVE_PAYMENT_FIELD'),
			),
			'HAVE_ACTION' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_HAVE_ACTION_FIELD'),
			),
			'HAVE_RESULT' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_HAVE_RESULT_FIELD'),
			),
			'HAVE_PREPAY' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_HAVE_PREPAY_FIELD'),
			),
			'HAVE_RESULT_RECEIVE' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_HAVE_RESULT_RECEIVE_FIELD'),
			),
			'ENCODING' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateEncoding'),
				'title' => Loc::getMessage('PAY_SYSTEM_ENTITY_ENCODING_FIELD'),
			),
			(new OneToMany("PAYMENT", PaymentTable::class, "PAY_SYSTEM"))->configureJoinType("left"),
		);
	}

	/**
	 * Returns validators for LID field.
	 *
	 * @return array
	 */
	public static function validateLid()
	{
		return array(
			new Main\Entity\Validator\Length(null, 2),
		);
	}

	/**
	 * Returns validators for CURRENCY field.
	 *
	 * @return array
	 */
	public static function validateCurrency()
	{
		return array(
			new Main\Entity\Validator\Length(null, 3),
		);
	}

	/**
	 * Returns validators for NAME field.
	 *
	 * @return array
	 */
	public static function validateName()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for ACTION_FILE field.
	 *
	 * @return array
	 */
	public static function validateActionFile()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for RESULT_FILE field.
	 *
	 * @return array
	 */
	public static function validateResultFile()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for ENCODING field.
	 *
	 * @return array
	 */
	public static function validateEncoding()
	{
		return array(
			new Main\Entity\Validator\Length(null, 45),
		);
	}
}

