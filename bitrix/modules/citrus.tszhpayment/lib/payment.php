<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 11.05.2018 10:16
 */

namespace Citrus\Tszhpayment;

use Bitrix\Main,
	Bitrix\Main\Entity,
	Bitrix\Main\Localization\Loc;

use Bitrix\Main\ORM\Fields\Relations\OneToMany;
use Bitrix\Main\ORM\Query\Join;


Loc::loadMessages(__FILE__);

/**
 * Class PaymentTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> LID string(2) mandatory
 * <li> PAYED bool optional default 'N'
 * <li> DATE_PAYED datetime optional
 * <li> EMP_PAYED_ID int optional
 * <li> SUMM double mandatory
 * <li> SUMM_PAYED double mandatory
 * <li> CURRENCY string(3) mandatory
 * <li> USER_ID int optional
 * <li> PAY_SYSTEM_ID int optional
 * <li> DATE_INSERT datetime mandatory
 * <li> DATE_UPDATE datetime mandatory
 * <li> ACCOUNT_ID int optional
 * <li> PSYS_STATUS string(255) optional
 * <li> PSYS_STATUS_DESCR string(255) optional
 * <li> PSYS_STATUS_RESPONSE string optional
 * <li> PSYS_STATUS_DATE datetime optional
 * <li> PSYS_OPERATION_ID string(255) optional
 * <li> PSYS_FEE double optional
 * <li> TSZH_ID int optional
 * <li> C_PAYEE_NAME string(100) optional
 * <li> C_ADDRESS string(255) optional
 * <li> C_ACCOUNT string(50) optional
 * <li> C_COMMENTS string(255) optional
 * <li> INSURANCE_INCLUDED int mandatory
 * <li> IS_OVERHAUL string(1) optional
 * <li> IS_PENALTY string(1) optional
 * </ul>
 *
 * @package Bitrix\Tszh
 **/
class PaymentTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_payment';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			new Entity\IntegerField(
				'ID',
				array(
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('PAYMENT_ENTITY_ID_FIELD'),
				)
			),
			new Entity\StringField(
				'LID',
				array(
					'required' => true,
					'validation' => array(__CLASS__, 'validateLid'),
					'title' => Loc::getMessage('PAYMENT_ENTITY_LID_FIELD'),
				)
			),
			new Entity\BooleanField(
				'PAYED',
				array(
					'values' => array('N', 'Y'),
					'title' => Loc::getMessage('PAYMENT_ENTITY_PAYED_FIELD'),
				)
			),
			new Entity\DatetimeField(
				'DATE_PAYED',
				array(
					'title' => Loc::getMessage('PAYMENT_ENTITY_DATE_PAYED_FIELD'),
				)
			),
			new Entity\IntegerField(
				'EMP_PAYED_ID',
				array(
					'title' => Loc::getMessage('PAYMENT_ENTITY_EMP_PAYED_ID_FIELD'),
				)
			),
			new Entity\FloatField(
				'SUMM',
				array(
					'required' => true,
					'title' => Loc::getMessage('PAYMENT_ENTITY_SUMM_FIELD'),
				)
			),
			new Entity\FloatField(
				'SUMM_PAYED',
				array(
					'required' => true,
					'title' => Loc::getMessage('PAYMENT_ENTITY_SUMM_PAYED_FIELD'),
				)
			),
			new Entity\StringField(
				'CURRENCY',
				array(
					'required' => true,
					'validation' => array(__CLASS__, 'validateCurrency'),
					'title' => Loc::getMessage('PAYMENT_ENTITY_CURRENCY_FIELD'),
				)
			),
			new Entity\IntegerField(
				'USER_ID',
				array(
					'title' => Loc::getMessage('PAYMENT_ENTITY_USER_ID_FIELD'),
				)
			),
			new Entity\IntegerField(
				'PAY_SYSTEM_ID',
				array(
					'title' => Loc::getMessage('PAYMENT_ENTITY_PAY_SYSTEM_ID_FIELD'),
				)
			),
			new Entity\DatetimeField(
				'DATE_INSERT',
				array(
					'required' => true,
					'title' => Loc::getMessage('PAYMENT_ENTITY_DATE_INSERT_FIELD'),
				)
			),
			new Entity\DatetimeField(
				'DATE_UPDATE',
				array(
					'required' => true,
					'title' => Loc::getMessage('PAYMENT_ENTITY_DATE_UPDATE_FIELD'),
				)
			),
			new Entity\IntegerField(
				'ACCOUNT_ID',
				array(
					'title' => Loc::getMessage('PAYMENT_ENTITY_ACCOUNT_ID_FIELD'),
				)
			),
			new Entity\StringField(
				'PSYS_STATUS',
				array(
					'validation' => array(__CLASS__, 'validatePsysStatus'),
					'title' => Loc::getMessage('PAYMENT_ENTITY_PSYS_STATUS_FIELD'),
				)
			),
			new Entity\StringField(
				'PSYS_STATUS_DESCR',
				array(
					'validation' => array(__CLASS__, 'validatePsysStatusDescr'),
					'title' => Loc::getMessage('PAYMENT_ENTITY_PSYS_STATUS_DESCR_FIELD'),
				)
			),
			new Entity\TextField(
				'PSYS_STATUS_RESPONSE',
				array(
					'title' => Loc::getMessage('PAYMENT_ENTITY_PSYS_STATUS_RESPONSE_FIELD'),
				)
			),
			new Entity\DatetimeField(
				'PSYS_STATUS_DATE',
				array(
					'title' => Loc::getMessage('PAYMENT_ENTITY_PSYS_STATUS_DATE_FIELD'),
				)
			),
			new Entity\StringField(
				'PSYS_OPERATION_ID',
				array(
					'validation' => array(__CLASS__, 'validatePsysOperationId'),
					'title' => Loc::getMessage('PAYMENT_ENTITY_PSYS_OPERATION_ID_FIELD'),
				)
			),
			new Entity\FloatField(
				'PSYS_FEE',
				array(
					'title' => Loc::getMessage('PAYMENT_ENTITY_PSYS_FEE_FIELD'),
				)
			),
			new Entity\IntegerField(
				'TSZH_ID',
				array(
					'title' => Loc::getMessage('PAYMENT_ENTITY_TSZH_ID_FIELD'),
				)
			),
			new Entity\StringField(
				'C_PAYEE_NAME',
				array(
					'validation' => array(__CLASS__, 'validateCPayeeName'),
					'title' => Loc::getMessage('PAYMENT_ENTITY_C_PAYEE_NAME_FIELD'),
				)
			),
			new Entity\StringField(
				'C_ADDRESS',
				array(
					'validation' => array(__CLASS__, 'validateCAddress'),
					'title' => Loc::getMessage('PAYMENT_ENTITY_C_ADDRESS_FIELD'),
				)
			),
			new Entity\StringField(
				'C_ACCOUNT',
				array(
					'validation' => array(__CLASS__, 'validateCAccount'),
					'title' => Loc::getMessage('PAYMENT_ENTITY_C_ACCOUNT_FIELD'),
				)
			),
			new Entity\StringField(
				'EMAIL',
				array(
					'validation' => array(__CLASS__, 'validateCAccount'),
					'title' => Loc::getMessage('PAYMENT_ENTITY_EMAIL_FIELD'),
				)
			),
			new Entity\StringField(
				'C_COMMENTS',
				array(
					'validation' => array(__CLASS__, 'validateCComments'),
					'title' => Loc::getMessage('PAYMENT_ENTITY_C_COMMENTS_FIELD'),
				)
			),
			new Entity\IntegerField(
				'INSURANCE_INCLUDED',
				array(
					'required' => true,
					'title' => Loc::getMessage('PAYMENT_ENTITY_INSURANCE_INCLUDED_FIELD'),
				)
			),
			new Entity\BooleanField(
				'IS_OVERHAUL',
				array(
					'values' => array('N', 'Y'),
					'title' => Loc::getMessage('PAYMENT_ENTITY_IS_OVERHAUL_FIELD'),
				)
			),
			new Entity\BooleanField(
				'IS_PENALTY',
				array(
					'values' => array('N', 'Y'),
					'title' => Loc::getMessage('PAYMENT_ENTITY_IS_PENALTY_FIELD'),
				)
			),
			new Entity\ReferenceField(
				'PAYER_ID',
				'Citrus\Tszhpayment\PaymentPayerIdTable',
				array(
					'=this.ID' => 'ref.PAYMENT_ID',
				)
			),

			(new Entity\ReferenceField(
				'PAY_SYSTEM',
				PaySystemTable::class,
				Entity\Query\Join::on('this.PAY_SYSTEM_ID', 'ref.ID')
			)
			)
				->configureJoinType('inner'),

			(new OneToMany("SERVICE", PaymentServicesTable::class, "PAYMENT"))->configureJoinType("left"),

		);
	}

	/**
	 * Returns validators for LID field.
	 *
	 * @return array
	 */
	public static function validateLid()
	{
		return array(
			new Main\Entity\Validator\Length(null, 2),
		);
	}

	/**
	 * Returns validators for CURRENCY field.
	 *
	 * @return array
	 */
	public static function validateCurrency()
	{
		return array(
			new Main\Entity\Validator\Length(null, 3),
		);
	}

	/**
	 * Returns validators for PSYS_STATUS field.
	 *
	 * @return array
	 */
	public static function validatePsysStatus()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for PSYS_STATUS_DESCR field.
	 *
	 * @return array
	 */
	public static function validatePsysStatusDescr()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for PSYS_OPERATION_ID field.
	 *
	 * @return array
	 */
	public static function validatePsysOperationId()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for C_PAYEE_NAME field.
	 *
	 * @return array
	 */
	public static function validateCPayeeName()
	{
		return array(
			new Main\Entity\Validator\Length(null, 100),
		);
	}

	/**
	 * Returns validators for C_ADDRESS field.
	 *
	 * @return array
	 */
	public static function validateCAddress()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for C_ACCOUNT field.
	 *
	 * @return array
	 */
	public static function validateCAccount()
	{
		return array(
			new Main\Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for C_COMMENTS field.
	 *
	 * @return array
	 */
	public static function validateCComments()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}
}