<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 10.05.2018 14:23
 */

namespace Citrus\Tszhpayment;

use Bitrix\Main,
	Bitrix\Main\Entity,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class PaymentPayerIdTable
 *
 * Fields:
 * <ul>
 * <li> PAYMENT_ID int mandatory
 * <li> ACCOUNT_ID int mandatory
 * <li> DOCUMENT_TYPE int mandatory
 * <li> DOCUMENT_NUMBER string(20) mandatory
 * <li> NATIONALITY int mandatory
 * </ul>
 *
 * @package Bitrix\Tszh
 **/
class PaymentPayerIdTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_payment_payer_id';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			new Entity\IntegerField(
				'PAYMENT_ID',
				array(
					'primary' => true,
					'required' => true,
					'title' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_PAYMENT_ID_FIELD'),
					'validation' => array(__CLASS__, 'validatePaymentID'),
				)
			),
			new Entity\IntegerField(
				'ACCOUNT_ID',
				array(
					'title' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_ACCOUNT_ID_FIELD'),
				)
			),
			new Entity\StringField(
				'DOCUMENT_TYPE',
				array(
					'required' => true,
					'title' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_FIELD'),
					'validation' => array(__CLASS__, 'validateDocumentType'),
				)
			),
			new Entity\StringField(
				'DOCUMENT_NUMBER',
				array(
					'required' => true,
					'validation' => array(__CLASS__, 'validateDocumentNumber'),
					'title' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_NUMBER_FIELD'),
				)
			),
			new Entity\IntegerField(
				'NATIONALITY',
				array(
					'required' => true,
					'title' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_NATIONALITY_FIELD'),
				)
			),
			new Entity\ReferenceField(
				'PAYMENT',
				'Citrus\Tszhpayment\PaymentTable',
				array(
					'=this.PAYMENT_ID' => 'ref.ID',
				)
			),
			new Entity\ReferenceField(
				'ACCOUNT',
				'Citrus\Tszhpayment\AccountsTable',
				array(
					'=this.ACCOUNT_ID' => 'ref.ID',
				)
			),
		);
	}

	/**
	 * Returns validators for DOCUMENT_NUMBER field.
	 *
	 * @return array
	 */
	public static function validateDocumentNumber()
	{
		return array(
			new Main\Entity\Validator\Length(null, 20),
			function ($value) {
				if (preg_match('/^[A-Z\d]*$/', $value))
				{
					return true;
				}
				else
				{
					return Loc::getMessage("PAYMENT_PAYER_ID_ENTITY_DOCUMENT_NUMBER_REGEXP_ERROR");
				}
			}
			// new Main\Entity\Validator\RegExp('/[A-Z\d]*/'),
		);
	}

	/**
	 * Returns validators for PAYMENT_ID field.
	 *
	 * @return array
	 */
	public static function validatePaymentID()
	{
		return array(
			new Main\Entity\Validator\Unique(),
		);
	}

	/**
	 * Returns validators for DOCUMENT_TYPE field.
	 *
	 * @return array
	 */
	function validateDocumentType()
	{
		return array(
			function ($value) {
				$list = self::getDocumentTypeList();
				if (!in_array($value, array_keys($list)))
				{
					return Loc::getMessage("PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_ERROR_TYPE");
				}

				return true;
			},
		);
	}

	/**
	 * Returns list of values for field DOCUMENT_TYPE
	 *
	 * @return array
	 */
	public static function getDocumentTypeList()
	{
		return array(
			'01' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_01'),
			'02' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_02'),
			'03' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_03'),
			'04' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_04'),
			'05' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_05'),
			'06' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_06'),
			'07' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_07'),
			'08' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_08'),
			'09' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_09'),
			'10' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_10'),
			'11' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_11'),
			'12' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_12'),
			'13' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_13'),
			'14' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_14'),
			'21' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_21'),
			'22' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_22'),
			'24' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_24'),
			'25' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_25'),
			'26' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_26'),
			'27' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_27'),
			'28' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_28'),
			'29' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_29'),
			'30' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_DOCUMENT_TYPE_30'),
		);
	}

	/**
	 * Returns list of values for field NATIONALITY
	 *
	 * @return array
	 */
	public static function getNationalityList()
	{
		return array(
			'643' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_NATIONALITY_643'),
			'999' => Loc::getMessage('PAYMENT_PAYER_ID_ENTITY_NATIONALITY_999'),
		);
	}

	/**
	 * @param $paymentID
	 *
	 * @return string
	 * @throws Main\ArgumentException
	 * @throws Main\ObjectPropertyException
	 * @throws Main\SystemException
	 * @throws \Exception
	 */
	public static function getAltPayerIdentifier($paymentID)
	{
		if (!isset($paymentID))
		{
			throw new \Exception('Not set payment identificator parameter');
		}

		$arPayerId = self::getByPrimary($paymentID)->fetch();

		if (is_array($arPayerId))
		{
			$payer_identifier = self::prepareAltPayerIdentifier($arPayerId);
		}
		else
		{
			$payer_identifier = '';
		}

		return $payer_identifier;
	}

	/**
	 * @param $arPayerIdentifier
	 *
	 * @return string
	 */
	protected static function prepareAltPayerIdentifier($arPayerIdentifier)
	{
		$str_payer_identifier = $arPayerIdentifier['DOCUMENT_TYPE'] . str_pad($arPayerIdentifier['DOCUMENT_NUMBER'], 20, '0', STR_PAD_LEFT) . $arPayerIdentifier['NATIONALITY'];

		return $str_payer_identifier;
	}
}