<form action="<?echo $APPLICATION->GetCurPage()?>">
<?=bitrix_sessid_post()?>
	<input type="hidden" name="lang" value="<?echo LANG?>">
	<input type="hidden" name="id" value="vdgb.tszhepasport">
	<input type="hidden" name="uninstall" value="Y">
	<input type="hidden" name="step" value="2">
	<?echo CAdminMessage::ShowMessage(GetMessage("MOD_UNINST_WARN"))?>
	<p><?echo GetMessage("MOD_UNINST_SAVE")?></p>
	<p><input type="checkbox" name="save_tables" id="save_tables" value="1" checked><label for="save_tables"><?echo GetMessage("VDGB_TSZHEPASSPORT_UNINST_SAVE_TABLES")?></label></p>
	<p><input type="checkbox" name="save_public_files" id="save_public_files" value="1" checked><label for="save_public_files"><?= GetMessage("VDGB_TSZHEPASSPORT_UNINST_SAVE_FILES") ?></label></p>
	<input type="submit" name="inst" value="<?echo GetMessage("MOD_UNINST_DEL")?>">
</form>
