ymaps.ready(function () {
    var MyBalloonContentLayoutClass = ymaps.templateLayoutFactory.createClass(
        "<img class='balloon-house-image' src='{{properties.houseImgSrc}}'>" +
        "<div class='balloon-text'>" +
        "<div class='balloon-item-name'>" + BX.message("TYPE_OF_HOUSE") + "</div>" + "<div class='balloon-item'> {{properties.typeOfHouse}}</div>" +
        "<div class='balloon-item-name'>" + BX.message("SUMM_COM_AREA") + "</div>" + "<div class='balloon-item'> {{properties.comArea}}" + BX.message("M2") + "</div>" +
        "<div class='balloon-item-name'>" + BX.message("FLOORS") + "</div>" + "<div class='balloon-item'> {{properties.floors}}</div>" +
        "<div class='balloon-item-name'>" + BX.message("PORCHES") + "</div>" + "<div class='balloon-item'> {{properties.porches}}</div>" +
        "<div class='balloon-item-name'>" + BX.message("YEAR_OF_BUILT") + "</div>" + "<div class='balloon-item'> {{properties.yearOfBuilt}}</div>" +
        "{%if properties.typeOfHouse == '��' %}" +
        /*"<div class='balloon-item-name'>" + BX.message("YEAR_OF_COMMISSIONING") + "</div>" + "<div class='balloon-item'> {{properties.yearOfCommisioning}}</div>" +*/
        "{%endif%}" +
        "<a style='position: absolute; bottom: 15px;' class='main-bg ' href='?home-id=" + '{{properties.arResultId}}' + "'>" +
        BX.message("DETAIL") + " <img src='" + componentPath + "/images/arrow_detail.png'>" +
        "</a>" +
        "</div>"
    );

    var myMap = window.map = new ymaps.Map('first_map', {
            center: arCoordinates[0], // ����� ������� �� ����������� ������� ����
            zoom: 10,
            behaviors: ['default'],
            controls: arMapControls,
        }),

        clusterer = new ymaps.Clusterer({
            preset: 'twirl#darkgreenClusterIcons',
            groupByCoordinates: false,
            clusterDisableClickZoom: true,
            //zoomMargin: 50,
            clusterBalloonItemContentLayout: MyBalloonContentLayoutClass
        }),

        getPointData = function (i) {
            var result = {
                houseImgSrc: (arResult.EP_HOUSE_IMAGE_SRC[i] != null) ? arResult.EP_HOUSE_IMAGE_SRC[i] : componentPath + "/images/home_default_image.png",
                adress: arResult.EP_ADDRESS[i],
                typeOfHouse: arResult.TYPE[i],
                comArea: arResult.AREA[i],
                floors: arResult.FLOORS[i],
                porches: arResult.PORCHES[i],
                yearOfBuilt: arResult.YEAR_OF_BUILT[i],
                clusterCaption: '<strong class="cluster-menu-item" id=house_' + i + '>' + BX.message("HOUSE") + arResult.EP_HOUSE_ID[i] + '</strong>',
                id: i,
                arResultId: arResult.EP_HOUSE_ID[i],
            };
            $.each(result, function (key, value) {
                if (value == "" || (parseInt(value) == 0) || value == null) {
                    if (key != "id") {
                        result[key] = BX.message("DATA_NOT_SET");
                    }
                }
            });
            return result;
        },

        getPointOptions = function (i) {

            return arResult.POINTOPTIONS[i];
        },

        MyGeoObjects = [],
        currOpenedCluster = -1;

    for (var i = 0, len = points.length; i < len; i++) {
        MyGeoObjects[i] = new ymaps.Placemark(points[i], getPointData(i), getPointOptions(i));
        MyGeoObjects[i].options.set('balloonCloseButton', false);
        MyGeoObjects[i].options.set('balloonAutoPan', true);
        MyGeoObjects[i].options.set('balloonMinHeight', 200);
        MyGeoObjects[i].options.set('balloonMinWidth', 400);
        MyGeoObjects[i].options.set('balloonAutoPanMargin', [200, 200, 200, 200]);
        MyGeoObjects[i].options.set('balloonContentLayout', MyBalloonContentLayoutClass);

        MyGeoObjects[i].events.add('click', function (e) {
            var eGeoObject = e.get('target');
            var indexOfhouse = eGeoObject.properties.get("id");
            init(indexOfhouse, "0");
        });
    }

    clusterer.options.set({
        gridSize: 80,
        clusterDisableClickZoom: true,
        minClusterSize: arResult.MINCLUSTERSIZE,
        balloonCloseButton: false,
        balloonAutoPan: true,
        balloonAutoPanMargin: [200, 200, 200, 200]

    });

    /**
     * ��������� �������� ����������� ����������,
     * �������� �� ����������, ����� ��������� ����� �������, ������� ��� ��������.
     * ���������� ����� once ����� ������� ��� ���� ���.
     */
    clusterer.events.once('objectsaddtomap', function () {
        myMap.setBounds(clusterer.getBounds());
    });

    var stateMonitor;
    clusterer.events.add('balloonopen', function (e) {
        var MyCluster = e.get("cluster");
        //   console.log(MyCluster);
        currOpenedCluster = clusterer.getClusters().indexOf(MyCluster);
        if (MyCluster) {
            stateMonitor = new ymaps.Monitor(MyCluster.state);
            stateMonitor.add("activeObject", function (newValue) {
                init(newValue.properties.get('id'), "1");
            });
        } else {
            MyCluster = e.get("target");
            init(MyCluster.properties.get("id"), "1");
        }
    });
    clusterer.events.add('balloonclose', function (e) {
        var MyCluster = e.get("cluster");
        if (MyCluster) {
            stateMonitor.remove("activeObject");
        }
        if (clusterer.getClusters().indexOf(MyCluster) != currOpenedCluster) {
            currOpenedCluster = -1;
        }
    });
    // myMap.behaviors.enable('scrollZoom');
    //myMap.controls.add('zoomControl');
    clusterer.add(MyGeoObjects);
    myMap.geoObjects.add(clusterer);
    //myMap.setBounds(clusterer.getBounds()); /** �������� �� ����� ��� �������*/

    form_string = function (i) {
        var str = '<span class="detail-descr-adress">' + arResult.EP_ADDRESS[i] + '</span><br /><br />\
        <span>' + BX.message("TYPE_OF_HOUSE") + arResult.TYPE[i] + '</span><br />\
        <span>' + BX.message("SUMM_COM_AREA") + arResult.AREA[i] + '</span><br />\
        <span>' + BX.message("FLOORS") + arResult.FLOORS[i] + '</span><br />\
        <span>' + BX.message("PORCHES") + arResult.PORCHES[i] + '</span><br />\
        <span>' + BX.message("YEAR_OF_BUILT") + arResult.YEAR_OF_BUILT[i] + '</span><br /><br />\
        <a href="?home-id=' + arResult.EP_HOUSE_ID[i] + '" class="podrobnee main-bg">' + BX.message("DETAIL") + ' <span style="font-size:22px;"> <img src="' + componentPath + '/images/arrow_detail.png"></span></a>';
        return str;
    }
    insert_detail_info = function (i) {
        var str = form_string(i)
        $("#detaile-descr").html(str);
        $("#detaile-descr").css("display", "block");

    }

    init = function (i, balisopened) {
        var objectState = clusterer.getObjectState(MyGeoObjects[i]);
        var myAction = new ymaps.map.action.Single({
            center: points[i],
            duration: 200,
            zoom: 18,
            timingFunction: "ease-in"
        });
        if (objectState.isShown) {
            if (objectState.isClustered) {
                // ���� ����� ��������� � ��������, �������� �� � �������� ��������� �������.
                // ����� ��� ����� "�������" � �������� ������ ��������.
                objectState.cluster.state.set('activeObject', MyGeoObjects[i]);
                if ((balisopened == '0')) {
                    if (clusterer.getClusters().indexOf(objectState.cluster) != currOpenedCluster) {
                        clusterer.balloon.open(objectState.cluster);
                        currOpenedCluster = clusterer.getClusters().indexOf(objectState.cluster);
                    }
                }
            } else {
                MyGeoObjects[i].balloon.open();//�������� ������
                currOpenedCluster = -1;
            }
        } else {
            myMap.panTo([MyGeoObjects[i].geometry.getCoordinates()], {flying: true}).then(function () {
                MyGeoObjects[i].balloon.open();
            });
        }
        /*����� �������� �� ��������������� �����, ���� ����� ������*/

        $("div .element-spisok .pre-arrow").removeClass("item-selected");
        $("#element_" + arResult.EP_HOUSE_ID[i] + " .pre-arrow").addClass("item-selected");
        $(".element-spisok img").attr("src", componentPath + "/images/arrow_notactive.png");
        $(".spisoksok [href=" + "'?home-id=" + arResult.EP_HOUSE_ID[i] + "'] img").attr("src", componentPath + "/images/arrow.png");
        ii = Math.floor(i / chislodomov);
        $('.spisoksok').css('margin-top', -(hperemotki * ii) + 'px');
    }
});