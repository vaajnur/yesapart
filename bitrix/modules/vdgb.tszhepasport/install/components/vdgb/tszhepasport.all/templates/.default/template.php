<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
} ?>
<?$yandexApiKey = htmlspecialcharsbx(COption::GetOptionString('fileman', "yandex_map_api_key", ""));?>
<? $APPLICATION->AddHeadScript('https://api-maps.yandex.ru/2.1/?apikey='.$yandexApiKey.'lang=ru_RU&mode=debug'); ?>
<script type="text/javascript">
    var arResult = <?= \Bitrix\Main\Web\Json::encode($arResult);?>;
    var arMapControls = <?= \Bitrix\Main\Web\Json::encode($arParams["MAPCONTROLS"]);?>;
    var points = arCoordinates = <?= \Bitrix\Main\Web\Json::encode($arResult["COORDINATES"]);?>;
    var componentPath = <?= \Bitrix\Main\Web\Json::encode($componentPath);?>;
</script>
<? if (!$_GET['home-id']): ?>
	<? CJSCore::Init(array('ymaps_js', 'jquery')); ?>
	<? if ((isset($arResult["WARNINGS"])) && ($USER->IsAdmin())): ?>
		<div id="warning" class="warning">
			<div class="close" onclick="document.getElementById('warning').style.display ='none';">X</div>
			<?=GetMessage('WARNING_PART_1')?><?=$arResult["WARNING_HOUSES"]?><?=GetMessage('WARNING_PART_2')?>
			<?
			foreach ($arResult["WARNINGS"] as $idx => $warning_adress)
			{
				echo ($idx + 1) . ') ' . $warning_adress . " ";
			}
			?>
			<?=GetMessage('WARNING_PART_3')?>
		</div>
	<? endif ?>
	<div id="tabs-home">
		<div class="home-work main-border main-bg"><?=GetMessage('HOUSES_IN_SERVICE')?></div>
		<div class="home-nowork main main-border"><?=GetMessage("HOUSES_OUT_OF_SERVICE")?></div>
		<div style="clear:both;">
		</div>
		<div>
			<div id="first_map">
			</div>
			<div class="list-homes">
				<div class="list-homes-header">
					<img class="main-bg lupa" src="<?=$componentPath?>/images/lupa.png">
					<input class="val_text" type="text" value="" placeholder="<?=GetMessage("ADRESS_SEARCH")?>">
				</div>
				<div class="spisok">
					<div class="donego main-bg"><img src="<?=$componentPath?>/images/vverh-s.png"></div>
					<div class="esche main-bg"><img src="<?=$componentPath?>/images/vniz-s.png"></div>
					<div class="spisoksok">
						<? for ($i = 0; $i < count($arResult['EP_ADDRESS']); $i++): ?>
							<div id="element_<?=$arResult['EP_HOUSE_ID'][$i]?>" class="element-spisok">
								<div class="pre-arrow" onclick=init("<?=$i?>","0");>
									<?=($arResult['EP_ADDRESS'] == '') ? GetMessage("DATA_NOT_SET") : $arResult['EP_ADDRESS'][$i]?>
								</div>
								<div class="arrow">
									<a style="color:#000;text-decoration:none;" href="?home-id=<?=$arResult['EP_HOUSE_ID'][$i]?>">
										<img class="arrowclass" src="<?=$componentPath?>/images/arrow_notactive.png">
									</a>
								</div>
							</div>
						<? endfor ?>
						<div style='clear: both;'></div>
					</div>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
		<div id="detaile-descr">
		</div>
	</div>
<? endif ?>

<script type="text/javascript" src="<?=$componentPath?>/templates/.default/search-home.js"></script>
<script type="text/javascript" src="<?=$componentPath?>/templates/.default/list-scroll.js"></script>