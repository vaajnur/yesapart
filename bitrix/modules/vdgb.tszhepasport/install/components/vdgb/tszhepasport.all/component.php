<?
/**
 * ������ ���ƻ
 * ��������� ���� � ���������� (vdgb.tszhepasport)
 * @package tszh
 */

use Vdgb\Tszhepasport\EpasportTable;
use Citrus\Tszh\House;
use Bitrix\Main\Localization\Loc;

\Bitrix\Main\Loader::includeModule('vdgb.tszhepasport');

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}


CPageOption::SetOptionString("main", "nav_page_in_session", "N");


$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"] == "Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"] != "N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"] != "N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"] == "Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"] !== "N";


// ��������� ������ ���������� - ���������� � ������ ����������
function get_response_from_yandex($adr_to_send)
{
    $adr_to_send = iconv("windows-1251", "UTF-8", $adr_to_send);
    $yandexApiKey = htmlspecialcharsbx(COption::GetOptionString('fileman', "yandex_map_api_key", ""));
	$params = array(
		'geocode' => urldecode($adr_to_send), // �����
        'apikey' => $yandexApiKey,
		'format' => 'json',
        'results' => 1,
		'lang' => 'en_US',
        );
	$urlget = 'https://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&');
	sleep(1);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $urlget);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$file_get_coor = curl_exec($ch);
	curl_close($ch);

	return json_decode($file_get_coor);
}

if ($this->StartResultCache())
{
	$arResult = Array();
	$arHouses = \Citrus\Tszh\HouseTable::getList(array(
		'select' => array(
			"*",
			"FULL_ADDRESS",
			'' => 'Vdgb\Tszhepasport\EpasportTable:HOUSE.*',
		),
		'order' => array('ID' => 'ASC'),
	));
	$numOfIncorrectAddr = 0;
	$countHouse = 0;
	while ($arHouse = $arHouses->Fetch())
	{
		foreach ($arHouse as $houseField => $houseValue)
		{
			switch ($houseField)
			{
				case 'EP_LONGITUDE':
				case 'EP_LATITUDE':
					$houseValue = floatval($houseValue);
					break;
				case 'TYPE':
					$houseValue = (strlen($houseValue) == 0) ? GetMessage("MKD") : $houseValue;
					break;
				case 'EP_HTML':
					$houseValue = null;
					break;
			}
			$arResult[$houseField][$countHouse] = $houseValue;
		}


		$addrToSearch = CTszhAccount::GetFullAddress($arHouse, array("FLAT"));

		$arResult['EP_ADDRESS'][$countHouse] = $addrToSearch;
		$arResult['EP_HOUSE_IMAGE_SRC'][$countHouse] = CFile::GetPath($arHouse['EP_HOUSE_IMG_ID']);
		$arResult["COORDINATES"][$countHouse] = array($arHouse['EP_LONGITUDE'], $arHouse['EP_LATITUDE']);
		$arCoorFromBase = array("0" => $arHouse['EP_LATITUDE'], "1" => $arHouse['EP_LONGITUDE']); //

		if ($arHouse['EP_HOUSE_IMG_ID'] != null)
		{
			$resizedFile = CFile::ResizeImageGet($arHouse['EP_HOUSE_IMG_ID'], array(
				'width' => 150,
				'height' => 150,
			), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$arResult["POINTOPTIONS"][$countHouse] = array("iconLayout" => 'default#image', "iconImageHref" => $resizedFile["src"]);
		}
		else
		{
			$arResult["POINTOPTIONS"][$countHouse] = array("preset" => 'twirl#blue');
		}

		if (($arCoorFromBase[0] == 0)/* && ($arCoorFromBase[1] == 0)*/) // ���� ����������, ���������� �� �� �������
		{
			$arCoorFromYandex = array("0" => 0, "1" => 0);
			$errHouseAdded = false;

			while (($arCoorFromYandex[0] == 0) && strlen($addrToSearch) != 0) /*������ ������ � �������, ���� ������ �� ������� ��� ������� � ����� ��������*/
			{
				$response = get_response_from_yandex($addrToSearch); // �������� json-o���� �� ����������� �������
				if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0)
				{
					$coordinates_string = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos; // ������, �������� ������������ https://tech.yandex.ru/maps/doc/geocoder/desc/concepts/input_params-docpage/
					$arCoorFromYandex = explode(" ", $coordinates_string); // ���������� ��������� � ������� "Point":{"pos":"47.899878 56.634407"}
					$arCoorToBase = array(
						'EP_LATITUDE' => floatval($arCoorFromYandex[0]),
						'EP_LONGITUDE' => floatval($arCoorFromYandex[1]),
					); //latitude - ������, longintude - �������
					if ($arCoorFromYandex[0])
					{
						$arResult["EP_LATITUDE"][$countHouse] = $arCoorToBase['EP_LATITUDE'];
						$arResult["EP_LONGITUDE"][$countHouse] = $arCoorToBase['EP_LONGITUDE'];

						if ($arHouse['EP_ID'] != null)
						{
							$res2 = \Vdgb\Tszhepasport\EpasportTable::Update($arHouse['EP_ID'], $arCoorToBase);
						}
						else
						{
							$arCoorToBase = array_merge($arCoorToBase, array("EP_HOUSE_ID" => $arHouse["ID"]));
							$res2 = \Vdgb\Tszhepasport\EpasportTable::Add($arCoorToBase);
						}
						$arResult["COORDINATES"][$countHouse] = array($arCoorToBase['EP_LONGITUDE'], $arCoorToBase['EP_LATITUDE']);
					}
				}
				else
				{
					if (!$errHouseAdded)
					{
						$numOfIncorrectAddr++;
						$errHouseAdded = true;
						$arResult["WARNINGS"][$countHouse] = $addrToSearch;
					}
				}
				if ($arCoorFromYandex[0] == 0)
				{
					$addrToSearch = substr($addrToSearch, 0, strrpos($addrToSearch, ','));
				}
			}
		}
		$countHouse++;
	}


	if ($arParams["YANDEX_CLUSTERIZATION"] != "Y")
	{
		$arResult["MINCLUSTERSIZE"] = 2000;
	}
	else
	{
		$arResult["MINCLUSTERSIZE"] = 2;
	}
	if ($numOfIncorrectAddr > 0)
	{
		$arResult["WARNING_HOUSES"] = $numOfIncorrectAddr;
		\CAdminNotify::add(
			array(
				"MESSAGE" => Loc::GetMessage('VDGB_HOUSES_WITH_INCORRECT_COORDINATES'),
				"TAG" => "incorrect_coordinates_epassports",
				"MODULE_ID" => "vdgb.tszhepasport",
				"ENABLE_CLOSE" => "Y",
			));
	}
	else
	{
		\CAdminNotify::DeleteByTag("incorrect_coordinates_epassports");
	}

	if (!(file_exists($file_cache) && date("d", filemtime($file_cache)) == date("d")))
	{
		$this->AbortResultCache();
	}
	$this->IncludeComponentTemplate();
}

?>