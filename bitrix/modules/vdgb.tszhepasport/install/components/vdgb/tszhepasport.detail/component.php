<?
/**
 * ������ ���ƻ
 * ��������� ���� � ���������� (vdgb.tszhepasport)
 * @package tszh
 */

use Vdgb\Tszhepasport\EpasportTable;
use Citrus\Tszh\HouseTable;

\Bitrix\Main\Loader::includeModule('vdgb.tszhepasport');

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"] == "Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"] != "N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"] != "N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"] == "Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"] !== "N";


$id_home = $_GET['home-id'];
if ($this->StartResultCache())
{
	if ($id_home)
	{
		$arResult = Array();
		$res = \Citrus\Tszh\HouseTable::getList(array(
			'select' => array(
				"*",
				'' => 'Vdgb\Tszhepasport\EpasportTable:HOUSE.*',
			),
			"filter" => array("ID" => $id_home),
		));
		if (!$res->getSelectedRowsCount())
		{
			$this->abortResultCache();
			ShowError(GetMessage("PASSPORT_NOT_FOUND"));

			return;
		}


		$arHome = $res->Fetch();

		if (strlen($arHome['EP_ADDRESS']) <= 0)
		{
			$arHome['EP_ADDRESS'] = CTszhAccount::GetFullAddress($arHome, array("FLAT"));
		}
		if (strlen($arHome['EP_TYPE']) <= 0)
		{
			$arHome['EP_TYPE'] = GetMessage("MKD");
		}
		$arHome['EP_HOUSE_IMAGE_SRC'] = CFile::GetPath($arHome['EP_HOUSE_IMG_ID']);

		$arViewTableField = array(
			'EP_TYPE',
			'AREA',
			'FLOORS',
			'PORCHES',
			'YEAR_OF_BUILT',
		);

		$arResult = $arHome;

		foreach ($arViewTableField as $tableField)
		{
			if (array_key_exists($tableField, $arHome))
			{
				$arResult["TABLE"][$tableField] = $arHome[$tableField];
			}
		}


		$HTML_D = explode(" || ", $arHome['EP_HTML']);

		for ($iter_html = 0; $iter_html < count($HTML_D); $iter_html = $iter_html + 3)
		{
			if ($HTML_D[$iter_html] == "")
			{
				continue;
			} /*�� ��������� �������� �������� ������� � �����������*/
			$arResult['HTML'][($iter_html / 3) + 1] = array(
				"NAME" => $HTML_D[$iter_html],
				"ATR" => $HTML_D[$iter_html + 1],
				"THIS" => $HTML_D[$iter_html + 2],
			);
		}
	}

	if (!(file_exists($file_cache) && date("d", filemtime($file_cache)) == date("d")))
	{
		$this->AbortResultCache();
	}

	$this->IncludeComponentTemplate();
}
