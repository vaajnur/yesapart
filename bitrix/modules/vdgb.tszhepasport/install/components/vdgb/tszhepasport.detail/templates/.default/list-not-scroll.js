//
//	���� ��������� ������
//
var h_hght = 0; // height of the header
var h_mrg = 0;    // margin when header is already hidden
var result = []; // massive of objects, items of menu
var offset_items = []; // massive of offsets of items of menu
var home_addr_hght = 60; // height of the adress div
var h_doc = 0;
$(function () {
    var elem = $('#menu-area');
    var atop = $(this).scrollTop(); //scroll of the window
    $(window).scroll(function () {
        atop = $(this).scrollTop();
        if ((atop + h_mrg > h_hght)) {
            if (atop + home_addr_hght + $("#border-left").height() > h_doc) {
                elem.css("top", h_mrg - ((atop + home_addr_hght + $("#border-left").height()) - h_doc));
            }
            else {
                elem.css("top", h_mrg);
            }
        } else {
            elem.css("top", h_hght - atop);
        }
        if (atop > 0) { // ������������ ���������
            var min = result[0];
            var image = $('#image-focused');
            var iter = 0;
            result.forEach(function (res) {
                if (atop > res.offset - home_addr_hght) {
                    min = res;
                }
            });
            if (atop + $(window).height() + 1 >= $(document).height()) { //���������
                min = result[result.length - 1];
                //console.log($(window).scrollTop(), $(window).height() +1, $(document).height()); //document.body.clientHeight + 1
            }
            $("#menu-area div").removeClass("main"); //change color of all elements in menu
            $("#menu-area div:contains(" + min.html + ")").addClass("main"); // change color of selected element in menu
            image.css('top', offset_items[min.index] - image.height() / 2 + "px"); // move scroller to the element in menu
            image.html("<img src='" + componentPath + "/images/menu-icons/0.png'>");
        }
    });
});
$(document).ready(function () { //���������� � result ������ {�����;����}
    h_hght = $('#header').height() + $('#top-links').height() / 2 + $('.top-links').height() + $('#bx-panel').height();// ������ �����
    var iter = 0;
    h_doc = $(document).height() - $("#footer").outerHeight(true) - $("#bottom-menu").outerHeight(true);
    $(".home-adress a").after(" " + ((arResult.EP_ADDRESS == "" || (parseInt(arResult.EP_ADDRESS) == 0) || arResult.EP_ADDRESS == null) ? BX.message("DATA_NOT_SET") : arResult.EP_ADDRESS));
    $(".home-adress a img").attr("src", componentPath + "/images/arrow_back.png");
    $(".home-adress a img").addClass("main-bg");
    var home_addr_width = 0
    if ($("#top-links").length > 0) {
        home_addr_width = parseInt($("#top-links").width(), 10)
    }
    else {
        home_addr_width = parseInt($(".top_menu").width(), 10);
    }
    console.log(home_addr_width);
    $(".home-adress").css("width", home_addr_width);
    //home_addr_hght = $(".home-adress")[0].clientHeight;
    $(".subsection").addClass("main-border");
    $("#work h2").addClass('main-border');
    //$("#work #pagetitle").after(form_string2(arResult.ID.indexOf(home_id))); // insert main information of the house in the beginning of the page
    $('.sections h2').each(function () { // ��������� � ���� ������� � ������ h1,h2
        obj = {};
        obj.offset = $(this).offset().top;
        obj.html = $(this).html();
        obj.index = iter;
        result.push(obj);
        $('#menu-area').html($('#menu-area').html() + "<div id='menu_" + iter + "'>" + result[iter].html + "</div>"); //insert elements of menu
        $('#menu-area ' + '#menu_' + iter).addClass("menu-item");
        $('#menu-area ' + '#menu_' + iter).css("top", (home_addr_hght - ($('#menu_' + iter).height() / 2) + 30 + 80 * iter) + "px"); //move elements of menu in their positions
        offset_items.push((home_addr_hght) + 20 + 80 * iter); // add coordinates of elements menu to array
        iter++;
    });

    $('#menu-area').html($('#menu-area').html() + "<div id='image-focused' class='main-bg'></div><div id='border_left' class='main-bg'></div><div class='clearfix'></div>");
    $("#border_left").css("top", "62px");
    $("#border_left").css("height", (20 + 80 * result.length) + "px"); //height of left border
    $("#border_left").css("margin-bottom", $("#footer").outerHeight(true) + $("#bottom-menu").outerHeight(true))
    for (var iter1 = 0; iter1 < $("#border_left").height(); iter1 = iter1 + 20) { //form left border
        if (offset_items.indexOf(iter1 + home_addr_hght) == -1) {
            $("#border_left").html($("#border_left").html() + "<img class='border_image main-bg' src='" + componentPath + "/images/menu-icons/not_used_item.png'>");
        } else {
            $("#border_left").html($("#border_left").html() + "<img class='border_image main-bg' src='" + componentPath + "/images/menu-icons/used_item.png'>");
        }
    }
    ;
    $('#menu-area').css("position", "fixed");
    $("#menu-area").css("top", h_hght);
    $("#menu-area #menu_0").addClass("main");
    $("#work").css("padding-left", '0');
    var image = $('#image-focused');
    image.html("<img src='" + componentPath + "/images/menu-icons/0.png'>");
    image.css('top', offset_items[0] - image.height() / 2 + "px");
    work_margin = $(window).height() - $("#footer").outerHeight(true) - $("#bottom-menu").outerHeight(true);
    var element = $("#work").children().last();
    work_margin = work_margin - element.outerHeight(true);
    element = element.prev();
    work_margin = work_margin - element.outerHeight(true);
    work_margin = Math.max(work_margin, $("#border_left").height());
    $("#work").css("margin-bottom", work_margin); //set margin-bottom for #work_area# to
    $('.menu-item').click(function () {
        $('#image-focused').html("<img src='" + componentPath + "/images/menu-icons/0.png'>");
        $("body, html").animate({scrollTop: result[($(this).attr('id')).split('_')[1]].offset - home_addr_hght + 1}, '500', 'swing');
    });
});

