<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

CJSCore::Init(array('jquery'));
if ($_GET['home-id']): ?>
	<script type="text/javascript">
        var arResult = <?=\Bitrix\Main\Web\Json::encode($arResult); ?>;
        var home_id = <?=\Bitrix\Main\Web\Json::encode($arResult["ID"]); ?>;
        var componentPath = <?=\Bitrix\Main\Web\Json::encode($componentPath); ?>;
        var templatePath = "<?=$this->GetFolder()?>";
        var arMapControls = <?=\Bitrix\Main\Web\Json::encode($arParams["MAPCONTROLS"]); ?>;
        var points = arCoordinates = <?=\Bitrix\Main\Web\Json::encode(array(
			$arResult['EP_LONGITUDE'],
			$arResult['EP_LATITUDE'],
		)); ?>;
	</script>

	<div id='menu-area'>
		<div class="home-adress main-border">
			<a href="/epassports/"><img></a>
		</div>

	</div>
	<div class="sections">
		<div class='subsection'>
			<table>
				<tr>
					<td>
						<img class='house-image'
						     src='<? echo ($arResult['EP_HOUSE_IMAGE_SRC'] != null) ? $arResult['EP_HOUSE_IMAGE_SRC'] : $componentPath . "/images/home_default_image.png"; ?>'>
					</td>
					<td style='vertical-align: top'>
						<h2 style="margin-top: 20px"><b><?=GetMessage("MAIN_HOUSE_INFORMATION")?></b></h2>
						<?
						foreach ($arResult["TABLE"] as $homeField => $homeValue): ?>
							<div class='item-name' style="border-bottom: 1px solid">
								<?=GetMessage($homeField)?>
							</div>
							<div class='item' style="border-bottom: 1px solid">
								&nbsp;&nbsp;<?=strlen($homeValue) > 0 ? $homeValue : GetMessage("EP_NO_INFORMATION")?>
							</div>
						<? endforeach; ?>
					</td>
				</tr>
			</table>
		</div>
		<?

		$pos = strripos($_SERVER['REQUEST_URI'], '?');
		$rest = substr($_SERVER['REQUEST_URI'], 0, $pos);
		?>
		<? $APPLICATION->AddHeadScript($componentPath . "/templates/.default/list-not-scroll.js") ?>
		<? foreach ($arResult["HTML"] as $arHtmlBlock): ?>
			<h2 id="<?=$arHtmlBlock["ATR"]?>"><?=$arHtmlBlock["NAME"]?></h2>
			<?=$arHtmlBlock["THIS"]?>
		<? endforeach ?>
	</div>
<? endif ?>

