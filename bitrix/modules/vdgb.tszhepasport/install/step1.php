<?if(!check_bitrix_sessid()) return;?>
<form action="<? echo $APPLICATION->GetCurPage() ?>" name="form1">
    <? echo bitrix_sessid_post();?>
    <input type="hidden" name="lang" value="<? echo LANG ?>">
    <input type="hidden" name="id" value="vdgb.tszhepasport">
    <input type="hidden" name="install" value="Y">
    <input type="hidden" name="step" value="2">
    <input type="hidden" name="partner_modules" value="1">
    <?=GetMessage("MOD_DESCRIPTION1")?><br>
    <img style="width: 50%" src="/images/map.png"><br>
    <?=GetMessage("MOD_DESCRIPTION2")?><br>
    <ul>
        <li><i><?=GetMessage("MOD_DESCRIPTION3")?></i></li>
        <br>
        <img style="width: 50%" src="/images/detail.png"><br>
        <br>
        <li><i><?=GetMessage("MOD_DESCRIPTION4")?></i></li>
        <br>
        <img style="width: 50%" src="/images/main-info.png"><br>
        <br>
        <li><i><?=GetMessage("MOD_DESCRIPTION5")?></i></li>
        <br>
        <img style="width: 50%" src="/images/files.png"><br>
    </ul>
    <?=GetMessage("MOD_DESCRIPTION6")?>
    <br><br>
    <img style="width: 50%" src="/images/upload_photo.png"><br>
    <br>
    <a target="_blank" href="https://www.vdgb-soft.ru/faq/faq_site_gkh/ustanovka_modulya_doma_v_upravlenii/"><?=GetMessage("MOD_DETAIL_INSTRUCTION")?></a>
    <br><br>
    <input type="submit" name="inst" value="<?=GetMessage("MOD_CONTINUE")?>">
</form>
