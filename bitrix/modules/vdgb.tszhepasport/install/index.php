<?
/**
 * ������ ����������� ��������� ���ƻ
 * ���������/�������� ������
 * @package tszh
 */
use Bitrix\Main\Localization\Loc;

IncludeModuleLangFile(__FILE__);
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/install/wizard_sol/utils.php");
if (class_exists("vdgb_tszhepasport"))
{
	return;
}

Class vdgb_tszhepasport extends CModule
{
	var $MODULE_ID = "vdgb.tszhepasport"; // ID ������
	var $MODULE_VERSION; // ������ ������
	var $MODULE_VERSION_DATE; // ���� ���������� ������
	var $MODULE_NAME; // �������� ������
	var $MODULE_DESCRIPTION; // �������� ������
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";

    public $params = array();

	function vdgb_tszhepasport()
	{
		$arModuleVersion = array();

		include(dirname(__FILE__) . "/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else
		{
			$this->MODULE_VERSION = "1.0.15";
			$this->MODULE_VERSION_DATE = "2018-01-15 17:00:00";
		}

		$this->MODULE_NAME = GetMessage("C_TSZH_EPASPORT_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("C_TSZH_EPASPORT_MODULE_DESCRIPTION");

		$this->PARTNER_NAME = GetMessage("C_MODULE_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("C_MODULE_PARTNER_URI");
	}

    private static function &_GetModuleObject($moduleID)
    {
        if(!class_exists('CModule'))
        {
            global $DB, $DBType, $DBHost, $DBLogin, $DBPassword, $DBName, $DBDebug, $DBDebugToFile, $APPLICATION, $USER, $DBSQLServerType;
            require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include.php");
        }

        $installFile = $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$moduleID."/install/index.php";
        if (!file_exists($installFile))
        {
            return false;
        }
        include_once($installFile);

        $moduleIDTmp = str_replace(".", "_", $moduleID);
        if (!class_exists($moduleIDTmp))
        {
            return false;
        }

        return new $moduleIDTmp;
    }

    private static function LoadAndInstallModule($selectedModule)
    {
        require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/classes/general/update_client_partner.php');

        $obModule =& self::_GetModuleObject($selectedModule);
        // already downloaded?
        if (!is_object($obModule))
        {
            if (!CUpdateClientPartner::LoadModuleNoDemand($selectedModule, $strError, $bStable = "N", LANGUAGE_ID))
            {
                throw new Exception(GetMessage("TSZH_ERROR_MODULE_LOAD", Array("#ERROR#" => $strError)));
            }
        }

        if (!IsModuleInstalled($selectedModule))
        {
            $module =& self::_GetModuleObject($selectedModule);
            if (!is_object($module))
            {
                throw new Exception(GetMessage("TSZH_ERROR_MODULE_OBJECT"));
            }
            $module->DoInstall();

        }
    }

    private static function LoadModules($arModules)
    {
        @set_time_limit(0);
        foreach ($arModules as $strModule)
        {
            try
            {
                self::LoadAndInstallModule($strModule);
            }
            catch (Exception $e)
            {
                ShowError(GetMessage("TSZH_ERROR_INSTALLING_MODULE", Array("#MODULE_ID#" => $strModule)) . $e->getMessage());
            }
        }
    }

    protected function setParams($arParams)
    {
        $this->params = $arParams;

        return $this;
    }

    protected function getParams($method)
    {
        return array_key_exists($method, $this->params) ? $this->params[$method] : array();
    }

	// �������� ������ ������ � ���� ������ � ������������� ������ � ��������
	function InstallDB()
	{
		global $DB, $APPLICATION;
		$this->errors = false;

		// �������� ������ � ��, ���� ��� ��� �� ����������
		if (!$DB->Query("SELECT 'x' FROM b_tszh_epasports WHERE 1=0", true))
		{
			$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/db/mysql/install.sql");
		}

		// ���� ��������� ������ - ������� ��
		if ($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("", $this->errors));

			return false;
		}
		// ����� - �������������� ������ � ������� �������
		else
		{
			RegisterModule($this->MODULE_ID);
			return true;
		}
	}

	// �������� ������ ������ �� �� � ������ ����������� ������
	function UnInstallDB()
	{
		global $DB;

        $params = $this->getParams(__FUNCTION__);

		// ������ �����������
		UnRegisterModule($this->MODULE_ID);

		// ������ ������� ������ �� ��, ���� �� �������� �������� ������ �� ���� �������� ������� ���������� ��������
		if (!$params['save_tables'])
		{
			$DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/db/mysql/uninstall.sql");
		}

		return true;
	}

	// ������� ������ ������������� ���������� ������� (���)�, ���� ��� �� ���� ������� �����
	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	// ����������� ������ ������

	function InstallFiles()
	{
        $arParams = $this->getParams(__FUNCTION__);

		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/themes/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/themes/", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/images/", $_SERVER["DOCUMENT_ROOT"] . "/images/", true, true);

        $bReWriteAdditionalFiles = ($arParams["public_rewrite"] == "Y");

        if (tszhCheckMinEdition('smallbusiness'))	{
            CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/public/ru/epassports", $_SERVER["DOCUMENT_ROOT"] . "/epassports", true, true);
            \CAdminNotify::DeleteByTag("add_epassports");
        }
		$bReWriteAdditionalFiles = ($arParams["public_rewrite"] == "Y");

		return true;
	}

	// �������� ������ ������
	function UnInstallFiles()
	{
        $params = $this->getParams(__FUNCTION__);

		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components");
		if (!$params['save_tables'])
		{
			$bDeleted = DeleteDirFilesEx("/epassports/");
		}
        \CAdminNotify::DeleteByTag("add_epassports");
        \CAdminNotify::DeleteByTag("incorrect_coordinates_epassports");
		DeleteDirFilesEx("/bitrix/themes/.default/icons/{$this->MODULE_ID}/"); //icons

		return true;
	}


	function DoInstall()
	{
		if (!check_bitrix_sessid())
		{
			return false;
		}
        $arInstallModules = Array(
            'vdgb.documents'
        );
		global $APPLICATION, $step, $partner_modules;
		$step = IntVal($step);
        $partner_modules = IntVal($partner_modules);
        if (($partner_modules == 1) || ($step == 0))
		{
            if ($step < 2)
            {
                $APPLICATION->IncludeAdminFile(GetMessage("TSZH_EPASPORT_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/step1.php");
            }
			elseif ($step == 2)
            {
                $APPLICATION->IncludeAdminFile(GetMessage("TSZH_EPASPORT_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/step2.php");
            }
			elseif ($step == 3)
            {
                self::LoadModules($arInstallModules);
                if ($this->InstallDB())
                {
                    $this->setParams(array(
                        'InstallFiles' => array(
                            "public_dir" => $_REQUEST["public_dir"],
                            "public_rewrite" => $_REQUEST["public_rewrite"],
                        )
                    ));
                    $this->InstallEvents();
                    $this->InstallFiles();
                };

				$GLOBALS["errors"] = $this->errors;
                $APPLICATION->IncludeAdminFile(GetMessage("TSZH_EPASPORT_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/step3.php");
            }
        }
        else
		{
            $Wizard = new WizardServices();
            WizardServices::AddMenuItem("/documents/.show_add.menu.php", Array(
                Loc::GetMessage("C_TSZH_EPASPORT_MENU_NAME"),
                "/epassports/",
                Array(),
                Array(),
                ""
            ),false,-1);
            $arInstallModules = Array(
                'vdgb.documents'
            );

            self::LoadModules($arInstallModules);
        }

	}

	// �������� ������ (���������� ���������)
	function DoUninstall()
	{
		global $APPLICATION;

		if (!check_bitrix_sessid())
		{
			return false;
		}

		$step = IntVal($_REQUEST['step']);
		if ($step < 2)
		{
			$APPLICATION->IncludeAdminFile(GetMessage("TSZH_EPASPORT_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/uninst1.php");
		}
		elseif ($step == 2)
		{
            $this->setParams(array(
                'UnInstallDB' => array(
                    "save_tables" => IntVal($_REQUEST["save_tables"]),
                ),
                'UnInstallFiles' => array(
                    "save_tables" => IntVal($_REQUEST["save_public_files"]),
                )
            ));
			$this->UnInstallDB();
			$this->UnInstallFiles();
			$this->UnInstallEvents();
			$GLOBALS["errors"] = $this->errors;
			$APPLICATION->IncludeAdminFile(GetMessage("TSZH_EPASPORT_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/uninst2.php");
		}

	}
}
?>
