
<?
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/install/wizard_sol/utils.php");
if(!check_bitrix_sessid()) return;
IncludeModuleLangFile(__FILE__);

if($ex = $APPLICATION->GetException())
	echo CAdminMessage::ShowMessage(Array(
		"TYPE" => "ERROR",
		"MESSAGE" => GetMessage("MOD_INST_ERR"),
		"DETAILS" => $ex->GetString(),
		"HTML" => true,
	));
else
	echo CAdminMessage::ShowNote(GetMessage("MOD_INST_OK"));

?>

<?
if ($_REQUEST['add_menu'] == 'Y') {
    $arMenu = $APPLICATION->GetMenu(
        "top"
    )->arMenu;
    $menuTree = array();

    foreach($arMenu as $idx => $IndMenuOptions)
    {
        $tmpMenu = $APPLICATION->GetMenu("section",false,false,$IndMenuOptions[1])->arMenu;
        $newMenu = array();
        $newMenu["name"] = $IndMenuOptions[0];
        $newMenu["dir"] = $IndMenuOptions[1];
        $newMenu["menu"] = $tmpMenu;
        $menuTree[] = $newMenu;
    };
    $Wizard = new WizardServices();
    if (is_numeric($_REQUEST["add_section"]))
    {
        $menuFile = "/".$menuTree[$_REQUEST["add_section"]]["dir"].".show_add.menu.php";
        WizardServices::AddMenuItem($menuFile, Array(
            $_REQUEST["menu_item_name"],
            $_REQUEST["public_dir"],
            Array(),
            Array(),
            ""
        ),false,is_numeric($_REQUEST["add_menu_item"])? $_REQUEST["add_menu_item"] : -1);
    }
    else
    {
        WizardServices::AddMenuItem("/.top.menu.php", Array(
            $_REQUEST["menu_item_name"],
            $_REQUEST["public_dir"],
            Array(),
            Array(),
            ""
        ),false,is_numeric($_REQUEST["add_menu_item"])? $_REQUEST["add_menu_item"] : count($menuTree));
    }
}
?>
<form action="<?echo $APPLICATION->GetCurPage()?>">
    <? echo bitrix_sessid_post();?>
	<input type="hidden" name="lang" value="<?echo LANG?>">
    <input type="hidden" name="id" value="vdgb.tszhepasport">
    <input type="hidden" name="install" value="Y">
    <input type="hidden" name="step" value="4">
    <input type="hidden" name="partner_modules" value="1">
	<input type="submit" name="" value="<?echo GetMessage("MOD_BACK")?>">
</form>
