<? if(!check_bitrix_sessid()) return;?>
<form action="<? echo $APPLICATION->GetCurPage() ?>" name="form2">
	<? echo bitrix_sessid_post();?>
	<input type="hidden" name="lang" value="<? echo LANG ?>">
	<input type="hidden" name="id" value="vdgb.tszhepasport">
	<input type="hidden" name="install" value="Y">
	<input type="hidden" name="step" value="3">
    <input type="hidden" name="partner_modules" value="1">
    <?
    $arMenu = $APPLICATION->GetMenu(
        "top"
    )->arMenu;
    $menuTree = array();
    foreach($arMenu as $idx => $IndMenuOptions)
    {
        $tmpMenu = $APPLICATION->GetMenu("section",false,false,$IndMenuOptions[1])->arMenu;
        $newMenu = array();
        $newMenu["name"] = $IndMenuOptions[0];
        $newMenu["dir"] = $IndMenuOptions[1];
        $newMenu["menu"] = $tmpMenu;
        $menuTree[] = $newMenu;
    }
    $men = $menuTree;
    ?>
	<table cellpadding="3" cellspacing="0" border="0" width="0%">
		<tr>
			<td>
				<input type="checkbox" name="install_public" value="Y" id="id_install_public" OnClick="ChangeInstallPublic(this.checked)">
			</td>
			<td>
				<p>
					<label for="id_install_public"><?=GetMessage("COPY_FILES")?></label>
				</p>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table cellpadding="3" cellspacing="0" border="0" width="0%">
					<tr>
						<td>
							<p>
								<?=GetMessage("COPY_FOLDER")?>
							</p>
						</td>
						<td>
							<input type="input" name="public_dir" value="/epassports/" size="40" onchange="ChangeOption()">
						</td>
					</tr>
					<tr>
						<td>
							<p>
								<label for="id_public_rewrite"><?=GetMessage("INSTALL_PUBLIC_REW")?>:</label>
							</p>
						</td>
						<td>
							<input type="checkbox" name="public_rewrite" value="Y" id="id_public_rewrite">
						</td>
					</tr>
                    <tr>
                        <td>
                            <p>
                                <label for="id_add_menu"><?=GetMessage("VDGB_TSZHEPASSPORT_ADD_MENU_ITEM")?>:</label>
                            </p>
                        </td>
                        <td>
                            <input type="checkbox" name="add_menu" value="Y" id="id_add_menu" OnClick="ChangeInstallMenuItem(this.checked)" checked>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label for="id_menu_item_name"><?=GetMessage("VDGB_TSZHEPASSPORT_MENU_ITEM_NAME")?>:</label>
                            </p>
                        </td>
                        <td>
                            <input type="input" name="menu_item_name" id="id_menu_item_name" value="<?=GetMessage("VDGB_TSZHEPASSPORT_EPD_HOUSES")?>" size="40">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label for="id_add_section"><?=GetMessage("VDGB_TSZHEPASSPORT_ADD_SECTION")?>:</label>
                            </p>
                        </td>
                        <td>
                            <select name="add_section" value="Y" id="id_add_section" OnChange="FormMenuItems();">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label for="id_add_menu_item"><?=GetMessage("VDGB_TSZHEPASSPORT_USE_MENU_ITEM")?>:</label>
                            </p>
                        </td>
                        <td>
                            <select name="add_menu_item" value="Y" id="id_add_menu_item">
                            </select>
                        </td>
                    </tr>
                </table>
			</td>
		</tr>
	</table>
    <script language="JavaScript">
        var arAllMenu = <? echo $value = \Bitrix\Main\Web\Json::encode($menuTree);?>;
        function ChangeInstallPublic(val) {
            document.form2.public_dir.disabled = !val;
            document.form2.public_rewrite.disabled = !val;
            document.form2.add_menu.disabled = !val;
            document.form2.add_section.disabled = !val;
            document.form2.add_menu_item.disabled = !val;
            document.form2.menu_item_name.disabled = !val;
        }
        function ChangeInstallMenuItem(val) {
            document.form2.add_section.disabled = !val;
            document.form2.add_menu_item.disabled = !val;
            document.form2.menu_item_name.disabled = !val;
        }
        function InsertItemOptions(itemOption, indexOption, element) {
            document.form2.add_menu_item.options[document.form2.add_menu_item.options.length] = new Option(itemOption[0], indexOption);
        }

        function InitOptions(){
            document.form2.add_section.options[document.form2.add_section.options.length] = new Option("<?=GetMessage('VDGB_TSZHEPASSPORT_NEW_SECTION')?>",document.form2.public_dir.value);
            if (document.form2.add_section.options.selectedIndex == 0) {
                arAllMenu.forEach(InsertSectionItems);
                arAllMenu.forEach(InsertSectionItemsToSections);
            }
            else {
                arAllMenu[document.form2.add_section.options.selectedIndex].menu.forEach(InsertItemOptions);
            }
            document.form2.add_menu_item.options[document.form2.add_menu_item.options.length] = new Option("<?=GetMessage("VDGB_TSZHEPASSPORT_NEW_MENU_ITEM")?>",document.form2.public_dir.value);
        }
        function ChangeOption(){
            document.form2.add_section.remove(document.form2.add_section.options.length - 1);
            document.form2.add_section.options[document.form2.add_section.options.length] = new Option("<?=GetMessage("VDGB_TSZHEPASSPORT_NEW_SECTION")?>",document.form2.public_dir.value);
        }
        function InsertSectionItemsToSections(itemSection,indexSection,element){
            document.form2.add_section.options[document.form2.add_section.options.length] = new Option(itemSection.name, indexSection);
        }
        function InsertSectionItems(itemSectionItem,indexSectionItem,element){
            document.form2.add_menu_item.options[document.form2.add_menu_item.options.length] = new Option(itemSectionItem.name, indexSectionItem);
        }
        function FormMenuItems(){
            document.form2.add_menu_item.innerHTML = "";
            document.form2.add_menu_item.options[document.form2.add_menu_item.options.length] = new Option("<?=GetMessage("VDGB_TSZHEPASSPORT_NEW_MENU_ITEM")?>",document.form2.public_dir.value);
            if (document.form2.add_section.options.selectedIndex == 0) {
                arAllMenu.forEach(InsertSectionItems);
            }
            else {
                arAllMenu[document.form2.add_section.options.selectedIndex-1].menu.forEach(InsertItemOptions);
            }
        }
        ChangeInstallPublic(false);
        ChangeInstallMenuItem(false);
        InitOptions();
    </script>
	<br>
	<input type="submit" name="inst" value="<?=GetMessage("MOD_INSTALL")?>">
</form>
