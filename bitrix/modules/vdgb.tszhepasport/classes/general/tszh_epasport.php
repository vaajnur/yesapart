<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

IncludeModuleLangFile(__FILE__);

class CTszhEPasport {

	// �������� ������ � ��������� ��������, ���� ������������ ���������������� ���� �������� ������; false, ���� �� ������������
	const USER_FIELD_ENTITY = 'TSZH_EPASPORT';


	/**
	 * ��������� ������ �� ������ � ������ �������
	 * @param CApplicationException|string $message
	 * @param bool $ID
	 */
	protected static function logError($message, $ID = false)
	{
		CEventLog::Add(Array(
			"SEVERITY" => "ERROR",
			"AUDIT_TYPE_ID" => "EPASPORT",
			"MODULE_ID" => "vdgb.tszhepasport",
			"ITEM_ID" => $ID,
			"DESCRIPTION" => nl2br(is_object($message) ? $message->GetString() : $message),
		));
	}
	
	/**
	 * ���������� �������� �������� �������� � ���� ������ citrus.tszh
	 * @param array $aGlobalMenu
	 * @param array $aModuleMenu
	 */
	public static function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
	{
	}
	
	/**
	 * ���������� ��� ���������� � ��������� ������� ���������� � ������ citrus.tszh 
	 * @param int $ID ID ������� ���������� (CTszh)
	 * @param array $arFields ���� ������� ����������
	 * @return bool
	 */
	public static function OnAfterTszhAdd($ID, $arFields)
	{
		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		/// ...
		return true;
	}

	/**
	 * ���������� ����� �������� ������� ���������� 
	 * @param int $ID ID ������� ����������
	 * @return bool
	 */
	public static function OnAfterTszhDelete($ID)
	{
		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		/// ...
		return true;
	}

}

?>
