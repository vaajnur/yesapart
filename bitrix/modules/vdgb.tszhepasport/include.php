<?
global $DBType;
if ($GLOBALS['DBType'] != 'mysql') { return false; }

$tszhDBType = 'general';

define("TSZH_EPASPORT_MODULE_ID", 'vdgb.tszhepasport');


if (!CModule::IncludeModule("citrus.tszh")) {
	return false;
}

// ���������� �������� ������
IncludeModuleLangFile(__file__);

CJSCore::RegisterExt('ymaps_js', array(
	'js' => '/bitrix/components/vdgb/tszhepasport.all/templates/.default/ymaps.js',
	'lang' => '/bitrix/components/vdgb/tszhepasport.all/templates/.default/lang/'.LANGUAGE_ID.'/template.php'
));


CModule::AddAutoloadClasses(
	TSZH_EPASPORT_MODULE_ID,
	array(
		"CTszhEPasport" => "classes/".$tszhDBType."/tszh_epasport.php",
	)
);

//include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhepasport/functions.php");


/**
 * ����� ������� ��� ���������������� �����
 *
 * @param array $arFields ������ � ��������� �����
 * @return array
 */
function TszhEpAdminFilterParams($arFields)
{
	$ar = Array("find", "find_types");
	foreach ($arFields as $arField)
	{
		switch ($arField['type'])
		{
			case 'int':
			case 'float':
			case 'date':
			case 'datetime':
				$ar[] = 'find_' . strtolower($arField['id']) . '_from';
				$ar[] = 'find_' . strtolower($arField['id']) . '_to';
				break;
			case 'string':
			default:
				$ar[] = 'find_' . strtolower($arField['id']);
				break;
		}
	}
	return $ar;
}

/**
 * ����� ������� ��� ���������������� �����
 *
 * @param array $arFindType ������ � ��������� �����
 * @return void
 */
function TszhEpShowShowAdminFilter($arFields)
{
	$arFindTypes = Array("ID" => "ID");
	?>
	<tr>
		<td><b><?=GetMessage("TSZH_FIND")?>:</b></td>
		<td>
			<input type="text" size="25" name="find" value="<?echo htmlspecialcharsbx($GLOBALS['find'])?>" title="<?=GetMessage("TSZH_FIND_TITLE")?>">
			<?
			$arSelectBox = Array();
			foreach ($arFindTypes as $id => $val)
			{
				$arSelectBox["REFERENCE"][] = $val;
				$arSelectBox["REFERENCE_ID"][] = $id;
			}
			echo SelectBoxFromArray("find_types", $arSelectBox, $GLOBALS['find_types'], "", "");
			?>
		</td>
	</tr>
	<?
	foreach ($arFields as $arField)
	{
		$fieldName = 'find_' . strtolower($arField['id']);
		?>
		<tr>
			<td><?=$arField['content']?></td>
			<td>
				<?
				if (is_set($arField['filterHtml']))
					echo $arField['filterHtml'];
				else
					switch ($arField['type'])
					{
						case 'account':
						case 'service':
							echo tszhLookup($fieldName, htmlspecialcharsbx($GLOBALS[$fieldName]), Array("type" => $arField["type"], "formName" => 'form_filter'));
							break;

						case 'user':
							echo FindUserID($fieldName, htmlspecialcharsbx($GLOBALS[$fieldName]), "", "form_filter");
							break;

						case 'int':
						case 'float':
							?>
							<?=GetMessage("TSZH_FILTER_FROM")?>
							<input type="text" name="<?=$fieldName?>_from" size="10" value="<?echo htmlspecialcharsbx($GLOBALS[$fieldName . '_from'])?>">
							<?=GetMessage("TSZH_FILTER_TO")?>
							<input type="text" name="<?=$fieldName?>_to" size="10" value="<?echo htmlspecialcharsbx($GLOBALS[$fieldName . '_to'])?>">
							<?
							break;

						case 'list':
							$arItems = Array("REFERENCE_ID" => Array(''), "REFERENCE" => Array(GetMessage("TSZH_FILTER_LIST_ALL")));
							if (is_array($arField['items']))
							{
								foreach ($arField['items'] as $id => $val)
								{
									$arItems["REFERENCE_ID"][] = $id;
									$arItems["REFERENCE"][] = $val;
								}
							}
							elseif (is_object($arField['items']))
							{
								while ($ar = $arField['items']->GetNext())
								{
									$arItems["REFERENCE_ID"][] = $ar['ID'];
									$arItems["REFERENCE"][] = '[' . $ar['ID'] . '] ' . $ar['NAME'];
								}
							}
							echo SelectBoxFromArray($fieldName, $arItems, $GLOBALS[$fieldName], "", "");
							?>

							<?
							break;

						case 'checkbox':
							$arItems = Array(
								"REFERENCE_ID" => Array('', 'Y', "N"),
								"REFERENCE" => Array(GetMessage("TSZH_FILTER_LIST_ALL"), GetMessage("TSZH_YES"), GetMessage("TSZH_NO"))
							);
							echo SelectBoxFromArray($fieldName, $arItems, $GLOBALS[$fieldName], "", "");
							?>

							<?
							break;

						case 'date':
						case 'datetime':
							echo CalendarPeriod($fieldName . '_from', htmlspecialcharsbx($GLOBALS[$fieldName . '_from']), $fieldName . '_to', htmlspecialcharsbx($GLOBALS[$fieldName . '_to']), "form_filter","Y");
							break;

						case 'string':
						default:
							?>
								<input type="text" name="find_<?=strtolower($arField['id'])?>" size="40" value="<?echo htmlspecialchars($GLOBALS['find_' . strtolower($arField['id'])])?>">
							<?
							break;
					}
				?>
			</td>
		</tr>
	<?
	}
}

/**
 * ������������ ���������� ������� ��� ���������������� �����
 *
 * @param array $arFilter �������������� ������ � ��������
 * @param array $arFields ������ � ��������� �����
 * @return void
 */
function TszhEpAdminMakeFilter(&$arFilter, $arFields)
{
	global $DB;

	if ($GLOBALS['find']!="" && ToLower($GLOBALS['find_types']) == "id")
	{
		if (IntVal($GLOBALS['find']) > 0)
			$arFilter["ID"] = IntVal($GLOBALS['find']);
		elseif (IntVal($GLOBALS['find_id']) > 0)
			$arFilter["ID"] = IntVal($GLOBALS['find_id']);
	}
	else
	{
		if(IntVal($GLOBALS['find_id']) > 0)
			$arFilter["ID"] = IntVal($GLOBALS['find_id']);
	}

	foreach ($arFields as $arField)
	{
		$varName = 'find_' . strtolower($arField['id']);
		$field = $arField['id'];

		switch ($arField['type'])
		{
			case 'int':
			case 'float':
				$val = &$GLOBALS[$varName];
				if (isset($val) && strlen($val) > 0 && is_numeric($val))
					$arFilter[$field] = $arField['type'] == 'float' ? FloatVal($val) : IntVal($val);
				else
				{
					$val = &$GLOBALS[$varName . '_from'];
					if (isset($val) && strlen($val) > 0 && is_numeric($val))
						$arFilter['>=' . $field] = $arField['type'] == 'float' ? FloatVal($val) : IntVal($val);
					$val = &$GLOBALS[$varName . '_to'];
					if (isset($val) && strlen($val) > 0 && is_numeric($val))
						$arFilter['<=' . $field] = $arField['type'] == 'float' ? FloatVal($val) : IntVal($val);
				}
				break;

			case 'date':
			case 'datetime':
				$bFullDate = $arField['type'] == 'datetime';
				$val = &$GLOBALS[$varName . '_from'];
				if (isset($val) && strlen($val) > 0 && $DB->IsDate($val, false, false, $bFullDate ? "FULL" : "SHORT"))
					$arFilter['>=' . $field] = $val;
				$val = &$GLOBALS[$varName . '_to'];
				if (isset($val) && strlen($val) > 0 && $DB->IsDate($val, false, false, $bFullDate ? "FULL" : "SHORT"))
					$arFilter['<=' . $field] = $val;

				$val = &$GLOBALS[$varName . '_from_DAYS_TO_BACK'];
				if (isset($val) && strlen($val) > 0 && is_numeric($val))
					$arFilter['>=' . $field] = ConvertTimestamp($val > 0 ? strtotime('-' . IntVal($val) . ' days') : time());
				break;

			case 'string':
				$val = &$GLOBALS[$varName];
				if (isset($val) && strlen(trim($val)) > 0)
					$arFilter['%' . $field] = $val;
				break;

			case 'list':
			case 'checkbox':
			default:
				$val = &$GLOBALS[$varName];
				if (isset($val) && strlen(trim($val)) > 0)
					$arFilter[$field] = $val;
				break;
		}
	}

}

?>
