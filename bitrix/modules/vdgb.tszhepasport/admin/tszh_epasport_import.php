<?
/**
 * ������ ���ƻ
 * �������� ������� ������� ������, ����������� ����� � ��������� ��������� �� 1�
 * @package tszh
 */

use Bitrix\Main\Localization\Loc;
use Vdgb\Tszhepasport\Exchange\v4\Formats\Houses;

define("NO_KEEP_STATISTIC", true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
    $c = ob_get_contents();
    ob_end_clean();
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    echo $c;
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

    return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/vdgb.tszhepasport/include.php"); // ������������� ������
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/vdgb.tszhepasport/prolog.php"); // ������ ������

//define("TSZH_IMPORT_DEBUG", true);

IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("vdgb.tszhepasport");

$APPLICATION->SetTitle(GetMessage("TI_TITLE"));
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

CUtil::InitJSCore(Array("ajax", "window"));
if (isset($_FILES['this_file']['tmp_name'])){
    \Bitrix\Main\Loader::includeModule('vdgb.tszhepasport');

    global $USER;
    // ���� ������������ �� �����������, ������� ����� �����������
    if (!$USER->IsAuthorized()) {
        $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
    }
    echo "<a href='/bitrix/admin/tszh_epasport_import.php'>&larr; ����� � ������� �����</a> <br /><br />";
    //$uploaddir = '/home/bitrix/www/upload/';
    $uploaddir = $_SERVER["DOCUMENT_ROOT"].'/upload/';
    $uploadfile = $uploaddir.'home_data.xml';
    if(move_uploaded_file($_FILES['this_file']['tmp_name'], $uploadfile)){
        echo "���� �������� � <a href='/upload/home_data.xml' target='_blank'>/upload/home_data.xml</a> <br /><br />";
    }
    $obXMLFile = new CIBlockXMLFile;
    // ������� ��������� ���������� ��������
    $obXMLFile->DropTemporaryTables();
    // �������������� ��
    if(!$obXMLFile->CreateTemporaryTables())
        echo "������ �������� ��.";

    //if($fp = fopen('/home/bitrix/www/upload/home_data.xml', "rb"))
    if($fp = fopen($_SERVER["DOCUMENT_ROOT"].'/upload/home_data.xml', "rb"))
    {
        $obXMLFile->ReadXMLToDatabase($fp, $NS, 0);
        if(file_exists($_SERVER["DOCUMENT_ROOT"].'/upload/home_coord2.txt')) unlink($_SERVER["DOCUMENT_ROOT"].'/upload/home_coord2.txt');		// �������� ���� ���������
        echo "<iframe src='/epassports/' width='0' height='0' style='display:block; overflow:hidden; width:0; height:0; border:none;'></iframe>"; // ����������� ��������� �� �����
        echo "������ ���������� ������� <br /><br />";
        echo "<a href='/epassports/'>������� � ������ ����� &rarr;</a>";
    }
    else
    {
        // ���� ������� �� �������
        echo "������ �������� �����";
    }
    $strSql_org = "
		SELECT
			NAME,ATTRIBUTES
		FROM
			b_xml_tree
		WHERE
			NAME = 'org'
		";
    $res_org = $DB->Query($strSql_org, false, $err_mess.__LINE__);		// �������� ����
    while($row = $res_org->Fetch()){
        $array_attr_org = unserialize($row['ATTRIBUTES']);
    }
    // ��������� ��� �� org � ��� �� ������ ���

    $res_inn = CTszh::GetList(array(),array(),false,false,array("ID","INN"));
    while($row = $res_inn->Fetch()){
        if($row['INN'] = $array_attr_org['inn'])
            $id_org = $row['ID'];									// ���� �������� �������, �� �������� TSZH_ID
    }

    //	��������� � ������ ������ �� �����
    $strSql = "
		SELECT
			NAME,ATTRIBUTES
		FROM
			b_xml_tree
		WHERE
			NAME = 'house'
		";
    $array_attr = array(); 																		// ������ � ���������
    $res = $DB->Query($strSql, false, $err_mess.__LINE__);										// �������� ����
    while($row = $res->Fetch()){
        $array_u = unserialize($row['ATTRIBUTES']);												// ������� ���������
        $res2 = Vdgb\Tszhepasport\EpasportTable::getList(array(
            'select' => array(
                'ID',
                'TSZH_ID'),
            'filter' => array('EXTERNAL_ID' => $array_u['id']),
        ));
        $array_u['type'] = ($array_u["type"] == (Loc::getMessage('EPAS_ENTITY_EP_TYPE_MKD'))) ? 'A' : 'P';
        if ($res2->getSelectedRowsCount() > 0){
            $res3 = Vdgb\Tszhepasport\EpasportTable::Update(
                $array_u['id'],
                array(
                    "TSZH_ID"           		 => $id_org,
                    "EXTERNAL_ID"           	 => $array_u['id'],
                    "EP_TYPE"                    => $array_u['type'],
                    "EP_ADDRESS"                 => $array_u['address'],
                    "EP_STREET"                  => $array_u['street'],
                    "EP_HOUSE"          		 => $array_u['house'],
                    "EP_COM_AREA"        		 => $array_u['com_area'],
                    "EP_FLOORS"     		     => $array_u['floors'],
                    "EP_YEAR_OF_COMMISSIONING"   => $array_u['year_of_commissioning'],
                    "EP_REG_PEOPLE"    			 => $array_u['reg_people'],

                    "EP_SERIES"    				 => $array_u['series'],
                    "EP_WEAR"    				 => $array_u['wear'],
                    "EP_PORCHES"    			 => $array_u['porches'],
                    "EP_LIVING_ROOMS"    		 => $array_u['living_rooms'],
                    "EP_NONLIVING_ROOMS"   		 => $array_u['nonliving_rooms'],
                    "EP_PEOPLE"    				 => $array_u['people'],
                    "EP_ACCOUNTS"    			 => $array_u['accounts'],
                    "EP_PEOPLE_ACCOUNTS"    	 => $array_u['people_accounts'],
                    "EP_COMPANY_ACCOUNTS"    	 => $array_u['company_accounts'],

                    "EP_YEAR_OF_BUILT"  		 => $array_u['year_of_built'],
                    "DATE_UPDATE"  				 => \Bitrix\Main\Type\DateTime::createFromTimestamp(time())
                )
            );
        }else{
            $res3 = Vdgb\Tszhepasport\EpasportTable::Add(
                array(
                    "TSZH_ID"           		 => $id_org,
                    "EXTERNAL_ID"           	 => $array_u['id'],
                    "EP_TYPE"                    => $array_u['type'],
                    "EP_ADDRESS"                 => $array_u['address'],
                    "EP_STREET"                  => $array_u['street'],
                    "EP_HOUSE"          		 => $array_u['house'],
                    "EP_COM_AREA"        		 => $array_u['com_area'],
                    "EP_FLOORS"     		     => $array_u['floors'],
                    "EP_YEAR_OF_COMMISSIONING"   => $array_u['year_of_commissioning'],
                    "EP_REG_PEOPLE"    			 => $array_u['reg_people'],

                    "EP_SERIES"    				 => $array_u['series'],
                    "EP_WEAR"    				 => $array_u['wear'],
                    "EP_PORCHES"    			 => $array_u['porches'],
                    "EP_LIVING_ROOMS"    		 => $array_u['living_rooms'],
                    "EP_NONLIVING_ROOMS"   		 => $array_u['nonliving_rooms'],
                    "EP_PEOPLE"    				 => $array_u['people'],
                    "EP_ACCOUNTS"    			 => $array_u['accounts'],
                    "EP_PEOPLE_ACCOUNTS"    	 => $array_u['people_accounts'],
                    "EP_COMPANY_ACCOUNTS"    	 => $array_u['company_accounts'],

                    "EP_YEAR_OF_BUILT"  		 => $array_u['year_of_built'],
                    "DATE_CREATE"  				 => \Bitrix\Main\Type\DateTime::createFromTimestamp(time()),
                    "DATE_UPDATE"  				 => \Bitrix\Main\Type\DateTime::createFromTimestamp(time())
                )
            );
        }
    }
    //
    //	������� ������ contents, ������ ��� ������ content, � ���� ����� ��������
    //
    $strSql_house_nom = "
		SELECT
			NAME
		FROM
			b_xml_tree
		";
    $res_house_nom = $DB->Query($strSql_house_nom, false, $err_mess.__LINE__);		// �������� ����
    $res_cont = $res_house_nom->SelectedRowsCount(); // ����� ������� � �������
    $i=0;
    $k=0;
    $m=0;
    $arr_contents = array();
    $arr_house = array();
    while($row = $res_house_nom->Fetch()){
        $i++;
        if($row['NAME']=='contents'){
            $arr_contents[$k]=$i;				// ���������� ����� ������ ����� contents
            $k++;
        }
        if($row['NAME']=='house'){
            $arr_house[$k]=$i;					// ���������� ����� ������ ����� house
            $m++;
        }
    }
    //
    // ��������� ���� content �� �����
    //
    $num_house = count($arr_contents);							// ����� �����
    for($kk=0;$kk<$num_house;$kk++){
        $last_id = $arr_house[$kk+1]; 							// ID ��������� ������ content
        if(!$arr_house[$kk+1]) $last_id = $res_cont+1; 			// ID ��������� ������ ����� ��������� ������ � �������

        $ep_html_house = array();
        $n_house = 0;

        for($ii=$arr_contents[$kk]+1;$ii<$last_id;$ii++){
            $strSql_contents = "
				SELECT
					ID,VALUE,ATTRIBUTES
				FROM
					b_xml_tree
				WHERE ID='".$ii."'
				";
            $res_contents = $DB->Query($strSql_contents, false, $err_mess.__LINE__);		// �������� ����

            $ep_html_house_atr = array();
            $n_attr = 0;
            while($row = $res_contents->Fetch()){
                $content = $row['VALUE'];
                $attr_count = unserialize($row['ATTRIBUTES']); // �������� �������� � content
                $ep_html_house_atr[$n_attr] .=  $attr_count['name'].' || '.$attr_count['type'].' || '.$content.' || '; // ������ ��� ������ � �������: ������� name, ������� type, � html-���. ��������� ��� ����: ' || '

                $ep_html_house[$kk] .= $ep_html_house_atr[0].$ep_html_house_atr[1].$ep_html_house_atr[2].$ep_html_house_atr[3].$ep_html_house_atr[4];			// ��������� ������ � ���������� � html ������
                $n_attr++;
            }
        }
        //
        //	���������� �������� � html ���� � ������� � ������
        //
        $strSql = "
					SELECT
						NAME,ATTRIBUTES
					FROM
						b_xml_tree
					WHERE
						NAME = 'house'
					";

        $array_house_id = array();
        $res22 = $DB->Query($strSql, false, $err_mess.__LINE__);		// �������� ����
        $iii=0;
        while($row = $res22->Fetch()){
            $array_u = unserialize($row['ATTRIBUTES']);
            $iii++;
            $array_house_id[$iii]= $array_u['id'];
        }

        $id_house = $array_house_id[$kk+1];		// �������� ID ����
        //���������� � �������
        $arFields = array(
            "EP_HTML"           	 => $ep_html_house[$kk]
        );
        //$DB->Update("b_tszh_epasports", $arFields, "WHERE EXTERNAL_ID='".$id_house."'", $err_mess.__LINE__);
        $res4 = Vdgb\Tszhepasport\EpasportTable::getList(array(
            'select' => array(
                'ID'),
            'filter' => array('EXTERNAL_ID' => $id_house),
        ));
        $row = $res4->Fetch();
        $res3 = Vdgb\Tszhepasport\EpasportTable::Update($row["ID"],$arFields);
    }
} else {
    $rsSites = CSite::GetList($by = "ID", $order = "asc");
    $arSites = array();
    /** @noinspection PhpAssignmentInConditionInspection */
    while ($arSite = $rsSites->GetNext()) {
        $arSites[$arSite["ID"]] = $arSite["NAME"];
    }
    ?>
    <div id="tszh_import_result_div"></div>
    <?
    $aTabs = array(
        array(
            "DIV" => "edit1",
            "TAB" => GetMessage("TI_TAB"),
            "ICON" => "main_user_edit",
            "TITLE" => GetMessage("TI_TAB_TITLE"),
        ),
    );
    $tabControl = new CAdminTabControl("tabControl", $aTabs);
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

    <hr>

    <div id="result"></div>

    <form method="POST" action="/bitrix/admin/tszh_epasport_import.php?clear_cache=Y" name="form1" id="upload"
          enctype="multipart/form-data">
        <?
        $tabControl->Begin();
        $tabControl->BeginNextTab();
        ?>

        <tr valign="top">
            <td width="40%"><span class="required">*</span><? echo GetMessage("TI_URL_DATA_FILE") ?>:</td>
            <td width="60%">
                <!--input type="file" id="this_file" name="this_file" value="���������"/-->
                <form class="formup" action="/bitrix/admin/tszh_epasport_import.php?clear_cache=Y" method="post"
                      enctype="multipart/form-data">
                    <input type="file" name="this_file">
                    <input class="botup" type="submit" value="���������">
                </form>
                <img class="imgup" src="/epassports/ajax-loader.gif" style="display:none;"/>
                <script>
                    $('.botup').mouseup(function () {
                        $('form.formup').hide();
                        $('.imgup').show();
                    });
                </script>

            </td>
        </tr>
        <tr valign="top">
            <td width="40%">������������ ����� ������ �������:</td>
            <td width="60%">
                <? echo ini_get('max_execution_time'); ?> ���. (<? echo ini_get('mysql.connect_timeout'); ?> ���. ���
                ����)
            </td>
        </tr>
        <tr valign="top">
            <td width="40%">������������ ������ �����:</td>
            <td width="60%">
                <? echo ini_get('upload_max_filesize'); ?>
            </td>
        </tr>

        <? $tabControl->Buttons(); ?>
        <!--input type="button" type="submit" id="start_button" value="<? echo GetMessage("TI_START_IMPORT") ?>" class="adm-btn-save"/-->
        <? $tabControl->End(); ?>
    </form>
    <?
}
 require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>