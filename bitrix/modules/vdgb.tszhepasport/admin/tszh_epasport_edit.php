<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhepasport/include.php");
if (!CModule::IncludeModule("vdgb.tszhepasport")) {return false;}

use Vdgb\Tszhepasport\EpasportTable;

$modulePermissions = $APPLICATION->GetGroupRight("vdgb.tszhepasport");
if ($modulePermissions <= "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

IncludeModuleLangFile(__FILE__);

$bFatalError = false;

ClearVars();
ClearVars("str_");

$strRedirect = BX_ROOT."/admin/tszh_epasport_edit.php?lang=".LANG.GetFilterParams("find_", false);
$strRedirectList = BX_ROOT."/admin/tszh_epasport_list.php?lang=".LANG.GetFilterParams("find_", false);

$errorMessage = "";
$bVarsFromForm = false;
$message = null;

$ID = IntVal($_REQUEST['ID']);
$entityFields = EpasportTable::getFieldNames();

$aTabs = array(
		array("DIV" => "edit1", "TAB" => GetMessage("TSZH_EP_EPASPORT_EDIT"), "ICON" => "vdgb.tszhepasport", "TITLE" => GetMessage("TSZH_EP_EPASPORT_TITLE")),
		array("DIV" => "edit2", "TAB" => GetMessage("TSZH_EP_EPASPORT_HTML"), "ICON" => "vdgb.tszhepasport", "TITLE" => GetMessage("TSZH_EP_EPASPORT_TITLE"))
	);

$UF_ENTITY = "TSZH_EP_EPASPORT";

$tabControl = new CAdminForm(basename(__FILE__, '.php'), $aTabs);

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['Update']) && check_bitrix_sessid())
{
	if ($modulePermissions < "W")
		$errorMessage .= GetMessage("ACCESS_DENIED") . ".<br />";

	$ID = IntVal($ID);
	if ($ID < 0)
		$errorMessage .= GetMessage("TSZH_EP_GROUP_SAVE_ERROR") . ' ' . GetMessage("TSZH_EP_GROUP_SAVE_ERROR_ITEM_NOT_FOUND") . "<br />";

	if (strlen($errorMessage) <= 0)
	{
		$fieldValues = array();
		array_map(function ($fieldName) use (&$fieldValues) {
			if (isset($_POST[$fieldName]))
				$fieldValues[$fieldName] = $_POST[$fieldName];
		}, $entityFields);
		$USER_FIELD_MANAGER->EditFormAddFields($UF_ENTITY, $fieldValues);

		if ($ID > 0)
		{
			$result = EpasportTable::update($ID, $fieldValues);
		}
		else
		{
			$result = EpasportTable::add($fieldValues);
			if ($bResult)
				$ID = $result->getId();
		}

		if (!$result->isSuccess())
		{
			$errorMessage .= GetMessage("TSZH_EP_GROUP_SAVE_ERROR") . '<br>' . implode('<br>', $result->getErrorMessages());
		}
	}

	if (strlen($errorMessage) <= 0)
	{
		if (isset($_REQUEST["apply"]) && strlen($_REQUEST["apply"]))
			LocalRedirect($strRedirect."&ID=".$ID."&".$tabControl->ActiveTabParam());
		else
			LocalRedirect($strRedirectList);
	}
	else
	{
		$bVarsFromForm = true;
	}
}

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhepasport/prolog.php");

$APPLICATION->SetTitle(($ID > 0) ? str_replace('#ID#', $ID, GetMessage("TSZH_EP_EDIT_PAGE_TITLE")) : GetMessage("TSZH_EP_ADD_PAGE_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$fieldValues = array();
if ($ID > 0)
{
	$db = EpasportTable::getList(
		array(
			"filter" => array("ID" => $ID),
			"limit" => 1,
		)
	);
	
	if (!$fieldValues = $db->fetch())
	{
		$errorMessage .= GetMessage("TSZH_EP_EDIT_ITEM_NOT_FOUND") . ".<br />";
		$bFatalError = true;
	}
}

if ($bVarsFromForm)
{
	array_map(function ($fieldName) use ($fieldValues) {
		if (isset($_POST[$fieldName]))
			$fieldValues[$fieldName] = $_POST[$fieldName];
	}, $entityFields);
}


if(strlen($errorMessage)>0)
	CAdminMessage::ShowMessage(Array("DETAILS"=>$errorMessage, "TYPE"=>"ERROR", "MESSAGE"=>GetMessage("TSZH_EP_EDIT_ERRORS") . ':', "HTML"=>true));

if ($bFatalError)
{
	require_once ($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_admin.php");
	return;
}

/**
 *   CAdminForm()
**/

$aMenu = array(
		array(
				"TEXT" => GetMessage("TSZH_EP_ITEMS_LIST"),
				"LINK" => $strRedirectList,
				"ICON"	=> "btn_list",
				"TITLE" => GetMessage("TSZH_EP_ITEMS_LIST_TITLE"),
			)
	);

if ($ID > 0 && $modulePermissions >= "W")
{
	$aMenu[] = array("SEPARATOR" => "Y");

	$aMenu[] = array(
			"TEXT" => GetMessage("TSZH_EP_GROUP_DELETE_TITLE"),
			"LINK" => "javascript:if(confirm('" . GetMessage("TSZH_EP_GROUP_DELETE_CONFIRM") . "')) window.location='{$strRedirectList}&ID=".$ID."&edit_form_del=1&".bitrix_sessid_get()."#tb';",
			"WARNING" => "Y",
			"ICON"	=> "btn_delete",
		);
}
if(!empty($aMenu))
	$aMenu[] = array("SEPARATOR"=>"Y");

$link = DeleteParam(array("mode"));
$link = $APPLICATION->GetCurPage()."?mode=settings".($link <> ""? "&".$link:"");
$aMenu[] = array(
	"TEXT"=> GetMessage("TSZH_EP_EDIT_SETTING"),
	"TITLE"=>GetMessage("TSZH_EP_EDIT_SETTING_TITLE"),
	"LINK"=>"javascript:".$tabControl->GetName().".ShowSettings('".htmlspecialcharsbx(CUtil::addslashes($link))."')",
	"ICON"=>"btn_settings",
);

$context = new CAdminContextMenu($aMenu);
$context->Show();


// ============ FORM PROLOG ==================
$tabControl->BeginEpilogContent();
echo GetFilterHiddens("find_");
?>
<input type="hidden" name="Update" value="Y" />
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value="<?=$ID?>" />
<?
echo bitrix_sessid_post();
$tabControl->EndEpilogContent();
// ===========================================

$tabControl->Begin(array(
	"FORM_ACTION" => $APPLICATION->GetCurPage(),
	"FORM_ATTRIBUTES" => "method=\"POST\" enctype=\"multipart/form-data\"",
));
$tabControl->BeginNextFormTab();

$fieldsMap = EpasportTable::getEditableFields();

// �������� ����
$tabControl->AddSection('GENERAL', GetMessage("TSZH_EP_GENERAL_FIELDS"));
$selectedFields = array('ID', 'TSZH_ID', 'EXTERNAL_ID', 'EP_TYPE', 'EP_ADDRESS', 'EP_ADDRESS_VIEW_FULL' , 'EP_STREET', 'EP_HOUSE', 'EP_SERIES', 'EP_COM_AREA', 'EP_FLOORS', 'EP_YEAR_OF_BUILT');
foreach ($fieldsMap as $fieldName => $fieldSettings)
{
	if(!in_array($fieldName, $selectedFields)) continue;
	$fieldTitle = $fieldSettings['title'] . ':';
	switch ($fieldName)
	{
		case 'ID':
			$tabControl->AddViewField($fieldName, $fieldTitle, $fieldValues[$fieldName]);
			break;
		case 'TSZH_ID':
			$rsTszh = CTszh::GetList();
			$arTszhs = Array();
			while ($arTszh = $rsTszh->Fetch()) $arTszhs[$arTszh['ID']] = "[{$arTszh['ID']}] {$arTszh['NAME']}";
			$tabControl->AddDropDownField($fieldName, $fieldTitle, true, $arTszhs, $fieldValues[$fieldName]);
			break;
		case 'EP_TYPE':
			$arTypes = Array(
				'A' => GetMessage('EPAS_ENTITY_EP_TYPE_MKD'),
				'P' => GetMessage('EPAS_ENTITY_EP_TYPE_GD'),
			);
			$tabControl->AddDropDownField($fieldName, $fieldTitle, true, $arTypes, $fieldValues[$fieldName]);
			break;
		case 'EP_HOUSE':
		case 'EP_SERIES':
		case 'EP_FLOORS':
		case 'EP_YEAR_OF_BUILT':
		case 'EP_COM_AREA':
			$tabControl->AddEditField($fieldName, $fieldTitle, $fieldSettings['requied'], array("size"=>10), $fieldValues[$fieldName]);
			break;
		default:
			$tabControl->AddEditField($fieldName, $fieldTitle, $fieldSettings['requied'], array("size"=>40, "maxlength"=>255), $fieldValues[$fieldName]);
			break;
	}
}

/// ���� ��� ���
$tabControl->AddSection('MKD_TYPE', GetMessage("TSZH_EP_TYPE_MKD_FIELDS"));
$selectedFields = array('EP_WEAR', 'EP_PORCHES', 'EP_LIVING_ROOMS', 'EP_NONLIVING_ROOMS', 'EP_PEOPLE', 'EP_ACCOUNTS', 'EP_PEOPLE_ACCOUNTS', 'EP_COMPANY_ACCOUNTS');
foreach ($fieldsMap as $fieldName => $fieldSettings)
{
	if(!in_array($fieldName, $selectedFields)) continue;
	$fieldTitle = $fieldSettings['title'] . ':';
	$tabControl->AddEditField($fieldName, $fieldTitle, $fieldSettings['requied'], array("size"=>10), $fieldValues[$fieldName]);
}

/// ���� ��� ��
$tabControl->AddSection('GD_TYPE', GetMessage("TSZH_EP_TYPE_GD_FIELDS"));
$selectedFields = array('EP_YEAR_OF_COMMISSIONING', 'EP_REG_PEOPLE');
foreach ($fieldsMap as $fieldName => $fieldSettings)
{
	if(!in_array($fieldName, $selectedFields)) continue;
	$fieldTitle = $fieldSettings['title'] . ':';
	$tabControl->AddEditField($fieldName, $fieldTitle, $fieldSettings['requied'], array("size"=>10), $fieldValues[$fieldName]);
}

if(
    (count($USER_FIELD_MANAGER->GetUserFields($UF_ENTITY)) > 0) ||
    ($USER_FIELD_MANAGER->GetRights($UF_ENTITY) >= "W")
)
{
    $tabControl->AddSection('USER_FIELDS', GetMessage("TSZH_EP_USER_FIELDS"));
    $tabControl->ShowUserFields($UF_ENTITY, $str_ID, $bVarsFromForm);
}

$tabControl->BeginNextFormTab();

$tabControl->AddSection('HTML_CODE', GetMessage("TSZH_EP_HTML_CODE"));
$htmlField = $fieldsMap['EP_HTML'];
$tabControl->AddTextField('EP_HTML', $htmlField['title'].':', $fieldValues['EP_HTML'], array('cols' => 70, 'rows' => 25), false);

$tabControl->BeginCustomField("EP_HTML", $htmlField['title'], false);
?>
<tr id="tr_EP_HTML">
<td colspan="2">
<?if(COption::GetOptionString("iblock", "use_htmledit", "Y")=="Y"):?>
	<?CFileMan::AddHTMLEditorFrame(
	"EP_HTML",
	$fieldValues['EP_HTML'],
	"EP_HTML_TYPE",
	'html',
	array( 'height' => 450, 'width' => '100%' )
	);?>
<?else:?>
	<textarea cols="60" rows="10" name="EP_HTML" style="width:100%"><?echo $fieldValues['EP_HTML']?></textarea>
<?endif;?>
</td>
</tr>
<?
$tabControl->EndCustomField("EP_HTML", '<input type="hidden" name="EP_HTML" value="'.$fieldValues['EP_HTML'].'">');



$tabControl->Buttons(Array(
	"disabled" => ($modulePermissions < "W"),
	"back_url" => $strRedirectList,
));

$tabControl->Show();

if(method_exists($USER_FIELD_MANAGER, 'showscript')) {
	$tabControl->BeginPrologContent();
	echo $USER_FIELD_MANAGER->ShowScript();
	$tabControl->EndPrologContent();
}

$tabControl->ShowWarnings($tabControl->GetName(), $message);

if(!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):?>
<?echo BeginNote();?>
<span class="required">*</span> <?echo GetMessage("REQUIRED_FIELDS")?>
<?echo EndNote(); ?>
<?endif;?>
<?require_once ($DOCUMENT_ROOT.BX_ROOT."/modules/main/include/epilog_admin.php");?>
