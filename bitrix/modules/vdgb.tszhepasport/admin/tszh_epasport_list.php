<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhepasport/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhepasport/prolog.php");
if (!CModule::IncludeModule("vdgb.tszhepasport")) {return false;}
use Vdgb\Tszhepasport\EpasportTable;


IncludeModuleLangFile(__FILE__);


// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("vdgb.tszhepasport");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT <= "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$UF_ENTITY = "TSZH_EP_EPASPORT";

$sTableID = "tbl_tszh_epasports";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arTszhList = Array();
$rsTszh = CTszh::GetList(Array("NAME" => "ASC"), Array(), false, false, Array("ID", "NAME"));
while ($arTszh = $rsTszh->Fetch())
	$arTszhList[$arTszh['ID']] = $arTszh['NAME'];

$arHeaders = array();
$entityFields = EpasportTable::getMap();
$typesMap = array(
	'integer' => 'int',
	'float' => 'int',
	"string" => 'string',
);
$shownByDefault = array(
	"ID",
	"HOUSE_ID",
/*	"EP_ADDRESS",
	"EP_ADDRESS_VIEW_FULL",
	"EP_TYPE",*/
);
foreach ($entityFields as $fieldName => $fieldSettings)
{
	$isNumeric = in_array($fieldSettings['data_type'], array('integer', 'float'));

	$fieldHeading = array(
		"id" => $fieldName,
		"content" => $fieldSettings['title'],
		"align" => $isNumeric ? "right" : "left",
		"default" => in_array($fieldName, $shownByDefault),
		"type" => $typesMap[$fieldSettings['data_type']],
	);
	if (!isset($fieldSettings['expression']))
		$fieldHeading['sort'] = $fieldName;
	if ($fieldName !== "ID" && !isset($fieldSettings['expression']))
		$fieldHeading['edit'] = true;

	if ($fieldName == 'TSZH_ID')
	{
		$fieldHeading['type'] = 'list';
		$fieldHeading['items'] = $arTszhList;
		$fieldHeading['align'] = 'left';
	}
	
	$arHeaders[] = $fieldHeading;
}

// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
	global $FilterArr, $lAdmin;
	foreach ($FilterArr as $value) {
		global $$value;
	}

	return true;
}
// *********************** /CheckFilter ******************************* //

// ������ �������� �������
$FilterArr = TszhEpAdminFilterParams($arHeaders);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

$USER_FIELD_MANAGER->AdminListAddFilterFields($UF_ENTITY, $FilterArr);

// ���� ��� �������� ������� ���������, ���������� ���
$arFilter = array();
if (CheckFilter() && !is_set($_REQUEST, 'del_filter'))
{
	// �������� ������ ���������� ��� ������� �� ������ �������� �������
	TszhEpAdminMakeFilter($arFilter, $arHeaders);
	$USER_FIELD_MANAGER->AdminListAddFilter($UF_ENTITY, $arFilter);
}

// ******************************************************************** //
//                ��������� �������� ��������                           //
// ******************************************************************** //

if (isset($_REQUEST["edit_form_del"]) && IntVal($_REQUEST["edit_form_del"]) == 1 && check_bitrix_sessid())
{
	if (isset($_REQUEST["ID"]) && IntVal($_REQUEST["ID"]) > 0)
	{
		@set_time_limit(0);

		$arItem = EpasportTable::getById($_REQUEST["ID"])->fetch();
		if (is_array($arItem))
		{
			if(EpasportTable::Delete($arItem['ID']))
			{
				$messageOK = GetMessage("TSZH_EP_DELETE_OK");
			}
			else
			{
				$lAdmin->AddGroupError(GetMessage("TSZH_EP_DELETE_ERROR"), $ID);
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("TSZH_EP_DELETE_ERROR").". ".GetMessage("TSZH_EP_DELETE_ITEM_NOT_FOUND", Array("#ID#" => $ID)), $ID);
		}
	}
}


// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //

// ���������� ����������������� ���������
if ($lAdmin->EditAction() && $POST_RIGHT>="W" && check_bitrix_sessid())
{

	// ������� �� ������ ���������� ���������
	foreach($FIELDS as $ID=>$arFields)
	{
		if(!$lAdmin->IsUpdated($ID))
			continue;

		// �������� ��������� ������� ��������
		$ID = IntVal($ID);

		$arItem = EpasportTable::getById($ID)->fetch();
		if (is_array($arItem))
		{
			$USER_FIELD_MANAGER->AdminListPrepareFields($UF_ENTITY, $arFields);
			if (!EpasportTable::Update($ID, $arFields))
			{
				$strError = '';
				if ($ex = $APPLICATION->GetException())
					$strError = $ex->GetMessage();
				$lAdmin->AddGroupError(GetMessage("TSZH_EP_GROUP_SAVE_ERROR")." ".$strError, $ID);
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("TSZH_EP_GROUP_SAVE_ERROR")." ".GetMessage("TSZH_EP_GROUP_SAVE_ERROR_ITEM_NOT_FOUND"), $ID);
		}
	}
}

// ��������� ��������� � ��������� ��������
if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT>="W" && check_bitrix_sessid())
{
	// ���� ������� "��� ���� ���������"
	if($_REQUEST['action_target']=='selected')
	{
		$rsItems = EpasportTable::getList(array("filter" => $arFilter, "select" => Array("ID")));
		$arID = Array();
		while ($arItem = $rsItems->Fetch())
			$arID[] = $arItem['ID'];
	}

	@set_time_limit(0);

	// ������� �� ������ ���������
	foreach($arID as $ID)
	{
		if(strlen($ID)<=0)
			continue;

		$ID = IntVal($ID);

		// ��� ������� �������� �������� ��������� ��������
		switch($_REQUEST['action'])
		{
			// ��������
			case "delete":
				$arItem = EpasportTable::getByID($ID)->fetch();
				if (is_array($arItem))
				{
					if (EpasportTable::delete(IntVal($ID)))
						$messageOK = GetMessage("qroup_del_ok");
					else
						$lAdmin->AddGroupError(GetMessage("TSZH_EP_DELETE_ERROR"), $ID);
				}
				else
					$lAdmin->AddGroupError(GetMessage("TSZH_EP_DELETE_ERROR")." ".GetMessage("TSZH_EP_DELETE_ITEM_NOT_FOUND"), $ID);
				break;
		}
	}
}

// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //

// ������� ������ �������
$rsItems = EpasportTable::getList(
	array(
		"order" => array(ToUpper($by)=>$order),
		"filter" => $arFilter,
		"select" => Array("*"/*, "EP_TYPE"*/)
	)
);

// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsItems, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("TSZH_EP_PASPORTS")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

$USER_FIELD_MANAGER->AdminListAddHeaders($UF_ENTITY, $arHeaders);
$lAdmin->AddHeaders($arHeaders);

while ($rowData = $rsData->NavNext(true, "f_"))
{
	// ������� ������. ��������� - ��������� ������ CAdminListRow
	$row =& $lAdmin->AddRow($rowData["ID"], $rowData);

	// ����� �������� ����������� �������� ��� ��������� � �������������� ������
	foreach ($arHeaders as $field)
	{
		$fieldName = $field['id'];

		if ($fieldName == 'ID')
		{
			$row->AddViewField("ID", '<a href="tszh_epasport_edit.php?ID='.$rowData["ID"].'&lang='.LANG.'">'.$rowData["ID"].'</a>');
			continue;
		}
		elseif ($fieldName == 'TSZH_ID')
		{
			$row->AddSelectField($fieldName, $field['items']);
			$row->AddViewField($fieldName, '[<a href="tszh_epasport_edit.php?ID=' . $rowData[$fieldName] . '&lang=' . LANGUAGE_ID . '" title="' . GetMessage("AL_VIEW_TSZH") . '">' . $rowData[$fieldName] . '</a>] ' . $field['items'][$rowData[$fieldName]]);
			continue;
		}
		elseif ($fieldName == 'EP_TYPE')
		{
			$row->AddViewField($fieldName, $rowData[$fieldName]);
		}

		if (!isset($entityFields[$fieldName]['expression']))
			$row->AddInputField($fieldName, array("size"=>20));
	}
	$USER_FIELD_MANAGER->AddUserFields($UF_ENTITY, $rowData, $row);

	// ���������� ����������� ����
	$arActions = Array();

	// �������������� ��������
	$arActions[] = array(
		"ICON"		=> "edit",
		"DEFAULT"	=> true,
		"TEXT"		=> GetMessage("TSZH_EP_GROUP_EDIT_TITLE"),
		"ACTION"	=> $lAdmin->ActionRedirect("tszh_epasport_edit.php?ID=".$f_ID.'&lang='.LANG)
	);

	// �������� ��������
	if ($POST_RIGHT>="W")
		$arActions[] = array(
			"ICON"		=> "delete",
			"TEXT"		=> GetMessage("TSZH_EP_GROUP_DELETE_TITLE"),
			"ACTION"	=> "if(confirm('".GetMessage('TSZH_EP_GROUP_DELETE_CONFIRM')."')) ".$lAdmin->ActionDoGroup($f_ID, "delete")
		);

	// ������� �����������
	$arActions[] = array("SEPARATOR"=>true);

	// ���� ��������� ������� - �����������, �������� �����.
	if(is_set($arActions[count($arActions)-1], "SEPARATOR"))
		unset($arActions[count($arActions)-1]);

	// �������� ����������� ���� � ������
	$row->AddActions($arActions);

}

// ������ �������
$lAdmin->AddFooter(
	array(
		array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()), // ���-�� ���������
		array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"), // ������� ��������� ���������
	)
);

// ��������� ��������
$lAdmin->AddGroupActionTable(Array(
	"delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
//	"activate"=>GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // ������������ ��������� ��������
//	"deactivate"=>GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // �������������� ��������� ��������
));

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //

// ���������� ���� �� ������ ������ - ���������� ������
$aContext = array(
	array(
		"TEXT"=>GetMessage("TSZH_EP_ADD_ITEM"),
		"LINK"=>"tszh_epasport_edit.php?lang=".LANG,
		"TITLE"=>GetMessage("TSZH_EP_ADD_ITEM_TITLE"),
		"ICON"=>"btn_new",
	),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //

if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT>="W" && isset($messageOK) && $_REQUEST['action'] == "delete")
{
	$message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// �������������� �����
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("TSZH_EP_EPASPORTS"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

if (isset($message) && isset($_REQUEST["edit_form_del"]))
{
	$message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

$arFilterItems = Array();
foreach ($arHeaders as $arField)
{
	$arFilterItems[] = $arField['content'];
}

$USER_FIELD_MANAGER->AddFindFields($UF_ENTITY, $arFilterItems);
$oFilter = new CAdminFilter(
	$sTableID."_filter",
	$arFilterItems
);

?>
	<form name="form_filter" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
		<?
		$oFilter->Begin();

		TszhEpShowShowAdminFilter($arHeaders);

		$USER_FIELD_MANAGER->AdminListShowFilter($UF_ENTITY);

		$oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "form_filter"));
		$oFilter->End();
		?>
	</form>
<?

// ������� ������� ������ ���������
$lAdmin->DisplayList();


require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>
