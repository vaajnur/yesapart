<?
$MESS["VDGB_TSZH_EP_INCORRECT_DATETIME"] = "Указано некорректное занчение даты/времени (атрибут #ATTR#): #VALUE##";
$MESS["VDGB_TSZH_EP_XML_MISSING_REQUIRED_ATTR"] = "Элемент #ELEMENT# не содержит обязательного атрибута #NAME#";
$MESS["VDGB_TSZH_EP_XML_MISSING_REQUIRED_ELEMENT"] = "Отсутсвует обязательный дочерний элемент `#CHILD#` у `#PARENT#`";
$MESS["VDGB_TSZH_EP_XML_WRONG_INTEGER_ATTR"] = "Атрибут #NAME# элемента #ELEMENT# должен быть целым числом (указано значение `#VALUE#`)";
$MESS["VDGB_TSZH_EP_XML_WRONG_FLOAT_ATTR"] = "Атрибут #NAME# элемента #ELEMENT# должен быть числом (указано значение `#VALUE#`)";

