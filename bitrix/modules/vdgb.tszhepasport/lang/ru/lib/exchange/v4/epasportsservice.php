<?
$MESS["TSZH_EP_1C_ERROR_MODULE"] = "Не установлен модуль «1С:Сайт ЖКХ» (citrus.tszh)";
$MESS["TSZH_EP_1C_EPAS_ERROR_MODULE"] = "Не установлен модуль «1С:Сайт ЖКХ:Дома в управлении» (vdgb.tszhepasport)";
$MESS["TSZH_EP_1C_ERROR_IBLOCK_MODULE"] = "Модуль «Информационные блоки» не установлен.";
$MESS["TSZH_EP_1C_ERROR_ORG_NOT_FOUND"] = "Организация с указанным ИНН не найдена на сайте.";
