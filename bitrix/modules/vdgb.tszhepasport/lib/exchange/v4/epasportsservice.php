<?
namespace Vdgb\Tszhepasport\Exchange\v4;

use Vdgb\Tszhepasport\Exchange\v4\Formats\Houses;
use Vdgb\Tszhepasport\Exchange\ServiceBase;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * ��������� �������� ������ ������� ������
 * ?mode=import&type=epasports
 *
 * @package Vdgb\Tszhepasport\Controller\v4
 */
class EpasportsService extends ServiceBase
{
	private $debug = false;

	/**
	 * @throws \Exception
	 */
	public function run()
	{
		global $APPLICATION;

		if (!\CModule::IncludeModule("citrus.tszh"))
		{
			throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_MODULE"));
		}

		if (!\CModule::IncludeModule("vdgb.tszhepasport"))
		{
			throw new \Exception(Loc::getMessage("TSZH_EP_1C_EPAS_ERROR_MODULE"));
		}

		if (!\CModule::IncludeModule("iblock"))
		{
			throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_IBLOCK_MODULE"));
		}

		$arTszh = $this->facade->getOrg();
		if (!is_array($arTszh) || IntVal($arTszh["ID"]) <= 0)
		{
			throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_ORG_NOT_FOUND"));
		}

		$start_time = time();
		$ABS_FILE_NAME = $this->facade->getUploadedFile();
		$NS = &$_SESSION["BX_CITRUS_TSZH_EP_IMPORT"];
		if (!is_array($NS))
		{
			$NS = array(
				"STEP" => 0,
				"TSZH" => $arTszh['ID'],
			);
		}

		$obImport = new Houses($NS, $this->facade->timelimit, true);

		try
		{
			$progress = array();
			$finished = false;
			do
			{
				if ($NS["STEP"] < 1)
				{
					$_SESSION["TSZH_EP_IMPORT"] = array(
						"SERVICES" => Array(),
					);

					$NS["STEP"]++;

					$progress[] = Loc::getMessage("TI_STEP1_DONE");
				}
				elseif ($NS["STEP"] < 2)
				{
					if (file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME) && ($fp = fopen($ABS_FILE_NAME, "rb")))
					{
						if ($obImport->readXml($fp))
						{
							$NS["STEP"]++;
						}
						fclose($fp);
					}
					else
					{
						throw new \Exception(Loc::getMessage("TI_STEP3_ERROR"));
					}

					$file_size = file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME) ? filesize($ABS_FILE_NAME) : 0;
					$progress[] = Loc::getMessage("TI_STEP3_PROGRESS", Array('#PROGRESS#' => ($file_size > 0 ? round($obImport->getXml()->GetFilePosition() / $file_size * 100, 2) : 0)));
				}
				elseif ($NS["STEP"] < 3)
				{
					$obImport->ProcessOrg();
					$NS['ACCOUNTS_IMPORT_STARTED'] = time();
					$NS["STEP"]++;
					$progress[] = Loc::getMessage("TI_STEP5_DONE");
				}
				elseif ($NS["STEP"] < 4)
				{
					if ($obImport->importHouses())
					{
						$progress[] = Loc::getMessage("CITRUS_TSZH_HOUSES_IMPORTED");
						$NS["STEP"]++;
					}
					else
					{
						$progress[] = Loc::getMessage("CITRUS_TSZH_HOUSES_LOADING");
					}

				}
				elseif ($NS["STEP"] < 5)
				{
					$NS["STEP"]++;
					$progress[] = Loc::getMessage("TI_STEP4_DONE");
				}
				elseif ($NS["STEP"] < 6)
				{
					$result = $obImport->Cleanup($NS["ACTION"], $start_time, $this->facade->timelimit);

					$counter = 0;
					foreach ($result as $key => $value)
					{
						$NS["DONE"][$key] += $value;
						$counter += $value;
					}

					if (!$counter)
					{
						$NS["STEP"]++;
						$progress[] = Loc::getMessage("TI_STEP6_DONE");
					}
					else
					{
						$progress[] = Loc::getMessage("TI_STEP6_PROGRESS", Array('#CNT#' => intval($NS["DONE"]["DEL"]) + intval($NS["DONE"]["METER_DEL"]) + intval($NS["DONE"]["DEA"]) + intval($NS["DONE"]["METER_DEA"])));
					}
				}
				elseif ($NS["STEP"] < 7)
				{
					if (!$this->facade->debug)
					{
						@unlink($ABS_FILE_NAME);
					}

					$NS["STEP"]++;

					$NS = false;
					echo "success\n" . Loc::getMessage("TI_STEP7_DONE");
					if ($this->facade->debug)
					{
						$this->facade->logMessage(Loc::getMessage("VDGB_TSZH_IMPORT_FINISH") . "\n" . implode("\n", $progress));
					}
					$finished = true;
					break;
				}
				else
				{
					if ($this->debug)
					{
						$NS["STEP"] = 0;
					}
					else
					{
						throw new \Exception(Loc::getMessage("TI_ERROR_ALREADY_FINISHED"));
					}
				}
			} while ($this->facade->timelimit > 0 && (time() - $start_time) < $this->facade->timelimit);

			if ($finished)
			{
				if ($this->facade->debug)
				{
					$this->facade->logMessage(Loc::getMessage("VDGB_TSZH_IMPORT_FINISH"));
				}
			}
			else
			{
				echo "progress\n" . implode(', ', $progress);
			}
		}
		catch (\Exception $e)
		{
			$NS = array();
			throw $e;
		}

		$this->facade->textOutput = true;
	}
}
