<?

namespace Vdgb\Tszhepasport\Exchange\v4;

use Vdgb\Tszhepasport\Exchange\ControllerInterface;
use Vdgb\Tszhepasport\Exchange\Facade;
use Vdgb\Tszhepasport\Exchange\ServiceBase;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * ��������� ����� ������ 4
 *
 * @package Vdgb\Tszhepasport\Controller\v4
 */
class Controller implements ControllerInterface
{
	protected $facade = null;
	protected $objects = array();

	/**
	 * @param Facade $facade
	 */
	function __construct(Facade $facade)
	{
		$this->facade = $facade;
	}

	/**
	 * @param string $mode
	 * @param string $service
	 * @return string
	 */
	protected function getServiceClassname($service)
	{
		return '\\' . __NAMESPACE__ . '\\' . ucfirst($service) . 'Service';
	}

	/**
	 * @param string $mode
	 * @param string $service
	 * @return bool
	 */
	protected function hasObject($service)
	{
		return array_key_exists($service, $this->objects);
	}

	/**
	 * @param string $mode
	 * @param string $service
	 * @return bool
	 */
	public function hasService($service)
	{
		return class_exists($this->getServiceClassname($service));
	}

	/**
	 * @param string $mode
	 * @param string $service
	 * @return ServiceBase
	 */
	public function getService($service)
	{
		if (!$this->hasService($service))
		{
			throw new \InvalidArgumentException(Loc::getMessage("VDGB_TSZH_1C_ERROR_SERVICE_ERROR", array(
				'#SERVICE#' => $service,
				'#VERSION#' => $this->facade->getVersion(),
			)));
		}

		if (!$this->hasObject($service))
		{
			$classname = $this->getServiceClassname($service);
			$this->objects[$service] = new $classname($this, $this->facade);
		}

		return $this->objects[$service];
	}

	/**
	 * @param string $mode
	 * @param string $service
	 */
	public function processRequest($service)
	{
		$this->getService($service)->run();
	}
}
