<?

namespace Vdgb\Tszhepasport\Exchange\v4\Formats;

/**
 * ������������ ��� ������������ XML-������ ��������
 *
 * @package Vdgb\Tszhepasport\Controller\v4
 */
class Xml
{
	/**
	 * @param int|bool $timestamp
	 * @return bool|string
	 */
	public static function formatDateTime($timestamp = false)
	{
		return date("c", $timestamp ? $timestamp : time());
	}

	/**
	 * @param int $timestamp
	 * @return bool|string
	 * @deprecated
	 */
	public static function formatDate($timestamp)
	{
		return date("Y-m-d", $timestamp);
	}
}
