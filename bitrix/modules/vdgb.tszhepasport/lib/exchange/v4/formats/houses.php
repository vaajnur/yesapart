<?

namespace Vdgb\Tszhepasport\Exchange\v4\Formats;

use Bitrix\Main\ArgumentTypeException;
use Vdgb\Tszhepasport\Exchange\ApplicationException;
use \Vdgb\Tszhepasport\Exchange\FormatException;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\ArgumentException;
use Vdgb\Tszhepasport\EpasportTable;
use Vdgb\Tszhepasport\Types\XmlNode;

Loc::loadMessages(__FILE__);

/**
 * ��������� �������� ������ ������� ������ ������ 4
 *
 * @package Vdgb\Tszhepasport\Exchange\v4\Formats
 */
class Houses
{
	const MAX_ERRORS = 40;

	protected $supportedVersion = 4;

	/** @var array ������ ��� ���������� ����� ������ � ������  */
	protected $state;

	/** @var int ID ������� ����������  */
	protected $tszhID = false;

	/** @var string ID ����� ������� ���������� */
	protected $siteID = false;

	protected $arTszh = Array();
	protected $createTszh = false;
	protected $tszhName = false;
	protected $inn = false;

	/** @var int ������ ����� �� XML */
	var $version = 1;

	var $filetype = "E-Passports";

	/** @var array ������� ���� �� ���������� X � ��������� */
	var $xFields = array();

	/**
	 * @var string ��� ������� � ������� XML-�����
	 */
	protected $tableName;

	/**
	 * @var bool �������� ��������� ������ ��� ����������� ������������ ��������� ���������� ������
	 */
	protected $useSessions;

	/**
	 * @var string � ������ ������������� ������ �������� SQL-������� �������������� � ������� ������
	 */
	protected $andWhereThisSession;

	/**
	 * @var string
	 */
	protected $sessionId;

	/**
	 * @var \CIBlockXMLFile
	 */
	protected $xmlFile;

	/**
	 * @var int ����� �� ����� ���������� ������ ���� � ��������
	 */
	protected $timeLimit;

	/**
	 * @var float Timestamp ������� ������ ���������
	 */
	protected $startTime = null;

	/**
	 * @param array $state ���������� ��� �������� �������� ���������, �������� ������ ����������� ����� ������ �������� (������)
	 * @param int $timeLimit ����� �� ����� ���������� ������ ���� � ��������
	 * @param bool $useSessions �������� ��������� ������ ��� ����������� ������������ ��������� ���������� ������
	 * @param string $tableName ��� ������� � ������� XML-�����
	 * @throws ArgumentException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	function __construct(&$state, $timeLimit = 0, $useSessions = false, $tableName = 'b_xml_tree')
	{
		if (strlen(trim($tableName)) == 0)
			throw new ArgumentException("\$tableName must contain a non-empty string");
		if (!isset($state) || !is_array($state))
			throw new ArgumentException('Array variable must be passed in $state parameter');

		$this->useSessions = $useSessions;
		$this->tableName = $tableName . ($useSessions ? '_m' : '');
		$this->state = &$state;

		$this->setStartTime();

		if ($timeLimit > 0)
			$this->timeLimit = $timeLimit;
		else
			$this->timeLimit = 368*24*60*60; // ���� ��� ;)

		$this->xmlFile = new \CIBlockXMLFile($this->tableName);
		if ($this->useSessions)
		{
			$this->sessionId = substr(session_id(), 0, 32);
			if (!$this->state["tablesCreated"])
			{
				// ������� ������ ������ � ������� ������
				$this->clearSession();
			}
			$this->xmlFile->StartSession($this->sessionId);
			$this->state["tablesCreated"] = true;
		}
		elseif (!isset($this->state["tablesCreated"]))
		{
			$this->xmlFile->DropTemporaryTables();
			$this->xmlFile->CreateTemporaryTables();
			$this->state["tablesCreated"] = true;
		}
		$this->andWhereThisSession = $this->useSessions ? " and SESS_ID='" . $this->sessionId . "'" : '';

		$this->version = &$this->state["VERSION"];
		$this->strict = &$this->state["STRICT"];
		$this->filetype = &$this->state["FILETYPE"];

		$this->tszhID = &$this->state["TSZH"];
		$this->siteID = &$this->state["SITE_ID"];

		$this->inn = &$this->state["INN"];

		if (intval($this->tszhID) > 0)
			$this->arTszh = \CTszh::GetByID($this->tszhID);

		$this->filetype = "E-Passports";
	}

	/**
	 * ����������� ������ �� XML � �����, �������� ����������
	 *
	 * @param string $value ������ � ������ �� XML
	 * @return float �������������� �����
	 * @throws FormatException
	 */
	protected function formatNumber($value)
	{
		if (!is_numeric($value))
			throw new FormatException('Incorrect float: ' . $value);
		return floatval($value);
	}

	/**
	 * ������� � �������� ������ �� XML � ����
	 * �������� � ������������ �� �������
	 *
	 * @param resource $fp
	 * @return bool true ���� ���� ��� �������� ��������� � false ���� ���������� ���������� �������� � ������� ����� ��� ���
	 * @throws ArgumentTypeException
	 */
	public function readXml($fp)
	{
		if (!is_resource($fp))
			throw new ArgumentTypeException('Argument $fp must contain valid resouce');

		$done = $this->xmlFile->ReadXMLToDatabase($fp, $this->state, $this->timeLimit);
		// �������� ������� ����� ��������� �������� ������
		if ($done && !$this->useSessions)
		{
			$this->xmlFile->IndexTemporaryTables();
		}

		return $done;
	}

	/**
	 * @return \CIBlockXMLFile
	 */
	public function getXml()
	{
		return $this->xmlFile;
	}

	protected function resetX()
	{
		$this->xFields = array();
	}

	/**
	 * ��������� ���� � ������ ����� �� ��������� X
	 *
	 * @param string $field
	 */
	protected function addX($field)
	{
		$this->xFields[$field] = true;
	}

	/**
	 * ���������� ������ ����� (����� �������) �� ��������� X
	 *
	 * @return string
	 */
	protected function getX()
	{
		return implode(',', array_keys($this->xFields));
	}

	/**
	 * @param string $key �������� ����� (�������� ����)
	 * @param array|XmlNode $values ������ �������� ���������
	 * @param bool $number ���� true, �� ��������� ����� ��������� ��� ���� (������� ������ �������, �������� ������� �� �����)
	 * @param bool|string $field ��� ���� (� ���� ������ � API ������)
	 * @return bool|float|string
	 */
	protected function getValue($key, $values, $number = false, $field = false)
	{
		if ($values instanceof XmlNode)
			$value = isset($values[$key]) ? $values[$key] : false;
		else
			$value = array_key_exists($key, $values) ? $values[$key] : false;
		if (strtolower($value) == 'x')
		{
			if ($field)
				$this->addX($field);

			return $number ? false : 'X';
		}

		if ($number && $value !== false)
			return $this->formatNumber($value);
		else
			return $value;
	}

	/**
	 * ��������� ���������������� �������
	 * @param string $prefix ������� � ��������� ���� XML (��������, uf_ ��� puf_)
	 * @param XmlNode $arAttrs ������ ��������� �� XML
	 * @param array $arFields ������ �������������� ����� ��� ����������/����������
	 * @internal param string $entity �������� ���������������� �����
	 */
	protected function processUserFields($prefix, $arAttrs, &$arFields)
	{
		$prefix = ToLower($prefix);
		$prefixLen = strlen($prefix);
		$prefixLen = $prefixLen > 0 ? $prefixLen : false;

		foreach ($arAttrs as $attrName => $attrValue)
		{
			// ���������� ������ ���� � ��������� ���������
			if ($prefixLen)
			{
				if (strlen($attrName) > $prefixLen && ToLower(substr($attrName, 0, $prefixLen)) == $prefix)
				{
					$fieldName = strtoupper(str_replace($prefix, 'UF_', $attrName));
					$fieldValue = $attrValue;
					$arFields[strtoupper($fieldName)] = $fieldValue;
				}
			}
			else
			{
				$fieldName = strtoupper('UF_' . $attrName);
				$fieldValue = $attrValue;
				$arFields[strtoupper($fieldName)] = $fieldValue;
			}
		}
	}

	/**
	 * �������� ������ ����� �������������
	 * ������: pluralForm(5, '�����', '�����', '������');
	 *
	 * @param int $n
	 * @param string $f1
	 * @param string $f2
	 * @param string $f5
	 * @return string
	 */
	protected function pluralForm($n, $f1, $f2, $f5)
	{
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20) return $f5;
		if ($n1 > 1 && $n1 < 5) return $f2;
		if ($n1 == 1) return $f1;
		return $f5;
	}

	/**
	 * ���������� ����� ������ � ������� ������ ������
	 *
	 * @return bool|string
	 */
	public static function GetErrors()
	{
		if (count($_SESSION['TSZH_EP_IMPORT_ERRORS']) > 0) {
			return implode('<br />', $_SESSION['TSZH_EP_IMPORT_ERRORS']);
		}
		return false;
	}

	public static function ResetErrors()
	{
		$_SESSION['TSZH_EP_IMPORT_ERRORS'] = Array();
	}

	/**
	 * @param string $strError
	 * @param bool|array|XmlNode $arXMLElement
	 */
	protected function ImportError($strError, $arXMLElement = false)
	{
		global $APPLICATION;

		/** @noinspection PhpAssignmentInConditionInspection */
		if ($ex = $APPLICATION->GetException())
			$strError .= ': ' . $ex->GetString();

		$strError = strip_tags($strError);

		if (count($_SESSION['TSZH_EP_IMPORT_ERRORS']) < self::MAX_ERRORS)
		{
			if (defined("TSZH_IMPORT_DEBUG") && is_array($arXMLElement) && !empty($arXMLElement))
				$strError .= ' (' . var_export($arXMLElement, 1) . ')';
			$_SESSION['TSZH_EP_IMPORT_ERRORS'][] = $strError;
		}
		elseif (count($_SESSION['TSZH_EP_IMPORT_ERRORS']) <= self::MAX_ERRORS)
		{
			$_SESSION['TSZH_EP_IMPORT_ERRORS'][] = GetMessage("TI_SHOWN_FIRST_ERRORS") . ' ' . self::MAX_ERRORS . ' ' . self::pluralForm(self::MAX_ERRORS, GetMessage("TI_SHOWN_ERRORS1"), GetMessage("TI_SHOWN_ERRORS3"), GetMessage("TI_SHOWN_ERRORS5"));
		}

		$APPLICATION->ResetException();
	}

	/**
	 * ������ ��� ������� � ��������� ������ �����������
	 * @return bool
	 * @throws FormatException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 * @throws \Exception
	 */
	public function ProcessOrg()
	{
		global $APPLICATION, $DB;

		$xmlRootId = intval($this->useSessions ? $this->xmlFile->GetSessionRoot() : 1);
		$dbOrg = $this->getXml()->GetList(array(), array("NAME" => "org", "ID" => $xmlRootId));
		if ($dbOrg->SelectedRowsCount() <= 0)
			throw new FormatException('VDGB_TSZH_ORG_ELEMENT_NOT_FOUND');

		$org = new XmlNode($dbOrg->Fetch());

		$this->state["XML_HOUSES_PARENT"] = $org->getId();

		$this->version = $org['version'];
		$this->strict = true;

		if ($this->version != $this->supportedVersion)
			throw new FormatException('VDGB_TSZH_ERROR_WRONG_VERSION', array('#GIVEN#' => $this->version, '#MUSTBE#' => $this->supportedVersion));

		if ($this->filetype !== $org['filetype'])
			throw new FormatException('VDGB_TSZH_ERROR_WRONG_FILETYPE', array('#GIVEN#' => $org['filetype'], '#MUSTBE#' => $this->filetype));

		// inn
		if (!preg_match('/^[\d]+$/', $org['inn']))
			throw new FormatException('VDGB_TSZH_ERROR_WRONG_INN', array('#GIVEN#' => $org['inn']));

		$arTszh = \CTszh::GetList(
			array(),
			array("INN" => $org['inn']),
			false,
			array("nTopCount" => 1)
		)->Fetch();
		
		if (is_array($arTszh) && !empty($arTszh) && intval($arTszh["ID"]) > 0)
		{
			$this->arTszh = $arTszh;
			$this->tszhID = $arTszh["ID"];
			$this->siteID = $arTszh["SITE_ID"];
		}
		else
		{
			$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_WITH_INN_NOT_EXISTS", array("#INN#" => $org['inn'])));
			return false;
		}

		if ($this->state["XML_HOUSES_PARENT"])
		{
			$ar = $DB->Query("select count(*) C from " . $this->tableName . " where (NAME='house')" . $this->andWhereThisSession)->Fetch();
			$this->state["DONE"]["ALL"] = $ar["C"];
		}
		else
			throw new \LogicException("Unexpected next_step[\"XML_HOUSES_PARENT\"]!");

		$this->ResetErrors();

		return true;
	}

	/**
	 * ��������� ������ �������� XML-���������
	 *
	 * @param array|int $parent
	 * @param string|bool $tagName
	 * @return XmlNode
	 */
	protected function GetAllChildrenNested($parent, $tagName = false)
	{
		global $DB;

		$result = $parent;

		//So we get not parent itself
		if (!is_array($result))
		{
			$rs = $DB->Query("select ID, NAME, LEFT_MARGIN, RIGHT_MARGIN, ATTRIBUTES from " . $this->tableName . " where (ID = ".intval($result) . ')' . $this->andWhereThisSession);
			$result = $rs->Fetch();
			if (!$result)
				throw new \InvalidArgumentException('Incorrect $parent');

			$result["~attr"] = unserialize($result['ATTRIBUTES']);
			$result["~children"] = Array();
		}

		//Array of the references to the $arParent array members with xml_id as index.
		$arIndex = array($result["ID"] => &$result);
		$rs = $DB->Query("select * from " . $this->tableName . " where (LEFT_MARGIN between ".($result["LEFT_MARGIN"]+1)." and ".($result["RIGHT_MARGIN"]-1) . ($tagName ? " AND NAME='" . $DB->ForSql($tagName) . "'" : '') . ')' . $this->andWhereThisSession . " order by ID");
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($ar = $rs->Fetch())
		{
			$ar["~attr"] = unserialize($ar['ATTRIBUTES']);
			unset($ar["ATTRIBUTES"]);

			if(isset($ar["VALUE_CLOB"]))
				$ar["VALUE"] = $ar["VALUE_CLOB"];

			$parent_id = $ar["PARENT_ID"];
			if(!is_array($arIndex[$parent_id]))
				$arIndex[$parent_id] = array();

			$arIndex[$ar['ID']] = $ar;
			$arIndex[$ar["PARENT_ID"]]['~children'][$ar['NAME']][] = &$arIndex[$ar['ID']];
		}

		return new XmlNode($result);
	}

	/**
	 * ��������� ������ �����
	 */
	public function importHouses()
	{
		global $DB;

		if (!isset($this->state['HOUSES']))
		{
			if (!$this->state["XML_HOUSES_PARENT"])
				throw new \LogicException("XML_HOUSES_PARENT is not set");

			$houses = $this->getXml()->GetList(
				array(),
				array("NAME" => "houses", "PARENT_ID" => $this->state["XML_HOUSES_PARENT"])
			)->Fetch();
			if (!is_array($houses))
				throw new \Exception("houses element missing");
			$this->state["HOUSES"] = array(
				"XML_PARENT" => $houses["ID"],
				"LAST_ID" => 0
			);
		}

		if (!$this->state["HOUSES"]["XML_PARENT"])
			throw new \LogicException('$this->state["HOUSES"]["XML_PARENT"] is not set');

		$dbHouses = $DB->Query("select ID, NAME, LEFT_MARGIN, RIGHT_MARGIN, ATTRIBUTES from " . $this->tableName . " where (NAME='house' and PARENT_ID = ".$this->state['HOUSES']["XML_PARENT"] . ')' . $this->andWhereThisSession . " AND ID > ".intval($this->state["HOUSES"]["LAST_ID"])." order by ID");
		$countProcessed = 0;
		/** @noinspection PhpAssignmentInConditionInspection */
		while (!$this->timeIsUp() && $house = $dbHouses->Fetch())
		{
			$xmlElement = $this->GetAllChildrenNested($house);
			$this->importHouse($xmlElement);

			$this->state["HOUSES"]["LAST_ID"] = $house["ID"];
			$countProcessed++;
		}
		return $dbHouses->SelectedRowsCount() == $countProcessed;
	}

	/**
	 * ������������ ������� � �����
	 * @param XmlNode $house ������ XML-��������
	 * @throws ArgumentException
	 * @throws \Exception
	 */
	protected function importHouse($house)
	{
		/** @var XmlNode $address */
		$html = "";
		if (!empty($house->childs)){
			$content_array = $house->contents[0]->content;
			while (!empty($content_array))
			{

				$shifted_html = array_shift($content_array);
				$attributes = $shifted_html->GetAttributes();
				$html .= $attributes['name']." || ".$attributes['type']." || ".html_entity_decode(trim((string)$shifted_html))." || ";
			}
		}

		//$html = array_shift($house->contents[0]->content);

		$houseFields = array(
			"TSZH_ID" => $this->tszhID,
			"EXTERNAL_ID" => $house->get('id'),
			"EP_TYPE" => ($house->get('type') == '��')?'P':'A',
			"EP_ADDRESS" => $house->get('address'),
			"EP_ADDRESS_VIEW_FULL" => $house->get('address_view_full'),
			"EP_STREET" => $house->get('street'),
			"EP_HOUSE" => $house->get('house'),
			"EP_SERIES" => $house->getInt('series'),
			"EP_STATUS" => $house->get('status'),
			"EP_COM_AREA" => $house->getFloat('com_area'),
			"EP_FLOORS" => $house->getInt('floors'),
			"EP_YEAR_OF_BUILT" => $house->get('year_of_built'),
			"DATE_UPDATE" => new \Bitrix\Main\Type\DateTime(),
			"EP_HTML" => $html,
		);

		if($houseFields['EP_TYPE'] == 'A'){
			$houseFields["EP_WEAR"] = $house->getFloat('wear');
			$houseFields["EP_PORCHES"] = $house->getInt('porches');
			$houseFields["EP_ROOMS_AREA"] = $house->getFloat('rooms_area');
			$houseFields["EP_COM_ROOMS_AREA"] = $house->getFloat('com_rooms_area');
			$houseFields["EP_LIVING_ROOMS"] = $house->getInt('living_rooms');
			$houseFields["EP_NONLIVING_ROOMS"] = $house->getInt('nonliving_rooms');
			$houseFields["EP_PEOPLE"] = $house->getInt('people');
			$houseFields["EP_ACCOUNTS"] = $house->getInt('accounts');
			$houseFields["EP_PEOPLE_ACCOUNTS"] = $house->getInt('company_accounts');
			$houseFields["EP_COMPANY_ACCOUNTS"] = $house->getInt('company_accounts');
		}

		if($houseFields['EP_TYPE'] == 'P'){
			$houseFields["EP_YEAR_OF_COMMISSIONING"] = $house->get('year_of_commissioning'); 
			$houseFields["EP_REG_PEOPLE"] = $house->getInt('reg_people');
		}

		/**
		 * ��� ������������� �� ������ ��������� ������ (����� �� ���� ������� ����� �����), ������� ������������ �� ����� ������ ��� ������������� ���������� ���� ���� �� XML
		 */
		$houseFilter = array("EXTERNAL_ID" => $house['id'], "TSZH_ID" => $this->tszhID);

		$existingHouse = EpasportTable::getList(array(
			"filter" => $houseFilter,
			"select" => array("ID"),
		))->fetch();
		if (is_array($existingHouse))
		{
			$result = EpasportTable::update($existingHouse["ID"], $houseFields);
		}
		else
		{
			$houseFields['DATE_CREATE'] = new \Bitrix\Main\Type\DateTime();
			$result = EpasportTable::add($houseFields);
		}

		if (!$result->isSuccess())
			throw new FormatException("VDGB_TSZH_HOUSE_ERROR", array('#ERROR#' => implode("\n", $result->getErrorMessages())));
	}


	/**
	 *
	 * @param string $action
	 * @return array
	 * @throws \Exception
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public function Cleanup($action)
	{
		global $DB;

		$counter = array(
		);

		if (!in_array($this->filetype, array("E-Passports")))
			return $counter;

		if ($action!="D" && $action!="A")
			return $counter;

		$bDelete = $action=="D";

		return $counter;
	}

	protected function clearSession()
	{
		global $DB;

		if (!$this->useSessions)
			return;

		// ������ ��� ������ ������� ������
		if ($DB->TableExists($this->tableName))
		{
			$DB->Query("DELETE from " . $this->tableName . " WHERE SESS_ID = '" . $DB->ForSQL($this->sessionId) . "'");

			// ��������� ������� ������ 1 ����
			$this->xmlFile->EndSession();
		}
	}


	/**
	 * ������������� ����� ������ ���������. �������� ������������ ��������� ���� �� ������ ����������� �������
	 */
	protected function setStartTime()
	{
		if (isset($this->startTime))
			return;

		if (defined("START_EXEC_PROLOG_BEFORE_1"))
		{
			list($usec, $sec) = explode(" ", START_EXEC_PROLOG_BEFORE_1);
			$this->startTime = $sec + $usec;
		}
		else
			$this->startTime = microtime(true);
	}

	/**
	 * ��������� �� ������� �� �����, ���������� �� ���� ��� (���) ������
	 * @return bool
	 * @throws \Exception ���� ����� ���� �� ��� ������ ����� setStartTime
	 */
	protected function timeIsUp()
	{
		return microtime(true) - $this->startTime > $this->timeLimit;
	}

}
