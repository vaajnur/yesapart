<?

namespace Vdgb\Tszhepasport\Exchange;

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * ����� ��� ���������� ������ �� ���������
 *
 * @package Vdgb\Tszhepasport\Exchange
 */
class Facade
{

	/** @var string ������� �������� (������������ ��� ���������� ��������� �������� ��������) */
	protected $curPage = '';
	/** @var int ������������ ����� ���������� ������ ���� ������� */
	public $timelimit = 30;
	/** @var string ������ ���� � ����� ��� �������� ����������� ������ (������������ ����� �����) */
	public $dirName = '';

	/** @var array ������ ������� ����������, ������� ����� ��������� ����� ������ */
	public $sessionVars = null;

	/** @var bool true ���� ����������� ������� ����� ����� */
	public $textOutput = false;
	/** @var bool true ���� � ���������� ������� XML */
	public $xmlOutput = false;
	/** @var int ��� �������� �� �������� ����� �������� ���������� ����������� �� ���� ��� ������ */
	public $dataLen = false;
	/** @var bool ����� ������� � ����������� ������������ ���� � ���������� ������ */
	public $debug = false;

	/**
	 * ��������� ������, ������������ ����� � ������� ������
	 *
	 * @var \Vdgb\Tszhepasport\Exchange\ControllerInterface
	 */
	protected $exchange = null;

	/** @var string */
	protected $mode = null;

	/**
	 * ������ �����������, ����������� �������� ������ ������ ������
	 *
	 * @var array
	 */
	protected $versionList;

	/**
	 * ��������� �������� ���� ������ �� ����� � �������� ������������
	 * @var bool
	 */
	protected $skipPersmissionCheck = false;

	/**
	 * @param bool $skipPermissionCheck
	 */
	public function __construct($skipPermissionCheck = false)
	{
		global $APPLICATION;

		$this->versionList = array(
			'4' => __NAMESPACE__ . '\v4\Controller',
		);

		$this->curPage = substr($APPLICATION->GetCurPage(), 0, 22);
		$maxTime = intval(ini_get('max_execution_time'));
		$this->timelimit = \COption::GetOptionInt('vdgb.tszhepasport', '1c_exchange.Interval', min($maxTime > 10 ? $maxTime - 5 : 10, 30));
		$this->debug = \COption::GetOptionInt('vdgb.tszhepasport', '1c_exchange.Debug', "Y") != "N";
		$this->dirName = "/" . \COption::GetOptionString("main", "upload_dir", "upload") . "/tszh_epasport_exchange/";
		$this->skipPersmissionCheck = $skipPermissionCheck;

		$this->sessionVars = &$_SESSION["VDGB_TSZHEPAS_1C_EXCHANGE"];
		if (!is_array($this->sessionVars))
		{
			$this->sessionVars = array();
		}
	}

	/**
	 * ������� ��������� ��������� ������ ��������� ������
	 *
	 * @param string|bool $version ������ ������
	 * @return ControllerInterface
	 * @throws \InvalidArgumentException
	 */
	public function getController($version = false)
	{
		if (!$version)
			$version = $this->getVersion();

		if (!array_key_exists($version, $this->versionList)) {
			throw new \InvalidArgumentException("$version is not valid exchange version");
		}
		$className = $this->versionList[$version];
		return new $className($this);
	}

	/**
	 * ���������� ������������ �������������� ����� ������ ������
	 */
	public function getMaxVersion() {
		$versions = array_keys($this->versionList);
		sort($versions);
		return array_pop($versions);
	}

	/**
	 * ������� ������ ������
	 * �������� ����������� �������������� ������ ��������� �������� ������� ������
	 *
	 * @return mixed
	 */
	public function getVersion()
	{
		return min($this->getRemoteVersion(), $this->getMaxVersion());
	}

	/**
	 * ������������ ������ ������, �������������� ��������� ��������
	 *
	 * @return int
	 */
	public function getRemoteVersion()
	{
		return isset($this->sessionVars["remoteVersion"]) ? $this->sessionVars["remoteVersion"] : 4;
	}

	/**
	 * ������������� ������������ ������ ������, �������������� ��������� ��������
	 *
	 * @param int $version
	 */
	public function setRemoteVersion($version)
	{
		if (!is_numeric($version))
		{
			throw new \InvalidArgumentException("Invalid \$version argument");
		}
		$this->sessionVars["remoteVersion"] = $version;
	}

	/**
	 * ������ ��� ��������� � �������� �����������.
	 * ������������ ������ ?mode=checkauth, � ����� basic-����������� ��� ������ ��������
	 *
	 * ���������� ��������� �� ������ � ��������� WWW-Authenticate (���� ������������ �� �����������, �� ������� ��������� Basic-����������� ��� ������ �� ���������� ����� ��� ������
	 * ������� ��������� �� ������ ��� ������� � ?mode=checkauth, ���� ��� ����������� ��������� ������
	 * @return bool true ���� ����������� ������ �������, false � ������ ������
	 * @throws \Exception ��� ������ ����������� ��� ���� ������������ �� ������� ��������� �����������
	 */
	public function checkAuthorization()
	{
		global $USER, $APPLICATION;
		$authorized = $USER->IsAuthorized();
		if ($_GET["mode"] == "checkauth")
		{
			$authorized = self::basicAuth(true);
		}
		elseif (!$authorized)
		{
			$authorized = self::basicAuth();
			if (!$authorized)
			{
				$APPLICATION->RestartBuffer();
				\CHTTP::SetStatus("401 Not Authorized");
				header('WWW-Authenticate: Basic realm="vdgb:tszhepasport.1c.exchange"');
				throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_AUTHORIZE"));
			}
		}
		if ($authorized && !$this->userHaveAccess())
		{
			throw new \Exception(Loc::getMessage("TSZH_EP_1C_PERMISSION_DENIED"));
		}
		elseif (!\CModule::IncludeModule("citrus.tszh"))
		{
			throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_MODULE"));
		}
		elseif (!\CModule::IncludeModule('vdgb.tszhepasport'))
		{
			throw new \Exception(Loc::getMessage("TSZH_EP_1C_PERMISSION_DENIED"));
		}

		// ���� � ����� ������ ����� � ������� ������ 4, �� ��� ��������� cookies ��� �� �������������� �������� ������ ������ ������ � ��������� �version=4�:
		if (isset($_GET["version"]) && is_numeric($_GET['version']))
		{
			$this->setRemoteVersion($_GET["version"]);
		}

		return $authorized;
	}

	/**
	 * ��������� ������� ?mode=version
	 * ������ ����� � 1� ��� ����������� ������������ �������������� ������ ������ ������.
	 *
	 * ��� �������� ������ � �������, ������� �������������� ������ �������� ������������ ��������,
	 * �� �������� ���� � 1� ������ ������ ������ ���� �����. ��� ���������� ��� ��������, ����� ���� ��� 1�
	 * ����� �� ���������� ������, � ��������� ��������� ����� �� ������� ��������.
	 *
	 * � ����� ������ ���� ������ ��� ������:
	 * success
	 * version=<����� ������>
	 */
	public function processVersion()
	{
		echo "success\nversion=" . $this->getMaxVersion() . "\n";
		$this->textOutput = true;
	}

	/**
	 * ��������� ������� ?mode=init
	 *
	 * � ����� ���� ������ ������:
	 * file_limit=<�����>,
	 * ��� <�����> � ����������� ���������� ������ ����� � ������ ��� �������� �� ���� ������.
	 * ���� ������ ����� ������, �� �� ������ ���� ������� �� ����� � ������������ � ��������� ��������.
	 */
	public function processInit()
	{
		$uploadDir = $_SERVER["DOCUMENT_ROOT"] . $this->dirName . "/";
		$logDir = $uploadDir . 'log/';

		if ($this->debug)
		{
			// ���� �������� �������, ��������� ��� ���������� � ���������� ������� ������ ����� � ��������� �����
			// ����� ��� ��������� �������� ����� � ������������ ������, �� �� ����������� � ������ �����
			CheckDirPath($logDir);
			if (!is_dir(rtrim($logDir, '/')))
			{
				throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_INIT"));
			}

			$moveFiles = scandir($uploadDir);
			foreach ($moveFiles
					 as
					 $file)
			{
				if (in_array($file, array(".", "..")))
				{
					continue;
				}
				if (!rename($uploadDir . $file, $logDir . $file))
				{
					unlink($uploadDir . $file);
				}
			}
		}
		else
		{
			// ���� ������� ���������, ������ ������ ��� ������ �����
			DeleteDirFilesEx($this->dirName);
		}

		CheckDirPath($_SERVER["DOCUMENT_ROOT"] . $this->dirName . "/");

		if (!is_dir(rtrim($uploadDir, '/')))
		{
			throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_INIT"));
		}

		$ht_name = $_SERVER["DOCUMENT_ROOT"] . $this->dirName . "/.htaccess";
		if (!file_exists($ht_name))
		{
			$fp = fopen($ht_name, "w");
			if ($fp)
			{
				fwrite($fp, "Option -Indexes\nOrder Allow,Deny\nDeny from All");
				fclose($fp);
				@chmod($ht_name, BX_FILE_PERMISSIONS);
			}
		}
		$_SESSION["BX_VDGB_TSZHEPAS_IMPORT"] = false;

		$limit = \COption::GetOptionString("vdgb.tszhepasport", "1C_FILE_SIZE_LIMIT", 0);
		if ($limit <= 0)
		{
			// ��������� ������������ ������ ������������ �����
			$limit = min(
				self::returnBytes(ini_get('post_max_size')),
				self::returnBytes(ini_get('memory_limit'))
			);
		}
		echo "file_limit=" . $limit . "\n";

		$this->textOutput = true;
	}

	/**
	 * ��������� ������� ?mode=file
	 *
	 * ��� �������� ������ �� ������� ��������� �� ���� ������������ ������ ����:
	 * http://<����� ��� ����������>/bitrix/admin/tszh_exchange.php?mode=file&type=<���>&filename=<��� �����>
	 * ������� ��������� �� ������ ���� ������, ������� ���������� ����� (��������� ��� ��������� ��� �����)
	 * � ���� ������ POST. � ������ �������� ������ ����� ���� ������ "success".
	 */
	public function processFileUpload()
	{
		$absFilename = false;

		if (isset($_GET["filename"]) && (strlen($_GET["filename"]) > 0))
		{
			$filename = trim(str_replace("\\", "/", trim($_GET["filename"])), "/");
			$_filename = rel2abs($_SERVER["DOCUMENT_ROOT"] . $this->dirName, "/" . $filename);
			if ((strlen($filename) > 1) && ($_filename === "/" . $filename))
			{
				$absFilename = $_SERVER["DOCUMENT_ROOT"] . $this->dirName . $_filename;
			}
		}
		else
			throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_FILENAME"));
		$this->textOutput = true;
		if (!$absFilename)
			throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_FILENAME"));

		//Read http data
		if (function_exists("file_get_contents"))
		{
			$DATA = file_get_contents("php://input");
		}
		elseif (isset($GLOBALS["HTTP_RAW_POST_DATA"]))
		{
			$DATA = &$GLOBALS["HTTP_RAW_POST_DATA"];
		}
		else
		{
			$DATA = false;
		}

		$this->dataLen = defined("BX_UTF") ? mb_strlen($DATA, 'latin1') : strlen($DATA);
		//And save it the file
		if ($this->dataLen > 0)
		{
			CheckDirPath($absFilename);
			$fp = fopen($absFilename, "ab");
			if ($fp)
			{
				$result = fwrite($fp, $DATA);
				if ($result === $this->dataLen)
				{
					echo "success\n" . Loc::getMessage("VDGB_TSZHEPAS_EXCHANGE_FILE_UPLOADED", array("#FILE_NAME#" => $filename, "#SIZE#" => $this->dataLen));
				}
				else
				{
					throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_FILE_WRITE", array("#FILE_NAME#" => $filename)));
				}
			}
			else
			{
				throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_FILE_OPEN", array("#FILE_NAME#" => $filename)));
			}
		}
		else
		{
			throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_HTTP_READ"));
		}
	}

	/**
	 * � ������ ��������� ��������� � ������� ������ � ������� ��������� ����������� ������ ����:
	 * ?mode=export&type=<���>&inn=<��� �����������>&result=success
	 * ��������� ���� ��������� �������� ��������, ���� ������� ������ � ��� ����� ������ � �������������� �������� �������
	 * @return bool ���������� true, ���� ������� ������ � ��� ������ �� ������������� ��������� ��������� �������� � 1�, ����� false
	 * @throws \Exception
	 */
	public function processConfirmRequest()
	{
		if (array_key_exists('result', $_GET) && $_GET['result'] === 'success')
		{
			$curPage = substr($this->curPage, 0, 22);
			$arOrg = $this->getOrg();

			$type = htmlspecialcharsbx(trim($_GET['type']));
			$type = $this->prepareType($type);
			$type = strlen($type) > 0 ? $type : 'unknown';
			// ������ ���� ��������� �������� ��������
			if ($_COOKIE[\COption::GetOptionString("vdgb.tszhepasport", "export_session_name_" . $curPage, "")] == \COption::GetOptionString("vdgb.tszhepasport", "export_session_id_" . $curPage, ""))
			{
				// ���������� last_{$type}_export_time_* ����� ������ ��������
				\COption::SetOptionString("vdgb.tszhepasport", "last_{$type}_export_time_committed_" . $arOrg['ID'], \COption::GetOptionString("vdgb.tszhepasport", "last_{$type}_export_time_" . $arOrg['ID'], ""));
				echo "success\n";
			}
			else
			{
				throw new \Exception(Loc::getMessage("VDGB_TSZHEPAS_EXPORT_SESSION_FAILURE"));
			}

			return true;
		}

		return false;
	}

	public function prepareType($type){
		//if($type == 'E-Passports') 
		return 'epasports';
	}

	/**
	 * @return bool|string
	 */
	public function getLastConfimedDate()
	{
		$arOrg = $this->getOrg();

		$type = htmlspecialcharsbx(trim($_GET['type']));
		$type = $this->prepareType($type);
		$type = strlen($type) > 0 ? $type : 'unknown';
		// ������ ���� ��������� �������� ��������
		$lastExportDate = \COption::GetOptionString("vdgb.tszhepasport", "last_{$type}_export_time_committed_" . $arOrg['ID'], false);
		return $lastExportDate ? $lastExportDate : false;
	}

	/**
	 * ���������� ������ ����� ������� ����������, � �������� ��������� ������� ������ (������������ �� ������ ��������� inn, ���������� ��� �������)
	 * @return array|false ������ ����� ������� ���������� ��� false ���� ������ ���������� �� ������
	 * @throws \Exception
	 */
	public static function getOrg()
	{
		static $arInn2Org = Array();

		$inn = $_REQUEST['inn'];
		if (!array_key_exists($inn, $arInn2Org))
		{
			$arOrg = \CTszh::GetList(Array(), Array('INN' => $inn), false, Array('nTopCount' => 1))->Fetch();
			$arInn2Org[$inn] = is_array($arOrg) ? $arOrg : false;
		}

		if (!is_array($arInn2Org[$inn]) || IntVal($arInn2Org[$inn]["ID"]) <= 0)
			throw new \Exception(GetMessage("VDGB_TSZHEPAS_1C_ERROR_ORG_NOT_FOUND", array("#INN#" => htmlspecialcharsbx($inn))));

		return $arInn2Org[$inn];
	}

	/**
	 * ������ ��� ��������� Basic-�����������. ���������� ������������ �� ������ ���������� � ��������� ������� ������ � ������
	 * @param bool $outputResult
	 * @return bool
	 * @throws \Exception
	 */
	public function basicAuth($outputResult = false)
	{
		global $USER;

		$USER->Logout();
		$authRes = $USER->LoginByHttpAuth();
		if ($authRes === true)
		{
			// ��������, �������� �� ����� �������������� ������
			if (
				(\COption::GetOptionString("main", "use_session_id_ttl", "N") == "Y")
				&& (\COption::GetOptionInt("main", "session_id_ttl", 0) > 0)
				&& !defined("BX_SESSION_ID_CHANGE")
			)
			{
				if ($outputResult)
				{
					throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_SESSION_ID_CHANGE"));
				}

				return false;
			}
			else
			{
				if ($outputResult)
				{
					$this->textOutput = true;
					echo "success\n";
					echo session_name() . "\n";
					echo session_id() . "\n";
				}
				\COption::SetOptionString("vdgb.tszhepasport", "export_session_name_" . $this->curPage, session_name());
				\COption::SetOptionString("vdgb.tszhepasport", "export_session_id_" . $this->curPage, session_id());

				return true;
			}
		}
		else
		{
			$message = ($authRes["MESSAGE"] ? str_replace('<br>', "", $authRes["MESSAGE"]) : Loc::getMessage("TSZH_EP_1C_ERROR_LOGIN"));
			\CHTTP::SetStatus("401 Not Authorized");
			header('WWW-Authenticate: Basic realm="' . \CUtil::translit($message, LANGUAGE_ID, array(
					"max_len" => 256,
					"change_case" => false,
					"replace_space" => ' ',
					"replace_other" => ' ',
					"delete_repeat_replace" => false,
					"safe_chars" => '',
				)) . ' (vdgb.tszhepasport.1c.exchange)"');
			$this->textOutput = true;
			if ($outputResult)
			{
				throw new \Exception($message);
			}

			return false;
		}
	}

	public function processListFiles()
	{
		$absPath = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . '/' . $this->dirName);
		$this->textOutput = true;
		echo "Files in $this->dirName:\n\n";
		passthru("cd $absPath; ls -lR");
	}

	/**
	 * @return bool true, ���� � �������� ������������ ���������� ���� �� ���������� ������
	 */
	public function userHaveAccess()
	{
		global $APPLICATION;
		return $this->skipPersmissionCheck || $APPLICATION->GetGroupRight("vdgb.tszhepasport") >= 'U';
	}

	/**
	 * @param $val
	 * @return int
	 */
	protected static function returnBytes($val)
	{
		$val = trim($val);
		$last = strtolower($val[strlen($val) - 1]);
		switch ($last)
		{
			/** @noinspection PhpMissingBreakStatementInspection */
			case 'g':
				$val *= 1024;
			/** @noinspection PhpMissingBreakStatementInspection */
			case 'm':
				$val *= 1024;
			/** @noinspection PhpMissingBreakStatementInspection */
			case 'k':
				$val *= 1024;
			default:
				$val = intval($val);
		}

		return $val;
	}

	/**
	 * ���������� ������ � ������ �������
	 * @param string $message ����� ������
	 * @param string $severity ��������� (INFO, ERROR, WARNING, SECURITY, DEBUG)
	 * @param bool|string $itemId ������ �������
	 */
	public function logMessage($message, $severity = "INFO", $itemId = false)
	{
		global $DB;

		if ($severity == "DEBUG" && !$this->debug)
		{
			false;
		}

		$logFields = Array(
			"SEVERITY" => $severity,
			"AUDIT_TYPE_ID" => Loc::getMessage("VDGB_TSZHEPAS_EXCHANGE_AUDIT_TYPE"), // �������
			"MODULE_ID" => "vdgb.tszhepasport",
			"DESCRIPTION" => nl2br($message), // ���������
			"ITEM_ID" => $itemId ? $itemId : session_id()
		);

		// ���� ��� ���� ������� ��������� � ������� ����� � ���� ������ �������� ����� ������
		$logExists = \CEventLog::GetList(array(), array(
			"REQUEST_URI" => preg_replace("/(&?sessid=[0-9a-z]+)/", "", $_SERVER["REQUEST_URI"]),
			"SEVERITY" => $logFields["SEVERITY"],
			"AUDIT_TYPE_ID" => $logFields["AUDIT_TYPE_ID"],
			"MODULE_ID" => $logFields["MODULE_ID"],
			"ITEM_ID" => $logFields["ITEM_ID"],
		))->Fetch();
		if ($logExists)
		{
			$updateFields = array("DESCRIPTION" => $logExists["DESCRIPTION"] . "<br>" . nl2br(trim($message)));
			$strUpdate = $DB->PrepareUpdate("b_event_log", $updateFields);
			$DB->Query("UPDATE b_event_log SET ${strUpdate} WHERE ID=${logExists["ID"]}");

			return $logExists["ID"];
		}
		else
		{
			return \CEventLog::Add($logFields);
		}
	}

	/**
	 * ���������� ��������� ���� � ����� ������������ ����� � ������ $filename
	 *
	 * @param bool $filename ��� �����. ���� �� �������, ������������ $_GET["filename"]
	 * @return string ���������� ���� � �����
	 * @throws \Exception
	 */
	public function getUploadedFile($filename = false)
	{
		$absFilename = false;
		if (false === $filename)
		{
			$filename = $_GET["filename"];
		}
		if (isset($filename) && (strlen($filename) > 0))
		{
			$filename = trim(str_replace("\\", "/", trim($filename)), "/");
			$_absFilename = rel2abs($_SERVER["DOCUMENT_ROOT"] . $this->dirName, "/" . $filename);
			if ((strlen($_absFilename) > 1) && ($_absFilename === "/" . $filename))
			{
				$absFilename = $_SERVER["DOCUMENT_ROOT"] . $this->dirName . $_absFilename;
			}
			else
			{
				throw new \Exception("Invalid filename");
			}
		}

		if (!$absFilename)
		{
			throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_FILENAME"));
		}

		if (!file_exists($absFilename) || !is_file($absFilename))
		{
			throw new \Exception(Loc::getMessage("TSZH_EP_1C_IMPORT_FILE_NOT_FOUND"));
		}

		return $absFilename;
	}

	/**
	 * ��������� �������� ��������� datebeg, ���� �� ��� ������, ���� false
	 * @return bool|string ���������� ������������� ���� ��� ������� (DD.MM.YYYY[ HH:II:SS])
	 * @throws \Exception
	 */
	public static function dateFrom()
	{
		if (is_set($_GET, 'datebeg') && ($dateFrom = htmlspecialcharsbx($_GET['datebeg'])))
		{
			if (!CheckDateTime($dateFrom))
			{
				throw new \Exception(Loc::getMessage("VDGB_TSZHEPAS_EXCHANGE_WRONG_DATE_PARAM", array("#PARAM#" => "datebeg")));
			}

			return $dateFrom;
		}

		return false;
	}

	/**
	 * ��������� �������� ��������� dateend, ���� �� ��� ������, ���� false
	 * ���� ����� ���� ������� ��� �������, � ����� ������ �������� ������ ���� �� ���� ������� ������ ���������. ����� ���� ����� ��������� � ������ ���� ������-���-�����
	 * @return bool|string ���������� ������������� ������� (DD.MM.YYYY HH:II:SS)
	 * @throws \Exception
	 */
	public static function dateTo()
	{
		if (is_set($_GET, 'dateend') && ($dateTo = htmlspecialcharsbx($_GET['dateend'])))
		{
			if (!CheckDateTime($dateTo))
			{
				throw new \Exception(Loc::getMessage("VDGB_TSZHEPAS_EXCHANGE_WRONG_DATE_PARAM", array("#PARAM#" => "dateend")));
			}
			$arDateTo = ParseDateTime($dateTo);
			if (!isset($arDateTo["HH"]) && !isset($arDateTo["H"]) && !isset($arDateTo["GG"]) && !isset($arDateTo["G"]))
			{
				$dateTo = ConvertTimeStamp(
					AddToTimeStamp(array("HH" => 23, "MI" => 59, "SS" => 59), MakeTimeStamp($dateTo)),
					"FULL"
				);
			}

			return $dateTo;
		}

		return false;
	}

	/**
	 * ������������ ������ �� ����� 
	 *
	 * @param string $type ������ �������� 
	 * @throws \Exception
	 */
	public function processExchangeRequest($type)
	{
		$controller = $this->getController();
		$controller->processRequest($type);
	}

	/**
	 * ��������� � ����� ������ � �����
	 *
	 * @param string $contents
	 */
	protected function response($contents)
	{
		global $APPLICATION;

		if (toUpper(LANG_CHARSET) != "WINDOWS-1251")
		{
			$contents = $APPLICATION->ConvertCharset($contents, LANG_CHARSET, "windows-1251");
			if ($this->xmlOutput)
			{
				$contents = preg_replace('#^(<\?xml[^>]*?)encoding="([^"]+)"(.*\?>)#', '$1encoding="windows-1251"$3', $contents);
			}
		}
		$contentLength = (function_exists("mb_strlen") ? mb_strlen($contents, 'latin1') : strlen($contents));

		if ($this->textOutput)
		{
			header("Content-Type: text/plain; charset=windows-1251");
			if (stripos($contents, 'success') === 0)
			{
				$messages = explode("\n", $contents);
				array_shift($messages);
				$this->logMessage(Loc::getMessage("VDGB_TSZHEPAS_EXCHANGE_SUCCESS") . "\n\n" . implode("\n", $messages), "WARNING");
			}
			elseif (stripos($contents, 'warning') === 0)
			{
				$messages = explode("\n", $contents);
				array_shift($messages);
				$this->logMessage(Loc::getMessage("VDGB_TSZHEPAS_EXCHANGE_WARNING") . "\n\n" . implode("\n", $messages), "WARNING");
			}
			elseif (stripos($contents, 'failure') === 0)
			{
				$messages = explode("\n", $contents);
				array_shift($messages);
				$this->logMessage(Loc::getMessage("VDGB_TSZHEPAS_EXCHANGE_ERROR") . ":\n" . implode("\n", $messages), "ERROR");
			}
			elseif (stripos($contents, 'progress') === 0 && $this->debug)
			{
				$messages = explode("\n", $contents);
				array_shift($messages);
				$this->logMessage(implode("\n", $messages));
			}
			elseif ($this->debug)
			{
				$this->logMessage(Loc::getMessage("VDGB_TSZHEPAS_EXCHANGE_RESPONSE") . ":\n" . $contents);
			}
		}
		else
		{
			header("Content-Type: text/html; charset=windows-1251");
			if ($this->debug)
			{
				$filename = $this->dirName . '/export/' . time() . '.html';
				$absFilename = $_SERVER['DOCUMENT_ROOT'] . $filename;
				CheckDirPath($absFilename);
				file_put_contents($absFilename, $contents);
				$this->logMessage(Loc::getMessage("VDGB_TSZHEPAS_EXCHANGE_RESPONSE_FILE") . $filename . "\n", "DEBUG");
			}
		}

		// TODO ������� $this->sessionVars �� ��������� ������ ��������

		echo $contents;
		die();
	}

	/**
	 * ��������� �������� ������� �� ��������� ������
	 *
	 * @param string $type Get-�������� type
	 */
	public function handleRequest($mode, $type)
	{
		global $APPLICATION;

		$type = $this->prepareType($type);

		@set_time_limit(0);
		$APPLICATION->RestartBuffer();
		header("Pragma: no-cache");

		ob_start();

		try
		{
			if ($mode == "version")
			{
				$this->processVersion();
			}
			elseif ($mode == 'debug')
			{
				echo '<pre>$this->sessionVars = ' . htmlspecialcharsbx(var_export($this->sessionVars, true)) . ";</pre>";
			}
			elseif ($this->checkAuthorization())
			{
				if ($mode == "checkauth")
				{
					// ����������� ��� mode=checkauth ��� ��������� � ������ $this->checkAuthorization()
				}
				elseif ($mode == "import")
				{
					$this->processExchangeRequest($type);
				}
				elseif ($mode == "init")
				{
					$this->processInit();
				}
				elseif ($mode == "file")
				{
					$this->processFileUpload();
				}
				elseif ($mode == "listfiles")
				{
					$this->processListFiles();
				}
				else
				{
					throw new \Exception(Loc::getMessage("TSZH_EP_1C_ERROR_UNKNOWN_COMMAND"));
				}
			}
		}
		catch (\Exception $e)
		{
			$this->textOutput = true;
			echo "failure\n", $e->GetMessage();
			if ($this->debug)
			{
				//echo "\n";
				//echo $e->getTraceAsString();
			}
		}

		$contents = ob_get_contents();
		ob_end_clean();

		$this->response($contents);
	}
}
