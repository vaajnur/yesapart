<?

namespace Vdgb\Tszhepasport\Exchange;

/**
 * Interface ControllerInterface
 * @package Vdgb\Tszhepasport\Controller
 */
interface ControllerInterface
{
	/**
	 * @param Facade $facade
	 */
	function __construct(Facade $facade);

	/**
	 * @param string $mode
	 * @param string $service
	 * @return bool
	 */
	public function hasService($service);

	/**
	 * @param string $mode
	 * @param string $service
	 * @return ServiceBase
	 */
	public function getService($service);

	/**
	 * @param string $mode
	 * @param string $service
	 */
	public function processRequest($service);
}
