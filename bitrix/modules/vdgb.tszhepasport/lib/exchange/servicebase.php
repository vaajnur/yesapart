<?

namespace Vdgb\Tszhepasport\Exchange;

/**
 * ������� ����� ��� �������� ������������� ��� ����� ��������
 *
 * @package Vdgb\Tszhepasport\Controller
 */
abstract class ServiceBase
{
	protected	$controller = null,
				$facade = null;
	/**
	 * @param ControllerInterface $controller
	 * @param Facade $facade
	 */
	function __construct(ControllerInterface $controller, Facade $facade)
	{
		$this->controller = $controller;
		$this->facade = $facade;
	}

	public function run() {

	}

	/**
	 * @return ControllerInterface
	 */
	public function getController()
	{
		return $this->controller;
	}

	/**
	 * @return Facade
	 */
	public function getFacade()
	{
		return $this->facade;
	}
}
