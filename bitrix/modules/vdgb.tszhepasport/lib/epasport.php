<?php

namespace Vdgb\Tszhepasport;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

/**
 * Class EpasportTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> HOUSE_ID int mandatory
 * <li> EP_SERIES int optional
 * <li> EP_WEAR double optional
 * <li> EP_LIVING_ROOMS int optional
 * <li> EP_NONLIVING_ROOMS int optional 
 * <li> EP_PEOPLE int optional 
 * <li> EP_ACCOUNTS int optional 
 * <li> EP_PEOPLE_ACCOUNTS int optional 
 * <li> EP_COMPANY_ACCOUNTS int optional 
 * <li> EP_YEAR_OF_COMMISSIONING  int optional 
 * <li> EP_REG_PEOPLE int optional 
 * <li> EP_HTML text optional
 * <li> EP_LATITUDE double optional
 * <li> EP_LONGITUDE double optional
 * <li> DATE_CREATE datetime mandatory
 * <li> DATE_UPDATE datetime mandatory
 * </ul>
 *
 * @package Bitrix\Tszh
 **/
class EpasportTable extends Entity\DataManager implements IEntity
{
	/**
	 * @var array|null
	 */
	private static $editableMap = null;

	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_epasports';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{

		$sqlHelper = Application::getConnection()->getSqlHelper();

		return array(
			'EP_ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('EPAS_ENTITY_ID_FIELD'),
			),
			'EP_HOUSE_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('EPAS_ENTITY_HOUSE_ID_FIELD'),
			),
			'EP_SERIES' => array(
				'data_type' => 'integer',
//				'validation' => array(__CLASS__, 'validateInt'),
				'title' => Loc::getMessage('EPAS_ENTITY_EP_SERIES_FIELD'),
			),
			'EP_WEAR' => array(
				'data_type' => 'float',
//				'validation' => array(__CLASS__, 'validateFloat'),
				'title' => Loc::getMessage('EPAS_ENTITY_EP_WEAR_FIELD'),
			),
			'EP_LIVING_ROOMS' => array(
				'data_type' => 'integer',
//				'validation' => array(__CLASS__, 'validateInt'),
				'title' => Loc::getMessage('EPAS_ENTITY_EP_LIVING_ROOMS_FIELD'),
			),
			'EP_NONLIVING_ROOMS' => array(
				'data_type' => 'integer',
//				'validation' => array(__CLASS__, 'validateInt'),
				'title' => Loc::getMessage('EPAS_ENTITY_EP_NONLIVING_ROOMS_FIELD'),
			),
			'EP_PEOPLE' => array(
				'data_type' => 'integer',
//				'validation' => array(__CLASS__, 'validateInt'),
				'title' => Loc::getMessage('EPAS_ENTITY_EP_PEOPLE_FIELD'),
			),
			'EP_ACCOUNTS' => array(
				'data_type' => 'integer',
//				'validation' => array(__CLASS__, 'validateInt'),
				'title' => Loc::getMessage('EPAS_ENTITY_EP_ACCOUNTS_FIELD'),
			),
			'EP_PEOPLE_ACCOUNTS' => array(
				'data_type' => 'integer',
//				'validation' => array(__CLASS__, 'validateInt'),
				'title' => Loc::getMessage('EPAS_ENTITY_EP_PEOPLE_ACCOUNTS_FIELD'),
			),
			'EP_COMPANY_ACCOUNTS' => array(
				'data_type' => 'integer',
//				'validation' => array(__CLASS__, 'validateInt'),
				'title' => Loc::getMessage('EPAS_ENTITY_EP_COMPANY_ACCOUNTS_FIELD'),
			),
			'EP_REG_PEOPLE' => array(
				'data_type' => 'integer',
//				'validation' => array(__CLASS__, 'validateInt'),
				'title' => Loc::getMessage('EPAS_ENTITY_EP_REG_PEOPLE_FIELD'),
			),
			'EP_HTML' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('EPAS_ENTITY_EP_HTML_FIELD'),
			),
			'EP_LATITUDE' => array(
				'data_type' => 'float',
//				'validation' => array(__CLASS__, 'validateFloat'),
				'title' => Loc::getMessage('EPAS_ENTITY_EP_LATITUDE_FIELD'),
			),
			'EP_LONGITUDE' => array(
				'data_type' => 'float',
//				'validation' => array(__CLASS__, 'validateFloat'),
				'title' => Loc::getMessage('EPAS_ENTITY_EP_LONGITUDE_FIELD'),
			),
			'EP_HOUSE_IMG_ID' => array(
				'data_type' => 'integer',
//				'validation' => array(__CLASS__, 'validateInt'),
				'title' => Loc::getMessage('EPAS_ENTITY_EP_HOUSE_IMG_ID'),
			),
			'DATE_CREATE' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('EPAS_ENTITY_DATE_CREATE_FIELD'),
			),
			'DATE_UPDATE' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('EPAS_ENTITY_DATE_UPDATE_FIELD'),
			),
			new Entity\ReferenceField(
				'HOUSE',
				'\Citrus\Tszh\HouseTable',
				array('=this.EP_HOUSE_ID' => 'ref.ID')
			),
		);
	}

	/**
	 * Returns validators for Address field.
	 *
	 * @return array
	 */
	public static function validateAddress()
	{
		return array(
			new Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for STREET field.
	 *
	 * @return array
	 */
	public static function validateStreet()
	{
		return array(
			new Entity\Validator\Length(null, 100),
		);
	}

	/**
	 * Returns validators for HOUSE field.
	 *
	 * @return array
	 */
	public static function validateHouse()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}


	/**
	 * ���������� ������ � ������� ����� ��������
	 * @return array
	 */
	public static function getFieldNames()
	{
		return array_keys(static::getEditableFields());
	}

	/**
	 * @param string $fieldName
	 * @return string
	 * @throws \Exception
	 */
	public static function getFieldTitle($fieldName)
	{
		$map = static::getMap();
		if (array_key_exists($fieldName, $map))
			return $map[$fieldName]['title'];
		else
			throw new ArgumentException('Unknown $fieldName specified', 'fieldName');
	}

	/**
	 * ���������� ������ ������������� ����� ���������� ������ getMap()
	 * @return array
	 */
	public static function getEditableFields()
	{
		if (!isset(static::$editableMap))
		{
			static::$editableMap = array();
			$map = static::getMap();
			foreach ($map as $fieldName => $fieldSettings)
			{
				if (!array_key_exists('expression', $fieldSettings))
					static::$editableMap[$fieldName] = $fieldSettings;
			}
		}

		return static::$editableMap;
	}
}
