<?
// ��������� ��� ����������� �����:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/include.php"); // ������������� ������
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/prolog.php"); // ������ ������

// ��������� �������� ����
IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$sTableID = "tbl_periods"; // ID �������
$oSort = new CAdminSorting($sTableID, "ID", "DESC"); // ������ ����������
$lAdmin = new CAdminList($sTableID, $oSort); // �������� ������ ������
//echo '<pre>'; var_dump($oSort); echo '</pre>';

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = $arListFilter = array();
if (CTszhFunctionalityController::isPortal() && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups, "ENTITY_TYPE" => "tszh")
		)
	);

	while($arPerms = $rsPerms->fetch())
	{
		if($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["ENTITY_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = $arListFilter["@TSZH_ID"] = array_keys($arTszhRight);
}

// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
	global $FilterArr, $lAdmin;
	foreach ($FilterArr as $f) global $$f;

	// � ������ ������ ��������� ������.
	// � ����� ������ ����� ��������� �������� ���������� $find_���
	// � � ������ �������������� ������ ���������� �� �����������
	// ����������� $lAdmin->AddFilterError('�����_������').

	return count($lAdmin->arFilterErrors)==0; // ���� ������ ����, ������ false;
}
// *********************** /CheckFilter ******************************* //

// ������ �������� �������
$FilterArr = Array(
	"find",
	"find_type",
	"find_id",
	"find_tszh",
	"find_active",
	"find_only_debt",
);

$find_tszh_tmp = IntVal($_REQUEST['find_tszh']);
// �������������� ������
$lAdmin->InitFilter($FilterArr);
$find_tszh = $find_tszh_tmp;

$arFilter = Array();
// ���� ��� �������� ������� ���������, ���������� ���
if (CheckFilter())
{
	if (strlen($find) > 0 && strlen($find_type) > 0) {
		switch ($find_type) {
			case 'ID':
				$arFilter['ID'] = $find;
				break;
			default:
		}
	}
	if (strlen($find_id) > 0) {
		$arFilter['ID'] = $find_id;
	}
	if (intval($find_tszh) > 0) {
		$arFilter['TSZH_ID'] = $find_tszh;
	}
	if ($_REQUEST['find_active'] == 'Y' || $_REQUEST['find_active'] == 'N') {
		$arFilter['ACTIVE'] = $_REQUEST['find_active'];
	}
	if ($_REQUEST['find_only_debt'] == 'Y' || $_REQUEST['find_only_debt'] == 'N') {
		$arFilter['ONLY_DEBT'] = $_REQUEST['find_only_debt'];
	}
}
// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //

// ���������� ����������������� ���������
if($lAdmin->EditAction() && $POST_RIGHT>="W")
{
	// ������� �� ������ ���������� ���������
	foreach($FIELDS as $ID=>$arFields)
	{
		if(!$lAdmin->IsUpdated($ID))
			continue;

		// �������� ��������� ������� ��������
		$DB->StartTransaction();
		$ID = IntVal($ID);
		if ($arData = CTszhPeriod::GetByID($ID))
		{
			if(!CTszhPeriod::Update($ID, $arFields))
			{
				$lAdmin->AddGroupError(GetMessage("TP_ERROR_UPDATE"), $ID);
				$DB->Rollback();
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("TP_ERROR_UPDATE_NOT_FOUND"), $ID);
			$DB->Rollback();
		}
		$DB->Commit();
	}
}

// ��������� ��������� � ��������� ��������
if(($arID = $lAdmin->GroupAction()) && $POST_RIGHT=="W")
{
	// ���� ������� "��� ���� ���������"
	if($_REQUEST['action_target']=='selected')
	{
		$arSort = Array();
		$arSortBy = explode('|', $by);
		foreach ($arSortBy as $sBy) {
			$arSort[$sBy] = $order;
		}

		$rsData = CTszhPeriod::GetList($arSort, $arFilter);
		while ($arRes = $rsData->Fetch())
			$arID[] = $arRes['ID'];
	}

	// ������� �� ������ ���������
	foreach($arID as $ID)
	{
		if(strlen($ID)<=0)
			continue;
		$ID = IntVal($ID);

		// ��� ������� �������� �������� ��������� ��������
		switch($_REQUEST['action'])
		{
			// ��������
			case "delete":
				@set_time_limit(0);
				$DB->StartTransaction();
				if (!CTszhPeriod::Delete($ID)) {
					$DB->Rollback();
					$lAdmin->AddGroupError(GetMessage("TP_ERROR_DELETE"), $ID);
				}
				$DB->Commit();
				break;
		}
	}
}
// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //


$arSort = Array();
$arSortBy = explode('|', $by);
foreach ($arSortBy as $sBy) {
	$arSort[$sBy] = $order;
}

$rsData = CTszhPeriod::GetList(
	$arSort,
	array_merge($arFilter, $arListFilter),
	false,
	false,
	Array('*')
);

// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("TP_PERIODS_NAV")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

$lAdmin->AddHeaders(array(
	array(
		"id"    =>"ID",
		"content"  =>"ID",
		"sort"    => "ID",
		"align"    =>"left",
		"default"  =>true,
	),
	array(
		"id"    =>"PERIOD",
		"content"  => GetMessage("TP_F_PERIOD"),
		"sort"    => "DATE",
		"align"    =>"left",
		"default"  =>true,
	),
	array(
		"id"    =>"ACTIVE",
		"content"  => GetMessage("TP_F_ACTIVE"),
		"sort"    => "ACTIVE",
		"align"    =>"left",
		"default"  =>true,
	),
	array(
		"id"    =>"ONLY_DEBT",
		"content"  => GetMessage("TP_F_ONLY_DEBT"),
		"sort"    => "ONLY_DEBT",
		"align"    =>"left",
		"default"  =>true,
	),
	array(
		"id"    =>"TSZH_ID",
		"content"  => GetMessage("TP_F_TSZH_ID"),
		"sort"    => "TSZH_ID",
		"align"    =>"left",
		"default"  =>true,
	),
	array(
		"id"    =>"DATE",
		"content"  => GetMessage("TP_F_DATE"),
		"sort"    => "DATE",
		"align"    =>"left",
		"default"  =>false,
	),
	array(
		"id"    =>"TIMESTAMP_X",
		"content"  => GetMessage("TP_F_TIMESTAMP_X"),
		"sort"    => "TIMESTAMP_X",
		"align"    =>"left",
		"default"  =>true,
	),
	array(
		"id"    =>"ACCOUNTS",
		"content"  => GetMessage("TP_F_ACCOUNTS"),
		"align"    =>"left",
		"default"  =>true,
	),
));

while($arRes = $rsData->NavNext(true, "f_")):

	// ������� ������. ��������� - ��������� ������ CAdminListRow
	$row =& $lAdmin->AddRow($f_ID, $arRes);

	// ����� �������� ����������� �������� ��� ��������� � �������������� ������

	$row->AddViewField('PERIOD', CTszhPeriod::Format($arRes['DATE']));
	$row->AddViewField('DATE', $DB->FormatDate($arRes['DATE'], 'YYYY-MM-DD', CSite::GetDateFormat("SHORT")));
	$row->AddInputField("DATE", array("size"=>10));
	$row->AddViewField('TIMESTAMP_X', $DB->FormatDate($arRes['TIMESTAMP_X'], 'YYYY-MM-DD HH:MI:SS', CSite::GetDateFormat("FULL")));
	
	$row->AddViewField('ACTIVE', $arRes["ACTIVE"] != 'N' ? GetMessage("TP_YES") : GetMessage("TP_NO"));
	$row->AddViewField('ONLY_DEBT', $arRes["ONLY_DEBT"] == 'Y' ? GetMessage("TP_YES") : GetMessage("TP_NO"));

	if ($f_TSZH_ID > 0) {
		$arTszh = CTszh::GetByID($f_TSZH_ID);
	  	$row->AddViewField('TSZH_ID', '[<a href="tszh_edit.php?ID=' . $f_TSZH_ID . '&lang=' . LANGUAGE_ID . '" title="' . GetMessage("AL_VIEW_TSZH") . '">' . $f_TSZH_ID . '</a>] ' . $arTszh["NAME"]);
	} else {
	  	$row->AddViewField('TSZH_ID', '�');
	}

	$row->AddViewField('ACCOUNTS', "[<a href=\"/bitrix/admin/tszh_account_period_list.php?LANG=".LANG."&amp;find_period={$arRes['ID']}\">" . GetMessage("TP_ACCOUNTS_LIST") . "</a>]");


	// ���������� ����������� ����
	$arActions = Array();

	if ($POST_RIGHT>="W") {
		// �������� ��������
		$arActions[] = array(
			"ICON"=>"edit",
			"TEXT"=> GetMessage("TP_M_EDIT"),
			"DEFAULT"=>"Y",
			"ACTION"=>$lAdmin->ActionRedirect("tszh_periods_edit.php?ID=".$f_ID.'&LANG='.LANG)
		);

		$arActions[] = array(
			"ICON"=>"list",
			"TEXT"=> GetMessage("TP_M_ACCOUNTS"),
			"ACTION"=>$lAdmin->ActionRedirect("tszh_account_period_list.php?LANG=".LANG."&amp;find_period=".$f_ID),
		);

		// �������� ��������
		$arActions[] = array(
			"ICON"=>"delete",
			"TEXT"=> GetMessage("TP_M_DELETE"),
			"ACTION"=>"if(confirm('" . GetMessage("TP_M_DELETE_CONFIRM") . "')) ".$lAdmin->ActionDoGroup($f_ID, "delete")
		);
	}

	// ������� �����������
	//$arActions[] = array("SEPARATOR"=>true);

	// ���� ��������� ������� - �����������, �������� �����.
	if(is_set($arActions[count($arActions)-1], "SEPARATOR"))
		unset($arActions[count($arActions)-1]);

	// �������� ����������� ���� � ������
	$row->AddActions($arActions);

endwhile;

// ������ �������
$lAdmin->AddFooter(
	array(
		array("title"=> GetMessage("TP_CNT_TOTAL") . ":", "value"=>$rsData->SelectedRowsCount()), // ���-�� ���������
		array("counter"=>true, "title"=> GetMessage("TP_CNT_SELECTED") . ":", "value"=>"0"), // ������� ��������� ���������
	)
);

// ��������� ��������
$lAdmin->AddGroupActionTable(Array(
	"delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
/*  "activate"=>GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // ������������ ��������� ��������
  "deactivate"=>GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // �������������� ��������� ��������*/
));

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //

// ���������� ���� �� ������ ������ - ���������� ��������
$aContext = array(
	array(
		"TEXT"=> GetMessage("TP_C_ADD_PERIOD"),
		"LINK"=>"tszh_periods_edit.php?lang=".LANG,
		"TITLE"=> GetMessage("TP_C_ADD_PERIOD_TITLE"),
		"ICON"=>"btn_new",
	),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //

// �������������� �����
$lAdmin->CheckListMode();

// ��������� ��������� ��������
$APPLICATION->SetTitle(GetMessage("TP_PAGE_TITLE"));

// �� ������� ��������� ���������� ������ � �����
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

// �������� ������ �������
$oFilter = new CAdminFilter(
	$sTableID."_filter",
	array(
		"ID",
		GetMessage("TP_F_TSZH_ID"),
		GetMessage("TP_F_ACTIVE"),
		GetMessage("TP_F_ONLY_DEBT"),
	)
);
?>
<form name="find_form" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
<?$oFilter->Begin();?>
<tr>
  <td><b><?=GetMessage("TP_FIND")?>:</b></td>
  <td>
    <input type="text" size="25" name="find" value="<?echo htmlspecialcharsbx($find)?>" title="">
    <?
		$arr = array(
			"reference_id" => array(
				"ID",
			),
			"reference" => array(
				"ID",
			),
		);
    echo SelectBoxFromArray("find_type", $arr, $find_type, "", "");
    ?>
  </td>
</tr>
<tr>
  <td><?="ID"?>:</td>
  <td>
    <input type="text" name="find_id" size="47" value="<?echo htmlspecialcharsbx($find_id)?>">
  </td>
</tr>
<tr>
	<td><?echo GetMessage("TP_F_TSZH_ID");?>:</td>
	<td>
		<select name="find_tszh">
			<option value=""><?= htmlspecialcharsex(GetMessage("TP_F_ALL")) ?></option>
			<?
			$dbTszh = CTszh::GetList(Array("NAME" => "ASC"), $arTszhFilter);
			while ($arTszh = $dbTszh->Fetch())
			{
				?><option value="<?= $arTszh["ID"] ?>"<?if ($arTszh["ID"] == $find_tszh) echo " selected";?>>[<?= htmlspecialcharsex($arTszh["ID"]) ?>]&nbsp;<?= htmlspecialcharsex($arTszh["NAME"]) ?></option><?
			}
			?>
		</select>
	</td>
</tr>
<tr>
  <td><?=GetMessage("TP_F_ACTIVE")?>:</td>
  <td>
    <select name="find_active" size="1">
    	<option value=""<?=($_REQUEST['find_active'] != 'Y' && $_REQUEST['find_active'] != 'N' ? ' checked="checked"' : '')?>>(<?=GetMessage("TP_ALL")?>)</option>
		<option value="Y"<?=($_REQUEST['find_active'] == 'Y' ? ' checked="checked"' : '')?>><?=GetMessage("TP_YES")?></option>
		<option value="N"<?=($_REQUEST['find_active'] == 'N' ? ' checked="checked"' : '')?>><?=GetMessage("TP_NO")?></option>
	</select>
  </td>
</tr>
<tr>
  <td><?=GetMessage("TP_F_ONLY_DEBT")?>:</td>
  <td>
    <select name="find_only_debt" size="1">
    	<option value=""<?=($_REQUEST['find_only_debt'] != 'Y' && $_REQUEST['find_active'] != 'N' ? ' checked="checked"' : '')?>>(<?=GetMessage("TP_ALL")?>)</option>
		<option value="Y"<?=($_REQUEST['find_only_debt'] == 'Y' ? ' checked="checked"' : '')?>><?=GetMessage("TP_YES")?></option>
		<option value="N"<?=($_REQUEST['find_only_debt'] == 'N' ? ' checked="checked"' : '')?>><?=GetMessage("TP_NO")?></option>
	</select>
  </td>
</tr>
<?
$oFilter->Buttons(array("table_id"=>$sTableID,"url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
$oFilter->End();
?>
</form>

<?
// ������� ������� ������ ���������
$lAdmin->DisplayList();
?>

<?
// ���������� ��������
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>
