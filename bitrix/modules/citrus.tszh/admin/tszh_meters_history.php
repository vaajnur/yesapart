<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/include.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/prolog.php");

IncludeModuleLangFile(__FILE__);

// �������� ��������
ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT <= "D")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$UF_ENTITY = "TSZH_METER_VALUE";

$sTableID = "tbl_meters_history"; // ID �������
$oSort = new CAdminSorting($sTableID, "ID", "DESC"); // ������ ����������
$lAdmin = new CAdminList($sTableID, $oSort); // �������� ������ ������

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = $arListFilter = array();
if (CModule::IncludeModule("vdgb.portaltszh") && defined("TSZH_PORTAL") && TSZH_PORTAL === true && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups, "ENTITY_TYPE" => "tszh")
		)
	);

	while ($arPerms = $rsPerms->fetch())
	{
		if ($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["ENTITY_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = $arListFilter["@TSZH_ID"] = array_keys($arTszhRight);
}

$arTszhList = Array();
$rsTszh = CTszh::GetList(Array("NAME" => "ASC"), $arTszhFilter, false, false, Array("ID", "NAME"));
while ($arTszh = $rsTszh->Fetch())
{
	$arTszhList[$arTszh['ID']] = $arTszh['NAME'];
}

$arHeaders = array(

	array("id" => "ID", "content" => GetMessage("TSZH_F_ID"), "sort" => "id", "align" => "right", "default" => true, "type" => "int"),
	array(
		"id" => "METER_ID",
		"content" => GetMessage("TSZH_F_METER_ID"),
		"sort" => "meter_id",
		"align" => "left",
		"default" => true,
		"type" => "int",
	),
	/*array(
		"id" => "ACCOUNT_ID",
		"content" => GetMessage("TSZH_F_ACCOUNT_ID"),
		"sort" => "account_id",
		"align" => "left",
		"default" => true,
		"type" => "account",
	),*/
	array(
		"id" => "VALUE1",
		"content" => GetMessage("TSZH_F_VALUEN", Array('#N#' => 1)),
		"sort" => "value1",
		"align" => "left",
		"default" => true,
		"type" => "float",
	),
	array(
		"id" => "VALUE2",
		"content" => GetMessage("TSZH_F_VALUEN", Array('#N#' => 2)),
		"sort" => "value2",
		"align" => "left",
		"default" => true,
		"type" => "float",
	),
	array(
		"id" => "VALUE3",
		"content" => GetMessage("TSZH_F_VALUEN", Array('#N#' => 3)),
		"sort" => "value3",
		"align" => "left",
		"default" => false,
		"type" => "float",
	),
	array(
		"id" => "AMOUNT1",
		"content" => GetMessage("TSZH_F_AMOUNTN", Array('#N#' => 1)),
		"sort" => "amount1",
		"align" => "right",
		"default" => true,
		"type" => "float",
	),
	array(
		"id" => "AMOUNT2",
		"content" => GetMessage("TSZH_F_AMOUNTN", Array('#N#' => 2)),
		"sort" => "amount2",
		"align" => "right",
		"default" => true,
		"type" => "float",
	),
	array(
		"id" => "AMOUNT3",
		"content" => GetMessage("TSZH_F_AMOUNTN", Array('#N#' => 3)),
		"sort" => "amount3",
		"align" => "right",
		"default" => false,
		"type" => "float",
	),
	array(
		"id" => "TIMESTAMP_X",
		"content" => GetMessage("TSZH_F_TIMESTAMP_X"),
		"sort" => "timestamp_x",
		"align" => "left",
		"default" => true,
		"type" => "datetime",
	),
	array(
		"id" => "MODIFIED_BY",
		"content" => GetMessage("TSZH_F_MODIFIED_BY"),
		"sort" => "modified_by",
		"align" => "left",
		"default" => true,
		"type" => "user",
	),
	array(
		"id" => "MODIFIED_BY_OWNER",
		"content" => GetMessage("TSZH_F_MODIFIED_BY_OWNER"),
		"sort" => "modified_by_owner",
		"align" => "left",
		"default" => true,
		"type" => "checkbox",
	),

	array("id" => "METER_ACTIVE", "content" => GetMessage("TSZH_F_METER_ACTIVE"), "align" => "left", "default" => false, "type" => "checkbox"),
	array("id" => "XML_ID", "content" => GetMessage("TSZH_F_METER_XML_ID"), "align" => "left", "default" => false, "type" => "string"),
	array(
		"id" => "METER_HOUSE_METER",
		"content" => GetMessage("TSZH_F_METER_HOUSE_METER"),
		"align" => "left",
		"default" => false,
		"type" => "checkbox",
	),
	array(
		"id" => "VALUES_COUNT",
		"content" => GetMessage("TSZH_F_VALUES_COUNT"),
		"sort" => "values_count",
		"align" => "left",
		"default" => false,
		"type" => "int",
	),
	array(
		"id" => "DEC_PLACES",
		"content" => GetMessage("TSZH_F_DEC_PLACES"),
		"sort" => "dec_places",
		"align" => "left",
		"default" => false,
		"type" => "int",
	),
	array(
		"id" => "CAPACITY",
		"content" => GetMessage("TSZH_F_CAPACITY"),
		"sort" => "capacity",
		"align" => "left",
		"default" => false,
		"type" => "int",
	),

	array(
		"id" => "ACCOUNT_XML_ID",
		"content" => GetMessage("TSZH_F_XML_ID"),
		"sort" => "account_xml_id",
		"align" => "left",
		"default" => false,
		"type" => "string",
	),
	array("id" => "NAME", "content" => GetMessage("TSZH_F_NAME"), "sort" => "name", "align" => "left", "default" => false, "type" => "string"),
	array(
		"id" => "SERVICE_NAME",
		"content" => GetMessage("TSZH_F_SERVICE_NAME"),
		"sort" => "service_name",
		"align" => "left",
		"default" => false,
		"type" => "string",
	),

	array(
		"id" => "TSZH_ID",
		"content" => GetMessage("TSZH_F_TSZH_ID"),
		"sort" => "tszh_id",
		"align" => "left",
		"default" => false,
		"type" => "list",
		"items" => $arTszhList,
	),
);


// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
	global $FilterArr, $lAdmin;
	foreach ($FilterArr as $value)
	{
		global $$value;
	}

	return true;
}

// *********************** /CheckFilter ******************************* //

// ������ �������� �������
$FilterArr = TszhAdminFilterParams($arHeaders);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

$USER_FIELD_MANAGER->AdminListAddFilterFields($UF_ENTITY, $FilterArr);

if (is_set($_GET, "find_meter_id"))
{
	$GLOBALS['find_meter_id_from'] = $_GET['find_meter_id'];
	$GLOBALS['find_meter_id_to'] = $_GET['find_meter_id'];
}

// ���� ��� �������� ������� ���������, ���������� ���
$arFilter = array();
if (CheckFilter() && !is_set($_REQUEST, 'del_filter'))
{
	// �������� ������ ���������� ��� ������� �� ������ �������� �������
	TszhAdminMakeFilter($arFilter, $arHeaders);
	$USER_FIELD_MANAGER->AdminListAddFilter($UF_ENTITY, $arFilter);
}


// ******************************************************************** //
//                ��������� �������� ��������                           //
// ******************************************************************** //

if (isset($_REQUEST["edit_form_del"]) && IntVal($_REQUEST["edit_form_del"]) == 1 && check_bitrix_sessid() && $POST_RIGHT >= 'W')
{
	if (isset($_REQUEST["ID"]) && IntVal($_REQUEST["ID"]) > 0)
	{
		@set_time_limit(0);

		$arItem = CTszhMeterValue::GetByID($_REQUEST["ID"]);
		if (is_array($arItem))
		{
			if (CTszhMeterValue::Delete($arItem['ID']))
			{
				$messageOK = GetMessage("TSZH_DELETE_OK");
			}
			else
			{
				$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR"), $ID);
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR") . ". " . GetMessage("TSZH_DELETE_ITEM_NOT_FOUND", Array("#ID#" => $ID)), $ID);
		}
	}
}

// ��������� ��������� � ��������� ��������
if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT >= "W")
{
	// ���� ������� "��� ���� ���������"
	if ($_REQUEST['action_target'] == 'selected')
	{
		$arSort = Array();
		$arSortBy = explode('|', $by);
		foreach ($arSortBy as $sBy)
		{
			$arSort[$sBy] = $order;
		}

		$rsData = CTszhMeterValue::GetList($arSort, $arFilter);
		while ($arRes = $rsData->Fetch())
		{
			$arID[] = $arRes['ID'];
		}
	}

	// ������� �� ������ ���������
	foreach ($arID as $ID)
	{
		if (strlen($ID) <= 0)
		{
			continue;
		}
		$ID = IntVal($ID);

		// ��� ������� �������� �������� ��������� ��������
		switch ($_REQUEST['action'])
		{
			// ��������
			case "delete":
				@set_time_limit(0);
				$DB->StartTransaction();
				if (!CTszhMeterValue::Delete($ID))
				{
					$DB->Rollback();
					$lAdmin->AddGroupError("Error deleting", $ID);
				}
				$DB->Commit();
				break;
		}
	}
}
// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //

$maxValueCount = 3;

$USER_FIELD_MANAGER->AdminListAddHeaders($UF_ENTITY, $arHeaders);
$lAdmin->AddHeaders($arHeaders);

// visible columns
$arVisibleColumns = $lAdmin->GetVisibleHeaderColumns();

// additional fields required for formatting
if (in_array('ACCOUNT_ID', $arVisibleColumns))
{
	$arVisibleColumns[] = 'ACCOUNT_NAME';
}
if (in_array('METER_ID', $arVisibleColumns))
{
	$arVisibleColumns[] = 'NAME';
	$arVisibleColumns[] = 'SERVICE_NAME';
}
$arVisibleColumns = array_unique(array_merge($arVisibleColumns, Array(
	'ID',
	'VALUES_COUNT',
	'VALUE1',
	'VALUE2',
	'VALUE3',
	'AMOUNT1',
	'AMOUNT2',
	'AMOUNT3',
)));

// proper page navigation using limit in SQL
$arNavParams = CAdminResult::GetNavParams(CAdminResult::GetNavSize($sTableID));
$arNav = Array(
	'nPageSize' => $arNavParams['SIZEN'],
	'iNumPage' => $arNavParams['PAGEN'],
	'bShowAll' => $arNavParams['SHOW_ALL'],
);

$arSort = Array();
$arSortBy = explode('|', $by);
foreach ($arSortBy as $sBy)
{
	$arSort[$sBy] = $order;
}
// ������� ������ �������
$rsData = CTszhMeterValue::GetList(
	$arSort,
	array_merge($arFilter, $arListFilter),
	false,
	$_REQUEST["mode"] == "excel" ? false : $arNav,
	$arVisibleColumns
);

// ����������� ������ � ��������� ������ CTszhMeterValueAdminResult
$rsData = new CTszhMeterValueAdminResult($rsData, $sTableID, CTszhMeterValue::$arGetListFields);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("TMH_PAGE_TITLE")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

// ������ ������������ ��������� ��� ���������������� �����
$template_path = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/interface/navigation.php";
// ����� ������������� �������
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("TMH_PAGE_TITLE"), true, "", $template_path, array('action', 'sessid')));

while ($arRes = $rsData->NavNext(true, "f_"))
{
	// ������� ������. ��������� - ��������� ������ CAdminListRow
	$row =& $lAdmin->AddRow($f_ID, $arRes);

	// ����� �������� ����������� �������� ��� ��������� � �������������� ������
	foreach ($arHeaders as $arH)
	{
		if ($arH["type"] == 'checkbox')
		{
			$row->AddViewField($arH["id"], $arRes[$arH["id"]] == 'Y' ? GetMessage("TSZH_YES") : GetMessage("TSZH_NO"));
		}
	}

	for ($i = 1; $i <= 3; $i++)
	{
		if ($i > $arRes["VALUES_COUNT"])
		{
			if (in_array('VALUE' . $i, $arVisibleColumns))
			{
				$row->AddViewField('VALUE' . $i, "");
			}
			if (in_array('AMOUNT' . $i, $arVisibleColumns))
			{
				$row->AddViewField('AMOUNT' . $i, "");
			}
		}
	}

	/*if (in_array('ACCOUNT_ID', $arVisibleColumns))
	{
		$arAccountIDView = array();
		foreach ($arNames as $accountID => $accountName)
		{
			$arAccountIDView[] = '[<a href="/bitrix/admin/tszh_account_edit.php?lang=' . LANG . '&amp;ID=' . $accountID . '" title="' . GetMessage("TSZH_F_EDIT_ACCOUNT") . '">' . $accountID . '</a>] ' . $accountName;
		}

		$row->AddViewField('ACCOUNT_ID', implode("<br>", $arAccountIDView));
	}*/

	if (in_array('ACCOUNT_XML_ID', $arVisibleColumns))
	{
		$row->AddViewField('ACCOUNT_XML_ID', implode("<br>", $arRes["ACCOUNT_XML_ID"]));
	}

	if (in_array('METER_ID', $arVisibleColumns))
	{
		$row->AddViewField('METER_ID', '[<a href="/bitrix/admin/tszh_meters_edit.php?lang=' . LANG . '&amp;ID=' . $arRes['METER_ID'] . '">' . $arRes['METER_ID'] . '</a>] ' . (!empty($arRes['NAME']) ? $arRes['NAME'] : $arRes['SERVICE_NAME']));
	}

	if (in_array('TSZH_ID', $arVisibleColumns))
	{
		$arTszh = array_unique($arRes['TSZH_ID']);
		$arTszhIDView = array();
		foreach ($arTszh as $tszhID)
		{
			$arTszhIDView[] = '[<a href="/bitrix/admin/tszh_edit.php?lang=' . LANG . '&amp;ID=' . $tszhID . '">' . $tszhID . '</a>] ' . (array_key_exists($tszhID, $arTszhList) ? $arTszhList[$tszhID] : '');
		}

		$row->AddViewField('TSZH_ID', implode("<br>", $arTszhIDView));
	}

	if (in_array('MODIFIED_BY', $arVisibleColumns))
	{
		if ($arRes["MODIFIED_BY"])
		{
			$row->AddViewField('MODIFIED_BY', '[<a href="/bitrix/admin/user_edit.php?lang=' . LANG . '&amp;ID=' . $arRes['MODIFIED_BY'] . '">' . $arRes['MODIFIED_BY'] . '</a>] ' . CTszhAccount::GetFullName($arRes['MODIFIED_BY']));
		}
		else
		{
			$row->AddViewField('MODIFIED_BY', '');
		}
	}

	$USER_FIELD_MANAGER->AddUserFields($UF_ENTITY, $arRes, $row);

	// ���������� ����������� ����
	$arActions = Array();

	// �������� ��������
	if ($POST_RIGHT >= "W")
	{
		$arActions[] = array(
			"ICON" => "delete",
			"TEXT" => GetMessage("TSZH_GROUP_DELETE_TITLE"),
			"ACTION" => "if(confirm('" . GetMessage('TSZH_GROUP_DELETE_CONFIRM') . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete"),
		);
	}

	// �������� ����������� ���� � ������
	$row->AddActions($arActions);
}

// ������ �������
$lAdmin->AddFooter(
	array(
		array("title" => GetMessage("TMH_CNT_TOTAL") . ":", "value" => $rsData->SelectedRowsCount()), // ���-�� ���������

		//array("counter"=>true, "title"=>"�������:", "value"=>"0"), // ������� ��������� ���������
	)
);

// ��������� ��������
if ($POST_RIGHT >= "W")
{
	$lAdmin->AddGroupActionTable(Array(
		"delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
	));
}

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //

// ���������� ���� �� ������ ������ - ���������� ��������
$aContext = array(
	array(
		"TEXT" => GetMessage("TMH_C_METERS_LIST"),
		"LINK" => "tszh_meters.php?lang=" . LANG,
		"TITLE" => GetMessage("TMH_C_METERS_LIST_TITLE"),
		"ICON" => "btn_new",
	),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //

if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT >= "W" && isset($messageOK) && $_REQUEST['action'] == "delete")
{
	$message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// �������������� �����
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("TSZH_METERS_HISTORY"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

if (isset($message) && isset($_REQUEST["edit_form_del"]))
{
	$message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

$arFilterItems = Array();
foreach ($arHeaders as $arField)
{
	if ($arField['id'] != "ID")
	{
		$arFilterItems[] = $arField['content'];
	}
}

$USER_FIELD_MANAGER->AddFindFields($UF_ENTITY, $arFilterItems);
$oFilter = new CAdminFilter(
	$sTableID . "_filter",
	$arFilterItems
);

?>
	<form name="form_filter" method="get" action="<? echo $APPLICATION->GetCurPage(); ?>">
		<?
		$oFilter->Begin();

		TszhShowShowAdminFilter($arHeaders);

		$USER_FIELD_MANAGER->AdminListShowFilter($UF_ENTITY);

		$oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "form_filter"));
		$oFilter->End();
		?>
	</form>
<?

// ������� ������� ������ ���������
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>