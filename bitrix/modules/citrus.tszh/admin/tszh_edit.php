<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Citrus\Tszh\MonetaAdditionalInfoTable;
use Citrus\Tszh\TszhTable;

// creater events type and templates for tszh
if ($_SERVER["REQUEST_METHOD"] == "POST"
    && isset($_POST["createSubscribeEvent"])
    && $_POST["createSubscribeEvent"] == "Y"
    && $_POST["tszhId"] > 0
    && strlen($_POST["subscribeCode"]))
{
    define("NO_KEEP_STATISTIC", "Y");
    define("NO_AGENT_STATISTIC", "Y");
    define("NO_AGENT_CHECK", true);
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
    IncludeModuleLangFile(__FILE__);
    if (CModule::includeModule("citrus.tszh")
        && CTszhFunctionalityController::checkSubscribe(false, $isSubscribeEdition) && $isSubscribeEdition
        && ($className = CTszhSubscribe::getSubscribeClassName($_POST["subscribeCode"])) && CTszhSubscribe::checkSubscribeClass($className)
        && ($arTszh = CTszh::getById($_POST["tszhId"])) && is_array($arTszh)
    )
    {
        $arEvent = CTszhSubscribe::getCustomEvent($arTszh["ID"], $className::EVENT_TYPE);
        $arTemplate = is_array($arEvent) && isset($arEvent["TEMPLATES"]) && is_array($arEvent["TEMPLATES"]) && !empty($arEvent["TEMPLATES"]) ? array_shift($arEvent["TEMPLATES"]) : false;

        $errMsg = false;
        if ($arTemplate["ID"] <= 0)
        {
            try
            {
                CTszhSubscribe::addCustomEvent($arTszh, $className::EVENT_TYPE);
                $arEvent = CTszhSubscribe::getCustomEvent($arTszh["ID"], $className::EVENT_TYPE);
                $arTemplate = array_shift($arEvent["TEMPLATES"]);
            }
            catch (Exception $e)
            {
                $errMsg = $e->getMessage();
            }
        }

        if (strlen($errMsg))
        {
            echo "<span style=\"color: red;\">{$errMsg}</span>";
        }
        else
        {
            echo "{$arTemplate["EVENT_NAME"]} <a target=\"_blank\" href=\"/bitrix/admin/message_edit.php?ID={$arTemplate["ID"]}&lang=" . LANGUAGE_ID . '">' . getMessage("TEF_SUBSCRIBE_EVENT_EDIT_LINK") . "</a>";
        }
    }
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");

    return;
}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
    $c = ob_get_contents();
    ob_end_clean();
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    echo $c;
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

    return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

// can check data org fields in js
$canCheckOrgData = false;
if (!CTszhFunctionalityController::isUkrainianVersion()
    && ModuleManager::isModuleInstalled('otr.datavalidation')
    && Loader::includeModule('otr.datavalidation'))
{
    $canCheckOrgData = true;
    CJSCore::Init('tszh_data_validation');
}

$modulePermissions = $APPLICATION->GetGroupRight("citrus.tszh");
if ($modulePermissions <= "R")
{
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/citrus.tszh/include.php");
IncludeModuleLangFile(__FILE__);

$strRedirect = BX_ROOT . "/admin/tszh_edit.php?lang=" . LANG;
$strRedirectList = BX_ROOT . "/admin/tszh_list.php?lang=" . LANG;

ClearVars();
$errorMessage = "";
$bVarsFromForm = false;
$message = null;

$ID = IntVal($ID);

$isSubscribeSmVersion = CTszhFunctionalityController::checkSubscribe(false, $isSubscribeEdition);
if ($isSubscribeSmVersion && $isSubscribeEdition)
{
    $arSubscribes = CTszhSubscribe::getSubscribes4Admin($ID);
}

$aTabs = array(
    array(
        "DIV" => "edit1",
        "TAB" => GetMessage("TSZH_EDIT_TAB1"),
        "ICON" => "citrus.tszh",
        "TITLE" => GetMessage("TSZH_EDIT_TAB1_TITLE"),
    ),
    array(
        "DIV" => "edit2",
        "TAB" => GetMessage("TSZH_EDIT_TAB2"),
        "ICON" => "citrus.tszh",
        "TITLE" => GetMessage("TSZH_EDIT_TAB2_TITLE"),
    ),
);

// include addititional tabs from other modules
$i = 3;
$db_opt_res = CModule::GetList();
while ($opt_res = $db_opt_res->Fetch())
{
    $mdir = $opt_res["ID"];
    if (file_exists($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/" . $mdir) && is_dir($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/" . $mdir))
    {
        $ofile = $_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/" . $mdir . "/tszh_settings.php";
        if (file_exists($ofile))
        {
            IncludeModuleLangFile($ofile);
            $mname = str_replace(".", "_", $mdir);
            $aTabs[] = array(
                "DIV" => "edit" . $i,
                "TAB" => GetMessage($mname . "_TAB"),
                "ICON" => "citrus.tszh",
                "TITLE" => GetMessage($mname . "_TAB_TITLE"),
            );
            $i++;
        }
    }
}

$UF_ENTITY = "TSZH";

$tabControl = new CAdminForm("tszhEdit", $aTabs);

if ($REQUEST_METHOD == "POST"
    && strlen($Update) > 0
    && $modulePermissions >= "W"
    && check_bitrix_sessid())
{
    if ($modulePermissions < "W")
    {
        $errorMessage .= GetMessage("TE_ERROR_NO_PERMISSIONS") . "<br>";
    }

    if ($ID > 0 && !($arOldTszh = CTszh::GetByID($ID)))
    {
        $errorMessage .= str_replace('#ID#', $ID, GetMessage("TE_ERROR_TSZH_NOT_FOUND")) . "<br>";
    }

    if ($ENABLE_CUSTOM_PAYMENT == "Y" && is_array($PAYMENT_RECEIPT_TYPES))
    {
        $arTypesToSave = array();
        $arReceiptTypes = Citrus\Tszh\Types\ReceiptType::getTitles();
        foreach ($PAYMENT_RECEIPT_TYPES as $p_types)
        {
            if (in_array($p_types, array_keys($arReceiptTypes)))
            {
                $arTypesToSave[] = (int)$p_types;
            }
        }
    }

    if ($ENABLE_CUSTOM_PAYMENT == "Y" && is_array($PAYMENT_RECEIPT_TYPES))
    {
        $arTypesToSave = array();
        $arReceiptTypes = Citrus\Tszh\Types\ReceiptType::getTitles();
        foreach ($PAYMENT_RECEIPT_TYPES as $p_types)
        {
            if (in_array($p_types, array_keys($arReceiptTypes)))
            {
                $arTypesToSave[] = (int)$p_types;
            }
        }
    }

    if ($isSubscribeSmVersion && $isSubscribeEdition)
    {
        foreach ($arSubscribes as $className => $arSubscribe)
        {
            try
            {
                $className::checkParams($arSubscribe["SETTINGS_COLLECTED"]["PARAMS"], 0, false);
            }
            catch (Exception $e)
            {
                $errorMessage .= getMessage("TE_ERROR_SUBSCRIBE", array(
                        "#SUBSCRIBE#" => $arSubscribe["NAME"],
                        "#ERROR#" => $e->getMessage(),
                    )) . "<br>";
            }
        }
    }

    if (!TszhTable::isRschCorrect($RSCH) && $MONETA_ENABLED == 'Y')
    {
        $errorMessage .= Loc::getMessage("TE_ERROR_WRONG_RSCH", array('#RSCH#' => $RSCH)) . "<br>";
    }

    if (strlen($errorMessage) <= 0)
    {
        $arFields = Array(
            "NAME" => $NAME,
            "SITE_ID" => $SITE_ID,
            "CODE" => $CODE,
            "CONFIRM_PARAM" => $CONFIRM_PARAM == "Y" ? "Y" : "N",
            "CONFIRM_TEXT" => $CONFIRM_TEXT,
            "INN" => $INN,
            "KPP" => $KPP,
            "KBK" => $KBK,
            "OKTMO" => $OKTMO,
            "RSCH" => $RSCH,
            "BANK" => $BANK,
            "KSCH" => $KSCH,
            "BIK" => $BIK,
            "IS_BUDGET" => $IS_BUDGET == "Y" ? "Y" : "N",
            "HEAD_NAME" => $HEAD_NAME,

            "ADDRESS" => $ADDRESS,
            "LEGAL_ADDRESS" => $LEGAL_ADDRESS,
            "EMAIL" => $EMAIL,
            "PHONE" => $PHONE,
            "PHONE_DISP" => $PHONE_DISP,
            "RECEIPT_TEMPLATE" => $RECEIPT_TEMPLATE,

            "METER_VALUES_START_DATE" => $METER_VALUES_START_DATE,
            "METER_VALUES_END_DATE" => $METER_VALUES_END_DATE,

            "MONETA_ENABLED" => $MONETA_ENABLED == "Y" ? "Y" : "N",
            "MONETA_EMAIL" => $MONETA_EMAIL,

            "ENABLE_CUSTOM_PAYMENT" => $ENABLE_CUSTOM_PAYMENT == "Y" ? "Y" : "N",
            "PAYMENT_RECEIPT_TYPES" => serialize($arTypesToSave),

			"OFFICE_HOURS" => $_POST["OFFICE_HOURS"],
			"OVERHAUL_OFF" => $OVERHAUL_OFF == "Y" ? "Y" : "N",
			"ADDITIONAL_INFO_MAIN" => $_POST["ADDITIONAL_INFO_MAIN"],
			"ADDITIONAL_INFO_OVERHAUL" => $_POST["ADDITIONAL_INFO_OVERHAUL"],
			"ANNOTATION_MAIN" => $_POST["ANNOTATION_MAIN"],
			"ANNOTATION_OVERHAUL" => $_POST["ANNOTATION_OVERHAUL"],
            "ORGANIZATION_TYPE" => $ORGANIZATION_TYPE,
		);

        $USER_FIELD_MANAGER->EditFormAddFields($UF_ENTITY, $arFields);

        if ($ID > 0)
        {
            if (isset($_REQUEST["CONFIRM_PARAM"]) && empty($_REQUEST["CONFIRM_TEXT"]))
            {
                $errorMessage .= GetMessage("TE_M_CONFIRM_TEXT");
            }
            else
            {
                $bResult = CTszh::Update($ID, $arFields);
            }

            if ($isSubscribeSmVersion && $isSubscribeEdition)
            {
                foreach ($arSubscribes as $className => $arSubscribe)
                {
                    $className::saveSettings($arSubscribe["ID"], $arSubscribe["SETTINGS"]);
                }
            }
        }
        else
        {
            $ID = CTszh::Add($arFields);
            $bResult = $ID > 0;
        }
        if ($bResult)
        {
            // error messages from event handlers
            if ($ex = $APPLICATION->GetException())
            {
                $errorMessage .= $ex->GetString();
            }
        }
        elseif (!$bResult)
        {
            if ($ex = $APPLICATION->GetException())
            {
                $errorMessage .= $ex->GetString();
            }
            else
            {
                $errorMessage .= GetMessage("TE_ERROR_SAVE");
            }
        }
    }

    // save permission
    if ($ID > 0 && CTszhFunctionalityController::isPortal())
    {
        if (isset($_REQUEST["GROUP"]) && !empty($_REQUEST["GROUP"]))
        {
            $arPermsList = array_keys(\Vdgb\PortalTszh\PermsTable::getTszhPermissionList());
            try
            {
                \Vdgb\PortalTszh\PermsTable::clearTszhPermission($ID);
            }
            catch (Exception $e)
            {
                $arErrors[] = $e->getMessage();
            }
            foreach ($_REQUEST["GROUP"] as $group => $letter)
            {
                if (in_array($letter, $arPermsList))
                {
                    try
                    {
                        \Vdgb\PortalTszh\PermsTable::setPermission("tszh", $ID, $group, $letter);
                    }
                    catch (Exception $e)
                    {
                        $arErrors[] = $e->getMessage();
                    }
                }
            }
        }
    }

    if (strlen($errorMessage) <= 0)
    {
        if (strlen($apply) <= 0)
        {
            LocalRedirect($strRedirectList . GetFilterParams("filter_", false));
        }
        else
        {
            LocalRedirect($strRedirect . "&ID=" . $ID . "&" . $tabControl->ActiveTabParam());
        }
    }
    else
    {
        $bVarsFromForm = true;
    }

}

//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/prolog.php");

CJsCore::Init(Array("jquery"));

if ($ID > 0)
{
    $APPLICATION->SetTitle(str_replace('#ID#', $ID, GetMessage("TE_TITLE_EDIT")));
}
else
{
    $APPLICATION->SetTitle(GetMessage("TE_TITLE_ADD"));
}

require_once($DOCUMENT_ROOT . BX_ROOT . "/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

CTszhFunctionalityController::checkSubscribe(true);

$dbTszh = CTszh::GetList(
    Array(),
    Array("ID" => $ID),
    false,
    Array("nTopCount" => 1),
    Array("*")
);

if (!$arTszh = $dbTszh->ExtractFields("str_"))
{
    if ($modulePermissions < "W")
    {
        $errorMessage .= GetMessage("TE_ERROR_NO_ADD_PERMISSIONS") . "<br />";
    }
    $ID = 0;
}

if ($bVarsFromForm)
{
    $DB->InitTableVarsForEdit("b_tszh", "", "str_");
}

if (strlen($errorMessage) > 0)
{
    \CAdminMessage::ShowMessage(Array(
        "DETAILS" => $errorMessage,
        "TYPE" => "ERROR",
        "MESSAGE" => GetMessage("TE_ERROR_HAPPENED"),
        "HTML" => true,
    ));
}

if ($ID > 0 && !CTszh::postValidate($ID))
{
    \CAdminMessage::ShowMessage(Array(
        "DETAILS" => '<ul><li>' . implode('<li>', CTszh::$validationErrors) . '</ul>' . GetMessage("TSZH_VALIDATION_FAILED_NOTE"),
        "TYPE" => "ERROR",
        "MESSAGE" => GetMessage("TSZH_VALIDATION_FAILED"),
        "HTML" => true,
    ));
}

/**
 *   CAdminForm()
 **/
$aMenu = array(
    array(
        "TEXT" => GetMessage("TE_M_ACCOUNT_LIST"),
        "LINK" => "/bitrix/admin/tszh_list.php?lang=" . LANG . GetFilterParams("find_"),
        "ICON" => "btn_list",
        "TITLE" => GetMessage("TE_M_ACCOUNT_LIST_TITLE"),
    ),
);
if ($ID > 0 && $modulePermissions >= "W")
{
    $aMenu[] = array("SEPARATOR" => true);

    $aMenu[] = array(
        "TEXT" => GetMessage("TE_M_ADD_TSZH"),
        "LINK" => "/bitrix/admin/tszh_edit.php?lang=" . LANG . GetFilterParams("find_"),
        "ICON" => "btn_new",
        "TITLE" => GetMessage("TE_M_ADD_TSZH_TITLE"),
    );

    if ($modulePermissions >= "W")
    {
        $aMenu[] = array(
            "TEXT" => GetMessage("TE_M_DELETE_TSZH"),
            "LINK" => "javascript:if(confirm('" . GetMessage("TE_M_DELETE_TSZH_CONFIRM") . "')) window.location='/bitrix/admin/tszh_list.php?ID=" . $ID . "&action=delete&lang=" . LANG . "&" . bitrix_sessid_get() . "#tb';",
            "WARNING" => "Y",
            "ICON" => "btn_delete",
        );
    }

}
if (!empty($aMenu))
{
    $aMenu[] = array("SEPARATOR" => true);
}
$link = DeleteParam(array("mode"));
$link = $GLOBALS["APPLICATION"]->GetCurPage() . "?mode=settings" . ($link <> "" ? "&" . $link : "");
$aMenu[] = array(
    "TEXT" => GetMessage("TE_FORM_SETTING"),
    "TITLE" => GetMessage("TE_FORM_SETTNG_TITLE"),
    "LINK" => "javascript:" . $tabControl->GetName() . ".ShowSettings('" . htmlspecialcharsbx(CUtil::addslashes($link)) . "')",
    "ICON" => "btn_settings",
);

$context = new CAdminContextMenu($aMenu);
$context->Show();

if (method_exists($USER_FIELD_MANAGER, 'showscript'))
{
    $tabControl->BeginPrologContent();
    echo $USER_FIELD_MANAGER->ShowScript();
    $tabControl->EndPrologContent();
}



$bCopy = (isset($_REQUEST['action']) && $_REQUEST["action"] == "copy");
$copyID = (isset($_REQUEST['copyID']) ? (int)$_REQUEST['copyID'] : 0);
if (/*$REQUEST_METHOD == "POST"
    &&*/ $bCopy
    && $copyID > 0
    && $modulePermissions >= "W"
    //&& check_bitrix_sessid()
)
{
    $ID = 0;

    $arOldTszh = CTszh::GetByID($copyID);
    $str_NAME = $arOldTszh['NAME'];
    $str_SITE_ID = $arOldTszh['SITE_ID'];
    $str_INN = $arOldTszh['INN'];
    $str_KPP = $arOldTszh['KPP'];
    $str_BANK = $arOldTszh['BANK'];
    $str_KSCH = $arOldTszh['KSCH'];
    $str_BIK = $arOldTszh['BIK'];
    $str_HEAD_NAME = $arOldTszh['HEAD_NAME'];
    $str_LEGAL_ADDRESS = $arOldTszh['LEGAL_ADDRESS'];
    $str_EMAIL = $arOldTszh['EMAIL'];
}

// ============ FORM PROLOG ==================
$tabControl->BeginEpilogContent();
echo GetFilterHiddens("filter_");
?>
    <input type="hidden" name="Update" value="Y"/>
    <input type="hidden" name="lang" value="<? echo LANG ?>"/>
    <input type="hidden" name="ID" value="<? echo $ID ?>"/>
<?
echo bitrix_sessid_post();
$tabControl->EndEpilogContent();
// ===========================================

$tabControl->Begin(array(
    "FORM_ACTION" => $APPLICATION->GetCurPage(),
    "FORM_ATTRIBUTES" => "method=\"POST\" enctype=\"multipart/form-data\"",
));
$tabControl->BeginNextFormTab();

if ($str_ID > 0)
{
    $tabControl->AddViewField("ID", "ID", $str_ID);
    $tabControl->AddViewField("TIMESTAMP_X", GetMessage("TEF_TIMESTAMP_X"), $str_TIMESTAMP_X);
}

$tabControl->AddEditField("NAME", GetMessage("TEF_NAME"), true, array("size" => 50, "maxlength" => 100), $str_NAME);

$rsSites = CSite::GetList($by = "sort", $order = "desc", Array());
$arSites = Array();
while ($arSite = $rsSites->Fetch())
{
    $arSites[$arSite['ID']] = '[' . $arSite["ID"] . '] ' . $arSite["NAME"];
}
$tabControl->AddDropDownField("SITE_ID", GetMessage("TEF_SITE_ID"), true, $arSites, $str_SITE_ID);

$tabControl->AddEditField("CODE", GetMessage("TEF_CODE"), false, array("size" => 30, "maxlength" => 100), $str_CODE);

$tabControl->AddCheckBoxField("CONFIRM_PARAM", GetMessage("TEF_CONFIRM_PARAM"), false, "Y", ($str_CONFIRM_PARAM == "Y"));
$tabControl->BeginCustomField("CONFIRM_HELP", GetMessage("TEF_CONFIRM_HELP"), false); ?>

    <tr id="CONFIRM_HELP">
        <td></td>
        <td style="text-align: left;">
            <? //echo BeginNote();?>
            <?=GetMessage("TEF_CONFIRM_HELP")?></br>
            <?=GetMessage("TEF_CONFIRM_HELP_TEXT")?>
            <? //echo EndNote();?>
        </td>
    </tr>

<?
$tabControl->EndCustomField("CONFIRM_HELP", "");
$tabControl->AddEditField("CONFIRM_TEXT", GetMessage("TEF_CONFIRM_TEXT"), false, array("size" => 100, "maxlength" => 1000), $str_CONFIRM_TEXT);

$tabControl->BeginCustomField("CONFIRM_TEXT", "", false); ?>
    <tr id="CONFIRM_TEXT">
        <td></td>
        <td style="text-align: left;">
            <?php
            $APPLICATION->IncludeComponent("bitrix:fileman.light_editor", "", Array(
                    "CONTENT" => htmlspecialchars_decode($str_CONFIRM_TEXT),
                    "INPUT_NAME" => "CONFIRM_TEXT",
                    "WIDTH" => "600px",
                    "HEIGHT" => "150px",
                    "RESIZABLE" => "N",
                    "AUTO_RESIZE" => "N",
                    "VIDEO_ALLOW_VIDEO" => "N",
                    "USE_FILE_DIALOGS" => "N",
                )
            );
            ?>
        </td>
    </tr>
<?php
$tabControl->EndCustomField("CONFIRM_TEXT", "");
$tabControl->AddCheckBoxField("OVERHAUL_OFF", GetMessage("TEF_OVERHAUL_OFF"), false, "Y", ($str_OVERHAUL_OFF == "Y"));
$tabControl->AddSection("RECEIPT_INFO", GetMessage("TEF_RECEIPT_INFO"), false);
$tabControl->AddEditField("ADDRESS", GetMessage("TEF_ADDRESS"), false, array(
    "size" => 50,
    "maxlength" => 255,
), $str_ADDRESS);
$tabControl->AddEditField("PHONE", GetMessage("TEF_PHONE"), false, array("size" => 50, "maxlength" => 250), $str_PHONE);
$tabControl->AddEditField("PHONE_DISP", GetMessage("TEF_PHONE_DISP"), false, array(
    "size" => 50,
    "maxlength" => 255,
), $str_PHONE_DISP);
$tabControl->AddDropDownField("RECEIPT_TEMPLATE", GetMessage("TEF_RECEIPT_TEMPLATE"), false, CTszhReceiptTemplateProcessor::getTemplatesForSelect("FROM_COMPONENT_SETTINGS"), $str_RECEIPT_TEMPLATE);

$tabControl->AddTextField("ADDITIONAL_INFO_MAIN", GetMessage("TEF_ADDITIONAL_INFO_MAIN"), $str_ADDITIONAL_INFO_MAIN, array(
    "cols" => 46,
    "rows" => 3,
));
$tabControl->AddTextField("ADDITIONAL_INFO_OVERHAUL", GetMessage("TEF_ADDITIONAL_INFO_OVERHAUL"), $str_ADDITIONAL_INFO_OVERHAUL, array(
    "cols" => 46,
    "rows" => 3,
));
$tabControl->AddTextField("ANNOTATION_MAIN", GetMessage("TEF_ANNOTATION_MAIN"), $str_ANNOTATION_MAIN, array(
    "cols" => 46,
    "rows" => 3,
));
$tabControl->AddTextField("ANNOTATION_OVERHAUL", GetMessage("TEF_ANNOTATION_OVERHAUL"), $str_ANNOTATION_OVERHAUL, array(
    "cols" => 46,
    "rows" => 3,
));
$tabControl->AddSection("BANK_TITLE", GetMessage("TEF_BANK_TITLE"), false);
$tabControl->AddDropDownField(
    "ORGANIZATION_TYPE",
    GetMessage("TEF_ORGANIZATION_TYPE"),
    false,
    CTszhReceiptTemplateProcessor::getOrganizationType($ID),
    $str_ORGANIZATION_TYPE
);
$tabControl->AddEditField("INN", GetMessage("TEF_INN"), false, array("size" => 12, "maxlength" => 12), $str_INN);
$tabControl->AddEditField("KPP", GetMessage("TEF_KPP"), false, array("size" => 9, "maxlength" => 9), $str_KPP);
$tabControl->AddEditField("RSCH", GetMessage("TEF_RSCH"), false, array("size" => 20, "maxlength" => 24), $str_RSCH);
$tabControl->AddEditField("BANK", GetMessage("TEF_BANK"), false, array("size" => 50, "maxlength" => 100), $str_BANK);
$tabControl->AddEditField("KSCH", GetMessage("TEF_KSCH"), false, array("size" => 20, "maxlength" => 24), $str_KSCH);
$tabControl->AddEditField("BIK", GetMessage("TEF_BIK"), false, array("size" => 12, "maxlength" => 16), $str_BIK);
$tabControl->AddCheckBoxField("IS_BUDGET", GetMessage("CITRUS_TSZH_IS_BUDGET"), false, "Y", ($str_IS_BUDGET == "Y"));
$tabControl->BeginCustomField("IS_BUDGET_ANNOTATION", "");
?>
    <tr id="tr_IS_BUDGET_ANNOTATION">
        <td></td>
        <td>
            <?
            echo BeginNote("id=\"budget-annotation\"");
            echo Loc::getMessage('CITRUS_TSZH_IS_BUDGET_ANNOTATION');
            echo EndNote();
            ?>
        </td>
    </tr>
<?
$tabControl->EndCustomField("IS_BUDGET_ANNOTATION");
if ($str_MONETA_OFFER == "Y") :
    $tabControl->BeginCustomField("IS_MONETA_ANNOTATION", "");
    ?>
    <tr id="tr_IS_MONETA_ANNOTATION">
        <td></td>
        <td>
            <?
            echo BeginNote("id=\"moneta-annotation\"");
            echo Loc::getMessage('CITRUS_TSZH_IS_MONETA_ANNOTATION');
            echo EndNote();
            ?>
        </td>
    </tr>
    </tr>
    <?
    $tabControl->EndCustomField("IS_MONETA_ANNOTATION");
endif;
$tabControl->AddEditField("KBK", GetMessage("TEF_KBK"), false, array("size" => 20, "maxlength" => 20), $str_KBK);
$tabControl->AddEditField("OKTMO", GetMessage("TEF_OKTMO"), false, array("size" => 11, "maxlength" => 11), $str_OKTMO);

$tabControl->AddSection("ADDITIONAL", GetMessage("TEF_ADDITIONAL"), false);

$tabControl->AddEditField("HEAD_NAME", GetMessage("TEF_HEAD_NAME"), false, array(
    "size" => 24,
    "maxlength" => 255,
), $str_HEAD_NAME);
$tabControl->AddEditField("LEGAL_ADDRESS", GetMessage("TEF_LEGAL_ADDRESS"), false, array(
    "size" => 50,
    "maxlength" => 255,
), $str_LEGAL_ADDRESS);
$tabControl->AddEditField("EMAIL", GetMessage("TEF_EMAIL"), false, array("size" => 50, "maxlength" => 255), $str_EMAIL);

if (CModule::IncludeModule("citrus.tszhpayment") && !CTszhFunctionalityController::isUkrainianVersion())
{
    $tabControl->AddSection("MONETA", GetMessage("TEF_MONETA_SECTION"));

    $tabControl->AddCheckBoxField("MONETA_ENABLED", GetMessage("CITRUS_TSZH_PAYMENT_MONETA_ENABLED"), false, "Y", ($str_MONETA_ENABLED == "Y"));

    $tabControl->AddEditField("MONETA_EMAIL", GetMessage("TEF_MONETA_EMAIL"), true, array(
        "size" => 50,
        "maxlength" => 255,
    ), $str_MONETA_EMAIL);

    $tabControl->BeginCustomField("MONETA_EMAIL_INFO", "");
    ?>
    <tr id="tr_MONETA_EMAIL_INFO">
        <td></td>
        <td>
            <?
            $m_email_disable_edit = strlen($str_MONETA_EMAIL) > 0;
            $msg_code = ($m_email_disable_edit ? "TEF_MONETA_EMAIL_INFO_ISSET" : "TEF_MONETA_EMAIL_INFO_USSET");
            echo BeginNote();
            echo Loc::getMessage($msg_code);
            echo EndNote();
            ?>
        </td>
    </tr>
    <?
    $tabControl->EndCustomField("MONETA_EMAIL_INFO");

    $tabControl->BeginCustomField("MONETA_OFFER", strip_tags(GetMessage("CITRUS_TSZH_PAYMENT_MONETA_OFFER")), false);
    ?>
    <tr id="tr_MONETA_OFFER" <?=$str_MONETA_ENABLED == "Y" ? '' : 'class="hidden"'?> style="text-align: center;">
        <td colspan="2">
            <?
            $APPLICATION->IncludeComponent(
                "otr:tszh.moneta.usage.table",
                "",
                Array(
                    "DISABLE_OFFER" => "N",
                    "ENTITY_ID" => $ID,
                    "ENTITY_TYPE" => "tszh",
                    "HAS_ERRORS" => "N",
                )
            ); ?>
        </td>
    </tr>
    <?
    $tabControl->EndCustomField("MONETA_OFFER", '<input type="hidden" name="MONETA_OFFER" value="' . $str_MONETA_OFFER . '">');

    $tabControl->BeginCustomField("MONETA_ADDITIONAL_INFO", Loc::getMessage("TSZH_MONETA_ADDITIONAL_INFO"), false);

    $arMonetaAddInfo = MonetaAdditionalInfoTable::getByPrimary($ID)->fetch();
    $arMonetaAddInfoFields = MonetaAdditionalInfoTable::getEditableFields();

    if (!is_array($arMonetaAddInfo))
    {
        $status = "NOT_ACTIVE";
    }
    else
    {
        $status = $arMonetaAddInfo['STATUS'];
    }

    if ($status == 'WITHOUT_DOCS')
    {
        $arEditMonetaOfferAgreementUrlParam = array(
            'title' => Loc::getMessage('MONETA_OFFER_ADDITITIONAL_INFO_PARTNER_AGREEMENT_DIALOG_TITLE'),
            'content_url' => '/bitrix/admin/tszh_moneta_offer_partner_agreement.php',
            'content_post' => 'ID=' . $ID,
            'draggable' => true,
            'resizable' => false,
            'overlay' => true,
            'height' => 300,
            'width' => 600,
        );

        $strEditMonetaOfferAgreementUrl = 'javascript:(new BX.CDialog(' . CUtil::PhpToJsObject($arEditMonetaOfferAgreementUrlParam) . ')).Show()';
        $strDocs = Loc::getMessage('MONETA_OFFER_ADDITITIONAL_DOCS_NOTE', array('#URL#' => $strEditMonetaOfferAgreementUrl));
    }
    else
    {
        $strDocs = '—';
    }

    if (isset($ID) && $ID > 0)
    {
        $arEditMonetaOfferUrlParam = array(
            'title' => Loc::getMessage('MONETA_OFFER_ADDITITIONAL_INFO_DIALOG_TITLE'),
            'content_url' => '/bitrix/admin/tszh_moneta_offer.php',
            'content_post' => 'ID=' . $ID,
            'draggable' => true,
            'resizable' => false,
            'overlay' => true,
            'height' => 600,
            'width' => 900,
        );

        $strEditMonetaOfferUrl = 'javascript:(new BX.CDialog(' . CUtil::PhpToJsObject($arEditMonetaOfferUrlParam) . ')).Show()';
    }

    $tabControl->EndCustomField("MONETA_ADDITIONAL_INFO", '<input type="hidden" name="MONETA_ADDITIONAL_INFO" value="' . $str_MONETA_ADDITIONAL_INFO . '">');
}

$tabControl->AddSection("OFFICE_HOURS_SECTION", GetMessage("TEF_OFFICE_HOURS"), false);
$tabControl->BeginCustomField("OFFICE_HOURS", GetMessage("OFFICE_HOURS_FORM"), false);
?>
    <tr>
        <td colspan="2" style="text-align: center;">
            <? $APPLICATION->IncludeComponent(
                "citrus:tszh.office_hours.edit",
                "",
                Array(
                    "OFFICE_HOURS" => $bVarsFromForm ? $arFields["OFFICE_HOURS"] : $arTszh["OFFICE_HOURS"],
                ),
                false
            ); ?>
        </td>
    </tr>
<?
$tabControl->EndCustomField("OFFICE_HOURS", "");

$tabControl->AddSection("METER_VALUES_PERIOD", GetMessage("TEF_METER_VALUES_PERIOD"), false);
$tabControl->AddEditField("METER_VALUES_START_DATE", GetMessage("TEF_METER_VALUES_START_DATE"), false, array(
    "size" => 5,
    "maxlength" => 2,
), $str_METER_VALUES_START_DATE);
$tabControl->AddEditField("METER_VALUES_END_DATE", GetMessage("TEF_METER_VALUES_END_DATE"), false, array(
    "size" => 5,
    "maxlength" => 2,
), $str_METER_VALUES_END_DATE);

// set receipt types for payment
$PAYMENT_RECEIPT_TYPES = unserialize($str_PAYMENT_RECEIPT_TYPES);
$arReceiptTypes = Citrus\Tszh\Types\ReceiptType::getTitles();
$tabControl->AddSection("CUSTOM_PAYMENT", GetMessage("TEF_CUSTOM_PAYMENT"), true);
$tabControl->AddCheckBoxField("ENABLE_CUSTOM_PAYMENT", GetMessage("TEF_ENABLE_CUSTOM_PAYMENT"), false, "Y", ($str_ENABLE_CUSTOM_PAYMENT == "Y"));
$tabControl->AddDropDownField("PAYMENT_RECEIPT_TYPES[]", GetMessage("TEF_PAYMENT_RECEIPT_TYPES"), false, $arReceiptTypes, "", array(
    "multiple",
    "id = \"PAYMENT_RECEIPT_TYPES\"",
));
$tabControl->BeginCustomField("CUSTOM_PAYMENT_ANNOTATION", "");
?>
    <tr id="tr_CUSTOM_PAYMENT_ANNOTATION">
        <td colspan="2">
            <?
            echo BeginNote("id=\"CUSTOM_PAYMENT_ANNOTATION\"");
            echo Loc::getMessage('CUSTOM_PAYMENT_ANNOTATION');
            echo EndNote();
            ?>
        </td>
    </tr>
<?
$tabControl->EndCustomField("BUDGET_ANNOTATION");

if (
    (count($USER_FIELD_MANAGER->GetUserFields($UF_ENTITY)) > 0) ||
    ($USER_FIELD_MANAGER->GetRights($UF_ENTITY) >= "W")
)
{
    $tabControl->AddSection('USER_FIELDS', GetMessage("TEF_USER_FIELDS"));
    $tabControl->ShowUserFields($UF_ENTITY, $str_ID, $bVarsFromForm);
}

if ($isSubscribeSmVersion && $isSubscribeEdition)
{
    if (!function_exists("printSubscribeInput")):
        function printSubscribeInput($tabControl, $settingCode, $arSetting)
        {
            if ($arSetting["readOnly"])
            {
                if (strlen($arSetting["value"]) <= 0)
                {
                    return;
                }

                $methodName = "addViewField";
                $arParams = array($arSetting["key"], $arSetting["name"], $arSetting["value"]);
            }
            elseif (in_array($arSetting["type"], array("int", "float", "string", "currency")))
            {
                $methodName = "addEditField";
                $arParams = array(
                    $arSetting["key"],
                    $arSetting["name"],
                    false,
                    $arSetting["htmlParams"],
                    $arSetting["value"],
                );
            }
            elseif ($arSetting["type"] == "checkbox")
            {
                $methodName = "addCheckBoxField";
                $arParams = array(
                    $arSetting["key"],
                    $arSetting["name"],
                    false,
                    "Y",
                    ($arSetting["value"] == "Y"),
                    $arSetting["htmlParams"] ? : array(),
                );
            }
            else
            {
                return;
            }

            $arParams[1] .= ":";
            call_user_func_array(array($tabControl, $methodName), $arParams);

            if (strlen($arSetting["error"]))
            {
                $fieldId = "SUBSCRIBE_" . $settingCode . "_ERRORS";
                $tabControl->BeginCustomField($fieldId, "", false);
                ?>
                <tr>
                    <td></td>
                    <td>
                        <?=$arSetting["error"]?>
                    </td>
                </tr>
                <?
                $tabControl->EndCustomField($fieldId);
            }

            if (strlen($arSetting["note"]))
            {
                $fieldId = "SUBSCRIBE_" . $settingCode . "_NOTE";
                $tabControl->BeginCustomField($fieldId, "", false);
                ?>
                <tr>
                    <td></td>
                    <td>
                        <?
                        echo BeginNote();
                        echo $arSetting["note"];
                        echo EndNote();
                        ?>
                    </td>
                </tr>
                <?
                $tabControl->EndCustomField($fieldId);
            }
        }
    endif;

    $tabControl->BeginNextFormTab();

    foreach ($arSubscribes as $className => $arSubscribe)
    {
        $tabControl->AddSection("SUBSCRIBE_" . $className::CODE, $arSubscribe["NAME"], false);

        if (strlen($arSubscribe["NOTE"])):
            $fieldId = "SUBSCRIBE_" . $className::CODE . "_NOTE";
            $tabControl->BeginCustomField($fieldId, "", false); ?>
            <tr>
                <td></td>
                <td>
                    <?
                    echo BeginNote();
                    echo $arSubscribe["NOTE"];
                    echo EndNote();
                    ?>
                </td>
            </tr>
            <? $tabControl->EndCustomField($fieldId);
        endif;

        foreach ($arSubscribe["SETTINGS"] as $code => $arSetting)
        {
            if ($code == "PARAMS")
            {
                foreach ($arSetting as $paramCode => $arParam)
                {
                    printSubscribeInput($tabControl, $paramCode, $arParam);
                }
            }
            else
            {
                printSubscribeInput($tabControl, $code, $arSetting);
            }
        }

        if ($ID > 0)
        {
            $fieldId = "SUBSCRIBE_" . $className::CODE . "_CREATE_SUBSCRIBE_EVENT";
            $tabControl->BeginCustomField($fieldId, "", false);
            ?>
            <tr>
                <td><?=getMessage("TEF_SUBSCRIBE_EVENT_LABEL")?>:</td>
                <? $cellId = "custom-subscribe-event-{$ID}-" . $className::CODE; ?>
                <td id="<?=$cellId?>">
                    <? $arTemplate = isset($arSubscribe["CUSTOM_EVENT"]["TEMPLATES"]) && !empty($arSubscribe["CUSTOM_EVENT"]["TEMPLATES"]) ? array_shift($arSubscribe["CUSTOM_EVENT"]["TEMPLATES"]) : false;
                    if (is_array($arTemplate) && !empty($arTemplate) && $arTemplate["ID"] > 0):?>
                        <?=$arTemplate["EVENT_NAME"]?> <a target="_blank"
                                                          href="/bitrix/admin/message_edit.php?ID=<?=$arTemplate["ID"]?>&lang=<?=LANGUAGE_ID?>"><?=getMessage("TEF_SUBSCRIBE_EVENT_EDIT_LINK")?></a>
                    <?
                    else:?>
                        <?=getMessage("TEF_SUBSCRIBE_EVENT_NOT_EXISTS")?>
                        <button
                                onclick="BX.showWait(); $('#<?=$cellId?>').load('<?=$APPLICATION->getCurPage()?>', {'createSubscribeEvent': 'Y', 'tszhId': <?=$ID?>, 'subscribeCode': '<?=$className::CODE?>'}); BX.closeWait(); return false;"
                                style="cursor: pointer;"><?=getMessage("TEF_SUBSCRIBE_EVENT_BUTTON")?></button>
                    <? endif ?>
                </td>
            </tr>
            <?
            $tabControl->EndCustomField($fieldId);
        }
    }
}

// addititional tabs from other modules
$db_opt_res = CModule::GetList();
while ($opt_res = $db_opt_res->Fetch())
{
    $mdir = $opt_res["ID"];
    if (file_exists($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/" . $mdir) && is_dir($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/" . $mdir))
    {
        $ofile = $_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/" . $mdir . "/tszh_settings.php";
        if (file_exists($ofile))
        {
            $mname = str_replace(".", "_", $mdir);
            $tabControl->BeginNextFormTab();
            $tabControl->BeginCustomField("MODULE_TAB_" . $mname, GetMessage($mname . "_TAB"));
            include($ofile);
            $tabControl->EndCustomField("MODULE_TAB_" . $mname);
        }
    }
}

$tabControl->Buttons(Array(
    "disabled" => ($modulePermissions < "W"),
    "back_url" => "/bitrix/admin/tszh_account_list.php?lang=" . LANG . GetFilterParams("filter_"),
));

$tabControl->Show();

$tabControl->ShowWarnings($tabControl->GetName(), $message);

if (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):?>
    <? echo BeginNote(); ?>
    <span class="required">*</span> <? echo GetMessage("REQUIRED_FIELDS") ?>
    <? echo EndNote(); ?>
<? endif; ?>

    <style type="text/css">
        #tr_MONETA_OFFER,
        #tr_MONETA_ADDITIONAL_INFO,
        #tr_MONETA_EMAIL_INFO,
        #tr_MONETA_EMAIL {
            display: none;
        }
        .tszh-validation-error__input {
            background-color: rgba(255, 0, 0, 0.2) !important;
            border-color: rgb(251, 94, 94) !important;
        }
        .tszh-validation-error__descr {
            display: inline-block;
            margin-left: -2px;
            font-size: 11px;
            color: rgb(252, 41, 41) !important;
            border: 1px solid red;
            padding: 6px 5px 5px 10px;
            border-left: none;
            border-radius: 0px 4px 4px 0px;
        }
        .tszh-data__error-popup {
            background-color: rgb(255, 112, 112) !important;
            color: rgb(130, 47, 59) !important;
        }
        .tszh-data__error-popup:after {
            content: ' ';
            display: block;
            position: absolute;
            border: 10px solid transparent;
            border-right: 10px solid rgb(255, 112, 112);
            left: -17px;
            top: 3px;
        }
        #budget-annotation .adm-info-message {
            margin: 0;
        }
    </style>

<? if ($m_email_disable_edit) : ?>
    <script>
        BX.ready(function () {
            BX('tszhEdit_form').MONETA_EMAIL.readOnly = true;
        });
    </script>
<? endif; ?>

    <script>
        // check addititional field for connect to PS Moneta.ru
        $(document).ready(function () {
            var $monetaEnabledCheckbox = $('#tr_MONETA_ENABLED input[type=checkbox]'),
                $monetaOffer = $('#tr_MONETA_OFFER'),
                $monetaOfferAdd = $('#tr_MONETA_ADDITIONAL_INFO'),
                $monetaEmailInfo = $('#tr_MONETA_EMAIL_INFO'),
                $monetaEmail = $('#tr_MONETA_EMAIL'),
                monetaEnabledCheck = function () {
                    var checked = $monetaEnabledCheckbox.is(':checked');
                    if (checked) {
                        $monetaEmail.fadeIn(300);
                        $monetaOffer.fadeIn(300);
                        $monetaOfferAdd.fadeIn(300);
                        $monetaEmailInfo.fadeIn(300);
                    }
                    else {
                        $monetaEmail.fadeOut(300);
                        $monetaOffer.fadeOut(300);
                        $monetaOfferAdd.fadeOut(300);
                        $monetaEmailInfo.fadeOut(300);
                    }
                };

            $(function () {
                monetaEnabledCheck();
                $monetaEnabledCheckbox.change(monetaEnabledCheck);
                $('input[name=MONETA_EMAIL]').change(function () {
                    $('.moneta-email').html(this.value);
                });
            });
        });

		<?php if ($canCheckOrgData) : ?>

        BX.ready(function () {
            var organizationTypeSelect = BX.findChild(
                BX('tr_ORGANIZATION_TYPE'),
                {'tag': 'select', 'name': 'ORGANIZATION_TYPE'},
                true,
                false,
            );

            function hideOrganizationForm(organizationTypeSelect) {
                var isBudgetCheckbox = BX.findChild(
                    BX('tr_IS_BUDGET'),
                    {'tag': 'input', 'name': 'IS_BUDGET'},
                    true,
                    false,
                );

                var selectedIndex = organizationTypeSelect.options.selectedIndex;
                var selectedValue = organizationTypeSelect.options[selectedIndex].value;

                if (selectedValue == '1') {
                    isBudgetCheckbox.checked = false;
                    BX('tr_KPP').style.display = 'none';
                    BX('tr_IS_BUDGET').style.display = 'none';
                    BX('tr_IS_BUDGET_ANNOTATION').style.display = 'none';
                    BX('tr_KBK').style.display = 'none';
                    BX('tr_OKTMO').style.display = 'none';
                } else if (selectedValue == '2') {
                    isBudgetCheckbox.checked = false;
                    BX('tr_KPP').style.display = 'table-row';
                    BX('tr_IS_BUDGET').style.display = 'none';
                    BX('tr_IS_BUDGET_ANNOTATION').style.display = 'none';
                    BX('tr_KBK').style.display = 'none';
                    BX('tr_OKTMO').style.display = 'none';
                } else if (selectedValue == '3') {
                    isBudgetCheckbox.checked = true;
                    BX('tr_KPP').style.display = 'table-row';
                    BX('tr_IS_BUDGET').style.display = 'none';
                    BX('tr_IS_BUDGET_ANNOTATION').style.display = 'table-row';
                    BX('tr_KBK').style.display = 'table-row';
                    BX('tr_OKTMO').style.display = 'table-row';
                }
            }

            hideOrganizationForm(organizationTypeSelect);

            organizationTypeSelect.addEventListener('change', function() {
                hideOrganizationForm(organizationTypeSelect);
            });
            var checkFields = ['INN', 'KPP', 'RSCH', 'BIK', 'KSCH'];
            var fields = [];
            var popup = [];
            checkFields.forEach(function (item, i, arr) {
                fields[item] = document.forms['tszhEdit_form'][item];
                BX.bind(fields[item], 'bxchange', function () {
                    var val = fields[item].value;
                    var error = {
                        code: null,
                        message: null
                    };
                    var result_validation = false;
                    switch (item) {
                        case 'INN':
                            result_validation = BX.tszh.validateInn(val, error);
                            break;
                        case 'KPP':
                            result_validation = BX.tszh.validateKpp(val, error);
                            break;
                        case 'RSCH':
                            var bik = document.forms['tszhEdit_form'].BIK.value;
                            result_validation = BX.tszh.validateRs(val, bik, error);
                            break;
                        case 'BIK':
                            result_validation = BX.tszh.validateBik(val, error);
                            if (result_validation
                                && document.forms['tszhEdit_form'].KSCH.value == ''
                                && document.forms['tszhEdit_form'].BANK.value == '') {
                                BX.ajax.loadJSON(
                                    window.location.protocol + '//www.bik-info.ru/api.html?type=json&bik=' + val,
                                    function (data) {
                                        if (data.error) {
                                            console.log(data);
                                        }
                                        else {
                                            document.forms['tszhEdit_form'].KSCH.value = data.ks;
                                            document.forms['tszhEdit_form'].BANK.value = BX.util.htmlspecialcharsback(data.name);
                                            BX.removeClass(document.forms['tszhEdit_form'].KSCH, 'tszh-validation-error__input');
                                            if (typeof popup['KSCH'] !== 'undefined') {
                                                popup['KSCH'].close();
                                            }
                                        }
                                    },
                                    function (error) {
                                        console.log(error);
                                    }
                                );
                            }
                            break;
                        case 'KSCH':
                            var bik = document.forms['tszhEdit_form'].BIK.value;
                            result_validation = BX.tszh.validateKs(val, bik, error);
                            break;
                    }
                    if (!result_validation) {
                        BX.addClass(fields[item], 'tszh-validation-error__input');
                        var input_width = parseInt(fields[item].clientWidth);
                        if (typeof popup[item] == 'undefined') {
                            popup[item] = BX.PopupWindowManager.create("popup-message-" + item, fields[item], {
                                content: error.message + '',
                                darkMode: true,
                                autoHide: false,
                                closeByEsc: false,
                                overlay: false,
                                closeIcon: false,
                                angle: false,
                                offsetLeft: input_width + 20,
                                offsetTop: -25,
                                width: 250,
                                className: 'tszh-data__error-popup'
                            });
                        }
                        else {
                            popup[item].setContent(error.message + '');
                        }
                        popup[item].show();
                    }
                    else {
                        BX.removeClass(fields[item], 'tszh-validation-error__input');
                        if (typeof popup[item] !== 'undefined') {
                            popup[item].close();
                        }
                    }
                    organizationTypeSelect.addEventListener('change', function() {
                        popup[item].close();
                    });
                });
            });

        });

        <?php endif; ?>

        BX.ready(function () {
            function checkCustomPayment() {
                var enableCustomPayment = tszhEdit_form.ENABLE_CUSTOM_PAYMENT.checked;
                var display = enableCustomPayment ? 'table-row' : 'none';
                BX.style(BX('tr_PAYMENT_RECEIPT_TYPES[]'), 'display', display);
            }

            // set values from js, because Bitrix not working with multyply list values
            function setReceiptTypes() {
                var selectedTypes = <?= CUtil::PhpToJSObject($PAYMENT_RECEIPT_TYPES) ?>;
                var select = BX('PAYMENT_RECEIPT_TYPES');
                for (var i = 0, l = select.options.length, o; i < l; i++) {
                    o = select.options[i];
                    o.selected = selectedTypes.indexOf(o.value) != -1;
                }
            }

            BX.bind(tszhEdit_form.ENABLE_CUSTOM_PAYMENT, 'bxchange', function () {
                checkCustomPayment();
            });
            checkCustomPayment();
            setReceiptTypes();
        });
    </script>
<? require_once($DOCUMENT_ROOT . BX_ROOT . "/modules/main/include/epilog_admin.php"); ?>