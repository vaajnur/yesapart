<?
/**
 * ������ ���ƻ
 * �������� �������� ������� ������ � ��������� ��������� � ����� � 1�
 * @package tszh
 */

// ��������� ��� ����������� �����:
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/include.php"); // ������������� ������
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/prolog.php"); // ������ ������

// ��������� �������� ����
IncludeModuleLangFile(__FILE__);

set_time_limit(0);

$step = IntVal($_REQUEST['step']);
if ($step < 1 || $step > 2 || !check_bitrix_sessid())
{
	$step = 1;
}

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT < "E")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$strError = false;

// �������� ����� ��������
if ($_REQUEST['delete_btn'] && check_bitrix_sessid())
{
	if (strlen($_SESSION['citrus.tszh.export']['FILENAME']) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'] . $_SESSION['citrus.tszh.export']['FILENAME']))
	{
		unlink($_SERVER['DOCUMENT_ROOT'] . $_SESSION['citrus.tszh.export']['FILENAME']);
		unset($_SESSION['citrus.tszh.export']);
	}
}

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = array();
if (CModule::IncludeModule("vdgb.portaltszh") && TSZH_PORTAL === true && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups, "ENTITY_TYPE" => "tszh")
		)
	);

	while ($arPerms = $rsPerms->fetch())
	{
		if ($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["ENTITY_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = array_keys($arTszhRight);
}

// ��������� � ���������� ������
if ($step > 1 && check_bitrix_sessid())
{

	if ($step == 2)
	{

		$lastID = isset($_REQUEST['lastID']) && IntVal($_REQUEST['lastID']) > 0 ? IntVal($_REQUEST['lastID']) : 0;
		if ($lastID <= 0)
		{
			$tszhExportFormat               = in_array($_REQUEST['tszhExportFormat'], Array(
				'csv',
				'xml'
			)) ? $_REQUEST['tszhExportFormat'] : 'xml';
			$_SESSION['citrus.tszh.export'] = Array(
				'FILENAME' => "/upload/export_" . date('Y-m-d_His') . ".{$tszhExportFormat}",
				'TSZH_ID' => IntVal($_REQUEST["tszhID"]),
				'STEP_TIME' => isset($_REQUEST['step_time']) && IntVal($_REQUEST['step_time']) > 0 ? IntVal($_REQUEST['step_time']) : 25,
				'TSZH' => CTszh::GetByID($_REQUEST["tszhID"]),
				'FORMAT' => $tszhExportFormat,
				"ONLY_OWNER_VALUES" => $_REQUEST['tszhExportOnlyOwnerValues'] == 'Y' ? true : false,
			);

			$dateFrom = trim($_REQUEST['tszhExportDateFrom']);
			$dateTo   = trim($_REQUEST['tszhExportDateTo']);
			if (strlen($dateFrom) > 0 || strlen($dateTo) > 0)
			{
				CheckFilterDates($dateFrom, $dateTo, $date1_wrong, $date2_wrong, $date2_less);
				if ($date1_wrong == "Y")
					$strError .= GetMessage("TSZH_EXPORT_ERROR_DATE_1");
				if ($date2_wrong == "Y")
					$strError .= GetMessage("TSZH_EXPORT_ERROR_DATE_2");
				if ($date2_less == "Y")
					$strError .= GetMessage("TSZH_EXPORT_ERROR_DATE_3");

				if (strlen($strError) <= 0)
				{
					if (strlen($dateFrom) > 0)
						$_SESSION['citrus.tszh.export']['DATE_FROM'] = $dateFrom;
					if (strlen($dateTo) > 0)
						$_SESSION['citrus.tszh.export']['DATE_TO'] = $dateTo;
				}
			}
			else
			{
				$currentDate                               = new DateTime();
				$_SESSION['citrus.tszh.export']['DATE_TO'] = $dateTo = $currentDate->format('Y-m-d H:i:s');
				$currentDate->modify('-1 month');
				$_SESSION['citrus.tszh.export']['DATE_FROM'] = $dateFrom = $currentDate->format('Y-m-d H:i:s');
			}

			CUserOptions::SetOption('citrus.tszh', 'export.format', $_SESSION['citrus.tszh.export']['FORMAT']);
			CUserOptions::SetOption('citrus.tszh', 'export.onlyOwnerValues', $_SESSION['citrus.tszh.export']['ONLY_OWNER_VALUES']);
		}

		$filename         = $_SESSION['citrus.tszh.export']['FILENAME'];
		$tszhID           = $_SESSION['citrus.tszh.export']['TSZH_ID'];
		$stepTime         = $_SESSION['citrus.tszh.export']['STEP_TIME'];
		$arTszh           = $_SESSION['citrus.tszh.export']['TSZH'];
		$tszhExportFormat = $_SESSION['citrus.tszh.export']['FORMAT'];

		$limit = CUserOptions::GetOption('citrus.tszh', 'export.MetterAccountsLimit', false);
		if (!$limit)
		{
			$limit = 2000;
			CUserOptions::SetOption('citrus.tszh', 'export.MetterAccountsLimit', $limit);
		}

		if (is_array($arTszh))
		{
			$orgName = $arTszh["NAME"];
			$orgINN  = $arTszh["INN"];
		}
		else
		{
			$strError .= GetMessage("TSZH_EXPORT_NO_TSZH_SELECTED");
		}

		if (strlen($strError) <= 0)
		{
			if ($tszhExportFormat == 'xml')
				$obExport = new CTszhExport();
			else
				$obExport = new CTszhExportCSV();

			//$obExport::$limit = $limit;

			$arValueFilter = Array();
			if ($_SESSION['citrus.tszh.export']['ONLY_OWNER_VALUES'])
				$arValueFilter['MODIFIED_BY_OWNER'] = "Y";
			if ($_SESSION['citrus.tszh.export']['DATE_FROM'])
				$arValueFilter[">=TIMESTAMP_X"] = $_SESSION['citrus.tszh.export']['DATE_FROM'];
			if ($_SESSION['citrus.tszh.export']['DATE_TO'])
			{
				$dateTo   = $_SESSION['citrus.tszh.export']['DATE_TO'];
				$arDateTo = ParseDateTime($dateTo);
				if (!isset($arDateTo["HH"]) && !isset($arDateTo["H"]) && !isset($arDateTo["GG"]) && !isset($arDateTo["G"]))
				{
					$dateTo = ConvertTimeStamp(
						AddToTimeStamp(array("HH" => 23, "MI" => 59, "SS" => 59), MakeTimeStamp($dateTo)),
						"FULL"
					);
				}
				$arValueFilter["<=TIMESTAMP_X"] = $dateTo;
			}

			list($total, $last, $limit, $lastID) = $obExport->DoExport($orgName, $orgINN, $_SERVER['DOCUMENT_ROOT'] . $filename, Array("TSZH_ID" => $tszhID), $stepTime, $lastID, $arValueFilter);

			if ($lastID === false)
			{
				if ($ex = $APPLICATION->GetException())
				{
					$strError = $ex->GetString() . '<br />';
				}
				else
				{
					$strError = GetMessage("TE_ERROR_EXPORT") . "<br />";
				}
				$APPLICATION->ResetException();
			}
			elseif ($last > 0)
			{
				$href = $APPLICATION->GetCurPageParam("lastID=$lastID&step=$step&" . bitrix_sessid_get(), Array(
					'tszhID',
					"lastID",
					'step',
					'step_time',
					'sessid'
				));
				?>
				<!doctype html>
				<html>
				<meta http-equiv="REFRESH" content="0;url=<?=$href?>">
				</html>
				<body>
				<?
				echo GetMessage("TSZH_EXPORT_PROGRESS", array('#CNT#' => $total, '#TOTAL#' => $last));
				?>
				</body>
				<?
				return;
			}
		}

		if (strlen($strError) <= 0)
		{
			// ��������� ���� ��������� ��������� (����� �������� ����� ����� �������)
			//COption::SetOptionString("citrus.tszh", "meters_block_edit", "Y");
		}

		if (strlen($strError) > 0)
			$step = 1;
	}
}
?>
<?

$APPLICATION->SetTitle(GetMessage("TE_PAGE_TITLE"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php"); // ������ ����� ������

echo $demoNotice;

//==========================================================================================
//==========================================================================================


CAdminMessage::ShowMessage($strError);

?>
	<form method="POST" action="<? echo $sDocPath ?>?lang=<? echo LANG ?>" enctype="multipart/form-data" name="dataload"
	      id="dataload">
		<?=bitrix_sessid_post()?>
		<?

		// ����� ��������
		//==========================================================================================
		$aTabs = array(
			array(
				"DIV" => "edit1",
				"TAB" => GetMessage("TE_TAB1"),
				"ICON" => "iblock",
				"TITLE" => GetMessage("TE_TAB1_TITLE")
			),
			array(
				"DIV" => "edit2",
				"TAB" => GetMessage("TE_TAB2"),
				"ICON" => "iblock",
				"TITLE" => GetMessage("TE_TAB2_TITLE")
			),
		);

		$tabControl = new CAdminTabControl("tabControl", $aTabs, false);
		$tabControl->Begin();
		?>

		<?
		$tabControl->BeginNextTab();

		if ($step == 1)
		{ ?>
			<tr>
				<td></td>
				<td>
					<? echo BeginNote(); ?>
					<? echo GetMessage("TSZH_EXPORT_TEXT"); ?>
					<? echo EndNote(); ?>
				</td>
			</tr>
			<tr>
				<td><? echo GetMessage("TSZH_EXPORT_TSZH"); ?>:</td>
				<td>
					<select name="tszhID" id="tszhID">
						<?
						$dbTszh = CTszh::GetList(Array("NAME" => "ASC"), $arTszhFilter);
						while ($arTszh = $dbTszh->Fetch())
						{
							?>
							<option value="<?=$arTszh["ID"]?>"<? if ($arTszh["ID"] == CUserOptions::GetOption('citrus.tszh.import', 'TszhID', false))
							echo " selected"; ?>>[<?=htmlspecialcharsex($arTszh["ID"])?>]
							&nbsp;<?=htmlspecialcharsex($arTszh["NAME"])?></option><?
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td><? echo GetMessage("TSZH_EXPORT_STEP_TIME"); ?>:</td>
				<td>
					<input name="step_time" value="25" type="text"/>
				</td>
			</tr>
			<tr><?
				$dateFrom = trim($_REQUEST['tszhExportDateFrom']);
				$dateTo   = trim($_REQUEST['tszhExportDateTo']);
				?>
				<td><label for="tszhExportDateFrom"> <?=GetMessage("TSZH_EXPORT_PERIOD")?></label></td>
				<td>
					<?=CalendarPeriod("tszhExportDateFrom", $dateFrom, "tszhExportDateTo", $dateTo, "dataload", "N", 'id="tszhExportPeriod"')?>
				</td>
			</tr>
			<tr><?
				$tszhExportOnlyOwnerValues = CUserOptions::GetOption('citrus.tszh', 'export.onlyOwnerValues', false);
				?>
				<td><label for="tszhExportOnlyOwnerValues"> <?=GetMessage("TSZH_EXPORT_VALUES_BY_OWNER")?></label></td>
				<td>
					<input type="checkbox" value="Y"
					       name="tszhExportOnlyOwnerValues"<?=($tszhExportOnlyOwnerValues ? ' checked="checked"' : '')?>
					       id="tszhExportOnlyOwnerValues"/>
				</td>
			</tr>
			<tr><?

				$tszhExportFormat = CUserOptions::GetOption('citrus.tszh', 'export.format', 'xml');

				?>
				<td><? echo GetMessage("TSZH_EXPORT_FORMAT"); ?>:</td>
				<td>
					<label title="<?=GetMessage("TSZH_EXPORT_FORMAT_XML_DESC")?>"><input type="radio"
					                                                                     name="tszhExportFormat"
					                                                                     value="xml"<?=($tszhExportFormat == 'xml' ? ' checked="checked"' : '')?> />XML</label>
					<label title="<?=GetMessage("TSZH_EXPORT_FORMAT_CSV_DESC")?>"><input type="radio"
					                                                                     name="tszhExportFormat"
					                                                                     value="csv"<?=($tszhExportFormat == 'csv' ? ' checked="checked"' : '')?> />CSV</label>
				</td>
			</tr>
			<?
		}

		$tabControl->EndTab();
		?>


		<?
		$tabControl->BeginNextTab();

		if ($step == 2)
		{ ?>
			<tr>
				<td>
					<?=GetMessage("TE_EXPORT_DONE")?>.<br/>
					<a href="<?=htmlspecialcharsbx($filename)?>" target="_blank"
					   download="<?=htmlspecialcharsbx(basename($filename))?>"><?=GetMessage("TE_DOWNLOAD_XML")?></a>
				</td>
			</tr>
			<?
		}

		$tabControl->EndTab();
		?>

		<?
		$tabControl->Buttons();
		?>
		<input type="hidden" name="step" value="<?=$step + 1?>"/>
		<?
		if ($step < 2)
		{
			?>
			<input type="submit" name="submit_btn" value="<?=GetMessage("TE_NEXT_BTN")?>" class="adm-btn-save"/>
			<?
		}
		else
		{
			?>
			<input type="submit" name="delete_btn" value="<?=GetMessage("TE_DELETE_FILE_BTN")?>"/>
			<?
		}
		?>

		<?
		$tabControl->End();
		?>
	</form>

	<script language="JavaScript">
		<!--
		<?if ($step < 2):?>
		tabControl.SelectTab("edit1");
		tabControl.DisableTab("edit2");
		<?elseif ($step >= 2):?>
		tabControl.SelectTab("edit2");
		tabControl.DisableTab("edit1");
		<?endif;?>
		//-->
	</script>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>