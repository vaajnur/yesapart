<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/include.php");

$modulePermissions = $APPLICATION->GetGroupRight("citrus.tszh");
if ($modulePermissions <= "D")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

IncludeModuleLangFile(__FILE__);

$bFatalError = false;

ClearVars();
ClearVars("str_");

$strRedirect = BX_ROOT . "/admin/tszh_contractors_edit.php?lang=" . LANG . GetFilterParams("find_", false);
$strRedirectList = BX_ROOT . "/admin/tszh_contractors.php?lang=" . LANG . GetFilterParams("find_", false);

$errorMessage = "";
$bVarsFromForm = false;
$message = null;

$ID = IntVal($ID);

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = array();
if (CTszhFunctionalityController::isPortal() && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups, "ENTITY_TYPE" => "tszh")
		)
	);

	while ($arPerms = $rsPerms->fetch())
	{
		if ($arPerms["PERMS"] >= "W")
		{
			$arTszhRight[$arPerms["ENTITY_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = array_keys($arTszhRight);
}

$aTabs = array(
	array(
		"DIV" => "edit1",
		"TAB" => GetMessage("TSZH_CONTRACTORS_EDIT"),
		"ICON" => "citrus.shop",
		"TITLE" => GetMessage("TSZH_CONTRACTORS_EDIT_TITLE")
	)
);

$UF_ENTITY = "TSZH_CONTRACTOR";

$tabControl = new CAdminForm(basename(__FILE__, '.php'), $aTabs);

if ($REQUEST_METHOD == "POST" && strlen($Update) > 0 && check_bitrix_sessid())
{
	if ($modulePermissions < "W")
	{
		$errorMessage .= GetMessage("ACCESS_DENIED") . ".<br />";
	}

	$ID = IntVal($ID);
	if ($ID < 0)
	{
		$errorMessage .= GetMessage("TSZH_GROUP_SAVE_ERROR") . ' ' . GetMessage("TSZH_GROUP_SAVE_ERROR_ITEM_NOT_FOUND") . "<br />";
	}

	if (strlen($errorMessage) <= 0)
	{
		$arFields = Array(
			"XML_ID" => $XML_ID,
			"EXECUTOR" => $EXECUTOR,
			"TSZH_ID" => $TSZH_ID,
			"NAME" => $NAME,
			"ADDRESS" => $ADDRESS,
			"SERVICES" => $SERVICES,
			"PHONE" => $PHONE,
			"BILLING" => $BILLING,

			"INN" => $INN,
			"KPP" => $KPP,
			"RSCH" => $RSCH,
			"BANK" => $BANK,
			"KSCH" => $KSCH,
			"BIK" => $BIK,
		);
		$USER_FIELD_MANAGER->EditFormAddFields($UF_ENTITY, $arFields);

		if ($ID > 0)
		{
			$bResult = CTszhContractor::Update($ID, $arFields);
		}
		else
		{
			$bResult = ($ID = CTszhContractor::Add($arFields)) > 0;
		}

		if (!$bResult)
		{
			if ($ex = $APPLICATION->GetException())
			{
				$errorMessage .= $ex->GetString() . "<br />";
			}
			else
			{
				$errorMessage .= GetMessage("TSZH_GROUP_SAVE_ERROR") . "<br />";
			}
		}
	}

	if (strlen($errorMessage) <= 0)
	{
		if (strlen($apply) <= 0)
		{
			LocalRedirect($strRedirectList);
		}
		else
		{
			LocalRedirect($strRedirect . "&ID=" . $ID . "&" . $tabControl->ActiveTabParam());
		}
	}
	else
	{
		$bVarsFromForm = true;
	}
}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/prolog.php");

$APPLICATION->SetTitle(($ID > 0) ? str_replace('#ID#', $ID, GetMessage("TSZH_EDIT_PAGE_TITLE")) : GetMessage("TSZH_ADD_PAGE_TITLE"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$arMargins = Array();
if ($ID > 0)
{
	$rsItem = CTszhContractor::GetList(
		Array(),
		Array("ID" => $ID),
		false,
		Array("nTopCount" => 1),
		Array("*")
	);

	if (!$arItem = $rsItem->ExtractFields("str_"))
	{
		$errorMessage .= GetMessage("TSZH_EDIT_ITEM_NOT_FOUND") . ".<br />";
		$bFatalError = true;
	}
}

if ($bVarsFromForm)
{
	$DB->InitTableVarsForEdit("b_tszh_contractors", "", "str_");
}


if (strlen($errorMessage) > 0)
{
	echo CAdminMessage::ShowMessage(Array(
		"DETAILS" => $errorMessage,
		"TYPE" => "ERROR",
		"MESSAGE" => GetMessage("TSZH_EDIT_ERRORS") . ':',
		"HTML" => true
	));
}

/*if (strlen($result) > 0)
{
	$okMessage = "";

	if ($result == "ok_ps")
		$okMessage = GetMessage("SOD_OK_PS");

	CAdminMessage::ShowNote($okMessage);
}*/


if ($bFatalError)
{
	require_once($DOCUMENT_ROOT . BX_ROOT . "/modules/main/include/epilog_admin.php");

	return;
}

/**
 *   CAdminForm()
 **/

$aMenu = array(
	array(
		"TEXT" => GetMessage("TSZH_ITEMS_LIST"),
		"LINK" => $strRedirectList,
		"ICON" => "btn_list",
		"TITLE" => GetMessage("TSZH_ITEMS_LIST_TITLE"),
	)
);

if ($ID > 0 && $modulePermissions >= "W")
{
	$aMenu[] = array("SEPARATOR" => "Y");

	$aMenu[] = array(
		"TEXT" => GetMessage("TSZH_GROUP_DELETE_TITLE"),
		"LINK" => "javascript:if(confirm('" . GetMessage("TSZH_GROUP_DELETE_CONFIRM") . "')) window.location='{$strRedirectList}&ID=" . $ID . "&edit_form_del=1&" . bitrix_sessid_get() . "#tb';",
		"WARNING" => "Y",
		"ICON" => "btn_delete",
	);
}
if (!empty($aMenu))
{
	$aMenu[] = array("SEPARATOR" => "Y");
}

$link = DeleteParam(array("mode"));
$link = $GLOBALS["APPLICATION"]->GetCurPage() . "?mode=settings" . ($link <> "" ? "&" . $link : "");
$aMenu[] = array(
	"TEXT" => GetMessage("TSZH_EDIT_SETTING"),
	"TITLE" => GetMessage("TSZH_EDIT_SETTING_TITLE"),
	"LINK" => "javascript:" . $tabControl->GetName() . ".ShowSettings('" . htmlspecialcharsbx(CUtil::addslashes($link)) . "')",
	"ICON" => "btn_settings",
);

$context = new CAdminContextMenu($aMenu);
$context->Show();

// ============ FORM PROLOG ==================
$tabControl->BeginEpilogContent();
echo GetFilterHiddens("find_");
?>
	<input type="hidden" name="Update" value="Y"/>
	<input type="hidden" name="lang" value="<? echo LANG ?>"/>
	<input type="hidden" name="ID" value="<? echo $ID ?>"/>
<?
echo bitrix_sessid_post();
$tabControl->EndEpilogContent();
// ===========================================

$tabControl->Begin(array(
	"FORM_ACTION" => $APPLICATION->GetCurPage(),
	"FORM_ATTRIBUTES" => "method=\"POST\" enctype=\"multipart/form-data\"",
));
$tabControl->BeginNextFormTab();

if ($str_ID > 0)
{
	$tabControl->AddViewField("ID", GetMessage("TSZH_F_ID"), $str_ID);
}
else
{
	$tabControl->AddViewField("ID", GetMessage("TSZH_F_ID"), '&mdash;');
}

$tabControl->AddEditField("XML_ID", GetMessage("TSZH_F_XML_ID"), false, array("size" => 40, "maxlength" => 255), $str_XML_ID);

$executorVariants = array(
	"N" => GetMessage("CITRUS_TSZH_EXECUTOR_N"),
	"Y" => GetMessage("CITRUS_TSZH_EXECUTOR_Y"),
	"Z" => GetMessage("CITRUS_TSZH_EXECUTOR_Z"),
);
$tabControl->AddDropDownField("EXECUTOR", GetMessage("TSZH_F_EXECUTOR"), true, $executorVariants, $str_EXECUTOR);

$rsTszh = CTszh::GetList(array(), $arTszhFilter);
$arTszhs = Array();
while ($arTszh = $rsTszh->Fetch())
{
	$arTszhs[$arTszh['ID']] = "[{$arTszh['ID']}] {$arTszh['NAME']}";
}
$tabControl->AddDropDownField("TSZH_ID", GetMessage("TSZH_F_TSZH_ID"), true, $arTszhs, $str_TSZH_ID);

$tabControl->AddEditField("NAME", GetMessage("TSZH_F_NAME"), true, array("size" => 40, "maxlength" => 100), $str_NAME);
$tabControl->AddTextField("ADDRESS", GetMessage("TSZH_F_ADDRESS"), $str_ADDRESS, array("cols" => 40, "rows" => 3, "maxlength" => 255), false);
$tabControl->AddEditField("SERVICES", GetMessage("TSZH_F_SERVICES"), false, array("size" => 40, "maxlength" => 255), $str_SERVICES);
$tabControl->AddEditField("PHONE", GetMessage("TSZH_F_PHONE"), false, array("size" => 40, "maxlength" => 100), $str_PHONE);

$tabControl->AddSection("BANK_TITLE", GetMessage("TEF_BANK_TITLE"), false);
$tabControl->AddEditField("INN", GetMessage("TEF_INN"), false, array("size" => 12, "maxlength" => 12), $str_INN);
$tabControl->AddEditField("KPP", GetMessage("TEF_KPP"), false, array("size" => 9, "maxlength" => 9), $str_KPP);
$tabControl->AddEditField("RSCH", GetMessage("TEF_RSCH"), false, array("size" => 20, "maxlength" => 24), $str_RSCH);
$tabControl->AddEditField("BANK", GetMessage("TEF_BANK"), false, array("size" => 50, "maxlength" => 100), $str_BANK);
$tabControl->AddEditField("KSCH", GetMessage("TEF_KSCH"), false, array("size" => 20, "maxlength" => 24), $str_KSCH);
$tabControl->AddEditField("BIK", GetMessage("TEF_BIK"), false, array("size" => 12, "maxlength" => 16), $str_BIK);
$tabControl->AddTextField("BILLING", GetMessage("TSZH_F_BILLING"), $str_BILLING, array("cols" => 40, "rows" => 3, "maxlength" => 255), false);

if (
	(count($USER_FIELD_MANAGER->GetUserFields($UF_ENTITY)) > 0) ||
	($USER_FIELD_MANAGER->GetRights($UF_ENTITY) >= "W")
)
{
	$tabControl->AddSection('USER_FIELDS', GetMessage("TSZH_USER_FIELDS"));
	$tabControl->ShowUserFields($UF_ENTITY, $str_ID, $bVarsFromForm);
}

$tabControl->Buttons(Array(
	"disabled" => ($modulePermissions < "W"),
	"back_url" => $strRedirectList,
));

$tabControl->Show();

if (method_exists($USER_FIELD_MANAGER, 'showscript'))
{
	$tabControl->BeginPrologContent();
	echo $USER_FIELD_MANAGER->ShowScript();
	$tabControl->EndPrologContent();
}

$tabControl->ShowWarnings($tabControl->GetName(), $message);

if (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):?>
	<? echo BeginNote(); ?>
	<span class="required">*</span> <? echo GetMessage("REQUIRED_FIELDS") ?>
	<? echo EndNote(); ?>
<? endif; ?>
<? require_once($DOCUMENT_ROOT . BX_ROOT . "/modules/main/include/epilog_admin.php"); ?>