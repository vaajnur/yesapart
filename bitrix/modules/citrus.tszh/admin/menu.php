<?
IncludeModuleLangFile(__FILE__);

$module_rights = $APPLICATION->GetGroupRight("citrus.tszh");


if($module_rights > "D")
{

	if ($module_rights >= 'E') {
		$export_menu_items[] = Array(
			"text" => GetMessage("TSZH_MENU_EXPORT_METERS"),
			"url" => "tszh_export.php?lang=" . LANG,
			"more_url" => array(),
			"title" => GetMessage("TSZH_MENU_EXPORT_METERS_TITLE")
		);
	}

	if ($module_rights >= 'E') {
		$export_menu_items[] = Array(
			"text" => GetMessage("TSZH_MENU_EXPORT_ACCOUNTS"),
			"url" => "tszh_export_accounts.php?lang=" . LANG,
			"more_url" => array(),
			"title" => GetMessage("TSZH_MENU_EXPORT_ACCOUNTS_TITLE")
		);
	}

	if ($module_rights >= 'U') {
		$export_menu_items[] = Array(
			"text" => GetMessage("TSZH_MENU_IMPORT"),
			"url" => "tszh_import.php?lang=" . LANG,
			"more_url" => array(),
			"title" => GetMessage("TSZH_MENU_IMPORT_TITLE")
		);
	}

	if ($module_rights >= 'X') {
		$export_menu_items[] = Array(
			"text" => GetMessage("TSZH_MENU_IMPORT_DBF"),
			"url" => "tszh_import_dbf.php?lang=" . LANG,
			"more_url" => array(),
			"title" => GetMessage("TSZH_MENU_IMPORT_TITLE_DBF")
		);
	}
	
	$arItems = Array(
		Array(
			"text" => GetMessage("TSZH_MENU_TSZH"),
			"url" => "tszh_list.php?lang=" . LANG,
			"more_url" => array('tszh_edit.php'),
			"title" => GetMessage("TSZH_MENU_TSZH_TITLE")
		),
		Array(
			"text" => GetMessage("TSZH_MENU_TSZH_CONTRACTORS"),
			"url" => "tszh_contractors.php?lang=" . LANG,
			"more_url" => array('tszh_contractors_edit.php'),
			"title" => GetMessage("TSZH_MENU_TSZH_CONTRACTORS_TITLE")
		),
		Array(
			"text" => GetMessage("TSZH_MENU_TSZH_HOUSES"),
			"url" => "tszh_house_list.php?lang=" . LANG,
			"more_url" => array('tszh_house_edit.php'),
			"title" => GetMessage("TSZH_MENU_TSZH_HOUSES_TITLE")
		),

		Array(
			"text" => GetMessage("TSZH_MENU_ACCOUNTS"),
			"url" => "tszh_account_list.php?lang=" . LANG,
			"more_url" => array('tszh_account_edit.php'),
			"title" => GetMessage("TSZH_MENU_ACCOUNTS_TITLE")
		),
        Array(
            "text" => GetMessage("TSZH_MENU_CONFIRM_UNAUTH"),
            "url" => "tszh_confirm_unauth.php?lang=" . LANG,
            "more_url" => array(),
            "title" => GetMessage("TSZH_MENU_CONFIRM_UNAUTH_TITLE")
        ),
		Array(
			"text" => GetMessage("TSZH_MENU_METERS"),
			"url" => "tszh_meters.php?lang=" . LANG,
			"more_url" => array('tszh_meters_edit.php'),
			"title" => GetMessage("TSZH_MENU_METERS_TITLE"),
			"items_id"	=> "menu_tszh_meters",
			"items" => Array(
				Array(
					"text" => GetMessage("TSZH_MENU_METERS_HISTORY"),
					"url" => "tszh_meters_history.php?lang=" . LANG,
					"title" => GetMessage("TSZH_MENU_METERS_HISTORY_TITLE")
				),
			),
		),
		Array(
			"text" => GetMessage("TSZH_MENU_SERVICES"),
			"url" => "tszh_services.php?lang=" . LANG,
			"more_url" => array('tszh_services_edit.php'),
			"title" => GetMessage("TSZH_MENU_SERVICES_TITLE"),
		),
		Array(
			"text" => GetMessage("TSZH_MENU_PERIODS"),
			"url" => "tszh_periods.php?lang=" . LANG,
			"more_url" => array('tszh_periods_edit.php'),
			"title" => GetMessage("TSZH_MENU_PERIODS_TITLE"),
			"items_id"	=> "menu_tszh_periods",
		),
		Array(
			"text" => GetMessage("TSZH_MENU_ACCOUNT_PERIODS"),
			"url" => "tszh_account_period_list.php?lang=" . LANG,
			"more_url" => array('tszh_account_period_edit.php'),
			"title" => GetMessage("TSZH_MENU_ACCOUNT_PERIODS_TITLE"),
			"items_id"	=> "menu_tszh_account_periods",
		),
		Array(
			"text" => GetMessage("TSZH_MENU_DEMODATA"),
			"url" => "tszh_demodata.php?lang=" . LANG,
			"more_url" => array('tszh_demodata.php'),
			"title" => GetMessage("TSZH_MENU_DEMODATA_TITLE"),
			"items_id"	=> "menu_tszh_demodata",
		),
	);
	$aMenu = array(
		"parent_menu" => "global_menu_services",
		"section" => "citrus.tszh",
		"sort" => 10,
		"text" => GetMessage("TSZH_MENU_SECT"),
		"title" => GetMessage("TSZH_MENU_SECT_TITLE"),
		"icon" => "tszh_menu_icon",
		"page_icon" => "tszh_page_icon",
		"items_id" => "menu_tszh",
		"items" => $module_rights >= 'R' ? $arItems : Array(),
	);
	
	if ($module_rights >= 'E')
	{
		$aMenu['items'][] = Array(
				"text" => GetMessage("TSZH_MENU_IMPORT_EXPORT"),
				"more_url" => array(
					"tszh_export.php?lang=" . LANG,
					"tszh_import.php?lang=" . LANG,
				),
				"title" => GetMessage("TSZH_MENU_IMPORT_EXPORT_TITLE"),
				"page_icon" => "tszh_page_icon",
				"items_id"	=> "menu_tszh_import_export",
				"items" => $export_menu_items
			);
	}

	return $aMenu;
}
return false;
?>