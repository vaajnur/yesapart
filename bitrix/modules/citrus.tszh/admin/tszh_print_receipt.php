<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������

IncludeModuleLangFile(__FILE__);

$hash = preg_replace('/[^\d\w]+/i', '', $_GET['hash']);
if (array_key_exists($hash, $_SESSION['TSZH_PRINT'])) 
{
	$arFilter = $_SESSION['TSZH_PRINT'][$hash]['filter'];
	$arSort = $_SESSION['TSZH_PRINT'][$hash]['sort'];
	$strTemplate = $_SESSION['TSZH_PRINT'][$hash]['template'];

	if (!CTszhReceiptTemplateProcessor::CheckTemplate($strTemplate))
	{
		ShowError(GetMessage("TSZH_PRINT_ERROR_TEMPLATE_NOT_FOUND"));
		return;
	}

	$arTemplatePath = explode('/', $strTemplate);

	if (is_array($arFilter) && is_array($arSort) && is_array($arTemplatePath))
	{
		$APPLICATION->IncludeComponent(
			"citrus:tszh.receipt",
			array_pop($arTemplatePath),
			Array(
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"INLINE" => "N",
				"DISABLE_TIME_ZONE" => "Y",
				"ACCOUNT_PERIOD_FILTER" => $arFilter,
				"ACCOUNT_PERIOD_SORT" => $arSort,
				"CUSTOM_TEMPLATE_PATH" => $strTemplate,
			)
		);
		return;
	}
}

ShowError(GetMessage("TSZH_PRINT_ERROR_SESSION_EXPIRED"));
return;
?>