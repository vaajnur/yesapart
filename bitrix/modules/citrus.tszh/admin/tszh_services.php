<?
// ��������� ��� ����������� �����:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/include.php"); // ������������� ������
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/prolog.php"); // ������ ������

// ��������� �������� ����
IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$UF_ENTITY = "TSZH_SERVICE";

$sTableID = "tbl_services"; // ID �������
$oSort = new CAdminSorting($sTableID, "ID", "DESC"); // ������ ����������
$lAdmin = new CAdminList($sTableID, $oSort); // �������� ������ ������
//echo '<pre>'; var_dump($oSort); echo '</pre>';

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = $arServiceFilter = array();
if (CTszhFunctionalityController::isPortal() && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups, "ENTITY_TYPE" => "tszh")
		)
	);

	while($arPerms = $rsPerms->fetch())
	{
		if($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["ENTITY_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = $arServiceFilter["@TSZH_ID"] = array_keys($arTszhRight);
}

// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
	global $FilterArr, $lAdmin;
	foreach ($FilterArr as $f) global $$f;

	// � ������ ������ ��������� ������.
	// � ����� ������ ����� ��������� �������� ���������� $find_���
	// � � ������ �������������� ������ ���������� �� �����������
	// ����������� $lAdmin->AddFilterError('�����_������').

	return count($lAdmin->arFilterErrors)==0; // ���� ������ ����, ������ false;
}
// *********************** /CheckFilter ******************************* //

// ������ �������� �������
$FilterArr = Array(
	"find",
	"find_type",
	"find_id",
	"find_active",
	"find_name",
	"find_tszh",
);

$USER_FIELD_MANAGER->AdminListAddFilterFields($UF_ENTITY, $FilterArr);

$find_tszh_tmp = IntVal($_REQUEST['find_tszh']);
// �������������� ������
$lAdmin->InitFilter($FilterArr);
$find_tszh = $find_tszh_tmp;

foreach ($FilterArr as $filterVar) {
	if(is_string($_REQUEST[$filterVar]) && CUtil::DetectUTF8($_REQUEST[$filterVar], true))
		CUtil::decodeURIComponent($_REQUEST[$filterVar]);
}

$arFilter = Array();

// ���� ��� �������� ������� ���������, ���������� ���
if (CheckFilter())
{
	if (strlen($_REQUEST['find']) > 0 && strlen($_REQUEST['find_type']) > 0) {
		switch ($_REQUEST['find_type']) {
			case 'ID':
				$arFilter['ID'] = $_REQUEST['find'];
				break;
			default:
		}
	}
	if (IntVal($_REQUEST['find_id']) > 0) {
		$arFilter['ID'] = IntVal($_REQUEST['find_id']);
	}
	if ($_REQUEST['find_active'] == 'Y' || $_REQUEST['find_active'] == 'N') {
		$arFilter['ACTIVE'] = $_REQUEST['find_active'];
	}
	if (strlen($_REQUEST['find_name']) > 0) {
		$arFilter['%NAME'] = $_REQUEST['find_name'];
	}
	if (intval($find_tszh) > 0) {
		$arFilter['TSZH_ID'] = $find_tszh;
	}

	$USER_FIELD_MANAGER->AdminListAddFilter($UF_ENTITY, $arFilter);
}

// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //

// ���������� ����������������� ���������
if($lAdmin->EditAction() && $POST_RIGHT>="W")
{
	// ������� �� ������ ���������� ���������
	foreach($FIELDS as $ID=>$arFields)
	{
		if(!$lAdmin->IsUpdated($ID))
			continue;

		// �������� ��������� ������� ��������
/*		$DB->StartTransaction();
		$ID = IntVal($ID);
		if ($arData = CTszhService::GetByID($ID))
		{
			$USER_FIELD_MANAGER->AdminListPrepareFields($UF_ENTITY, $arFields);
			if(!CTszhService::Update($ID, $arFields))
			{
				$lAdmin->AddGroupError("������ ��� ���������� ������", $ID);
				$DB->Rollback();
			}
		}
		else
		{
			$lAdmin->AddGroupError("������ ��� ����������, ������ �� ����������", $ID);
			$DB->Rollback();
		}
		$DB->Commit();*/
	}
}

// ��������� ��������� � ��������� ��������
if(($arID = $lAdmin->GroupAction()) && $POST_RIGHT=="W")
{
	// ���� ������� "��� ���� ���������"
	if($_REQUEST['action_target']=='selected')
	{
		$arSort = Array();
		$arSortBy = explode('|', $by);
		foreach ($arSortBy as $sBy) {
			$arSort[$sBy] = $order;
		}

		$rsData = CTszhService::GetList($arSort, $arFilter);
		while ($arRes = $rsData->Fetch())
			$arID[] = $arRes['ID'];
	}

	// ������� �� ������ ���������
	foreach($arID as $ID)
	{
		if(strlen($ID)<=0)
			continue;
		$ID = IntVal($ID);

		// ��� ������� �������� �������� ��������� ��������
		switch($_REQUEST['action'])
		{
			// ��������
			case "delete":
				@set_time_limit(0);
				$DB->StartTransaction();
				if (!CTszhService::Delete($ID)) {
					$DB->Rollback();
					$lAdmin->AddGroupError(str_replace("#ID#", IntVal($ID), GetMessage("TS_ERROR_DELETE")), $ID);
				}
				$DB->Commit();
				break;
			case "activate":
				@set_time_limit(0);
				$DB->StartTransaction();
				if (!CTszhService::Update($ID, Array("ACTIVE" => "Y"))) {
					$DB->Rollback();
					$lAdmin->AddGroupError(str_replace("#ID#", IntVal($ID), GetMessage("TS_ERROR_UPDATE")), $ID);
				}
				$DB->Commit();
				break;
			case "deactivate":
				@set_time_limit(0);
				$DB->StartTransaction();
				if (!CTszhService::Update($ID, Array("ACTIVE" => "N"))) {
					$DB->Rollback();
					$lAdmin->AddGroupError(str_replace("#ID#", IntVal($ID), GetMessage("TS_ERROR_UPDATE")), $ID);
				}
				$DB->Commit();
				break;
		}
	}
}
// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //


$arSort = Array();
$arSortBy = explode('|', $by);
foreach ($arSortBy as $sBy) {
	$arSort[$sBy] = $order;
}

$rsData = CTszhService::GetList(
	$arSort,
	array_merge($arFilter, $arServiceFilter),
	false,
	false,
	Array('*', 'UF_*')
);

// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("TS_NAV_TITLE")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

$arAdminListHeaders = array(
	array(
		"id"    =>"ID",
		"content"  =>"ID",
		"sort"    => "ID",
		"align"    =>"left",
		"default"  =>true,
	),
	array(
		"id"		=> "TSZH_ID",
		"content"	=> GetMessage("TS_F_TSZH"),
		"sort"		=> "TSZH_ID",
		"align"		=> "left",
		"default"	=> true,
	),
	array(
		"id"    =>"ACTIVE",
		"content"  => GetMessage("TS_F_ACTIVE"),
		"sort"    => "ACTIVE",
		"align"    =>"left",
		"default"  =>true,
	),
	array(
		"id"    =>"XML_ID",
		"content"  => GetMessage("TS_F_XML_ID"),
		"sort"    => "XML_ID",
		"align"    =>"left",
		"default"  =>false,
	),
	array(
		"id"    =>"NAME",
		"content"  => GetMessage("TS_F_NAME"),
		"sort"    => "NAME",
		"align"    =>"left",
		"default"  =>true,
	),
	array(
		"id"    =>"NORM",
		"content"  => GetMessage("TS_F_NORM"),
		"sort"    => "NORM",
		"align"    =>"right",
		"default"  =>true,
	),
	array(
		"id"    =>"UNITS",
		"content"  => GetMessage("TS_F_UNITS"),
		"sort"    => "UNITS",
		"align"    =>"left",
		"default"  =>true,
	),	
	array(
		"id"    =>"TARIFF",
		"content"  => GetMessage("TS_F_TARIFF"),
		"sort"    => "TARIFF",
		"align"    =>"right",
		"default"  =>true,
	),
	array(
		"id"    =>"TARIFF2",
		"content"  => GetMessage("TS_F_TARIFF2"),
		"sort"    => "TARIFF2",
		"align"    =>"right",
		"default"  =>false,
	),
	array(
		"id"    =>"TARIFF3",
		"content"  => GetMessage("TS_F_TARIFF3"),
		"sort"    => "TARIFF3",
		"align"    =>"right",
		"default"  =>false,
	),
);
$USER_FIELD_MANAGER->AdminListAddHeaders($UF_ENTITY, $arAdminListHeaders);
$lAdmin->AddHeaders($arAdminListHeaders);

while($arRes = $rsData->NavNext(true, "f_")):

	// ������� ������. ��������� - ��������� ������ CAdminListRow
	$row =& $lAdmin->AddRow($f_ID, $arRes);

	// ����� �������� ����������� �������� ��� ��������� � �������������� ������

	$row->AddViewField('ACTIVE', $arRes["ACTIVE"] == 'Y' ? GetMessage("TS_YES") : GetMessage("TS_NO"));

	if ($f_TSZH_ID > 0) {
		$arTszh = CTszh::GetByID($f_TSZH_ID);
	  	$row->AddViewField('TSZH_ID', '[<a href="tszh_edit.php?ID=' . $f_TSZH_ID . '&lang=' . LANGUAGE_ID . '" title="' . GetMessage("TS_VIEW_TSZH") . '">' . $f_TSZH_ID . '</a>] ' . $arTszh["NAME"]);
	} else {
	  	$row->AddViewField('TSZH_ID', '�');
	}

	$USER_FIELD_MANAGER->AddUserFields($UF_ENTITY, $arRes, $row);

	// ���������� ����������� ����
	$arActions = Array();

	if ($POST_RIGHT>="W") {
		// �������� ��������
		$arActions[] = array(
			"ICON"=>"edit",
			"TEXT"=> GetMessage("TS_M_EDIT"),
			"DEFAULT"=>"Y",
			"ACTION"=>$lAdmin->ActionRedirect("tszh_services_edit.php?ID=".$f_ID.'&LANG='.LANG)
		);

		// �������� ��������
		$arActions[] = array(
			"ICON"=>"delete",
			"TEXT"=> GetMessage("TS_M_DELETE"),
			"ACTION"=>"if(confirm('" . GetMessage("TS_M_DELETE_CONFIRM") . "')) ".$lAdmin->ActionDoGroup($f_ID, "delete")
		);
	}

	// ������� �����������
	//$arActions[] = array("SEPARATOR"=>true);

	// ���� ��������� ������� - �����������, �������� �����.
	if(is_set($arActions[count($arActions)-1], "SEPARATOR"))
		unset($arActions[count($arActions)-1]);

	// �������� ����������� ���� � ������
	$row->AddActions($arActions);

endwhile;

// ������ �������
$lAdmin->AddFooter(
	array(
		array("title"=> GetMessage("TS_CNT_TOTAL") . ":", "value"=>$rsData->SelectedRowsCount()), // ���-�� ���������
		array("counter"=>true, "title"=> GetMessage("TS_CNT_SELECTED") . ":", "value"=>"0"), // ������� ��������� ���������
	)
);

// ��������� ��������
$lAdmin->AddGroupActionTable(Array(
	"delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
	"activate"=>GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // ������������ ��������� ��������
	"deactivate"=>GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // �������������� ��������� ��������
));

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //

// ���������� ���� �� ������ ������ - ���������� ��������
$aContext = array(
	array(
		"TEXT"=> GetMessage("TS_C_ADD_SERVICE"),
		"LINK"=>"tszh_services_edit.php?lang=".LANG,
		"TITLE"=> GetMessage("TS_C_ADD_SERVICE_TITLE"),
		"ICON"=>"btn_new",
	),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //

// �������������� �����
$lAdmin->CheckListMode();

// ��������� ��������� ��������
$APPLICATION->SetTitle(GetMessage("TS_PAGE_TITLE"));

// �� ������� ��������� ���������� ������ � �����
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

$arFindFields = Array(
	GetMessage("TS_F_ID"),
	GetMessage("TS_F_ACTIVE"),
	GetMessage("TS_F_NAME"),
	GetMessage("TS_F_TSZH"),
);
$USER_FIELD_MANAGER->AddFindFields($UF_ENTITY, $arFindFields);
// �������� ������ �������
$oFilter = new CAdminFilter(
	$sTableID."_filter",
	$arFindFields
);
?>
<form name="find_form" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
<?$oFilter->Begin();?>
<tr>
  <td><b><?=GetMessage("TS_FIND")?>:</b></td>
  <td>
    <input type="text" size="25" name="find" value="<?echo htmlspecialcharsbx($find)?>" title="">
    <?
		$arr = array(
			"reference_id" => array(
				"ID",
			),
			"reference" => array(
				"ID",
			),
		);
    echo SelectBoxFromArray("find_type", $arr, $find_type, "", "");
    ?>
  </td>
</tr>
<tr>
  <td><?="ID"?>:</td>
  <td>
    <input type="text" name="find_id" size="4" value="<?echo htmlspecialcharsbx($_REQUEST['find_id'])?>">
  </td>
</tr>
<tr>
  <td><?=GetMessage("TS_F_ACTIVE")?>:</td>
  <td>
    <select name="find_active" size="1">
    	<option value=""<?=($_REQUEST['find_active'] != 'Y' && $_REQUEST['find_active'] != 'N' ? ' checked="checked"' : '')?>>(<?=GetMessage("TS_ALL")?>)</option>
		<option value="Y"<?=($_REQUEST['find_active'] == 'Y' ? ' checked="checked"' : '')?>><?=GetMessage("TS_YES")?></option>
		<option value="N"<?=($_REQUEST['find_active'] == 'N' ? ' checked="checked"' : '')?>><?=GetMessage("TS_NO")?></option>
	</select>
  </td>
</tr>
<tr>
  <td><?=GetMessage("TS_F_NAME")?>:</td>
  <td>
    <input type="text" name="find_name" size="20" value="<?echo htmlspecialcharsbx($_REQUEST['find_name'])?>">
  </td>
</tr>
<tr>
	<td><?echo GetMessage("TS_F_TSZH");?>:</td>
	<td>
		<select name="find_tszh">
			<option value=""><?= htmlspecialcharsex(GetMessage("TSZH_F_ALL")) ?></option>
			<?
			$dbTszh = CTszh::GetList(Array("NAME" => "ASC"), $arTszhFilter);
			while ($arTszh = $dbTszh->Fetch())
			{
				?><option value="<?= $arTszh["ID"] ?>"<?if ($arTszh["ID"] == $find_tszh) echo " selected";?>>[<?= htmlspecialcharsex($arTszh["ID"]) ?>]&nbsp;<?= htmlspecialcharsex($arTszh["NAME"]) ?></option><?
			}
			?>
		</select>
	</td>
</tr>
<?
$USER_FIELD_MANAGER->AdminListShowFilter($UF_ENTITY);

$oFilter->Buttons(array("table_id"=>$sTableID,"url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
$oFilter->End();
?>
</form>

<?
// ������� ������� ������ ���������
$lAdmin->DisplayList();
?>

<?
// ���������� ��������
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>