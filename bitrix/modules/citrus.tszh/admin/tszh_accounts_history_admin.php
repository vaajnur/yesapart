<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/prolog.php");
IncludeModuleLangFile(__FILE__);

CUtil::JSPostUnescape();
/*
 * this page only for actions and get info
 *
 */
define('B_ADMIN_SUBELEMENTS',1);
define('B_ADMIN_SUBELEMENTS_LIST',true);

CFile::DisableJSFunction(true);

global $APPLICATION;

$accountID = intval($_REQUEST['ACCOUNT_ID']);

if ($accountID <= 0)
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if (!CModule::IncludeModule("citrus.tszh"))
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if ($APPLICATION->GetGroupRight("citrus.tszh") < 'R')
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));


//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
$strSubElementAjaxPath = '/bitrix/admin/tszh_accounts_history_admin.php?ACCOUNT_ID='.$accountID.'&lang='.LANGUAGE_ID;
require($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/citrus.tszh/tools/tszh_account_history.php');

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_popup_admin.php");
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>