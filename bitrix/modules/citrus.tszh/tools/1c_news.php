<?
require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

IncludeModuleLangFile(__FILE__);

if ($APPLICATION->GetGroupRight("citrus.tszh") < "U")
{
	echo GetMessage("ACCESS_DENIED");
	require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
	return;
}

$iblockID = COption::GetOptionInt('citrus.tszh', "news.iblock", false, SITE_ID);
if (!$iblockID)
{
	echo GetMessage("CITRUS_TSZH_NEWS_IBLOCK_NOT_FOUND");
	require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
	return;
}

// ����������� �� css ������� �����
DeleteDirFilesEx(BX_PERSONAL_ROOT."/tmp/templates/__bx_preview/");
$_GET['bx_template_preview_mode'] = "Y";

?>
<!doctype html>
<html>
<head>
	<?$APPLICATION->ShowHead();?>
	<title><?=$APPLICATION->ShowTitle();?></title>
	<style type="text/css">
	body {
		font: 14px/18px Arial, Helvetica, sans-serif;
	}
	h1 {
		font: 32px/32px Arial, Helvetica, sans-serif;
	}
	</style>
</head>
<body>
<div style="padding: 30px;">
	<h1 id="pagetitle"><?$APPLICATION->ShowTitle(false);?></h1>
	<?$APPLICATION->IncludeComponent("bitrix:news", "citrus.tszh.1c", array(
			"IBLOCK_TYPE" => "news",
			"IBLOCK_ID" => "1",
			"NEWS_COUNT" => "5",
			"USE_SEARCH" => "N",
			"USE_RSS" => "N",
			"USE_RATING" => "N",
			"USE_CATEGORIES" => "N",
			"USE_REVIEW" => "N",
			"USE_FILTER" => "N",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_ORDER1" => "DESC",
			"SORT_BY2" => "SORT",
			"SORT_ORDER2" => "ASC",
			"CHECK_DATES" => "Y",
			"SEF_MODE" => "N",
			"SEF_FOLDER" => "",
			"AJAX_MODE" => "Y",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "Y",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "360000",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"SET_TITLE" => "Y",
			"SET_STATUS_404" => "Y",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"ADD_SECTIONS_CHAIN" => "N",
			"USE_PERMISSIONS" => "N",
			"PREVIEW_TRUNCATE_LEN" => "",
			"LIST_ACTIVE_DATE_FORMAT" => "j F Y",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"DISPLAY_NAME" => "N",
			"META_KEYWORDS" => "KEYWORDS",
			"META_DESCRIPTION" => "DESCRIPTION",
			"BROWSER_TITLE" => "BROWSER_TITLE",
			"DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",
			"DETAIL_PROPERTY_CODE" => array(
				0 => "SOURCE",
				1 => "",
			),
			"DETAIL_DISPLAY_TOP_PAGER" => "N",
			"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
			"DETAIL_PAGER_TITLE" => GetMessage("CITRUS_TSZH_NEWS_DETAIL_PAGER_TITLE"),
			"DETAIL_PAGER_TEMPLATE" => "",
			"DETAIL_PAGER_SHOW_ALL" => "Y",
			"PAGER_TEMPLATE" => "arrows",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"PAGER_TITLE" => GetMessage("CITRUS_TSZH_NEWS_PAGER_TITLE"),
			"PAGER_SHOW_ALWAYS" => "Y",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"DISPLAY_DATE" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"USE_SHARE" => "N",
			"RESIZE_IMAGE_WIDTH" => "150",
			"RESIZE_IMAGE_HEIGHT" => "150",
			"COLORBOX_MAXWIDTH" => "800",
			"COLORBOX_MAXHEIGHT" => "600",
			"MORE_PHOTO" => "",
			"AJAX_OPTION_ADDITIONAL" => "",
			"VARIABLE_ALIASES" => array(
				"SECTION_ID" => "section",
				"ELEMENT_ID" => "id",
			)
		),
		false
	);
	?>
</div>
</body>
</html>
<?

require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
