<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/include.php");

use Citrus\Tszh\HouseTable;

IncludeModuleLangFile(__FILE__);

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}


IncludeModuleLangFile(__FILE__);

$FN = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST["FN"]);
$FC = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST["FC"]);
if ($FN == "")
{
	$FN = "find_form";
}
if ($FC == "")
{
	$FC = "HOUSE_ID";
}

if (isset($_REQUEST['JSFUNC']))
{
	$JSFUNC = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST['JSFUNC']);
}
else
{
	$JSFUNC = '';
}

$UF_ENTITY = "TSZH_HOUSE";

$sTableID = "tbl_tszh_house";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = $arListFilter = array();
if(CModule::IncludeModule("vdgb.portaltszh") && defined("TSZH_PORTAL") && TSZH_PORTAL === true && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups)
		)
	);

	while($arPerms = $rsPerms->fetch())
	{
		if($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = $arListFilter["@TSZH_ID"] = array_keys($arTszhRight);
}

$arTszhList = Array();
$rsTszh = CTszh::GetList(Array("NAME" => "ASC"), $arTszhFilter, false, false, Array("ID", "NAME"));
while ($arTszh = $rsTszh->Fetch())
	$arTszhList[$arTszh['ID']] = $arTszh['NAME'];

$arHeaders = array();
$entityFields = HouseTable::getMap();
$typesMap = array(
	'integer' => 'int',
	'float' => 'int',
	"string" => 'string',
);
$shownByDefault = array(
	"ID",
	"TSZH_ID",
	"FULL_ADDRESS",
	"AREA",
);
foreach ($entityFields as $fieldName => $fieldSettings)
{
	$isNumeric = in_array($fieldSettings['data_type'], array('integer', 'float'));

	$fieldHeading = array(
		"id" => $fieldName,
		"content" => $fieldSettings['title'],
		"align" => $isNumeric ? "right" : "left",
		"default" => in_array($fieldName, $shownByDefault),
		"type" => $typesMap[$fieldSettings['data_type']],
	);
	if (!isset($fieldSettings['expression']))
		$fieldHeading['sort'] = $fieldName;
	if ($fieldName !== "ID" && !isset($fieldSettings['expression']))
		$fieldHeading['edit'] = true;

	if ($fieldName == 'TSZH_ID')
	{
		$fieldHeading['type'] = 'list';
		$fieldHeading['items'] = $arTszhList;
		$fieldHeading['align'] = 'left';
	}

	$arHeaders[] = $fieldHeading;
}

// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
	global $FilterArr, $lAdmin;
	foreach ($FilterArr as $value) {
		global $$value;
	}

	return true;
}
// *********************** /CheckFilter ******************************* //

// ������ �������� �������
$FilterArr = TszhAdminFilterParams($arHeaders);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

$USER_FIELD_MANAGER->AdminListAddFilterFields($UF_ENTITY, $FilterArr);

// ���� ��� �������� ������� ���������, ���������� ���
$arFilter = array();
if (CheckFilter() && !is_set($_REQUEST, 'del_filter'))
{
	// �������� ������ ���������� ��� ������� �� ������ �������� �������
	TszhAdminMakeFilter($arFilter, $arHeaders);
	$USER_FIELD_MANAGER->AdminListAddFilter($UF_ENTITY, $arFilter);
}

// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //

// ������� ������ �������
$rsItems = HouseTable::getList(
	array(
		"order" => array(ToUpper($by)=>$order),
		"filter" => array_merge($arFilter, $arListFilter),
		"select" => Array("*", "FULL_ADDRESS")
	)
);

// ����������� ������ � ��������� ������ CAdminReSlt
$rsData = new CAdminResult($rsItems, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("TSZH_HOUSES")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

$USER_FIELD_MANAGER->AdminListAddHeaders($UF_ENTITY, $arHeaders);
$lAdmin->AddHeaders($arHeaders);

while ($rowData = $rsData->NavNext(true, "f_"))
{
	// ������� ������. ��������� - ��������� ������ CAdminListRow
	$row =& $lAdmin->AddRow($rowData["ID"], $rowData);

	// ����� �������� ����������� �������� ��� ��������� � �������������� ������
	foreach ($arHeaders as $field)
	{
		$fieldName = $field['id'];

		if ($fieldName == 'ID')
		{
			$row->AddViewField("ID", '<a href="tszh_house_edit.php?ID='.$rowData["ID"].'&lang='.LANG.'">'.$rowData["ID"].'</a>');
			continue;
		}
		elseif ($fieldName == 'TSZH_ID')
		{
			$row->AddSelectField($fieldName, $field['items']);
			$row->AddViewField($fieldName, '[<a href="tszh_edit.php?ID=' . $rowData[$fieldName] . '&lang=' . LANGUAGE_ID . '" title="' . GetMessage("AL_VIEW_TSZH") . '">' . $rowData[$fieldName] . '</a>] ' . $field['items'][$rowData[$fieldName]]);
			continue;
		}
		elseif ($fieldName == 'FULL_ADDRESS')
		{
			$row->AddViewField($fieldName, CTszhAccount::GetFullAddress($rowData, Array("FLAT")));
		}

		if (!isset($entityFields[$fieldName]['expression']))
			$row->AddInputField($fieldName, array("size"=>20));
	}
	$USER_FIELD_MANAGER->AddUserFields($UF_ENTITY, $rowData, $row);

	$arActions = array();
	$arActions[] = array(
		"ICON" => "",
		"TEXT" => GetMessage("TSZH_SELECT"),
		"DEFAULT" => true,
		"ACTION" => "SetValue('" . $rowData['ID'] . "');"
	);
	$row->AddActions($arActions);

}

// "������" ������
$lAdmin->AddFooter(
	array(
		array("title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()),
		array("counter" => true, "title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"),
	)
);

$lAdmin->AddAdminContextMenu(array());

// �������� �� ����� ������ ������ (� ������ ������, ������ ������ ����������� �� �����)
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("TSZH_HOUSES"));
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php")
?>
<script language="JavaScript">
	<!--
	function SetValue(id) {
		<?if ($JSFUNC <> ''){?>
		window.opener.SUV<?=$JSFUNC?>(id);
		<?}else{?>
		window.opener.document.<?echo $FN;?>["<?echo $FC;?>"].value = id;
		window.close();
		<?}?>
	}
	//-->
</script>
<?

echo $demoNotice;

if (isset($message) && isset($_REQUEST["edit_form_del"]))
{
	$message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

$arFilterItems = Array();
foreach ($arHeaders as $arField)
{
	$arFilterItems[] = $arField['content'];
}

$USER_FIELD_MANAGER->AddFindFields($UF_ENTITY, $arFilterItems);
$oFilter = new CAdminFilter(
	$sTableID."_filter",
	$arFilterItems
);

?>
	<form name="form_filter" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
		<?
		$oFilter->Begin();

		TszhShowShowAdminFilter($arHeaders);

		$USER_FIELD_MANAGER->AdminListShowFilter($UF_ENTITY);

		$oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "form_filter"));
		$oFilter->End();
		?>
	</form>
<?

// ������� ������� ������ ���������
$lAdmin->DisplayList();

//require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin_js.php");

require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>