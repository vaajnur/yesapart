<?
define("STOP_STATISTICS", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/include.php");

if($APPLICATION->GetGroupRight("citrus.tszh") < "R")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$rsPeriods = CTszhPeriod::GetList(Array(), Array("ID" => $ID));
if ($arPeriod = $rsPeriods->Fetch()) {
	$res = "&nbsp;[<a title='".GetMessage("TSZH_EDIT_PERIOD")."' class='tablebodylink' href='/bitrix/admin/tszh_periods_edit.php?ID=".$arPeriod["ID"]."&lang=".LANG."'>".$arPeriod["ID"]."</a>] ".htmlspecialcharsbx(CTszhPeriod::Format($arPeriod['DATE']));
}
else
	$res = "&nbsp;".GetMessage("TSZH_SERVICE_NOT_FOUND");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_popup_admin.php");

$strName = preg_replace("/[^a-z0-9_\\[\\]]/i", "", $_REQUEST["strName"]);
?>
<script type="text/javascript" id="tszh_lookup_js" src="/bitrix/js/citrus.tszh/tszh_lookup.js?id=div_<?=$strName?>&html=<?=rawurlencode($APPLICATION->ConvertCharset($res, SITE_CHARSET, 'UTF-8'))?>"></script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_popup_admin.php");?>