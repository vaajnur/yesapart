<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/include.php");

// ��������� �������� ����
IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));


IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/admin/user_admin.php");
IncludeModuleLangFile(__FILE__);

$FN = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST["FN"]);
$FC = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST["FC"]);
if($FN == "")
	$FN = "find_form";
if($FC == "")
	$FC = "PERIOD_ID";

if (isset($_REQUEST['JSFUNC']))
{
	$JSFUNC = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST['JSFUNC']);
}
else
{
	$JSFUNC = '';
}
// ������������� �������
$sTableID = "tbl_period_popup";

// ������������� ����������
$oSort = new CAdminSorting($sTableID, "ID", "asc");
// ������������� ������
$lAdmin = new CAdminList($sTableID, $oSort);

// ������������� ���������� ������ - �������
$arFilterFields = Array(
	"find",
	"find_type",
	"find_tszh",
	"find_id",
	"find_date",
	);

$lAdmin->InitFilter($arFilterFields);

//������������� ������� ������� ��� GetList
function CheckFilter($FilterArr) // �������� ��������� �����
{
	global $strError;
	foreach($FilterArr as $f)
		global $$f;


	$strError .= $str;
	if(strlen($str)>0)
	{
		global $lAdmin;
		$lAdmin->AddFilterError($str);
		return false;
	}

	return true;
}

$arFilter = Array();
if(CheckFilter($arFilterFields))
{
	if (strlen($find) > 0 && strlen($find_type) > 0) {
		switch ($find_type) {
			case 'id':
				$arFilter['ID'] = $find;
				break;
			default:
		}
	}

	if (strlen($find_tszh) > 0) {
		$arFilter['TSZH_ID'] = $find_tszh;
	}
	if (strlen(htmlspecialcharsbx($find_id)) > 0) {
		$arFilter["ID"] = htmlspecialcharsbx($find_id);
	}
	if (strlen(htmlspecialcharsbx($find_date)) > 0) {
		$arFilter["DATE"] = htmlspecialcharsbx($find_date);
	}
}

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = $arListFilter = array();
if(CModule::IncludeModule("vdgb.portaltszh") && defined("TSZH_PORTAL") && TSZH_PORTAL === true && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups)
		)
	);

	while($arPerms = $rsPerms->fetch())
	{
		if($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = $arListFilter["@TSZH_ID"] = array_keys($arTszhRight);
}

// ������������� ������ - ������� ������
$rsData = CTszhPeriod::GetList(Array($by => $order), array_merge($arFilter, $arListFilter), false, array("nPageSize"=>CAdminResult::GetNavSize($sTableID)));
$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();

// ��������� ���������� ������
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("PAGES")));

// ��������� ������
$lAdmin->AddHeaders(array(
	array("id"=>"ID",				"content"=>"ID", 	"sort"=>"id", "default"=>true),
	array("id"=>"DATE",				"content"=>GetMessage("TSZH_FLT_DATE"), 	"sort"=>"DATE", "default"=>true),
	array("id"=>"MONTH",				"content"=>GetMessage("TSZH_FLT_PERIOD"), 	"sort"=>"MONTH", "default"=>true),
	array(
		"id"    =>"TSZH_ID",
		"content"  => GetMessage("TSZH_FLT_TSZH_ID"),
		"sort"    => "TSZH_ID",
		"align"    =>"left",
		"default"  =>true,
	),
));


global $DB;
// ���������� ������
while($arRes = $rsData->GetNext())
{

	$row =& $lAdmin->AddRow($arRes['ID'], $arRes);
	//echo '<pre>' . var_export($arRes, true) . '</pre>';

	if ($arRes["TSZH_ID"] > 0) {
		$arTszh = CTszh::GetByID($arRes["TSZH_ID"]);
	  	$row->AddViewField('TSZH_ID', '[<a href="tszh_edit.php?ID=' . $arRes["TSZH_ID"] . '&lang=' . LANGUAGE_ID . '" title="' . GetMessage("AL_VIEW_TSZH") . '">' . $arRes["TSZH_ID"] . '</a>] ' . $arTszh["NAME"]);
	} else {
	  	$row->AddViewField('TSZH_ID', '�');
	}

	$row->AddViewField("DATE", $DB->FormatDate($arRes["DATE"], 'YYYY-MM-DD', CSite::GetDateFormat("SHORT")));
	$row->AddViewField("MONTH", CTszhPeriod::Format($arRes['DATE']));

	$arActions = array();
	$arActions[] = array(
		"ICON"=>"",
		"TEXT"=>GetMessage("TSZH_SELECT"),
		"DEFAULT"=>true,
		"ACTION"=>"SetValue('".$arRes['ID']."');"
	);
	$row->AddActions($arActions);
}

// "������" ������
$lAdmin->AddFooter(
	array(
		array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()),
		array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"),
	)
);

$lAdmin->AddAdminContextMenu(array());

// �������� �� ����� ������ ������ (� ������ ������, ������ ������ ����������� �� �����)
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("TSZH_PAGE_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_popup_admin.php")
?>
<script language="JavaScript">
<!--
function SetValue(id)
{
	<?if ($JSFUNC <> ''){?>
	window.opener.SUV<?=$JSFUNC?>(id);
	<?}else{?>
	window.opener.document.<?echo $FN;?>["<?echo $FC;?>"].value=id;
	window.close();
	<?}?>
}
//-->
</script>
<form name="find_form" method="GET" action="<?echo $APPLICATION->GetCurPage()?>?">
<?
$oFilter = new CAdminFilter(
	$sTableID."_filter",
	array(
		GetMessage('TSZH_FLT_TSZH_ID'),
		GetMessage('TSZH_FLT_ID'),
		GetMessage('TSZH_FLT_DATE'),
	)
);

$oFilter->Begin();
?>
<tr>
	<td><b><?=GetMessage("TSZH_FLT_SEARCH")?></b></td>
	<td nowrap>
		<input type="text" size="25" name="find" value="<?echo htmlspecialcharsbx($find)?>" title="<?=GetMessage("TSZH_FLT_SEARCH_TITLE")?>">
		<select name="find_type">
			<option value="id"<?if($find_type=="id") echo " selected"?>><?=GetMessage('TSZH_FLT_ID')?></option>
		</select>
	</td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_TSZH_ID");?>:</td>
	<td>
		<select name="find_tszh">
			<option value=""><?= htmlspecialcharsex(GetMessage("TSZH_F_ALL")) ?></option>
			<?
			$dbTszh = CTszh::GetList(Array("NAME" => "ASC"), $arTszhFilter);
			while ($arTszh = $dbTszh->Fetch())
			{
				?><option value="<?= $arTszh["ID"] ?>"<?if ($arTszh["ID"] == $find_tszh) echo " selected";?>>[<?= htmlspecialcharsex($arTszh["ID"]) ?>]&nbsp;<?= htmlspecialcharsex($arTszh["NAME"]) ?></option><?
			}
			?>
		</select>
	</td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_ID")?></td>
	<td><input type="text" name="find_id" size="10" value="<?echo htmlspecialcharsbx($find_id)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_DATE")?></td>
	<td><input type="text" name="find_date" size="47" value="<?echo htmlspecialcharsbx($find_date)?>"></td>
</tr>
<input type="hidden" name="FN" value="<?echo htmlspecialcharsbx($FN)?>">
<input type="hidden" name="FC" value="<?echo htmlspecialcharsbx($FC)?>">
<input type="hidden" name="JSFUNC" value="<?echo htmlspecialcharsbx($JSFUNC)?>">
<?
$oFilter->Buttons(array("table_id"=>$sTableID, "url"=>$APPLICATION->GetCurPage(), "form"=>"find_form"));
$oFilter->End();
?>
</form>
<?
// ����� ��� ������ ������
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_popup_admin.php");
?>
