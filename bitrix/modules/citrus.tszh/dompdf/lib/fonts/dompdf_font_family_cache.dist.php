<?php return array(
	'sans-serif' =>
		array(
			'normal' => DOMPDF_FONT_DIR . 'HelveticaNeue',
			'bold' => DOMPDF_FONT_DIR . 'HelveticaNeueBold',
			'italic' => DOMPDF_FONT_DIR . 'HelveticaNeueItalic',
			'bold_italic' => DOMPDF_FONT_DIR . 'HelveticaNeueBoldItalic',
		),
	'helvetica' =>
		array(
			'normal' => DOMPDF_FONT_DIR . 'HelveticaNeue',
			'bold' => DOMPDF_FONT_DIR . 'HelveticaNeueBold',
			'italic' => DOMPDF_FONT_DIR . 'HelveticaNeueItalic',
			'bold_italic' => DOMPDF_FONT_DIR . 'HelveticaNeueBoldItalic',
		),
	'zapfdingbats' =>
		array(
			'normal' => DOMPDF_FONT_DIR . 'ZapfDingbats',
			'bold' => DOMPDF_FONT_DIR . 'ZapfDingbats',
			'italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
			'bold_italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
		),
	'symbol' =>
		array(
			'normal' => DOMPDF_FONT_DIR . 'Symbol',
			'bold' => DOMPDF_FONT_DIR . 'Symbol',
			'italic' => DOMPDF_FONT_DIR . 'Symbol',
			'bold_italic' => DOMPDF_FONT_DIR . 'Symbol',
		),
	'helveticaneue' =>
		array(
			'normal' => DOMPDF_FONT_DIR . 'HelveticaNeue',
			'bold' => DOMPDF_FONT_DIR . 'HelveticaNeueBold',
			'italic' => DOMPDF_FONT_DIR . 'HelveticaNeueItalic',
			'bold_italic' => DOMPDF_FONT_DIR . 'HelveticaNeueBoldItalic',
		),
) ?>