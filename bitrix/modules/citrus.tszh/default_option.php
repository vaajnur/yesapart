<?
\Bitrix\Main\Localization\Loc::loadMessages(__DIR__ . "/options.php");
$citrus_tszh_default_option = array(
	"user_group_id" => "3",
	"meters_block_edit" => "N",
	"dolg_subscription_min_debt" => 1000,
	"1C_FILE_SIZE_LIMIT" => 262144,
	'1c_exchange.UpdateMode' => 'Y',
	'1c_exchange.CreateUsers' => 'Y',
	'1c_exchange.UpdateUsers' => "N",
	'1c_exchange.Action' => 'N',
	"1c_exchange.Depersonalize" => "N",
	"1c_exchange.Debug" => "Y",
	"subscribe_max_emails_per_hit" => CTszhSubscribe::getOption("max_emails_per_hit", true),
	"subscribe_posting_interval" => CTszhSubscribe::getOption("posting_interval", true),
	"hidden_notification" => "N",
	"notification_dummyMail" => "Y",
	"notification_confirmAccount" => "Y",
	"personal_path" => "/personal/",
	"tszh_module_caption" => \Bitrix\Main\Localization\Loc::getMessage("CITRUS_TSZH_MODULE_CAPTION_DEFAULT"),
	"post_354_ShowPenaltiesColumn" => "N",
	"shared_meters" => "Y",
	"pay_to_executors_only" => "N",
);
?>
