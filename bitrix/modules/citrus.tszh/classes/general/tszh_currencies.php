<?

IncludeModuleLangFile(__FILE__);

/**
 * ����� ��� ������ � ��������
 */
class CTszhCurrency
{
	// ������� ��, ���������� ������
	const TABLE_NAME = 'b_tszh_currencies';
	// �������� ������ � ��������� ��������, ���� ������������ ���������������� ���� �������� ������; false, ���� �� ������������
	const USER_FIELD_ENTITY = false;

	/**
	 * @param string $id
	 * @return int
	 */
	protected static function checkId(&$id)
	{
		$id = ToLower($id);
		return preg_match('#^[a-z]{3}$#i', $id);
	}

	/**
	 * @param array $arFields
	 * @param bool $ID
	 * @return bool
	 */
	private static function CheckFields(&$arFields, $ID = false)
	{
		global $APPLICATION, $USER_FIELD_MANAGER;

		$arErrors = Array();

		if (!$ID || array_key_exists("ID", $arFields))
		{
			if (strlen($arFields['ID']) <= 0)
				$arErrors[] = Array('id' => 'TSZH_ERROR_CURRENCIES_NO_ID', 'text' => GetMessage("TSZH_ERROR_CURRENCIES_NO_ID"));
			elseif (!self::checkId($arFields["ID"]))
				$arErrors[] = Array('id' => 'TSZH_ERROR_CURRENCIES_WRONG_ID', 'text' => GetMessage("TSZH_ERROR_CURRENCIES_WRONG_ID"));
		}

		if (!$ID || array_key_exists("NAME", $arFields))
		{
			if (strlen($arFields['NAME']) <= 0)
				$arErrors[] = Array('id' => 'TSZH_ERROR_CURRENCIES_NO_NAME', 'text' => GetMessage("TSZH_ERROR_CURRENCIES_NO_NAME"));
		}

		if (!$ID || array_key_exists("SHORT", $arFields))
		{
			if (strlen($arFields['SHORT']) <= 0)
				$arErrors[] = Array('id' => 'TSZH_ERROR_CURRENCIES_NO_SHORT', 'text' => GetMessage("TSZH_ERROR_CURRENCIES_NO_SHORT"));
		}

		if (!$ID || array_key_exists("FULL", $arFields))
		{
			if (strlen($arFields['FULL']) <= 0)
				$arErrors[] = Array('id' => 'TSZH_ERROR_CURRENCIES_NO_FULL', 'text' => GetMessage("TSZH_ERROR_CURRENCIES_NO_FULL"));
		}

		if (!empty($arErrors))
		{
			$e = new CAdminException($arErrors);
			$APPLICATION->ThrowException($e);
			return false;
		}

		if (self::USER_FIELD_ENTITY && !$USER_FIELD_MANAGER->CheckFields(self::USER_FIELD_ENTITY, $ID, $arFields))
			return false;

		return true;
	}

	/**
	 * ���������� ������
	 *
	 * @param array $arFields ������ � ������ ����� ������
	 * @return bool false � ������ ������
	 */
	public static function Add($arFields)
	{
		global $USER_FIELD_MANAGER;

		if (!self::CheckFields($arFields))
			return false;

		$ID = CDatabase::Add(self::TABLE_NAME, $arFields);
		if (self::USER_FIELD_ENTITY && $ID > 0 && CTszh::hasUserFields($arFields))
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);

		return $ID;
	}

	/**
	 * ���������� ������
	 *
	 * @param string $ID ������������� ������
	 * @param array $arFields ������ � ������ ������
	 * @return boolean � ������ ������ ����������� false, �������� ������ ����� �������� ����� $APPLICATION->GetException()
	 * @throws \Bitrix\Main\ArgumentTypeException
	 */
	public static function Update($ID, $arFields)
	{
		global $DB, $USER_FIELD_MANAGER;

		if (!self::checkId($ID))
			throw new \Bitrix\Main\ArgumentTypeException("ID");

		if (!self::CheckFields($arFields, $ID))
			return false;

		$strUpdate = $DB->PrepareUpdate(self::TABLE_NAME, $arFields);
		$strSql =
			"UPDATE " . self::TABLE_NAME . " SET ".
			$strUpdate.
			" WHERE ID='".$DB->ForSql($ID)."'";

		$bSuccess = $DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);
		if (self::USER_FIELD_ENTITY && $bSuccess && CTszh::hasUserFields($arFields))
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);

		return $bSuccess;
	}

	/**
	 * �������� ������
	 *
	 * @param string $ID ������������� ������
	 * @return boolean � ������ ������ ����������� false, �������� ������ ����� �������� ����� $APPLICATION->GetException()
	 * @throws \Bitrix\Main\ArgumentTypeException
	 */
	public static function Delete($ID)
	{
		global $DB, $USER_FIELD_MANAGER;

		if (!self::checkId($ID))
			throw new \Bitrix\Main\ArgumentTypeException("ID");

		$DB->StartTransaction();

		$strSql = 'DELETE FROM ' . self::TABLE_NAME . ' WHERE ID=\''.$DB->ForSql($ID)."'";
		$DB->Query($strSql, false, "FILE: ".__FILE__."<br> LINE: ".__LINE__);

		if (self::USER_FIELD_ENTITY)
			$USER_FIELD_MANAGER->Delete(self::USER_FIELD_ENTITY, $ID);

		$DB->Commit();

		return true;
	}

	/**
	 * ��������� ������
	 *
	 * @param array $arOrder ����������� ����������. ������, ������� �������� �������� ���� �����, ���������� � ����������� asc ��� desc.
	 * @param array $arFilter ������, �������� ������ �� ������������ ������. ������� � ������� �������� �������� �����, � ���������� � �� ��������.
	 * @param array|bool $arGroupBy ������, �������� ����������� ��������������� ������. ���� �������� �������� ������ �������� �����, �� �� ���� ����� ����� ����������� �����������. ���� �������� �������� ������ ������, �� ����� ������ ���������� �������, ��������������� �������. �� ��������� �������� ����� false � �� ������������.
	 * @param array|bool $arNavStartParams ������, �������� ������� ������ ��� ����������� ������������ ���������.
	 * @param array $arSelectFields ������, ���������� ������ �����, ������� ������ ������������� � ���������� �������
	 * @return CDBResult|array ����� ���������� ������ ���� CDBResult, ���������� ������, ��������������� ������� �������. ��������� ������ ��� ������������� �����������
	 */
	public static function GetList($arOrder = Array(), $arFilter = Array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = Array())
	{
		global $USER_FIELD_MANAGER;

		static $arFields = array(
			"ID" => Array("FIELD" => "T.ID", "TYPE" => "string"),
			"NAME" => Array("FIELD" => "T.NAME", "TYPE" => "string"),
			"SHORT" => Array("FIELD" => "T.SHORT", "TYPE" => "string"),
			"FULL" => Array("FIELD" => "T.FULL", "TYPE" => "string"),
		);

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = array_keys($arFields);
		}
		elseif (in_array('*', $arSelectFields))
		{
			$arSelectFields = array_keys($arFields);
		}

		if (self::USER_FIELD_ENTITY)
		{
			$obUserFieldsSql = new CUserTypeSQL;
			$obUserFieldsSql->SetEntity(self::USER_FIELD_ENTITY, "T.ID");
			$obUserFieldsSql->SetSelect($arSelectFields);
			$obUserFieldsSql->SetFilter($arFilter);
			$obUserFieldsSql->SetOrder($arOrder);

			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . ' T', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields,  $obUserFieldsSql, $strUserFieldsID = "T.ID");
		}
		else
			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . ' T', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

		if (self::USER_FIELD_ENTITY && is_object($dbRes))
			$dbRes->SetUserFields($USER_FIELD_MANAGER->GetUserFields(self::USER_FIELD_ENTITY));

		return $dbRes;
	}

	/**
	 * ��������� ����� ������ � ��������������� $ID
	 *
	 * @param int $ID ������������� ������
	 * @return array
	 * @throws \Bitrix\Main\ArgumentTypeException
	 */
	public static function GetByID($ID)
	{
		if (!self::checkId($ID))
			throw new \Bitrix\Main\ArgumentTypeException("ID");
		return self::GetList(Array(), Array("ID" => $ID))->GetNext();
	}

}

