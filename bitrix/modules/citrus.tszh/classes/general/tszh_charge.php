<?php

IncludeModuleLangFile(__FILE__);

$GLOBALS["TSZH_SERVICE"] = Array();

/**
 * CTszhCharge
 * ����� ��� ������ � ������������
 *
 * ����
 * ----
 * ID            - ID ������
 * SERVICE_ID    - ID ������
 * ACCOUNT_ID    - ID �������� �����
 * TIMESTAMP_X  - ���� ���������
 * PERIOD_ID    - ������
 * AMOUNT        - ���������� (���������)
 * SUMM            - ����� (���������)
 * SUMM_PAYED    - ���������� �����
 * SUMM2PAY        - ����� � ������
 *
 *
 * @package
 * @author
 * @copyright nook.yo
 * @version 2010
 * @access public
 */
class CTszhCharge
{
	// ������� ��, ���������� ������
	const TABLE_NAME = 'b_tszh_services_period';
	// ������� ��, ���������� �������� ��������� � �����������
	const TABLE_NAME_CHARGES_METERS = 'b_tszh_charges_meters';
	// �������� ������ � ��������� ��������, ���� ������������ ���������������� ���� �������� ������; false, ���� �� ������������
	const USER_FIELD_ENTITY = 'TSZH_CHARGE';

	public static $defaultSelectFields = array(
		"SERVICE_NAME",
	);

	public static $arGetListFields = array(
		// b_tszh_services_period
		"ID" => Array("FIELD" => "TSP.ID", "TYPE" => "int"),
		"COMPONENT" => Array("FIELD" => "TSP.COMPONENT", "TYPE" => "char"),
		"GROUP" => Array("FIELD" => "TSP.GROUP", "TYPE" => "string"),
		"SERVICE_ID" => Array("FIELD" => "TSP.SERVICE_ID", "TYPE" => "int"),
		"ACCOUNT_PERIOD_ID" => Array("FIELD" => "TSP.ACCOUNT_PERIOD_ID", "TYPE" => "int"),
		"DEBT_ONLY" => Array("FIELD" => "TSP.DEBT_ONLY", "TYPE" => "char"),
		"AMOUNT" => Array("FIELD" => "TSP.AMOUNT", "TYPE" => "double"),
		"AMOUNT_VIEW" => Array("FIELD" => "TSP.AMOUNT_VIEW", "TYPE" => "string"),
		"AMOUNTN" => Array("FIELD" => "TSP.AMOUNTN", "TYPE" => "int"),
		"AMOUNT_NORM" => Array("FIELD" => "TSP.AMOUNT_NORM", "TYPE" => "double"),
		"RATE" => Array("FIELD" => "TSP.RATE", "TYPE" => "string"),
		"SUMM" => Array("FIELD" => "TSP.SUMM", "TYPE" => "double"),
		"CORRECTION" => Array("FIELD" => "TSP.CORRECTION", "TYPE" => "double"),
		"COMPENSATION" => Array("FIELD" => "TSP.COMPENSATION", "TYPE" => "double"),
		"SUMM_PAYED" => Array("FIELD" => "TSP.SUMM_PAYED", "TYPE" => "double"),
		"SUMM2PAY" => Array("FIELD" => "TSP.SUMM2PAY", "TYPE" => "double"),
        "DEBT_OVERPAY" => Array("FIELD" => "TSP.DEBT_OVERPAY", "TYPE" => "double"),
		"SORT" => Array("FIELD" => "TSP.SORT", "TYPE" => "int"),
		"HAMOUNT" => Array("FIELD" => "TSP.HAMOUNT", "TYPE" => "double"),
		"HAMOUNTN" => Array("FIELD" => "TSP.HAMOUNTN", "TYPE" => "int"),
		"HSUMM" => Array("FIELD" => "TSP.HSUMM", "TYPE" => "double"),
		"HSUMM2PAY" => Array("FIELD" => "TSP.HSUMM2PAY", "TYPE" => "double"),
		"HNORM" => Array("FIELD" => "TSP.HNORM", "TYPE" => "double"),
		"VOLUMEP" => Array("FIELD" => "TSP.VOLUMEP", "TYPE" => "double"),
		"VOLUMEH" => Array("FIELD" => "TSP.VOLUMEH", "TYPE" => "double"),
		"VOLUMEA" => Array("FIELD" => "TSP.VOLUMEA", "TYPE" => "double"),
		"PENALTIES" => Array("FIELD" => "TSP.PENALTIES", "TYPE" => "double"),
		"DEBT_BEG" => Array("FIELD" => "TSP.DEBT_BEG", "TYPE" => "double"),
		"DEBT_END" => Array("FIELD" => "TSP.DEBT_END", "TYPE" => "double"),
		"X_FIELDS" => Array("FIELD" => "TSP.X_FIELDS", "TYPE" => "string"),

        "RAISE_MULTIPLIER" => Array("FIELD" => "TSP.RAISE_MULTIPLIER", "TYPE" => "double"),
        "RAISE_SUM" => Array("FIELD" => "TSP.RAISE_SUM", "TYPE" => "double"),
        "SUM_WITHOUT_RAISE" => Array("FIELD" => "TSP.SUM_WITHOUT_RAISE", "TYPE" => "double"),
        "CSUM_WITHOUT_RAISE" => Array("FIELD" => "TSP.CSUM_WITHOUT_RAISE", "TYPE" => "double"),
        "IS_INSURANCE" => Array("FIELD" => "TSP.IS_INSURANCE", "TYPE" => "char"),
		"CONTRACTOR_ID" => Array("FIELD" => "TSP.CONTRACTOR_ID", "TYPE" => "char"),
		"GUID" => Array("FIELD" => "TSP.GUID", "TYPE" => "char"),

		// b_tszh_accounts_period
		"ACCOUNT_ID" => Array(
			"FIELD" => "TAP.ACCOUNT_ID",
			"TYPE" => "int",
			"FROM" => "INNER JOIN b_tszh_accounts_period TAP ON (TSP.ACCOUNT_PERIOD_ID = TAP.ID)"
		),
		"PERIOD_ID" => Array(
			"FIELD" => "TAP.PERIOD_ID",
			"TYPE" => "int",
			"FROM" => "INNER JOIN b_tszh_accounts_period TAP ON (TSP.ACCOUNT_PERIOD_ID = TAP.ID)"
		),
		"PERIOD_TYPE" => Array(
			"FIELD" => "TAP.TYPE",
			"TYPE" => "int",
			"FROM" => "INNER JOIN b_tszh_accounts_period TAP ON (TSP.ACCOUNT_PERIOD_ID = TAP.ID)"
		),

		// b_tszh_services
		"SERVICE_ACTIVE" => Array("FIELD" => "TS.ACTIVE", "TYPE" => "char", "FROM" => "INNER JOIN b_tszh_services TS ON (TSP.SERVICE_ID = TS.ID)"),
		"SERVICE_XML_ID" => Array("FIELD" => "TS.XML_ID", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_services TS ON (TSP.SERVICE_ID = TS.ID)"),
		"SERVICE_TSZH_ID" => Array("FIELD" => "TS.TSZH_ID", "TYPE" => "int", "FROM" => "INNER JOIN b_tszh_services TS ON (TSP.SERVICE_ID = TS.ID)"),
		"SERVICE_NAME" => Array("FIELD" => "TS.NAME", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_services TS ON (TSP.SERVICE_ID = TS.ID)"),
		"SERVICE_NORM" => Array("FIELD" => "TS.NORM", "TYPE" => "double", "FROM" => "INNER JOIN b_tszh_services TS ON (TSP.SERVICE_ID = TS.ID)"),
		"SERVICE_TARIFF" => Array("FIELD" => "TS.TARIFF", "TYPE" => "double", "FROM" => "INNER JOIN b_tszh_services TS ON (TSP.SERVICE_ID = TS.ID)"),
		"SERVICE_TARIFF2" => Array(
			"FIELD" => "TS.TARIFF2",
			"TYPE" => "double",
			"FROM" => "INNER JOIN b_tszh_services TS ON (TSP.SERVICE_ID = TS.ID)"
		),
		"SERVICE_TARIFF3" => Array(
			"FIELD" => "TS.TARIFF3",
			"TYPE" => "double",
			"FROM" => "INNER JOIN b_tszh_services TS ON (TSP.SERVICE_ID = TS.ID)"
		),
		"SERVICE_UNITS" => Array("FIELD" => "TS.UNITS", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_services TS ON (TSP.SERVICE_ID = TS.ID)"),

		// b_tszh_account
		"USER_ID" => Array("FIELD" => "TA.USER_ID", "TYPE" => "int", "FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID = TA.ID)"),

		// b_tszh_period
		"PERIOD_TSZH_ID" => Array("FIELD" => "TP.TSZH_ID", "TYPE" => "int", "FROM" => "INNER JOIN b_tszh_period TP ON (TAP.PERIOD_ID = TP.ID)"),
		"PERIOD_DATE" => Array("FIELD" => "TP.DATE", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_period TP ON (TAP.PERIOD_ID = TP.ID)"),
		"PERIOD_ACTIVE" => Array("FIELD" => "TP.ACTIVE", "TYPE" => "char", "FROM" => "INNER JOIN b_tszh_period TP ON (TAP.PERIOD_ID = TP.ID)"),
		"PERIOD_ONLY_DEBT" => Array("FIELD" => "TP.ONLY_DEBT", "TYPE" => "char", "FROM" => "INNER JOIN b_tszh_period TP ON (TAP.PERIOD_ID = TP.ID)"),

		"HMETER_ID" => Array(
			"FIELD" => "_TCM.HMETER_ID",
			"TYPE" => "int",
			"FROM" => "LEFT JOIN (SELECT TCMH.CHARGE_ID, MIN(TCMH.METER_ID) AS HMETER_ID FROM b_tszh_charges_meters TCMH INNER JOIN b_tszh_meters TMH ON (TMH.ID = TCMH.METER_ID AND TMH.HOUSE_METER = 'Y') GROUP BY TCMH.CHARGE_ID) _TCM ON (_TCM.CHARGE_ID = TSP.ID)"
		),
		"HMETER_IDS" => Array(
			"FIELD" => "TM2.ID",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TMS.ID ORDER BY TMS.ID SEPARATOR '|') FROM b_tszh_meters TMS INNER JOIN b_tszh_charges_meters TCMS ON TCMS.METER_ID = TMS.ID WHERE TSP.ID = TCMS.CHARGE_ID AND TMS.HOUSE_METER = 'Y')",
			"TYPE" => "int",
			"FROM" => "LEFT JOIN b_tszh_charges_meters TCM ON (TCM.CHARGE_ID = TSP.ID) LEFT JOIN b_tszh_meters TM2 ON (TM2.ID = TCM.METER_ID AND TM2.HOUSE_METER = 'Y')",
			/*"BINDING_TABLE" => "b_tszh_meters TMW INNER JOIN b_tszh_charges_meters TCMW ON (TCMW.METER_ID = TMW.ID AND TMW.HOUSE_METER = 'Y')", 
			"BINDING_EXTERNAL_FIELD" => "TSP.ID", 
			"BINDING_SELECT_FIELD" => "TCMW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TMW.ID"*/
		),
		"METER_IDS" => Array(
			"FIELD" => "TM1.ID",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TMS.ID ORDER BY TMS.ID SEPARATOR '|') FROM b_tszh_meters TMS INNER JOIN b_tszh_charges_meters TCMS ON TCMS.METER_ID = TMS.ID WHERE TSP.ID = TCMS.CHARGE_ID AND TMS.HOUSE_METER != 'Y')",
			"TYPE" => "int",
			"FROM" => "LEFT JOIN b_tszh_meters TM1 ON (TM1.ID = TCM.METER_ID AND TM1.HOUSE_METER != 'Y')",
			/*"BINDING_TABLE" => "b_tszh_meters TMW INNER JOIN b_tszh_charges_meters TCMW ON (TCMW.METER_ID = TMW.ID AND TMW.HOUSE_METER != 'Y')", 
			"BINDING_EXTERNAL_FIELD" => "TSP.ID", 
			"BINDING_SELECT_FIELD" => "TCMW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TMW.ID"*/
		),
	);

	/**
	 * CTszhCharge::obtainMeterIDs()
	 * ���������� ������ ID ��������� �� ����� ����������
	 *
	 * @param array $arFields ���� ����������
	 *
	 * @return array
	 */
	private static function obtainMeterIDs($arFields)
	{
		$arMeterIDs = array();
		$arMeterFields = array("HMETER_ID", "HMETER_IDS", "METER_IDS");
		foreach ($arMeterFields as $meterField)
		{
			if (isset($arFields[$meterField]) && !empty($arFields[$meterField]))
			{
				if (is_array($arFields[$meterField]))
				{
					$arMeterIDs = array_merge($arMeterIDs, $arFields[$meterField]);
				}
				else
				{
					$arMeterIDs[] = $arFields[$meterField];
				}
			}
		}

		return array_unique($arMeterIDs);
	}

	/**
	 * ����� ���������� ������������� ������ ���������� ���������� � ����� ID
	 *
	 * @param int $ID ID ����������
	 *
	 * @return array|false
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function GetByID($ID)
	{
		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (isset($GLOBALS["TSZH_SERVICE"]["CHARGE_CACHE_" . $ID]) && is_array($GLOBALS["TSZH_SERVICE"]["CHARGE_CACHE_" . $ID]) && is_set($GLOBALS["TSZH_SERVICE"]["CACHE_" . $ID], "ID"))
		{
			return $GLOBALS["TSZH_SERVICE"]["CHARGE_CACHE_" . $ID];
		}
		else
		{
			$arCharge = self::GetList(array(), array("ID" => $ID), false, array("nTopCount" => 1))->GetNext();
			if (is_array($arCharge) && !empty($arCharge))
			{
				$GLOBALS["TSZH_SERVICE"]["CHARGE_CACHE_" . $arCharge['ID']] = $arCharge;

				return $arCharge;
			}
		}

		return false;
	}

	/**
	 * @param array $arFields
	 * @param string $strOperation
	 *
	 * @return bool
	 */
	public static function CheckFields(&$arFields, $strOperation = 'ADD')
	{
		global $APPLICATION, $USER_FIELD_MANAGER;

		$arErrors = Array();

		if (array_key_exists('SERVICE_ID', $arFields) || $strOperation == 'ADD')
		{
			$arFields['SERVICE_ID'] = IntVal($arFields['SERVICE_ID']);
		}

		/**
		 * ��� ������������ ������������� �� ������� ���������� ����������� ������
		 *
		 * ��� ���������� ������ ������� �������� (�� �������� ACCOUNT_ID, PERIOD_ID ������ ������ ACCOUNT_PERIOD_ID)
		 * ������� ���������� ����������� ��������������� ACCOUNT_PERIOD_ID ���������� �� �� (�� ������ ���� ������ ���� ��� �������� ���������)
		 *
		 * ������������� �� ������ ������� ������ �������������� �� ������ ��������� � ������� � �������������� �������� (����� ����������� self::$arGetListFields)
		 */
		if (!isset($arFields["ACCOUNT_PERIOD_ID"]))
		{
			if (array_key_exists('ACCOUNT_ID', $arFields) && array_key_exists('PERIOD_ID', $arFields))
			{
				$dbAccountPeriod = CTszhAccountPeriod::GetList(array(), array(
					"ACCOUNT_ID" => $arFields["ACCOUNT_ID"],
					"PERIOD_ID" => $arFields["PERIOD_ID"],
					"TYPE" => 0,
				));
				/** @noinspection PhpAssignmentInConditionInspection */
				if ($accountPeriod = $dbAccountPeriod->Fetch())
				{
					$arFields["ACCOUNT_PERIOD_ID"] = $accountPeriod['ID'];
					unset($arFields['ACCOUNT_ID']);
					unset($arFields['PERIOD_ID']);
				}
				else
				{
					$arErrors[] = GetMessage("CITRUS_TSZH_ERROR_ACCOUNT_PERIOD_NOT_FOUND_COMPAT", array(
						"#ACCOUNT_ID#" => $arFields["ACCOUNT_ID"],
						"#PERIOD_ID#" => $arFields["PERIOD_ID"],
					));
				}
			}
			elseif (array_key_exists('ACCOUNT_ID', $arFields) || array_key_exists('PERIOD_ID', $arFields))
			{
				$arErrors[] = GetMessage("CITRUS_TSZH_ERROR_ACCOUNT_PERIOD_DEPRECATED_USE");
			}
		}

		if (array_key_exists('COMPENSATION', $arFields) || $strOperation == 'ADD')
		{
			$arFields['COMPENSATION'] = FloatVal($arFields['COMPENSATION']);
			if ($arFields['COMPENSATION'] <= 0)
			{
				$arFields['COMPENSATION'] = 0;
			}
		}

		if (array_key_exists('DEBT_ONLY', $arFields) || $strOperation == 'ADD')
		{
			$arFields['DEBT_ONLY'] = $arFields['DEBT_ONLY'] == "Y" ? "Y" : "N";
		}

		if ($strOperation == 'ADD' && $arFields['ACCOUNT_PERIOD_ID'] <= 0)
		{
			$arErrors[] = GetMessage("CITRUS_TSZH_ERROR_TSZH_CHARGE_NO_ACCOUNT_PERIOD_ID");
		}

		if (count($arErrors) > 0)
		{
			$APPLICATION->ThrowException(implode('<br>', $arErrors));

			return false;
		}

		if (self::USER_FIELD_ENTITY && !$USER_FIELD_MANAGER->CheckFields(self::USER_FIELD_ENTITY, $arFields['ID'], $arFields))
		{
			return false;
		}

		unset($arFields['ID']);

		return true;
	}

	/**
	 * ����� ��������� ����� ������ � ����������
	 *
	 * @param array $arFields ���� ����������
	 *
	 * @return bool
	 */
	public static function Add($arFields)
	{
		global $USER_FIELD_MANAGER;

		if (!self::CheckFields($arFields))
		{
			return false;
		}

		$ID = CDatabase::Add(self::TABLE_NAME, $arFields);
		if (self::USER_FIELD_ENTITY && $ID > 0 && CTszh::hasUserFields($arFields))
		{
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		if ($ID > 0)
		{
			$arMeterIDs = self::obtainMeterIDs($arFields);
			if (!empty($arMeterIDs))
			{
				self::bindMeter($ID, $arMeterIDs);
			}
		}

		return $ID;
	}

	/**
	 * CTszhCharge::Update()
	 * ����� �������� ���������� � ����������
	 *
	 * @param int $ID ID ����������
	 * @param array $arFields ���� ����������
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 * @return
	 */
	public static function Update($ID, $arFields)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (!self::CheckFields($arFields, 'UPDATE'))
		{
			return false;
		}

		$strUpdate = $DB->PrepareUpdate(self::TABLE_NAME, $arFields);
		$strSql =
			"UPDATE " . self::TABLE_NAME . " SET " .
			$strUpdate .
			" WHERE ID=" . $ID;

		$bSuccess = (bool)$DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		if (self::USER_FIELD_ENTITY && $bSuccess && CTszh::hasUserFields($arFields))
		{
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		if ($bSuccess && ($arMeterIDs = self::obtainMeterIDs($arFields)) && !empty($arMeterIDs))
		{
			self::bindMeter($ID, $arMeterIDs);
		}

		return $bSuccess;
	}

	/**
	 * ������� ����������
	 *
	 * @param int $ID ID ����������
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Delete($ID)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = intval($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		//$DB->StartTransaction();

		self::unbindMeter($ID);

		$strSql = 'DELETE FROM ' . self::TABLE_NAME . ' WHERE ID=' . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		if (self::USER_FIELD_ENTITY && $z)
		{
			$USER_FIELD_MANAGER->Delete(self::USER_FIELD_ENTITY, $ID);
		}

		//$DB->Commit();

		return true;
	}

	/**
	 * ������� ������� ���������� �� ��������� ������
	 *
	 * @param int $PERIOD_ID ID �������
	 * @param int|bool $ACCOUNT_ID ID �������� ����� (���� �� ������, �� ��������� ���������� ���� ������� ������)
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function DeletePeriod($PERIOD_ID, $ACCOUNT_ID = false)
	{
		$PERIOD_ID = intval($PERIOD_ID);
		if ($PERIOD_ID <= 0)
		{
			return false;
		}

		$ACCOUNT_ID = IntVal($ACCOUNT_ID);

		$arFilter = array("PERIOD_ID" => $PERIOD_ID);
		if ($ACCOUNT_ID > 0)
		{
			$arFilter["ACCOUNT_ID"] = $ACCOUNT_ID;
		}
		$rsCharges = self::getList(
			array("ID" => "ASC"),
			$arFilter,
			false,
			false,
			array("ID")
		);
		$arIDs = array();
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arCharge = $rsCharges->fetch())
		{
			$arIDs[] = $arCharge["ID"];
		}

		foreach ($arIDs as $id)
		{
			self::delete($id);
		}

		return true;
	}

	/**
	 * CTszhCharge::GetList()
	 * ����� ���������� ������ ����������, ��������������� ���������� ��������
	 *
	 * @param array $arOrder ������� ���������� (������������� ������)
	 * @param array $arFilter ���� �������
	 * @param bool|array $arGroupBy �����������
	 * @param bool|array $arNavStartParams ��������� ��� ������������ ��������� � ����������� ���������� ��������� ���������
	 * @param array $arSelectFields ������ ������������ �����
	 *
	 * @return CTszhChargeResult
	 */
	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		global $USER_FIELD_MANAGER;

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = Array();
			foreach (self::$arGetListFields as $fieldName => $fieldDefinition)
			{
				if (!isset($fieldDefinition["FROM"]) || in_array($fieldName, self::$defaultSelectFields))
				{
					$arSelectFields[] = $fieldName;
				}
			}
		}
		elseif (in_array('*', $arSelectFields))
		{
			$arSelectFields = array_keys(self::$arGetListFields);
		}

		if (in_array("METER_IDS", self::$arGetListFields))
		{
			$arGetListFields[] = "HMETER_IDS";
		}

		/**
		 * ��� ������� ����� ��� ��� ���������� ��������� ������ (����� ������� ������� ������ � ������� ��������)
		 * ��� ��� �� ����� ��������� ���������� INNER JOIN �� ������� �/� � ��������
		 */
		$arSelectFields = array_merge(array("ACCOUNT_ID", "PERIOD_ID"), $arSelectFields);
		$arSelectFields = array_unique($arSelectFields);

		if (self::USER_FIELD_ENTITY)
		{
			$obUserFieldsSql = new CUserTypeSQL;
			$obUserFieldsSql->SetEntity(self::USER_FIELD_ENTITY, "TSP.ID");
			$obUserFieldsSql->SetSelect($arSelectFields);
			$obUserFieldsSql->SetFilter($arFilter);
			$obUserFieldsSql->SetOrder($arOrder);

			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . ' TSP', self::$arGetListFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields, $obUserFieldsSql, $strUserFieldsID = "TSP.ID");
		}
		else
		{
			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . ' TSP', self::$arGetListFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
		}

		if (self::USER_FIELD_ENTITY && is_object($dbRes))
		{
			$dbRes->SetUserFields($USER_FIELD_MANAGER->GetUserFields(self::USER_FIELD_ENTITY));
		}

		return new CTszhChargeResult($dbRes, self::$arGetListFields);
	}

	/**
	 * CTszhCharge::bindMeter()
	 * ����������� ���������� � ��������
	 *
	 * @param int $chargeID ID ����������
	 * @param int|array $meterID ID �������� | ������ ID ���������
	 *
	 * @return true � ������ ������, ����� false
	 */
	public static function bindMeter($chargeID, $meterID)
	{
		global $DB;

		$chargeID = intval($chargeID);
		if ($chargeID <= 0)
		{
			return false;
		}

		if (!is_array($meterID))
		{
			$meterID = array(intval($meterID));
		}

		if (empty($meterID))
		{
			return false;
		}

		foreach ($meterID as $key => $value)
		{
			$meterID[$key] = intval($value);
			if ($meterID[$key] <= 0)
			{
				return false;
			}
		}

		$res = true;

		$DB->StartTransaction();
		foreach ($meterID as $key => $value)
		{
			$strSql = "SELECT CHARGE_ID, METER_ID FROM " . self::TABLE_NAME_CHARGES_METERS . " WHERE CHARGE_ID={$chargeID} AND METER_ID={$value}";
			$dbBindig = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			if (!$dbBindig->Fetch())
			{
				$strSql = "INSERT INTO " . self::TABLE_NAME_CHARGES_METERS . " (CHARGE_ID, METER_ID) VALUES ({$chargeID}, {$value})";
				$res = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);
				if (!$res)
				{
					break;
				}
			}
		}
		if ($res)
		{
			$DB->Commit();
		}
		else
		{
			$DB->Rollback();
		}

		return $res;
	}

	/**
	 * CTszhCharge::unbindMeter()
	 * ���������� ���������� �� ���������
	 *
	 * @param int $chargeID ID ����������
	 * @param int|array|bool $meterID ID �������� | ������ ID ��������� (���� �� �����, �� ���������� ���������� �� ���� ���������)
	 *
	 * @return true � ������ ������, ����� false
	 */
	public static function unbindMeter($chargeID, $meterID = false)
	{
		global $DB;

		$chargeID = intval($chargeID);
		if ($chargeID <= 0)
		{
			return false;
		}

		$delAllBindingsFlag = $meterID === false || is_array($meterID) && empty($meterID);
		if (!$delAllBindingsFlag)
		{
			if (!is_array($meterID))
			{
				$meterID = array(intval($meterID));
			}

			foreach ($meterID as $key => $value)
			{
				$meterID[$key] = intval($value);
				if ($meterID[$key] <= 0)
				{
					return false;
				}
			}
		}
		$res = true;

		$DB->StartTransaction();
		if ($delAllBindingsFlag)
		{
			$strSql = "DELETE FROM " . self::TABLE_NAME_CHARGES_METERS . " WHERE CHARGE_ID={$chargeID}";
			$res = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);
		}
		else
		{
			foreach ($meterID as $key => $value)
			{
				$strSql = "DELETE FROM " . self::TABLE_NAME_CHARGES_METERS . " WHERE CHARGE_ID={$chargeID} AND METER_ID={$value}";
				$res = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);
				if (!$res)
				{
					break;
				}
			}
		}
		if ($res)
		{
			$DB->Commit();
		}
		else
		{
			$DB->Rollback();
		}

		return $res;
	}
}