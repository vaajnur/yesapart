<?
use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Entity\EventResult;
use Bitrix\Main\Localization\Loc;
use Citrus\Tszh\Subscribe\CTszhSubscribeTable;

Loc::loadMessages(__FILE__);

/**
 * ����� �������� ����������� � ������������� ����� ��������� ���������
 */
class CTszhSubscribeMeterValues extends CTszhBaseSubscribe implements ITszhSubscribeAgent
{
	const CODE = "MeterValues";
	const TYPE = self::TYPE_OPTIONAL;
	const EVENT_TYPE = "TSZH_METERS_VALUES_NEED_NOTICE";
	const SORT = 200;

	const EXEC_HOUR = 2;

	protected static $arParams = array(
		"DATE" => array(
			"type" => "int",
			"default" => 25,
			"readOnly" => false,
			"htmlParams" => array("size" => 5, "maxlength" => 2),
		),
	);

	protected static function calcNextExec(array $arSubscribe = null)
	{
		if (!is_set($arSubscribe, "PARAMS"))
		{
			$arSubscribe = CTszhSubscribeTable::getRowById($arSubscribe["ID"]);
			if (!is_array($arSubscribe["PARAMS"]))
				$arSubscribe["PARAMS"] = array();
			self::checkParams($arSubscribe["PARAMS"], $arSubscribe["ID"], true);
		}

		$nextExec = strtotime("first day of +1 month");
		return mktime(self::EXEC_HOUR, self::EXEC_MINUTE, 0, date("n", $nextExec), $arSubscribe["PARAMS"]["DATE"], date("Y", $nextExec));
	}

	/**
	 * ������������� (��������� � ������� b_tszh_subscribe) ��������� ��������
	 * ������������� ���������� ����� ���������� ����������
	 * @param int $subscribeId Id ������ ��������
	 * @param array $arSettings ������ ��������������� �������� (code => array)
	 *
	 * @return \Bitrix\Main\Entity\UpdateResult | false
	 */
	public static function saveSettings($subscribeId, array $arSettings)
	{
		$subscribeId = intval($subscribeId);
		if ($subscribeId <= 0)
		{
			return false;
		}

		if (!is_array($arSettings) || empty($arSettings))
		{
			return false;
		}

		foreach ($arSettings as $code => $arSetting)
		{
			if ($code == "PARAMS")
			{
				foreach ($arSetting as $paramCode => $arParam)
				{
					if ($arParam["readOnly"])
					{
						if (strlen($arParam["~value"]))
						{
							$arSettings["PARAMS"][$paramCode] = $arParam["~value"];
						}
						else
						{
							unset($arSettings["PARAMS"][$paramCode]);
						}
					}
					else
					{
						$arSettings["PARAMS"][$paramCode] = $arParam["value"];
					}
				}
			}
			else
			{
				$arSettings[$code] = $arSetting["value"];
			}
		}


		$month = ($arSettings["PARAMS"]["DATE"] > intval(date("j"))) ? date("n") : date("n") + 1;

		$nextExec = mktime(self::EXEC_HOUR, self::EXEC_MINUTE, 0, $month, $arSettings["PARAMS"]["DATE"], date("Y"));

		$arSettings["NEXT_EXEC"] = new \Bitrix\Main\Type\DateTime(convertTimeStamp($nextExec, "FULL"));


		return CTszhSubscribeTable::update($subscribeId, $arSettings);
	}

	/**
	 * ���������� ��������� �������� ����������� � ������������� ����� ��������� ���������
	 *
	 * @param array $arParams ������ ���������� �������� (paramCode => value)
	 * @param int $subscribeId Id ������ ��������
	 * @param bool $useDefault ����: ������������ �������� ���������� �������� �� ���������� ����������
	 * @param bool $throwKey ����: � ���������� Main\ArgumentException ���������� ���� (key) ��������� ������ ��� ����
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Exception
	 * @return void
	 */
	public static function checkParams(array &$arParams, $subscribeId = 0, $useDefault = false, $throwKey = false)
	{
		$code = "DATE";
		$throwCode = $throwKey ? self::getSettingKey($code) : $code;
		if (is_set($arParams, $code) && strlen($arParams[$code]))
		{
			$arParams[$code] = intval($arParams[$code]);
			if ($arParams[$code] < 1 || $arParams[$code] > 31)
			{
				if ($useDefault)
					unset($arParams[$code]);
				else
					throw new Main\ArgumentException(self::getMessage("PARAM-{$code}-ERROR-RANGE", array("#PARAM#" => self::getMessage("PARAM-{$code}-NAME"))), $throwCode);
			}
		}
		elseif ($subscribeId == 0 && !$useDefault)
			throw new \Exception(Loc::getMessage("Common-ERROR-PARAM-MISSING", array("#PARAM#" => self::getMessage("PARAM-{$code}-NAME"))), $throwCode);

		if ($useDefault)
			parent::checkParams($arParams, $subscribeId, $useDefault);
	}

	/**
	 * �������-����� �������� ����������� ��������� ����������� � ������������� ����� ��������� ���������.
	 *
	 * ����������� ������������ ���� ������� ������ ������� ����������,
	 * ������� ���� �� ���� �������, ��������� email (�� ��������)
	 * � �� ��������� ��������� � ������� ����������� ������.
	 *
	 * @param int $subscribeId ID ������ ��������
	 * @param array $arSubscribeParams ������ ������ ����������� ���������� ��������
	 * @param array $arAgentParams ������ ���������� ������:
	 * 		string HEADER_FROM - �������� � ��������� From ������
	 * 		string HEADER_SENDER - �������� � ��������� Sender ������
	 * 		string METERS_URL - URL �������� ����� ��������� ��������� ������� ��������
	 * 		int TSZH_ID - ID ���
	 * 		string PERIOD_DATE - ���� (YYYY-MM-DD) �������, ���������� � ������������� ����� ��������� �� ������� ����� �����������
	 * 		string LAST_EMAIL - e-mail, �� ������� ���� ���������� ��������� ���������� ���� �������.
	 * @return string
	 */
	public static function execAgent($subscribeId, array $arSubscribeParams, array $arAgentParams)
	{
		$subscribeId = intval($subscribeId);
		$arAgentParams["TSZH_ID"] = intval($arAgentParams["TSZH_ID"]);

		global $USER;
		$bTmpUser = false;

		if (!is_object($USER))
		{
			$bTmpUser = true;
			$USER = new CUser();
		}

		// ������� ���� �����������
		$arTszh = CTszh::GetByID($arAgentParams["TSZH_ID"]);

		// ��������� ����������, �������� ��������� �������� ������ ��������� ���������
		$tsPeriodStart = MakeTimeStamp($arAgentParams["PERIOD_DATE"], "YYYY-MM-DD");
		$tsPeriodEnd = strtotime("first day of +1 month", $tsPeriodStart);
		$strPeriodStart = ConvertTimeStamp($tsPeriodStart, "FULL");
		$strPeriodEnd = ConvertTimeStamp($tsPeriodEnd, "FULL");

		$arPeriod["PERIOD_NAME"] = CTszhPeriod::Format($arAgentParams["PERIOD_DATE"]);

		// ���-�� ������������ ����� ������� �������
		$curEmailCount = 0;

		$lastEmail = $arAgentParams["LAST_EMAIL"];
		$maxEmailsCount = CTszhSubscribe::getOption("max_emails_per_hit");

		$rsAccounts = CTszhAccount::GetList(
			array("USER_EMAIL" => "ASC"),
			array(
				"TSZH_ID" => $arAgentParams["TSZH_ID"],
				"!USER_EMAIL" => false,
				"!@USER_ID" => CTszhSubscribe::getUnsubscribedUsers(self::CODE),
				">USER_EMAIL" => $lastEmail,
				"HAS_METERS" => "Y",
			),
			false,
			false,
			array("*")
		);
		$arAccounts = array();
		while ($arAccount = $rsAccounts->GetNext())
		{
			if (CTszhSubscribe::isDummyMail($arAccount["USER_EMAIL"], $arAccount["USER_SITE"]))
				continue;

			$arAccounts[$arAccount["ID"]] = $arAccount;
		}
		unset($rsAccounts);

		if (!empty($arAccounts))
		{
			/**
			 * ��������� ���, ��� ��� ���� ��������� � ������� ������
			 */
			$rsMeterValues = CTszhMeterValue::GetList(
				array(),
				array(
					"@ACCOUNT_ID" => array_keys($arAccounts),
					">=TIMESTAMP_X" => $strPeriodStart,
					"<TIMESTAMP_X" => $strPeriodEnd
				),
				false,
				false,
				array("ID", "ACCOUNT_ID")
			);
			while ($arValue = $rsMeterValues->fetch())
			{
				unset($arAccounts[$arValue["ACCOUNT_ID"]]);
			}
			unset($rsMeterValues);

			// ���������� ������ e-mail => ������� ����
			$arEmailAccounts = array();
			foreach ($arAccounts as $accountId => $arAccount)
			{
				$arEmailAccounts[$arAccount["USER_EMAIL"]] = $arAccount;
			}
			unset($arAccounts);

			CTszh::DisableTimeZone();
			foreach ($arEmailAccounts as $email => $arAccount)
			{
				if (self::getGenericEmailCount() >= $maxEmailsCount)
				{
					if ($bTmpUser)
						unset($USER);
					CTszh::EnableTimeZone();

					$arAgentParams["LAST_EMAIL"] = $lastEmail;
					return CTszhSubscribe::getExecAgentString(__CLASS__, array($subscribeId, array(), $arAgentParams));
				}

				// ������� URL ������� �� ��������
				$listUnsubscribe = true;
				CTszhSubscribe::getMailHeaders($arTszh["SITE_ID"], $from = false, $sender = false, $listUnsubscribe, $arAccount["USER_ID"], self::CODE);

				$arSendFields = array(
					"EMAIL_FROM" => $arAgentParams["HEADER_FROM"],
					"HEADER_SENDER" => $arAgentParams["HEADER_SENDER"],
					"EMAIL_TO" => $arAccount["USER_EMAIL"],
					//"NAME" => $arAccount["NAME"],
					"PERIOD_NAME" => $arPeriod["PERIOD_NAME"],
					"ORG_NAME" => $arTszh["NAME"],
					"~ORG_NAME" => $arTszh["~NAME"],
					"METERS_URL" => $arAgentParams["METERS_URL"],
					"UNSUBSCRIBE_URL" => $listUnsubscribe,
				);

				// �������� ��� �������
				if (CTszhFunctionalityController::isPortal())
				{
					// ��� ������ �������
					$entity_type = COption::GetOptionString('vdgb.portaltszh', 'entity_type');
					if ($entity_type == 'house')
					{
						// ������ ���������� �� ����
						$arHouse = \Citrus\Tszh\HouseTable::getById($arAccount['HOUSE_ID'])->fetch();
						// ���������� �� ����� � �������� �������� ���
						$arHouseSite = CSite::GetByID($arHouse['SITE_ID'])->Fetch();
						// ���������� �� ����� � �������� �������� ������������
						$arTszhSite = CSite::GetByID($arTszh['SITE_ID'])->Fetch();
						// ���� ���������� ����� ����� ���� �� ������ ����� ������������, �� ������� � ������ ������ ������
						if ($arHouseSite['SERVER_NAME'] != $arTszhSite['SERVER_NAME'])
						{
							$arSendFields['METERS_URL'] = str_replace($arTszhSite['SERVER_NAME'], $arHouseSite['SERVER_NAME'], $arSendFields['METERS_URL']);
							$arSendFields['UNSUBSCRIBE_URL'] = str_replace($arTszhSite['SERVER_NAME'], $arHouseSite['SERVER_NAME'], $arSendFields['UNSUBSCRIBE_URL']);
						}
					}
				}

				CTszhSubscribe::send(self::EVENT_TYPE, $arTszh["SITE_ID"], $arSendFields, $arTszh["ID"]);

				$curEmailCount++;
				self::incGenericEmailCount();

				$lastEmail = $arAccount["USER_EMAIL"];
			}
			CTszh::EnableTimeZone();
		}

		$arNewSubscribe = array(
			"ID" => $subscribeId,
		);
		parent::exec($arNewSubscribe, self::calcNextExec(array("ID" => $subscribeId)));

		if ($bTmpUser)
			unset($USER);

		return "";
	}

	/**
	 * ��������� ����������� ����������� � ������������� ����� ��������� ��������� ����� execAgent()
	 *
	 * @param array $arSubscribe ������ ����� ������ �������� (�� ������� b_tszh_subscribe)
	 * @param bool $nextExec false
	 * @return void
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\Db\SqlQueryException
	 */
	public static function exec(array $arSubscribe, $nextExec = false)
	{
		self::checkParams($arSubscribe["PARAMS"], $arSubscribe["ID"], true);

		if (!isset($arSubscribe["NEXT_EXEC"]))
		{
			$now = time();
			$curDay = intval(date("j", $now));
			$nextExec = false;
			if ($curDay < $arSubscribe["PARAMS"]["DATE"])
			{
				$nextExec = mktime(self::EXEC_HOUR, self::EXEC_MINUTE, 0, date("n", $now), $arSubscribe["PARAMS"]["DATE"], date("Y", $now));
			}
			elseif ($curDay > $arSubscribe["PARAMS"]["DATE"])
			{
				$nextExec = strtotime("first day of +1 month");
				$nextExec = mktime(self::EXEC_HOUR, self::EXEC_MINUTE, 0, date("n", $nextExec), $arSubscribe["PARAMS"]["DATE"], date("Y", $nextExec));
			}

			if ($nextExec > 0)
			{
				$arUpdateSubscribe = array(
					"NEXT_EXEC" => new \Bitrix\Main\Type\DateTime(convertTimeStamp($nextExec, "FULL")),
					"RUNNING" => "N"
				);
				$res = CTszhSubscribeTable::update($arSubscribe["ID"], $arUpdateSubscribe);
				return;
			}
		}

		try
		{
			// ������� ���� �����������
			$arTszh = CTszh::GetByID($arSubscribe["TSZH_ID"]);
			// ������� �������� ��������� From � Sender
			$from = $sender = true;
			CTszhSubscribe::getMailHeaders($arTszh["SITE_ID"], $from, $sender);
			// ������� URL �������� ����� ��������� ���������
			$metersUrl = true;
			CTszhPublicHelper::getPersonalUrls($arTszh, $personalUrl = false, $paymentUrl = false, $metersUrl);

			$now = time();
			$arAgentParams = array(
				"HEADER_FROM" => $from,
				"HEADER_SENDER" => $sender,
				"METERS_URL" => $metersUrl,
				"TSZH_ID" => $arSubscribe["TSZH_ID"],
				"PERIOD_DATE" => date("Y", $now) . "-" . date("m", $now) . "-01",
				"LAST_EMAIL" => "",
			);
			$agentID = CTszhSubscribe::addExecAgent(__CLASS__, array($arSubscribe["ID"], array(), $arAgentParams));
			if ($agentID <= 0)
				throw new Exception("", -1);
		}
		catch (Exception $e)
		{
			parent::exec(array("ID" => $arSubscribe["ID"]), $e->getCode() == -1 ? false : self::calcNextExec($arSubscribe));
		}
	}

	/**
	 * ���������� ������� ��������� ������ ��������. ���������� �� CTszhSubscribeTable::onBeforeUpdate()
	 *
	 * @param Entity\Event $event ������ �������
	 * @param array $arOldSubscribe ������ ����� ������������ ������ ��������
	 * @return void
	 */
	public static function onBeforeUpdate(Entity\Event $event, array $arOldSubscribe)
	{
		parent::onBeforeUpdate($event, $arOldSubscribe);

		$arParameters = $event->getParameters();
		$arFields = $event->mergeFields($arParameters["fields"]);

		if (is_set($arFields, "NEXT_EXEC")
		    || (!is_set($arFields, "PARAMS") || !is_set($arFields["PARAMS"], "DATE")) && ($arFields["ACTIVE"] != "Y" || $arOldSubscribe["ACTIVE"] == "Y"))
			return;

		if (is_set($arFields, "PARAMS") && is_set($arFields["PARAMS"], "DATE"))
		{
			self::checkParams($arFields["PARAMS"], $arOldSubscribe["ID"], true);
			$date = $arFields["PARAMS"]["DATE"];
		}
		elseif ($arFields["ACTIVE"] == "Y" && $arOldSubscribe["ACTIVE"] != "Y")
		{
			self::checkParams($arOldSubscribe["PARAMS"], $arOldSubscribe["ID"], true);
			$date = $arOldSubscribe["PARAMS"]["DATE"];
		}

		// ���� �������� ��� ����������� � ������� ������, �� ��������� ���� ���������� ������� ��������� �������,
		// ������ ������� ���� ���������� ������� (����� ����������� � self::exec())
		$nextExec = null;
		if (is_object($arOldSubscribe["LAST_EXEC"]) && ($lastExec = $arOldSubscribe["LAST_EXEC"]->getTimestamp()))
		{
			$now = time();
			if (date("n", $lastExec) == date("n", $now) && date("Y", $lastExec) == date("Y", $now))
			{
				$nextExec = strtotime("first day of +1 month", $now);
				$nextExec = mktime(self::EXEC_HOUR, self::EXEC_MINUTE, 0, date("n", $nextExec), $date, date("Y", $nextExec));
				$nextExec = new \Bitrix\Main\Type\DateTime(convertTimeStamp($nextExec, "FULL"));
			}
		}

		$eventResult = new EventResult();
		$eventResult->modifyFields(array("NEXT_EXEC" => $nextExec));
		$event->addResult($eventResult);
	}
}