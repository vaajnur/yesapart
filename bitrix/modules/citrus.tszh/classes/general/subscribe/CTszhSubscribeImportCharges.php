<?

/**
 * ����� �������� ����������� � ������������ ��������� ����������
 */
class CTszhSubscribeImportCharges extends CTszhBaseSubscribe
{
	const CODE = "ImportCharges";
	const TYPE = self::TYPE_PRODUCT;
	const INTERVAL = 28800; // 20*24*60 == 20 ����
	const INTERVAL2 = 4320; // 3*24*60 == 3 ���
	const EVENT_TYPE = "TSZH_IMPORT_CHARGES_NOTICE";
	const SORT = 8;

	protected static $arParams = array(
		"TSZH_CHECK_DATES" => array(
			"type" => "array",
			"default" => array(),
			"readOnly" => true,
		),
	);

	protected static function calcNextExec(array $arSubscribe = null)
	{
		if (!is_array($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"]) || empty($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"]))
		{
			return strtotime("+" . self::INTERVAL . " minute");
		}

		sort($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"]);

		return $arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][0];
	}

	public static function checkParams(array &$arParams, $subscribeId = 0, $useDefault = false, $throwKey = false)
	{
		if (is_set($arParams, "TSZH_CHECK_DATES"))
		{
			if (!is_array($arParams["TSZH_CHECK_DATES"]))
			{
				$arParams["TSZH_CHECK_DATES"] = array();
			}

			foreach ($arParams["TSZH_CHECK_DATES"] as $tszhId => $checkDatetime)
			{
				$arParams["TSZH_CHECK_DATES"][$tszhId] = intval($checkDatetime);
			}
		}

		if ($useDefault)
		{
			parent::checkParams($arParams, $subscribeId, $useDefault);
		}
	}

	/**
	 * ������������ �������� ����������� � ������������ ��������� ����������
	 *
	 * @param array $arSubscribe ������ ����� ������ �������� (�� ������� b_tszh_subscribe)
	 * @param bool $nextExec false
	 *
	 * @return void
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\Db\SqlQueryException
	 */
	public static function exec(array $arSubscribe, $nextExec = false)
	{
		try
		{
			self::checkParams($arSubscribe["PARAMS"], $arSubscribe["ID"], true);

			// ������� ��� ���
			$rsTszhs = CTszh::getList(
				array("ID" => "ASC"),
				array(),
				false,
				false,
				array("ID", "SITE_ID", "NAME")
			);
			$arTszhs = array();
			$arSiteFormats = array();
			while ($arTszh = $rsTszhs->getNext())
			{
				$arTszhs[$arTszh["ID"]] = $arTszh;
				$arSiteFormats[$arTszh["SITE_ID"]] = false;
			}
			if (empty($arTszhs))
			{
				$arSubscribe["PARAMS"]["TSZH_CHECK_DATES"] = array();
				throw new Exception("");
			}

			foreach ($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"] as $tszhId => $checkDatetime)
			{
				if (!is_set($arTszhs, $tszhId))
				{
					unset($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId]);
				}
			}

			$now = time();
			foreach ($arTszhs as $tszhId => $arTszh)
			{
				if (!is_set($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"], $tszhId))
				{
					$arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId] = $now;
				}
			}

			foreach ($arTszhs as $tszhId => $arTszh)
			{
				if ($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId] > $now)
				{
					unset($arTszhs[$tszhId]);
				}
			}
			if (empty($arTszhs))
			{
				throw new Exception("");
			}

			// �������� ���, �� ������� �� ������ ��������� �/�
			$arRealAccountsTszhIds = CTszh::realAccountsTszhs();
			foreach ($arTszhs as $tszhId => $arTszh)
			{
				if (!in_array($tszhId, $arRealAccountsTszhIds))
				{
					$arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId] = strtotime("+" . self::INTERVAL . " minute");
					unset($arTszhs[$tszhId]);
				}
			}
			if (empty($arTszhs))
			{
				throw new Exception("");
			}

			/*$rsSites = CSite::GetList($by="sort", $order="desc", array("LANGUAGE_ID" => array_keys($arSiteFormats)));
			while ($arSite = $rsSites->Fetch())
			{
				$arSiteFormats[$arSite["LID"]] = $arSite["FORMAT_DATETIME"];
			}
			foreach ($arTszhs as $tszhId => $arTszh)
			{
				$arTszhs[$tszhId]["_FULL_FORMAT"] = $arSiteFormats[$arTszh["SITE_ID"]];
			}*/

			// ������� � �������� ���, ������� ���������, ���������� round(self::INTERVAL / (24*60)) ��� ����� ���� �����
			$rsCheckedTszhs = CTszhAccountPeriod::getList(
				array(),
				array(
					"@PERIOD_TSZH_ID" => array_keys($arTszhs),
					">=TIMESTAMP_X" => convertTimeStamp(strtotime("-" . (round(self::INTERVAL / (24 * 60))) . " day"), "FULL"),
				),
				array("PERIOD_TSZH_ID", "MAX" => "TIMESTAMP_X")
			);
			while ($arCheckedTszh = $rsCheckedTszhs->fetch())
			{
				$tszhId = $arCheckedTszh["PERIOD_TSZH_ID"];
				$arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId] = strtotime(
					"+" . self::INTERVAL . " minute 1 second",
					makeTimeStamp($arCheckedTszh["TIMESTAMP_X"], "YYYY-MM-DD HH:MI:SS"/*$arTszhs[$tszhId]["_FULL_FORMAT"]*/)
				);
				unset($arTszhs[$tszhId]);
			}
			if (empty($arTszhs))
			{
				throw new Exception("");
			}

			// ��������� ����������� ��������
			$arAdminEmails = CTszhSubscribe::getSubscribedAdminEmails(self::CODE);
			if (empty($arAdminEmails))
			{
				throw new Exception("");
			}

			$curEmailCount = 0;
			foreach ($arTszhs as $tszhId => $arTszh)
			{
				// ������� URL � ������������ ����� ���
				CTszhPublicHelper::getOrgUrlAndSiteName($arTszh, $orgUrl, $orgSiteName);

				foreach ($arAdminEmails as $userId => $email)
				{
					// ������� �������� ��������� From, Sender � List-Unsubscribe
					$from = $sender = $listUnsubscribe = true;
					CTszhSubscribe::getMailHeaders($arTszh["SITE_ID"], $from, $sender, $listUnsubscribe, $userId, self::CODE);

					$arSendFields = array(
						"EMAIL_FROM" => $from,
						"HEADER_SENDER" => $sender,
						"EMAIL_TO" => $email,
						"ORG_NAME" => $arTszh["NAME"],
						"~ORG_NAME" => $arTszh["~NAME"],
						"ORG_SITE_NAME" => $orgSiteName,
						"ORG_URL" => $orgUrl,
						"VDGB_CONTACTS_HTML" => CTszhSubscribe::getVdgbContactsHtml(),
						"UNSUBSCRIBE_URL" => $listUnsubscribe,
					);
					CTszhSubscribe::send(self::EVENT_TYPE, $arTszh["SITE_ID"], $arSendFields, $arTszh["ID"]);

					$curEmailCount++;
					self::incGenericEmailCount();
				}

				$arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId] = strtotime("+" . self::INTERVAL2 . " minute");
			}
		}
		catch (Exception $e)
		{
		}

		$arNewSubscribe = array(
			"ID" => $arSubscribe["ID"],
			"TSZH_ID" => null,
			"PARAMS" => $arSubscribe["PARAMS"],
		);
		parent::exec($arNewSubscribe, self::calcNextExec($arSubscribe));
	}
}
