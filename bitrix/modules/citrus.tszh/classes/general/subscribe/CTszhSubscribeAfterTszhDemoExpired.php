<?

/**
 * ����� �������� ����������� ����� ��������� ����-������� ���-������
 */
class CTszhSubscribeAfterTszhDemoExpired extends CTszhBaseSubscribe implements ITszhSubscribeConditionalInstall
{
	const CODE = "AfterTszhDemoExpired";
	const TYPE = self::TYPE_PRODUCT;
	const INTERVAL = 10080; // 7*24*60 == 7 ����
	const EVENT_TYPE = "TSZH_AFTER_TSZH_DEMO_EXPIRED_NOTICE";
	const SORT = 5;

	protected static $arParams = array();

	protected static function calcNextExec(array $arSubscribe = null)
	{
		return strtotime("+" . self::INTERVAL . " minute");
	}

	public static function checkParams(array &$arParams, $subscribeId = 0, $useDefault = false, $throwKey = false)
	{
		$arParams = array();
	}

	/**
	 * ���������� ������� ������������� ��������� �������� ����������� ����� ��������� ����-������� ���-������
	 *
	 * @param array $arReturn ���������� ������ ����������� ����������
	 *
	 * @return boolean
	 */
	public static function isNeedInstall(&$arReturn = null)
	{
		$arReturn = array(
			"edition" => CTszhFunctionalityController::GetEdition($editionName),
			"moduleStatus" => null,
		);

		if ($arReturn["edition"] === false)
		{
			CTszhSubscribe::reportError("Edition module not found.");

			return true;
		}

		$arReturn["moduleStatus"] = @CModule::IncludeModuleEx($arReturn["edition"]);

		return in_array($arReturn["moduleStatus"], array(MODULE_DEMO, MODULE_DEMO_EXPIRED));
		//return true;
	}

	/**
	 * ������������ �������� ����������� ����� ��������� ����-������� ���-������
	 *
	 * @param array $arSubscribe ������ ����� ������ �������� (�� ������� b_tszh_subscribe)
	 * @param bool $nextExec false
	 *
	 * @return void
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\Db\SqlQueryException
	 */
	public static function exec(array $arSubscribe, $nextExec = false)
	{
		$needDeactivate = false;
		try
		{
			if (!self::isNeedInstall($arReturn))
			{
				throw new Exception("", -2);
			}

			if ($arReturn["moduleStatus"] !== MODULE_DEMO_EXPIRED)
			{
				throw new Exception("");
			}

			// ��������� ����������� ��������
			$arAdminEmails = CTszhSubscribe::getSubscribedAdminEmails(self::CODE);
			if (empty($arAdminEmails))
			{
				throw new Exception("");
			}

			// ������� ���� ��������� �����������
			$arTszh = CTszh::getList(
				array("ID" => "DESC"),
				array(),
				false,
				array("nTopCount" => 1),
				array("ID", "SITE_ID", "NAME")
			)->getNext();

			// ������� URL � ������������ ����� ���
			CTszhPublicHelper::getOrgUrlAndSiteName($arTszh, $orgUrl, $orgSiteName);

			$curEmailCount = 0;
			foreach ($arAdminEmails as $userId => $email)
			{
				// ������� �������� ��������� From, Sender � List-Unsubscribe
				$from = $sender = $listUnsubscribe = true;
				CTszhSubscribe::getMailHeaders($arTszh["SITE_ID"], $from, $sender, $listUnsubscribe, $userId, self::CODE);

				$arSendFields = array(
					"EMAIL_FROM" => $from,
					"HEADER_SENDER" => $sender,
					"EMAIL_TO" => $email,
					"ORG_NAME" => $arTszh["NAME"],
					"~ORG_NAME" => $arTszh["~NAME"],
					"ORG_SITE_NAME" => $orgSiteName,
					"ORG_URL" => $orgUrl,
					"VDGB_CONTACTS_HTML" => CTszhSubscribe::getVdgbContactsHtml(),
					"UNSUBSCRIBE_URL" => $listUnsubscribe,
				);
				CTszhSubscribe::send(self::EVENT_TYPE, $arTszh["SITE_ID"], $arSendFields, $arTszh["ID"]);

				$curEmailCount++;
				self::incGenericEmailCount();
			}
		}
		catch (Exception $e)
		{
			if ($e->getCode() == -2)
			{
				$needDeactivate = true;
			}
		}

		$arNewSubscribe = array(
			"ID" => $arSubscribe["ID"],
			"TSZH_ID" => null,
		);
		if ($needDeactivate)
		{
			$arNewSubscribe["ACTIVE"] = "N";
		}

		parent::exec($arNewSubscribe, self::calcNextExec($arSubscribe));
	}
}
