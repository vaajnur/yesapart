<?
use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Entity\EventResult;
use Bitrix\Main\Localization\Loc;
use Citrus\Tszh\Subscribe\CTszhSubscribeTable;

Loc::loadMessages(__FILE__);

/**
 * ����� �������� ����������� ���������
 */
class CTszhSubscribeDebtors extends CTszhBaseSubscribe implements ITszhSubscribeAgent
{
	const CODE = "Debtors";
	const TYPE = self::TYPE_OPTIONAL;
	const EVENT_TYPE = "TSZH_DEBTOR_NOTICE";
	const SORT = 100;

	const EXEC_HOUR = 1;

	protected static $arParams = array(
		"DATE" => array(
			"type" => "int",
			"default" => 15,
			"readOnly" => false,
			"htmlParams" => array("size" => 5, "maxlength" => 2),
		),
		"MIN_SUM" => array(
			"type" => "float",
			"default" => 1000,
			"readOnly" => false,
			"htmlParams" => array("size" => 5, "maxlength" => 15),
		),
	);

	protected static function calcNextExec(array $arSubscribe = null)
	{
		$nextExec = strtotime("first day of +1 month");

		return mktime(self::EXEC_HOUR, self::EXEC_MINUTE, 0, date("n", $nextExec), $arSubscribe["PARAMS"]["DATE"], date("Y", $nextExec));
	}

	/**
	 * ������������� (��������� � ������� b_tszh_subscribe) ��������� ��������
	 * ������������� ���������� ����� ���������� ����������
	 *
	 * @param int $subscribeId Id ������ ��������
	 * @param array $arSettings ������ ��������������� �������� (code => array)
	 *
	 * @return \Bitrix\Main\Entity\UpdateResult | false
	 */
	public static function saveSettings($subscribeId, array $arSettings)
	{
		$subscribeId = intval($subscribeId);
		if ($subscribeId <= 0)
		{
			return false;
		}

		if (!is_array($arSettings) || empty($arSettings))
		{
			return false;
		}

		foreach ($arSettings as $code => $arSetting)
		{
			if ($code == "PARAMS")
			{
				foreach ($arSetting as $paramCode => $arParam)
				{
					if ($arParam["readOnly"])
					{
						if (strlen($arParam["~value"]))
						{
							$arSettings["PARAMS"][$paramCode] = $arParam["~value"];
						}
						else
						{
							unset($arSettings["PARAMS"][$paramCode]);
						}
					}
					else
					{
						$arSettings["PARAMS"][$paramCode] = $arParam["value"];
					}
				}
			}
			else
			{
				$arSettings[$code] = $arSetting["value"];
			}
		}


		$month = ($arSettings["PARAMS"]["DATE"] > intval(date("j"))) ? date("n") : date("n") + 1;

		$nextExec = mktime(self::EXEC_HOUR, self::EXEC_MINUTE, 0, $month, $arSettings["PARAMS"]["DATE"], date("Y"));

		$arSettings["NEXT_EXEC"] = new \Bitrix\Main\Type\DateTime(convertTimeStamp($nextExec, "FULL"));


		return CTszhSubscribeTable::update($subscribeId, $arSettings);
	}

	/**
	 * ���������� ��������� �������� ����������� ���������
	 *
	 * @param array $arParams ������ ���������� �������� (paramCode => value)
	 * @param int $subscribeId Id ������ ��������
	 * @param bool $useDefault ����: ������������ �������� ���������� �������� �� ���������� ����������
	 * @param bool $throwKey ����: � ���������� Main\ArgumentException ���������� ���� (key) ��������� ������ ��� ����
	 *
	 * @throws \Bitrix\Main\ArgumentException
	 * @return void
	 */
	public static function checkParams(array &$arParams, $subscribeId = 0, $useDefault = false, $throwKey = false)
	{
		$code = "DATE";
		$throwCode = $throwKey ? self::getSettingKey($code) : $code;
		if (is_set($arParams, $code) && strlen($arParams[$code]))
		{
			$arParams[$code] = intval($arParams[$code]);
			if ($arParams[$code] < 1 || $arParams[$code] > 31)
			{
				if ($useDefault)
				{
					unset($arParams[$code]);
				}
				else
				{
					throw new Main\ArgumentException(self::getMessage("PARAM-{$code}-ERROR-RANGE", array("#PARAM#" => self::getMessage("PARAM-{$code}-NAME"))), $throwCode);
				}
			}
		}
		elseif ($subscribeId == 0 && !$useDefault)
		{
			throw new Main\ArgumentException(Loc::getMessage("Common-ERROR-PARAM-MISSING", array("#PARAM#" => self::getMessage("PARAM-{$code}-NAME"))), $throwCode);
		}

		$code = "MIN_SUM";
		$throwCode = $throwKey ? self::getSettingKey($code) : $code;
		if (is_set($arParams, $code) && strlen($arParams[$code]))
		{
			$arParams[$code] = floatval($arParams[$code]);
			if ($arParams[$code] <= 0)
			{
				if ($useDefault)
				{
					unset($arParams[$code]);
				}
				else
				{
					throw new Main\ArgumentException(self::getMessage("PARAM-{$code}-ERROR-MIN", array("#PARAM#" => self::getMessage("PARAM-{$code}-NAME"))), $throwCode);
				}
			}
		}
		elseif ($subscribeId == 0 && !$useDefault)
		{
			throw new Main\ArgumentException(Loc::getMessage("Common-ERROR-PARAM-MISSING", array("#PARAM#" => self::getMessage("PARAM-{$code}-NAME"))), $throwCode);
		}

		if ($useDefault)
		{
			parent::checkParams($arParams, $subscribeId, $useDefault);
		}
	}

	/**
	 * �������-����� �������� ����������� ��������� �����������
	 *
	 * @param int $subscribeId ID ������ ��������
	 * @param array $arSubscribeParams ������ ������ ����������� ���������� ��������
	 * @param array $arAgentParams ������ ���������� ������:
	 *        string HEADER_FROM - �������� � ��������� From ������
	 *        string HEADER_SENDER - �������� � ��������� Sender ������
	 *        string PERSONAL_URL - URL ������� ��������
	 *        string PAYMENT_URL - URL �������� ������
	 *        int TSZH_ID - ID ���
	 *        int LAST_ID - ID �/�, �� �������� ���� ���������� ��������� ���������� ���� �������.
	 *
	 * @return string
	 */
	public static function execAgent($subscribeId, array $arSubscribeParams, array $arAgentParams)
	{
		$subscribeId = intval($subscribeId);
		$arAgentParams["TSZH_ID"] = intval($arAgentParams["TSZH_ID"]);
		$arAgentParams["LAST_ID"] = intval($arAgentParams["LAST_ID"]);

		global $USER;
		$bTmpUser = false;

		if (!is_object($USER))
		{
			$bTmpUser = true;
			$USER = new CUser();
		}

		$arSubscribe = CTszhSubscribeTable::getRowById($subscribeId);
		if (!is_array($arSubscribe["PARAMS"]))
		{
			$arSubscribe["PARAMS"] = array();
		}
		self::checkParams($arSubscribe["PARAMS"], $subscribeId, true);

		// ������� ���� �����������
		$arTszh = CTszh::GetByID($arAgentParams["TSZH_ID"]);

		// ���-�� ������������ ����� ������� �������
		$curEmailCount = 0;

		$lastID = $arAgentParams["LAST_ID"];
		$maxEmailsCount = CTszhSubscribe::getOption("max_emails_per_hit");

		// ����� max ID ������� �������
		$rsLastPeriods = CTszhPeriod::GetList(
			array("ID" => "ASC"),
			array("TSZH_ID" => $arAgentParams["TSZH_ID"]),
			array("DATE", "MAX" => "ID"),
			false
		);
		$arLastPeriodIds = array();
		while ($arPeriod = $rsLastPeriods->fetch())
		{
			$arDate = explode("-", $arPeriod["DATE"]);
			if (!is_array($arDate) || count($arDate) != 3)
			{
				continue;
			}

			$periodKey = "{$arDate["0"]}-{$arDate["1"]}";
			$pLastPeriodId =& $arLastPeriodIds[$periodKey];
			if ($pLastPeriodId < $arPeriod["ID"])
			{
				$pLastPeriodId = $arPeriod["ID"];
			}
		}
		unset($rsLastPeriods);

		// ������� ��������� id �������� ��� ���� �������� ����������
		// todo ����������, ���������� ��� �� ���� ������������ ����� ������� ����
		global $DB;
		$err_mess = '';
		/*$strSql_tszh = "SELECT * FROM b_tszh";
		$res_id_tszh = $DB->Query($strSql_tszh, false, $err_mess . __LINE__);

		while($row = $res_id_tszh->Fetch()){
				$id_tszh[] = $row['ID'];
		}

//		$id_period = array();

		for($i=0;$i<count($id_tszh);$i++)
		{
			$id_tszh_this = $id_tszh[$i];
			$strSql_id_period = "SELECT * FROM b_tszh_period WHERE TSZH_ID=".$id_tszh_this." ORDER BY DATE DESC LIMIT 1";
			$res_id_period = $DB->Query($strSql_id_period, false, $err_mess . __LINE__);
			while($row = $res_id_period->Fetch()){
					$id_period[$i] = $row['ID'];
			}
		};

		if(count($id_period)==1) $id_period = $id_period[0];*/ // ���� ������ ����� ����, ��, ����� �� ���� �������, ������� ������ �����

		$id_tszh = $arTszh['ID'];
		$strSql_id_period = "SELECT * FROM b_tszh_period WHERE TSZH_ID=" . $id_tszh . " ORDER BY DATE DESC LIMIT 1";
		$res_id_period = $DB->Query($strSql_id_period, false, $err_mess . __LINE__);
		while ($row = $res_id_period->Fetch())
		{
			$id_period = $row['ID'];
		}

		if (!empty($arLastPeriodIds))
		{
			$filter = array(
				"PERIOD_ID" => $id_period,
				">DEBT_END" => $arSubscribe["PARAMS"]["MIN_SUM"],
				"!USER_EMAIL" => false,
				"!@USER_ID" => CTszhSubscribe::getUnsubscribedUsers(self::CODE),
				">ACCOUNT_ID" => $lastID,
			);

			$rsAccountPeriods = CTszhAccountPeriod::GetTotalDebt($filter, "");

			/*$rsAccountPeriods = CTszhAccountPeriod::GetList(
				array("ACCOUNT_ID" => "ASC"),
				array(
					"@PERIOD_ID" => $id_period,
					">=DEBT_END" => $arSubscribe["PARAMS"]["MIN_SUM"],
					"!USER_EMAIL" => false,
					"!@USER_ID" => CTszhSubscribe::getUnsubscribedUsers(self::CODE),
					">ACCOUNT_ID" => $lastID
				),
				false,
				false,
				array("*", "UF_*")
			);*/

			$arAccountPeriods = array();
			while ($arAccountPeriod = $rsAccountPeriods->getNext())
			{
				if (CTszhSubscribe::isDummyMail($arAccountPeriod["USER_EMAIL"], $arAccountPeriod["USER_SITE"]))
				{
					continue;
				}

				$periodKey = array_search($arAccountPeriod["PERIOD_ID"], $arLastPeriodIds);
				if ($periodKey === false)
				{
					continue;
				}

				$accountId = $arAccountPeriod["ACCOUNT_ID"];
				if ($arAccountPeriods[$accountId]["_PERIOD_KEY"] < $periodKey)
				{
					$arAccountPeriods[$accountId] = $arAccountPeriod;
					$arAccountPeriods[$accountId]["_PERIOD_KEY"] = $periodKey;
				}
			}
			unset($rsAccountPeriods);

			if (!empty($arAccountPeriods))
			{
				$accountPeriodIds = array();
				$arAccountIds = array();
				foreach ($arAccountPeriods as $accountId => $arAccountPeriod)
				{
					$accountPeriodIds[$arAccountPeriod["ID"]] = true;
					$arAccountIds[] = $accountId;
				}
				$accountPeriodIds = array_keys($accountPeriodIds);

				$rsCharges = CTszhCharge::GetList(
					Array(),
					Array(
						"@ACCOUNT_PERIOD_ID" => $accountPeriodIds,
						"COMPONENT" => "N",
						"DEBT_ONLY" => "N",
					),
					array("PERIOD_ID", "ACCOUNT_ID", "SUM" => "SUMM"),
					false
				);
				$arCharges = array();
				while ($arCharge = $rsCharges->GetNext())
				{
					$arCharges[$arCharge["PERIOD_ID"]][$arCharge["ACCOUNT_ID"]] = $arCharge["SUMM"];
				}
				unset($rsCharges);

				foreach ($arAccountPeriods as $accountId => $arAccountPeriod)
				{
					// �������� ������ ������� ��������� ������� ������
					CTszhAccountHistory::CorrectFromHistory($arAccountPeriod, $arAccountPeriod['ACCOUNT_ID'], ConvertTimeStamp(MakeTimeStamp($arAccountPeriod['PERIOD_DATE'], 'YYYY-MM-DD')));

					//$debt = floatval($arAccountPeriod["DEBT_END"]) - floatval($arCharges[$arAccountPeriod["PERIOD_ID"]][$accountId]); // ����� ������������� � ������ �� ����������
					$debt = floatval($arAccountPeriod["DEBT_END"]);
					if ($debt < $arSubscribe["PARAMS"]["MIN_SUM"])
					{
						$lastID = $accountId;
						continue;
					}

					if (self::getGenericEmailCount() >= $maxEmailsCount)
					{
						if ($bTmpUser)
						{
							unset($USER);
						}
						$arAgentParams["LAST_ID"] = $lastID;

						return CTszhSubscribe::getExecAgentString(__CLASS__, array($subscribeId, $arSubscribeParams, $arAgentParams));
					}

					//$arAccountPeriod["DEBT_BEG"] = CTszhPublicHelper::FormatCurrency($arAccountPeriod["DEBT_BEG"], false);
					//$arAccountPeriod["DEBT_END"] = CTszhPublicHelper::FormatCurrency($arAccountPeriod["DEBT_END"], false);
					$arAccountPeriod["_DEBT_END_WITHOUT_CHARGES"] = CTszhPublicHelper::FormatCurrency($debt, false);

					// ������� URL ������� �� ��������
					$listUnsubscribe = true;
					CTszhSubscribe::getMailHeaders($arTszh["SITE_ID"], $from = false, $sender = false, $listUnsubscribe, $arAccountPeriod["USER_ID"], self::CODE);

					$arSendFields = array(
						"EMAIL_FROM" => $arAgentParams["HEADER_FROM"],
						"HEADER_SENDER" => $arAgentParams["HEADER_SENDER"],
						"EMAIL_TO" => $arAccountPeriod["USER_EMAIL"],
						"ACCOUNT_NAME" => $arAccountPeriod["ACCOUNT_NAME"],
						"DEBT_END" => $arAccountPeriod["_DEBT_END_WITHOUT_CHARGES"],
						"ORG_NAME" => $arTszh["NAME"],
						"~ORG_NAME" => $arTszh["~NAME"],
						"PERSONAL_URL" => $arAgentParams["PERSONAL_URL"],
						"PAYMENT_URL" => $arAgentParams["PAYMENT_URL"],
						"UNSUBSCRIBE_URL" => $listUnsubscribe,
					);

					// �������� ��� �������
					if (CTszhFunctionalityController::isPortal())
					{
						// ��� ������ �������
						$entity_type = COption::GetOptionString('vdgb.portaltszh', 'entity_type');
						if ($entity_type == 'house')
						{
							// ������ ������� ����
							$arAccount = CTszhAccount::GetByID($arAccountPeriod["ACCOUNT_ID"]);
							// ������ ���������� �� ����
							$arHouse = \Citrus\Tszh\HouseTable::getById($arAccount['HOUSE_ID'])->fetch();
							// ���������� �� ����� � �������� �������� ���
							$arHouseSite = CSite::GetByID($arHouse['SITE_ID'])->Fetch();
							// ���������� �� ����� � �������� �������� ������������
							$arTszhSite = CSite::GetByID($arTszh['SITE_ID'])->Fetch();
							// ���� ���������� ����� ����� ���� �� ������ ����� ������������, �� ������� � ������ ������ ������
							if ($arHouseSite['SERVER_NAME'] != $arTszhSite['SERVER_NAME'])
							{
								$arSendFields['PERSONAL_URL'] = str_replace($arTszhSite['SERVER_NAME'], $arHouseSite['SERVER_NAME'], $arSendFields['PERSONAL_URL']);
								$arSendFields['PAYMENT_URL'] = str_replace($arTszhSite['SERVER_NAME'], $arHouseSite['SERVER_NAME'], $arSendFields['PAYMENT_URL']);
								$arSendFields['UNSUBSCRIBE_URL'] = str_replace($arTszhSite['SERVER_NAME'], $arHouseSite['SERVER_NAME'], $arSendFields['UNSUBSCRIBE_URL']);
							}
						}
					}

					CTszhSubscribe::send(self::EVENT_TYPE, $arTszh["SITE_ID"], $arSendFields, $arTszh["ID"]);

					$curEmailCount++;
					self::incGenericEmailCount();

					$lastID = $arAccountPeriod["ACCOUNT_ID"];
				}
			}
		}

		$arNewSubscribe = array(
			"ID" => $subscribeId,
		);
		parent::exec($arNewSubscribe, self::calcNextExec($arSubscribe));

		if ($bTmpUser)
		{
			unset($USER);
		}

		return "";
	}

	/**
	 * ��������� ����������� ����� execAgent() ����������� ���������
	 *
	 * @param array $arSubscribe ������ ����� ������ �������� (�� ������� b_tszh_subscribe)
	 * @param bool $nextExec false
	 *
	 * @return void
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\Db\SqlQueryException
	 */
	public static function exec(array $arSubscribe, $nextExec = false)
	{
		self::checkParams($arSubscribe["PARAMS"], $arSubscribe["ID"], true);

		if (!isset($arSubscribe["NEXT_EXEC"]))
		{
			$now = time();
			$curDay = intval(date("j", $now));
			$nextExec = false;
			if ($curDay < $arSubscribe["PARAMS"]["DATE"])
			{
				$nextExec = mktime(self::EXEC_HOUR, self::EXEC_MINUTE, 0, date("n", $now), $arSubscribe["PARAMS"]["DATE"], date("Y", $now));
			}
			elseif ($curDay > $arSubscribe["PARAMS"]["DATE"])
			{
				$nextExec = strtotime("first day of +1 month");
				$nextExec = mktime(self::EXEC_HOUR, self::EXEC_MINUTE, 0, date("n", $nextExec), $arSubscribe["PARAMS"]["DATE"], date("Y", $nextExec));
			}

			if ($nextExec > 0)
			{
				$arUpdateSubscribe = array(
					"NEXT_EXEC" => new \Bitrix\Main\Type\DateTime(convertTimeStamp($nextExec, "FULL")),
					"RUNNING" => "N",
				);
				$res = CTszhSubscribeTable::update($arSubscribe["ID"], $arUpdateSubscribe);

				return;
			}
		}

		try
		{
			// ������� ���� �����������
			$arTszh = CTszh::GetByID($arSubscribe["TSZH_ID"]);
			// ������� �������� ��������� From � Sender
			$from = $sender = true;
			CTszhSubscribe::getMailHeaders($arTszh["SITE_ID"], $from, $sender);
			// ������� URL ������� �������� � URL �������� ������
			$personalUrl = $paymentUrl = true;
			CTszhPublicHelper::getPersonalUrls($arTszh, $personalUrl, $paymentUrl);

			$arAgentParams = array(
				"HEADER_FROM" => $from,
				"HEADER_SENDER" => $sender,
				"PERSONAL_URL" => $personalUrl,
				"PAYMENT_URL" => $paymentUrl,
				"TSZH_ID" => $arSubscribe["TSZH_ID"],
				"LAST_ID" => 0,
			);
			$agentID = CTszhSubscribe::addExecAgent(__CLASS__, array($arSubscribe["ID"], array(), $arAgentParams));
			if ($agentID <= 0)
			{
				throw new Exception("", -1);
			}
		}
		catch (Exception $e)
		{
			parent::exec(array("ID" => $arSubscribe["ID"]), $e->getCode() == -1 ? false : self::calcNextExec($arSubscribe));
		}
	}

	/**
	 * ���������� ������� ��������� ������ ��������. ���������� �� CTszhSubscribeTable::onBeforeUpdate()
	 *
	 * @param Entity\Event $event ������ �������
	 * @param array $arOldSubscribe ������ ����� ������������ ������ ��������
	 *
	 * @return void
	 */
	public static function onBeforeUpdate(Entity\Event $event, array $arOldSubscribe)
	{
		parent::onBeforeUpdate($event, $arOldSubscribe);

		$arParameters = $event->getParameters();
		$arFields = $event->mergeFields($arParameters["fields"]);

		if (is_set($arFields, "NEXT_EXEC")
		    || (!is_set($arFields, "PARAMS") || !is_set($arFields["PARAMS"], "DATE")) && ($arFields["ACTIVE"] != "Y" || $arOldSubscribe["ACTIVE"] == "Y"))
		{
			return;
		}

		if (is_set($arFields, "PARAMS") && is_set($arFields["PARAMS"], "DATE"))
		{
			self::checkParams($arFields["PARAMS"], $arOldSubscribe["ID"], true);
			$date = $arFields["PARAMS"]["DATE"];
		}
		elseif ($arFields["ACTIVE"] == "Y" && $arOldSubscribe["ACTIVE"] != "Y")
		{
			self::checkParams($arOldSubscribe["PARAMS"], $arOldSubscribe["ID"], true);
			$date = $arOldSubscribe["PARAMS"]["DATE"];
		}

		// ���� �������� ��� ����������� � ������� ������, �� ��������� ���� ���������� ������� ��������� �������,
		// ������ ������� ���� ���������� ������� (����� ����������� � self::exec())
		$nextExec = null;
		if (is_object($arOldSubscribe["LAST_EXEC"]) && ($lastExec = $arOldSubscribe["LAST_EXEC"]->getTimestamp()))
		{
			$now = time();
			if (date("n", $lastExec) == date("n", $now) && date("Y", $lastExec) == date("Y", $now))
			{
				$nextExec = strtotime("first day of +1 month", $now);
				$nextExec = mktime(self::EXEC_HOUR, self::EXEC_MINUTE, 0, date("n", $nextExec), $date, date("Y", $nextExec));
				$nextExec = new \Bitrix\Main\Type\DateTime(convertTimeStamp($nextExec, "FULL"));
			}
		}

		$eventResult = new EventResult();
		$eventResult->modifyFields(array("NEXT_EXEC" => $nextExec));
		$event->addResult($eventResult);
	}
}