<?

/**
 * ����� �������� ����������� � ������������ ��������� ������� �����
 */
class CTszhSubscribeImportAccounts extends CTszhBaseSubscribe
{
	const CODE = "ImportAccounts";
	const TYPE = self::TYPE_PRODUCT;
	const INTERVAL = 43200; // 30*24*60 == 30 ����
	const EVENT_TYPE = "TSZH_IMPORT_ACCOUNTS_NOTICE";
	const SORT = 7;

	protected static $arParams = array();

	protected static function calcNextExec(array $arSubscribe = null)
	{
		return strtotime("+" . self::INTERVAL . " minute");
	}

	public static function checkParams(array &$arParams, $subscribeId = 0, $useDefault = false, $throwKey = false)
	{
		$arParams = array();
	}

	/**
	 * ������������ �������� ����������� � ������������ ��������� ������� �����
	 *
	 * @param array $arSubscribe ������ ����� ������ �������� (�� ������� b_tszh_subscribe)
	 * @param bool $nextExec false
	 *
	 * @return void
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\Db\SqlQueryException
	 */
	public static function exec(array $arSubscribe, $nextExec = false)
	{
		try
		{
			// ��������� ����������� ��������
			$arAdminEmails = CTszhSubscribe::getSubscribedAdminEmails(self::CODE);
			if (empty($arAdminEmails))
			{
				throw new Exception("");
			}

			// ������� ��� ���
			$rsTszhs = CTszh::getList(
				array("ID" => "ASC"),
				array(),
				false,
				false,
				array("ID", "SITE_ID", "NAME")
			);
			$arTszhs = array();
			while ($arTszh = $rsTszhs->getNext())
			{
				$arTszhs[$arTszh["ID"]] = $arTszh;
			}
			if (empty($arTszhs))
			{
				throw new Exception("");
			}

			// �������� ���, ������� ���� �� ���� �������� �/�
			$arRealAccountsTszhIds = CTszh::realAccountsTszhs();
			foreach ($arRealAccountsTszhIds as $tszhId)
			{
				unset($arTszhs[$tszhId]);
			}
			if (empty($arTszhs))
			{
				throw new Exception("");
			}

			$curEmailCount = 0;
			foreach ($arTszhs as $tszhId => $arTszh)
			{
				// ������� URL � ������������ ����� ���
				CTszhPublicHelper::getOrgUrlAndSiteName($arTszh, $orgUrl, $orgSiteName);

				foreach ($arAdminEmails as $userId => $email)
				{
					// ������� �������� ��������� From, Sender � List-Unsubscribe
					$from = $sender = $listUnsubscribe = true;
					CTszhSubscribe::getMailHeaders($arTszh["SITE_ID"], $from, $sender, $listUnsubscribe, $userId, self::CODE);

					$arSendFields = array(
						"EMAIL_FROM" => $from,
						"HEADER_SENDER" => $sender,
						"EMAIL_TO" => $email,
						"ORG_NAME" => $arTszh["NAME"],
						"~ORG_NAME" => $arTszh["~NAME"],
						"ORG_SITE_NAME" => $orgSiteName,
						"ORG_URL" => $orgUrl,
						"VDGB_CONTACTS_HTML" => CTszhSubscribe::getVdgbContactsHtml(),
						"UNSUBSCRIBE_URL" => $listUnsubscribe,
					);
					CTszhSubscribe::send(self::EVENT_TYPE, $arTszh["SITE_ID"], $arSendFields, $arTszh["ID"]);

					$curEmailCount++;
					self::incGenericEmailCount();
				}
			}
		}
		catch (Exception $e)
		{
		}

		parent::exec(array("ID" => $arSubscribe["ID"], "TSZH_ID" => null), self::calcNextExec($arSubscribe));
	}
}