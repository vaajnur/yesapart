<?php

use Citrus\Tszh\HouseTable;

if (!tszhCheckMinEdition('start'))
{
	return false;
}

IncludeModuleLangFile(__FILE__);

$GLOBALS['TSZH_PERIOD'] = Array();
$GLOBALS['TSZH'] = Array();

/**
 * ����� ��� ������ � ��������� ����������
 * ����� �������� ������ ���������������� ������
 */
class CTszh
{
	const CACHE_TAG_NAME = 'citrus:tszh';

	private static $demoAccounts = array(
		"TSGWu7Cy8O",
		"TSG1Ky8Wu6",
		"TSGM3uL1uH",
		"TSGe3Gv8Sp",
		"TSG2Om3Pw7",
		"TSGuO3iH1l",
		"TSG3Ic9Zr9",
		"TSG4wF4eT9",
		"TSG3Hq6Px7",
		"TSGtC1dS4g",
		"TSGWv7Rl5T",
		"TSG5Yx1Lg4",
		"TSGE9rZ0fQ",
		"TSGfS6yE7e",
		"TSGs1Yx1Ng",
		"TSGgH3hK3q",
		"TSG8yG1vV8",
		"TSG8Xq6Ao7",
		"TSGp6Zz8Xp",
		"TSGaE9iD0u",
		"TSG2Ea7Le8",
		"TSG9lM6hX5",
		"TSG1tW9lS8",
		"TSGsT2wL6w",
		"TSGWc0St5E",
		"TSGD4sG3uR",
		"TSGz2Lm8Oh",
		"TSGhP6kW1p",
		"TSGS6tJ8eN",
		"TSG2Va9Dl0",
		"TSG7Gv4Ej6",
		"TSGSb1Yx9L",
		"TSG7Fa4Lb4",
		"TSGiU0zK6m",
		"TSGU9rC3nR",
		"TSGUz8Tz2N"
	);

	public static $validationErrors = Array();

	// �������� ������ � ��������� ��������, ���� ������������ ���������������� ���� �������� ������; false, ���� �� ������������
	const USER_FIELD_ENTITY = 'TSZH';

	/**
	 * ��������� ������������ ��������� �����
	 *
	 * @param array $arFields
	 * @param int $ID
	 * @param string $strOperation
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function CheckFields(&$arFields, $ID = 0, $strOperation = 'ADD')
	{
		global $APPLICATION;

		$arErrors = Array();

		unset($arFields['ID']);
		unset($arFields['TIMESTAMP_X']);

		if (array_key_exists('SITE_ID', $arFields))
		{
			if (!CSite::GetByID($arFields["SITE_ID"])->Fetch())
			{
				$arErrors[] = Array('id' => 'TSZH_ERROR_WRONG_SITE_ID', 'text' => GetMessage('TSZH_ERROR_WRONG_SITE_ID'));
			}
		}
		elseif ($strOperation == 'ADD')
		{
			$arErrors[] = Array('id' => 'TSZH_ERROR_NO_SITE_ID', 'text' => GetMessage('TSZH_ERROR_NO_SITE_ID'));
		}

		if (array_key_exists("NAME", $arFields) || $strOperation == "ADD")
		{
			$arFields["NAME"] = trim($arFields["NAME"]);
			if (strlen($arFields["NAME"]) <= 0)
			{
				$arErrors[] = Array('id' => 'TSZH_ERROR_NO_NAME', 'text' => GetMessage('TSZH_ERROR_NO_NAME'));
			}
		}

		if (array_key_exists("SUBSCRIBE_DOLG_DATE", $arFields))
		{
			if (strlen($arFields["SUBSCRIBE_DOLG_DATE"]) > 0 || $arFields["SUBSCRIBE_DOLG_ACTIVE"] == "Y")
			{
				$arFields["SUBSCRIBE_DOLG_DATE"] = intval($arFields["SUBSCRIBE_DOLG_DATE"]);
				if ($arFields["SUBSCRIBE_DOLG_DATE"] <= 0 || $arFields["SUBSCRIBE_DOLG_DATE"] > 31)
				{
					$arErrors[] = Array('id' => 'TSZH_ERROR_WRONG_SUBSCRIBE_DOLG_DATE', 'text' => GetMessage('TSZH_ERROR_WRONG_SUBSCRIBE_DOLG_DATE'));
				}
			}
			else
			{
				$arFields["SUBSCRIBE_DOLG_DATE"] = false;
			}
		}

		if (array_key_exists("SUBSCRIBE_DOLG_MIN_SUM", $arFields))
		{
			if (strlen($arFields["SUBSCRIBE_DOLG_MIN_SUM"]) <= 0 && $arFields["SUBSCRIBE_DOLG_ACTIVE"] != "Y")
			{
				$arFields["SUBSCRIBE_DOLG_MIN_SUM"] = false;
			}
		}

		if (array_key_exists("SUBSCRIBE_METERS_DATE", $arFields))
		{
			if (strlen($arFields["SUBSCRIBE_METERS_DATE"]) > 0 || $arFields["SUBSCRIBE_METERS_ACTIVE"] == "Y")
			{
				$arFields["SUBSCRIBE_METERS_DATE"] = intval($arFields["SUBSCRIBE_METERS_DATE"]);
				if ($arFields["SUBSCRIBE_METERS_DATE"] <= 0 || $arFields["SUBSCRIBE_METERS_DATE"] > 31)
				{
					$arErrors[] = Array(
						'id' => 'TSZH_ERROR_WRONG_SUBSCRIBE_METERS_DATE',
						'text' => GetMessage('TSZH_ERROR_WRONG_SUBSCRIBE_METERS_DATE')
					);
				}
			}
			else
			{
				$arFields["SUBSCRIBE_METERS_DATE"] = false;
			}
		}

		if (array_key_exists("METER_VALUES_START_DATE", $arFields))
		{
			if (strlen($arFields["METER_VALUES_START_DATE"]) > 0)
			{
				$arFields["METER_VALUES_START_DATE"] = intval($arFields["METER_VALUES_START_DATE"]);
				if ($arFields["METER_VALUES_START_DATE"] <= 0 || $arFields["METER_VALUES_START_DATE"] > 31)
				{
					$arErrors[] = Array(
						'id' => 'TSZH_ERROR_WRONG_METER_VALUES_START_DATE',
						'text' => GetMessage('TSZH_ERROR_WRONG_METER_VALUES_START_DATE')
					);
				}
			}
			else
			{
				$arFields["METER_VALUES_START_DATE"] = false;
			}
		}

		if (array_key_exists("METER_VALUES_END_DATE", $arFields))
		{
			if (strlen($arFields["METER_VALUES_END_DATE"]) > 0)
			{
				$arFields["METER_VALUES_END_DATE"] = intval($arFields["METER_VALUES_END_DATE"]);
				if ($arFields["METER_VALUES_END_DATE"] <= 0 || $arFields["METER_VALUES_END_DATE"] > 31)
				{
					$arErrors[] = Array(
						'id' => 'TSZH_ERROR_WRONG_METER_VALUES_END_DATE',
						'text' => GetMessage('TSZH_ERROR_WRONG_METER_VALUES_END_DATE')
					);
				}
			}
			else
			{
				$arFields["METER_VALUES_END_DATE"] = false;
			}
		}

		if (is_set($arFields, "EMAIL") && !empty($arFields["EMAIL"]) && !self::checkEmail($arFields["EMAIL"]))
		{
			$arErrors[] = Array('id' => 'TSZH_ERROR_WRONG_EMAIL', 'text' => GetMessage('TSZH_ERROR_WRONG_EMAIL'));
		}

		$checkMonetaFields = is_set($arFields, "MONETA_ENABLED") && $arFields["MONETA_ENABLED"] == "Y";
		$checkEmail = $checkMonetaFields || (is_set($arFields, "MONETA_EMAIL") && strlen($arFields["MONETA_EMAIL"]));
		if ($checkEmail && !self::checkEmail($arFields["MONETA_EMAIL"]))
		{
			$arErrors[] = Array('id' => 'CITRUS_TSZH_ERROR_INCORRECT_MONETA_EMAIL', 'text' => GetMessage('CITRUS_TSZH_ERROR_INCORRECT_MONETA_EMAIL'));
		}

		if (array_key_exists("OFFICE_HOURS", $arFields))
		{
			CBitrixComponent::includeComponentClass("citrus:tszh.office_hours.edit");
			$arOfficeHoursErrors = array();
			if (CCitrusTszhOfficeHoursEdit::checkValues($arFields["OFFICE_HOURS"], $arOfficeHoursErrors))
			{
				if (empty($arFields["OFFICE_HOURS"]))
				{
					$arFields["OFFICE_HOURS"] = false;
				}
				else
				{
					$arFields["OFFICE_HOURS"] = serialize($arFields["OFFICE_HOURS"]);
				}
			}
			else
			{
				$arErrors = array_merge($arErrors, $arOfficeHoursErrors);
			}
		}

		if (CModule::IncludeModule("citrus.tszhpayment"))
		{
			if (is_set($arFields, "MONETA_ENABLED"))
			{
				$arFields["MONETA_ENABLED"] = $arFields["MONETA_ENABLED"] == "Y" ? "Y" : "N";
			}
			if (is_set($arFields, "MONETA_OFFER"))
			{
				$arFields["MONETA_OFFER"] = $arFields["MONETA_OFFER"] == "Y" ? "Y" : "N";
				// ������ �� ������ ������� �������� ������ �� ������.��
				/*if ($arFields["MONETA_OFFER"] == "N" && $ID > 0)
				{
					$prevValues = self::GetByID($ID);
					if ($prevValues["MONETA_OFFER"] == "Y")
					{
						$arFields["MONETA_OFFER"] = "Y";
					}
				}*/
			}
		}

		if (!empty($arErrors))
		{
			$e = new CAdminException($arErrors);
			$APPLICATION->ThrowException($e);

			return false;
		}

		global $USER_FIELD_MANAGER;
		if (self::USER_FIELD_ENTITY && !$USER_FIELD_MANAGER->CheckFields(self::USER_FIELD_ENTITY, $ID, $arFields))
		{
			return false;
		}

		return true;
	}

	/**
	 * ��������� ����� ������ ����������
	 *
	 * @param array $arFields
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Add($arFields)
	{
		global $DB;

		if (!self::CheckFields($arFields))
		{
			return false;
		}

		$ID = $DB->Add("b_tszh", $arFields);
		if ($ID > 0)
		{
			global $USER_FIELD_MANAGER;
			if (self::USER_FIELD_ENTITY && self::hasUserFields($arFields))
			{
				$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
			}

			if (defined("BX_COMP_MANAGED_CACHE"))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->ClearByTag(self::CACHE_TAG_NAME);
			}

			self::postValidate($ID);

			// ������� ��������
			CTszhSubscribe::onAfterTszhAdd(array_merge($arFields, array("ID" => $ID)));

			$db_events = GetModuleEvents("citrus.tszh", "OnAfterTszhAdd");
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($arEvent = $db_events->Fetch())
			{
				ExecuteModuleEventEx($arEvent, array($ID, &$arFields));
			}
		}

		return $ID;
	}

	/**
	 * ��������� ���� ������� ����������
	 *
	 * ��� ������������� ������ �� ������ ����� �������� ����� $APPLICATION->GetException()
	 *
	 * @param int $ID ID ������� ����������
	 * @param array $arFields ����� ����� �����
	 *
	 * @return bool ��������� ��������
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Update($ID, $arFields)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (!self::CheckFields($arFields, $ID, 'UPDATE'))
		{
			return false;
		}

		$strUpdate = $DB->PrepareUpdate("b_tszh", $arFields);
		$strSql = "UPDATE b_tszh SET {$strUpdate} WHERE ID={$ID}";
		$bSuccess = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		if ($bSuccess)
		{
			if (self::USER_FIELD_ENTITY && self::hasUserFields($arFields))
			{
				global $USER_FIELD_MANAGER;
				$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
			}

			if (defined("BX_COMP_MANAGED_CACHE"))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->ClearByTag(self::CACHE_TAG_NAME);
			}
			if (is_set($GLOBALS["TSZH"], $ID))
			{
				unset($GLOBALS["TSZH"][$ID]);
			}

			self::postValidate($ID);

			$db_events = GetModuleEvents("citrus.tszh", "OnAfterTszhUpdate");
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($arEvent = $db_events->Fetch())
			{
				ExecuteModuleEventEx($arEvent, array($ID, &$arFields));
			}
		}

		return true;
	}

	/**
	 * ������� ������ ���������� � ��������� ID, � ����� ��� ��������� � ��� ������
	 *
	 * @param int $ID
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Delete($ID)
	{
		global $DB;

		$ID = intval($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		$DB->StartTransaction();

		$rsAccounts = CTszhAccount::GetList(Array(), Array("TSZH_ID" => $ID), false, false, Array("ID"));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arAccount = $rsAccounts->Fetch())
		{
			CTszhAccount::Delete($arAccount["ID"]);
		}

		$rsPeriods = CTszhPeriod::GetList(Array(), Array("TSZH_ID" => $ID), false, false, Array("ID"));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arPeriod = $rsPeriods->Fetch())
		{
			CTszhPeriod::Delete($arPeriod["ID"]);
		}

		$rsServices = CTszhService::GetList(Array(), Array("TSZH_ID" => $ID), false, false, Array("ID"));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arService = $rsServices->Fetch())
		{
			CTszhService::Delete($arService["ID"]);
		}

		$rsContractors = CTszhContractor::GetList(Array(), Array("TSZH_ID" => $ID), false, false, Array("ID"));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arContractor = $rsContractors->Fetch())
		{
			CTszhContractor::Delete($arContractor["ID"]);
		}

		$dbHouses = HouseTable::GetList(array(
			"filter" => array("=TSZH_ID" => $ID),
			"select" => array("ID")
		));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($house = $dbHouses->fetch())
		{
			HouseTable::delete($house["ID"]);
		}

		$strSql = "DELETE FROM b_tszh WHERE ID=" . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		if (self::USER_FIELD_ENTITY && $z)
		{
			global $USER_FIELD_MANAGER;
			$USER_FIELD_MANAGER->Delete(self::USER_FIELD_ENTITY, $ID);
		}

		CAdminNotify::DeleteByTag("citrus.tszh.validation." . $ID);

		$DB->Commit();

		if (defined("BX_COMP_MANAGED_CACHE"))
		{
			global $CACHE_MANAGER;
			$CACHE_MANAGER->ClearByTag(self::CACHE_TAG_NAME);
		}

		// ������ ��������
		CTszhSubscribe::onAfterTszhDelete($ID);

		$db_events = GetModuleEvents("citrus.tszh", "OnAfterTszhDelete");
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arEvent = $db_events->Fetch())
		{
			ExecuteModuleEventEx($arEvent, array($ID));
		}

		return true;
	}

	/**
	 * ���������� ���� ������� ���������� � ��������� ID
	 *
	 * @param int $ID ID ������� ����������
	 *
	 * @return bool ������������� ������ � ������ ���� false, ���� ������ ���������� �� ������
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function GetByID($ID)
	{
		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (isset($GLOBALS["TSZH"][$ID]) && is_array($GLOBALS["TSZH"][$ID]) && is_set($GLOBALS["TSZH"][$ID], "ID"))
		{
			if (defined("BX_COMP_MANAGED_CACHE"))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->RegisterTag(self::CACHE_TAG_NAME);
			}

			return $GLOBALS["TSZH"][$ID];
		}
		else
		{
			$rsTszh = self::GetList(Array(), Array("ID" => $ID), false, Array('nTopCount' => 1), Array("*", "UF_*"));
			/** @noinspection PhpAssignmentInConditionInspection */
			if ($arTszh = $rsTszh->getNext())
			{
				$GLOBALS["TSZH"][$ID] = $arTszh;

				return $arTszh;
			}
		}

		return false;
	}

	/**
	 * ���������� ������ ���������� ��� ���������� �����
	 *
	 * @param string $siteID
	 *
	 * @return array|bool ID �����, ���� �� ������ � �������
	 * @deprecated
	 */
	public static function GetBySite($siteID = SITE_ID)
	{
		$siteID = htmlspecialcharsbx($siteID);
		if (strlen($siteID) <= 0)
		{
			return false;
		}

		$rsTszh = self::GetList(Array(), Array("SITE_ID" => $siteID), false, Array('nTopCount' => 1));
		/** @noinspection PhpAssignmentInConditionInspection */
		if ($arTszh = $rsTszh->Fetch())
		{
			return $arTszh;
		}

		return false;
	}

	/**
	 * @param array $arOrder
	 * @param array $arFilter
	 * @param bool|array $arGroupBy
	 * @param bool|array $arNavStartParams
	 * @param array $arSelectFields
	 *
	 * @return CTszhResult
	 */
	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		static $arFields = array(
			"ID" => Array("FIELD" => "T.ID", "TYPE" => "int"),
			"NAME" => Array("FIELD" => "T.NAME", "TYPE" => "string"),
			"SITE_ID" => Array("FIELD" => "T.SITE_ID", "TYPE" => "string"),
			"CODE" => Array("FIELD" => "T.CODE", "TYPE" => "string"),
            "CONFIRM_PARAM" => Array("FIELD" => "T.CONFIRM_PARAM", "TYPE" => "string"),
            "CONFIRM_TEXT" => Array("FIELD" => "T.CONFIRM_TEXT", "TYPE" => "string"),
			"TIMESTAMP_X" => Array("FIELD" => "T.TIMESTAMP_X", "TYPE" => "datetime"),

			"ADDRESS" => Array("FIELD" => "T.ADDRESS", "TYPE" => "string"),
			"EMAIL" => Array("FIELD" => "T.EMAIL", "TYPE" => "string"),
			"PHONE" => Array("FIELD" => "T.PHONE", "TYPE" => "string"),
			"PHONE_DISP" => Array("FIELD" => "T.PHONE_DISP", "TYPE" => "string"),
			"RECEIPT_TEMPLATE" => Array("FIELD" => "T.RECEIPT_TEMPLATE", "TYPE" => "string"),

			"INN" => Array("FIELD" => "T.INN", "TYPE" => "string"),
			"KPP" => Array("FIELD" => "T.KPP", "TYPE" => "string"),
			"KBK" => Array("FIELD" => "T.KBK", "TYPE" => "string"),
			"OKTMO" => Array("FIELD" => "T.OKTMO", "TYPE" => "string"),
			"RSCH" => Array("FIELD" => "T.RSCH", "TYPE" => "string"),
			"BANK" => Array("FIELD" => "T.BANK", "TYPE" => "string"),
			"KSCH" => Array("FIELD" => "T.KSCH", "TYPE" => "string"),
			"BIK" => Array("FIELD" => "T.BIK", "TYPE" => "string"),

			"IS_BUDGET" => Array("FIELD" => "T.IS_BUDGET", "TYPE" => "char"),

			"HEAD_NAME" => Array("FIELD" => "T.HEAD_NAME", "TYPE" => "string"),
			"LEGAL_ADDRESS" => Array("FIELD" => "T.LEGAL_ADDRESS", "TYPE" => "string"),
			"OFFICE_HOURS" => Array("FIELD" => "T.OFFICE_HOURS", "TYPE" => "string"),
			"MONETA_ENABLED" => Array("FIELD" => "T.MONETA_ENABLED", "TYPE" => "char"),
			"MONETA_OFFER" => Array("FIELD" => "T.MONETA_OFFER", "TYPE" => "char"),
			"MONETA_NO_OFFER" => Array("FIELD" => "T.MONETA_NO_OFFER", "TYPE" => "char"),
			"MONETA_EMAIL" => Array("FIELD" => "T.MONETA_EMAIL", "TYPE" => "string"),

			"METER_VALUES_START_DATE" => Array("FIELD" => "T.METER_VALUES_START_DATE", "TYPE" => "int"),
			"METER_VALUES_END_DATE" => Array("FIELD" => "T.METER_VALUES_END_DATE", "TYPE" => "int"),

			"ENABLE_CUSTOM_PAYMENT" => Array("FIELD" => "T.ENABLE_CUSTOM_PAYMENT", "TYPE" => "char"),
			"OVERHAUL_OFF" => Array("FIELD" => "T.OVERHAUL_OFF", "TYPE" => "char"),
			"PAYMENT_RECEIPT_TYPES" => Array("FIELD" => "T.PAYMENT_RECEIPT_TYPES", "TYPE" => "string"),

			"ADDITIONAL_INFO_MAIN" => Array("FIELD" => "T.ADDITIONAL_INFO_MAIN", "TYPE" => "string"),
			"ADDITIONAL_INFO_OVERHAUL" => Array("FIELD" => "T.ADDITIONAL_INFO_OVERHAUL", "TYPE" => "string"),
			"ANNOTATION_MAIN" => Array("FIELD" => "T.ANNOTATION_MAIN", "TYPE" => "string"),
			"ANNOTATION_OVERHAUL" => Array("FIELD" => "T.ANNOTATION_OVERHAUL", "TYPE" => "string"),
            "ORGANIZATION_TYPE" => Array("FIELD" => "T.ORGANIZATION_TYPE", "TYPE" => "int"),
		);

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = array_keys($arFields);
		}
		elseif (in_array('*', $arSelectFields))
		{
			$arSelectFields = array_merge(array_keys($arFields), array('UF_*'));
		}

		$obUserFieldsSql = new CUserTypeSQL;
		$obUserFieldsSql->SetEntity("TSZH", "T.ID");
		$obUserFieldsSql->SetSelect($arSelectFields);
		$obUserFieldsSql->SetFilter($arFilter);
		$obUserFieldsSql->SetOrder($arOrder);

		$dbRes = self::GetListMakeQuery('b_tszh T', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields, $obUserFieldsSql, "T.ID");
		if (is_object($dbRes))
		{
			global $USER_FIELD_MANAGER;
			$dbRes->SetUserFields($USER_FIELD_MANAGER->GetUserFields("TSZH"));
		}

		if (defined("BX_COMP_MANAGED_CACHE"))
		{
			global $CACHE_MANAGER;
			$CACHE_MANAGER->RegisterTag(self::CACHE_TAG_NAME);
		}

		return new CTszhResult($dbRes);
	}

	/**
	 * ��������� �������� �� ������������ ���������� �������� �����
	 *
	 * ���������� true, ���� ������������ ����� ������� ���� ���/��� � ������ � ������ ���������� ������� ������
	 *
	 * @param int $USER_ID ID ������������. ���� �� ������, ��������������� ������� ������������
	 *
	 * @return bool
	 */
	public static function IsTenant($USER_ID = 0)
	{
		global $USER;
		if (!is_object($USER))
		{
			return false;
		}

		if ($USER_ID <= 0)
		{
			if (!$USER->IsAuthorized())
			{
				return false;
			}
			$USER_ID = $USER->GetID();
		}

		$arUserGroups = CUser::GetUserGroup($USER_ID);

		$isInGroup = array_search(self::GetTenantGroup(), $arUserGroups) !== false;

		if ($isInGroup)
		{
			return is_array(CTszhAccount::GetByUserID($USER_ID));
		}

		return false;
	}

	/**
	 * ���������� ID ������ ��������
	 *
	 * @return int
	 */
	public static function GetTenantGroup()
	{
		return COption::GetOptionInt(TSZH_MODULE_ID, "user_group_id");
	}

	/**
	 * �������� ������ ������ ������ � �������� ��������
	 *
	 * @param string $string
	 *
	 * @return string
	 */
	public static function ToUpperFirstChar($string)
	{
		return ToUpper(substr($string, 0, 1)) . substr($string, 1);
	}

	/**
	 * @param array $arFields
	 * @param array $arOrder
	 * @param array $arFilter
	 * @param array $arGroupBy
	 * @param array $arSelectFields
	 * @param bool|CUserTypeSQL $obUserFieldsSql
	 * @param bool|string $strUserFieldsID
	 *
	 * @return array
	 */
	protected static function PrepareSql(&$arFields, $arOrder, $arFilter, $arGroupBy, $arSelectFields, $obUserFieldsSql = false, $strUserFieldsID = false)
	{
		global $DB;

		$strSqlSelect = "";
		$strSqlFrom = "";
		$strSqlWhere = "";
		$strSqlGroupBy = "";

		$isDistinct = false;

		$arGroupByFunct = array("COUNT", "AVG", "MIN", "MAX", "SUM");

		$arAlreadyJoined = array();

		// GROUP BY -->
		if (is_array($arGroupBy) && count($arGroupBy) > 0)
		{
			$arSelectFields = $arGroupBy;
			foreach ($arGroupBy as $key => $val)
			{
				$val = strtoupper($val);
				$key = strtoupper($key);

				if (array_key_exists($val, $arFields))
				{
					if (!in_array($key, $arGroupByFunct))
					{
						if (strlen($strSqlGroupBy) > 0)
						{
							$strSqlGroupBy .= ", ";
						}
						$strSqlGroupBy .= $arFields[$val]["FIELD"];

						if (isset($arFields[$val]["FROM"])
						    && strlen($arFields[$val]["FROM"]) > 0
						    && !in_array($arFields[$val]["FROM"], $arAlreadyJoined)
						)
						{
							if (strlen($strSqlFrom) > 0)
							{
								$strSqlFrom .= " ";
							}
							$strSqlFrom .= $arFields[$val]["FROM"];
							$arAlreadyJoined[] = $arFields[$val]["FROM"];
							$isDistinct = $isDistinct || isset($arFields[$val]["FIELD_SELECT"]);
						}
					}
				}
				else
				{
					trigger_error("Unknown field $val or operation $key in group by parameter", E_USER_WARNING);
				}
			}
		}
		// <-- GROUP BY

		// SELECT -->
		$arFieldsKeys = array_keys($arFields);

		if (is_array($arGroupBy) && count($arGroupBy) == 0)
		{
			$strSqlSelect = "COUNT(%%_DISTINCT_%% " . $arFields[$arFieldsKeys[0]]["FIELD"] . ") as CNT ";
		}
		else
		{
			if (isset($arSelectFields) && !is_array($arSelectFields) && is_string($arSelectFields) && strlen($arSelectFields) > 0 && array_key_exists($arSelectFields, $arFields))
			{
				$arSelectFields = array($arSelectFields);
			}

			if (!isset($arSelectFields)
			    || !is_array($arSelectFields)
			    || count($arSelectFields) <= 0
			    || in_array("*", $arSelectFields)
			)
			{
				for ($i = 0; $i < count($arFieldsKeys); $i++)
				{
					if (isset($arFields[$arFieldsKeys[$i]]["WHERE_ONLY"])
					    && $arFields[$arFieldsKeys[$i]]["WHERE_ONLY"] == "Y"
					)
					{
						continue;
					}

					if (strlen($strSqlSelect) > 0)
					{
						$strSqlSelect .= ", ";
					}

					if ($arFields[$arFieldsKeys[$i]]["TYPE"] == "datetime")
					{
						if ((strtoupper($DB->type) == "ORACLE" || strtoupper($DB->type) == "MSSQL") && (array_key_exists($arFieldsKeys[$i], $arOrder)))
						{
							$strSqlSelect .= $arFields[$arFieldsKeys[$i]]["FIELD"] . " as `" . $arFieldsKeys[$i] . "_X1`, ";
						}

						$strSqlSelect .= $DB->DateToCharFunction($arFields[$arFieldsKeys[$i]]["FIELD"], "FULL") . " as `" . $arFieldsKeys[$i] . "`";
					}
					elseif ($arFields[$arFieldsKeys[$i]]["TYPE"] == "date")
					{
						if ((strtoupper($DB->type) == "ORACLE" || strtoupper($DB->type) == "MSSQL") && (array_key_exists($arFieldsKeys[$i], $arOrder)))
						{
							$strSqlSelect .= $arFields[$arFieldsKeys[$i]]["FIELD"] . " as `" . $arFieldsKeys[$i] . "_X1`, ";
						}

						$strSqlSelect .= $DB->DateToCharFunction($arFields[$arFieldsKeys[$i]]["FIELD"], "SHORT") . " as `" . $arFieldsKeys[$i] . "`";
					}
					else
					{
						$fieldName = "FIELD";
						if (isset($arFields[$arFieldsKeys[$i]]["FIELD_SELECT"]))
						{
							$fieldName = "FIELD_SELECT";
						}

						$strSqlSelect .= $arFields[$arFieldsKeys[$i]][$fieldName] . " as `" . $arFieldsKeys[$i] . "`";
					}

					if (isset($arFields[$arFieldsKeys[$i]]["FROM"])
					    && strlen($arFields[$arFieldsKeys[$i]]["FROM"]) > 0
					    && !in_array($arFields[$arFieldsKeys[$i]]["FROM"], $arAlreadyJoined)
					)
					{
						if (strlen($strSqlFrom) > 0)
						{
							$strSqlFrom .= " ";
						}
						$strSqlFrom .= $arFields[$arFieldsKeys[$i]]["FROM"];
						$arAlreadyJoined[] = $arFields[$arFieldsKeys[$i]]["FROM"];
						$isDistinct = $isDistinct || isset($arFields[$arFieldsKeys[$i]]["FIELD_SELECT"]);
					}
				}
			}
			else
			{
				foreach ($arSelectFields as $key => $val)
				{
					$val = strtoupper($val);
					$key = strtoupper($key);
					if (array_key_exists($val, $arFields))
					{
						if (strlen($strSqlSelect) > 0)
						{
							$strSqlSelect .= ", ";
						}

						if (in_array($key, $arGroupByFunct))
						{
							$strSqlSelect .= $key . "(" . $arFields[$val]["FIELD"] . ") as `" . $val . "`";
						}
						else
						{
							if ($arFields[$val]["TYPE"] == "datetime")
							{
								if ((strtoupper($DB->type) == "ORACLE" || strtoupper($DB->type) == "MSSQL") && (array_key_exists($val, $arOrder)))
								{
									$strSqlSelect .= $arFields[$val]["FIELD"] . " as `" . $val . "_X1`, ";
								}

								$strSqlSelect .= $DB->DateToCharFunction($arFields[$val]["FIELD"], "FULL") . " as `" . $val . "`";
							}
							elseif ($arFields[$val]["TYPE"] == "date")
							{
								if ((strtoupper($DB->type) == "ORACLE" || strtoupper($DB->type) == "MSSQL") && (array_key_exists($val, $arOrder)))
								{
									$strSqlSelect .= $arFields[$val]["FIELD"] . " as `" . $val . "_X1`, ";
								}

								$strSqlSelect .= $DB->DateToCharFunction($arFields[$val]["FIELD"], "SHORT") . " as `" . $val . "`";
							}
							else
							{
								$fieldName = "FIELD";
								if (isset($arFields[$val]["FIELD_SELECT"]))
								{
									$fieldName = "FIELD_SELECT";
								}

								$strSqlSelect .= $arFields[$val][$fieldName] . " as `" . $val . "`";
							}
						}

						if (isset($arFields[$val]["FROM"])
						    && strlen($arFields[$val]["FROM"]) > 0
						    && !in_array($arFields[$val]["FROM"], $arAlreadyJoined)
						)
						{
							if (strlen($strSqlFrom) > 0)
							{
								$strSqlFrom .= " ";
							}
							$strSqlFrom .= $arFields[$val]["FROM"];
							$arAlreadyJoined[] = $arFields[$val]["FROM"];
							$isDistinct = $isDistinct || isset($arFields[$val]["FIELD_SELECT"]);
						}
					}
				}
			}

			if (strlen($strSqlGroupBy) > 0)
			{
				if (strlen($strSqlSelect) > 0)
				{
					$strSqlSelect .= ", ";
				}
				$strSqlSelect .= "COUNT(%%_DISTINCT_%% " . $arFields[$arFieldsKeys[0]]["FIELD"] . ") as CNT";
			}
			else
			{
				$strSqlSelect = "%%_DISTINCT_%% " . $strSqlSelect;
			}
		}
		if (is_object($obUserFieldsSql))
		{
			if ($obUserFieldsSql->GetDistinct() && strstr($strSqlSelect, '%%_DISTINCT_%%') === false)
			{
				$strSqlSelect = "%%_DISTINCT_%% " . $strSqlSelect;
			}
			$strSqlSelect .= " " . $obUserFieldsSql->GetSelect();
		}
		// <-- SELECT

		// WHERE -->
		$arSqlSearch = Array();

		if (!is_array($arFilter))
		{
			$filter_keys = Array();
		}
		else
		{
			$filter_keys = array_keys($arFilter);
		}

		for ($i = 0; $i < count($filter_keys); $i++)
		{
			$vals = $arFilter[$filter_keys[$i]];
			if (!is_array($vals))
			{
				$vals = array($vals);
			}

			$key = $filter_keys[$i];
			$key_res = self::GetFilterOperation($key);
			$key = $key_res["FIELD"];
			$strNegative = $key_res["NEGATIVE"];
			$strOperation = $key_res["OPERATION"];
			$strOrNull = $key_res["OR_NULL"];

			if (array_key_exists($key, $arFields))
			{
				$arSqlSearch_tmp = array();
				if (count($vals) > 0)
				{
					if (isset($arFields[$key]["BINDING_TABLE"]))
					{
						if ($strOperation == "IN")
						{
							$strOperation = "=";
						}
						$arConditions = array();
						foreach ($vals as $val)
						{
							if ($val === false)
							{
								$condition = $arFields[$key]["BINDING_WHERE_FIELD"] . " IS NULL";
							}
							else
							{
								switch ($arFields[$key]["TYPE"])
								{
									case "int":
										$val = intval($val);
										break;

									case "double":
										$val = floatval($val);
										break;

									case "string":
									case "char":
										if ($strOperation == "QUERY")
										{
											$val = "'%" . $DB->ForSql($val) . "%'";
										}
										else
										{
											$val = "'" . $DB->ForSql($val) . "'";
										}
										break;
								}
								if ($strOperation == "QUERY")
								{
									$condition = $arFields[$key]["BINDING_WHERE_FIELD"] . " LIKE UPPER(" . $val . ")";
								}
								else
								{
									$condition = $arFields[$key]["BINDING_WHERE_FIELD"] . $strOperation . $val;
								}
							}
							$arConditions[] = $condition;
						}
						$conditionString = implode(" OR ", array_unique($arConditions));
						$arSqlSearch_tmp[] = $arFields[$key]["BINDING_EXTERNAL_FIELD"] . ($strNegative == "Y" ? " NOT" : "") . " IN (SELECT " . $arFields[$key]["BINDING_SELECT_FIELD"] . " FROM " . $arFields[$key]["BINDING_TABLE"] . " WHERE " . $conditionString . ")";
					}
					elseif ($strOperation == "IN")
					{
						if (isset($arFields[$key]["WHERE"]))
						{
							$arSqlSearch_tmp1 = call_user_func_array(
								$arFields[$key]["WHERE"],
								array($vals, $key, $strOperation, $strNegative, $arFields[$key]["FIELD"], $arFields, $arFilter)
							);
							if ($arSqlSearch_tmp1 !== false)
							{
								$arSqlSearch_tmp[] = $arSqlSearch_tmp1;
							}
						}
						else
						{
							if ($arFields[$key]["TYPE"] == "int")
							{
								array_walk($vals, create_function("&\$item", "\$item=IntVal(\$item);"));
								$vals = array_unique($vals);
								$val = implode(",", $vals);

								if (count($vals) <= 0)
								{
									$arSqlSearch_tmp[] = "(1 = 2)";
								}
								else
								{
									$arSqlSearch_tmp[] = (($strNegative == "Y") ? " NOT " : "") . "(" . $arFields[$key]["FIELD"] . " IN (" . $val . "))";
								}
							}
							elseif ($arFields[$key]["TYPE"] == "double" || $arFields[$key]["TYPE"] == "float")
							{
								array_walk($vals, create_function("&\$item", "\$item=DoubleVal(\$item);"));
								$vals = array_unique($vals);
								$val = implode(",", $vals);

								if (count($vals) <= 0)
								{
									$arSqlSearch_tmp[] = "(1 = 2)";
								}
								else
								{
									$arSqlSearch_tmp[] = (($strNegative == "Y") ? " NOT " : "") . "(" . $arFields[$key]["FIELD"] . " " . $strOperation . " (" . $val . "))";
								}
							}
							elseif ($arFields[$key]["TYPE"] == "string" || $arFields[$key]["TYPE"] == "char")
							{
								array_walk($vals, create_function("&\$item", "\$item=\"'\".\$GLOBALS[\"DB\"]->ForSql(\$item).\"'\";"));
								$vals = array_unique($vals);
								$val = implode(",", $vals);

								if (count($vals) <= 0)
								{
									$arSqlSearch_tmp[] = "(1 = 2)";
								}
								else
								{
									$arSqlSearch_tmp[] = (($strNegative == "Y") ? " NOT " : "") . "(" . $arFields[$key]["FIELD"] . " " . $strOperation . " (" . $val . "))";
								}
							}
							elseif ($arFields[$key]["TYPE"] == "datetime")
							{
								array_walk($vals, create_function("&\$item", "\$item=\"'\".\$GLOBALS[\"DB\"]->CharToDateFunction(\$GLOBALS[\"DB\"]->ForSql(\$item), \"FULL\").\"'\";"));
								$vals = array_unique($vals);
								$val = implode(",", $vals);

								if (count($vals) <= 0)
								{
									$arSqlSearch_tmp[] = "1 = 2";
								}
								else
								{
									$arSqlSearch_tmp[] = ($strNegative == "Y" ? " NOT " : "") . "(" . $arFields[$key]["FIELD"] . " " . $strOperation . " (" . $val . "))";
								}
							}
							elseif ($arFields[$key]["TYPE"] == "date")
							{
								array_walk($vals, create_function("&\$item", "\$item=\"'\".\$GLOBALS[\"DB\"]->CharToDateFunction(\$GLOBALS[\"DB\"]->ForSql(\$item), \"SHORT\").\"'\";"));
								$vals = array_unique($vals);
								$val = implode(",", $vals);

								if (count($vals) <= 0)
								{
									$arSqlSearch_tmp[] = "1 = 2";
								}
								else
								{
									$arSqlSearch_tmp[] = ($strNegative == "Y" ? " NOT " : "") . "(" . $arFields[$key]["FIELD"] . " " . $strOperation . " (" . $val . "))";
								}
							}
						}
					}
					else
					{
						for ($j = 0; $j < count($vals); $j++)
						{
							$val = $vals[$j];

							if (isset($arFields[$key]["WHERE"]))
							{
								$arSqlSearch_tmp1 = call_user_func_array(
									$arFields[$key]["WHERE"],
									array($val, $key, $strOperation, $strNegative, $arFields[$key]["FIELD"], $arFields, $arFilter)
								);
								if ($arSqlSearch_tmp1 !== false)
								{
									$arSqlSearch_tmp[] = $arSqlSearch_tmp1;
								}
							}
							else
							{
								if ($arFields[$key]["TYPE"] == "int")
								{
									if ((IntVal($val) == 0) && (strpos($strOperation, "=") !== false))
									{
										$arSqlSearch_tmp[] = "(" . $arFields[$key]["FIELD"] . " IS " . (($strNegative == "Y") ? "NOT " : "") . "NULL) " . (($strNegative == "Y") ? "AND" : "OR") . " " . (($strNegative == "Y") ? "NOT " : "") . "(" . $arFields[$key]["FIELD"] . " " . $strOperation . " 0)";
									}
									else
									{
										$arSqlSearch_tmp[] = (($strNegative == "Y") ? " " . $arFields[$key]["FIELD"] . " IS NULL OR NOT " : "") . "(" . $arFields[$key]["FIELD"] . " " . $strOperation . " " . IntVal($val) . " )";
									}
								}
								elseif ($arFields[$key]["TYPE"] == "double" || $arFields[$key]["TYPE"] == "float")
								{
									$val = str_replace(",", ".", $val);

									if ((DoubleVal($val) == 0) && (strpos($strOperation, "=") !== false))
									{
										$arSqlSearch_tmp[] = "(" . $arFields[$key]["FIELD"] . " IS " . (($strNegative == "Y") ? "NOT " : "") . "NULL) " . (($strNegative == "Y") ? "AND" : "OR") . " " . (($strNegative == "Y") ? "NOT " : "") . "(" . $arFields[$key]["FIELD"] . " " . $strOperation . " 0)";
									}
									else
									{
										$arSqlSearch_tmp[] = (($strNegative == "Y") ? " " . $arFields[$key]["FIELD"] . " IS NULL OR NOT " : "") . "(" . $arFields[$key]["FIELD"] . " " . $strOperation . " " . DoubleVal($val) . " )";
									}
								}
								elseif ($arFields[$key]["TYPE"] == "string" || $arFields[$key]["TYPE"] == "char")
								{
									if ($strOperation == "QUERY")
									{
										if ($arFields[$key]["CONCAT"])
										{
											$arSqlSearch_tmp[] = (($strNegative == "Y") ? " " . $arFields[$key]["FIELD"] . " IS NULL OR NOT " : "") . "(" . $arFields[$key]["FIELD"] . " LIKE UPPER('%" . $DB->ForSql($val) . "%') )";
										}
										else
										{
											$arSqlSearch_tmp[] = GetFilterQuery($arFields[$key]["FIELD"], $val, "Y");
										}
									}
									else
									{
										if ((strlen($val) == 0) && (strpos($strOperation, "=") !== false))
										{
											$arSqlSearch_tmp[] = "(" . $arFields[$key]["FIELD"] . " IS " . (($strNegative == "Y") ? "NOT " : "") . "NULL) " . (($strNegative == "Y") ? "AND NOT" : "OR") . " (" . $DB->Length($arFields[$key]["FIELD"]) . " <= 0) " . (($strNegative == "Y") ? "AND NOT" : "OR") . " (" . $arFields[$key]["FIELD"] . " " . $strOperation . " '" . $DB->ForSql($val) . "' )";
										}
										else
										{
											$arSqlSearch_tmp[] = (($strNegative == "Y") ? " " . $arFields[$key]["FIELD"] . " IS NULL OR NOT " : "") . "(" . $arFields[$key]["FIELD"] . " " . $strOperation . " '" . $DB->ForSql($val) . "' )";
										}
									}
								}
								elseif ($arFields[$key]["TYPE"] == "datetime")
								{
									if (strlen($val) <= 0)
									{
										$arSqlSearch_tmp[] = ($strNegative == "Y" ? "NOT" : "") . "(" . $arFields[$key]["FIELD"] . " IS NULL)";
									}
									else
									{
										$arSqlSearch_tmp[] = ($strNegative == "Y" ? " " . $arFields[$key]["FIELD"] . " IS NULL OR NOT " : "") . "(" . $arFields[$key]["FIELD"] . " " . $strOperation . " " . $DB->CharToDateFunction($DB->ForSql($val), "FULL") . ")";
									}
								}
								elseif ($arFields[$key]["TYPE"] == "date")
								{
									if (strlen($val) <= 0)
									{
										$arSqlSearch_tmp[] = ($strNegative == "Y" ? "NOT" : "") . "(" . $arFields[$key]["FIELD"] . " IS NULL)";
									}
									else
									{
										$arSqlSearch_tmp[] = ($strNegative == "Y" ? " " . $arFields[$key]["FIELD"] . " IS NULL OR NOT " : "") . "(" . $arFields[$key]["FIELD"] . " " . $strOperation . " " . $DB->CharToDateFunction($DB->ForSql($val), "SHORT") . ")";
									}
								}
							}
						}
					}
				}

				if (isset($arFields[$key]["FROM"])
				    && strlen($arFields[$key]["FROM"]) > 0
				    && !in_array($arFields[$key]["FROM"], $arAlreadyJoined)
				)
				{
					if (strlen($strSqlFrom) > 0)
					{
						$strSqlFrom .= " ";
					}
					$strSqlFrom .= $arFields[$key]["FROM"];
					$arAlreadyJoined[] = $arFields[$key]["FROM"];
					$isDistinct = $isDistinct || isset($arFields[$key]["FIELD_SELECT"]);
				}

				$strSqlSearch_tmp = "";
				for ($j = 0; $j < count($arSqlSearch_tmp); $j++)
				{
					if ($j > 0)
					{
						$strSqlSearch_tmp .= ($strNegative == "Y" ? " AND " : " OR ");
					}
					$strSqlSearch_tmp .= "(" . $arSqlSearch_tmp[$j] . ")";
				}
				if ($strOrNull == "Y")
				{
					if (strlen($strSqlSearch_tmp) > 0)
					{
						$strSqlSearch_tmp .= ($strNegative == "Y" ? " AND " : " OR ");
					}
					$strSqlSearch_tmp .= "(" . $arFields[$key]["FIELD"] . " IS " . ($strNegative == "Y" ? "NOT " : "") . "NULL)";

					if ($arFields[$key]["TYPE"] == "int" || $arFields[$key]["TYPE"] == "double")
					{
						if (strlen($strSqlSearch_tmp) > 0)
						{
							$strSqlSearch_tmp .= ($strNegative == "Y" ? " AND " : " OR ");
						}
						$strSqlSearch_tmp .= "(" . $arFields[$key]["FIELD"] . " " . ($strNegative == "Y" ? "<>" : "=") . " 0)";
					}
					elseif ($arFields[$key]["TYPE"] == "string" || $arFields[$key]["TYPE"] == "char")
					{
						if (strlen($strSqlSearch_tmp) > 0)
						{
							$strSqlSearch_tmp .= ($strNegative == "Y" ? " AND " : " OR ");
						}
						$strSqlSearch_tmp .= "(" . $arFields[$key]["FIELD"] . " " . ($strNegative == "Y" ? "<>" : "=") . " '')";
					}
				}

				if ($strSqlSearch_tmp != "")
				{
					$arSqlSearch[] = "(" . $strSqlSearch_tmp . ")";
				}
			}
			else
			{
				// do not show warnings for user fields
				if (preg_match('/^UF_*/', $key) > 0)
				{
					continue;
				}
				trigger_error("Unknown field $key in filter parameter", E_USER_WARNING);
			}
		}
		if (is_object($obUserFieldsSql))
		{
			$strWhere = $obUserFieldsSql->GetFilter();
			if (strlen($strWhere) > 0)
			{
				$arSqlSearch[] = $strWhere;
			}

			$strJoin = $obUserFieldsSql->GetJoin($strUserFieldsID);
			if (strlen($strJoin) > 0)
			{
				$strSqlFrom .= (strlen($strSqlFrom) > 0 ? ' ' : '') . $strJoin;
				$arAlreadyJoined[] = $strJoin;
			}
		}

		for ($i = 0; $i < count($arSqlSearch); $i++)
		{
			if (strlen($strSqlWhere) > 0)
			{
				$strSqlWhere .= " AND ";
			}
			$strSqlWhere .= "(" . $arSqlSearch[$i] . ")";
		}
		// <-- WHERE

		// ORDER BY -->
		$arSqlOrder = Array();
		if($arOrder === false)
		{
			$arOrder = array();
		}
		foreach ($arOrder as $by => $order)
		{
			$by = strtoupper($by);
			$order = strtoupper($order);

			if ($order != "ASC")
			{
				$order = "DESC";
			}
			else
			{
				$order = "ASC";
			}

			if (array_key_exists($by, $arFields))
			{
				$arSqlOrder[] = " " . $arFields[$by]["FIELD"] . " " . $order . " ";

				if (isset($arFields[$by]["FROM"])
				    && strlen($arFields[$by]["FROM"]) > 0
				    && !in_array($arFields[$by]["FROM"], $arAlreadyJoined)
				)
				{
					if (strlen($strSqlFrom) > 0)
					{
						$strSqlFrom .= " ";
					}
					$strSqlFrom .= $arFields[$by]["FROM"];
					$arAlreadyJoined[] = $arFields[$by]["FROM"];
					$isDistinct = $isDistinct || isset($arFields[$by]["FIELD_SELECT"]);
				}
			}
			elseif ($by == "RAND")
			{
				$arSqlOrder[] = " RAND() ";
			}
			elseif (is_object($obUserFieldsSql) && $s = $obUserFieldsSql->GetOrder($by))
			{
				$arSqlOrder[] = " " . strtoupper($s) . " " . $order . " ";
			}
			else
			{
				trigger_error("Unknown field $key in sort by parameter", E_USER_WARNING);
			}
		}

		$strSqlOrderBy = "";
		DelDuplicateSort($arSqlOrder);

		for ($i = 0; $i < count($arSqlOrder); $i++)
		{
			if (strlen($strSqlOrderBy) > 0)
			{
				$strSqlOrderBy .= ", ";
			}

			if (strtoupper($DB->type) == "ORACLE")
			{
				if (substr($arSqlOrder[$i], -3) == "ASC")
				{
					$strSqlOrderBy .= $arSqlOrder[$i] . " NULLS FIRST";
				}
				else
				{
					$strSqlOrderBy .= $arSqlOrder[$i] . " NULLS LAST";
				}
			}
			else
			{
				$strSqlOrderBy .= $arSqlOrder[$i];
			}
		}
		// <-- ORDER BY

		if ($isDistinct)
		{
			$strSqlSelect = str_replace("%%_DISTINCT_%%", "DISTINCT", $strSqlSelect);
		}

		return array(
			"SELECT" => $strSqlSelect,
			"FROM" => $strSqlFrom,
			"WHERE" => $strSqlWhere,
			"GROUPBY" => $strSqlGroupBy,
			"ORDERBY" => $strSqlOrderBy
		);
	}

	/**
	 * @param string $key
	 *
	 * @return array
	 */
	public static function GetFilterOperation($key)
	{
		$strNegative = "N";
		if (substr($key, 0, 1) == "!")
		{
			$key = substr($key, 1);
			$strNegative = "Y";
		}

		$strOrNull = "N";
		if (substr($key, 0, 1) == "+")
		{
			$key = substr($key, 1);
			$strOrNull = "Y";
		}

		if (substr($key, 0, 2) == ">=")
		{
			$key = substr($key, 2);
			$strOperation = ">=";
		}
		elseif (substr($key, 0, 1) == ">")
		{
			$key = substr($key, 1);
			$strOperation = ">";
		}
		elseif (substr($key, 0, 2) == "<=")
		{
			$key = substr($key, 2);
			$strOperation = "<=";
		}
		elseif (substr($key, 0, 1) == "<")
		{
			$key = substr($key, 1);
			$strOperation = "<";
		}
		elseif (substr($key, 0, 1) == "@")
		{
			$key = substr($key, 1);
			$strOperation = "IN";
		}
		elseif (substr($key, 0, 1) == "~")
		{
			$key = substr($key, 1);
			$strOperation = "LIKE";
		}
		elseif (substr($key, 0, 1) == "%")
		{
			$key = substr($key, 1);
			$strOperation = "QUERY";
		}
		else
		{
			$strOperation = "=";
		}

		return array("FIELD" => $key, "NEGATIVE" => $strNegative, "OPERATION" => $strOperation, "OR_NULL" => $strOrNull);
	}

	/**
	 * @param string $strTable
	 * @param array $arFields
	 * @param array $arOrder
	 * @param array $arFilter
	 * @param bool|array $arGroupBy
	 * @param bool|array $arNavStartParams
	 * @param array $arSelectFields
	 * @param bool|CUserTypeSQL $obUserFieldsSql
	 * @param bool|string $strUserFieldsID
	 *
	 * @return bool|CDBResult
	 */
	public static function GetListMakeQuery($strTable, &$arFields, $arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array(), $obUserFieldsSql = false, $strUserFieldsID = false)
	{
		global $DB;

		$arSelectFields = array_unique($arSelectFields);

		$arSqls = self::PrepareSql($arFields, $arOrder, $arFilter, $arGroupBy, $arSelectFields, $obUserFieldsSql, $strUserFieldsID);

		//		echo '<pre>' . htmlspecialcharsbx(print_r($arSqls, true)) . '</pre>';

		$arSqls["SELECT"] = str_replace("%%_DISTINCT_%%", "", $arSqls["SELECT"]);

		if (is_array($arGroupBy) && count($arGroupBy) == 0)
		{
			$strSql =
				"SELECT " . $arSqls["SELECT"] . " " .
				"FROM $strTable " .
				"	" . $arSqls["FROM"] . " ";
			if (strlen($arSqls["WHERE"]) > 0)
			{
				$strSql .= "WHERE " . $arSqls["WHERE"] . " ";
			}
			if (strlen($arSqls["GROUPBY"]) > 0)
			{
				$strSql .= "GROUP BY " . $arSqls["GROUPBY"] . " ";
			}

			//echo "!1!=".htmlspecialcharsbx($strSql)."<br>";

			$dbRes = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			/** @noinspection PhpAssignmentInConditionInspection */
			if ($arRes = $dbRes->Fetch())
			{
				return $arRes["CNT"];
			}
			else
			{
				return false;
			}
		}

		$strSql =
			"SELECT " . $arSqls["SELECT"] . " " .
			"FROM $strTable " .
			"	" . $arSqls["FROM"] . " ";
		if (strlen($arSqls["WHERE"]) > 0)
		{
			$strSql .= "WHERE " . $arSqls["WHERE"] . " ";
		}
		if (strlen($arSqls["GROUPBY"]) > 0)
		{
			$strSql .= "GROUP BY " . $arSqls["GROUPBY"] . " ";
		}
		if (strlen($arSqls["ORDERBY"]) > 0)
		{
			$strSql .= "ORDER BY " . $arSqls["ORDERBY"] . " ";
		}

		if (is_array($arNavStartParams) && IntVal($arNavStartParams["nTopCount"]) <= 0)
		{
			$table = array_pop(explode(' ', $strTable));
			$strSql_tmp =
				"SELECT COUNT(DISTINCT($table.ID)) as CNT " .
				"FROM $strTable " .
				"	" . $arSqls["FROM"] . " ";
			if (strlen($arSqls["WHERE"]) > 0)
			{
				$strSql_tmp .= "WHERE " . $arSqls["WHERE"] . " ";
			}
			if (strlen($arSqls["GROUPBY"]) > 0)
			{
				$strSql_tmp .= "GROUP BY " . $arSqls["GROUPBY"] . " ";
			}

			//echo "!2.1!=".htmlspecialcharsbx($strSql_tmp)."<br>";

			$dbRes = $DB->Query($strSql_tmp, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			$cnt = 0;
			if (strlen($arSqls["GROUPBY"]) <= 0)
			{
				/** @noinspection PhpAssignmentInConditionInspection */
				if ($arRes = $dbRes->Fetch())
				{
					$cnt = $arRes["CNT"];
				}
			}
			else
			{
				// ������ ��� MYSQL!!! ��� ORACLE ������ ���
				$cnt = $dbRes->SelectedRowsCount();
			}

			$dbRes = new CDBResult();

			$dbRes->NavQuery($strSql, $cnt, $arNavStartParams);
		}
		else
		{

			if (is_array($arNavStartParams) && IntVal($arNavStartParams["nTopCount"]) > 0)
			{
				$strSql .= "LIMIT " . $arNavStartParams["nTopCount"];
			}

			$dbRes = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

		}

		//echo '<p>' . htmlspecialcharsbx(print_r($strSql, true)) . '</p>';

		return $dbRes;
	}

	/**
	 * GetTimeZoneOffset()
	 * ���������� �������� �������� ����� ������������ ������������ ���������� ������� (������� ��� ����������� ������������� �� ������� �������� �������)
	 *
	 * @return int
	 */
	public static function GetTimeZoneOffset()
	{
		return class_exists('CTimeZone') ? CTimeZone::GetOffset() : 0;
	}

	/**
	 * DisableTimeZone()
	 * �������� ��������� ��������� �������� �� ��� ������� ������ (������� ��� ����������� ������������� �� ������� �������� �������)
	 *
	 * @return int
	 */
	public static function DisableTimeZone()
	{
		if (class_exists('CTimeZone'))
		{
			CTimeZone::Disable();
		}
	}

	/**
	 * EnableTimeZone()
	 * ��������� ��������� �������� �� ��� ������� ������ (������� ��� ����������� ������������� �� ������� �������� �������)
	 *
	 * @return int
	 */
	public static function EnableTimeZone()
	{
		if (class_exists('CTimeZone'))
		{
			CTimeZone::Enable();
		}
	}

	/**
	 * ���������� true, ���� � ������ ������ (�� ������� ����) ���� ��������� ��������� ��������
	 *
	 * @param bool|int $tszhID ID ������� ����������. ���� �� ������, ��������������� ������ ���������� �������� �����
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function IsMeterValuesInputEnabled($tszhID = false)
	{
		if ($tszhID === false)
		{
			$arTszh = self::GetBySite(SITE_ID);
		}
		else
		{
			$arTszh = self::GetByID($tszhID);
		}

		if (!is_array($arTszh) || empty($arTszh))
		{
			return false;
		}

		$start = intval($arTszh['METER_VALUES_START_DATE']);
		$end = intval($arTszh['METER_VALUES_END_DATE']);
		if (!$start || !$end)
		{
			return true;
		}

		$curDay = intval(date('j'));

		if ($start <= $end)
		{
			return $curDay >= $start && $curDay <= $end;
		}
		else
		{
			return $curDay >= $start || $curDay <= $end;
		}
	}

	/*��������� ��������� �������� �� ��������� ������*/
    public static function IsConfirmParam($tszhID = false)
    {
        if ($tszhID === false) {
            $arTszh = self::GetBySite(SITE_ID);
        } else {
            $arTszh = self::GetByID($tszhID);
        }

        if (!is_array($arTszh) || empty($arTszh)) {
            return false;
        }

        $start = $arTszh['CONFIRM_PARAM'];
        $end = $arTszh['CONFIRM_PARAM'];

        if (isset($start)) {
            return true;
        }
        if (isset($end)) {
            return true;
        }
    }
	/**
	 * ��������� ��� (����������������� ����� �����������������) �� ������������
	 *
	 * @param int $n ��� ��� ��������
	 * @param bool $bMustBeOrg ��������������� ����� ��� ����������� �� 10 ������
	 *
	 * @return bool true, ���� ��� ��������� � false � ��������� ������
	 */
    function checkINN($inn)
    {

        $result = false;

        if (CTszhFunctionalityController::isUkrainianVersion())
        {
            return $result;
        }

        $inn = (string) $inn;
        $inn_length = strlen($inn);

        $check_digit = function($inn, $coefficients) {
            $n = 0;
            foreach ($coefficients as $i => $k) {
                $n += $k * (int) $inn{$i};
            }
            return $n % 11 % 10;
        };

        switch ($inn_length) {
            case 10:
                $n10 = $check_digit($inn, [2, 4, 10, 3, 5, 9, 4, 6, 8]);
                if ($n10 === (int) $inn{9}) {
                    $result = true;
                }
                break;
            case 12:
                $n11 = $check_digit($inn, [7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);
                $n12 = $check_digit($inn, [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);
                if (($n11 === (int) $inn{10}) && ($n12 === (int) $inn{11})) {
                    $result = true;
                }
                break;
        }

        return $result;
    }
	/**
	 * ������� ������������ �������� ���������� �����
	 *
	 * @param int $n ��������� ����
	 * @param int $bik ��� �����
	 *
	 * @return bool true, ���� ��������� ���� ��������� � false � ��������� ������
	 */
	function checkRsch($n, $bik)
	{
		if (CTszhFunctionalityController::isUkrainianVersion())
		{
			return true;
		}

		$n = strval($n);
		if (!preg_match('/^[0-9]{20}$/', $n))
		{
			return false;
		}
		$bik = strval($bik);
		if (!preg_match('/^[0-9]{9}$/', $bik))
		{
			return false;
		}

		$sum = 0;
		$bikPart = substr($bik, -3, 3);
		// ��� ��������� ����������, ��� ������� ������������ �� 000 � 001 (��� � ����),
		// 23�-������� ����� ��� �������� ����������� ������� �����, ��� ��� ������ ������.
		// ������� ������ ���� "0", ����� 5 � 6 ����� ���, ����� 20-������� ����� �����.
		// � ��������� �������� ����������.
		if ($bikPart == '000' || $bikPart == '001')
		{
			$bikPart = '0' . substr($bik, 4, 2);
		}
		$n = $bikPart . $n;
		foreach (array(7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1) as $i => $weight)
		{
			$sum += ($weight * substr($n, $i, 1)) % 10;
		}

		return $sum % 10 == 0;
	}

	function checkEmail($email, $bStrict=false)
	{
		if(!$bStrict)
		{
			$email = trim($email);
			if(preg_match("#.*?[<\\[\\(](.*?)[>\\]\\)].*#i", $email, $arr) && strlen($arr[1])>0)
				$email = $arr[1];
		}

		//http://tools.ietf.org/html/rfc2821#section-4.5.3.1
		//4.5.3.1. Size limits and minimums
		if(strlen($email) > 320)
		{
			return false;
		}

		//http://tools.ietf.org/html/rfc2822#section-3.2.4
		//3.2.4. Atom
		static $atom = "=_0-9a-z+~'!\$&*^`|\\#%/?{}-";

		//"." can't be in the beginning or in the end of local-part
		//dot-atom-text = 1*atext *("." 1*atext)
		if(preg_match("#^[".$atom."]+(\\.[".$atom."]+)*@(([-0-9a-z]+\\.)+)([a-z0-9-]{2,20})$#i", $email))
		{
			return true;
		}
		else
		{
			/*punycode converter*/
			$arEmail = explode("@", $email);
			if ($arEmail[1])
			{
				require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/wizards/citrus/site_tszh/classes/idna_convert.class.php");
				$idn = new idna_convert(array('idn_version' => 2008));
				if (LANG_CHARSET == "windows-1251")
				{
					$arEmail[1] = iconv("CP1251", "UTF-8", $arEmail[1]);
				}
				$host = (stripos($arEmail[1], 'xn--') !== false) ? $idn->decode($arEmail[1]) : $idn->encode($arEmail[1]);

				if (preg_match("#^[" . $atom . "]+(\\.[" . $atom . "]+)*@(([-0-9a-z]+\\.)+)([a-z0-9-]{2,20})$#i", $arEmail[0]. "@". $host))
				{
					return true;
				}
			}
			return false;
		}
	}
	/**
	 * ���������� ���������� �������� ������������ ���������� ����� ������� ����������
	 * ������������ � ������� ���������� ��������� ����� �� ��������� ���������, ������� ��� �������� ������ � �������� ��������������.
	 * ���� ���� �������� ������, �� ����������� �������� ����������� ���������������� ����������� � �����������
	 *
	 * @param int $ID ID ������� ����������, ������� ���������� �������������
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function postValidate($ID)
	{
		// ���� �������� "������ ����������� �� �������" ���������� �������� ����� � ������� ���������������� �����������
		if (COption::GetOptionString("citrus.tszh", "hidden_notification", "N") == "Y")
		{
			CAdminNotify::DeleteByModule("citrus.tszh");

			return true;
		}

		self::$validationErrors = Array();

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		$arFields = self::GetByID($ID);
		if (!is_array($arFields))
		{
			throw new \Bitrix\Main\ArgumentException("Tszh not found!", "ID");
		}

		$arRequiredTszhFields = array(
			"NAME",
			"INN",
			"KPP",
			"RSCH",
			"BANK",
			"BIK",
			"HEAD_NAME",
			"LEGAL_ADDRESS"
		);

		if ($arFields["MONETA_ENABLED"] == "Y")
		{
			$arRequiredTszhFields[] = "MONETA_EMAIL";
		}

		$arTszhDefValues = array(
			"INN" => array("7731122600", "123456789012", "1234567890"),
			"RSCH" => array("40704820100320030032", "0000 0000 0000 0000 0000"),
		);

		$arErrors = array();
		foreach ($arRequiredTszhFields as $field)
		{
			if (array_key_exists($field, $arFields) && strlen(trim($arFields[$field])) <= 0)
			{
			    if (!($arFields["ORGANIZATION_TYPE"] == "1" && $field == "KPP")) {
                    $arErrors[$field] = GetMessage("CITRUS_TSZH_ERROR_EMPTY_FIELD", array("#FIELD#" => GetMessage("CITRUS_TSZH_F_{$field}")));
                }
			}
		}

		foreach ($arTszhDefValues as $field => $arValues)
		{
			if (!array_key_exists($field, $arErrors) && array_key_exists($field, $arFields) && in_array(trim($arFields[$field]), $arValues))
			{
				$arErrors[$field] = GetMessage("CITRUS_TSZH_ERROR_DEFAULT_FIELD", array("#FIELD#" => GetMessage("CITRUS_TSZH_F_{$field}")));
			}
		}

		// ���
		$field = "INN";
		if (!array_key_exists($field, $arErrors) && !CTszhFunctionalityController::isUkrainianVersion())
		{
			$v = strval($arFields[$field]);
			if ($arFields["ORGANIZATION_TYPE"] == "1") {

                if (!preg_match('/^[0-9]{12}$/', $v))
                {
                    $arErrors[$field] = GetMessage("CITRUS_TSZH_ERROR_INCORRECT_IP_INN");
                }
                elseif (!self::checkINN($arFields[$field]))
                {
                    $arErrors[$field] = GetMessage("CITRUS_TSZH_ERROR_INCORRECT_INN_CHECK");
                }

            }elseif ($arFields["ORGANIZATION_TYPE"] == "2" || $arFields["ORGANIZATION_TYPE"] == "3") {

                if (!preg_match('/^[0-9]{10}$/', $v)) {
                    $arErrors[$field] = GetMessage("CITRUS_TSZH_ERROR_INCORRECT_INN");
                } elseif (!self::checkINN($arFields[$field])) {
                    $arErrors[$field] = GetMessage("CITRUS_TSZH_ERROR_INCORRECT_INN_CHECK");
                }

            }
		}

        // ���
        $field = "KPP";
        if (!array_key_exists($field, $arErrors) && !CTszhFunctionalityController::isUkrainianVersion())
        {
            $v = strval($arFields[$field]);
            if ($arFields["ORGANIZATION_TYPE"] == "2" || $arFields["ORGANIZATION_TYPE"] == "3") {

                if (!preg_match('/^[0-9]{9}$/', $v))
                {
                    $arErrors[$field] = GetMessage("CITRUS_TSZH_ERROR_INCORRECT_KPP");
                }

            }
        }

		// ��������� ����
		$field = "RSCH";
		if (!array_key_exists($field, $arErrors))
		{
			$v = strval($arFields[$field]);
			$regExp = '/^[0-9]{20}$/';
			if (CTszhFunctionalityController::isUkrainianVersion())
			{
				$regExp = '/^[0-9]{10,14}$/';
			}

			if (!preg_match($regExp, $v))
			{
				$arErrors[$field] = GetMessage("CITRUS_TSZH_ERROR_INCORRECT_RSCH");
			}
		}

		// ���
		$field = "BIK";
		if (!array_key_exists($field, $arErrors) && !CTszhFunctionalityController::isUkrainianVersion())
		{
			$v = strval($arFields[$field]);
			$regExp = '/^[0-9]{9}$/';

			if (!preg_match($regExp, $v))
			{
				$arErrors[$field] = GetMessage("CITRUS_TSZH_ERROR_INCORRECT_BIK");
			}
		}

		// ��������� ����
		$field = "RSCH";
		if (!array_key_exists($field, $arErrors) && !array_key_exists("BIK", $arErrors))
		{
			if (!self::checkRsch($arFields[$field], $arFields['BIK']))
			{
				$arErrors[$field] = GetMessage("CITRUS_TSZH_ERROR_INCORRECT_RSCH_CHECK");
			}
		}

		if ($arFields["MONETA_ENABLED"] == "Y")
		{
			$field = 'MONETA_EMAIL';
			if (!array_key_exists($field, $arFields) && !self::checkEmail($arFields[$field]))
			{
				$arErrors[$field] = GetMessage("CITRUS_TSZH_ERROR_INCORRECT_MONETA_EMAIL");
			}
		}

		// ����� �����
		$arSites = array();
		$rs = CSite::GetList($by = 'id', $order = 'asc', array("ACTIVE" => "Y"));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($ar = $rs->Fetch())
		{
			$arSites[$ar["ID"]] = $ar;
		}
		$mainServerName = empty($arSites[$arFields["SITE_ID"]]["SERVER_NAME"]);
		$serverName = $mainServerName ? COption::GetOptionString("main", "server_name", $_SERVER['HTTP_HOST']) : $arSites[$arFields["SITE_ID"]]["SERVER_NAME"];
		$siteUrl = (CMain::IsHTTPS() ? "https" : "http") . '://' . $serverName;
		$validUrl = preg_match('#^(http|https)://([A-Z0-9][A-Z0-9_-]*(?:.[A-Z0-9][A-Z0-9_-]*)+):?(d+)?/?$#i', $siteUrl) === 1;
		if (!$validUrl)
		{
			$editUrl = $mainServerName ? BX_ROOT . '/admin/settings.php?lang=' . LANG . '&mid=main&mid_menu=1' : BX_ROOT . '/admin/site_edit.php?lang=' . LANG . '&LID=' . $arFields["SITE_ID"] . '&tabControl_active_tab=edit1';
			$arErrors["URL"] = GetMessage($mainServerName ? "CITRUS_TSZH_ERROR_INVALID_URL" : "CITRUS_TSZH_ERROR_INVALID_SITE_URL", array("#URL#" => $editUrl));
		}

		// �������� �� ������������ ���������� ������ ����� �������� (���� ���� ����)
		// ������ �� ������������ �������� www
		$httpHost = preg_replace('#^www\.#i', '', $_SERVER["HTTP_HOST"]);
		$serverName = preg_replace('#^www\.#i', '', $serverName);
		// ������ �� ������������ ���� :80
		$httpHost = preg_replace('#:80$#', '', $httpHost);
		$serverName = preg_replace('#:80^#', '', $serverName);
		// ������� ������ ������� ��� �������� �����
		$arD = explode("\n", $arSites[$arFields["SITE_ID"]]["DOMAINS"]);
		$arDomains = array();
		foreach ($arD as $domain)
		{
			$arDomains[] = \CBXPunycode::ToASCII(trim($domain), $errors);
		}
		if (count($arSites) == 1 && !empty($_SERVER["HTTP_HOST"]) && !in_array($httpHost, $arDomains) && $serverName !== $httpHost)
		{
			$editUrl = $mainServerName ? BX_ROOT . '/admin/settings.php?lang=' . LANG . '&mid=main&mid_menu=1' : BX_ROOT . '/admin/site_edit.php?lang=' . LANG . '&LID=' . $arFields["SITE_ID"] . '&tabControl_active_tab=edit1';
			$arErrors["URL"] = GetMessage($mainServerName ? "CITRUS_TSZH_ERROR_INVALID_URL" : "CITRUS_TSZH_ERROR_INVALID_SITE_URL", array("#URL#" => $editUrl)) . "<br>" . GetMessage("CITRUS_TSZH_ERROR_WRONG_URL", array(
					"#URL#" => $serverName,
					"#CURRENT_URL#" => $httpHost
				));
		}

		if (self::hasDemoAccounts($ID))
		{
			$arErrors["URL"] = GetMessage("CITRUS_TSZH_ERROR_DEMO_DATA");
		}

		self::$validationErrors = $arErrors;

		if (empty($arErrors))
		{
			CAdminNotify::DeleteByTag("citrus.tszh.validation." . $ID);
		}
		else
		{
			CAdminNotify::Add(Array(
				"MESSAGE" => GetMessage("CITRUS_TSZH_ERORR_NOTIFY", Array("#ID#" => $ID, "#NAME#" => $arFields["~NAME"])),
				"TAG" => "citrus.tszh.validation." . $ID,
				"MODULE_ID" => "citrus.tszh",
				"ENABLE_CLOSE" => "N"
			));
		}

		return empty($arErrors);
	}

	/**
	 * ������ URL ���������� �����.
	 * � ������ ���� �������� $siteId �� ������ ��� ������������� �-�� � ���������������� ������� (����� ������� ���� ���������� �� ��������), ����� ������� ����������
	 *
	 * @param bool $siteId ID �����, ��� �������� ����� �������� URL (�� ��������� - ������� ����)
	 *
	 * @return string ������ URL �����
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getSiteUrl($siteId = false)
	{
		if (false === $siteId)
		{
			if (defined('ADMIN_SECTION'))
			{
				throw new \Bitrix\Main\ArgumentException('siteId must be specified explicitly for use in admin section.');
			}
			$siteId = SITE_ID;
		}
		$site = CSite::GetByID($siteId)->Fetch();
		$serverName = empty($site["SERVER_NAME"]) ? COption::GetOptionString("main", "server_name", $_SERVER['HTTP_HOST']) : $site["SERVER_NAME"];
		$siteUrl = (CMain::IsHTTPS() ? "https" : "http") . '://' . $serverName;

		return $siteUrl;
	}

	/**
	 * ��������� true ���� ���� �������� � ����������� ���������� ������� ����������
	 * � ������� �� ����� ������, ��������� ���� ����� ����� ������������ ������������ ������, � ����� ������ �� ����� �������������� ������� ���������� ��������� ��������������.
	 *
	 * @param $ID
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function isDirty($ID)
	{
		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		$rsNotify = CAdminNotify::GetList(Array(), Array("TAG" => "citrus.tszh.validation." . $ID));
		if (!is_object($rsNotify))
		{
			trigger_error('Unexpected result', E_USER_WARNING);

			return true;
		}

		return ($rsNotify->SelectedRowsCount() > 0);
	}

	/**
	 * ����� ��� ������������� �������� ������������ ����� ���������� ���� �������� ���������� � �������� ����������� �������������� � ������ ������
	 *
	 * @return string
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function checkAgent()
	{
		$rsTszh = self::GetList(Array("ID" => "ASC"), Array(), false, false, Array("*"));
		$arErrorsByMail = $cc = $tszhList = Array();
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arTszh = $rsTszh->Fetch())
		{
			if (!self::postValidate($arTszh["ID"]))
			{
				$tszhList[$arTszh["ID"]] = $arTszh;
				$email = COption::GetOptionString("main", "email_from", false);
				$site = CSite::GetByID($arTszh['SITE_ID'])->Fetch();
				if (is_array($site) && $site['EMAIL'])
				{
					$cc[$email][] = $site["EMAIL"];
				}
				if (self::checkEmail($email))
				{
					$arErrorsByMail[$email][$arTszh["ID"]] = self::$validationErrors;
				}
			}
		}
		if (!empty($arErrorsByMail))
		{
			foreach ($arErrorsByMail as $email => $arTszhErrors)
			{
				$copies = array_unique($cc[$email]);
				$serverName = SITE_SERVER_NAME ? SITE_SERVER_NAME : (COption::GetOptionString('main', 'server_name', $_SERVER['HTTP_HOST']));
				$subject = GetMessage("CITRUS_TSZH_ERROR_EMAIL_SUBJECT") . " " . $serverName;
				$message = GetMessage("CITRUS_TSZH_ERROR_EMAIL_BODY");
				$headers = array('Content-type: text/plain; charset=' . SITE_CHARSET, "Content-Transfer-Encoding: 8bit");
				foreach ($arTszhErrors as $tszhId => $errors)
				{
					$tszh = $tszhList[$tszhId];
					$message .= $tszh["NAME"] . " (http://$serverName/bitrix/admin/tszh_edit.php?ID={$tszhId}&lang=ru):\n\n" . strip_tags(preg_replace('/\<br(\s*)?\/?\>/i', PHP_EOL, implode("\n", $errors))) . "\n\n";
				}
				bxmail($email, $subject, $message, count($headers) ? implode(CEvent::GetMailEOL(), $headers) : "", count($copies) ? "CC: " . implode(',', $copies) : "");
			}
		}

		return __CLASS__ . '::' . __FUNCTION__ . '();';
	}

	/**
	 * ������������ ��� ����������� ������� ���������������� �����, ����� ��� �� ���������� ����� ���������� ������ ��� �� �������
	 *
	 * @param array $fields ���� ��������
	 *
	 * @return bool ���������� true, ���� � ����� �������� ������������ �������� ���������������� �����
	 */
	public static function hasUserFields($fields)
	{
		foreach ($fields as $field => $value)
		{
			if (strpos($field, 'UF_') === 0)
			{
				return true;
			}
		}

		return false;
	}


	/**
	 * ����� ����������� ��������� ��� �������� ������ ������ ��������/��������
	 * ����� �������� ������������ ���������� �������� ���� ������� �������� � ���������� �������� ������ (������� ������� �������)
	 */
	public static function cleanupLogFilesAgent()
	{
		$cleanupDays = COption::GetOptionInt("main", "event_log_cleanup_days", 7);
		if ($cleanupDays > 0)
		{
			$deleteOlderThan = strtotime("-$cleanupDays days");
			$pathName = $_SERVER['DOCUMENT_ROOT'] . "/" . COption::GetOptionString("main", "upload_dir", "upload") . "/tszh_exchange/";

			if (file_exists($pathName) && is_dir($pathName))
			{
				$di = new RecursiveDirectoryIterator($pathName);
				foreach (new RecursiveIteratorIterator($di) as $filename => $file)
				{
					/** @var SplFileInfo $file */
					if (
						$file->isFile()
						&& $file->getFilename() !== '.htaccess'
						&& $file->isWritable()
						&& $file->getMTime() < $deleteOlderThan
					)
					{
						unlink($filename);
					}
				}
			}
		}

		return __CLASS__ . '::' . __FUNCTION__ . '();';
	}

	/**
	 * ����������, ������� �� ��������� ��� ���������������� ������� �����
	 *
	 * @param int $tszhId ID ������� ����������
	 * @param array|null $arDemoAccounts ���� �������� ���������, �� � ��� ����� ��������� ������ ������� ������ �� ����-������
	 *
	 * @return bool true ���� ����� ������� ������ ������� ���������� ���� ����-������
	 */
	public static function hasDemoAccounts($tszhId, &$arDemoAccounts = null)
	{
		// ��������, ��������� �� �/� �� ������� ����-���������
		$arDemoAccounts = array();
		$dbAccount = CTszhAccount::GetList(array(), array("TSZH_ID" => $tszhId, "@EXTERNAL_ID" => self::$demoAccounts), false, false, array(
			"ID",
			"EXTERNAL_ID"
		));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($account = $dbAccount->Fetch())
		{
			$arDemoAccounts[$account["ID"]] = $account;
		}

		return !empty($arDemoAccounts);
	}

	/**
	 * ���������� ������ Id ���, ����������� ���� �� ���� �������� (�� ����������������) ������� ����
	 *
	 * @return array
	 */
	public static function realAccountsTszhs()
	{
		$arTszhIds = array();
		$dbAccount = CTszhAccount::GetList(
			array(),
			array("!@EXTERNAL_ID" => self::$demoAccounts),
			array("TSZH_ID")
		);
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($account = $dbAccount->Fetch())
		{
			$arTszhIds[] = $account["TSZH_ID"];
		}

		return $arTszhIds;
	}

	/**
	 * ��������� �������� �� ��������� ������ ���������� ���������������� ������� �����
	 *
	 * @param int $tszhId ID ������� ����������
	 *
	 * @return bool true ���� ������ ���������� �������� ����-������
	 */
	public static function hasDemoDataOnly($tszhId)
	{
		$tszh = self::GetByID($tszhId);

		// ��� ��������� � ����-�������
		if (in_array($tszh["INN"], array("7731122600", "123456789012")))
		{
			return true;
		}

		// ���� ���-�� �/� ��������� � ����-������� � ����� ��� ���� ����������������
		if (36 == CTszhAccount::GetList(array(), array("TSZH_ID" => $tszhId), array()))
		{
			return self::hasDemoAccounts($tszhId);
		}

		return false;
	}
}