<?php

/**
 * ���������� �������������� ��������� ����������� ��������
 *
 * ��������� ���� BILLING � CONTRACTOR_BILLING �� ������ �������� ����� ����������, ���� ���� BILLING �� �������
 */
class CTszhContractorResult extends CDBResult
{
	/**
	 * ��������� �������� $arResult['BILLING'] (��� $arResult['CONTRACTOR_BILLING']) �� ������ ������ ����������
	 *
	 * @param array $arResult
	 */
	private function fillBillingField(&$arResult)
	{
		$prefix = array_key_exists('CONTRACTOR_BILLING', $arResult) ? 'CONTRACTOR_' : '';
		$template = GetMessage("CITRUS_TSZH_CONTRACTORS_BILLING_TEMPLATE");
		if (strlen($prefix))
		{
			$template = preg_replace('/#([^#]+)#/i', $prefix . '$1', $template);
		}
		$arResult[$prefix . "BILLING"] = str_replace(array_keys($arResult), array_values($arResult), $template);
	}

	/**
	 * @param bool $bTextHtmlAuto
	 * @param bool $use_tilda
	 *
	 * @return array
	 */
	public function GetNext($bTextHtmlAuto = true, $use_tilda = true)
	{
		$arResult = parent::GetNext($bTextHtmlAuto, $use_tilda);

		if (is_array($arResult))
		{
			$this->fillBillingField($arResult);
		}

		return $arResult;
	}

	/**
	 * @param bool $bSetGlobalVars
	 * @param string $strPrefix
	 * @param bool $bDoEncode
	 * @param bool $bSkipEntities
	 *
	 * @return array
	 */
	public function NavNext($bSetGlobalVars = true, $strPrefix = "str_", $bDoEncode = true, $bSkipEntities = true)
	{
		$arResult = parent::NavNext($bSetGlobalVars, $strPrefix, $bDoEncode, $bSkipEntities);

		$this->fillBillingField($arResult);

		return $arResult;
	}
}
