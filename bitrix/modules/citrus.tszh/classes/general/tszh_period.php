<?php
IncludeModuleLangFile(__FILE__);

/**
 * CTszhPeriod
 *
 * ����:
 *  ID - ID �������
 *  DATE - ���� �������
 *  TIMESTAMP_X - ���� ��������� �������
 *
 * @package
 * @author
 * @copyright nook.yo
 * @version 2010
 * @access public
 */
class CTszhPeriod
{

	/**
	 * ����������� ������ ��� ������ ������������
	 *
	 * @param string $sPeriod �������� �������
	 *
	 * @return string ������ � ����������������� ����
	 */
	public static function Format($sPeriod)
	{
		$months = Array(
			1 => GetMessage("TSZH_MONTH_1"),
			GetMessage("TSZH_MONTH_2"),
			GetMessage("TSZH_MONTH_3"),
			GetMessage("TSZH_MONTH_4"),
			GetMessage("TSZH_MONTH_5"),
			GetMessage("TSZH_MONTH_6"),
			GetMessage("TSZH_MONTH_7"),
			GetMessage("TSZH_MONTH_8"),
			GetMessage("TSZH_MONTH_9"),
			GetMessage("TSZH_MONTH_10"),
			GetMessage("TSZH_MONTH_11"),
			GetMessage("TSZH_MONTH_12"),
		);

		$arPeriod = explode('-', $sPeriod);
		$strResult = $months[intval($arPeriod[1])] . ' ' . $arPeriod[0];

		//$strResult = ToUpper(substr($strResult, 0, 1)) . substr($strResult, 1);
		return $strResult;
	}

	/**
	 * ����� ���������� ������������� ������ ����� ������� � ��������� ID
	 *
	 * @param int $ID
	 *
	 * @return bool|array
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function GetByID($ID)
	{
		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentException("Incorrect ID given", "ID");
		}

		if (isset($GLOBALS["TSZH_PERIOD"]["CACHE_" . $ID]) && is_array($GLOBALS["TSZH_PERIOD"]["CACHE_" . $ID]) && is_set($GLOBALS["TSZH_PERIOD"]["CACHE_" . $ID], "ID"))
		{
			return $GLOBALS["TSZH_PERIOD"]["CACHE_" . $ID];
		}
		else
		{
			$arPeriod = self::GetList(Array(), Array("ID" => $ID), false, Array('nTopCount' => 1))->GetNext(false, false);
			if (is_array($arPeriod))
			{
				$GLOBALS["TSZH_PERIOD"]["CACHE_" . $arPeriod['ID']] = $arPeriod;
				$GLOBALS["TSZH_PERIOD"]["DATE_CACHE_" . $arPeriod['DATE']] = $arPeriod;

				return $arPeriod;
			}
		}

		return false;
	}

	/**
	 * ���������� ���� ������� �� ��������� ����
	 *
	 * @param string $sDate ���� � ������� YYYY-MM-DD, YYYY-MM%, YYYY%
	 *
	 * @return bool|array
	 */
	public static function GetByDate($sDate, $tszhId = false)
	{
		global $DB;

		$sDate = $DB->ForSql(trim($sDate));
		if (strlen($sDate) <= 0)
		{
			return false;
		}

		if (isset($GLOBALS["TSZH_PERIOD"]["DATE_CACHE_" . $sDate]) && is_array($GLOBALS["TSZH_PERIOD"]["DATE_CACHE_" . $sDate]) && is_set($GLOBALS["TSZH_PERIOD"]["DATE_CACHE_" . $sDate], "ID"))
		{
			return $GLOBALS["TSZH_PERIOD"]["DATE_CACHE_" . $sDate];
		}
		else
		{
			$arFilterPeriod = Array("%DATE" => $sDate);
			if (IntVal($tszhId)>0)
			{
				$arFilterPeriod["TSZH_ID"] = $tszhId;
			}
			$arPeriod = self::GetList(Array(), $arFilterPeriod, false, Array('nTopCount' => 1))->GetNext(false, false);
			if (is_array($arPeriod))
			{
				$GLOBALS["TSZH_PERIOD"]["CACHE_" . $arPeriod['ID']] = $arPeriod;
				$GLOBALS["TSZH_PERIOD"]["DATE_CACHE_" . $arPeriod['DATE']] = $arPeriod;

				return $arPeriod;
			}
		}

		return false;
	}

	/**
	 * ��������� ������������ ��������� ����� ������� ����������
	 *
	 * @param array $arFields
	 * @param int $ID
	 * @param string $strOperation
	 *
	 * @return bool
	 */
	public static function CheckFields(&$arFields, $ID = 0, $strOperation = 'ADD')
	{

		global $APPLICATION;

		if (array_key_exists('TSZH_ID', $arFields) || $strOperation == 'ADD')
		{
			$arFields['TSZH_ID'] = IntVal($arFields['TSZH_ID']);
			if ($arFields['TSZH_ID'] <= 0)
			{
				$APPLICATION->ThrowException(GetMessage("TSZH_ERROR_NO_TSZH_ID"), "TSZH_ERROR_NO_TSZH_ID");

				return false;
			}
		}

		if (isset($arFields['ACTIVE']))
		{
			$arFields['ACTIVE'] = $arFields['ACTIVE'] != 'N' ? 'Y' : 'N';
		}
		elseif ($strOperation == 'ADD')
		{
			$arFields["ACTIVE"] = "Y";
		}

		if (isset($arFields['ONLY_DEBT']))
		{
			$arFields['ONLY_DEBT'] = $arFields['ONLY_DEBT'] != 'Y' ? 'N' : 'Y';
		}
		elseif ($strOperation == 'ADD')
		{
			$arFields["ONLY_DEBT"] = "N";
		}

		unset($arFields['ID']);
		unset($arFields['TIMESTAMP_X']);

		return true;
	}

	/**
	 * ��������� ����� ������.
	 * � ������ ������������� ������, �� ����� ����� �������� � ������� $APPLICATION->GetException()
	 *
	 * @param array $arFields
	 *
	 * @return bool
	 */
	public static function Add($arFields)
	{
		global $DB;

		if (!self::CheckFields($arFields))
		{
			return false;
		}

		$ID = $DB->Add("b_tszh_period", $arFields);

		return $ID;
	}

	/**
	 * ������������� ����� ���� �������
	 * � ������ ������������� ������, �� ����� ����� �������� � ������� $APPLICATION->GetException()
	 *
	 * @param int $ID
	 * @param array $arFields
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Update($ID, $arFields)
	{

		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (!self::CheckFields($arFields, $ID, 'UPDATE'))
		{
			return false;
		}

		$strUpdate = $DB->PrepareUpdate("b_tszh_period", $arFields);
		$strSql = "UPDATE b_tszh_period SET $strUpdate WHERE ID=$ID";
		$DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

		return true;
	}

	/**
	 * ������� ������ � ��������� ID.
	 * ����� ��������� ��������� �� ��������� ������ ������ �� ����� ���������� �������
	 *
	 * @param int $ID
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Delete($ID)
	{
		global $DB;
		$ID = intval($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		// ��������� ������� ����� API ����� ��������� ���������� ��������� � ���� ������
		$rs = CTszhAccountPeriod::GetList(Array(), Array("PERIOD_ID" => $ID), false, false, Array("ID"));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($ar = $rs->Fetch())
		{
			CTszhAccountPeriod::Delete($ar['ID']);
		}

		$strSql = "DELETE FROM b_tszh_period WHERE ID=$ID";
		$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		return true;
	}

	/**
	 * ���������� ������� ��������
	 * ���������� ������ CDBResult � ������������, ���� ���������� ��������� ������� ��� ������������� �����������
	 *
	 * @param array $arOrder
	 * @param array $arFilter
	 * @param bool $arGroupBy
	 * @param bool $arNavStartParams
	 * @param array $arSelectFields
	 *
	 * @return bool|CDBResult
	 */
	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{

		static $arFields = array(
			"ID" => Array("FIELD" => "TP.ID", "TYPE" => "int"),
			"TSZH_ID" => Array("FIELD" => "TP.TSZH_ID", "TYPE" => "int"),
			"DATE" => Array("FIELD" => "TP.DATE", "TYPE" => "string"),
			"MONTH" => Array("FIELD" => "LEFT(TP.DATE,7)", "TYPE" => "string"),
			"TIMESTAMP_X" => Array("FIELD" => "TP.TIMESTAMP_X", "TYPE" => "string"),
			"ACTIVE" => Array("FIELD" => "TP.ACTIVE", "TYPE" => "char"),
			"ONLY_DEBT" => Array("FIELD" => "TP.ONLY_DEBT", "TYPE" => "char"),
		);

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = Array(
				"ID",
				"TSZH_ID",
				"DATE",
				"MONTH",
				"TIMESTAMP_X",
				"ACTIVE",
				"ONLY_DEBT",
			);
		}
		elseif (in_array('*', $arSelectFields))
		{
			$arSelectFields = array_keys($arFields);
		}

		return CTszh::GetListMakeQuery('b_tszh_period TP', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

	}

	/**
	 * ��������� ���� ���������� (�� ����) ��������� �������
	 *
	 * @param bool|int $tszhId ID ������� ���������� (���� �� ������ ��������� �� �������������� �� ����� ����)
	 *
	 * @return array|bool
	 */
	public static function GetLast($tszhId = false)
	{
		global $DB;

		static $arLastPeriodCache = Array();

		if (!array_key_exists($tszhId, $arLastPeriodCache))
		{
			$strWhere = IntVal($tszhId) > 0 ? 'TP.TSZH_ID=' . IntVal($tszhId) . ' AND' : '';
			$strSql = "SELECT TP.ID AS ID FROM b_tszh_period TP WHERE {$strWhere} TP.ACTIVE='Y' ORDER BY TP.DATE DESC, TP.ID DESC LIMIT 1;";
			$dbResult = $DB->Query($strSql);
			/** @noinspection PhpAssignmentInConditionInspection */
			if ($arResult = $dbResult->Fetch())
			{
				$arLastPeriodCache[$tszhId] = $arResult['ID'];
			}
			else
			{
				$arLastPeriodCache[$tszhId] = false;
			}
		}

		return $arLastPeriodCache[$tszhId];
	}
}