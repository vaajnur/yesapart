<?

IncludeModuleLangFile(__FILE__);

define('TSZH_IMPORT_MAX_ERRORS', 100);
//define('TSZH_IMPORT_DEBUG', true);

/**
 * �������� ������ ��� ��������� ������ � ������� ������� ������ �� 1�
 * http://wiki.citrus-soft.ru/dev/import-accounts
 */
class CTszhImport
{
	/** @var string NEW_EXTERNAL_ID_SEPARATOR ������������ ��� ���������� �������� (�����������) � ���� �������� ����� � �������� ORG->PersAcc->kod_ls_new */
	const NEW_EXTERNAL_ID_SEPARATOR = " ";

	/** @var array ������ ��� ���������� ����� ������ � ������ */
	protected $state;

	/** @var int ID ������� ���������� */
	protected $tszhID = false;

	/** @var string ID ����� ������� ���������� */
	protected $siteID = false;

	/** @var string ��� ������� */
	protected $sPeriod = false;

	/** @var int ID ������� */
	protected $periodID = false;

	protected $updateMode = false;
	protected $onlyDebt = false;
	protected $createUsers = false;
	protected $depersonalize = false;
	protected $arTszh = Array();
	protected $createTszh = false;
	protected $tszhName = false;
	protected $inn = false;

	/** @var array �������� ���� ���������, ������� ���� ������� �� ���� ���� (������ ������� 1�, ������ �16052) */
	var $deletedMeters = array();

	/** @var int ������ ����� �� XML */
	var $version = 1;

	/** @var bool ������������ �� ������� ����� �������� ��������� (� ������ ������� ������ ������ �������� ��������� ��������� ���������� ��� 0, ���� ���� ������� ���������� -- ������ �������� �������� ����� ������������� ��� ������) */
	var $strict = false;

	/** @var string ������� ������ �� XML (http://wiki.citrus-soft.ru/dev/import-accounts#��������-���������_��������-������) */
	var $filetype = "accounts";

	/** @var array ������� ���� �� ���������� X � ��������� */
	var $xFields = array();

	/**
	 * @var string ��� ������� � ������� XML-�����
	 */
	protected $tableName;

	/**
	 * @var bool �������� ��������� ������ ��� ����������� ������������ ��������� ���������� ������
	 */
	protected $useSessions;

	/**
	 * @var string � ������ ������������� ������ �������� SQL-������� �������������� � ������� ������
	 */
	protected $andWhereThisSession;

	/**
	 * @var string
	 */
	protected $sessionId;

	/**
	 * @var CIBlockXMLFile
	 */
	protected $xmlFile;

	/**
	 * @var int ����� �� ����� ���������� ������ ���� � ��������
	 */
	protected $timeLimit;

	/**
	 * @var float Timestamp ������� ������ ���������
	 */
	protected $startTime = null;

	/**
	 * @param array $state ���������� ��� �������� �������� ���������, �������� ������ ����������� ����� ������ �������� (������)
	 * @param int $timeLimit ����� �� ����� ���������� ������ ���� � ��������
	 * @param bool $useSessions �������� ��������� ������ ��� ����������� ������������ ��������� ���������� ������
	 * @param string $tableName ��� ������� � ������� XML-�����
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	function __construct(&$state, $timeLimit = 0, $useSessions = false, $tableName = 'b_tszh_xml_storage')
	{
		if (strlen(trim($tableName)) == 0)
		{
			throw new \Bitrix\Main\ArgumentException("\$tableName must contain a non-empty string");
		}
		if (!isset($state) || !is_array($state))
		{
			throw new \Bitrix\Main\ArgumentException('Array variable must be passed in $state parameter');
		}

		$this->useSessions = $useSessions;
		$this->tableName = $tableName . ($useSessions ? '_m' : '');
		$this->state = &$state;

		$this->setStartTime();

		if ($timeLimit > 0)
		{
			$this->timeLimit = $timeLimit;
		}
		else
		{
			$this->timeLimit = 368 * 24 * 60 * 60;
		} // ���� ��� ;)

		// TODO �������� �������� ��� ������ ���������
		CModule::IncludeModule("iblock");

		$this->xmlFile = new \CIBlockXMLFile($this->tableName);
		if ($this->useSessions)
		{
			$this->sessionId = substr(session_id(), 0, 32);
			if ($this->state["tablesCreated"])
			{
				// ������� ������ ������ � ������� ������
				// $this->clearSession();
			}
			$this->xmlFile->StartSession($this->sessionId);
			$this->state["tablesCreated"] = true;
		}
		elseif (!isset($this->state["tablesCreated"]))
		{
			$this->xmlFile->DropTemporaryTables();
			$this->xmlFile->CreateTemporaryTables();
			$this->state["tablesCreated"] = true;
		}
		$this->andWhereThisSession = $this->useSessions ? " and SESS_ID='" . $this->sessionId . "'" : '';

		if ($this->state["PERIOD_ID"] > 0)
		{
			$this->periodID = $this->state["PERIOD_ID"];
		}
		if (array_key_exists('FILE_PERIOD', $this->state) && strlen($this->state["FILE_PERIOD"]) > 0)
		{
			$this->sPeriod = $this->state["FILE_PERIOD"];
		}
		$this->updateMode = &$this->state["UPDATE_MODE"];
		$this->onlyDebt = &$this->state["ONLY_DEBT"];
		$this->createUsers = &$this->state["CREATE_USERS"];
		//$this->updateUsers = &$this->next_step["UPDATE_USERS"];
		$this->depersonalize = $this->state["DEPERSONALIZE"];

		$this->version = &$this->state["VERSION"];
		$this->strict = &$this->state["STRICT"];
		$this->filetype = &$this->state["FILETYPE"];

		$this->tszhID = &$this->state["TSZH"];
		$this->siteID = &$this->state["SITE_ID"];

		$this->createTszh = &$this->state["CREATE_TSZH"];
		$this->tszhName = &$this->state["TSZH_NAME"];
		$this->inn = &$this->state["INN"];

		if (intval($this->tszhID) > 0)
		{
			$this->arTszh = CTszh::GetByID($this->tszhID);
		}
	}

	/**
	 * @param $NS
	 * @deprecated ����������� �����������
	 */
	public function Init(&$NS)
	{

	}

	/**
	 * ������� � �������� ������ �� XML � ����
	 * �������� � ������������ �� �������
	 *
	 * @param resource $fp
	 * @return bool true ���� ���� ��� �������� ��������� � false ���� ���������� ���������� �������� � ������� ����� ��� ���
	 * @throws \Bitrix\Main\ArgumentTypeException
	 */
	public function readXml($fp)
	{
		if (!is_resource($fp))
		{
			throw new \Bitrix\Main\ArgumentTypeException('Argument $fp must contain valid resouce');
		}

		$done = $this->xmlFile->ReadXMLToDatabase($fp, $this->state, $this->timeLimit);
		// �������� ������� ����� ��������� �������� ������
		if ($done && !$this->useSessions)
		{
			$this->xmlFile->IndexTemporaryTables();
		}

		return $done;
	}

	/**
	 * @return CIBlockXMLFile
	 */
	public function getXml()
	{
		return $this->xmlFile;
	}

	/**
	 * ���������� ����� �� XML (�������� ��������, ������ ������� �� �����)
	 * @param string $value ������ � ������ �� XML
	 * @param bool $strict ���� == true, ������ ������ ����� ��������� false (� ��������� � ���� ��� null-��������), ����� ����� ����� 0
	 * @return float �������������� �����
	 */
	protected function formatNumber($value, $strict = false)
	{
		$val = preg_replace('/[^\d.\-]/', '', str_replace(',', '.', $value));
		return $strict && $val == '' ? false : floatval($val);
	}

	protected function resetX()
	{
		$this->xFields = array();
	}

	/**
	 * ��������� ���� � ������ ����� �� ��������� X
	 *
	 * @param string|array $field
	 */
	protected function addX($field)
	{
		if (is_array($field))
		{
			foreach ($field as $f)
			{
				$this->xFields[$f] = true;
			}
		}
		else
		{
			$this->xFields[$field] = true;
		}
	}

	/**
	 * ���������� ������ ����� (����� �������) �� ��������� X
	 *
	 * @return string
	 */
	protected function getX()
	{
		return implode(',', array_keys($this->xFields));
	}

	/**
	 * @param string $key �������� ����� (�������� ����)
	 * @param array $values ������ �������� ���������
	 * @param bool $number ���� true, �� ��������� ����� ��������� ��� ���� (������� ������ �������, �������� ������� �� �����)
	 * @param bool|string|array $field ��� ���� (� ���� ������ � API ������)
	 * @return bool|float|string
	 */
	protected function getValue($key, $values, $number = false, $field = false)
	{
		$value = array_key_exists($key, $values) ? $values[$key] : false;
		if (strtolower($value) == 'x')
		{
			if ($field)
			{
				$this->addX($field);
			}

			return false;
		}

		if ($this->strict)
		{
			if ($number && $value !== false)
			{
				return $this->formatNumber($value, $this->strict);
			}
			else
			{
				return $value;
			}
		}
		else
		{
			return $number ? $this->formatNumber($value) : trim($value);
		}
	}

	/**
	 * ��������� ���������������� �������
	 * @param string $entity �������� ���������������� �����
	 * @param string $prefix ������� � ��������� ���� XML (��������, uf_ ��� puf_)
	 * @param array $arAttrs ������ ��������� �� XML
	 * @param array $arFields ������ �������������� ����� ��� ����������/����������
	 */
	protected function processUserFields($entity, $prefix, $arAttrs, &$arFields)
	{
		static $arNumericFields = Array();

		// �������� ����, ��� ������� ��������� �������������� ��������� � XML
		if (!array_key_exists($entity, $arNumericFields))
		{
			$arNumericFields[$entity] = Array();
			$rs = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID" => $entity));
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($ar = $rs->GetNext())
			{
				if (in_array($ar['USER_TYPE_ID'], Array("integer", "double")))
				{
					$arNumericFields[$entity][$ar['FIELD_NAME']] = true;
				}
			}
		}

		$prefix = ToLower($prefix);
		$prefixLen = strlen($prefix);
		$prefixLen = $prefixLen > 0 ? $prefixLen : false;

		foreach ($arAttrs as $attrName => $attrValue)
		{
			// ���������� ������ ���� � ��������� ���������
			if ($prefixLen)
			{
				if (strlen($attrName) > $prefixLen && ToLower(substr($attrName, 0, $prefixLen)) == $prefix)
				{
					$fieldName = strtoupper(str_replace($prefix, 'UF_', $attrName));
					$fieldValue = $attrValue;
					if (array_key_exists($fieldName, $arNumericFields[$entity]))
					{
						$fieldValue = self::formatNumber($attrValue);
					}
					$arFields[strtoupper($fieldName)] = $fieldValue;
				}
			}
			else
			{
				$fieldName = strtoupper('UF_' . $attrName);
				$fieldValue = $attrValue;
				if (array_key_exists($fieldName, $arNumericFields[$entity]))
				{
					$fieldValue = self::formatNumber($attrValue);
				}
				$arFields[strtoupper($fieldName)] = $fieldValue;
			}
		}
	}

	/**
	 * �������� ������ ����� �������������
	 * ������: pluralForm(5, '�����', '�����', '������');
	 *
	 * @param int $n
	 * @param string $f1
	 * @param string $f2
	 * @param string $f5
	 * @return string
	 */
	protected function pluralForm($n, $f1, $f2, $f5)
	{
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20) return $f5;
		if ($n1 > 1 && $n1 < 5) return $f2;
		if ($n1 == 1) return $f1;
		return $f5;
	}

	/**
	 * ���������� ����� ������ � ������� ������ ������
	 *
	 * @return bool|string
	 */
	public static function GetErrors()
	{
		if (count($_SESSION['TSZH_IMPORT_ERRORS']) > 0)
		{
			return implode('<br />', $_SESSION['TSZH_IMPORT_ERRORS']);
		}
		return false;
	}

	public static function ResetErrors()
	{
		$_SESSION['TSZH_IMPORT_ERRORS'] = Array();
	}

	/**
	 * @param string $strError
	 * @param bool|array $arXMLElement
	 */
	protected function ImportError($strError, $arXMLElement = false)
	{
		global $APPLICATION;

		/** @noinspection PhpAssignmentInConditionInspection */
		if ($ex = $APPLICATION->GetException())
		{
			$strError .= ': ' . $ex->GetString();
		}

		$strError = strip_tags($strError);

		if (count($_SESSION['TSZH_IMPORT_ERRORS']) < TSZH_IMPORT_MAX_ERRORS)
		{
			if (defined("TSZH_IMPORT_DEBUG") && is_array($arXMLElement) && !empty($arXMLElement))
			{
				$strError .= ' (' . var_export($arXMLElement, 1) . ')';
			}
			$_SESSION['TSZH_IMPORT_ERRORS'][] = $strError;
		}
		elseif (count($_SESSION['TSZH_IMPORT_ERRORS']) <= TSZH_IMPORT_MAX_ERRORS)
		{
			$_SESSION['TSZH_IMPORT_ERRORS'][] = GetMessage("TI_SHOWN_FIRST_ERRORS") . ' ' . TSZH_IMPORT_MAX_ERRORS . ' ' . CTszhImport::pluralForm(TSZH_IMPORT_MAX_ERRORS, GetMessage("TI_SHOWN_ERRORS1"), GetMessage("TI_SHOWN_ERRORS3"), GetMessage("TI_SHOWN_ERRORS5"));
		}

		$APPLICATION->ResetException();
	}

	/**
	 * ������ ��� ������� � ��������� ������ �������
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public function ProcessPeriod()
	{
		global $APPLICATION, $DB;

		$this->state["SERVICES"] = array();

		$xmlRootId = intval($this->useSessions ? $this->xmlFile->GetSessionRoot() : 1);
		$ar = $DB->Query("select ID, ATTRIBUTES from " . $this->tableName . " where ID = " . $xmlRootId . " and NAME='ORG'" . $this->andWhereThisSession)->Fetch();
		if (is_array($ar) && (strlen($ar["ATTRIBUTES"]) > 0))
		{
			$this->state["XML_ACCOUNTS_PARENT"] = $ar['ID'];
			$info = unserialize($ar["ATTRIBUTES"]);
			$lowerCaseInfo = array();
			foreach ($info as $key => $value)
				$lowerCaseInfo[ToLower($key)] = ToLower($value);

			// default attrs
			$arDefAttrs = array("filetype" => "accounts", "version" => 1);
			$lowerCaseInfo = array_merge($arDefAttrs, $lowerCaseInfo);

			// required attrs
			$arReqAttrs = array("inn", "filetype", "version", "filedate");
			$arMissingAttrs = array_diff($arReqAttrs, array_keys($lowerCaseInfo));
			if (!empty($arMissingAttrs))
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_MISSING_ORG_ATTRS", array("#ATTRS#" => implode(", ", $arMissingAttrs))));
				return false;
			}
			$arEmptyAttrs = array();
			foreach ($arReqAttrs as $attr)
			{
				if (strlen(trim($lowerCaseInfo[$attr])) <= 0)
				{
					$arEmptyAttrs[] = $attr;
				}
			}
			if (!empty($arEmptyAttrs))
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_EMPTY_ORG_ATTRS", array("#ATTRS#" => implode(", ", $arEmptyAttrs))));
				return false;
			}

			$this->version = $this->formatNumber($lowerCaseInfo["version"]);
			$this->strict = is_set($lowerCaseInfo, "strict") && $lowerCaseInfo["strict"];

			if ($this->version > TSZH_EXCHANGE_CURRENT_VERSION)
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_WRONG_VERSION"));
				return false;
			}

			// filetype
			$this->filetype = $lowerCaseInfo["filetype"];
			// �������� ������������ ��������� �������� ��������� version � filetype
			switch ($this->filetype)
			{
				case "accounts":
					break;

				case "access":
				case "calculations":
					if ($this->version < 3)
					{
						$APPLICATION->ThrowException(GetMessage("TI_ERROR_WRONG_VERSION_FILETYPE"));
						return false;
					}
					break;

				default:
					$APPLICATION->ThrowException(GetMessage("TI_ERROR_WRONG_FILETYPE"));
					return false;
			}

			// filedate
			$arFileDate = ParseDateTime($lowerCaseInfo['filedate'], "DD.MM.YYYY");
			if (is_array($arFileDate))
			{
				$this->sPeriod = $arFileDate['YYYY'] . '-' . $arFileDate['MM'] . '-' . $arFileDate['DD'];
				$this->state['FILE_PERIOD'] = $this->sPeriod;
			}
			else
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_WRONG_ORG_FILEDATE"));
				return false;
			}

			// inn
			if (!preg_match('/^[0-9]{10}$/', $lowerCaseInfo["inn"]) && !CTszhFunctionalityController::isUkrainianVersion())
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_WRONG_INN"));
				return false;
			}
			$arTszh = CTszh::GetList(
				array(),
				array("INN" => $lowerCaseInfo["inn"]),
				false,
				array("nTopCount" => 1)
			)->Fetch();
			if (is_array($arTszh) && !empty($arTszh) && intval($arTszh["ID"]) > 0)
			{
				$this->arTszh = $arTszh;
				$this->tszhID = $arTszh["ID"];
				$this->siteID = $arTszh["SITE_ID"];
			}
			elseif ($this->createTszh === "Y")
			{
				if (strlen($this->siteID) <= 0)
				{
					$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_CREATE_REQ_SITE_ID"));
					return false;
				}
				if (strlen($this->tszhName) <= 0)
				{
					$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_CREATE_REQ_NAME"));
					return false;
				}

				$arTszhFields = array(
					"SITE_ID" => $this->siteID,
					"NAME" => htmlspecialcharsBack($this->tszhName),
					"INN" => $this->inn,
				);
				$tszhID = CTszh::Add($arTszhFields);
				if ($tszhID > 0)
				{
					$this->arTszh = CTszh::GetByID($tszhID);
					$this->tszhID = $this->arTszh["ID"];
					$this->siteID = $this->arTszh["SITE_ID"];
				}
				else
				{
					$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_CREATE"));
					return false;
				}
			}
			elseif (!CModule::IncludeModule("vdgb.portaljkh"))
			{
				$this->siteID = "";
				$this->inn = htmlspecialcharsEx($lowerCaseInfo["inn"]);
				$this->tszhName = "";
				if (isset($info["name"]))
				{
					$this->tszhName = htmlspecialcharsEx($info["name"]);
				}
				$this->createTszh = "Q";
				// ���������� ��� �������� �� 1� - ������ ���������� ������
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_WITH_INN_NOT_EXISTS", array("#INN#" => $lowerCaseInfo["inn"])));
				return 1;
			}
			else
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_WITH_INN_NOT_EXISTS", array("#INN#" => $lowerCaseInfo["inn"])));
				return false;
			}

			if (count(array_intersect(Array('inn', 'kpp', 'rs', 'bank', 'ks', 'bik'), array_keys($info))) > 0)
			{
				$arUpdateOrg = Array();
				if (array_key_exists('inn', $info))
				{
					$arUpdateOrg['INN'] = $info['inn'];
				}
				if (array_key_exists('kpp', $info))
				{
					$arUpdateOrg['KPP'] = $info['kpp'];
				}
				if (array_key_exists('rs', $info))
				{
					$arUpdateOrg['RSCH'] = $info['rs'];
				}
				if (array_key_exists('bank', $info))
				{
					$arUpdateOrg['BANK'] = $info['bank'];
				}
				if (array_key_exists('ks', $info))
				{
					$arUpdateOrg['KSCH'] = $info['ks'];
				}
				if (array_key_exists('bik', $info))
				{
					$arUpdateOrg['BIK'] = $info['bik'];
				}
				CTszh::Update($this->tszhID, $arUpdateOrg);
			}
		}
		else
		{
			$APPLICATION->ThrowException(GetMessage("TI_ERROR_NO_ORG_ELEMENT"));
			return false;
		}

		// ������� ������ �� ������� ����������� �������?
		if (in_array($this->filetype, array("accounts", "calculations")))
		{
			if ($this->updateMode)
			{
				$rsPeriod = CTszhPeriod::GetList(Array("DATE" => "DESC"), Array("TSZH_ID" => $this->tszhID, "MONTH" => substr($this->sPeriod, 0, 7)));
				/** @noinspection PhpAssignmentInConditionInspection */
				if ($arPeriod = $rsPeriod->Fetch())
				{
					$this->periodID = $arPeriod['ID'];
					$this->state["PERIOD_ID"] = $this->periodID;
				}
				else
				{
					$this->updateMode = false;
				}
			}

			$arPeriodFields = Array(
				"DATE" => $this->sPeriod,
				"TSZH_ID" => $this->tszhID,
				"ACTIVE" => "Y",
				"ONLY_DEBT" => $this->onlyDebt ? "Y" : "N",
			);
			if ($this->updateMode)
			{
				CTszhPeriod::Update($this->periodID, $arPeriodFields);
			}
			else
			{
				$this->periodID = CTszhPeriod::Add($arPeriodFields);
				$this->state["PERIOD_ID"] = $this->periodID;
			}

			if ($this->periodID <= 0)
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_PERIOD_ADD_ERROR"));
				return false;
			}
		}

		if ($this->state["XML_ACCOUNTS_PARENT"])
		{
			$rs = $DB->Query("select count(*) C from " . $this->tableName . " where NAME='PersAcc' and PARENT_ID = " . intval($this->state["XML_ACCOUNTS_PARENT"]) . $this->andWhereThisSession);
			$ar = $rs->Fetch();
			$this->state["DONE"]["ALL"] = $ar["C"];
		}
		else
		{
			throw new LogicException("Unexpected next_step[\"XML_ACCOUNTS_PARENT\"]!");
		}

		// ����������� �������� (������� �����)
		if (in_array($this->filetype, array("accounts", "calculations")))
		{
			$arHouseMeter = $DB->Query("select ID, LEFT_MARGIN, RIGHT_MARGIN, ATTRIBUTES from " . $this->tableName . " where NAME='hmeters' and PARENT_ID = " . $this->state["XML_ACCOUNTS_PARENT"] . $this->andWhereThisSession)->Fetch();
			$arHouseMeters = $this->GetAllChildrenNested($arHouseMeter);

			$counter = array();

			if (is_array($arHouseMeters))
			{
				$this->__processMeters(false, $arHouseMeters['~children']['hmeter'], $counter);
			}

			if ($counter["METER_ERR"] > 0 || $counter['METER_VALUE_ERR'] > 0)
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_HOUSE_METER"));
				return false;
			}

			if (!$this->__processContractors())
			{
				return false;
			}
		}

		$this->ResetErrors();

		return true;
	}

	/**
	 * ��������� ������ �����������
	 * ORG -> contractors
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	protected function __processContractors()
	{
		global $DB;

		$arContractorsParent = $DB->Query("select ID, LEFT_MARGIN, RIGHT_MARGIN, ATTRIBUTES from " . $this->tableName . " where NAME='contractors' and PARENT_ID = " . $this->state["XML_ACCOUNTS_PARENT"] . $this->andWhereThisSession)->Fetch();
		$arContractorsXML = $this->GetAllChildrenNested($arContractorsParent);
		if (is_array($arContractorsXML))
		{
			foreach ($arContractorsXML['~children']['contractor'] as $arContractorXML)
			{
				switch ($arContractorXML['~attr']['executor'])
				{
					case 1:
						$executor = $this->strict ? "Z" : "Y";
						break;
					case 2:
						if ($this->strict)
						{
							$executor = "Y";
						}
						else
						{
							$this->ImportError(GetMessage("TI_WRONG_CONTRACTOR_EXECUTOR"), $arContractorsXML);
							return false;
						}
						break;
					case '':
						$executor = "N";
						break;
					default:
						$this->ImportError(GetMessage("TI_WRONG_CONTRACTOR_EXECUTOR"), $arContractorsXML);
						return false;
						break;
				}
				$arContractorFields = Array(
					"TSZH_ID" => $this->tszhID,
					"XML_ID" => $arContractorXML['~attr']['id'],
					"EXECUTOR" => $executor,
					"NAME" => $arContractorXML['~attr']['name'],
					"SERVICES" => $arContractorXML['~attr']['services'],
					"ADDRESS" => $arContractorXML['~attr']['address'],
					"PHONE" => $arContractorXML['~attr']['phone'],
					"BILLING" => $arContractorXML['VALUE'],
				);
				$this->processUserFields("TSZH_CONTRACTOR", 'uf_', $arContractorXML['~attr'], $arContractorFields);
				$arContractorDB = CTszhContractor::GetList(Array(), Array("TSZH_ID" => $this->tszhID, "XML_ID" => $arContractorFields["XML_ID"]))->Fetch();
				if (is_array($arContractorDB))
				{
					$contractorID = $arContractorDB['ID'];
					if (!CTszhContractor::Update($contractorID, $arContractorFields))
					{
						$this->ImportError(str_replace('#ACCOUNT_ID#', '?', GetMessage("TI_ERROR_UPDATE_CONTRACTOR")), $arContractorFields);
						return false;
					}
				}
				else
				{
					$contractorID = CTszhContractor::Add($arContractorFields);
					if ($contractorID <= 0)
					{
						$this->ImportError(str_replace('#ACCOUNT_ID#', '?', GetMessage("TI_ERROR_ADD_CONTRACTOR")), $arContractorFields);
						return false;
					}
				}
				$this->state["CONTRACTORS"][$arContractorFields["XML_ID"]] = $contractorID;
			}
		}
		return true;
	}

	/**
	 * ��������� �������� ��� ������
	 * ����������� �� ������ �������� ������ ���������
	 *
	 * @param array $arItem
	 * @return string
	 */
	protected static function __makeServiceExternalID($arItem)
	{
		$ar = Array(
			trim($arItem['edizm']),
			trim($arItem['name']),
			self::FormatNumber($arItem['norm']),
			array_key_exists('tarif1', $arItem) ? self::FormatNumber($arItem['tarif1']) : self::FormatNumber($arItem['tarif'])
		);
		$str = implode('|', $ar);
		$md5 = md5($str);
		return $md5;
	}

	public function ImportServices()
	{
		if (!in_array($this->filetype, array("accounts", "calculations")))
		{
			return;
		}

		global $DB;
		$arServices = array();

		$rsItems = $DB->Query("select * from " . $this->tableName . " where NAME='item' or NAME='item_debt'" . $this->andWhereThisSession);
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($xmlTag = $rsItems->Fetch())
		{
			$xmlTag = unserialize($xmlTag["ATTRIBUTES"]);
			if (strlen(trim($xmlTag['kod'])) == 0 && strlen($xmlTag['name']) == 0)
			{
				// ��������� ���������� �������� � 1� ��� �������� �������� � ������
				$xmlTag['kod'] = '-';
				$xmlTag['name'] = GetMessage("TSZH_DOLG_REMAINS_SERVICE");
			}

			$arService = Array(
				'ACTIVE' => 'Y',
				'UNITS' => $this->getValue("edizm", $xmlTag),
				'TSZH_ID' => $this->tszhID,
				'XML_ID' => trim($xmlTag['kod']),
				'NAME' => trim($xmlTag['name']),
				'NORM' => $this->getValue("norm", $xmlTag, true),
				'TARIFF' => $this->getValue('tarif1', $xmlTag) ? $this->getValue('tarif1', $xmlTag, true) : $this->getValue('tarif', $xmlTag, true),
				'TARIFF2' => $this->getValue('tarif2', $xmlTag, true),
				'TARIFF3' => $this->getValue('tarif3', $xmlTag, true),
			);

			$arService["XML_ID"] = self::__makeServiceExternalID($xmlTag);
			$arServices[$arService["XML_ID"]] = $arService;
		}

		foreach ($arServices as $XML_ID => $arService)
		{
			$rsOldService = CTszhService::GetList(Array(), Array("XML_ID" => $XML_ID, "TSZH_ID" => $this->tszhID));
			$arOldService = $rsOldService->Fetch();

			if (is_array($arOldService))
			{
				$serviceID = $arOldService['ID'];
				CTszhService::Update($serviceID, $arService);
			}
			else
			{
				$serviceID = CTszhService::Add($arService);
			}

			if ($serviceID)
			{
				$this->state["SERVICES"][$XML_ID] = $serviceID;
			}
		}
	}

	/**
	 * ��������� ������ �������� XML-���������
	 *
	 * @param array $arParent
	 * @return array|bool
	 */
	protected function GetAllChildrenNested($arParent)
	{
		global $DB;

		$arResult = $arParent;

		//So we get not parent itself
		if (!is_array($arResult))
		{
			$rs = $DB->Query("select ID, LEFT_MARGIN, RIGHT_MARGIN, ATTRIBUTES from " . $this->tableName . " where ID = " . intval($arResult) . $this->andWhereThisSession);
			$arResult = $rs->Fetch();
			if (!$arResult)
			{
				return false;
			}
			$arResult["~attr"] = unserialize($arResult['ATTRIBUTES']);
			$arResult["~children"] = Array();
		}

		//Array of the references to the $arParent array members with xml_id as index.
		$arIndex = array($arResult["ID"] => &$arResult);
		$rs = $DB->Query("select * from " . $this->tableName . " where LEFT_MARGIN between " . ($arParent["LEFT_MARGIN"] + 1) . " and " . ($arResult["RIGHT_MARGIN"] - 1) . $this->andWhereThisSession . " order by ID");
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($ar = $rs->Fetch())
		{
			$ar["~attr"] = unserialize($ar['ATTRIBUTES']);
			unset($ar["ATTRIBUTES"]);

			if (isset($ar["VALUE_CLOB"]))
			{
				$ar["VALUE"] = $ar["VALUE_CLOB"];
			}

			$parent_id = $ar["PARENT_ID"];
			if (!is_array($arIndex[$parent_id]))
			{
				$arIndex[$parent_id] = array();
			}

			$arIndex[$ar['ID']] = $ar;
			$arIndex[$ar["PARENT_ID"]]['~children'][$ar['NAME']][] = &$arIndex[$ar['ID']];
		}

		return $arResult;
	}

	/**
	 * ��������� ������ ������� ������
	 *
	 * @return array
	 */
	public function ImportAccounts()
	{
		global $DB;

		$counter = array();
		if ($this->state["XML_ACCOUNTS_PARENT"])
		{
			$rsParents = $DB->Query("select ID, LEFT_MARGIN, RIGHT_MARGIN, ATTRIBUTES from " . $this->tableName . " where NAME='PersAcc' and PARENT_ID = " . $this->state["XML_ACCOUNTS_PARENT"] . $this->andWhereThisSession . " AND ID > " . intval($this->state["XML_LAST_ID"]) . " order by ID");
			/** @noinspection PhpAssignmentInConditionInspection */
			while (!$this->timeIsUp() && $arParent = $rsParents->Fetch())
			{
				$arParent['~attr'] = unserialize($arParent["ATTRIBUTES"]);
				unset($arParent['ATTRIBUTES']);
				$arXMLElement = $this->GetAllChildrenNested($arParent);
				$ID = $this->ImportAccount($arXMLElement, $counter);

				if ($ID)
				{
					if ($this->useSessions)
					{
						$DB->Query("INSERT INTO " . $this->tableName . " (SESS_ID, PARENT_ID, LEFT_MARGIN) values ('" . $DB->ForSql($this->sessionId) . "', 0, " . $ID . ")");
					}
					else
					{
						$DB->Query("INSERT INTO " . $this->tableName . " (PARENT_ID, LEFT_MARGIN) values (0, " . $ID . ")");
					}
				}

				$this->state["XML_LAST_ID"] = $arParent["ID"];
			}
		}
		return $counter;
	}

	/**
	 * ��������� ������ ������� �������� ���� �/� �� �����
	 * (������ �12276)
	 *
	 * @param int $accountId ID ��������� �������� �����
	 * @param string $newExternalId ����� ������� ���
	 */
	protected function mergeAccount($accountId, $newExternalId)
	{
		// ����� ������� ��� ������� �� ���� ������: �������� � �� �� 1�, ����������� ��������
		// ����� ����� � ���������� ������ �������� �������� ���� (� ��������� � ���)
		$parts = explode(self::NEW_EXTERNAL_ID_SEPARATOR, $newExternalId, 2);
		CTszhAccount::mergeAccounts($accountId, false, array(
			$newExternalId, // �� ������ ������ ����� ������� � ������������
			$parts[1], // ��� ��������
			$parts[0] . $parts[1], // � ���������
		));
	}

	/**
	 * ��������� �������� �����
	 * ORG -> PersAcc
	 *
	 * @param array $arXMLElement
	 * @param array $counter
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	protected function ImportAccount($arXMLElement, &$counter)
	{
		// http://yrogay2000.ru/ uses old version
		$bOldVersion = COption::GetOptionInt('citrus.tszh', 'import.old.version', 0);

		$arAttrs = Array();
		foreach ($arXMLElement['~attr'] as $key => $value)
			$arAttrs[ToLower($key)] = $value;

		if ($bOldVersion)
		{
			$accountNumber = $accountLogin = $accountExternalID = trim($arAttrs['kod_ls']);
		}
		else
		{
			// ���������� ����� �������� ����� �� ��������, ��������� �� �
			$nameLs = trim($arAttrs['name_ls']);

			$nPos = strpos($nameLs, GetMessage("TI_N_SIGN"));
			if ($nPos !== false)
			{
				$nameLs = trim(substr($nameLs, $nPos + 1));
			}

			$accountNumber = $nameLs;
			$accountLogin = array_key_exists('login', $arAttrs) ? trim($arAttrs['login']) : trim($arAttrs['kod_ls']);
			$accountExternalID = $arAttrs['kod_ls'];
		}

		switch ($this->filetype)
		{
			case "access":
				$rewrite = array_key_exists("rewrite", $arAttrs) && ToLower($arAttrs["rewrite"]) == "true";
				$accountNumber = trim($arAttrs['kod_ls']);
				break;

			case "calculations":
				$rewrite = false;
				break;

			case "accounts":
			default:
				if ($this->version < 3)
				{
					$rewrite = $this->createUsers;
				}
				else
				{
					$rewrite = false;
				}
				break;
		}

		// �������� �������� ����� � ��������� del
		if (in_array($this->filetype, array("accounts", "calculations")) && array_key_exists('del', $arAttrs))
		{
			$rsAccount = CTszhAccount::GetList(Array(), Array("EXTERNAL_ID" => $accountExternalID, "TSZH_ID" => $this->tszhID));
			/** @noinspection PhpAssignmentInConditionInspection */
			if ($arAccount = $rsAccount->Fetch())
			{
				$userID = $arAccount['USER_ID'];
				$bUserExists = is_array(CUser::GetByID($userID)->Fetch());

				if (CTszhAccount::Delete($arAccount['ID']))
				{
					if ($bUserExists)
					{
						CUser::Delete($userID);
					}
					$counter["DEL"]++;
				}
			}
			return false;
		}

		if ($this->depersonalize)
		{
			$accountName = false;
			$arFIO = array(false, false, false);
		}
		else
		{
			$accountName = trim($arAttrs['name']);
			$arFIO = explode(' ', $accountName);
		}

		$_accountPassword = (array_key_exists('password', $arAttrs) && strlen($arAttrs['password']) > 0) ? $arAttrs['password'] : false;
		$_accountPassword = (array_key_exists('uf_regcode', $arAttrs) && strlen($arAttrs['uf_regcode']) > 0) ? $arAttrs['uf_regcode'] : $_accountPassword;

		$accountPassword = $_accountPassword === false ? tszhGeneratePassword() : $_accountPassword;
		$serverName = trim(CUtil::translit($_SERVER["SERVER_NAME"], 'ru', Array("safe_chars" => "-.", "replace_space" => '-', "replace_other" => '-')), '-');
		$accountNumberTranslit = trim(CUtil::translit($accountNumber, 'ru', Array("replace_space" => '-', "replace_other" => '.')), '-.');
		if (!check_email('mail@' . $serverName))
		{
			$serverName = 'unknown.domain';
		}
		$_accountEmail = trim($arAttrs['email']);
		$accountEmail = (strlen($_accountEmail) > 0 && self::check_email($_accountEmail)) ? $_accountEmail : $accountNumberTranslit . '@' . $serverName;
		if (!self::check_email($accountEmail))
		{
			$accountEmail = 'mail@' . $serverName;
		}

		$bError = false;
		if (strlen($accountNumber) <= 0 && in_array($this->filetype, array("accounts", "calculations")))
		{
			$this->ImportError(GetMessage("TI_WRONG_ACCOUNT_NUMBER"), $arXMLElement);
			$bError = true;
		}
		if (strlen($accountExternalID) <= 0)
		{
			$this->ImportError(GetMessage("TI_WRONG_ACCOUNT_EXTERNAL_ID"), $arXMLElement);
			$bError = true;
		}
		if ($rewrite && strlen($accountLogin) < 3)
		{
			$this->ImportError(GetMessage("TI_WRONG_ACCOUNT_LOGIN", Array('#LOGIN#' => $accountLogin, "#FIELD#" => array_key_exists('login', $arAttrs) ? 'login' : 'kod_ls')), $arXMLElement);
			$bError = true;
		}
		if ($rewrite && !self::check_email($accountEmail) && !$bError)
		{
			$bError = true;
			$this->ImportError(str_replace("#MAIL#", $accountEmail, GetMessage("TI_WRONG_ACCOUNT_EMAIL")), $arXMLElement);
		}
		$newExternalIdParts = array();
		if (array_key_exists('kod_ls_new', $arAttrs))
		{
			$newExternalId = $arAttrs["kod_ls_new"];
			$newExternalIdParts = explode(self::NEW_EXTERNAL_ID_SEPARATOR, $newExternalId, 2);
			if (count($newExternalIdParts) !== 2)
			{
				$this->ImportError(GetMessage("TI_WRONG_ACCOUNT_KOD_LS_NEW", array("#ACCOUNT#" => $accountNumber)), $arXMLElement);
				$bError = true;
			}
		}
		else
		{
			$newExternalId = false;
		}
		if ($bError)
		{
			$counter["ERR"]++;
			return false;
		}

		$obUser = new CUser();

		$userDefaultGroup = CTszh::GetTenantGroup();
		$userOrgGroup = 2;
		if (strlen($this->arTszh['CODE']) > 0)
		{
			$dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "jkh_users_" . $this->arTszh['CODE']));
			/** @noinspection PhpAssignmentInConditionInspection */
			if ($arGroup = $dbGroup->Fetch())
			{
				$userOrgGroup = $arGroup['ID'];
			}
		}

		$accountID = false;
		$userID = false;

		// ������� ���� ��� ���������� � ������� ��� ����
		if ($newExternalId && is_array($newExternalIdParts)) // ��. ����� self::mergeAccount()
		{
			$_external = array(
				$accountExternalID, // �� kod_ls
				$newExternalId, // ����� �������
				$newExternalIdParts[1], // ��� ��������
				$newExternalIdParts[0] . $newExternalIdParts[1] // ������� + ��� (��� �����������)
			);
		}
		else
		{
			$_external = $accountExternalID;
		}

		$rsAccount = CTszhAccount::GetList(Array("ID" => "ASC"), Array("EXTERNAL_ID" => $_external, "TSZH_ID" => $this->tszhID));
		/** @noinspection PhpAssignmentInConditionInspection */
		if ($arAccount = $rsAccount->Fetch())
		{
			$accountID = $arAccount['ID'];
			$userID = $arAccount['USER_ID'];
			$bUserExists = is_array(CUser::GetByID($userID)->Fetch());
			if (!$bUserExists)
			{
				$userID = false;
			}

			if ($newExternalId)
			{
				self::mergeAccount($accountID, $newExternalId);
			}

			$arUpdateAccount = Array(
				'XML_ID' => $accountNumber,
				"EXTERNAL_ID" => $newExternalId ? $newExternalId : $accountExternalID,
				"TSZH_ID" => $this->tszhID,
				'NAME' => $accountName,
				'CITY' => $arAttrs['addresscity'],
				'DISTRICT' => $arAttrs['addressdistrict'],
				'REGION' => $arAttrs['addressregion'],
				'SETTLEMENT' => $arAttrs['addresssettlement'],
				'STREET' => $arAttrs['addressstreet'],
				'HOUSE' => $arAttrs['addresshouse'],
				'FLAT' => $arAttrs['addressflat'],
				'FLAT_ABBR' => strlen($arAttrs['flatabbr']) > 0 ? $arAttrs['flatabbr'] : false,
				'AREA' => $this->formatNumber($arAttrs['commonarea']),
				'LIVING_AREA' => $this->formatNumber($arAttrs['habarea']),
				'PEOPLE' => $arAttrs['people'],
			);
			if (is_set($arAttrs, "flattype"))
			{
				$arUpdateAccount["FLAT_TYPE"] = strlen($arAttrs["flattype"]) > 0 ? $arAttrs["flattype"] : false;
			}
			if (is_set($arAttrs, "num_of_reg"))
			{
				$arUpdateAccount["REGISTERED_PEOPLE"] = strlen($arAttrs["num_of_reg"]) > 0 ? $arAttrs["num_of_reg"] : false;
			}
			if (is_set($arAttrs, "num_of_comp"))
			{
				$arUpdateAccount["EXEMPT_PEOPLE"] = strlen($arAttrs["num_of_comp"]) > 0 ? $arAttrs["num_of_comp"] : false;
			}
			if (is_set($arAttrs, "housearea"))
			{
				$arUpdateAccount["HOUSE_AREA"] = strlen($arAttrs["housearea"]) > 0 ? $this->formatNumber($arAttrs["housearea"]) : false;
			}
			if (is_set($arAttrs, "flatsarea"))
			{
				$arUpdateAccount["HOUSE_ROOMS_AREA"] = strlen($arAttrs["flatsarea"]) > 0 ? $this->formatNumber($arAttrs["flatsarea"]) : false;
			}
			if (is_set($arAttrs, "comflatsarea"))
			{
				$arUpdateAccount["HOUSE_COMMON_PLACES_AREA"] = strlen($arAttrs["comflatsarea"]) > 0 ? $this->formatNumber($arAttrs["comflatsarea"]) : false;
			}

			/*if (!$bUserExists && $this->updateUsers)
				$arUpdateAccount['UF_REGCODE'] = $accountPassword;*/

			// ���������������� ����
			$this->processUserFields("TSZH_ACCOUNT", 'uf_', $arAttrs, $arUpdateAccount);

			// � ������ ������ ������� ������ ������������� ������ ���� ����������� �������������� ������� �� ����� �������
			// � ������ 3 � ������/�������������� ���� ������ ������� rewrite ��� ������������ ��� �� ����������,
			// � ����� �������������� ��� $this->filetype == "calculations" && $bUserExists
			// ��������� ���������� $this->filetype == "accounts" && $bUserExists
			if (($this->version < 3 && $this->createUsers)
			    || ($this->version >= 3 && ($rewrite || !$bUserExists || (($this->filetype == "calculations" || $this->filetype == "accounts") && $bUserExists)))
			)
			{
				// ������� ������������ � ������ ���������� ������� ������ (���)�
				$arUserGroups = array_values(array_unique(array_merge(
					array_diff(CUser::GetUserGroup($userID), Array(2)),
					Array($userDefaultGroup, $userOrgGroup)
				)));
				$arUser = Array(
					'ACTIVE' => 'Y',
					"LID" => $this->siteID,
					'LOGIN' => $accountLogin,
					'XML_ID' => $accountNumber,
					'NAME' => $arFIO[1],
					'SECOND_NAME' => $arFIO[2],
					'LAST_NAME' => $arFIO[0],
					'GROUP_ID' => $arUserGroups,
					'PERSONAL_STATE' => $arAttrs['addressregion'],
					'PERSONAL_CITY' => strlen($arAttrs['addresscity']) > 0 ? $arAttrs['addresscity'] : $arAttrs['addresssettlement'],
					'PERSONAL_STREET' => $arAttrs['addressstreet'] . (strlen($arAttrs['addresshouse']) > 0 ? ', ' . $arAttrs['addresshouse'] : ''),
					"EMAIL" => $accountEmail,
					"PASSWORD" => $accountPassword,
					"CONFIRM_PASSWORD" => $accountPassword,
					'ADMIN_NOTES' => str_replace("#PASSWORD#", $accountPassword, GetMessage("TSZH_IMPORT_USER_ADMIN_NOTES")),
				);

				if ($this->filetype == "access")
				{
					unset($arUser['XML_ID']);
					unset($arUser['NAME']);
					unset($arUser['SECOND_NAME']);
					unset($arUser['LAST_NAME']);
					unset($arUser['PERSONAL_STATE']);
					unset($arUser['PERSONAL_CITY']);
					unset($arUser['PERSONAL_STREET']);
					if ($userID > 0)
					{
						unset($arUser['EMAIL']);
					}
				}
				elseif ($this->filetype == "calculations")
				{
					unset($arUser["ACTIVE"]);
					unset($arUser["LID"]);
					unset($arUser["LOGIN"]);
					unset($arUser["GROUP_ID"]);
					unset($arUser["EMAIL"]);
					unset($arUser["PASSWORD"]);
					unset($arUser["CONFIRM_PASSWORD"]);
					unset($arUser["ADMIN_NOTES"]);
				}

				// ������, ����� ������� ���� ����������� ��� ������������, � ��� ���� ���� ������������ � ���� �� ������� � �/� ������ �����������
				// ��������� ID ������������ �� ���������� ������
				if (!$userID)
				{
					$arOtherUser = CUser::GetByLogin($arUser['LOGIN'])->Fetch();
					if (is_array($arOtherUser) && $arOtherUser["ID"])
					{
						$userID = $arOtherUser["ID"];
					}
				}

				if ($userID > 0)
				{
					if ($this->filetype == "accounts")
					{
						//if ($this->version >= 3)
						{
							unset($arUser["LOGIN"]);
							unset($arUser["EMAIL"]);
							unset($arUser["PASSWORD"]);
							unset($arUser["CONFIRM_PASSWORD"]);
							unset($arUser["ADMIN_NOTES"]);
						}
						/*else
						{
							if (strlen($nativeAccountEmail) <= 0)
								unset($arUser["EMAIL"]);
							if (strlen($nativeAccountPassword) <= 0)
							{
								unset($arUser["PASSWORD"]);
								unset($arUser["CONFIRM_PASSWORD"]);
								unset($arUser["ADMIN_NOTES"]);
							}
						}*/
					}
					$userID = $obUser->Update($userID, $arUser) ? $userID : false;
				}
				elseif ($this->filetype != "calculations")
				{
					$userID = $obUser->Add($arUser);
				}

				if ($userID)
				{
					$arUpdateAccount['USER_ID'] = $userID;
					if ($_accountPassword !== false && isset($arUser["PASSWORD"]))
					{
						$arUpdateAccount["UF_REGCODE"] = $_accountPassword;
					}
					$counter["USR_UPD"]++;

					if ($this->filetype == "access")
					{
						$arUpdateAcc = array("USER_ID" => $userID);
						if ($_accountPassword !== false)
						{
							$arUpdateAcc["UF_REGCODE"] = $_accountPassword;
						}
						if (CTszhAccount::Update($accountID, $arUpdateAcc, ConvertTimeStamp(MakeTimeStamp($this->sPeriod, 'YYYY-MM-DD'), 'FULL')))
						{
							$counter["UPD"]++;
						}
						else
						{
							$counter["ERR"]++;
							$this->ImportError(GetMessage("TI_ERROR_UPDATE_ACCOUNT"), $arXMLElement);
						}
					}
				}
				elseif (!($this->filetype == "calculations" && !$bUserExists))
				{
					$counter["USR_ERR"]++;
					$counter["ERR"]++;
					$this->ImportError(str_replace('#LOGIN#', $arUser['LOGIN'], GetMessage("TI_ERROR_ADD_UPDATE_USER")) . ': ' . $obUser->LAST_ERROR, $arUser);
				}
			}

			if (in_array($this->filetype, array("accounts", "calculations")))
			{
				if (CTszhAccount::Update($accountID, $arUpdateAccount, ConvertTimeStamp(MakeTimeStamp($this->sPeriod, 'YYYY-MM-DD'), 'FULL')))
				{
					$counter["UPD"]++;
				}
				else
				{
					$counter["ERR"]++;
					$this->ImportError(GetMessage("TI_ERROR_UPDATE_ACCOUNT"), $arXMLElement);
				}

				$arUpdateAccountPeriod = Array(
					"ACCOUNT_ID" => $accountID,
					"PERIOD_ID" => $this->periodID,
					'DEBT_BEG' => $this->formatNumber($arAttrs['debtbeg']),
					'DEBT_END' => $this->formatNumber($arAttrs['debtend']),
					'SUMM_TO_PAY' => $this->formatNumber($arAttrs['sumtopay']),
				);
				$barcodes = $this->__processBarcodes($arXMLElement['~children']['barcode'], $accountNumber);
				if (!empty($barcodes))
				{
					$arUpdateAccountPeriod["BARCODE"] = '';
					$arUpdateAccountPeriod["BARCODES"] = $barcodes;
				}
				elseif (is_set($arAttrs, "barcode") && strlen(trim($arAttrs['barcode'])))
				{
					$arUpdateAccountPeriod["BARCODE"] = trim($arAttrs['barcode']);
				}
				else
				{
					$arUpdateAccountPeriod["BARCODE"] = '';
					$arUpdateAccountPeriod["BARCODES"] = array();
				}

				// ���������������� ���� ���������
				$this->processUserFields("TSZH_ACCOUNT_PERIOD", 'ruf_', $arAttrs, $arUpdateAccountPeriod);

				if ($this->version >= 2)
				{
					$arUpdateAccountPeriod["DEBT_PREV"] = $this->formatNumber($arAttrs['debtprev']);
					$arUpdateAccountPeriod["PREPAYMENT"] = $this->formatNumber($arAttrs['prepayment']);
					$arUpdateAccountPeriod["LAST_PAYMENT"] = $arAttrs['lastpaymentdate'];
					$arUpdateAccountPeriod["SUMM_TO_PAY"] = $this->formatNumber($arAttrs['sumtopay']);
				}
				if (is_set($arAttrs, "creditpayed"))
				{
					$arUpdateAccountPeriod["CREDIT_PAYED"] = $this->formatNumber($arAttrs['creditpayed']);
				}

				if ($this->updateMode)
				{
					$rsOldAccountPeriod = CTszhAccountPeriod::GetList(array(), array("ACCOUNT_ID" => $accountID, "PERIOD_ID" => $this->periodID), false, false, array("*"));
					/** @noinspection PhpAssignmentInConditionInspection */
					if ($arOldAccountPeriod = $rsOldAccountPeriod->Fetch())
					{
						CTszhAccountPeriod::Delete($arOldAccountPeriod['ID']);

						/*// ���� ��������� ���� ������� �� e-mail, � ���� ���� ������������� ��������� �� ���������, �� �������� ������� �������� ���������
						if ($arOldAccountPeriod["IS_SENT"] == "Y")
						{
							$isChanged = false;
							foreach ($arUpdateAccountPeriod as $key => $value)
							{
								if ($arOldAccountPeriod[$key] != $value)
								{
									$isChanged = true;
									break;
								}
							}

							if (!$isChanged)
							{
								$arUpdateAccountPeriod["DATE_SENT"] = $arOldAccountPeriod["DATE_SENT"];
							}
						}*/

						// ���� ��������� ���� ������� �� e-mail, �� �������� ������� � �������� �������� �� ����� ��������� � �����
						if ($arOldAccountPeriod["IS_SENT"] == "Y")
						{
							$arUpdateAccountPeriod["DATE_SENT"] = $arOldAccountPeriod["DATE_SENT"];
						}
					}
				}
				$accountPeriodID = CTszhAccountPeriod::Add($arUpdateAccountPeriod);
				if ($accountPeriodID <= 0)
				{
					$counter["ERR"]++;
					$this->ImportError(str_replace('#ACCOUNT#', $accountNumber, GetMessage("TI_ERROR_ADD_ACCOUNT_PERIOD")), $arXMLElement);
				}
			}

			// ���������� ������ �������� �����
		}
		else
		{
			$arUpdateAccount = Array(
				'XML_ID' => $accountNumber,
				"EXTERNAL_ID" => $newExternalId ? $newExternalId : $accountExternalID,
				"TSZH_ID" => $this->tszhID,
				'NAME' => $accountName,
				'CITY' => $arAttrs['addresscity'],
				'DISTRICT' => $arAttrs['addressdistrict'],
				'REGION' => $arAttrs['addressregion'],
				'SETTLEMENT' => $arAttrs['addresssettlement'],
				'STREET' => $arAttrs['addressstreet'],
				'HOUSE' => $arAttrs['addresshouse'],
				'FLAT' => $arAttrs['addressflat'],
				'FLAT_ABBR' => strlen($arAttrs['flatabbr']) > 0 ? $arAttrs['flatabbr'] : false,
				'AREA' => $this->formatNumber($arAttrs['commonarea']),
				'LIVING_AREA' => $this->formatNumber($arAttrs['habarea']),
				'PEOPLE' => $arAttrs['people'],
				'UF_REGCODE' => $accountPassword,
				'USER_ID' => false,
			);
			if (is_set($arAttrs, "flattype"))
			{
				$arUpdateAccount["FLAT_TYPE"] = strlen($arAttrs["flattype"]) > 0 ? $arAttrs["flattype"] : false;
			}
			if (is_set($arAttrs, "num_of_reg"))
			{
				$arUpdateAccount["REGISTERED_PEOPLE"] = strlen($arAttrs["num_of_reg"]) > 0 ? $arAttrs["num_of_reg"] : false;
			}
			if (is_set($arAttrs, "num_of_comp"))
			{
				$arUpdateAccount["EXEMPT_PEOPLE"] = strlen($arAttrs["num_of_comp"]) > 0 ? $arAttrs["num_of_comp"] : false;
			}
			if (is_set($arAttrs, "housearea"))
			{
				$arUpdateAccount["HOUSE_AREA"] = strlen($arAttrs["housearea"]) > 0 ? $this->formatNumber($arAttrs["housearea"]) : false;
			}
			if (is_set($arAttrs, "flatsarea"))
			{
				$arUpdateAccount["HOUSE_ROOMS_AREA"] = strlen($arAttrs["flatsarea"]) > 0 ? $this->formatNumber($arAttrs["flatsarea"]) : false;
			}
			if (is_set($arAttrs, "comflatsarea"))
			{
				$arUpdateAccount["HOUSE_COMMON_PLACES_AREA"] = strlen($arAttrs["comflatsarea"]) > 0 ? $this->formatNumber($arAttrs["comflatsarea"]) : false;
			}
			// ���������������� ����
			$this->processUserFields("TSZH_ACCOUNT", 'uf_', $arAttrs, $arUpdateAccount);

			if ($this->filetype == "access")
			{
				unset($arUpdateAccount["XML_ID"]);
				unset($arUpdateAccount["NAME"]);
				unset($arUpdateAccount["CITY"]);
				unset($arUpdateAccount["DISTRICT"]);
				unset($arUpdateAccount["REGION"]);
				unset($arUpdateAccount["SETTLEMENT"]);
				unset($arUpdateAccount["STREET"]);
				unset($arUpdateAccount["HOUSE"]);
				unset($arUpdateAccount["FLAT"]);
				unset($arUpdateAccount["FLAT_ABBR"]);
				unset($arUpdateAccount["AREA"]);
				unset($arUpdateAccount["LIVING_AREA"]);
				unset($arUpdateAccount["PEOPLE"]);
			}

			if ($rewrite || $this->filetype == "access")
			{
				$arUser = Array(
					'ACTIVE' => 'Y',
					"LID" => $this->siteID,
					'LOGIN' => $accountLogin,
					'XML_ID' => $accountNumber,
					'NAME' => $arFIO[1],
					'SECOND_NAME' => $arFIO[2],
					'LAST_NAME' => $arFIO[0],
					'GROUP_ID' => Array($userDefaultGroup, $userOrgGroup),
					'PERSONAL_STATE' => $arAttrs['addressregion'],
					'PERSONAL_CITY' => strlen($arAttrs['addresscity']) > 0 ? $arAttrs['addresscity'] : $arAttrs['addresssettlement'],
					'PERSONAL_STREET' => $arAttrs['addressstreet'] . (strlen($arAttrs['addresshouse']) > 0 ? ', ' . $arAttrs['addresshouse'] : ''),
					"EMAIL" => $accountEmail,
					"PASSWORD" => $accountPassword,
					"CONFIRM_PASSWORD" => $accountPassword,
					'ADMIN_NOTES' => str_replace("#PASSWORD#", $accountPassword, GetMessage("TSZH_IMPORT_USER_ADMIN_NOTES")),
				);

				if ($this->filetype == "access")
				{
					unset($arUser["XML_ID"]);
					unset($arUser["NAME"]);
					unset($arUser["SECOND_NAME"]);
					unset($arUser["LAST_NAME"]);
					unset($arUser["PERSONAL_STATE"]);
					unset($arUser["PERSONAL_CITY"]);
					unset($arUser["PERSONAL_STREET"]);
				}

				$rsUser = CUser::GetByLogin($arUser['LOGIN']);
				/** @noinspection PhpAssignmentInConditionInspection */
				if ($arOldUser = $rsUser->Fetch())
				{
					switch ($this->filetype)
					{
						case "accounts":
							unset($arUser['PASSWORD']);
							unset($arUser['CONFIRM_PASSWORD']);
							unset($arUser['EMAIL']);
							unset($arUser['ADMIN_NOTES']);
							break;

						case "access":
							unset($arUser['EMAIL']);
							break;
					}

					if ($obUser->Update($arOldUser['ID'], $arUser))
					{
						$userID = $arOldUser['ID'];
						$counter["USR_UPD"]++;
					}
					else
					{
						$counter["USR_ERR"]++;
						$this->ImportError(str_replace('#ID#', $userID, GetMessage("TI_ERROR_UPDATE_USER")) . ': ' . $obUser->LAST_ERROR, $arUser);
					}
				}
				else
				{
					$userID = $obUser->Add($arUser);
					if ($userID > 0)
					{
						$counter["USR_ADD"]++;
					}
					else
					{
						$counter["USR_ERR"]++;
						$this->ImportError(str_replace('#ACCOUNT_ID#', $accountNumber, GetMessage("TI_ERROR_ADD_USER")) . ': ' . $obUser->LAST_ERROR, $arXMLElement);
					}
				}
			}

			if (IntVal($userID) > 0)
			{
				$arUpdateAccount['USER_ID'] = $userID;
			}
			$needAddAccountFlag = $this->filetype == "accounts"
				|| $this->filetype == "calculations"
				|| $this->filetype == "access"/* && $rewrite*/
			;
			if ($needAddAccountFlag)
			{
				$accountID = CTszhAccount::Add($arUpdateAccount, false, ConvertTimeStamp(MakeTimeStamp($this->sPeriod, 'YYYY-MM-DD'), 'FULL'), $this->filetype != "access");
			}

			if ($accountID > 0)
			{
				if ($newExternalId)
				{
					self::mergeAccount($accountID, $newExternalId);
				}

				if (in_array($this->filetype, array("accounts", "calculations")))
				{
					$arUpdateAccountPeriod = Array(
						"ACCOUNT_ID" => $accountID,
						"PERIOD_ID" => $this->periodID,
						'DEBT_BEG' => $this->formatNumber($arAttrs['debtbeg']),
						'DEBT_END' => $this->formatNumber($arAttrs['debtend']),
					);
					$barcodes = $this->__processBarcodes($arXMLElement['~children']['barcode'], $accountNumber);
					if (!empty($barcodes))
					{
						$arUpdateAccountPeriod["BARCODE"] = '';
						$arUpdateAccountPeriod["BARCODES"] = $barcodes;
					}
					elseif (is_set($arAttrs, "barcode") && strlen(trim($arAttrs['barcode'])))
					{
						$arUpdateAccountPeriod["BARCODE"] = trim($arAttrs['barcode']);
					}
					else
					{
						$arUpdateAccountPeriod["BARCODE"] = '';
						$arUpdateAccountPeriod["BARCODES"] = array();
					}

					// ���������������� ���� ���������
					$this->processUserFields("TSZH_ACCOUNT_PERIOD", 'ruf_', $arAttrs, $arUpdateAccountPeriod);

					if ($this->version >= 2)
					{
						$arUpdateAccountPeriod["DEBT_PREV"] = $this->formatNumber($arAttrs['debtprev']);
						$arUpdateAccountPeriod["PREPAYMENT"] = $this->formatNumber($arAttrs['prepayment']);
						$arUpdateAccountPeriod["LAST_PAYMENT"] = $arAttrs['lastpaymentdate'];
						$arUpdateAccountPeriod["SUMM_TO_PAY"] = $this->formatNumber($arAttrs['sumtopay']);
					}
					$accountPeriodID = CTszhAccountPeriod::Add($arUpdateAccountPeriod);
					if ($accountPeriodID <= 0)
					{
						$counter["ERR"]++;
						$this->ImportError(str_replace('#ACCOUNT#', $accountNumber, GetMessage("TI_ERROR_ADD_ACCOUNT_PERIOD")), $arXMLElement);
					}
					else
					{
						$counter["ADD"]++;
					}
				}
			}
			elseif ($needAddAccountFlag)
			{
				$counter["ERR"]++;
				$this->ImportError(str_replace('#ACCOUNT_ID#', $accountNumber, GetMessage("TI_ERROR_ADD_ACCOUNT")), $arXMLElement);
			}
		}

		if ($accountID > 0 && in_array($this->filetype, array("accounts", "calculations")))
		{
			$arUpdateAccount['ID'] = $accountID;

			// ��������
			$this->__processMeters($arUpdateAccount, $arXMLElement['~children']['meter'], $counter, $accountNumber);

			// ����������
			$this->__processCharges($accountPeriodID, $arXMLElement['~children']['item'], $counter, $accountNumber);
			$this->__processCharges($accountPeriodID, $arXMLElement['~children']['item_debt'], $counter, $accountNumber, $debt = true);

			if ($this->version >= 2 && isset($accountPeriodID))
			{
				$this->__processAccountContractors($accountID, $arXMLElement['~children']['contractor'], $accountPeriodID, $accountNumber);
				$this->__processAccountCorrections($accountID, $arXMLElement['~children']['correction'], $accountPeriodID, $accountNumber);
			}

			if ($this->version >= 3 && isset($accountPeriodID))
			{
				$this->__processAccountInstallments($arXMLElement["~children"]["credit"], $accountPeriodID, $accountNumber);
			}
		}

		return $accountID;
	}

	/**
	 * ��������� ������ �������� ����� �� �����������
	 * ORG -> PersAcc -> contractor
	 *
	 * @param int $accountID
	 * @param array $arContractorsXML
	 * @param int $accountPeriodID
	 * @param string $accountNumber
	 * @return bool
	 */
	protected function __processAccountContractors($accountID, $arContractorsXML, $accountPeriodID, $accountNumber)
	{
		// delete old
		$rs = CTszhAccountContractor::GetList(Array(), Array(
			"ACCOUNT_PERIOD_ID" => $accountPeriodID,
		), false, false, Array('ID'));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($ar = $rs->Fetch())
			CTszhAccountContractor::Delete($ar['ID']);

		foreach ($arContractorsXML as $idx => $arXMLContractor)
		{
			$arAttrs = $arXMLContractor["~attr"];
			if (!array_key_exists($arAttrs["id"], $this->state['CONTRACTORS']))
			{
				$this->ImportError(getMessage("TI_ERROR_ADD_ACCOUNT_CONTRACTOR", array("#ACCOUNT_ID#" => $accountNumber)), $arXMLContractor);
				return false;
			}

			$contractorID = $this->state['CONTRACTORS'][$arAttrs["id"]];
			$arFields = Array(
				"ACCOUNT_PERIOD_ID" => $accountPeriodID,
				"CONTRACTOR_ID" => $contractorID,
				"SUMM" => $this->formatNumber($arAttrs["sum_to_pay"]),
			);
			if (is_set($arAttrs, "debtbeg") && strlen($arAttrs["debtbeg"]) > 0)
			{
				$arFields["DEBT_BEG"] = $this->formatNumber($arAttrs["debtbeg"]);
			}
			if (is_set($arAttrs, "sum_payed") && strlen($arAttrs["sum_payed"]) > 0)
			{
				$arFields["SUMM_PAYED"] = $this->formatNumber($arAttrs["sum_payed"]);
			}
			if (is_set($arAttrs, "peni") && strlen($arAttrs["peni"]) > 0)
			{
				$arFields["PENALTIES"] = $this->formatNumber($arAttrs["peni"]);
			}
			if (is_set($arAttrs, "sum") && strlen($arAttrs["sum"]) > 0)
			{
				$arFields["SUMM_CHARGED"] = $this->formatNumber($arAttrs["sum"]);
			}
			if (is_set($arAttrs, "num") && intval($arAttrs["num"]) > 0)
			{
				$arFields["RECEIPT_ORDER"] = intval($arAttrs["num"]);
			}
			if (is_set($arAttrs, "services") && strlen(trim($arAttrs["services"])))
			{
				$arFields["SERVICES"] = $arAttrs["services"];
			}

			$id = CTszhAccountContractor::Add($arFields);
			if ($id <= 0)
			{
				$this->ImportError(getMessage("TI_ERROR_ADD_ACCOUNT_CONTRACTOR", array("#ACCOUNT_ID#" => $accountNumber, "#ID#" => $accountID)), $arFields);
			}
		}
		return true;
	}

	/**
	 * ��������� ������������ �� �������� �����
	 *
	 * @param int $accountID
	 * @param array $arCorrectionsXML
	 * @param int $accountPeriodID
	 * @param string $accountNumber
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	protected function __processAccountCorrections($accountID, $arCorrectionsXML, $accountPeriodID, $accountNumber)
	{
		// delete existing corrections
		$rs = CTszhAccountCorrections::GetList(Array(), Array(
			"ACCOUNT_PERIOD_ID" => $accountPeriodID,
		), false, false, Array('ID'));

		/** @noinspection PhpAssignmentInConditionInspection */
		while ($ar = $rs->Fetch())
			CTszhAccountCorrections::Delete($ar['ID']);

		foreach ($arCorrectionsXML as $idx => $arXMLCorrection)
		{
			$arAttrs = $arXMLCorrection["~attr"];
			$contractorID = $this->state['CONTRACTORS'][$arAttrs["contractor"]];
			$arFields = Array(
				"XML_ID" => $arAttrs['id'],
				"ACCOUNT_ID" => $accountID,
				"ACCOUNT_PERIOD_ID" => $accountPeriodID,
				"CONTRACTOR_ID" => $contractorID,
				"SUMM" => $this->formatNumber($arAttrs['summ']),
				"GROUNDS" => $arAttrs['grounds'],
				"SERVICE" => $arAttrs['service'],
			);
			$id = CTszhAccountCorrections::Add($arFields);
			if ($id <= 0)
			{
				$this->ImportError(str_replace('#ACCOUNT_ID#', $accountNumber, GetMessage("TI_ERROR_ADD_CORRECTIONS")), $arFields);
			}
		}
	}

	/**
	 * ��������� ������ �� ��������� ��������� �����
	 * ORG -> PersAcc -> credit
	 *
	 * @param array $arInstallmentsXML
	 * @param int $accountPeriodID
	 * @param string $accountNumber
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	protected function __processAccountInstallments($arInstallmentsXML, $accountPeriodID, $accountNumber)
	{
		// delete old
		$rs = CTszhAccountInstallments::GetList(Array(), Array(
			"ACCOUNT_PERIOD_ID" => $accountPeriodID,
		), false, false, Array("ID"));

		/** @noinspection PhpAssignmentInConditionInspection */
		while ($ar = $rs->Fetch())
			CTszhAccountInstallments::Delete($ar["ID"]);

		foreach ($arInstallmentsXML as $idx => $arXMLInstallment)
		{
			$arAttrs = $arXMLInstallment["~attr"];
			$contractorID = $this->state['CONTRACTORS'][$arAttrs["contractor"]];
			$arFields = Array(
				"ACCOUNT_PERIOD_ID" => $accountPeriodID,
				"CONTRACTOR_ID" => $contractorID,
				"SERVICE" => $arAttrs["name"],
				"SUMM_PAYED" => $this->formatNumber($arAttrs["sum"]),
				"SUMM_PREV_PAYED" => $this->formatNumber($arAttrs["balance"]),
				"PERCENT" => $this->formatNumber($arAttrs["rate"]),
				"SUMM_RATED" => $this->formatNumber($arAttrs["sumcredit"]),
				"SUMM2PAY" => $this->formatNumber($arAttrs["sumtopay"]),
			);
			$id = CTszhAccountInstallments::Add($arFields);
			if ($id <= 0)
			{
				$this->ImportError(getMessage("TI_ERROR_ADD_ACCOUNT_INSTALLMENT", array("#ACCOUNT_ID#" => $accountNumber)), $arFields);
			}
		}
		return true;
	}

	/**
	 * ��������� ���������� ��� ����� �������� �����
	 * ORG -> PersAcc -> item (��� item_debt)
	 *
	 * @param int $accountPeriodID
	 * @param array $arChargesXML
	 * @param array $counter
	 * @param string $accountNumber
	 * @param bool $debt
	 */
	protected function __processCharges($accountPeriodID, $arChargesXML, &$counter, $accountNumber, $debt = false)
	{
		static $sortCounter = array();

		foreach ($arChargesXML as $idx => $arXMLCharge)
		{
			$xmlTag = $arXMLCharge['~attr'];
			if (strlen(trim($xmlTag['kod'])) == 0 && strlen($xmlTag['name']) == 0)
			{
				// ��������� ���������� �������� � 1� ��� �������� �������� � ������
				$xmlTag['kod'] = '-';
				$xmlTag['name'] = GetMessage("TSZH_DOLG_REMAINS_SERVICE");
			}

			$strServiceXML_ID = $arService["XML_ID"] = self::__makeServiceExternalID($xmlTag);
			$serviceID = false;
			if (array_key_exists($strServiceXML_ID, $this->state['SERVICES']))
			{
				$serviceID = $this->state['SERVICES'][$strServiceXML_ID];
			}

			if ($serviceID > 0)
			{
				$this->resetX();
				$arFields = Array(
					'SERVICE_ID' => $serviceID,
					'ACCOUNT_PERIOD_ID' => $accountPeriodID,
					"SORT" => (++$sortCounter[$accountPeriodID]) * 10,
					'AMOUNT' => $this->getValue("ammount", $xmlTag, true, "AMOUNT"),
					'SUMM' => $this->getValue("sum", $xmlTag, true, "SUMM"),
					'CORRECTION' => $this->getValue("correction", $xmlTag, true, "CORRECTION"),
					'COMPENSATION' => $this->getValue("compensation", $xmlTag, true, "COMPENSATION"),
					'SUMM_PAYED' => $this->getValue("sumpayed", $xmlTag, true, "SUMM_PAYED"),
					'SUMM2PAY' => $this->getValue("sumtopay", $xmlTag, true, "SUMM2PAY"),
					'DEBT_ONLY' => $debt ? "Y" : "N",
				);

				// is_set() ��������� ����� ��� ������������� �� ������� �������� ������: � ��� ���� ��� null � ���� �����, ����� �������� �� ���� �������
				if (is_set($xmlTag, "debtbeg"))
				{
					$arFields["DEBT_BEG"] = $this->getValue("debtbeg", $xmlTag, true, "DEBT_BEG");
				}
				if (is_set($xmlTag, "debtend"))
				{
					$arFields["DEBT_END"] = $this->getValue("debtend", $xmlTag, true, "DEBT_END");
				}

				// ���������������� ���� ����������
				$this->processUserFields("TSZH_CHARGE", 'uf_', $xmlTag, $arFields);

				if ($this->version >= 2)
				{
					$arFields['HAMOUNT'] = $this->getValue('hammount', $xmlTag, true, array("HAMOUNT", "AMOUNT"));
					$arFields['HNORM'] = $this->getValue('hnorm', $xmlTag, true, "HNORM");
					$arFields['HSUMM'] = $this->getValue('hsum', $xmlTag, true, array("HSUMM", "SUMM"));
					$arFields['HSUMM2PAY'] = $this->getValue('hsumtopay', $xmlTag, true, "HSUMM2PAY");
					$arFields['PENALTIES'] = $this->getValue('peni', $xmlTag, true, "PENALTIES");

					$componentValues = array(1 => 'Y', 2 => 'Z');
					$componentAttr = $this->getValue('component', $xmlTag, true);
					$arFields['COMPONENT'] = in_array($componentAttr, array_keys($componentValues)) ? $componentValues[$componentAttr] : 'N';

					// is_set() ��������� ����� ��� ������������� �� ������� �������� ������: � ��� ���� ��� null � ���� �����, ����� �������� �� ���� �������
					if (is_set($xmlTag, 'volumep'))
					{
						$arFields['VOLUMEP'] = $this->getValue('volumep', $xmlTag, true, "VOLUMEP");
					}
					if (is_set($xmlTag, 'volumeh'))
					{
						$arFields['VOLUMEH'] = $this->getValue('volumeh', $xmlTag, true, "VOLUMEH");
					}
					if (is_set($xmlTag, 'volumea'))
					{
						$arFields['VOLUMEA'] = $this->getValue('volumea', $xmlTag, true, "VOLUMEA");
					}
                    if (is_set($xmlTag, 'raise_multiplier'))
                    {
                        $arFields['RAISE_MULTIPLIER'] = $this->getValue('raise_multiplier', $xmlTag, true, "RAISE_MULTIPLIER");
                    }
                    if (is_set($xmlTag, 'raise_sum'))
                    {
                        $arFields['RAISE_SUM'] = $this->getValue('raise_sum', $xmlTag, true, "RAISE_SUM");
                    }
                    if (is_set($xmlTag, 'sum_without_raise'))
                    {
                        $arFields['SUM_WITHOUT_RAISE'] = $this->getValue('sum_without_raise', $xmlTag, true, "SUM_WITHOUT_RAISE");
                    }
                    if (is_set($xmlTag, 'csum_without_raise'))
                    {
                        $arFields['CSUM_WITHOUT_RAISE'] = $this->getValue('csum_without_raise', $xmlTag, true, "CSUM_WITHOUT_RAISE");
                    }

					// ��������� ����������� � ���������� ��������
					$arHMeterIds = array();
					$arNotImportedHMeters = array();
					foreach ($xmlTag as $attrName => $attrValue)
					{
						if (strlen($attrValue) <= 0)
						{
							continue;
						}

						// 1� ��������� � ���� ��������� ��������� ��������, ���������� ����� �������� (������ �16052)
						if (array_key_exists($attrValue, $this->deletedMeters))
						{
							continue;
						}

						if (preg_match('/^hmeter\d*$/', $attrName))
						{
							if (array_key_exists($attrValue, $this->state['HMETERS']))
							{
								$hMeter = $this->state['HMETERS'][$attrValue];
								if (is_array($hMeter))
								{
									$arNotImportedHMeters[] = $hMeter;
								}
								else
								{
									$arHMeterIds[] = $hMeter;
									$arFields["HMETER_IDS"][] = $hMeter;
								}
							}
							else
							{
								$counter["CHARGE_ERR"]++;
								$this->ImportError(GetMessage("TI_ERROR_CHARGE_METER_NOT_FOUND",
									array(
										"#ACCOUNT_ID#" => $accountNumber,
										"#SERVICE_KOD#" => $xmlTag["kod"],
										"#ATTR_NAME#" => $attrName,
										"#ATTR_VALUE#" => $attrValue,
									)),
									$xmlTag
								);
							}
						}
						elseif (preg_match('/^meter\d*$/', $attrName))
						{
							if (array_key_exists($attrValue, $this->state['ACCOUNT_METERS']))
							{
								$arFields["METER_IDS"][] = $this->state['ACCOUNT_METERS'][$attrValue];
							}
							else
							{
								$counter["CHARGE_ERR"]++;
								$this->ImportError(GetMessage("TI_ERROR_CHARGE_METER_NOT_FOUND",
									array(
										"#ACCOUNT_ID#" => $accountNumber,
										"#SERVICE_KOD#" => $xmlTag["kod"],
										"#ATTR_NAME#" => $attrName,
										"#ATTR_VALUE#" => $attrValue,
									)),
									$xmlTag
								);
							}
						}
					}
				}

				if (is_set($xmlTag, "group") && strlen($xmlTag['group']))
				{
					$arFields["GROUP"] = $xmlTag["group"];
				}
				if (is_set($xmlTag, "amountn") && strlen($xmlTag['amountn']))
				{
					$arFields["AMOUNTN"] = $xmlTag["amountn"];
				}
				if (is_set($xmlTag, "hamountn") && strlen($xmlTag['hamountn']))
				{
					$arFields["HAMOUNTN"] = $xmlTag["hamountn"];
				}
				if (is_set($xmlTag, "amount_view") && strlen($xmlTag['amount_view']))
				{
					$arFields["AMOUNT_VIEW"] = $xmlTag["amount_view"];
				}

				if (is_set($xmlTag, 'ammountNorm'))
				{
					$arFields['AMOUNT_NORM'] = $this->getValue('ammountNorm', $xmlTag, true, "AMOUNT_NORM");
				}

				// �������������� ����, ������� ����� ��������� X
				$additionalX = array("edizm" => "UNITS", "tarif" => "TARIFF", "tarif1" => "TARIFF", "tarif2" => "TARIFF", "tarif3" => "TARIFF", "norm" => "NORM");
				foreach ($additionalX as $attr => $field)
					$this->getValue($attr, $xmlTag, false, $field);
				// ==============================================
				$arFields["X_FIELDS"] = $this->getX();


				$CHARGE_ID = CTszhCharge::Add($arFields);
				if ($CHARGE_ID > 0)
				{
					$isError = false;

					if (!empty($arNotImportedHMeters))
					{
						$this->__processMeters(false, $arNotImportedHMeters, $counter, false, $arFields['ACCOUNT_ID']);
						$arHMeterIds = array();
						foreach ($arNotImportedHMeters as $arXMLMeterTag)
						{
							$arItem = $arXMLMeterTag['~attr'];
							if (is_set($this->state['HMETERS'], $arItem["kod"]) && !is_array($this->state['HMETERS'][$arItem["kod"]])
								&& ($hMeterId = $this->state['HMETERS'][$arItem["kod"]])
							)
							{
								$arHMeterIds[] = $hMeterId;
							}
						}
						if (!empty($arHMeterIds))
						{
							$res = CTszhCharge::bindMeter($CHARGE_ID, $arHMeterIds);
							if (!$res)
							{
								$isError = true;
								$counter["CHARGE_ERR"]++;
								$this->ImportError(getMessage(
									"TI_ERROR_CHARGE_BIND_HMETERS",
									array('#ACCOUNT_ID#' => $accountNumber, "#SERVICE_NAME#" => $xmlTag['name'])
								), $xmlTag);
							}
						}
					}

					if (!$isError)
					{
						$counter["CHARGE_ADD"]++;
					}
				}
				else
				{
					$counter["CHARGE_ERR"]++;
					$this->ImportError(str_replace('#ACCOUNT_ID#', $accountNumber, GetMessage("TI_ERROR_ADD_CHARGE")), $xmlTag);
				}
			}
			else
			{
				$counter["CHARGE_ERR"]++;
				$this->ImportError(str_replace('#ACCOUNT_ID#', $accountNumber, GetMessage("TI_ERROR_ADD_CHARGE_NO_SERVICE")), $xmlTag);
			}
		}
	}

	/**
	 * @param array|false $arAccount
	 * @param array $arMeterXML
	 * @param array $counter
	 * @param int|bool $accountNumber
	 * @param int|bool $accountId Id �������� ����� (��� ������� ����������� ���������)
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	protected function __processMeters($arAccount, $arMeterXML, &$counter, $accountNumber = false, $accountId = false)
	{
		global $DB;

		if (!is_array($arMeterXML) || empty($arMeterXML))
		{
			return;
		}

		$bHouseMeters = $arAccount === false;
		if (!$bHouseMeters)
		{
			$this->state['ACCOUNT_METERS'] = array();
		}
		$bOldVersion = COption::GetOptionInt('citrus.tszh', 'import.old.version', 0);

		foreach ($arMeterXML as $idx => $arXMLMeterTag)
		{
			$arItem = $arXMLMeterTag['~attr'];

			$dbEvents = GetModuleEvents(TSZH_MODULE_ID, "OnBeforeMeterImport");
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($arEvent = $dbEvents->Fetch())
				ExecuteModuleEventEx($arEvent, array(&$arItem, $bHouseMeters, $arAccount));

			// ��������� ��������� ��������, ����������� � �/� �� ������ �����������?
			if (COption::GetOptionString(TSZH_MODULE_ID, "shared_meters", "Y") == "Y")
			{
				$arFilter = array('XML_ID' => $arItem['kod']);
			}
			else
			{
				$arFilter = Array('TSZH_ID' => $this->tszhID, 'XML_ID' => $arItem['kod']);
			}

			if ($bHouseMeters)
			{
				$arFilter = array_merge($arFilter, Array("HOUSE_METER" => "Y"));
			}
			else
			{
				$arFilter = array_merge($arFilter, Array("HOUSE_METER" => "N"));
			}
			$arOldMeter = CTszhMeter::GetList(Array(), $arFilter)->Fetch();

			// �������� ��������� � ��������� del
			if (array_key_exists('del', $arItem))
			{
				// �������� ���� ��������� ���������, ����� ������������ �� � <item meter*> (������ �16052)
				$this->deletedMeters[$arItem['kod']] = true;

				if (is_array($arOldMeter))
				{
					CTszhMeter::Delete($arOldMeter["ID"], is_array($arAccount) ? $arAccount["ID"] : false);
					$counter["METER_DEL"]++;
				}
				continue;
			}

			if ($bHouseMeters && $accountId === false)
			{
				if (!is_array($arOldMeter))
				{
					$this->state['HMETERS'][$arItem['kod']] = $arXMLMeterTag;
					continue;
				}
			}

			$values_count = IntVal($arItem['value_count']);
			if ($values_count <= 0 || $values_count > 3)
			{
				$values_count = 1;
			}

			$arMeter = Array(
				'ACTIVE' => 'Y',
				'XML_ID' => $arItem['kod'],
				"SORT" => $idx * 10,
				'NAME' => $bOldVersion ? $arItem['service'] : $arItem['name'],
				'NUM' => is_set($arItem, "num") && strlen(trim($arItem['num'])) ? $arItem['num'] : false,
				'SERVICE_ID' => false,
				'SERVICE_NAME' => $bOldVersion ? $arItem['name'] : $arItem['service'],
				'VALUES_COUNT' => $values_count,
				"HOUSE_METER" => $bHouseMeters ? "Y" : "N",
			);
			if (is_set($arItem, 'precision'))
			{
				$arMeter["DEC_PLACES"] = $arItem['precision'];
			}
			if (is_set($arItem, 'capacity'))
			{
				$arMeter["CAPACITY"] = $arItem['capacity'];
			}
			if (is_set($arItem, 'verificationDate') && strlen($arItem['verificationDate']))
			{
				$arMeter["VERIFICATION_DATE"] = ConvertTimeStamp(MakeTimeStamp($arItem['verificationDate'], "DD.MM.YYYY"), "SHORT", $this->arTszh["SITE_ID"]);
			}

			if (array_key_exists('tarif', $arItem))
			{
				$arMeter['TARIFF1'] = $arItem["tarif"];
			}
			for ($i = 1; $i <= $values_count; $i++)
			{
				if (array_key_exists('tarif' . $i, $arItem))
				{
					$arMeter['TARIFF1'] = $this->formatNumber($arItem['tarif' . $i]);
				}
			}

			$meterID = false;
			if (!is_array($arOldMeter) || $arOldMeter['ID'] <= 0)
			{
				$meterID = CTszhMeter::Add($arMeter);
				if ($meterID > 0)
				{
					if ($bHouseMeters)
					{
						if ($accountId)
						{
							CTszhMeter::bindAccount($meterID, $accountId);
						}
					}
					else
					{
						CTszhMeter::bindAccount($meterID, $arAccount['ID']);
					}
					$counter["METER_ADD"]++;
				}
				else
				{
					$counter["METER_ERR"]++;
					$this->ImportError(str_replace('#ACCOUNT_ID#', $accountNumber, GetMessage($bHouseMeters ? "TI_ERROR_ADD_HMETER" : "TI_ERROR_ADD_METER")), $bException = true, $arItem);
				}
			}
			else
			{
				if (CTszhMeter::Update($arOldMeter['ID'], $arMeter))
				{
					$meterID = $arOldMeter['ID'];
					if ($bHouseMeters)
					{
						if ($accountId)
						{
							CTszhMeter::bindAccount($meterID, $accountId);
						}
					}
					else
					{
						CTszhMeter::bindAccount($meterID, $arAccount['ID']);
					}
					$counter["METER_UPD"]++;
				}
				else
				{
					$counter["METER_ERR"]++;
					$this->ImportError(str_replace('#ACCOUNT_ID#', $accountNumber, GetMessage($bHouseMeters ? "TI_ERROR_UPDATE_HMETER" : "TI_ERROR_UPDATE_METER", Array("#NAME#" => $arMeter['NAME']))), $bException = true, $arItem);
				}
			}

			if ($meterID)
			{
				// save in session for future use (while importing charges)
				if ($bHouseMeters)
				{
					$this->state['HMETERS'][$arMeter['XML_ID']] = $meterID;
				}
				else
				{
					$this->state['ACCOUNT_METERS'][$arMeter['XML_ID']] = $meterID;
				}

				// add meter ID to "imported" list, will be used in ::Cleanup();
				if ($this->useSessions)
				{
					$DB->Query("INSERT INTO " . $this->tableName . " (SESS_ID, PARENT_ID, RIGHT_MARGIN) values ('" . $DB->ForSql($this->sessionId) . "', 0, " . $meterID . ")");
				}
				else
				{
					$DB->Query("INSERT INTO " . $this->tableName . " (PARENT_ID, RIGHT_MARGIN) values (0, " . $meterID . ")");
				}

				// special for skype:xalker42
				if (array_key_exists('date', $arItem) && array_key_exists('indiccur1', $arItem))
				{
					$arValueFilter = Array(
						"METER_ID" => $meterID,
						"TIMESTAMP_X" => $arItem['date']
					);
					if (is_array($arAccount) && $arAccount['USER_ID'] > 0)
					{
						$arValueFilter['MODIFIED_BY'] = $arAccount['USER_ID'];
					}

					$arExistingMeterValue = CTszhMeterValue::GetList(
						Array(),
						$arValueFilter,
						false,
						false,
						Array('ID')
					)->Fetch();

					$curValueFields = $prevValueFields = Array(
						'METER_ID' => $meterID,
						'TIMESTAMP_X' => $arItem['date'],
					);
					// import user fields
					$arMeterFields = Array('kod', 'name', 'service', 'value_count', 'indiccur1', 'indiccur2', 'indiccur3', 'indicbef1', 'indicbef2', 'indicbef3', 'date', 'date_indicbef', 'date_indiccur');
					$arUserFields = array_diff_key($arItem, array_fill_keys($arMeterFields, 1));
					// ���������������� ���� ���������
					$this->processUserFields("TSZH_METER_VALUE", '', $arUserFields, $curValueFields);

					if (strtolower($arItem['addedbyowner']) == 'true' && $arAccount["USER_ID"] > 0)
					{
						$curValueFields['MODIFIED_BY'] = $arAccount['USER_ID'];
					}

					for ($i = 1; $i <= $values_count; $i++)
					{
						$curValueFields["VALUE$i"] = $this->formatNumber($arItem['indiccur' . $i]);
						if (array_key_exists("charge$i", $arItem))
						{
							$curValueFields["AMOUNT$i"] = strlen($arItem["charge$i"]) > 0 ? $this->formatNumber($arItem["charge$i"]) : false;
						}
					}

					if ($arExistingMeterValue)
					{
						$valueID = $arExistingMeterValue['ID'];
						if (!CTszhMeterValue::Update($valueID, $curValueFields))
						{
							$valueID = false;
						}
					}
					else
					{
						$valueID = CTszhMeterValue::Add($curValueFields);
					}

					if ($valueID > 0)
					{
						$counter["METER_VALUE_ADD"]++;
					}
					else
					{
						$counter["METER_VALUE_ERR"]++;
						$this->ImportError(str_replace('#ACCOUNT_ID#', $accountNumber, GetMessage("TI_ERROR_UPDATE_METER_VALUE")), $bException = true, $arItem);
					}
				}
				elseif (array_key_exists('indiccur1', $arItem) || array_key_exists('indicbef1', $arItem))
				{
					$prevValueFields = $curValueFields = Array(
						'METER_ID' => $meterID,
					);

					// �������� � ���������� ��������� ���������������� ����
					$arMeterFields = Array('kod', 'name', 'service', 'value_count', 'indiccur1', 'indiccur2', 'indiccur3', 'indicbef1', 'indicbef2', 'indicbef3', 'charge1', 'charge2', 'charge3', 'date', 'date_indicbef', 'date_indiccur');
					$arUserFields = array_diff_key($arItem, array_fill_keys($arMeterFields, 1));
					$this->processUserFields("TSZH_METER_VALUE", '', $arUserFields, $curValueFields);

					// TODO ������� ��� ������ ��� �����������
					$arPeriodTimestamp = ParseDateTime($this->sPeriod, "YYYY-MM-DD");
					$strMonth = $arPeriodTimestamp["YYYY"] . '-' . $arPeriodTimestamp["MM"] . "-01";
					$timeMonth = MakeTimeStamp($strMonth, "YYYY-MM-DD");
					$timePrevMonth = strtotime("-1 day", $timeMonth);

					$hasPrevValue = is_set($arItem, "indicbef1");
					// ���������� ���������
					// ====================
					if ($hasPrevValue)
					{

						// ���� ���������
						if (is_set($arItem, "date_indicbef"))
						{
							$prevValueTime = MakeTimeStamp($arItem["date_indicbef"], "DD.MM.YYYY HH:MI:SS");
						}
						else
						{
							$prevValueTime = $timePrevMonth;
						}
						$prevValueFields["TIMESTAMP_X"] = ConvertTimeStamp($prevValueTime, "FULL");

						$arExistingPrevValue = CTszhMeterValue::GetList(
							Array(),
							Array(
								"METER_ID" => $meterID,
								"TIMESTAMP_X" => $prevValueFields["TIMESTAMP_X"]
							),
							false,
							array("nTopCount" => 1),
							Array('ID')
						)->Fetch();
						$prevValueId = is_array($arExistingPrevValue) ? $arExistingPrevValue["ID"] : false;

						for ($i = 1; $i <= $values_count; $i++)
							$prevValueFields["VALUE$i"] = $this->formatNumber($arItem['indicbef' . $i]);

						if ($prevValueId)
						{
							$success = CTszhMeterValue::Update($prevValueId, $prevValueFields);
						}
						else
						{
							$success = CTszhMeterValue::Add($prevValueFields) > 0;
						}

						if ($success)
						{
							$counter["METER_VALUE_ADD"]++;
						}
						else
						{
							$counter["METER_VALUE_ERR"]++;
							$this->ImportError(str_replace('#METER_XML_ID#', $arMeter["XML_ID"], GetMessage("TI_ERROR_UPDATE_METER_VALUE")), $bException = true, $arItem);
						}
					}

					$hasCurValue = is_set($arItem, "indiccur1");
					// ������� ���������
					// =================
					if ($hasCurValue)
					{

						// ���� ���������
						if (is_set($arItem, "date_indiccur"))
						{
							$curValueTime = MakeTimeStamp($arItem["date_indiccur"], "DD.MM.YYYY HH:MI:SS");
						}
						else
						{
							$curValueTime = $timeMonth;
						}
						$curValueFields["TIMESTAMP_X"] = ConvertTimeStamp($curValueTime, "FULL");

						$arExistingCurValue = CTszhMeterValue::GetList(
							Array(),
							Array(
								"METER_ID" => $meterID,
								"TIMESTAMP_X" => $curValueFields["TIMESTAMP_X"]
							),
							false,
							array("nTopCount" => 1),
							Array('ID')
						)->Fetch();
						$curValueId = is_array($arExistingCurValue) ? $arExistingCurValue["ID"] : false;

						for ($i = 1; $i <= $values_count; $i++)
						{
							$curValueFields["VALUE$i"] = $this->formatNumber($arItem['indiccur' . $i]);
							if (array_key_exists("charge$i", $arItem) && strlen($arItem["charge$i"]) > 0)
							{
								$curValueFields["AMOUNT$i"] = $this->formatNumber($arItem["charge$i"]);
							}
						}

						if ($curValueId)
						{
							$success = CTszhMeterValue::Update($curValueId, $curValueFields);
						}
						else
						{
							$success = CTszhMeterValue::Add($curValueFields) > 0;
						}

						if ($success)
						{
							$counter["METER_VALUE_ADD"]++;
						}
						else
						{
							$counter["METER_VALUE_ERR"]++;
							$this->ImportError(str_replace('#METER_XML_ID#', $arMeter["XML_ID"], GetMessage("TI_ERROR_UPDATE_METER_VALUE")), $bException = true, $arItem);
						}

					}
				}
			}
		}
	}

	/**
	 * @param array $arBarcodesXML
	 * @return array
	 * @internal param int $accountPeriodID
	 */
	protected function __processBarcodes($arBarcodesXML)
	{
		$barcodes = array();
		foreach ($arBarcodesXML as $idx => $barcodeXML)
		{
			$attrs = $barcodeXML["~attr"];
			$barcodes[] = Array(
				"TYPE" => $attrs["type"],
				"VALUE" => trim($barcodeXML["VALUE"]),
			);
		}
		return $barcodes;
	}

	/**
	 *
	 * @param string $action
	 * @return array
	 * @throws Exception
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public function Cleanup($action)
	{
		global $DB;

		$counter = array();

		if (!in_array($this->filetype, array("accounts", "calculations")))
		{
			return $counter;
		}

		if ($this->updateMode || $action != "D" && $action != "A")
		{
			return $counter;
		}

		if ($this->onlyDebt)
		{
			return $counter;
		}

		$bDelete = $action == "D";
		$bUserDeactivate = $action == "A";

		//This will protect us from deactivating when next_step is lost
		if (intval($this->state["ACCOUNTS_IMPORT_STARTED"] < 1))
		{
			return $counter;
		}

		if (!array_key_exists("LAST_METER_ID", $this->state))
		{
			// deactivate/delete accounts/users
			$arFilter = array(
				">ID" => $this->state["LAST_ID"],
				"TSZH_ID" => $this->tszhID,
			);
			if (!$bDelete)
			{
				$arFilter["USER_ACTIVE"] = "Y";
			}

			$obAccount = new CTszhAccount();
			$rsAccount = $obAccount->GetList(
				Array("ID" => "asc"),
				$arFilter,
				false, false,
				Array("ID", "USER_ID", "USER_ACTIVE", "TSZH_ID")
			);

			$obUser = new CUser();
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($arAccount = $rsAccount->Fetch())
			{
				$rs = $DB->Query("select ID from " . $this->tableName . " where PARENT_ID+0 = 0 AND LEFT_MARGIN = " . $arAccount["ID"] . $this->andWhereThisSession);
				if (!$rs->Fetch())
				{
					if ($bDelete)
					{
						CTszhAccount::Delete($arAccount["ID"], true);
						$counter["DEL"]++;
					}
					elseif ($bUserDeactivate && $arAccount["USER_ID"] != 1) // won't deactivate admin account
					{
						//CTszhAccount::Update($arAccount["ID"], Array("ACTIVE" => "N"));
						$obUser->Update($arAccount["USER_ID"], Array("ACTIVE" => "N"));
						$counter["DEA"]++;
					}
				}
				else
				{
					$counter["NON"]++;
				}

				$this->state["LAST_ID"] = $arAccount["ID"];

				if ($this->timeIsUp())
				{
					return $counter;
				}
			}
		}

		// deactivate/delete meters
		$arFilter = array(
			">ID" => $this->state["LAST_METER_ID"],
			"TSZH_ID" => $this->tszhID,
		);
		$obMeter = new CTszhMeter;
		$rsMeter = $obMeter->GetList(
			Array("ID" => "asc"),
			$arFilter,
			false, false,
			Array("ID", "TSZH_ID")
		);

		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arMeter = $rsMeter->Fetch())
		{
			$rs = $DB->Query("select ID from " . $this->tableName . " where ID > 1 and PARENT_ID+0 = 0 AND RIGHT_MARGIN = " . $arMeter["ID"] . $this->andWhereThisSession);
			if (!$rs->Fetch())
			{
				if ($bDelete)
				{
					CTszhMeter::Delete($arMeter["ID"], true);
					$counter["METER_DEL"]++;
				}
				else
				{
					$obMeter->Update($arMeter["ID"], Array("ACTIVE" => "N"));
					$counter["METER_DEA"]++;
				}
			}
			else
			{
				$counter["METER_NON"]++;
			}

			$this->state["LAST_METER_ID"] = $arMeter["ID"];

			if ($this->timeIsUp())
			{
				return $counter;
			}
		}

		return $counter;
	}

	public function clearSession()
	{
		global $DB;

		if (!$this->useSessions)
		{
			return;
		}

		// ������ ��� ������ ������� ������
		if ($DB->TableExists($this->tableName))
		{
			$DB->Query("DELETE from " . $this->tableName . " WHERE SESS_ID = '" . $DB->ForSQL($this->sessionId) . "'");
			// ��������� ������� ������ 1 ����
			$this->xmlFile->EndSession();
		}
	}

	/**
	 * �������� ������������ email
	 *
	 * @param string $email
	 * @return bool
	 */
	protected function check_email($email)
	{
		$email = trim($email);

		if (preg_match("#.*?[<\[\(](.*?)[>\]\)].*#i", $email, $arr) && strlen($arr[1]) > 0)
		{
			$email = $arr[1];
		}

		if (preg_match('/^[a-z0-9\._-]+@[a-z0-9\._-]+\.(xn--)?[a-z0-9]{2,}$/i', $email))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * ������������� ����� ������ ���������. �������� ������������ ��������� ���� �� ������ ����������� �������
	 */
	protected function setStartTime()
	{
		if (isset($this->startTime))
		{
			return;
		}

		if (defined("START_EXEC_PROLOG_BEFORE_1"))
		{
			list($usec, $sec) = explode(" ", START_EXEC_PROLOG_BEFORE_1);
			$this->startTime = $sec + $usec;
		}
		else
		{
			$this->startTime = microtime(true);
		}
	}

	/**
	 * ��������� �� ������� �� �����, ���������� �� ���� ��� (���) ������
	 * @return bool
	 * @throws Exception ���� ����� ���� �� ��� ������ ����� setStartTime
	 */
	protected function timeIsUp()
	{
		return microtime(true) - $this->startTime > $this->timeLimit;
	}
}

