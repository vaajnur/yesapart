<?php

IncludeModuleLangFile(__FILE__);

/**
 * ����� ��� ������ � ������������� (������������ �����, �������������)
 */
class CTszhContractor
{
	// ������� ��, ���������� ������
	const TABLE_NAME = 'b_tszh_contractors';
	// �������� ������ � ��������� ��������, ���� ������������ ���������������� ���� �������� ������; false, ���� �� ������������
	const USER_FIELD_ENTITY = 'TSZH_CONTRACTOR';

	/**
	 * @param array $arFields
	 * @param int $ID
	 *
	 * @return bool
	 */
	private static function CheckFields(&$arFields, $ID = 0)
	{
		global $APPLICATION, $USER_FIELD_MANAGER;

		if (array_key_exists('ID', $arFields))
		{
			unset($arFields['ID']);
		}

		$arErrors = Array();

		if ($ID == 0 || array_key_exists("NAME", $arFields))
		{
			if (strlen($arFields['NAME']) <= 0)
			{
				$arErrors[] = Array('id' => 'TSZH_ERROR_CONTRACTORS_NO_NAME', 'text' => GetMessage("TSZH_ERROR_CONTRACTORS_NO_NAME"));
			}
		}

		if ($ID == 0 || array_key_exists("TSZH_ID", $arFields))
		{
			if (IntVal($arFields['TSZH_ID']) <= 0)
			{
				$arErrors[] = Array('id' => 'TSZH_ERROR_CONTRACTORS_NO_TSZH_ID', 'text' => GetMessage("TSZH_ERROR_CONTRACTORS_NO_TSZH_ID"));
			}
		}

		if ($ID == 0 || array_key_exists("EXECUTOR", $arFields))
		{
			if ($arFields["EXECUTOR"] && !in_array($arFields["EXECUTOR"], array("Y", "N", "Z")))
			{
				$arErrors[] = Array('id' => 'TSZH_ERROR_CONTRACTORS_WRONG_EXECUTOR', 'text' => GetMessage("TSZH_ERROR_CONTRACTORS_WRONG_EXECUTOR"));
			}
			if (!$arFields["EXECUTOR"])
			{
				$arFields["EXECUTOR"] = "N";
			}
		}

		if (!empty($arErrors))
		{
			$e = new CAdminException($arErrors);
			$APPLICATION->ThrowException($e);

			return false;
		}

		if (self::USER_FIELD_ENTITY && !$USER_FIELD_MANAGER->CheckFields(self::USER_FIELD_ENTITY, $ID, $arFields))
		{
			return false;
		}

		return true;
	}

	/**
	 * ���������� ������ ���������� �����
	 *
	 * @param array $arFields ������ � ������ ������ ����������
	 *
	 * @return int ID ������������ ����������
	 */
	public static function Add($arFields)
	{
		global $USER_FIELD_MANAGER;

		if (!self::CheckFields($arFields))
		{
			return false;
		}

		$ID = CDatabase::Add(self::TABLE_NAME, $arFields);
		if (self::USER_FIELD_ENTITY && $ID > 0 && CTszh::hasUserFields($arFields))
		{
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		return $ID;
	}

	/**
	 * ���������� ������������� ���������� �����
	 *
	 * @param int $ID ������������� ���������� �����
	 * @param array $arFields ������ � ������
	 *
	 * @return bool|CDBResult � ������ ������ ����������� false, �������� ������ ����� �������� ����� $APPLICATION->GetException()
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Update($ID, $arFields)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (!self::CheckFields($arFields, $ID))
		{
			return false;
		}

		$strUpdate = $DB->PrepareUpdate(self::TABLE_NAME, $arFields);
		$strSql =
			"UPDATE " . self::TABLE_NAME . " SET " .
			$strUpdate .
			" WHERE ID=" . $ID;

		$bSuccess = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		if (self::USER_FIELD_ENTITY && $bSuccess && CTszh::hasUserFields($arFields))
		{
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		return $bSuccess;
	}

	/**
	 * �������� ���������� �����
	 *
	 * @param int $ID ������������� ���������� �����
	 *
	 * @return boolean � ������ ������ ����������� false, �������� ������ ����� �������� ����� $APPLICATION->GetException()
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Delete($ID)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = intval($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		$DB->StartTransaction();

		$strSql = 'DELETE FROM b_tszh_accounts_contractors WHERE CONTRACTOR_ID=' . $ID;
		$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		$strSql = 'DELETE FROM ' . self::TABLE_NAME . ' WHERE ID=' . $ID;
		$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		if (self::USER_FIELD_ENTITY)
		{
			$USER_FIELD_MANAGER->Delete(self::USER_FIELD_ENTITY, $ID);
		}

		$DB->Commit();

		return true;
	}

	/**
	 * ��������� ������ ����������� ����� �� �������� ���������
	 *
	 * *������ ��������� �����:*
	 * ID � ������������� ����������
	 * XML_ID � ������� ��� (ID � 1�)
	 * TSZH_ID � ID ������� ���������� (���)
	 * EXECUTOR � �������� ������������-������������ ����� (Y - ��������, Z - ������������ �������� ����������� ������������/�������� ����������, N - �� ��������)
	 * NAME � ������������ �����������
	 * SERVICES � ������ ��������������� ����� (������)
	 * PHONE � ����� ��������
	 * BILLING � ��������� ���������
	 *
	 * @param array $arOrder ����������� ����������. ������, ������� �������� �������� ���� �����, ���������� � ����������� asc ��� desc.
	 * @param array $arFilter ������, �������� ������ �� ������������ ������. ������� � ������� �������� �������� �����, � ���������� � �� ��������.
	 * @param array|bool $arGroupBy ������, �������� ����������� ��������������� ������. ���� �������� �������� ������ �������� �����, �� �� ���� ����� ����� ����������� �����������. ���� �������� �������� ������ ������, �� ����� ������ ���������� �������, ��������������� �������. �� ��������� �������� ����� false � �� ������������.
	 * @param array|bool $arNavStartParams ������, �������� ������� ������ ��� ����������� ������������ ���������.
	 * @param array $arSelectFields ������, ���������� ������ �����, ������� ������ ������������� � ���������� �������
	 *
	 * @return CDBResult|array ����� ���������� ������ ���� CDBResult, ���������� ������, ��������������� ������� �������. ��������� ������ ��� �������� � ������������
	 */
	public static function GetList($arOrder = Array(), $arFilter = Array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = Array())
	{
		global $USER_FIELD_MANAGER;

		static $arFields = array(
			"ID" => Array("FIELD" => "T.ID", "TYPE" => "int"),
			"XML_ID" => Array("FIELD" => "T.XML_ID", "TYPE" => "string"),
			"TSZH_ID" => Array("FIELD" => "T.TSZH_ID", "TYPE" => "int"),
			"EXECUTOR" => Array("FIELD" => "T.EXECUTOR", "TYPE" => "char"),
			"NAME" => Array("FIELD" => "T.NAME", "TYPE" => "string"),
			"SERVICES" => Array("FIELD" => "T.SERVICES", "TYPE" => "string"),
			"ADDRESS" => Array("FIELD" => "T.ADDRESS", "TYPE" => "string"),
			"PHONE" => Array("FIELD" => "T.PHONE", "TYPE" => "string"),
			"BILLING" => Array("FIELD" => "T.BILLING", "TYPE" => "string"),

			"INN" => Array("FIELD" => "T.INN", "TYPE" => "string"),
			"KPP" => Array("FIELD" => "T.KPP", "TYPE" => "string"),
			"RSCH" => Array("FIELD" => "T.RSCH", "TYPE" => "string"),
			"BANK" => Array("FIELD" => "T.BANK", "TYPE" => "string"),
			"KSCH" => Array("FIELD" => "T.KSCH", "TYPE" => "string"),
			"BIK" => Array("FIELD" => "T.BIK", "TYPE" => "string"),
		);

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = array_keys($arFields);
		}
		elseif (in_array('*', $arSelectFields))
		{
			$arSelectFields = array_keys($arFields);
		}

		if (array_key_exists('BILLING', $arSelectFields))
		{
			$arSelectFields = array_merge($arSelectFields, array(
				"INN",
				"KPP",
				"RSCH",
				"BANK",
				"KSCH",
				"BIK",
			));
		}

		if (self::USER_FIELD_ENTITY)
		{
			$obUserFieldsSql = new CUserTypeSQL;
			$obUserFieldsSql->SetEntity(self::USER_FIELD_ENTITY, "T.ID");
			$obUserFieldsSql->SetSelect($arSelectFields);
			$obUserFieldsSql->SetFilter($arFilter);
			$obUserFieldsSql->SetOrder($arOrder);

			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . ' T', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields, $obUserFieldsSql, $strUserFieldsID = "T.ID");
		}
		else
		{
			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . ' T', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
		}

		if (self::USER_FIELD_ENTITY && is_object($dbRes))
		{
			$dbRes->SetUserFields($USER_FIELD_MANAGER->GetUserFields(self::USER_FIELD_ENTITY));
		}

		return new CTszhContractorResult($dbRes);
	}

	/**
	 * ��������� ����� ���������� � ��������������� $ID
	 *
	 * @param int $ID
	 *
	 * @return array|bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function GetByID($ID)
	{
		$ID = intval($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		return self::GetList(Array(), Array("ID" => $ID))->GetNext();
	}

}