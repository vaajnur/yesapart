<?php

IncludeModuleLangFile(__FILE__);

/**
 * CTszhAccountHistory
 * ������� ��������� ������� ������
 *
 * ����
 * ----
 * ID            - ID
 * ACCOUNT_ID    - ID ��������
 * TIMESTAMP_X   - ���� � ����� ���������
 * FIELDS        - ������ �������� ������ �/�
 *
 */
class CTszhAccountHistory
{
	/**
	 * ���������� ���� ������ ������� � ��������� ID
	 *
	 * @param int $ID
	 *
	 * @return array|false
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function GetByID($ID)
	{
		$ID = intval($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		return self::GetList(Array(), Array("ID" => $ID), false, false, Array('*'))->GetNext();
	}

	/**
	 * ��������� ������ ������� �� ������� ��������� ������� ������ �� �������
	 *
	 * @param array $arOrder
	 * @param array $arFilter
	 * @param array|bool $arGroupBy
	 * @param array|bool $arNavStartParams
	 * @param array $arSelectFields
	 *
	 * @return CDBResult|array ������ ��� ������������� �����������, CDBResult �� ���� ��������� �������
	 */
	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		static $arFields = array(
			"ID" => Array("FIELD" => "UAH.ID", "TYPE" => "int"),
			"ACCOUNT_ID" => Array("FIELD" => "UAH.ACCOUNT_ID", "TYPE" => "int"),
			"TIMESTAMP_X" => Array("FIELD" => "UAH.TIMESTAMP_X", "TYPE" => "datetime"),
			"FIELDS" => Array("FIELD" => "UAH.FIELDS", "TYPE" => "string"),
		);

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = array_keys($arFields);
		}

		$dbRes = CTszh::GetListMakeQuery('b_tszh_accounts_history UAH', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

		return $dbRes;
	}

	/**
	 * ��������� ������������ ��������� ����� ��� ������ �������
	 *
	 * @param array $arFields
	 * @param int $ID
	 * @param string $strOperation
	 *
	 * @return bool
	 */
	public static function CheckFields(&$arFields, $ID = 0, $strOperation = 'ADD')
	{
		global $APPLICATION, $DB;

		if (array_key_exists("ACCOUNT_ID", $arFields) || $strOperation == 'ADD')
		{
			$arFields['ACCOUNT_ID'] = IntVal($arFields['ACCOUNT_ID']);
			if ($arFields["ACCOUNT_ID"] <= 0)
			{
				$APPLICATION->ThrowException(GetMessage("TSZH_ERROR_WRONG_ACCOUNT_ID"), "TSZH_ERROR_WRONG_ACCOUNT_ID");

				return false;
			}
		}

		if (array_key_exists("FIELDS", $arFields) || $strOperation == 'ADD')
		{
			if (!is_array($arFields['FIELDS']))
			{
				$APPLICATION->ThrowException(GetMessage("TSZH_ERROR_WRONG_HISTORY_FIELDS"), "TSZH_ERROR_WRONG_HISTORY_FIELDS");

				return false;
			}
			$arFields['FIELDS'] = serialize($arFields['FIELDS']);
		}

		if (array_key_exists('TIMESTAMP_X', $arFields))
		{
			if (!$DB->IsDate($arFields['TIMESTAMP_X'], false, false, "FULL"))
			{
				$APPLICATION->ThrowException(GetMessage("TSZH_ERROR_WRONG_HISTORY_DATE"), "TSZH_ERROR_WRONG_HISTORY_DATE");

				return false;
			}
		}
		elseif ($strOperation == "ADD")
		{
			$arFields['TIMESTAMP_X'] = ConvertTimeStamp(false, "FULL");
		}

		unset($arFields['ID']);

		return true;
	}

	/**
	 * ��������� ����� ������ � ������� ��������� ������� ������
	 *
	 * @param mixed $arFields ���� ������ ��� ����������
	 *
	 * @return int
	 */
	public static function Add($arFields)
	{
		if (!self::CheckFields($arFields, 0, 'ADD', false))
		{
			return false;
		}

		return CDatabase::Add("b_tszh_accounts_history", $arFields);
	}

	/**
	 * �������� ������ � ������� ������� ������
	 *
	 * @param int $ID ID ������
	 * @param array $arFields
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Update($ID, $arFields)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (!self::CheckFields($arFields, $ID, 'UPDATE'))
		{
			return false;
		}

		$strSql = "UPDATE b_tszh_accounts_history SET " . $DB->PrepareUpdate("b_tszh_accounts_history", $arFields) . " WHERE ID=" . $ID;

		return (bool)$DB->Query($strSql, true, "File: " . __FILE__ . "<br>Line: " . __LINE__);
	}

	/**
	 * ������� ������ � ������� � ��������� ID
	 *
	 * @param mixed $ID ID ������
	 *
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 * @return bool
	 */
	public static function Delete($ID)
	{
		global $DB;
		$ID = intval($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		$strSql = "DELETE FROM b_tszh_accounts_history WHERE ID=" . $ID;

		return (bool)$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);
	}

	/**
	 * ����� ��������������� ���������� �� ��������� ���� ���� �������� ����� �� �������
	 *
	 * @param array $arAccountFields ������� ���� �������� �����
	 * @param array $accountID ID �������� �����
	 * @param string $strDate ���� ��� ������� ����� �������� ���������� �������� (� ������� ��������� ����� ��� �������� ����� ���������������� �����)
	 *
	 * @return bool true, ���� ���� ���� ������������� �� �������, false, ���� ���������� ������ � ������� �� ���� �������
	 */
	public static function CorrectFromHistory(&$arAccountFields, $accountID, $strDate)
	{
		global $DB;

		$accountID = IntVal($accountID);
		if ($accountID <= 0)
		{
			return false;
		}
		if (!$DB->IsDate($strDate))
		{
			return false;
		}
		$rsHistory = self::GetList(Array("TIMESTAMP_X" => "DESC", "ID" => "DESC"), Array(
			"ACCOUNT_ID" => $accountID,
			"<=TIMESTAMP_X" => $strDate
		), false, Array('nTopCount' => 1));
		/** @noinspection PhpAssignmentInConditionInspection */
		if ($arHistory = $rsHistory->GetNext())
		{
			$arHistoryFields = unserialize($arHistory["~FIELDS"]);
			if (is_array($arHistoryFields))
			{
				$arAccountFields = array_merge($arAccountFields, $arHistoryFields);

				return true;
			}
		}

		return false;
	}
}