<?php

IncludeModuleLangFile(__FILE__);

/**
 * ��������� ����������� � �����������
 *
 * @see CTszhContractor
 * @see CTszhAccountPeriod
 */
class CTszhAccountContractor
{
	// ������� ��, ���������� ������
	const TABLE_NAME = 'b_tszh_accounts_contractors';
	// �������� ������ � ��������� ��������, ���� ������������ ���������������� ���� �������� ������; false, ���� �� ������������
	const USER_FIELD_ENTITY = false;

	/**
	 * @param array $arFields
	 * @param int $ID
	 *
	 * @return bool
	 */
	private static function CheckFields(&$arFields, $ID = 0)
	{
		global $APPLICATION, $USER_FIELD_MANAGER;

		if (array_key_exists('ID', $arFields))
		{
			unset($arFields['ID']);
		}

		$arErrors = Array();

		if ($ID == 0 || array_key_exists("ACCOUNT_PERIOD_ID", $arFields))
		{
			if (IntVal($arFields['ACCOUNT_PERIOD_ID']) <= 0)
			{
				$arErrors[] = Array('id' => 'TSZH_ERROR_AC_NO_ACCOUNT_PERIOD', 'text' => GetMessage("TSZH_ERROR_AC_NO_ACCOUNT_PERIOD"));
			}
		}

		if ($ID == 0 || array_key_exists("CONTRACTOR_ID", $arFields))
		{
			if (IntVal($arFields['CONTRACTOR_ID']) <= 0)
			{
				$arErrors[] = Array('id' => 'TSZH_ERROR_AC_NO_CONTRACTOR', 'text' => GetMessage("TSZH_ERROR_AC_NO_CONTRACTOR"));
			}
		}

		if (!empty($arErrors))
		{
			$e = new CAdminException($arErrors);
			$APPLICATION->ThrowException($e);

			return false;
		}

		if (self::USER_FIELD_ENTITY && $ID > 0)
		{
			if (!$USER_FIELD_MANAGER->CheckFields(self::USER_FIELD_ENTITY, $ID, $arFields))
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * ���������� ������ ���������� �����
	 *
	 * @param array $arFields ������ � ������ ������ ����������
	 *
	 * @return int ID ������������ ����������
	 */
	public static function Add($arFields)
	{
		global $USER_FIELD_MANAGER;

		if (!self::CheckFields($arFields))
		{
			return false;
		}

		$ID = CDatabase::Add(self::TABLE_NAME, $arFields);
		if (self::USER_FIELD_ENTITY && $ID > 0)
		{
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		return $ID;
	}

	/**
	 * ���������� ������������� ���������� �����
	 *
	 * @param int $ID ������������� ���������� �����
	 * @param array $arFields ������ � ������ ���������� �����
	 *
	 * @return bool|CDBResult � ������ ������ ����������� false, �������� ������ ����� �������� ����� $APPLICATION->GetException()
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function Update($ID, $arFields)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentException("No \$ID given", "ID");
		}

		if (!self::CheckFields($arFields, $ID))
		{
			return false;
		}

		$strUpdate = $DB->PrepareUpdate(self::TABLE_NAME, $arFields);
		$strSql =
			"UPDATE " . self::TABLE_NAME . " SET " .
			$strUpdate .
			" WHERE ID=" . $ID;

		$bSuccess = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		if (self::USER_FIELD_ENTITY && $bSuccess)
		{
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		return $bSuccess;
	}

	/**
	 * �������� ���������� �����
	 *
	 * @param int $ID ������������� ���������� �����
	 *
	 * @return boolean � ������ ������ ����������� false, �������� ������ ����� �������� ����� $APPLICATION->GetException()
	 */
	public static function Delete($ID)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = intval($ID);
		if ($ID <= 0)
		{
			return false;
		}

		$DB->StartTransaction();

		$strSql = 'DELETE FROM ' . self::TABLE_NAME . ' WHERE ID=' . $ID;
		$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		if (self::USER_FIELD_ENTITY)
		{
			$USER_FIELD_MANAGER->Delete(self::USER_FIELD_ENTITY, $ID);
		}

		$DB->Commit();

		return true;
	}

	/**
	 * ��������� ������ ����������� ����� �� �������� ���������
	 *
	 * *������ ��������� �����:*
	 * ID � ������������� ����������
	 * ACCOUNT_PERIOD_ID � ID ������� �/� (���������)
	 * CONTRACTOR_ID � ID ����������
	 * DEBT_BEG - ������������� �� ������� �������
	 * SUMM_PAYED - �������� �� ������
	 * PENALTIES - ������, ���� �� ������
	 * SUMM_CHARGED - ��������� �� ������
	 * SUMM � ����� � ������
	 * RECEIPT_ORDER � ���������� ����� ���������� � ���������
	 * *������ ��������� ����� ����������:*
	 * CONTRACTOR_XML_ID � ������� ��� (ID � 1�)
	 * CONTRACTOR_TSZH_ID � ID ������� ���������� (���)
	 * CONTRACTOR_EXECUTOR � �������� ������������ (Y/N)
	 * CONTRACTOR_NAME � ������������ �����������
	 * CONTRACTOR_SERVICES � ������ ��������������� ����� (������)
	 * CONTRACTOR_ADDRESS - �����
	 * CONTRACTOR_PHONE � ����� ��������
	 * CONTRACTOR_BILLING � ��������� ���������
	 *
	 * @param array $arOrder ����������� ����������. ������, ������� �������� �������� ���� �����, ���������� � ����������� asc ��� desc.
	 * @param array $arFilter ������, �������� ������ �� ������������ ������. ������� � ������� �������� �������� �����, � ���������� � �� ��������.
	 * @param array|bool $arGroupBy ������, �������� ����������� ��������������� ������. ���� �������� �������� ������ �������� �����, �� �� ���� ����� ����� ����������� �����������. ���� �������� �������� ������ ������, �� ����� ������ ���������� �������, ��������������� �������. �� ��������� �������� ����� false � �� ������������.
	 * @param array|bool $arNavStartParams ������, �������� ������� ������ ��� ����������� ������������ ���������.
	 * @param array $arSelectFields ������, ���������� ������ �����, ������� ������ ������������� � ���������� �������
	 *
	 * @return CDBResult|array ����� ���������� ������ ���� CDBResult, ���������� ������, ��������������� ������� �������. ��������� ������ ��� �������� � ������������
	 */
	public static function GetList($arOrder = Array(), $arFilter = Array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = Array())
	{
		global $USER_FIELD_MANAGER;

		static $arFields = array(
			"ID" => Array("FIELD" => "T.ID", "TYPE" => "int"),
			"ACCOUNT_PERIOD_ID" => Array("FIELD" => "T.ACCOUNT_PERIOD_ID", "TYPE" => "int"),
			"CONTRACTOR_ID" => Array("FIELD" => "T.CONTRACTOR_ID", "TYPE" => "int"),
			"DEBT_BEG" => Array("FIELD" => "T.DEBT_BEG", "TYPE" => "float"),
			"SUMM_PAYED" => Array("FIELD" => "T.SUMM_PAYED", "TYPE" => "float"),
			"PENALTIES" => Array("FIELD" => "T.PENALTIES", "TYPE" => "float"),
			"SUMM_CHARGED" => Array("FIELD" => "T.SUMM_CHARGED", "TYPE" => "float"),
			"SUMM" => Array("FIELD" => "T.SUMM", "TYPE" => "float"),
			"RECEIPT_ORDER" => Array("FIELD" => "T.RECEIPT_ORDER", "TYPE" => "int"),
			"SERVICES" => Array("FIELD" => "T.SERVICES", "TYPE" => "string"),
			"BARCODE_TYPE" => Array("FIELD" => "T.BARCODE_TYPE", "TYPE" => "string"),
			"BARCODE_VALUE" => Array("FIELD" => "T.BARCODE_VALUE", "TYPE" => "string"),
			"IS_OVERHAUL" => Array("FIELD" => "T.IS_OVERHAUL", "TYPE" => "int"),

			"CONTRACTOR_XML_ID" => Array(
				"FIELD" => "TC.XML_ID",
				"TYPE" => "string",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),
			"CONTRACTOR_TSZH_ID" => Array(
				"FIELD" => "TC.TSZH_ID",
				"TYPE" => "int",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),
			"CONTRACTOR_EXECUTOR" => Array(
				"FIELD" => "TC.EXECUTOR",
				"TYPE" => "char",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),
			"CONTRACTOR_NAME" => Array(
				"FIELD" => "TC.NAME",
				"TYPE" => "string",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),
			"CONTRACTOR_SERVICES" => Array(
				"FIELD" => "IFNULL(T.SERVICES,TC.SERVICES)",
				"TYPE" => "string",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),
			"CONTRACTOR_ADDRESS" => Array(
				"FIELD" => "TC.ADDRESS",
				"TYPE" => "string",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),
			"CONTRACTOR_PHONE" => Array(
				"FIELD" => "TC.PHONE",
				"TYPE" => "string",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),
			"CONTRACTOR_BILLING" => Array(
				"FIELD" => "TC.BILLING",
				"TYPE" => "string",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),

			"CONTRACTOR_INN" => Array(
				"FIELD" => "TC.INN",
				"TYPE" => "string",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),
			"CONTRACTOR_KPP" => Array(
				"FIELD" => "TC.KPP",
				"TYPE" => "string",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),
			"CONTRACTOR_RSCH" => Array(
				"FIELD" => "TC.RSCH",
				"TYPE" => "string",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),
			"CONTRACTOR_BANK" => Array(
				"FIELD" => "TC.BANK",
				"TYPE" => "string",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),
			"CONTRACTOR_KSCH" => Array(
				"FIELD" => "TC.KSCH",
				"TYPE" => "string",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),
			"CONTRACTOR_BIK" => Array(
				"FIELD" => "TC.BIK",
				"TYPE" => "string",
				"FROM" => "INNER JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"
			),
		);

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = Array(
				"ID",
				"ACCOUNT_PERIOD_ID",
				"CONTRACTOR_ID",
				"DEBT_BEG",
				"SUMM_PAYED",
				"PENALTIES",
				"SUMM_CHARGED",
				"SUMM",
				"RECEIPT_ORDER",
				"BARCODE_TYPE",
				"BARCODE_VALUE",
				"IS_OVERHAUL"
			);
		}
		elseif (in_array('*', $arSelectFields))
		{
			$arSelectFields = array_keys($arFields);
		}

		if (in_array('CONTRACTOR_BILLING', $arSelectFields))
		{
			$arSelectFields = array_merge($arSelectFields, array(
				"CONTRACTOR_INN",
				"CONTRACTOR_KPP",
				"CONTRACTOR_RSCH",
				"CONTRACTOR_BANK",
				"CONTRACTOR_KSCH",
				"CONTRACTOR_BIK",
			));
		}

		if (self::USER_FIELD_ENTITY)
		{
			$obUserFieldsSql = new CUserTypeSQL;
			$obUserFieldsSql->SetEntity(self::USER_FIELD_ENTITY, "T.ID");
			$obUserFieldsSql->SetSelect($arSelectFields);
			$obUserFieldsSql->SetFilter($arFilter);
			$obUserFieldsSql->SetOrder($arOrder);

			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . ' T', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields, $obUserFieldsSql, $strUserFieldsID = "T.ID");
		}
		else
		{
			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . ' T', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
		}

		if (self::USER_FIELD_ENTITY && is_object($dbRes))
		{
			$dbRes->SetUserFields($USER_FIELD_MANAGER->GetUserFields(self::USER_FIELD_ENTITY));
		}

		return new CTszhContractorResult($dbRes);
	}

	/**
	 * ��������� ����� ��� ������ $ID
	 *
	 * @param int $ID ������������� ��������� �����������
	 *
	 * @return CDBResult ����� ���������� ������ ���� CDBResult, ���������� ������, ��������������� ������� �������.
	 */
	public static function GetByID($ID)
	{
		return self::GetList(Array(), Array("ID" => $ID))->GetNext();
	}

}