<?php

IncludeModuleLangFile(__FILE__);

$GLOBALS["TSZH_METER"] = Array();

/**
 * CTszhMeter
 * ����� ��� ������ �� ����������
 *
 * ����:
 * ----
 *
 * ID            - ID ������
 * ACTIVE        - ���� ���������� (��������� � 1� �������� ����� ��������������?)
 * HOUSE_METER  - ��������� �� ����������� ���������
 * XML_ID        - ������� ��� �������� (�� 1�)
 * NAME            - ������ �������� ��������
 * NUM            - ��������� �����
 * ACCOUNT_ID    - ID �������� �����, �������� ����������� �������
 * SERVICE_NAME - ������������ ������
 * SERVICE_ID    - ID ������
 * VALUES_COUNT - ���������� �������
 * SORT            - ������� ����������
 *
 * @package
 * @author
 * @copyright nook.yo
 * @version 2010
 * @access public
 */
class CTszhMeter
{

	// ������� ��, ���������� ������
	const TABLE_NAME = 'b_tszh_meters';
	// ������� ��, ���������� �������� ��������� � ������� ������
	const TABLE_NAME_METERS_ACCOUNTS = 'b_tszh_meters_accounts';
	// �������� ������ � ��������� ��������, ���� ������������ ���������������� ���� �������� ������; false, ���� �� ������������
	const USER_FIELD_ENTITY = 'TSZH_METER';

	public static $arGetListFields = array(
		"ID" => Array("FIELD" => "TM.ID", "TYPE" => "int"),
		"ACTIVE" => Array("FIELD" => "TM.ACTIVE", "TYPE" => "char"),
		"HOUSE_METER" => Array("FIELD" => "TM.HOUSE_METER", "TYPE" => "char"),
		"XML_ID" => Array("FIELD" => "TM.XML_ID", "TYPE" => "string"),
		"NAME" => Array("FIELD" => "TM.NAME", "TYPE" => "string"),
		"NUM" => Array("FIELD" => "TM.NUM", "TYPE" => "string"),
		"SERVICE_ID" => Array("FIELD" => "TM.SERVICE_ID", "TYPE" => "int"),
		"SERVICE_NAME" => Array("FIELD" => "TM.SERVICE_NAME", "TYPE" => "string"),
		"VALUES_COUNT" => Array("FIELD" => "TM.VALUES_COUNT", "TYPE" => "int"),
		"DEC_PLACES" => Array("FIELD" => "TM.DEC_PLACES", "TYPE" => "int"),
		"CAPACITY" => Array("FIELD" => "TM.CAPACITY", "TYPE" => "int"),
		"SORT" => Array("FIELD" => "TM.SORT", "TYPE" => "int"),
		"VERIFICATION_DATE" => Array("FIELD" => "TM.VERIFICATION_DATE", "TYPE" => "date"),
		"ACCOUNT_ID" => Array(
			"FIELD" => "_TMA.ACCOUNT_ID",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TMAS.ACCOUNT_ID ORDER BY TMAS.ACCOUNT_ID SEPARATOR '|') FROM b_tszh_meters_accounts TMAS WHERE TM.ID = TMAS.METER_ID)",
			"TYPE" => "int",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts _TMA ON (_TMA.METER_ID = TM.ID)",
			/*"BINDING_TABLE" => "b_tszh_meters_accounts TMAW", 
			"BINDING_EXTERNAL_FIELD" => "TM.ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TMAW.ACCOUNT_ID"*/
		),
		"TSZH_ID" => Array(
			"FIELD" => "TA.TSZH_ID",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.TSZH_ID ORDER BY TAS.ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TMAS.ACCOUNT_ID = TAS.ID WHERE TM.ID = TMAS.METER_ID)",
			"TYPE" => "int",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TM.ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TMAW.ACCOUNT_ID = TAW.ID", 
			"BINDING_EXTERNAL_FIELD" => "TM.ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.TSZH_ID"*/
		),
		"USER_ID" => Array(
			"FIELD" => "TA.USER_ID",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.USER_ID ORDER BY TAS.ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TMAS.ACCOUNT_ID = TAS.ID WHERE TM.ID = TMAS.METER_ID)",
			"TYPE" => "int",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TM.ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TMAW.ACCOUNT_ID = TAW.ID", 
			"BINDING_EXTERNAL_FIELD" => "TM.ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.USER_ID"*/
		),
		"ACCOUNT_XML_ID" => Array(
			"FIELD" => "TA.XML_ID",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.XML_ID ORDER BY TAS.ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TMAS.ACCOUNT_ID = TAS.ID WHERE TM.ID = TMAS.METER_ID)",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TM.ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TMAW.ACCOUNT_ID = TAW.ID", 
			"BINDING_EXTERNAL_FIELD" => "TM.ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.XML_ID"*/
		),
		"ACCOUNT_EXTERNAL_ID" => Array(
			"FIELD" => "TA.EXTERNAL_ID",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.EXTERNAL_ID ORDER BY TAS.ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TMAS.ACCOUNT_ID = TAS.ID WHERE TM.ID = TMAS.METER_ID)",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TM.ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TMAW.ACCOUNT_ID = TAW.ID", 
			"BINDING_EXTERNAL_FIELD" => "TM.ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.EXTERNAL_ID"*/
		),
		"ACCOUNT_NAME" => Array(
			"FIELD" => "TA.NAME",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.NAME ORDER BY TAS.ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TMAS.ACCOUNT_ID = TAS.ID WHERE TM.ID = TMAS.METER_ID)",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TM.ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TMAW.ACCOUNT_ID = TAW.ID", 
			"BINDING_EXTERNAL_FIELD" => "TM.ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.NAME"*/
		),
		"ACCOUNT_FLAT" => Array(
			"FIELD" => "TA.FLAT",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.FLAT ORDER BY TAS.ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TMAS.ACCOUNT_ID = TAS.ID WHERE TM.ID = TMAS.METER_ID)",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TM.ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TMAW.ACCOUNT_ID = TAW.ID", 
			"BINDING_EXTERNAL_FIELD" => "TM.ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.FLAT"*/
		),
		"ACCOUNT_AREA" => Array(
			"FIELD" => "TA.AREA",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.AREA ORDER BY TAS.ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TMAS.ACCOUNT_ID = TAS.ID WHERE TM.ID = TMAS.METER_ID)",
			"TYPE" => "double",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TM.ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TMAW.ACCOUNT_ID = TAW.ID", 
			"BINDING_EXTERNAL_FIELD" => "TM.ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.AREA"*/
		),
		"ACCOUNT_LIVING_AREA" => Array(
			"FIELD" => "TA.LIVING_AREA",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.LIVING_AREA ORDER BY TAS.ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TMAS.ACCOUNT_ID = TAS.ID WHERE TM.ID = TMAS.METER_ID)",
			"TYPE" => "double",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TM.ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TMAW.ACCOUNT_ID = TAW.ID", 
			"BINDING_EXTERNAL_FIELD" => "TM.ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.LIVING_AREA"*/
		),
		"ACCOUNT_PEOPLE" => Array(
			"FIELD" => "TA.PEOPLE",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.PEOPLE ORDER BY TAS.ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TMAS.ACCOUNT_ID = TAS.ID WHERE TM.ID = TMAS.METER_ID)",
			"TYPE" => "int",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TM.ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TMAW.ACCOUNT_ID = TAW.ID", 
			"BINDING_EXTERNAL_FIELD" => "TM.ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.PEOPLE"*/
		),
	);

	/**
	 * CTszhMeter::GetByID()
	 * ����� ���������� ���� �������� � ����� ID
	 *
	 * @param int $ID ��� ��������
	 *
	 * @return array|bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function GetByID($ID)
	{
		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (isset($GLOBALS["TSZH_METER"]["CACHE_" . $ID]) && is_array($GLOBALS["TSZH_METER"]["CACHE_" . $ID]) && is_set($GLOBALS["TSZH_METER"]["CACHE_" . $ID], "ID"))
		{
			return $GLOBALS["TSZH_METER"]["CACHE_" . $ID];
		}
		else
		{
			$arMeter = self::GetList(array(), array("ID" => $ID), false, array("nTopCount" => 1))->GetNext();
			if (is_array($arMeter) && !empty($arMeter))
			{
				$GLOBALS["TSZH_METER"]["CACHE_" . $arMeter['ID']] = $arMeter;
				$GLOBALS["TSZH_METER"]["XML_ID_CACHE_" . $arMeter['XML_ID']] = $arMeter;

				return $arMeter;
			}
		}

		return false;
	}

	/**
	 * CTszhMeter::GetByXmlID()
	 * ����� ���������� ���� �������� � ������� ����� ID (��� �� 1�)
	 *
	 * @param string $ID ������� ��� ��������
	 *
	 * @return array|bool
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function GetByXmlID($ID)
	{
		$ID = trim($ID);
		if (strlen($ID) <= 0)
		{
			throw new \Bitrix\Main\ArgumentException("Empty XML_ID given", "ID");
		}

		if (isset($GLOBALS["TSZH_METER"]["XML_ID_CACHE_" . $ID]) && is_array($GLOBALS["TSZH_METER"]["XML_ID_CACHE_" . $ID]) && is_set($GLOBALS["TSZH_METER"]["XML_ID_CACHE_" . $ID], "ID"))
		{
			return $GLOBALS["TSZH_METER"]["XML_ID_CACHE_" . $ID];
		}
		else
		{
			$arMeter = self::GetList(array(), array("XML_ID" => $ID), false, array("nTopCount" => 1))->GetNext();
			if (is_array($arMeter) && !empty($arMeter))
			{
				$GLOBALS["TSZH_METER"]["CACHE_" . $arMeter['ID']] = $arMeter;
				$GLOBALS["TSZH_METER"]["XML_ID_CACHE_" . $arMeter['XML_ID']] = $arMeter;

				return $arMeter;
			}
		}

		return false;
	}

	/**
	 * CTszhMeter::CheckFields()
	 * �������� ����� (Add � Update)
	 *
	 * @param array $arFields ���� ��������
	 * @param string $strOperation �������� (ADD ��� UPDATE)
	 * @param int $ID ��� ��������
	 *
	 * @return bool
	 */
	protected static function CheckFields(&$arFields, $strOperation = 'ADD', $ID = 0)
	{
		global $APPLICATION, $USER_FIELD_MANAGER;

		if (array_key_exists('SERVICE_ID', $arFields) || $strOperation == "ADD")
		{
			$arFields['SERVICE_ID'] = IntVal($arFields['SERVICE_ID']);
		}

		if (isset($arFields['HOUSE_METER']))
		{
			$arFields['HOUSE_METER'] = $arFields['HOUSE_METER'] == 'Y' ? 'Y' : 'N';
		}
		elseif ($strOperation == 'ADD')
		{
			$arFields["HOUSE_METER"] = "N";
		}

		/*if ($arFields["HOUSE_METER"] == "Y")
		{
			$arFields['ACCOUNT_ID'] = false;
		}*/
		/*else
		{
			if (array_key_exists('ACCOUNT_ID', $arFields) || $strOperation == "ADD")
			{
				$arFields['ACCOUNT_ID'] = IntVal($arFields['ACCOUNT_ID']);
				if ($arFields['ACCOUNT_ID'] <= 0)
				{
					$APPLICATION->ThrowException(GetMessage("ERROR_TSZH_METER_WRONG_ACCOUNT"));
					return false;
				}
			}
		}*/

		if (isset($arFields['VALUES_COUNT']) || $strOperation == 'ADD')
		{
			$arFields['VALUES_COUNT'] = IntVal($arFields['VALUES_COUNT']);
			if ($arFields['VALUES_COUNT'] <= 0 || $arFields["VALUES_COUNT"] > 3)
			{
				$arFields['VALUES_COUNT'] = 1;
			}
		}

		if (isset($arFields['DEC_PLACES']) || $strOperation == 'ADD')
		{
			$arFields['DEC_PLACES'] = IntVal($arFields['DEC_PLACES']);
			if ($arFields['DEC_PLACES'] <= 0)
			{
				$arFields['DEC_PLACES'] = 2;
			}
			if ($arFields['DEC_PLACES'] > 8)
			{
				$APPLICATION->ThrowException(GetMessage("ERROR_TSZH_METER_LARGE_DEC_PLACES"));

				return false;
			}
		}

		if (isset($arFields['CAPACITY']) || $strOperation == 'ADD')
		{
			$arFields['CAPACITY'] = IntVal($arFields['CAPACITY']);
			if ($arFields['CAPACITY'] <= 0)
			{
				$arFields['CAPACITY'] = false;
			}
			if ($arFields['CAPACITY'] > 20)
			{
				$APPLICATION->ThrowException(GetMessage("ERROR_TSZH_METER_LARGE_CAPACITY"));

				return false;
			}
		}

		if (isset($arFields["VERIFICATION_DATE"]) && strlen($arFields["VERIFICATION_DATE"]))
		{
			$ts = MakeTimeStamp($arFields["VERIFICATION_DATE"]);
			if ($ts <= 0)
			{
				$APPLICATION->ThrowException(GetMessage("ERROR_TSZH_METER_INVALID_VERIFICATION_DATE"));

				return false;
			}
		}

		if (isset($arFields['ACTIVE']))
		{
			$arFields['ACTIVE'] = $arFields['ACTIVE'] == 'Y' ? 'Y' : 'N';
		}
		elseif ($strOperation == 'ADD')
		{
			$arFields["ACTIVE"] = "Y";
		}

		if (self::USER_FIELD_ENTITY && !$USER_FIELD_MANAGER->CheckFields(self::USER_FIELD_ENTITY, $ID, $arFields))
		{
			return false;
		}

		unset($arFields['ID']);

		return true;
	}

	/**
	 * CTszhMeter::Add()
	 * ����� ��������� ����� ������� � ������ $arFields
	 *
	 * @param array $arFields ���� ��������
	 *
	 * @return bool
	 */
	public static function Add($arFields)
	{
		global $USER_FIELD_MANAGER;

		if (!CTszhMeter::CheckFields($arFields, 'ADD'))
		{
			return false;
		}

		$ID = CDatabase::Add(self::TABLE_NAME, $arFields);
		if (self::USER_FIELD_ENTITY && $ID > 0 && CTszh::hasUserFields($arFields))
		{
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		if ($ID > 0 && isset($arFields["ACCOUNT_ID"]) && !empty($arFields["ACCOUNT_ID"]))
		{
			self::bindAccount($ID, $arFields["ACCOUNT_ID"]);
		}

		return $ID;
	}

	/**
	 * CTszhMeter::Update()
	 * ����� �������� ������� � ����� $ID
	 *
	 * @param int $ID ��� ��������
	 * @param array $arFields ���� ��������
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Update($ID, $arFields)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (!CTszhMeter::CheckFields($arFields, 'UPDATE', $ID))
		{
			return false;
		}

		$bSuccess = true;
		$strUpdate = $DB->PrepareUpdate(self::TABLE_NAME, $arFields);
		if (strlen($strUpdate))
		{
			$strSql =
				"UPDATE " . self::TABLE_NAME . " SET " .
				$strUpdate .
				" WHERE ID=" . $ID;
			$bSuccess = (bool)$DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		}
		if (self::USER_FIELD_ENTITY && $bSuccess && CTszh::hasUserFields($arFields))
		{
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		if ($bSuccess && is_set($arFields, "ACCOUNT_ID"))
		{
			self::unbindAccount($ID);
		}

		if ($bSuccess && isset($arFields["ACCOUNT_ID"]) && !empty($arFields["ACCOUNT_ID"]))
		{
			self::bindAccount($ID, $arFields["ACCOUNT_ID"]);
		}

		return $bSuccess;
	}

	/**
	 * CTszhMeter::Delete()
	 * ����� ������� �������
	 * ���� ����� $accountID, � ���������, ��� ������� �������� �� ������ � ����� �/�, �� ������� ����� �� �����, � ������� �� ����� �/�
	 *
	 * @param int $ID ��� ��������
	 * @param int|bool $accountID ��� �������� �����, � �������� �������� �������
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Delete($ID, $accountID = false)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = intval($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		$isNeedDelete = true;

		$accountID = intval($accountID);
		if ($accountID > 0)
		{
			$arMeter = self::GetList(Array(), Array("ID" => $ID), false, Array("nTopCount" => 1), Array("ID", "ACCOUNT_ID"))->getNext();
			if (is_array($arMeter)  && (count($arMeter["ACCOUNT_ID"]) > 1))
			{
				foreach ($arMeter["ACCOUNT_ID"] as $meterAccountID)
				{
					if ($meterAccountID != $accountID)
					{
						$isNeedDelete = false;
						break;
					}
				}
			}
		}

		if ($isNeedDelete)
		{
			$rsMeterValue = CTszhMeterValue::GetList(Array(), Array("METER_ID" => $ID), false, false, Array("ID"));
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($arMeterValue = $rsMeterValue->Fetch())
			{
				CTszhMeterValue::Delete($arMeterValue['ID']);
			}

			self::unbindAccount($ID);

			$strSql = "DELETE FROM " . self::TABLE_NAME . " WHERE ID=" . $ID;
			$res = (bool)$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

			if (self::USER_FIELD_ENTITY && $res)
			{
				$USER_FIELD_MANAGER->Delete(self::USER_FIELD_ENTITY, $ID);
			}
		}
		else
		{
			$res = (bool)self::unbindAccount($ID, $accountID);
		}

		return $res;
	}

	/**
	 * ��������� ������ ��������� �� �������
	 *
	 * @param array $arOrder
	 * @param array $arFilter
	 * @param array|bool $arGroupBy
	 * @param array|bool $arNavStartParams
	 * @param array $arSelectFields
	 *
	 * @return CTszhMultiValuesResult
	 */
	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		global $USER_FIELD_MANAGER;

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = Array(
				"ID",
				"ACTIVE",
				"HOUSE_METER",
				"XML_ID",
				"NAME",
				"NUM",
				"ACCOUNT_ID",
				"SERVICE_ID",
				"SERVICE_NAME",
				"VALUES_COUNT",
				"DEC_PLACES",
				"CAPACITY",
				"VERIFICATION_DATE",
				"SORT"
			);
		}
		elseif (in_array('*', $arSelectFields))
		{
			$arSelectFields = array_keys(self::$arGetListFields);
		}

		if (self::USER_FIELD_ENTITY)
		{
			$obUserFieldsSql = new CUserTypeSQL;
			$obUserFieldsSql->SetEntity(self::USER_FIELD_ENTITY, "TM.ID");
			$obUserFieldsSql->SetSelect($arSelectFields);
			$obUserFieldsSql->SetFilter($arFilter);
			$obUserFieldsSql->SetOrder($arOrder);

			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . " TM", self::$arGetListFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields, $obUserFieldsSql, $strUserFieldsID = "TM.ID");
		}
		else
		{
			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . " TM", self::$arGetListFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
		}

		if (self::USER_FIELD_ENTITY && is_object($dbRes))
		{
			$dbRes->SetUserFields($USER_FIELD_MANAGER->GetUserFields(self::USER_FIELD_ENTITY));
		}

		return new CTszhMultiValuesResult($dbRes, self::$arGetListFields);
	}

	/**
	 * ��������� �� � ������ ������ ������������� ������� ��������� ���������
	 *
	 * @return true, ���� � ������ ������ ��������� ������� ��������� ���������, ����� false
	 */
	public static function CanPostMeterValues()
	{
		return COption::GetOptionString(TSZH_MODULE_ID, "meters_block_edit") != "Y";
	}

	/**
	 * ������������� ��������� � ���� ���������� �������� ��������� ���������
	 *
	 * @param bool $bValue
	 */
	public static function SetCanPostMeterValues($bValue)
	{
		$strBlock = $bValue ? 'N' : 'Y';
		COption::SetOptionString(TSZH_MODULE_ID, 'meters_block_edit', $strBlock);
	}

	/**
	 * CTszhMeter::bindAccount()
	 * ����������� ������� � ������� ������
	 *
	 * @param int $meterID ID ��������
	 * @param int|array $accountID ID �������� ����� | ������ ID ������� ������
	 *
	 * @return true � ������ ������, ����� false
	 */
	public static function bindAccount($meterID, $accountID)
	{
		global $DB;

		$meterID = intval($meterID);
		if ($meterID <= 0)
		{
			return false;
		}

		if (!is_array($accountID))
		{
			$accountID = array(intval($accountID));
		}

		if (empty($accountID))
		{
			return false;
		}

		foreach ($accountID as $key => $value)
		{
			$accountID[$key] = intval($value);
			if ($accountID[$key] <= 0)
			{
				return false;
			}
		}

		$res = true;

		$DB->StartTransaction();
		foreach ($accountID as $key => $value)
		{
			$strSql = "SELECT METER_ID, ACCOUNT_ID FROM " . self::TABLE_NAME_METERS_ACCOUNTS . " WHERE METER_ID={$meterID} AND ACCOUNT_ID={$value}";
			$dbBindig = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			if (!$dbBindig->Fetch())
			{
				$strSql = "INSERT INTO " . self::TABLE_NAME_METERS_ACCOUNTS . " (METER_ID, ACCOUNT_ID) VALUES ({$meterID}, {$value})";
				$res = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);
				if (!$res)
				{
					break;
				}
			}
		}
		if ($res)
		{
			$DB->Commit();
		}
		else
		{
			$DB->Rollback();
		}

		return $res;
	}

	/**
	 * ���������� ������� �� ������� ������
	 *
	 * @param int $meterID ID ��������
	 * @param array|bool|int $accountID ID �������� ����� | ������ ID ������� ������ (���� �� �����, �� ���������� ������� �� ���� ������� ������)
	 *
	 * @return bool true � ������ ������, ����� false
	 */
	public static function unbindAccount($meterID, $accountID = false)
	{
		global $DB;

		$meterID = intval($meterID);
		if ($meterID <= 0)
		{
			return false;
		}

		$delAllBindingsFlag = $accountID === false || is_array($accountID) && empty($accountID);
		if (!$delAllBindingsFlag)
		{
			if (!is_array($accountID))
			{
				$accountID = array(intval($accountID));
			}

			foreach ($accountID as $key => $value)
			{
				$accountID[$key] = intval($value);
				if ($accountID[$key] <= 0)
				{
					return false;
				}
			}
		}
		$res = true;

		$DB->StartTransaction();
		if ($delAllBindingsFlag)
		{
			$strSql = "DELETE FROM " . self::TABLE_NAME_METERS_ACCOUNTS . " WHERE METER_ID={$meterID}";
			$res = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);
		}
		else
		{
			foreach ($accountID as $key => $value)
			{
				$strSql = "DELETE FROM " . self::TABLE_NAME_METERS_ACCOUNTS . " WHERE METER_ID={$meterID} AND ACCOUNT_ID={$value}";
				$res = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);
				if (!$res)
				{
					break;
				}
			}
		}
		if ($res)
		{
			$DB->Commit();
		}
		else
		{
			$DB->Rollback();
		}

		return $res;
	}
}