<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

use Bitrix\Main\Localization\Loc;

$APPLICATION->SetAdditionalCSS('/bitrix/gadgets/otr/tszhaccess/styles.css');
?>
<div class="otr-gadgets-tszhaccess">
	<div class="otr-gadget-tszhaccess__header"><?= Loc::getMessage("OTR_GADGETS_TSZHACCESS_HEADER") ?></div>
	<div class="otr-gadget-tszhaccess__auto"></div>
	<a class="tszhaccess-button" href="https://vgkh.ru/jsk/jkh-sistema-propuskov/" target="_blank">
		<div class="tszhaccess-button__text"><?= Loc::getMessage("OTR_GADGETS_TSZHACCESS_BUTTON_TEXT") ?></div>
	</a>
</div>

