<?
$MESS["GADGET_TSZH_FLYSHEET_TEXT"] = "Листовки про ваш сайт";
$MESS["OTR_GADGETS_DEMODATA_HEADER"] = "Сервис демо-данных";
$MESS["OTR_GADGETS_DEMODATA_TEXT"] = "Изучите сайт ЖКХ быстрее с демо-данными. После завершения вы всегда сможете безболезненно для сайта и остальных данных удалить их.";
$MESS["OTR_GADGETS_DEMODATA_BUTTON_TEXT"] = "Загрузить данные";