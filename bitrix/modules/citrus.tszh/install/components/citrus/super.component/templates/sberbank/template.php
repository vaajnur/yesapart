<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION, $USER;

$bConfigMode = $USER->IsAuthorized() && $APPLICATION->GetPublicShowMode() != 'view';

if (!$bConfigMode)
	$APPLICATION->RestartBuffer();

$arResult['PAYMENT_FOR'] = "���������� ������� ������������ �" . $USER->GetID();


if (!$bConfigMode):
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><head>

<title>��������� ���������</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<style type="text/css">
body {
	color: #000; background-color: #fff;
	font: 10pt/1.3em "Times New Roman", Times, sans-serif;
}
h3 {font-size: 12pt;}
p, ul, ol, h3 {margin-top:6px; margin-bottom:6px}
td {font-size: 9pt;}
small {font-size: 7pt;}
</style>
</head><body>
<?endif;?>
<table style="width: 180mm; height: 145mm;" border="0" cellpadding="0"
cellspacing="0">
<tbody><tr valign="top">
	<td style="width: 50mm; height: 70mm; border-width: 1pt medium medium
1pt; border-style: solid none none solid; border-color: rgb(0, 0, 0)
-moz-use-text-color -moz-use-text-color rgb(0, 0, 0);" align="center">
	<b>���������</b><br>
	<font style="font-size: 53mm;">&nbsp;<br></font>
	<b>������</b>
	</td>
	<td style="border-width: 1pt 1pt medium; border-style: solid solid
none; border-color: rgb(0, 0, 0) rgb(0, 0, 0) -moz-use-text-color;"
align="center">
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td align="right"><small><i>����� � ��-4</i></small></td>
			</tr>
			<tr>
				<td style="border-bottom: 1pt solid rgb(0, 0, 0);"><?=$arParams['PAYEE']?></td>
			</tr>
			<tr>
				<td align="center"><small>(������������ ���������� �������)</small></td>
			</tr>
		</tbody></table>

		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td style="width: 37mm; border-bottom: 1pt solid rgb(0, 0, 0);"><?=$arParams['INN']?>/<?=$arParams['KPP']?></td>
				<td style="width: 9mm;">&nbsp;</td>
				<td style="border-bottom: 1pt solid rgb(0, 0, 0);"><?=$arParams['ACCOUNT']?></td>
			</tr>
			<tr>
				<td align="center"><small>(��� ���������� �������)</small></td>
				<td><small>&nbsp;</small></td>
				<td align="center"><small>(����� ����� ���������� �������)</small></td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td>�&nbsp;</td>
				<td style="width: 73mm; border-bottom: 1pt solid rgb(0, 0, 0);"><?=$arParams['BANK']?></td>
				<td align="right">���&nbsp;&nbsp;</td>
				<td style="width: 33mm; border-bottom: 1pt solid rgb(0, 0, 0);"><?=$arParams['BIK']?></td>
			</tr>
			<tr>
				<td></td>
				<td align="center"><small>(������������ ����� ���������� �������)</small></td>
				<td></td>
				<td></td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td width="1%" nowrap="nowrap">����� ���./��. ����� ����������
�������&nbsp;&nbsp;</td>
				<td style="border-bottom: 1pt solid rgb(0, 0, 0);" width="100%"><?=$arParams['BANK_ACCOUNT']?></td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td style="width: 60mm; border-bottom: 1pt solid rgb(0, 0, 0);"><?=$arResult['PAYMENT_FOR']?></td>
				<td style="width: 2mm;">&nbsp;</td>
				<td style="border-bottom: 1pt solid rgb(0, 0, 0);">&nbsp;</td>
			</tr>
			<tr>
				<td align="center"><small>(������������ �������)</small></td>
				<td><small>&nbsp;</small></td>
				<td align="center"><small>(����� �������� ����� (���) �����������)</small></td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td width="1%" nowrap="nowrap">�.�.�. �����������&nbsp;&nbsp;</td>
				<td style="border-bottom: 1pt solid rgb(0, 0, 0);" width="100%">&nbsp;</td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td width="1%" nowrap="nowrap">����� �����������&nbsp;&nbsp;</td>
				<td style="border-bottom: 1pt solid rgb(0, 0, 0);" width="100%">&nbsp;</td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td>����� �������&nbsp;&nbsp;_______&nbsp;���.&nbsp;___&nbsp;���.</td>
				<td align="right">&nbsp;&nbsp;����� ����� ��
������&nbsp;&nbsp;______&nbsp;���.&nbsp;___&nbsp;���.</td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td>�����&nbsp;&nbsp;_______&nbsp;���.&nbsp;___&nbsp;���.</td>
				<td align="right">&nbsp;&nbsp;�______�________________ 20____ �.</td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td><small>� ��������� ������ ��������� � ��������� ��������� �����,

				� �.�. � ������ ��������� ����� �� ������ �����, ���������� �
��������.</small></td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td align="right"><b>������� ����������� _____________________</b></td>
			</tr>
		</tbody></table>
	</td>
</tr>



<tr valign="top">
	<td style="width: 50mm; height: 70mm; border-width: 1pt medium 1pt 1pt;
 border-style: solid none solid solid; border-color: rgb(0, 0, 0)
-moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0);" align="center">
	<b>���������</b><br>
	<font style="font-size: 53mm;">&nbsp;<br></font>
	<b>������</b>
	</td>
	<td style="border: 1pt solid rgb(0, 0, 0);" align="center">
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td align="right"><small><i>����� � ��-4</i></small></td>
			</tr>
			<tr>
				<td style="border-bottom: 1pt solid rgb(0, 0, 0);"><?=$arParams['PAYEE']?></td>
			</tr>
			<tr>
				<td align="center"><small>(������������ ���������� �������)</small></td>
			</tr>
		</tbody></table>

		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td style="width: 37mm; border-bottom: 1pt solid rgb(0, 0, 0);"><?=$arParams['INN']?>/<?=$arParams['KPP']?></td>
				<td style="width: 9mm;">&nbsp;</td>
				<td style="border-bottom: 1pt solid rgb(0, 0, 0);"><?=$arParams['ACCOUNT']?></td>
			</tr>
			<tr>
				<td align="center"><small>(��� ���������� �������)</small></td>
				<td><small>&nbsp;</small></td>
				<td align="center"><small>(����� ����� ���������� �������)</small></td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td>�&nbsp;</td>
				<td style="width: 73mm; border-bottom: 1pt solid rgb(0, 0, 0);"><?=$arParams['BANK']?></td>
				<td align="right">���&nbsp;&nbsp;</td>
				<td style="width: 33mm; border-bottom: 1pt solid rgb(0, 0, 0);"><?=$arParams['BIK']?></td>
			</tr>
			<tr>
				<td></td>
				<td align="center"><small>(������������ ����� ���������� �������)</small></td>
				<td></td>
				<td></td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td width="1%" nowrap="nowrap">����� ���./��. ����� ����������
�������&nbsp;&nbsp;</td>
				<td style="border-bottom: 1pt solid rgb(0, 0, 0);" width="100%"><?=$arParams['BANK_ACCOUNT']?></td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td style="width: 60mm; border-bottom: 1pt solid rgb(0, 0, 0);"><?=$arResult['PAYMENT_FOR']?></td>
				<td style="width: 2mm;">&nbsp;</td>
				<td style="border-bottom: 1pt solid rgb(0, 0, 0);">&nbsp;</td>
			</tr>
			<tr>
				<td align="center"><small>(������������ �������)</small></td>
				<td><small>&nbsp;</small></td>
				<td align="center"><small>(����� �������� ����� (���) �����������)</small></td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td width="1%" nowrap="nowrap">�.�.�. �����������&nbsp;&nbsp;</td>
				<td style="border-bottom: 1pt solid rgb(0, 0, 0);" width="100%">&nbsp;</td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td width="1%" nowrap="nowrap">����� �����������&nbsp;&nbsp;</td>
				<td style="border-bottom: 1pt solid rgb(0, 0, 0);" width="100%">&nbsp;</td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td>����� �������&nbsp;&nbsp;_______&nbsp;���.&nbsp;___&nbsp;���.</td>
				<td align="right">&nbsp;&nbsp;����� ����� ��
������&nbsp;&nbsp;_____&nbsp;���.&nbsp;___&nbsp;���.</td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td>�����&nbsp;&nbsp;_______&nbsp;���.&nbsp;___&nbsp;���.</td>
				<td align="right">&nbsp;&nbsp;�______�________________ 20____ �.</td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td><small>� ��������� ������ ��������� � ��������� ��������� �����,

				� �.�. � ������ ��������� ����� �� ������ �����, ���������� �
��������.</small></td>
			</tr>
		</tbody></table>
		<table style="width: 122mm; margin-top: 3pt;" border="0"
cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td align="right"><b>������� ����������� _____________________</b></td>
			</tr>
		</tbody></table>
	</td>
</tr>
</tbody></table>
<br>
<?/*<h3>��������! � ��������� ������ �� �������� �������� �����.</h3>*/?>

<h3><b>����� ������:</b></h3>
<ol>
  <li><a href="javascript:window.print();">������������ ���������</a>. ���� � ��� ��� ��������, ���������� ������� ����� ��������� � ��������� �� ����� ������� ����������� ����� ��������� � ����� �����.</li>
  <li>��������� ���� ��.�.�. �����������, ������ ����������� � ������ �������</li>
  <li>�������� ��������� �� �������.</li>
  <li>�������� ��������� � ����� ��������� �����, ������������ ������� �� ������� ���.</li>
  <li>��������� ��������� �� ������������� ���������� ������.</li>
</ol>

<h3><b>������� ���������� ������� �� ��� ������ �� ����� <a href="http://ikomissionka.ru">ikomissionka.ru</a>:</b> </h3>
<ul>
	<li>���������� ������������ ����� ������������� ����� �������.</li>
</ul>


<p><b>����������:</b>
ikomissionka.ru  �� ����� ������������� ���������� ����� ����������
������ �������. �� �������������� ����������� � ������ ��������
��������� � ���� ����������, ����������� � ���� ����.</p>

<?if (!$bConfigMode):?>
</body></html><?

// ����������� ��������� ����� �������
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");

die();

?>
<?endif;?>