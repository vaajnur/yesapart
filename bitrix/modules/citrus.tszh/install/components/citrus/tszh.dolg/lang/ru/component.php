<?
$MESS["TSZH_MODULE_NOT_FOUND_ERROR"] = "Модуль 1С: Сайт ЖКХ не установлен.";
$MESS["COMP_ERROR_TSZH_NOT_FOUND"] = "Не найдено ТСЖ для текущего сайта.";
$MESS["FL_TIMESTAMP_X"] = "Дата обновления";
$MESS["FL_PERIOD_DATE"] = "Период";
$MESS["FL_USER_LAST_NAME"] = "Фамилия";
$MESS["FL_USER_NAME"] = "Имя";
$MESS["FL_USER_SECOND_NAME"] = "Отчество";
$MESS["FL_DEBT_BEG"] = "Задолженность на начало периода";
$MESS["FL_DEBT_END"] = "Задолженность на конец периода";
$MESS["FL_DEBT_END_WITHOUT_CHARGES"] = "Задолженность на конец периода за вычетом начислений за период";
$MESS["FL_DISTRICT"] = "Район";
$MESS["FL_CITY"] = "Город";
$MESS["FL_SETTLEMENT"] = "Населенный пункт";
$MESS["FL_STREET"] = "Улица";
$MESS["FL_HOUSE"] = "Дом";
$MESS["FL_FLAT"] = "Квартира";
$MESS["FL_USER_FULL_NAME"] = "ФИО";
$MESS["FL_USER_FULL_ADDRESS"] = "Адрес";
$MESS["FL_XML_ID"] = "Номер л/с";
$MESS["COMP_COLS_COUNT"] = "Количество столбцов таблицы";
$MESS["COMP_MAX_COUNT"] = "Максимальное количество выводимых строк";
$MESS["COMP_GROUP_SUMMARY_F_COUNT"] = "количество";
$MESS["COMP_GROUP_SUMMARY_F_SUM"] = "сумма";
$MESS["COMP_GROUP_SUMMARY_F_AVG"] = "среднее";
$MESS["COMP_GROUP_SUMMARY_F_MAX"] = "максимальное";
$MESS["COMP_GROUP_SUMMARY_F_MIN"] = "минимальное";
?>