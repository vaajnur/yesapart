<?
/**
 * ������ ���ƻ
 * ��������� ����������� ������ (citrus.tszh.sheet)
 * ������ ����������� ����� �������� ������������ �� ��������
 * @package tszh
 */

use Bitrix\Main\Application;
use Citrus\Tszh\Types\ReceiptType;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!CModule::IncludeModule('citrus.tszh'))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));

	return;
}

if (!CTszhFunctionalityController::CheckEdition())
{
	return;
}

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

$arParams["MAX_COUNT"] = IntVal($arParams["MAX_COUNT"]) > 0 ? IntVal($arParams["MAX_COUNT"]) : 10;
$arParams["SHOW_SERVICE_CORRECTIONS"] = $arParams["SHOW_SERVICE_CORRECTIONS"] == "Y" ? true : false;
$arParams["SHOW_SERVICE_COMPENSATION"] = $arParams["SHOW_SERVICE_COMPENSATION"] == "Y" ? true : false;
$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"] == "Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"] != "N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"] != "N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"] == "Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"] !== "N";
$arParams['RECEIPT_URL'] = trim($arParams['RECEIPT_URL']);
if (strlen($arParams["RECEIPT_URL"]) <= 0)
{
	$arParams['RECEIPT_URL'] = "#SITE_DIR#personal/receipt/?period=#ID#&print=Y";
}
$arParams['RECEIPT_URL'] = str_replace("#SITE_DIR#", SITE_DIR, $arParams['RECEIPT_URL']);

if ($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"])
{
	$arNavParams = array(
		"nPageSize" => $arParams["MAX_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if ($arNavigation["PAGEN"] == 0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] > 0)
	{
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
	}
}
else
{
	$arNavParams = array(
		"nTopCount" => $arParams["MAX_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

global $USER, $APPLICATION;
$app = Application::getInstance();
$request = $app->getContext()->getRequest();

// ���� ������������ �� �����������, ������� ����� �����������
if (!$USER->IsAuthorized())
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

if (!CTszh::IsTenant() && !$USER->IsAdmin())
{
	//$this->AbortResultCache();
	$APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"));

	return;
}

$arAccount = CTszhAccount::GetByUserID($USER->GetID());

// ��������� ������ ���������� - ���������� � ������ ����������
$arResult = Array();

// ������ �� ���� ���������
$sheet_type = $request->get("type");
if ($sheet_type != 'overhaul')
{
	$arResult["SHEET_TYPE"] = ReceiptType::MAIN;
	$fType = array(ReceiptType::MAIN, ReceiptType::FINES_MAIN);
}
else
{
	$arResult["SHEET_TYPE"] = ReceiptType::OVERHAUL;
	$fType = array(ReceiptType::OVERHAUL, ReceiptType::FINES_OVERHAUL);
}

// ������ ���������
$rsAccPeriod = CTszhAccountPeriod::GetList(array(), array("ACCOUNT_ID" => $arAccount["ID"], "TYPE" => $fType));
while ($arAccPeriod = $rsAccPeriod->GetNext())
{
	$arPeriodsIDs[] = $arAccPeriod["PERIOD_ID"];
}

// ������ ��� �������� ����� �����������
$arResult["MAIN_URL"] = $APPLICATION->GetCurPage();
$arResult["OVERHAUL_URL"] = $APPLICATION->GetCurPageParam("type=overhaul");

$rsPeriods = CTszhPeriod::GetList(
// sort
	Array('DATE' => 'DESC', 'ID' => 'DESC'),
	// filter
	Array("TSZH_ID" => $arAccount["TSZH_ID"], "ACTIVE" => "Y", "@ID" => $arPeriodsIDs),
	false,
	$arNavParams
);

$months = Array();
$arResult['PERIODS'] = array();
/** @noinspection PhpAssignmentInConditionInspection */
while ($arPeriod = $rsPeriods->GetNext())
{
	$arPeriod['DISPLAY_NAME'] = CTszh::ToUpperFirstChar(CTszhPeriod::Format($arPeriod['DATE']));
	if (array_key_exists($arPeriod['MONTH'], $months))
	{
		// ��� ��� ������ � ����� ������� - ������� ������� � ��������
		$arPeriod['DISPLAY_NAME'] .= " ({$months[$arPeriod['MONTH']]})";
	}
	$arPeriod['DETAIL_PAGE_URL'] = 'javascript:void();';
	$months[$arPeriod['MONTH']] += 1;

	$rsAccountPeriod = CTszhAccountPeriod::GetList(
		Array(),
		Array(
			'ACCOUNT_ID' => $arAccount["ID"],
			'PERIOD_ID' => $arPeriod['ID'],
			'@TYPE' => $fType,
		)
	);

	$arPeriod['TOTAL_CHARGES'] = $arPeriod['TOTAL_PAYED'] = $arPeriod['CORRECTION'] = $arPeriod["TOTAL_COMPENSATION"] = 0;
	$arPeriod['CHARGES'] = Array();

	while ($arPeriodTemp = $rsAccountPeriod->GetNext())
	{
		$arPeriod['ACCOUNT_PERIOD'] = $arPeriodTemp;
		$dbCharge = CTszhCharge::GetList(
			Array("SORT" => "ASC", "ID" => "ASC"),
			Array(
				'ACCOUNT_PERIOD_ID' => $arPeriod['ACCOUNT_PERIOD']["ID"],
			)
		);
		$arPeriod['TOTAL_DEBT_BEG'] += $arPeriodTemp['DEBT_BEG'];
		$arPeriod['TOTAL_DEBT_END'] += $arPeriodTemp['DEBT_END'];
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arCharge = $dbCharge->GetNext())
		{
			if ($arPeriodTemp['TYPE'] == ReceiptType::FINES_MAIN || $arPeriodTemp['TYPE'] == ReceiptType::FINES_OVERHAUL)
			{
				$arCharge['SERVICE_NAME'] .= ' ' . GetMessage('PERIOD_FINES');
			}
			$arPeriod['CHARGES'][] = $arCharge;
			if ($arCharge["COMPONENT"] == "N")
			{
				if ($arCharge["IS_INSURANCE"] != "Y")
				{
					$arPeriod['TOTAL_CHARGES'] += $arCharge['SUMM'];
				}
				$arPeriod['TOTAL_PAYED'] += $arCharge['SUMM_PAYED'];
				$arPeriod['CORRECTION'] += $arCharge['CORRECTION'];
				$arPeriod['TOTAL_COMPENSATION'] += $arCharge['COMPENSATION'];
			}
		}

		if ($arPeriodTemp['SUM_PAYED'] > 0)
		{
			$arPeriod['TOTAL_PAYED'] = (float)$arPeriodTemp['SUM_PAYED'];
		}
	}
	if (!$arResult['VIEW_CORRECTION'] && $arPeriod['CORRECTION'] <> 0)
	{
		$arResult['VIEW_CORRECTION'] = true;
	}
	if (!$arResult['VIEW_COMPENSATION'] && $arPeriod['TOTAL_COMPENSATION'] <> 0)
	{
		$arResult['VIEW_COMPENSATION'] = true;
	}

	$arResult['PERIODS'][] = $arPeriod;

	unset($arCharge);
	unset($arPeriodTemp);
}

$arResult['ACCOUNT'] = $arAccount;

$arResult["NAV_STRING"] = $rsPeriods->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);

// ����������� ������� ����������
$this->IncludeComponentTemplate();
