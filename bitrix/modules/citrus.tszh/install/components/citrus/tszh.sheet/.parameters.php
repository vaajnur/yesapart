<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS"  =>  array(
		"MAX_COUNT" => array(
			"NAME" => GetMessage("COMP_MAX_COUNT"),
			"TYPE" => "STRING",
			"DEFAULT" => '10',
		),
		"RECEIPT_URL" => Array(
			"NAME" => GetMessage("RECEIPT_URL"),
			"TYPE" => "STRING",
			"DEFAULT" => "#SITE_DIR#personal/receipt/?period=#ID#",
		),
        "SHOW_SERVICE_CORRECTIONS" => Array(
            "NAME" => GetMessage("CITRUS_TSZH_SHEET_SHOW_SERVICE_CORRECTIONS"),
            "TYPE" => "CHECKBOX",
            "VALUE" => "Y",
            "DEFAULT" => "N",
        ),
		"SHOW_SERVICE_COMPENSATION" => Array(
			"NAME" => GetMessage("CITRUS_TSZH_SHEET_SHOW_SERVICE_COMPENSATION"),
			"TYPE" => "CHECKBOX",
			"VALUE" => "Y",
			"DEFAULT" => "N",
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>300),
	),
);

if (CModule::IncludeModule("iblock"))
	CIBlockParameters::AddPagerSettings($arComponentParameters, GetMessage("COMP_NAV_PAGER"), true, true);


?>