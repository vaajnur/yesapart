<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

CJSCore::Init(array('tszh_datepicker', 'tszh_tabs'));

global $arrResult;
$arrResult = $arResult;

?>

<div class="tabs">
	<input id="tab1" type="radio" name="tabs" <?=$arResult["SHEET_TYPE"] == Citrus\Tszh\Types\ReceiptType::OVERHAUL ? '' : 'checked'?>><label
			for="tab1" title="<? $title = GetMessage("TSZH_SHOW_TAB1_RECEIPT");
	echo $title; ?>"><a href="<?=($arResult["SHEET_TYPE"] == Citrus\Tszh\Types\ReceiptType::OVERHAUL ? $arResult["MAIN_URL"] : "")?>">
			<div><?=$title?></div>
		</a></label>
	<? if($arResult['USER_TSZH']["OVERHAUL_OFF"]=='Y'): ?>
	<input id="tab2" type="radio" name="tabs" <?=$arResult["SHEET_TYPE"] == Citrus\Tszh\Types\ReceiptType::MAIN ? '' : 'checked'?> ><label for="tab2"
	                                                                                                                                       title="<? $title = GetMessage("TSZH_SHOW_TAB2_RECEIPT");
	                                                                                                                                       echo $title; ?>"><a
				href="<?=($arResult["SHEET_TYPE"] == Citrus\Tszh\Types\ReceiptType::MAIN ? $arResult["OVERHAUL_URL"] : "")?>">
			<div><?=$title?></div>
		</a></label>
	<?endif;?>
	<?
	if ($arResult["SHEET_TYPE"] != Citrus\Tszh\Types\ReceiptType::MAIN)
	{
		?>
		<section></section>
		<?
	}

	?>

	<section>
		<? $APPLICATION->IncludeComponent(
			"citrus:tszh.receipt",
			$arParams['RECEIPT_TEMPLATE'],
			Array(
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
			)
		); ?>
	</section>
</div>