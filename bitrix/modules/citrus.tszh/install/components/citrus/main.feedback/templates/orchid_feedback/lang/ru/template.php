<?
$MESS["MFT_FEEDBACK"] = "Обратная связь";
$MESS["MFT_NAME"] = "Ваше имя";
$MESS["MFT_EMAIL"] = "Ваш E-mail";
$MESS["MFT_MESSAGE"] = "Текст сообщения";
$MESS["MFT_ADD_FILES"] = "Добавить файлы";
$MESS["MFT_SIZE_ERROR_FILES"] = "Превышен максимальный размер загружаемых файлов";
$MESS["MFT_SIZE_ERROR_FILES_BITES"] = "Мб";
$MESS["MFT_CAPTCHA"] = "Защита от автоматических сообщений";
$MESS["MFT_CAPTCHA_CODE"] = "Введите слово на картинке";
$MESS["MFT_SUBMIT"] = "Отправить";
$MESS["OPD_ERROR_MESSAGE"] = "Необходимо дать согласие <br> на обработку персанальных данных";
?>