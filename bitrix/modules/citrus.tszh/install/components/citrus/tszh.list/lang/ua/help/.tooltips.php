<?
$MESS["IBLOCK_TYPE_TIP"] = "Із випадаючого списку вибирається один із створених в системі типів інфоблоків. Після натискання кнопки <b> <i> ок </i> </b> в поле <i> Код інформаційного блоку </i> будуть завантажені інфоблоки, створені для обраного типу.";
$MESS["IBLOCKS_TIP"] = "Перераховано всі інфоблоки обраного типу. Вкажіть потрібні, утримуючи <i> Ctrl </i>.";
$MESS["NEWS_COUNT_TIP"] = "Вказується кількість елементів списку, яке буде виведено на одній сторінці.";
$MESS["SORT_BY1_TIP"] = "У списку перераховані поля, по яких може проводитися сортування новин. Також можна вибрати пункт <i> інше </i> та задати в полі поруч код потрібного параметра.";
$MESS["CACHE_TIME_TIP"] = "Поле слугує для вказівки часу кешування в секундах.";
$MESS["SORT_ORDER1_TIP"] = "Вкажіть , в якому напрямку будуть відсортовані елементи по обраному полю .";
$MESS["SORT_BY2_TIP"] = "Вкажіть поле для другого сортування елементів на випадок збігу значень полів першого сортування.";
$MESS["SORT_ORDER2_TIP"] = "Вкажіть , в якому напрямку будуть відсортовані елементи при другій сортуванню.";
$MESS["DETAIL_URL_TIP"] = "Вказується шлях до сторінки з детальним описом елемента інфоблоків . Через параметр має передаватися ID елемента . За замовчуванням поле містить news/news_detail.php?ID=#ELEMENT_ID# ";
$MESS["ACTIVE_DATE_FORMAT_TIP"] = "У випадаючому списку перераховані всі можливі варіанти показу дати, що формуються всередині компонента. Вибравши пункт <i>другое</i> , можна сформувати свій варіант на підставі PHP - функції <i>date</i>. .";
$MESS["CACHE_TYPE_TIP"] = "<i> Авто </ I> : діє при включеному кешированя протягом заданого часу; <br /> <i> Кешувати </ I> : для кешування необхідно визначити тільки час кешування ; <br /> <i> Чи не кешувати </ я > : кеширования немає в будь-якому випадку .";
?>