<?
$MESS ['IBLOCK_DETAIL_URL'] = "URL, ведущий на страницу с содержимым элемента раздела";
$MESS ['TSZH_DESC_ASC'] = "По возрастанию";
$MESS ['TSZH_DESC_DESC'] = "По убыванию";
$MESS ['TSZH_DESC_FID'] = "ID";
$MESS ['TSZH_DESC_FNAME'] = "Название";
$MESS ['TSZH_DESC_FTSAMP'] = "Дата последнего изменения";
$MESS ['TSZH_DESC_IBORD'] = "Поле для сортировки ТСЖ";
$MESS ['TSZH_DESC_IBBY'] = "Направление для сортировки ТСЖ";
$MESS ['TSZH_DESC_LIST_CONT'] = "Количество ТСЖ на странице";
$MESS ['TSZH_DESC_PAGER_TSZH'] = "ТСЖ";
?>