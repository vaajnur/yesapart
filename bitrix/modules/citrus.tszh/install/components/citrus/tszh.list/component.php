<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule('citrus.tszh'))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));
	return;
}

if (!CTszhFunctionalityController::CheckEdition())
	return;

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 7200;

$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
if(strlen($arParams["SORT_BY"])<=0)
	$arParams["SORT_BY"] = "NAME";
if($arParams["SORT_ORDER"]!="DESC")
	 $arParams["SORT_ORDER"]="ASC";

if (!is_array($arParams['FIELD_CODE']))
	$arParams["FIELD_CODE"] = Array('*');

$arParams["TSZH_COUNT"] = intval($arParams["TSZH_COUNT"]);
if($arParams["TSZH_COUNT"]<=0)
	$arParams["TSZH_COUNT"] = 20;

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]!="N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]!=="N";

if($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"])
{
	$arNavParams = array(
		"nPageSize" => $arParams["TSZH_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
}
else
{
	$arNavParams = array(
		"nTopCount" => $arParams["TSZH_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

if($this->StartResultCache(false, Array($arNavigation)))
{
	$arResult=array(
		"SITES" => Array(),
		"ITEMS" => Array(),
	);

	$rsSite = CSite::GetList($by="sort", $order="asc");
	while ($arSite = $rsSite->GetNext())
	{
		if (strlen($arSite['DOMAINS']) > 0)
		{
			$arSite['DOMAINS'] = explode("\n", $arSite['DOMAINS']);
			foreach ($arSite['DOMAINS'] as $key => $domain)
				$arSite['DOMAINS'][$key] = trim($domain);
		}

		$arResult["SITES"][$arSite["LID"]] = array(
			"LID" => $arSite["LID"],
			"NAME" => $arSite["NAME"],
			"LANG" => $arSite["LANGUAGE_ID"],
			"DIR" => $arSite["DIR"],
			"DOMAINS" => $arSite["DOMAINS"],
			"CURRENT" => $arSite["LID"] == SITE_ID ? "Y" : "N",
		);
	}

	$arSelect = array_merge($arParams["FIELD_CODE"], array(
		"ID",
		"NAME",
		"SITE_ID",
		"CODE",
		"UF_*",
	));
	$arFilter = array ("SITE_ID" => SITE_ID);
	$arOrder = array(
		$arParams["SORT_BY"]=>$arParams["SORT_ORDER"],
	);
	if(!array_key_exists("ID", $arOrder))
		$arOrder["ID"] = "DESC";
	$rsItems = CTszh::GetList($arOrder, $arFilter, false, $arNavParams, $arSelect);
	while($arItem = $rsItems->GetNext())
	{
		$arItem["HREF"] = '#';
		$arSite = $arResult["SITES"][$arItem["SITE_ID"]];
		if (is_array($arSite)) {
			$arItem["HREF"] = ((is_array($arSite['DOMAINS']) && strlen($arSite['DOMAINS'][0]) > 0 || strlen($arSite['DOMAINS']) > 0) ? 'http://' : '') . (is_array($arSite["DOMAINS"]) ? $arSite["DOMAINS"][0] : $arSite["DOMAINS"]) . $arSite["DIR"];
		}

		$arResult["ITEMS"][]=$arItem;
	}

	$arResult["NAV_STRING"] = $rsItems->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
	$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
	$arResult["NAV_RESULT"] = $rsItems;
	$this->SetResultCacheKeys(array(
		'ITEMS',
		'NAV_STRING',
	));
	$this->IncludeComponentTemplate();
}
?>