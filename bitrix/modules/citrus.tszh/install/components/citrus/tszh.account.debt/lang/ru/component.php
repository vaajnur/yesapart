<?
$MESS["TSZH_MODULE_NOT_FOUND_ERROR"] = "Модуль 1С: Сайт ЖКХ.";
$MESS["TSZH_ALREADY_CONFIRMED"] = "Ваша принадлежность к лицевому счету уже подтверждена.";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Модуль информационных блоков не установлен.";
$MESS["TSZH_NO_TSZHS"] = "Не найдено ни одного ТСЖ.";
$MESS["TSZH_WRONG_CAPTCHA"] = "Символы, указанные на картинке, введены не верно.";
$MESS["ERROR_TSZH_NOT_SELECTED"] = "Пожалуйста, выберите ТСЖ";
$MESS["ERROR_REGCODE_EMPTY"] = "Необходимо ввести кодовое слово";
$MESS["ERROR_WRONG_CODE"] = "Кодовое слово введено неверно.";
$MESS["ERROR_SAVING_ACCOUNT"] = "При сохранении лицевого счета произошла ошибка";
$MESS["CONFIRM_ACCOUNT_SUCCESS"] = "Лицевой счет подтвержден.";
$MESS["TSZH_ACCOUNT_ALREADY_CONFIRMED"] = "Лицевой счет уже привязан к пользователю на сайте.";
$MESS ['TSZH_NOT_A_MEMBER'] = "Вы не являетесь владельцем лицевого счета.";
?>