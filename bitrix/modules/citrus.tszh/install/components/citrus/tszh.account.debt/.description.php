<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("TSZH_ACCOUNT_DEBT_NAME"),
	"DESCRIPTION" => GetMessage("TSZH_ACCOUNT_DEBT_DESC"),
	"ICON" => "/images/menu_ext.gif",
	"PATH" => array(
		"ID" => "citrus.tszh",
		"NAME" => GetMessage("COMPONENT_ROOT_SECTION"),
	),
);
?>