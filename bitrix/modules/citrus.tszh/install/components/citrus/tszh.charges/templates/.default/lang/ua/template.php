<?
$MESS["TSZH_CHARGES_SERVICE"] = "Послуга";
$MESS["TSZH_CHARGES_CHARGED"] = "Нараховано";
$MESS["TSZH_CHARGES_TO_PAY"] = "До сплати";
$MESS["TSZH_CHARGES_YOU_HAVE_NO_CHARGES"] = "У вас немає нарахувань.";
$MESS["TSZH_CHARGES_DEBT"] = "Заборгованість";
$MESS["TSZH_CHARGES_BALANCE"] = "Ваш баланс";

$MESS['TSZH_CHARGES_YANDEX_PAYMENT_DESCR'] = "за послуги ЖКГ по о/р № #ACCOUNT_NUMBER# за #PREV_MONTH#";
$MESS["CITRUS_TSZHPAYMENT_LINK"] = "<p>Ви можете <a href=\"#LINK#\">оплатити комунальні послуги</a> онлайн.</p>";
?>