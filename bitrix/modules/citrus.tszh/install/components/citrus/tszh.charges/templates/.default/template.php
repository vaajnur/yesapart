<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$totalCorrection = 0;

?>
<h3 style="margin-bottom: 10px;"><?=$arResult['DISPLAY_NAME']?></h3>
<table class="data-table">
<thead>
	<tr>
		<th style="line-height: 29px;"><?=GetMessage("TSZH_CHARGES_SERVICE")?></th>
		<th><?=GetMessage("TSZH_CHARGES_CHARGED")?></th>
		<th><?=GetMessage("TSZH_CHARGES_CORRECTION")?></th>
		<th><?=GetMessage("TSZH_CHARGES_TO_PAY")?></th>
	</tr>
</thead>
<tbody>
<?if (count($arResult['CHARGES']) > 0):?>
	<?foreach ($arResult['CHARGES'] as $arItem):

		if ($arItem["DEBT_ONLY"] == "Y" && $arItem["DEBT_END"] == 0 && $arItem["CORRECTION"] == 0)
			continue;

		if ($arItem["COMPONENT"] == "N")
			$totalCorrection += $arItem['CORRECTION'];
	?>
	<tr>
		<td><?=$arItem['SERVICE_NAME']?>:</td>
		<?
		if ($arItem["DEBT_ONLY"] == "Y")
		{
			?><td style="text-align: right">&mdash;</td><?
		}
		else
		{
			?><td class="cost"><?=CTszhPublicHelper::FormatCurrency($arItem['SUMM'])?></td><?
		}
		?>
		<td class="cost"><?=CTszhPublicHelper::FormatCurrency($arItem['CORRECTION'])?></td>
		<td class="cost"><?=CTszhPublicHelper::FormatCurrency($arItem['DEBT_END'])?></td>
	</tr>
	<?endforeach;?>
<?else:?>
	<tr><td colspan="3"><em><?=GetMessage("TSZH_CHARGES_YOU_HAVE_NO_CHARGES")?></em></td></tr>
<?endif;?>
</tbody>
<tfoot>
	<tr>
		<td colspan="2"><strong><?=$arResult['ACCOUNT_PERIOD']['DEBT_END'] > 0 ? GetMessage("TSZH_CHARGES_DEBT") : GetMessage("TSZH_CHARGES_BALANCE")?>:</strong></td>
		<td class="cost"><?=CTszhPublicHelper::FormatCurrency($totalCorrection)?></td>
		<td class="cost"><?=CTszhPublicHelper::FormatCurrency(abs($arResult['ACCOUNT_PERIOD']['DEBT_END']))?></td>
	</tr>
</tfoot>
</table>

<?

$summ2pay = $arResult["ACCOUNT_PERIOD"]["DEBT_END"];
if (COption::GetOptionString("citrus.tszh", "pay_to_executors_only", "N") == "Y") {
	$summ2pay = CTszhAccountContractor::GetList(array(), array("ACCOUNT_PERIOD_ID" => $arResult["ACCOUNT_PERIOD"]["ID"], "!CONTRACTOR_EXECUTOR" => "N"), array("SUMM"))->Fetch();
	$summ2pay = is_array($summ2pay) ? $summ2pay['SUMM'] - $arResult["ACCOUNT_PERIOD"]["PREPAYMENT"] : 0;
}
if ($summ2pay > 0 && CModule::IncludeModule("citrus.tszhpayment") && ($paymentPath = CTszhPaySystem::getPaymentPath($arResult["ACCOUNT"]["TSZH_SITE"])))
	echo '<div>' . GetMessage("CITRUS_TSZHPAYMENT_LINK", Array("#LINK#" => $paymentPath . '?summ=' . round($summ2pay))) . '</div>';
