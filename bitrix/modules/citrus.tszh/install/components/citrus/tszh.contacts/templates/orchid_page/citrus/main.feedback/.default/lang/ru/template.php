<?
$MESS["MFT_NAME"] = "Ваше имя";
$MESS["MFT_EMAIL"] = "Ваш E-mail";
$MESS["MFT_MESSAGE"] = "Сообщение";
$MESS["MFT_CAPTCHA"] = "Защита от автоматических сообщений";
$MESS["MFT_CAPTCHA_CODE"] = "Введите слово на картинке";
$MESS["MFT_SUBMIT"] = "Отправить";
$MESS["MFT_SIZE_ERROR_FILES_BITES"] = "Мб";
$MESS["T_HEADER"] = "Написать нам";
$MESS["MFT_INPUT_CAPTCHA"] = "Введите символы";
?>