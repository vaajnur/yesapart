<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?>

<div id="contacts-page-feedback">
    <h3><a name="feedbackForm"></a><?=GetMessage("T_HEADER")?>:</h3>


    <?
    $frame = $this->createFrame()->begin('');
    ?>
    <form id="feedbackForm" action="<?=$APPLICATION->GetCurPage()?>#feedbackForm" method="POST" enctype="multipart/form-data">
        <?
        if(!empty($arResult["ERROR_MESSAGE"]))
        {
            echo '<div class="feedback-error"><div>'.join('</div><div>', $arResult["ERROR_MESSAGE"]).'</div></div>';
        }
        if (strlen($arResult["OK_MESSAGE"]) > 0):
            if (strlen($arResult["OK_MESSAGE"]) <= 0)
                $arResult["OK_MESSAGE"] = $arParams["OK_TEXT"];?>
            <div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div>
        <?endif?>

        <div class="left-fields-container">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="tszh_id" id="feedback-tszh_id" value="<?=$arParams["TSZH_ID"]?>">
            <input type="text" name="user_name" class="name styled" placeholder="<?=GetMessage("MFT_NAME")?>"  value="<?=$arResult["AUTHOR_NAME"]?>">
            <input type="text" name="user_email" class="mail styled" placeholder="<?=GetMessage("MFT_EMAIL")?>" value="<?=$arResult["AUTHOR_EMAIL"]?>">

            <?if ($arParams["USE_CAPTCHA"] == "Y"):?>
                <div class="feedback-captcha">
                    <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
                    <div class="feedback-captcha__img">
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" height="40" alt="CAPTCHA"/>
                    </div>
                    <div class="feedback-captcha__input">
                        <input type="text" name="captcha_word" class="captcha styled" width="50" placeholder="<?=GetMessage("MFT_INPUT_CAPTCHA")?>" size="30" maxlength="50" value=""/>
                    </div>
                </div>
            <?endif?>
        </div>

        <div class="right-fields-container">
            <textarea name="MESSAGE" class="text-mai height-textarea styled" placeholder="<?=GetMessage("MFT_MESSAGE")?>"><?=$arResult["MESSAGE"]?></textarea>
            <?
            $MAX_UPLOAD_FILE_SIZE = intval($arParams["MAX_FILE_SIZE"])*1024*1024;
            $_SESSION["MAX_FILE_SIZE_CONTACT"] = $MAX_UPLOAD_FILE_SIZE;
            $pseudoID = md5(random_int(10000, 99999));
            ?>

            <?if ($arParams["USE_CAPTCHA"] == "Y"){?>
                <div class="uploadform_files" id="uploadform_files_<?=$pseudoID?>">
                    <div class="uploadform_files_field">
                        <input name="sessid" type="hidden" value="<?=bitrix_sessid();?>" />
                        <input type="hidden" name="MAX_FILE_SIZE" value="<?=$MAX_UPLOAD_FILE_SIZE?>">
                        <input type="file" multiple="multiple" value="" />
                        <?foreach($_REQUEST["filepath"] as $key => $path){?>
                            <div class="uploadform_files_item">
                                <input type="hidden" name="filename[]" value="<?=$_REQUEST["filename"][$key]?>" />
                                <input type="hidden" name="filepath[]" value="<?=$path?>" />
                                <p><span><?=$_REQUEST["filename"][$key]?></span><a class="uploadform_files_item_delete" href="#">&times;</a></p>
                            </div>
                        <?}?>
                        <span><?=GetMessage("MFT_ADD_FILES")?></span>
                    </div>
                </div>
                <p class="files_error"></p>
            <?}?>
        </div>
        <?if($arParams["USE_CAPTCHA"] != "Y"){?>
            <div class="uploadform_files" id="uploadform_files_<?=$pseudoID?>">
                <div class="uploadform_files_field">
                    <input name="sessid" type="hidden" value="<?=bitrix_sessid();?>" />
                    <input type="hidden" name="MAX_FILE_SIZE" value="<?=$MAX_UPLOAD_FILE_SIZE?>">
                    <input type="file" multiple="multiple" value="" />
                    <?foreach($_REQUEST["filepath"] as $key => $path){?>
                        <div class="uploadform_files_item">
                            <input type="hidden" name="filename[]" value="<?=$_REQUEST["filename"][$key]?>" />
                            <input type="hidden" name="filepath[]" value="<?=$path?>" />
                            <p><span><?=$_REQUEST["filename"][$key]?></span><a class="uploadform_files_item_delete" href="#">&times;</a></p>
                        </div>
                    <?}?>
                    <span><?=GetMessage("MFT_ADD_FILES")?></span>
                </div>
            </div>
            <p class="files_error"></p>
        <?}?>
        <?
        if ($USER->IsAuthorized()) {
            if ($arResult["CONFIRM_TSZH"] == "Y")
            {
                if ($arResult["CONFIRM_ACC"] != "Y")
                {
                    ?> <div class="feedbackForm__confirm">
                    <div class="input-checkbox feedback-checkbox"><input name="confirm" type="checkbox" id="confirm" ><label for="confirm">&nbsp;</label></div>
                    <p><?= $arResult["TSZH_DATA"]["~CONFIRM_TEXT"] ?><br>
                </div>
                    <?
                }
            }
        }
        else{
            if ($arResult["CONFIRM_TSZH"] == "Y")
            {
                ?>
                <div class="feedbackForm__confirm">
                    <div class="input-checkbox feedback-checkbox">
                        <input name="confirm" type="checkbox" id="confirm" required><label for="confirm">&nbsp;</label></div>
                    <p><?= $arResult["TSZH_DATA"]["~CONFIRM_TEXT"] ?></p>
                </div>
                <?
            }
        }
        ?>
        <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>"/>
        <button type="submit" class="styled" name="submit" value="Y"><?=GetMessage("MFT_SUBMIT")?></button>

    </form>
    <?
    $frame->end();
    ?>
</div>
<script>
    // ����� � ���������

    $(document).ready(function(){

        var files;
        function validateSize(fileInput,size) {
            var fileObj, oSize;
            if ( typeof ActiveXObject == "function" ) { // IE
                fileObj = (new ActiveXObject("Scripting.FileSystemObject")).getFile(fileInput.value);
            }else {
                fileObj = fileInput.files[0];
            }

            oSize = fileObj.size; // Size returned in bytes.
            if(oSize > size){
                return false;
            }
            return true;
        }
        $('body').on('change','#uploadform_files_<?=$pseudoID?> input[type="file"]',function(e){
            e.preventDefault();
            var $file = $(this),
                $form = $file.parents('form');
            // console.log('*'+$form.index());
            $form.find('[type="submit"]').removeAttr('disabled');
            $form.removeClass('error');
            files = this.files;
            if(typeof files == 'undefined')
                return;
            var data = new FormData();
            $.each(files, function(key, value){
                data.append(key, value);
            });
            var upoadedFiles = new Array();
            $form.find('[name="filepath[]"]').each(function(){
                upoadedFiles.push($(this).val());
            });
            if(files.length&&!validateSize(this,$('input[name="MAX_FILE_SIZE"]').val())){
                $form.find('.files_error').css({'display':'block'});
                $form.find('.files_error').text('<?=GetMessage("MFT_SIZE_ERROR_FILES").' '.$arParams["MAX_FILE_SIZE"].GetMessage("MFT_SIZE_ERROR_FILES_BITES")?>');
            }
            if(files.length&&validateSize(this,$('input[name="MAX_FILE_SIZE"]').val())){
                data.append('filepath',upoadedFiles.join('|'));
                data.append('sessid', $form.find('[name="sessid"]').val());
                data.append('MAX_FILE_SIZE', $form.find('input[name="MAX_FILE_SIZE"]').val());
                $.ajax({
                    url: '<?=$this->GetFolder()?>/ajax_files_upload.php',
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function(respond, status, jqXHR){
                        console.log(respond);
                        if(typeof respond.error === 'undefined'){
                            var upfiles = respond.files;
                            var html = '';
                            $.each(upfiles, function(key, val){
                                html += '<div class="uploadform_files_item"><input type="hidden" name="filename[]" value="'+val.filename+'" /><input type="hidden" name="filepath[]" value="'+val.path+'" /><p><span>'+val.filename+'</span><a class="uploadform_files_item_delete" href="#">&times;</a></p></div>';
                            } )
                            $form.find('.uploadform_files_field > span').before(html);
                            // ������� ����� ����������� ������, ���� ������������ ������ ���� � ��� �� ���� ��������� ���
                            $form.find('.uploadform_files_item').each(function(){
                                if($('.uploadform_files_item [name="filepath[]"][value="'+$(this).find('[name="filepath[]"]').val()+'"]').length > 1)
                                    $(this).remove();
                            });
                            if(respond.toosize == 'too_big_files'){
                                $form.addClass('error');
                                $form.find('[type="submit"]').attr('disabled','disabled');
                                $form.find('.files_error').text('<?=GetMessage("MFT_SIZE_ERROR_FILES").' '.$arParams["MAX_FILE_SIZE"].GetMessage("MFT_SIZE_ERROR_FILES_BITES")?>');
                            }
                        }
                        else{
                            $form.addClass('error');
                            $form.find('[type="submit"]').attr('disabled','disabled');
                            console.log('������: ' + respond.error);
                        }
                    },
                    error: function( jqXHR, status, errorThrown){
                        console.log( '������ AJAX �������: ' + status, jqXHR);
                    }
                });
            }
        });

        $('body').on('click','#uploadform_files_<?=$pseudoID?> .uploadform_files_item_delete',function(e){
            e.preventDefault();
            var $form = $(this).parents('form');
            $(this).parents('.uploadform_files_item').remove();
            if($form.find('.uploadform_files_item').length){
                $form.find('[type="submit"]').removeAttr('disabled');
                $form.removeClass('error');
                var data = new FormData();
                var upoadedFiles = new Array();
                $form.find('[name="filepath[]"]').each(function(){
                    upoadedFiles.push($(this).val());
                });
                if(files.length){
                    data.append('filepath',upoadedFiles.join('|'));
                    data.append('sessid', $form.find('[name="sessid"]').val());
                    data.append('MAX_FILE_SIZE', $form.find('input[name="MAX_FILE_SIZE"]').val());
                    $.ajax({
                        url: '<?=$this->GetFolder()?>/ajax_files_upload_size.php',
                        type: 'POST',
                        data: data,
                        cache: false,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function(respond, status, jqXHR){
                            console.log(respond);
                            if(typeof respond.error !== 'undefined'){
                                $form.addClass('error');
                                $form.find('[type="submit"]').attr('disabled','disabled');
                                $form.find('.files_error').text('<?=GetMessage("MFT_SIZE_ERROR_FILES")?> <?=$arParams["MAX_FILE_SIZE"]?>��');
                            }
                        },
                        error: function( jqXHR, status, errorThrown){
                            console.log( '������ AJAX �������: ' + status, jqXHR);
                        }
                    });
                }
            }
            else{
                $form.find('[type="submit"]').removeAttr('disabled');
                $form.removeClass('error');
            }
        });

    });
</script>