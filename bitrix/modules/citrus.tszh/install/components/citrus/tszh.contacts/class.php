<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class CCitrusTszhContacts extends CBitrixComponent
{
	/**
	 * ���������� �������� �������� ���������� From, Sender � To ��� ������, ������������� �� ����� �������� �����
	 * 
	 * ������ ����������� �������� ���������� ���������:
	 * From:
 	 * �������� ����� ����� ��:
	 * 	 1. ��������� "E-Mail ����� �� ���������" �����, � �������� ��������� ��������� �� �������� ��������� ��� (���� ���� email �����)
	 *   2. ��������� "E-Mail �������������� ����� (����������� �� ���������)" �������� ������ (���� email ����� ��� �� �����)
	 *   3. ���� ����� ����������� email'� �� ��������� � ������� �������� ����� (�� $_SERVER["HTTP_HOST"]), �� ������ �������������� email-����� no-reply@�����_��������_�����.
	 * 
	 * Sender:
 	 * �������� ����� ����� ��:
	 * 	 1. ��������� "E-Mail ����� �� ���������" �����, � �������� ��������� ��������� �� �������� ��������� ��� (���� ���� email �����)
	 *   2. ��������� "E-Mail �������������� ����� (����������� �� ���������)" �������� ������ (���� email ����� ��� �� �����)
	 *   3. ���� ���������� email ��������� � From, �� ������ ������ ������.
	 * 
	 * To:
 	 * ���� � ��������� $to �������� �������� ������, �� ����� ������ �, ����� ������������ �������� ����� ����� ��:
	 * 	 1. ��������� "E-mail ����������� ��� ����������� � ���������" ���������� �� �������� ��������� ��� (���� ���� email �����)
	 * 	 2. ��������� "E-Mail ����� �� ���������" �����, � �������� ��������� ��������� �� �������� ��������� ��� (���� �� �����, � 
	 *      "E-mail ����������� ��� ����������� � ���������" - ���)
	 *   3. ��������� "E-Mail �������������� ����� (����������� �� ���������)" �������� ������ (���� "E-mail ����������� ���
	 *      ����������� � ���������" � email ����� ���������� ��� �� ������).
	 *
 	 * @param array $arTszh		������ ����� ������� ����������, �������� ������������ ������
  	 * @param string $from		���������� �������� ��������� From
   	 * @param string $sender	���������� �������� ��������� Sender
 	 * @param string $to		������� � ����� �������� ��������� FEEDBACK_FORM_EMAIL_TO ����������; ���������� - �������� ��������� To
	 * @return 
	 */
	public static function getFeedbackFormMailHeaders($arTszh, &$from, &$sender, &$to)
	{
		/*$arSite = CSite::getByID($arTszh["SITE_ID"])->fetch();
		$defaultEmailFrom = COption::GetOptionString("main", "email_from");

		// From
	    if (strlen($arSite["EMAIL"]))
			$from = $arSite["EMAIL"];
	    else
			$from = $defaultEmailFrom;
		$arFrom = explode("@", $from);
		$url = "http://" . $_SERVER["HTTP_HOST"];
		$arUrl = parse_url($url);
		if ($arUrl["host"] != $arFrom[1])
			$from = "no-reply@" . $arUrl["host"];

		// Sender
	    if (strlen($arSite["EMAIL"]))
			$sender = $arSite["EMAIL"];
	    else
			$sender = $defaultEmailFrom;
		if ($sender == $from)
			$sender = "";*/

		$from = $sender = true;
		CTszhSubscribe::getMailHeaders($arTszh["SITE_ID"], $from, $sender);

		// To
		if (strlen($to) <= 0)
		{
			$arSite = CSite::getByID($arTszh["SITE_ID"])->fetch();
			$defaultEmailFrom = COption::GetOptionString("main", "email_from");

			if (strlen($arTszh["EMAIL"]))
				$to = $arTszh["EMAIL"];
	        elseif (strlen($arSite["EMAIL"]))
				$to = $arSite["EMAIL"];
	        else
				$to = $defaultEmailFrom;
		}
	}
}
