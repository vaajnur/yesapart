<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?><?$APPLICATION->IncludeComponent(
    "bitrix:socserv.auth.form",
    "",
    $arParams,
    $this->__component
);?>