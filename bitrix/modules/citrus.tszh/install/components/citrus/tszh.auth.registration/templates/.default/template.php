<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/** @var CBitrixComponent $component ������� ��������� ��������� */
/** @var CBitrixComponentTemplate $this ������� ������ (������, ����������� ������) */
/** @var array $arResult ������ ����������� ������ ���������� */
/** @var array $arParams ������ �������� ���������� ����������, ����� �������������� ��� ����� �������� ���������� ��� ������ ������� (��������, ����������� ��������� ����������� ��� ������). */
/** @var string $templateFile ���� � ������� ������������ ����� �����, �������� /bitrix/components/bitrix/iblock.list/templates/.default/template.php) */
/** @var string $templateName ��� ������� ���������� (��������: .d�fault) */
/** @var string $templateFolder ���� � ����� � �������� �� DOCUMENT_ROOT (�������� /bitrix/components/bitrix/iblock.list/templates/.default) */
/** @var array $templateData ������ ��� ������, �������� ��������, ����� ������� ����� �������� ������ �� template.php � ���� component_epilog.php, ������ ��� ������ �������� � ���, �.�. ���� component_epilog.php ����������� �� ������ ���� */
/** @var string $parentTemplateFolder ����� ������������� �������. ��� ����������� �������������� ����������� ��� �������� (��������) ������ ������������ ��� ����������. �� ����� ��������� ��� ������������ ������� ���� ������������ ����� ������� */
/** @var string $componentPath ���� � ����� � ����������� �� DOCUMENT_ROOT (����. /bitrix/components/bitrix/iblock.list) */

ShowMessage($arParams["~AUTH_RESULT"]);

if ($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) && $arParams["AUTH_RESULT"]["TYPE"] === "OK")
{
	?><p><?
	echo GetMessage("AUTH_EMAIL_SENT") ?></p><?
}

// TODO ���������� �� ������� ����������

$optionPhoneAdd = (COption::GetOptionString('citrus.tszh', 'input_phone_add', "Y") == "Y");
$optionPhoneRequired = (COption::GetOptionString('citrus.tszh', 'input_phone_require', "Y") == "Y");

$arSF = Array(
	"NAME",
	"SECOND_NAME",
	"LAST_NAME",
);
if ($optionPhoneAdd)
{
	$arSF[] = "PERSONAL_PHONE";
}

$arRF = Array(
	"NAME",
	"LAST_NAME",
);

if ($optionPhoneAdd && $optionPhoneRequired)
{
	$arRF[] = "PERSONAL_PHONE";
}

$APPLICATION->IncludeComponent(
	"citrus:tszh.register",
	"",
	Array(
		"USER_PROPERTY_NAME" => "",
		"SEF_MODE" => "N",
		"SHOW_FIELDS" => $arSF,
		"REQUIRED_FIELDS" => $arRF,
		"AUTH" => "Y",
		"USE_BACKURL" => "N",
		"SUCCESS_PAGE" => $APPLICATION->GetCurPageParam('registerSuccess=yes', array('backurl', 'register')),
		"SET_TITLE" => "Y",
		"USER_PROPERTY" => Array(
			"UF_ACCOUNT",
		),
	)
); ?>

<p><a href="<?=$arResult["AUTH_AUTH_URL"]?>"><b><?=GetMessage("AUTH_AUTH")?></b></a></p>