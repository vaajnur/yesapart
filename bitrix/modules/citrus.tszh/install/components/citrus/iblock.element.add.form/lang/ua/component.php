<?
$MESS["IBLOCK_ADD_ERROR_REQUIRED"] = "Поле '#PROPERTY_NAME#' повинно бути заповнене!";
$MESS["IBLOCK_ADD_ERROR_EMAIL"] = "У полі '# PROPERTY_NAME #' введений некоректний e-mail!";
$MESS["IBLOCK_ADD_ELEMENT_NOT_FOUND"] = "Елемент не знайдений";
$MESS["IBLOCK_ADD_ACCESS_DENIED"] = "Немає доступу";
$MESS["IBLOCK_FORM_WRONG_CAPTCHA"] = "Невірно введене слово з малюнка";
$MESS["IBLOCK_ADD_MAX_ENTRIES_EXCEEDED"] = "Перевищене максимальну кількість записів";
$MESS["IBLOCK_ADD_MAX_LEVELS_EXCEEDED"] = "Перевищене максимальну кількість розділів - #MAX_LEVELS#";
$MESS["IBLOCK_ADD_LEVEL_LAST_ERROR"] = "Дозволено додавання тільки в розділи останнього рівня";
$MESS["IBLOCK_USER_MESSAGE_ADD_DEFAULT"] = "Елемент успішно доданий";
$MESS["IBLOCK_USER_MESSAGE_EDIT_DEFAULT"] = "Зміни збережені успішно";
$MESS["IBLOCK_ERROR_FILE_TOO_LARGE"] = "Розмір завантажуваного файлу перевищує допустиме значення";
$MESS["IBLOCK_ERROR_NEED_AUTH"] = "Для того, щоб задати питання, необхідно <a href=\"#AUTH_URL#\">авторизуватись</a>";
?>