$(document).ready(function () {
    $('.ask-form').submit(
        function (event) {
            var confirm = $(this).find('[name="confirm"]');
            var input = $('#confirm_window-form');
            if(typeof confirm != 'undefined'&& input.length) {
                if(confirm.is(":checked"))
                {
                    $('.opdqa__input-opd-err').hide();
                }
                else
                {
                    event.preventDefault();
                    $('.opdqa__input-opd-err').show();
                }
            }
        });
});