<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (!CModule::IncludeModule("citrus.tszh"))
	die("failure\n" . GetMessage("CITRUS_TSZH_1C_ERROR_MODULE"));

/** @var CCitrusTszh1cExchange $this */

$exchangeFacade = new \Citrus\Tszh\Exchange\Facade($params["SKIP_PERMISSION_CHECK"] == "Y");
$exchangeFacade->handleRequest($_GET["mode"], $_GET['type']);
