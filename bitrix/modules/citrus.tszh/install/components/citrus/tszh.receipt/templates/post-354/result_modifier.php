<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// �������� ������� ����� � ������� 3?
// ===================================
$arResult['HAS_PENALTIES'] = COption::GetOptionString(TSZH_MODULE_ID, "post_354_ShowPenaltiesColumn") == "Y";

if ($arResult["ACCOUNT_PERIOD"]["TYPE"] == 0) {
    $sub�title = GetMessage("TPL_DOCUMENT_SUBTITLE_FOR_BASIC");
} elseif ($arResult["ACCOUNT_PERIOD"]["TYPE"] == 1) {
    $sub�title = GetMessage("TPL_DOCUMENT_SUBTITLE_FOR_BASIC_PENALTIES");
} elseif ($arResult["ACCOUNT_PERIOD"]["TYPE"] == 2) {
    $sub�title = GetMessage("TPL_DOCUMENT_SUBTITLE_FOR_MAJOR_REPAIRS");
} elseif ($arResult["ACCOUNT_PERIOD"]["TYPE"] == 3) {
    $sub�title = GetMessage("TPL_DOCUMENT_SUBTITLE_FOR_MAJOR_REPAIRS_PENALTIES");
};

$arResult["DOCUMENT_SUBTITLE"] = $sub�title;

/**
 * � �������� ���������� ������� � ��� ���������� ������ ������ ����������-����������� ����� (��� ������� � 1�)
 */
if (is_array($arResult["EXECUTOR"]))
{
	$arResult["TSZH"]["ADDRESS"] = $arResult["EXECUTOR"]["ADDRESS"];
	$fieldsMap = array(
		"ORG_TITLE" => 'NAME',
		"ORG_INN" => 'INN',
		"ORG_KPP" => 'KPP',
		"ORG_RSCH" => 'RSCH',
		"ORG_BANK" => 'BANK',
		"ORG_KSCH" => 'KSCH',
		"ORG_BIK" => 'BIK',
	);
	foreach ($fieldsMap as $to => $from)
	{
		if (isset($arResult["EXECUTOR"][$from]) && strlen($arResult["EXECUTOR"][$from]))
		{
			$arResult[$to] = $arResult["EXECUTOR"][$from];
		}
	}
}