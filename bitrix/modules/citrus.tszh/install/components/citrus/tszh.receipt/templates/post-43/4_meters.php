<?
use Citrus\Tszh\Types\ComponentType;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

// �������� ����� ������, �� ������� ������� ������
$sourceFields = array(
    array(
        "name",
        "edizm",
        "tarif1..tariff3",
        "hnorm",
        "meter",
        "hmeter",
        "volumep",
        "volumeh",
    ),
);
$colsCount = array();
foreach ($sourceFields as $idx => &$sf) {
    $sf = array_values(array_diff($sf, array(null)));
    $colsCount[$idx] = count($sf);
}
unset($sf);

?>
<table class="rcpt-inner rcpt-inner-table" style="border: none; width: 99%;">
    <tr class="rcpt-inner-empty">
        <?

        ?>
        <td colspan="8<? /*=(count($sourceFields[1]))*/ ?>">
            <?= GetMessage("TPL_SECTION4") ?>
        </td>

    </tr>
    <tr>
        <th rowspan="3" style="width: 15%"><?= GetMessage("TPL_SERVICES") ?></th>
        <th rowspan="3"><?= GetMessage("TPL_UNITS") ?></th>
        <th rowspan="3" rowspan="2"><?= GetMessage("TPL_NORM_IND") ?></th>
        <th rowspan="3" rowspan="2"><?= GetMessage("TPL_NORM_SOI") ?></th>

        <?
        if (!$arResult["IS_FINES_RECEIPT"]) {
            ?>

            <th colspan="2" rowspan="2"><?= GetMessage("TPL_METERS_CURRENT") ?></th>
            <th colspan="2" rowspan="2"><?= GetMessage("TPL_TOTAL_VOLUME") ?></th>
            <?
        } else {
            ?>
            <th colspan="<?= $colsCount[1] ?>" rowspan="2" style="border: 1pt solid #fff"></th><?
        }
        ?>
    </tr>
    <tr>
        <!--<th rowspan="2"><? /*=GetMessage("TPL_TOTAL")*/ ?></th>-->
        <!-- <th colspan="2"><? /*=GetMessage("TPL_FOR_SERVICE")*/ ?></th>-->
    </tr>
    <tr>
        <th><?= GetMessage("TPL_PERSONAL_CONS") ?></th>
        <?
        /*if ($arResult["HAS_AMOUNT_NOTES"])
            echo '<th>*</th>';*/
        ?>
        <th><?= GetMessage("TPL_SHARED_CONS") ?></th>
        <?
        /*if ($arResult["HAS_AMOUNT_NOTES"])
            echo '<th>*</th>';*/
        ?>
        <th><?= GetMessage("TPL_PERSONAL_CONS") ?></th>
        <th><?= GetMessage("TPL_SHARED_CONS") ?></th>

    </tr>
    <tr class="rcpt-inner-table-num">
        <?
        for ($i = 1; $i <= $colsCount[0] - ($arResult["HAS_AMOUNT_NOTES"] ? 2 : 0); $i++) {
            ?>
            <th<?= ($arResult["HAS_AMOUNT_NOTES"] && ($i == 3 || $i == 4) ? ' colspan="2"' : '') ?>><?= $i ?></th>
            <?
        }
        if (!$arResult["IS_FINES_RECEIPT"]) {
            for ($i = 1; $i <= $colsCount[1]; $i++) {
                ?>
                <th><?= $i ?></th>
                <?
            }
        } else {
            ?>
            <th colspan="<?= $colsCount[1] ?>" style="border:1pt solid #fff"></th><?
        }
        ?>
    </tr>
    <?

    /** @var int $tariffIndex ������� ������ ������ (������ ���������� � ������������ ������������� ������ �����������, ������� ������ ������� 1-� �����) */
    $tariffIndex = 0;
    foreach ($arResult['CHARGES'] as $idx => $arCharge) {
        /**
         * ��� ����� � component=2 ������ �� ������� ��������� ��.
         * ��� �������� ������ ����� ��������� ���������� ������� ����� ���� ����� �������, ������ � ������� ��������� ���� ��������� ���� �������� ������
         *
         * ��� ����� � component=1 ������� ����� ��������� ��������� ��������, ������� � �������� ��������� ��� ���� ���������.
         * � �������� ������ ������ �� �������
         */
        /** @var bool $isComponent �������� �� ����������� �� ��������� ������ ��� ������������ �� ������� */
        $isComponent = $arCharge["COMPONENT"] != ComponentType::NONE;
        /** @var int $tariffIndex ������� ������ ��������� (1 - �������, 2 - ������, 3 - �������) ��� ����������� �� ������� */
        $tariffIndex = $isComponent && $arCharge["COMPONENT"] == ComponentType::TARIFFS ? $tariffIndex + 1 : 0;

        // ��� ����� ����������, �� ������� �����������, ����� ����� ������ �����
        if (!$isComponent && !$arCharge["HAS_COMPONENTS"])
            $tariffIndex = 1;

        $meterValue = $hMeterValue = false;
        if ($tariffIndex > 0) {
            $decPlaces = -1;
            // ��������� ��������� �������������� ��������� �� �������� ������
            if ($arResult["HAS_CHARGES_METERS_BINDING"]) {
                $meterValues = array();
                foreach ($arCharge["METER_IDS"] as $meterID) {
                    $arMeter = $arResult["METERS"][$meterID];
                    if (is_array($arMeter) && isset($arMeter["VALUE"])) {
                        $meterValues[] = $arMeter["VALUE"]["VALUE" . $tariffIndex];
                        $decPlaces = max($decPlaces, $arMeter["DEC_PLACES"]);
                    }
                }
                $meterValue = empty($meterValues) ? false : CCitrusTszhReceiptComponentHelper::num(array_sum($meterValues), false, $decPlaces <= 0 ? 2 : $decPlaces);
            } else {
                $meterValues = array();
                foreach ($arResult["METERS"] as $meterID => $arMeter) {
                    if (trim($arMeter["~SERVICE_NAME"]) == trim($arCharge["~SERVICE_NAME"])) {
                        $meterValues[] = $arMeter["VALUE"]["VALUE" . $tariffIndex];
                        $decPlaces = max($decPlaces, $arMeter["DEC_PLACES"]);
                    }
                }
                $meterValue = empty($meterValues) ? false : CCitrusTszhReceiptComponentHelper::num(array_sum($meterValues), false, $decPlaces <= 0 ? 2 : $decPlaces);
            }

            $decPlaces = -1;
            // ��������� ��������� ����������� ��������� �� �������� ������
            $hMeterValues = array();
            foreach ($arCharge["HMETER_IDS"] as $hMeterID) {
                $hMeter = $arResult["HMETERS"][$hMeterID];
                if (is_array($hMeter) && isset($hMeter["VALUE"])) {
                    $hMeterValues[] = $hMeter["VALUE"]["VALUE" . $tariffIndex];
                    $decPlaces = max($decPlaces, $hMeter["DEC_PLACES"]);
                }
            }
            $hMeterValue = empty($hMeterValues) ? false : CCitrusTszhReceiptComponentHelper::num(array_sum($hMeterValues), false, $decPlaces <= 0 ? 2 : $decPlaces);
        } elseif ($arCharge["HAS_COMPONENTS"] === ComponentType::COMPLEX) {
            $decPlaces = -1;
            // ��������� ��������� �������������� ��������� �� �������� ������
            if ($arResult["HAS_CHARGES_METERS_BINDING"]) {
                $meterValues = array();
                foreach ($arCharge["METER_IDS"] as $meterID) {
                    $arMeter = $arResult["METERS"][$meterID];
                    if (is_array($arMeter) && isset($arMeter["VALUE"])) {
                        for ($i = 0; $i < $arMeter["VALUES_COUNT"]; $i++) {
                            $meterValues[$i] += $arMeter["VALUE"]["VALUE" . ($i + 1)];
                        }
                        $decPlaces = max($decPlaces, $arMeter["DEC_PLACES"]);
                    }
                }
                $meterValue = empty($meterValues) ? false : implode('/', $meterValues);
            } else {
                $meterValues = array();
                foreach ($arResult["METERS"] as $meterID => $arMeter) {
                    if (trim($arMeter["~SERVICE_NAME"]) == trim($arCharge["~SERVICE_NAME"])) {
                        for ($i = 0; $i < $arMeter["VALUES_COUNT"]; $i++) {
                            $meterValues[$i] += $arMeter["VALUE"]["VALUE" . ($i + 1)];
                        }
                        $decPlaces = max($decPlaces, $arMeter["DEC_PLACES"]);
                    }
                }
                $meterValue = empty($meterValues) ? false : implode('/', $meterValues);
            }

            $decPlaces = -1;
            // ��������� ��������� ����������� ��������� �� �������� ������
            $hMeterValues = array();
            foreach ($arCharge["HMETER_IDS"] as $hMeterID) {
                $hMeter = $arResult["HMETERS"][$hMeterID];
                if (is_array($hMeter) && isset($hMeter["VALUE"])) {
                    for ($i = 0; $i < $arMeter["VALUES_COUNT"]; $i++) {
                        $hMeterValues[$i] += $hMeter["VALUE"]["VALUE" . ($i + 1)];
                    }
                    $decPlaces = max($decPlaces, $hMeter["DEC_PLACES"]);
                }
            }
            $hMeterValue = empty($hMeterValues) ? false : implode('/', $hMeterValues);
        }

        $arTariffs = array();
        for ($i = 0; $i < 3; $i++) {
            $fieldName = "SERVICE_TARIFF";
            if ($i)
                $fieldName .= $i + 1;
            if ($arCharge[$fieldName])
                $arTariffs[] = $arCharge[$fieldName];
        }
        ?>
        <? if ($arCharge["IS_INSURANCE"] != "Y"):?>
            <tr>
                <td><?= $arCharge['SERVICE_NAME'] ?><?= ($arCharge["HAS_COMPONENTS"] && substr($arCharge['SERVICE_NAME'], -1, 1) !== ':' ? ':' : '') ?></td>
                <td class="center"><?= CCitrusTszhReceiptComponentHelper::getArrayValue(array('UNITS'), $arCharge, false, $arCharge["SERVICE_UNITS"]) ?></td>

                <?
                if (!$arResult["IS_FINES_RECEIPT"]) {
                    ?>
                    <td class="n"><?= CCitrusTszhReceiptComponentHelper::getArrayValue(array("NORM", "HNORM"), $arCharge, true, $arCharge["SERVICE_NORM"] - $arCharge["HNORM"], 3) ?></td>
                    <td class="n"><?= CCitrusTszhReceiptComponentHelper::getArrayValue("HNORM", $arCharge, true, false, 3) ?></td>

                    <td class="n" style="white-space: nowrap;"><?= $meterValue ?></td>
                    <td class="n"><?= $hMeterValue ?></td>

                    <td class="n"><?= CCitrusTszhReceiptComponentHelper::getArrayValue("VOLUMEP", $arCharge, true, false, 3) ?></td>

                    <td class="n"><?= CCitrusTszhReceiptComponentHelper::getArrayValue("VOLUMEH", $arCharge, true, false, 3) ?></td>
                    <?
                } else {
                    ?>
                    <td colspan="<?= $colsCount[1] ?>" style="border: 1px solid #fff;"></td><?
                }
                ?>
            </tr>
        <? endif; ?>
        <?
    } ?>


</table>
