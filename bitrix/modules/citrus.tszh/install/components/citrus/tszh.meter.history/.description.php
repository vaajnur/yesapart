<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("CITRUS_TSZH_METERS_HISTORY"),
	"DESCRIPTION" => GetMessage("CITRUS_TSZH_METERS_HISTORY_DESC"),
	"ICON" => "/images/icon.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "citrus.tszh",
		"NAME" => GetMessage("CITRUS_TSZH")
	),
	"COMPLEX" => "N",
);

?>