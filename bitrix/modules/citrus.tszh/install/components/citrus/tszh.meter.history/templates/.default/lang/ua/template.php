<?
$MESS["CITRUS_TSZH_METER_VALUES_NOT_FOUND"] = "Свідчень лічильників, не знайдено.";
$MESS["CITRUS_TSZH_DATE"] = "Дата";
$MESS["CITRUS_TSZH_METER_TARIF_1"] = "Дневное показание";
$MESS["CITRUS_TSZH_METER_TARIF_2"] = "Ночное показание";
$MESS["CITRUS_TSZH_METER_TARIF_3"] = "Пиковое показание";
$MESS["CITRUS_TSZH_METERS_NAME"] = "Лічильники";
$MESS["CITRUS_TSZH_METERS_VALUE"] = "Показання";
$MESS["CITRUS_TSZH_METERS_HISTORY_TITLE"] = "Історія показань лічильників";
$MESS["CITRUS_TSZH_METERS_NAME"] = "Найменування лічильника:";
$MESS["CITRUS_TSZH_METERS_NUM"] = "Заводський номер:";
$MESS["CITRUS_TSZH_METERS_DATE"] = "Дата останньої перевірки:";
$MESS["CITRUS_TSZH_METERS_SERVICE_NAME"] = "Найменування послуги:";
$MESS["CITRUS_TSZH_METERS_DATE_INPUT"] = "Дата подачі свідчень";
$MESS["CITRUS_TSZH_METERS_BACK_URL_TEXT"] = "Назад";
?>