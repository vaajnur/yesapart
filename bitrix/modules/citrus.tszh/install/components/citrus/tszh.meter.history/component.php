<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!CModule::IncludeModule('citrus.tszh'))
{
	ShowError(GetMessage("CITRUS_TSZH_MODULE_NOT_FOUND_ERROR"));

	return;
}

if (!CTszhFunctionalityController::CheckEdition())
{
	return;
}

// ���� ������������ �� ����������� � ������ ���������� ��ƻ
// ������� ��������� � �������� ������ ����������
if (!CTszh::IsTenant() && !$USER->IsAdmin())
{
	$this->AbortResultCache();
	$APPLICATION->AuthForm(GetMessage("CITRUS_TSZH_NOT_A_MEMBER"));

	return;
}

// set default value for missing parameters, simple param check
$componentParams = CComponentUtil::GetComponentProps($this->getName());
if (is_array($componentParams))
{
	foreach ($componentParams["PARAMETERS"] as $paramName => $paramArray)
	{
		if (!is_set($arParams, $paramName) && is_set($paramArray, "DEFAULT"))
		{
			$arParams[$paramName] = $paramArray["DEFAULT"];
		}

		$paramArray["TYPE"] = ToUpper(is_set($paramArray, "TYPE") ? $paramArray["TYPE"] : "STRING");
		switch ($paramArray["TYPE"])
		{
			case 'INT':
				$arParams[$paramName] = IntVal($arParams[$paramName]);
				break;

			case 'LIST':
				if (!array_key_exists($arParams[$paramName], $paramArray['VALUES']))
				{
					$arParams[$paramName] = $paramArray["DEFAULT"];
				}
				break;

			case 'CHECKBOX':
				$arParams[$paramName] = ($arParams[$paramName] == (is_set($paramArray, 'VALUE') ? $paramArray['VALUE'] : 'Y'));
				break;

			default:
				// string etc.
				break;
		}
	}
}

/*
 * // �� ������������
if (strlen($arParams["FILTER_NAME"]) <= 0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	$arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
	if (!is_array($arrFilter))
	{
		$arrFilter = array();
	}
}
*/

// ������� ���� �������� ������������
$arResult['ACCOUNT'] = null;
$rsAccount = Citrus\Tszh\AccountsTable::getList(
	array(
		'filter' => array(
			"USER_ID" => $USER->GetID(),
			//"CURRENT" => "Y",
		),
	)
);
while ($arAccount = $rsAccount->fetch())
{
	if (is_null($arResult['ACCOUNT']) || ($arAccount["CURRENT"] == "Y"))
	{
		$arResult['ACCOUNT'] = $arAccount;
	}
}
//var_dump($arResult['ACCOUNT']);

if (!is_array($arResult['ACCOUNT']))
{
	$this->AbortResultCache();
	$APPLICATION->AuthForm(GetMessage("CITRUS_TSZH_NOT_A_MEMBER"));

	return;
}

// ���������� � ��������
$arResult["METERS"] = Citrus\Tszh\MetersTable::getList(
	array(
		'filter' => array(
			"ID" => $arParams["ID"],
			"ACTIVE" => "Y",
			"!HOUSE_METER" => "Y",
		),
		'select' => array("*"),
	)
)->fetch();

if ($arResult["METERS"] != false)
{
	//���������� ��� ������� ������
	$arSort = array(
		"TIMESTAMP_X" => "DESC",
	);

	$arFilter = array(
		"ACCOUNT_ID" => $arResult["ACCOUNT"]["ID"],
		"METER_ID" => $arResult["METERS"]["ID"],
	);

	//��� ��������� (��������� ������������� - Y, ������ - N, ���)
	if (strlen($arParams["MODIFIED_BY_OWNER"]) > 0)
	{
		$arFilter["MODIFIED_BY_OWNER"] = $arParams["MODIFIED_BY_OWNER"];
	}

	//���������� ��������� �� ����� ��������
	$arParams["COUNT_METERS_HISTORY"] = intval($arParams["COUNT_METERS_HISTORY"]) > 0 ? intval($arParams["COUNT_METERS_HISTORY"]) : 6;

	//���� ������� ��������� ���������
	$arSelect = Array(
		"VALUE1",
		"VALUE2",
		"VALUE3",
		// "TIMESTAMP_X",
		"INPUT_DATE",
	);

	//������������ ���������
	$arResult["NAV"] = new \Bitrix\Main\UI\PageNavigation("meter-history");
	$arResult["NAV"]->allowAllRecords(true)
	                ->setPageSize($arParams["COUNT_METERS_HISTORY"])
	                ->initFromUri();

	$rsValueIDs = \Citrus\Tszh\MetersValuesTable::getList(
		array(
			"filter" => $arFilter,
			"select" => array("MID"),
			"group" => array("INPUT_DATE"),
			"runtime" => array(
				new \Bitrix\Main\Entity\ExpressionField("MID", "MAX(ID)"),
				new \Bitrix\Main\Entity\ExpressionField("INPUT_DATE", "DATE(TIMESTAMP_X)"),
			),
		)
	);

	$arValueIDs = $rsValueIDs->fetchAll();

	foreach ($arValueIDs as $value)
	{
		$arFilter['@ID'][] = $value['MID'];
	}

	//������ ���������
	$rsValues = \Citrus\Tszh\MetersValuesTable::getList(
		array(
			"order" => array("INPUT_DATE" => "DESC"),
			"filter" => $arFilter,
			"select" => $arSelect,
			"group" => $arSelect,
			"offset" => $arResult["NAV"]->getOffset(),
			"limit" => $arResult["NAV"]->getLimit(),
			"count_total" => true,
			"runtime" => array(
				new \Bitrix\Main\Entity\ExpressionField("INPUT_DATE", "DATE(TIMESTAMP_X)"),
			),
		)
	);

	$arResult["NAV"]->setRecordCount($rsValues->getCount());
	$arResult["FLAG_NAV"] = $rsValues->getCount() > $arParams["COUNT_METERS_HISTORY"];

	$arResult['ROWS'] = $arNames = Array();

	$arResult["MAX_VALUES_COUNT"] = 1;
	if ($arResult["METERS"]["VALUES_COUNT"] > $arResult["MAX_VALUES_COUNT"])
	{
		$arResult["MAX_VALUES_COUNT"] = $arResult["METERS"]["VALUES_COUNT"];
	}

	while ($arValue = $rsValues->fetch())
	{
		/*$date = $arValue["TIMESTAMP_X"];

		if (CModule::IncludeModule('iblock'))
		{
			$timeStamp = MakeTimeStamp($date);
			$date = ToLower(CIBlockFormatProperties::DateFormat($DB->DateFormatToPHP('Y.m.d'), $timeStamp));
		}*/

		$date = $arValue['INPUT_DATE']->format('Y.m.d');

		if (!(array_key_exists($date, $arResult['ROWS'])))
		{
			$arResult['ROWS'][$date] = $arValue;

			$name = empty($arValue["SERVICE_NAME"]) ? $arValue["NAME"] : $arValue["SERVICE_NAME"];

			if (!empty($arValue["SERVICE_NAME"]))
			{
				if (array_key_exists($name, $arNames) && $arNames[$name] != $arValue["METER_ID"])
				{
					$preMeterID = $arNames[$name];
					$arPrevMeter = $arResult["METERS"][$preMeterID];
					$arResult["METERS"][$preMeterID]["NAME"] = $arPrevMeter["NAME"] . " (" . $arPrevMeter["METER_NAME"] . ")";
					$name .= " (" . $arValue['NAME'] . ")";
				}
				else
				{
					$arNames[$name] = $arValue['METER_ID'];
				}
			}
		}
	}

	if (count($arResult["ROWS"]) <= 0)
	{
		$arResult["METERS"] = false;
	}

}

global $APPLICATION;
// ������ �� �������� ����� ���������
$arResult["BACK_URL"] = $APPLICATION->GetCurPage();

$this->IncludeComponentTemplate();
?>