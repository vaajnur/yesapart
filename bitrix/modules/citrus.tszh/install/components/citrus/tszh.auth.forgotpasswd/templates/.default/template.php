<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/** @var CBitrixComponent $component ������� ��������� ��������� */
/** @var CBitrixComponentTemplate $this ������� ������ (������, ����������� ������) */
/** @var array $arResult ������ ����������� ������ ���������� */
/** @var array $arParams ������ �������� ���������� ����������, ����� �������������� ��� ����� �������� ���������� ��� ������ ������� (��������, ����������� ��������� ����������� ��� ������). */
/** @var string $templateFile ���� � ������� ������������ ����� �����, �������� /bitrix/components/bitrix/iblock.list/templates/.default/template.php) */
/** @var string $templateName ��� ������� ���������� (��������: .d�fault) */
/** @var string $templateFolder ���� � ����� � �������� �� DOCUMENT_ROOT (�������� /bitrix/components/bitrix/iblock.list/templates/.default) */
/** @var array $templateData ������ ��� ������, �������� ��������, ����� ������� ����� �������� ������ �� template.php � ���� component_epilog.php, ������ ��� ������ �������� � ���, �.�. ���� component_epilog.php ����������� �� ������ ���� */
/** @var string $parentTemplateFolder ����� ������������� �������. ��� ����������� �������������� ����������� ��� �������� (��������) ������ ������������ ��� ����������. �� ����� ��������� ��� ������������ ������� ���� ������������ ����� ������� */
/** @var string $componentPath ���� � ����� � ����������� �� DOCUMENT_ROOT (����. /bitrix/components/bitrix/iblock.list) */

ShowMessage($arParams["~AUTH_RESULT"]);

?>
<p><?=GetMessage("AUTH_FORGOT_PASSWORD_1")?></p>
<h3 class="alt"><?=GetMessage("AUTH_GET_CHECK_STRING")?></h3>
<form method="post" action="<?=$arResult["AUTH_URL"]?>" id="auth-form" class="forgot-password">
	<?
	if (strlen($arResult["BACKURL"]) > 0)
	{
		echo '	<input type="hidden" name="backurl" value="' . $arResult["BACKURL"] . '" />';
	}
	?>
	<input type="hidden" name="AUTH_FORM" value="Y"/>
	<input type="hidden" name="TYPE" value="SEND_PWD"/>
	<fieldset>
		<label for="user-login"><?=GetMessage("AUTH_LOGIN")?></label>
		<input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" id="user-login" class="styled"/>
	</fieldset>
	<fieldset>
		<label> </label>
		<em style="display: inline-block; margin-bottom: 4px;"><?=GetMessage("AUTH_OR")?></em>
	</fieldset>
	<fieldset>
		<label for="user-email">E-Mail:</label>
		<input type="text" name="USER_EMAIL" maxlength="255" id="user-email" class="styled"/>
	</fieldset>

	<? if ($arResult["USE_CAPTCHA"]): ?>
		<fieldset>
			<label for="captcha_sid">&nbsp;</label>
			<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>"/>
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" id="CAPTCHA"/>
		</fieldset>
		<fieldset>
			<label for="captcha_word"><?=GetMessage("AUTH_CAPTCHA")?></label>
			<input type="text" name="captcha_word" maxlength="50" id="captcha_word" class="styled"/>
		</fieldset>
	<? endif ?>

	<fieldset>
		<label> </label>
		<?
		if (function_exists("TemplateShowButton"))
		{
			TemplateShowButton(array(
				"type" => "submit",
				"title" => GetMessage("AUTH_SEND"),
				"attr" => array(
					"name" => "send_account_info",
					"value" => GetMessage("AUTH_SEND"),
				),
			));
		}
		else
		{
			?>
			<button class="submit" type="submit" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>"><?=GetMessage("AUTH_SEND")?></button>
			<?
		}
		?>
	</fieldset>

	<p class="links"><a href="<?=$arResult["AUTH_AUTH_URL"]?>"><?=GetMessage("AUTH_AUTH")?></a></p>
</form>
<script type="text/javascript">
    document.getElementById('auth-form').USER_LOGIN.focus();
</script>
