$(document).ready(function () {

    function AuthFormAction(formAuth, g_recaptcha) {
        $.post(tszh.siteDir + 'ajax/auth.php', $(formAuth).serialize(),
            function (response) {
                response = response.replace(/<\/?[^>]+>/g, '');
                if (response == 'Auth|Y' || response.length > 200) {
                    var tourl = $(formAuth).find('input[name="tourl"]').val();
                    window.location.href = tourl !== undefined ? tourl : window.location.href;
                }
                else {
                    response = response.split('|');
                    switch (response[0]) {
                        case 'Redirect':
                            window.location.href = response[1] !== undefined ? response[1] : window.location.href;
                            break;
                        default:
                            if (response[3] && response[4].length > 0) {

                                //head-auth
                                $('#captcha_img').attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + response[4]);
                                $('#captcha_sid').attr('value', response[4]);
                                if (!g_recaptcha) {
                                    $('#block_captcha_img').show();
                                    $('#block_captcha_word').show();
                                }

                                //footer-auth
                                $('#captcha_img_foot').attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + response[4]);
                                $('#captcha_sid_foot').attr('value', response[4]);
                                if (!g_recaptcha) {
                                    $('#captcha_word_foot').show();
                                    $('#captcha_img_foot').show();
                                }
                            }
                            $('.block-auth-form__error').html(response[1]).show();
                            break;
                    }
                }
                var submitButton = $(formAuth).find('input[type=submit]');
                $(submitButton).toggleClass('tszh-auth__lg-progress');
            }, 'html');
    };

    $('.block-auth-form').each(
        function () {
            $(this).submit(
                function () {
                    var submitButton = $(this).find('input[type=submit]');
                    $(submitButton).toggleClass('tszh-auth__lg-progress');

                    var g_recaptcha = $(this).find('div.g-recaptcha-3');
                    var formAuth = $(this)[0];
                    if (g_recaptcha[0] !== undefined) {
                        var key_id = g_recaptcha[0].getAttribute("data-sitekey");
                        $('#captcha_word').val(key_id.substr(0, 5));
                        grecaptcha.execute(key_id, {action: 'contactForm'}).then(function (token) {
                            RecaptchafreeSubmitFormForAjax(formAuth, token);
                            AuthFormAction(formAuth, true);
                            return false;
                        });
                    }
                    else {
                        AuthFormAction(formAuth, false);
                        return false;
                    }
                    return false;
                });
        });

    function PasswordRecoveryFormAction(formPass) {
        $.post(tszh.siteDir + 'ajax/auth.php', $(formPass).serialize(),
            function (response) {
                response = response.replace(/<\/?[^>]+>/g, '');
                response = response.split('|');
                if (response[0] == 'ERROR') {
                    $('.block-password-recovery-form__error').html(response[1]).show();
                    $('.block-password-recovery-form__account_info_sent').hide();
                    $.getJSON($('#temp').val() + '/reload_capcha.php', function (data) {
                        $('.block-password-recovery-form #captcha_img').attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + data);
                        $('.block-password-recovery-form #captcha_sid').val(data);
                        $('.block-password-recovery-form #captcha_word').val('');
                    });
                }
                if (response[0] == 'OK') {
                    $('.block-password-recovery-form__account_info_sent').html(response[1]).show();
                    $('.block-password-recovery-form__error').hide();
                    $('#window__inputs-recovery').hide();
                    $('#window__sendbutton-recovery').prop('disabled', true);
                    $('#window__sendbutton-recovery').hide();
                    $('#window__text_or-recovery').hide();
                    $('#window__enter-recovery').css("text-align", "center");
                }
                var submitButton = $(formPass).find('input[type=submit]');
                $(submitButton).toggleClass('tszh-auth__lg-progress');
            }, 'html');
    };

    $('.block-password-recovery-form').each(
        function () {
            $(this).submit(
                function () {

                    var confirm = $(this).find('[name="confirm"]');
                    var input = $('#confirm_window-form');
                    if(typeof confirm != 'undefined'&& input.length) {
                        if(confirm.is(":checked"))
                        {
                            $('.window__input-opd-err').hide();
                        }
                        else
                        {
                            $('.window__input-opd-err').show();
                            return false;
                        }
					}
					
                    var submitButton = $(this).find('input[type=submit]');
                    $(submitButton).toggleClass('tszh-auth__lg-progress');

                    var g_recaptcha = $(this).find('div.g-recaptcha-3');
                    var formPass = $(this)[0];
                    if (g_recaptcha[0] !== undefined) {
                        var key_id = g_recaptcha[0].getAttribute("data-sitekey");
                        $('.block-password-recovery-form #captcha_word').val(key_id.substr(0, 5));
                        grecaptcha.execute(key_id, {action: 'contactForm'}).then(function (token) {
                            RecaptchafreeSubmitFormForAjax(formPass, token);
                            PasswordRecoveryFormAction(formPass);
                            return false;
                        });
                    }
                    else {
                        PasswordRecoveryFormAction(formPass);
                        return false;
                    }
                    return false;
                });
        });
});