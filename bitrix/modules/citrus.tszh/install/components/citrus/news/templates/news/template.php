<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?><?$APPLICATION->IncludeComponent(
    "bitrix:news",
    "news",
    $arParams,
    $this->__component
);?>