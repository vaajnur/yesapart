<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

IncludeTemplateLangFile(__FILE__);

if (array_key_exists("ERRORS", $arResult))
{
	echo ShowError($arResult["ERRORS"]);
}
if (array_key_exists("NOTE", $arResult))
{
	?>
	<div style="margin:0 auto; width:75%; text-align: center">
		<? echo ShowNote($arResult["NOTE"]); ?>
	</div>
<? } ?>
<div>
	<!--style="margin:0 auto; width:75%"-->
	<?
	if (is_array($arResult["ACCOUNT"])):
		foreach ($arResult["ACCOUNT"] as $arRes)
			{
			?>
			<p><?=str_replace("#ACCOUNT#", $arRes["XML_ID"], GetMessage("TAC_YOUR_ACCOUNT"))?></p><?
			}
			?>
		<p><?=GetMessage("TAC_GO_TO")?><a href="<?=$arParams["PERSONAL_PATH"]?>"><?=GetMessage("TAC_PERSONAL_SECTION")?></a>.</p>
	<?
	else:
	if ($arResult['ACC'] == 0 && $_REQUEST['action'] != 'confirm')
	{
	?>
		<script type="text/javascript">
            $(document).ready(function () {
                $("#step0").css('display', 'none');
                $("#step2").css('display', 'block');
                $("#error").css('display', 'block');
                $("#stepDesc0").attr('class', '');
                $("legend").text('<?=GetMessage("TAC_ADD_DATA")?>');
                $("#stepDesc2").attr('class', '');
                $("#stepDesc1").attr('class', 'current');
                $("#progress").attr('aria-valuenow', '50');
                $(".ui-progressbar-value").css('width', '50%');
            });
		</script>
		<p style="color: red"><?=GetMessage("TAC_ERROR_TEXT")?></p>
		<form method="post" action="#">
			<table style="width: 100%">
				<tr>
					<td class="span">
						<span class="title"><?=GetMessage("TAC_ACCOUNTS_LOGIN")?>:</span>
					</td>
					<td>
						<input type="text" name="regcode" value="" class="code">
					</td>
				</tr>
				<tr>
					<td class="span">
						<span class="title"><?=GetMessage("TAC_ACCOUNTS_PASS")?>:</span>
					</td>
					<td>
						<input type="password" name="password" value="" class="code">
					</td>
				</tr>
			</table>
			<br><br>
			<input type="hidden" id="word" name="word" value="2" class="code">
			<input type="hidden" id="check" name="check" value="citrus:tszh.account.confirm_code" class="code">
			<p style="text-align: right">
				<a href="<?=SITE_DIR.$arParams["PERSONAL_PATH"]."confirm-account/"?>" id="step3Prev"
				   class="form-variable__button form-variable__saved link-theme-default">
					<?=GetMessage("TAC_BUTTON_PREV")?>
				</a>
				<button type="submit" id="step3Prev1"
				        class="form-variable__button form-variable__saved link-theme-default">
					<?=GetMessage("TAC_BUTTON_NEXT")?>
				</button>
			</p>
		</form>
	<?
	}
	else
	{
	if ($_REQUEST['action'] != 'confirm')
	{
	?>
		<p> <?=GetMessage("TAC_ACCOUNTS_LIST")?> </p>
		<form method="post" enctype="multipart/form-data" action="#center" id="regcodeform">
			<table class="step3data">
				<?
				foreach ($arResult['ACC'] as $arRes)
				{
					?>
					<tr>
						<td>
							<label>
								<input type="checkbox" class="checkbox" name="accounts[]" value="<?=$arRes['ID']?>">
								<span class="checkbox-custom"></span>
							</label>
						</td>
						<td>
							<?=$arRes["XML_ID"]?><br>


							<span class="acc_name"><?=$arRes["NAME"]?></span>

						</td>
						<td class="whide">
							<?=$arRes["ADDRESS_FULL"]?>
						</td>
					</tr>
					<?
				} ?>
			</table>
			<br><br><br>
			<input type="hidden" name="action" value="confirm"/>
			<?=bitrix_sessid_post()?>
			<input type="hidden" name="regcode" class="input" id="confirm-regcode" value="<?=$arResult['REG_CODE']?>"/>
			<input type="hidden" id="check" name="check" value="citrus:tszh.account.confirm_code" class="code"><br>
			<p style="text-align: right; padding-top: 15px;" id="buttons">
				<a href="<?=SITE_DIR.$arParams["PERSONAL_PATH"]?>" id="step3Prev"
				   class="form-variable__button form-variable__saved link-theme-default"> <?=GetMessage("TAC_BUTTON_CANCEL")?></a>

				<button class="form-variable__button form-variable__saved"
				        form="regcodeform" id="step3Prev1" type="submit"><?=GetMessage("TAC_BUTTON_NEXT")?></button>
			</p>
		</form>
		<?
		}
	} ?>
	<?
	endif;
	?>
	<? if ($arResult['ACC'] != 0 && $_REQUEST['action'] != 'confirm')
	{
		?>
		<script type="text/javascript">
            $(document).ready(function () {
                var inputs = $("input.checkbox");
                $("#step3Prev1").attr('disabled', !isSelectedAccounts());

                function isSelectedAccounts() {
                    return $("input.checkbox:checked").length > 0;
                }

                inputs.click(function () {
                    if (isSelectedAccounts())
                    {
                        $("#step3Prev1").attr('disabled', !isSelectedAccounts()).addClass('link-theme-default');
                    }
                    else
                    {
                        $("#step3Prev1").attr('disabled', !isSelectedAccounts()).removeClass('link-theme-default');
                    }
                });
            });
		</script>
	<? } ?>
</div>
