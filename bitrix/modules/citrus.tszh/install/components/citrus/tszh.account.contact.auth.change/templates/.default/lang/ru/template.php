<?
$MESS['TACC_CONTACT_INFO'] = 'Контактная информация';
$MESS['TACC_TELEPHONE'] = 'Телефон';
$MESS['TACC_CHANGE_DATA'] = 'Изменить данные';
$MESS['TACC_SAVE_DATA'] = 'Сохранить данные';
$MESS['TACC_DATA_FOR_AUTH'] = 'Данные для авторизации на сайте';
$MESS['TACC_LOGIN_IN_SITE'] = 'Логин на сайте';
$MESS['TACC_CHANGE_PAS'] = 'Изменить пароль';
$MESS['TACC_CHANGING_PAS'] = 'Изменение пароля';
$MESS['TACC_SAVE_PAS'] = 'Сохранить пароль';
$MESS['TACC_CHANGE_CONTACT_INFO'] = 'Изменение контактной информации';
$MESS['TACC_INPUT_OLD_PAS'] = 'Введите старый пароль';
$MESS['TACC_INPUT_NEW_PAS'] = 'Введите новый пароль';
$MESS['TACC_REPEAT_NEW_PAS'] = 'Повторите новый пароль';
$MESS['TACC_REQUIREMENTS'] = 'Новый пароль должен быть не менее #minLenght# символов длиной';
$MESS['TACC_RECOMMENDATIONS'] = 'Рекомендуется использовать латинские буквы и цифры';

$MESS["PROFILE_CONFIRM_EMAIL_NOTIFICATION_SENT"] = "На адрес #EMAIL# отправлен запрос подтверждения Вашего e-mail'а.";
$MESS["PROFILE_EMAIL_CONFIRMED"] = "e-mail подтверждён";
$MESS["PROFILE_EMAIL_NONCONFIRMED"] = "требуется подтверждение e-mail'а";
?>