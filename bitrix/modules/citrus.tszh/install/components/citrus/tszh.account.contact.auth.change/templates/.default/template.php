<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? if ($arResult["CONFIRM_EMAIL_NOTIFICATION_SENT"]) : ?>
	<div class="mess">
		<? ShowNote(GetMessage("PROFILE_CONFIRM_EMAIL_NOTIFICATION_SENT", array("#EMAIL#" => $arResult["arUser"]["EMAIL"]))); ?>
	</div>
<? endif ?>

<div class="div-flex">
    <?if(isset($arResult['CONTACT'])):?>
    <div class="form-variable">
        <form method="POST">
            <input type="hidden" name="typeform" value="contact" />
            <div class="form-variable__header form-variable__saved"><?=GetMessage('TACC_CONTACT_INFO')?></div>
            <div class="form-variable__header form-variable__new"><?=GetMessage('TACC_CHANGE_CONTACT_INFO')?></div>
            <div class="form-variable__data">
                <div>
                    <div class="form-variable__saved">
	                    <p><span class="bold"><?=GetMessage('TACC_TELEPHONE')?>:</span> <?=$arResult['CONTACT']['TELEPHONE']?></p>
	                    <p><span class="bold">Email:</span> <?=$arResult['CONTACT']['EMAIL']?>
		                     <? if ($arResult['CONTACT']['EMAIL']): 
							
							if(in_array(trim($arResult['CONTACT']['EMAIL']),$arResult["NOT_CONFIRM_EMAIL_USER"]))
								$arResult["arUser"]["UF_TSZH_EML_CNFRM_RQ"] = 1;
							
							?>    <span
				                    class="icon <?=($arResult["arUser"]["UF_TSZH_EML_CNFRM_RQ"] ? 'nonconfirmed' : 'confirmed')?>"
				                    title="<?=(getMessage($arResult["arUser"]["UF_TSZH_EML_CNFRM_RQ"] ? "PROFILE_EMAIL_NONCONFIRMED" : "PROFILE_EMAIL_CONFIRMED"))?>"></span>
		                    <? endif ?>
	                    </p>

                    </div>
                    <div class="form-variable__new">
	                    <p><span class="bold"><?=GetMessage('TACC_TELEPHONE')?>:</span><input name="telephone"  value="<?=$arResult['CONTACT']['TELEPHONE']?>"/></p>
	                    <p><span class="bold">Email:</span><input name="email" value="<?=$arResult['CONTACT']['EMAIL']?>"/>
		                    <? if ($arResult['CONTACT']['EMAIL']): ?>    <span
				                    class="icon <?=($arResult["arUser"]["UF_TSZH_EML_CNFRM_RQ"] ? 'nonconfirmed' : 'confirmed')?>"
				                    title="<?=(getMessage($arResult["arUser"]["UF_TSZH_EML_CNFRM_RQ"] ? "PROFILE_EMAIL_NONCONFIRMED" : "PROFILE_EMAIL_CONFIRMED"))?>"></span>
		                    <? endif ?>
	                    </p>
                    </div>
                </div>
            </div>
            <input type="button" class="form-variable__button form-variable__saved" value="<?=GetMessage('TACC_CHANGE_DATA')?>"/>
            <input type="submit" class="form-variable__button form-variable__new" value="<?=GetMessage('TACC_SAVE_DATA')?>"/>
        </form>
    </div>
    <?endif;?>
    <?if(isset($arResult['LOGIN'])):?>
    <div class="form-variable">
        <form method="POST">
            <input type="hidden" name="typeform" value="password" />
            <div class="form-variable__header form-variable__saved"><?=GetMessage('TACC_DATA_FOR_AUTH')?></div>
            <div class="form-variable__header form-variable__new"><?=GetMessage('TACC_CHANGING_PAS')?></div>
            <div class="form-variable__data">
                <div>
                    <div class="form-variable__saved">
                        <p><span class="bold"><?=GetMessage('TACC_LOGIN_IN_SITE')?>:</span> <?=$arResult['LOGIN']?></p>
                    </div>
                    <div class="form-variable__new">
                        <p><span class="bold"><?=GetMessage('TACC_INPUT_OLD_PAS')?>:</span><input type="password" name="oldpassword" /></p>
                        <p><span class="bold"><?=GetMessage('TACC_INPUT_NEW_PAS')?>:</span><input type="password" name="newpassword1" /></p>
                        <p><span class="bold"><?=GetMessage('TACC_REPEAT_NEW_PAS')?>:</span><input type="password" name="newpassword2" /></p>
                    </div>
                </div>
            </div>
            <input type="button" class="form-variable__button form-variable__saved link-theme-default" value="<?=GetMessage('TACC_CHANGE_PAS')?>"/>
            <input type="submit" class="form-variable__button form-variable__new link-theme-default" value="<?=GetMessage('TACC_SAVE_PAS')?>"/>
        </form>
        <div class="form-variable__tips form-variable__new">
            <p><?=GetMessage('TACC_REQUIREMENTS', array('#minLenght#'=>'6'))?>.</p>
            <p><?=GetMessage('TACC_RECOMMENDATIONS')?></p>
        </div>
    </div>
    <?endif;?>
</div>