<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

$arResult["CONFIRM_EMAIL_NOTIFICATION_SENT"] = false;
$arChangedEmail = $USER->getParam("citrus.tszh.user.changedEmail");
if (is_array($arChangedEmail) && !empty($arChangedEmail))
{
	$arResult["CONFIRM_EMAIL_NOTIFICATION_SENT"] = $arChangedEmail["IS_NOTIFICATION_SENT"];
	$USER->setParam("citrus.tszh.user.changedEmail", false);
}


if (!CModule::IncludeModule("citrus.tszh"))
	return false;

global $USER;

// несколько лицевых счетов
$arXmlIdToAccount = array();
$dbAccount = CTszhAccount::GetList(
	array('XML_ID'=>"ASC"),
	array('USER_ID'=>$USER->GetID()),
	false,
	false,
	array("XML_ID")
);
while($arAccount = $dbAccount -> fetch()){
	$arXmlIdToAccount[] = $arAccount['XML_ID'];
}

//  несколько доменных имен сайта
$rsSites = CSite::GetList($by="sort", $order="desc", Array("ID" => SITE_ID));
if ($arSite = $rsSites->Fetch())
{
	if(!empty($arSite["DOMAINS"])) {
		$arSite["DOMAINS"] = explode(PHP_EOL,$arSite["DOMAINS"]);
	}
	else
	{
		$arSite["DOMAINS"] = array();
		$arSite["DOMAINS"][] = $arSite["SERVER_NAME"];
	}
}

if(!isset($arSite["DOMAINS"]) || !is_array($arSite["DOMAINS"]))
	$arSite["DOMAINS"] = array();

// текущий домен
$arSite["DOMAINS"][] = $_SERVER['HTTP_HOST'];

// массив возможных "левых" емайл адресов
$arEmail = array();
foreach($arXmlIdToAccount as $Account) {
	foreach($arSite["DOMAINS"] as $DOMAINS) {
		$arEmail[] = trim($Account).'@'.trim($DOMAINS);
	}
}

$arResult["NOT_CONFIRM_EMAIL_USER"] = $arEmail;
