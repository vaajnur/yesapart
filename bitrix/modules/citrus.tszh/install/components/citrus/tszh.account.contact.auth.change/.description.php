<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SECTION_NAME"),
	"DESCRIPTION" => GetMessage("SECTION_NAME"),
	"ICON" => "/images/menu_ext.gif",
	"PATH" => array(
		"ID" => "citrus.tszh",
		"NAME" => GetMessage("COMPONENT_ROOT_SECTION"),
	),
);
?>