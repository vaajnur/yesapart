<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2017 otr@rarus.ru
 * @date 15.09.2017 9:43
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

$arComponentDescription = array(
	'NAME' => Loc::getMessage('TSZH_MONETA_USAGE_TABLE_COMPONENT'),
	'DESCRIPTION' => Loc::getMessage('TSZH_MONETA_USAGE_TABLE_COMPONENT_DESCRIPTION'),
	'CACHE_PATH' => 'Y',
	'SORT' => 100,
	'PATH' => array(
		'ID' => 'citrus.tszh',
		'NAME' => Loc::getMessage('SITE_TSZH_PATH_NAME'),
		'CHILD' => array(
			'ID' => 'service',
			'NAME' => Loc::getMessage('SITE_TSZH_PATH_NAME_CHILD_SERVICE'),
		),
	),
);