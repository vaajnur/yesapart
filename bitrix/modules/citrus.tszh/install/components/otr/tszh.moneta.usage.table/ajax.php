<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2017 otr@rarus.ru
 * @date 24.10.2017 16:49
 */
define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getPostList()->toArray();

$ID = (int)$request['ID'];

$APPLICATION->IncludeComponent(
	"otr:tszh.moneta.usage.table",
	"",
	Array(
		"DISABLE_OFFER" => "N",
		"ENTITY_ID" => $ID,
		"ENTITY_TYPE" => "tszh",
		"HAS_ERRORS" => "N"
	)
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");