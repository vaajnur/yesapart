<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2017 otr@rarus.ru
 * @date 15.09.2017 9:46
 */

$MESS['TSZH_MODULE_NOT_INSTALLED'] = 'Модуль 1С: Сайт ЖКХ не установлен';
$MESS['MONETA_USAGE_TABLE_PARAMS_ENTITY_TYPE'] = 'Тип сущности (объект упраления, дом)';
$MESS['MONETA_USAGE_TABLE_PARAMS_ENTITY_TYPE_VALUES_TSZH'] = 'объект управления';
$MESS['MONETA_USAGE_TABLE_PARAMS_ENTITY_TYPE_VALUES_HOUSE'] = 'дом';
$MESS['MONETA_USAGE_TABLE_PARAMS_ENTITY_ID'] = 'ID сущности';
$MESS['MONETA_USAGE_TABLE_PARAMS_HAS_ERRORS'] = 'Присутствуют ошибки';
$MESS['MONETA_USAGE_TABLE_PARAMS_DISABLE_OFFER'] = 'Не подключать к схеме по оферте';