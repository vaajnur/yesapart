<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

global $USER, $APPLICATION;

$query = ltrim($_POST["q"]);
CUtil::decodeURIComponent($query);

if ($_REQUEST["ajax_call"] === "y")
{
	if (!empty($query))
	{
		/*�������� - ����� ��������� ����� ����� ���� */
		if ($_REQUEST["INPUT_ID"] == "HOUSE_SEARCH_INPUT")
		{
			$filter = array(
				"LOGIC" => "OR",
				array("STREET" => "%" . $query . "%",),
				array("HOUSE" => "%" . $query . "%",)
			);
			$rsHouse = \Citrus\Tszh\HouseTable::getList(array(
				'filter' => $filter,
			));
			while ($arHouse = $rsHouse->Fetch())
			{
				$arResult["SEARCH"][] =
					array(
						"HOUSE_ID" => $arHouse["ID"],
						"HOUSE_ADDRESS" => $arHouse["STREET"] . " " . $arHouse["HOUSE"],
					);
			}
		}
		/*�������� - ����� ��������� ����� ����� ���� */
		elseif ($_REQUEST["INPUT_ID"] == "FLAT_SEARCH_INPUT")
		{
			$rsAccount = CTszhAccount::GetList(
				array(),
				array(
					"HOUSE_ID" => intval($_REQUEST['HOUSE_ID_FOR_FLAT'])
				)
			);
			while ($arAccount = $rsAccount->Fetch())
			{
				if ((stristr($arAccount["FLAT_ABBR"], $query) !== false) || (stristr($arAccount["FLAT"], $query) !== false))

					$arResult["SEARCH"][] =
						array(
							"ACCOUNT_ID" => $arAccount["ID"],
							"FLAT" => ($arAccount["FLAT_ABBR"] ? $arAccount["FLAT_ABBR"] : GetMessage("TSZH_HOUSE_FLAT_ABBR"). " ". $arAccount["FLAT"]) ,
						);
			}
		}

		elseif (!isset($_REQUEST["INPUT_ID"]) || $_REQUEST["INPUT_ID"] == $arParams["INPUT_ID"])
		{
			/*����� �� ������� ��� ��� ��� , ���� �������*/
			$filter = array(
				"~NAME" => "%" . $query . "%",
			);
			$rsAccount = CTszhAccount::GetList(array("CURRENT" => "DESC"), $filter);
			while ($arAccount = $rsAccount->Fetch())
			{
				if ($_REQUEST["INPUT_ID"] == "title-search-input-tszhticket")
				{
					$arResult['SEARCH'][] =
						array(
							"USER_NAME" => $arAccount["NAME"] . " - " . $arAccount["STREET"] . " " . $arAccount["HOUSE"] . ", " . ($arAccount["FLAT_ABBR"] ? $arAccount["FLAT_ABBR"] : GetMessage("TSZH_HOUSE_FLAT_ABBR") . $arAccount["FLAT"]),
							"ACCOUNT_ID" => $arAccount["ID"],
						);
				}
			};
		}
	}
	elseif (isset($_REQUEST["SELECT_ID"]))
	{
		$rsAccount = CTszhAccount::GetList(array(), array("ID" => intval($_REQUEST["SELECT_ID"])));
		if ($arAccount = $rsAccount->Fetch())
		{
			$rsUser = \CUser::GetByID(intval($arAccount["USER_ID"]));
			if ($arUser = $rsUser->Fetch())
			{
				$phone = $arUser["PERSONAL_PHONE"] ? $arUser["PERSONAL_PHONE"] : $arUser["PERSONAL_MOBILE"];
			}

			$arResult['SELECT_ID_SEARCH'] =
				array(
					"ADDRESS_FULL" => $arAccount["ADDRESS_FULL"],
					"PHONE_USER" => $phone,
					"USER_NAME" => $arAccount["NAME"],
					"ACCOUNT" => $arAccount["XML_ID"],
					"ACCOUNT_ID" => $arAccount["ID"],
				);
		};
	}
}

if ($_REQUEST["ajax_call"] === "y" && ((!isset($_REQUEST["INPUT_ID"]) || $_REQUEST["INPUT_ID"] == $arParams["INPUT_ID"]) || (isset($_REQUEST["SELECT_ID"]))))
{
	$APPLICATION->RestartBuffer();
/* (isset($_REQUEST["SELECT_ID"]))) - ��������� �������� �� ���*/
	if (!empty($query) || (isset($_REQUEST["SELECT_ID"])))
	{
		$this->IncludeComponentTemplate('ajax');
	}
	CMain::FinalActions();
	die();
}
else
{
	$APPLICATION->AddHeadScript($this->GetPath() . '/script.js');
	CUtil::InitJSCore(array('ajax'));
	$this->IncludeComponentTemplate();
}
?>
