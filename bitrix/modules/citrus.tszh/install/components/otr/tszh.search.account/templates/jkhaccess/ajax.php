<? if ($_REQUEST["INPUT_ID"] == "HOUSE_SEARCH_INPUT") : ?>
	<table class="house-search-result">
		<? foreach ($arResult["SEARCH"] as $i => $arItem): ?>
			<tr>
				<td class="house-search-item">
					<a data_item="<?=$arItem["HOUSE_ID"]?>"><?=$arItem["HOUSE_ADDRESS"]?></a></td>
			</tr>
		<? endforeach; ?>
		<? if (!count($arResult["SEARCH"])): ?>
			<tr>
				<td class="house-search-item">
					<a> <?=GetMessage("NO_RESULT")?></a></td>
			</tr>
		<? endif ?>
	</table>
<? elseif ($_REQUEST["INPUT_ID"] == "FLAT_SEARCH_INPUT"): ?>
	<table class="flat-search-result">
		<? foreach ($arResult["SEARCH"] as $i => $arItem): ?>
			<tr>
				<td class="flat-search-item">
					<a data_item="<?=$arItem["ACCOUNT_ID"]?>"><?=$arItem["FLAT"]?></a></td>
			</tr>
		<? endforeach; ?>
		<? if (!count($arResult["SEARCH"])): ?>
			<tr>
				<td class="flat-search-item">
					<a> <?=GetMessage("NO_RESULT")?></a></td>
			</tr>
		<? endif ?>
	</table>
<? endif; ?>
