<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
$this->setFrameMode(true);
$INPUT_ID = trim($arParams["INPUT_ID"]);
$CONTAINER_ID = trim($arParams["CONTAINER_ID"]);
if (!$INPUT_ID || !$CONTAINER_ID)
{
	return;
}
?>

<? if ($arParams["SHOW_INPUT"] !== "N"): ?>
	<div id="<? echo $CONTAINER_ID ?>">
		<input id="<? echo $INPUT_ID ?>" name="<? echo $INPUT_ID ?>" autocomplete="off" <? if ($INPUT_ID == "FLAT_SEARCH_INPUT")
			echo 'disabled' ?>>
	</div>
<? endif ?>

<script>
    BX.ready(function () {
        new JCTitleSearch({
            'AJAX_PAGE': '<?echo CUtil::JSEscape(POST_FORM_ACTION_URI)?>',
            'CONTAINER_ID': '<?echo $CONTAINER_ID?>',
            'INPUT_ID': '<?echo $INPUT_ID?>',
            'MIN_QUERY_LEN': <?=($INPUT_ID == "HOUSE_SEARCH_INPUT" ? 2 : 1)?>,
        });
    });
</script>

<? if ($INPUT_ID == "HOUSE_SEARCH_INPUT"): ?>
	<script>
        $(document).on('click', '.house-search-item a', function () {
            if (this.getAttribute('data_item')) {
                $('#HOUSE_SEARCH_INPUT')[0].setAttribute("data_item", this.getAttribute('data_item'));
                $('#HOUSE_SEARCH_INPUT')[0].value = this.text;
                $('table.house-search-result').closest('.title-search-result')[0].style.display = 'none';
                validator.element($('#HOUSE_SEARCH_INPUT'));

                $('#FLAT_SEARCH_INPUT').attr('disabled', false);
                $('#FLAT_SEARCH_INPUT')[0].setAttribute("data_item", "");
                $('#FLAT_SEARCH_INPUT')[0].value = "";
                if (typeof($('table.flat-search-result')[0]) != 'undefined') $('table.flat-search-result').closest('.title-search-result')[0].innerHTML = "";
	           }
        });
	</script>
<? endif ?>

<? if ($INPUT_ID == "FLAT_SEARCH_INPUT"): ?>
	<script>
        $(document).on('click', '.flat-search-item a', function () {
            if (this.getAttribute('data_item')) {
                $('#FLAT_SEARCH_INPUT')[0].value = this.text;
                $('#FLAT_SEARCH_INPUT').addClass('select');
                $('#FLAT_SEARCH_INPUT')[0].setAttribute("data_item", this.getAttribute('data_item'));
                $('[name="ACCOUNT_ID"]')[0].setAttribute("value", this.getAttribute('data_item'));
                validator.element($('#FLAT_SEARCH_INPUT'));
                BX.ajax.post(templateFolder + "/ajax.php", {
                        account_id: this.getAttribute('data_item'),
                    },
                    function (data) {
                        res = JSON.parse(data);
                        $('[name="customer_phone"]')[0].value = res["PHONE"] ? res["PHONE"] : "";
                        $('[name="FIO"]')[0].value = (res["FIO"] ? res["FIO"] : "");
                        $('[name="ACCOUNT_ID"]')[0].value = res["ACCOUNT_ID"] ? res["ACCOUNT_ID"] : "";
                    }
                );

            //    validator.element($('#FLAT_SEARCH_INPUT'));
              //  $('#FLAT_SEARCH_INPUT').change();
            }
        });
	</script>
<? endif ?>
