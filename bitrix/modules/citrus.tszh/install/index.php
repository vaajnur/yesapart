<?php
/**
 * ������ ���ƻ
 * ���������/�������� ������
 * @package tszh
 */

global $DOCUMENT_ROOT, $MESS;

use Bitrix\Main\Application;
use Bitrix\Main\EventManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Composite\Helper;
use Citrus\Tszh\MonetaAdditionalInfoTable;
use Citrus\Tszh\Subscribe\CTszhSubscribeTable;

IncludeModuleLangFile(__FILE__);

include_once __DIR__ . '/tszhinstallparams.php';

if (class_exists("citrus_tszh")) {
	return;
}

/**
 * Class citrus_tszh
 */
Class citrus_tszh extends CModule
{
	use TszhInstallParams;

	public $MODULE_ID = "citrus.tszh";
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;
	public $MODULE_CSS;
	public $MODULE_GROUP_RIGHTS = "Y";

	public $docRoot = '';

	public $errors = array();

	public $userGroups = array(
		'TSZH_MEMBERS',
		'CITRUS_TSZH_1C_EXCHANGE',
	);

	// ����������� ������ �������� ������, ����������� ��� ������ ������
	const MIN_VERSION = '14.0.0';

	function citrus_tszh()
	{
		$arModuleVersion = array();

		$this->docRoot = Application::getDocumentRoot();

		include __DIR__ . "/version.php";
		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else
		{
			$this->MODULE_VERSION = "1.0";
			$this->MODULE_VERSION_DATE = "19.08.2009";
		}

		$this->MODULE_NAME = Loc::getMessage("CITRUS_TSZH_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("CITRUS_TSZH_MODULE_DESCRIPTION");

		$this->PARTNER_NAME = GetMessage("CITRUS_TSZH_MODULE_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("CITRUS_TSZH_MODULE_PARTNER_URI");
	}

	// ��������� ������ (���������� ���������)

	/**
	 * @return bool|mixed|void
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	function DoInstall()
	{
		global $APPLICATION;

		$APPLICATION->ResetException();

		if ($this->isBXVersionRequiredError())
		{
			$APPLICATION->ThrowException(Loc::getMessage('CITRUS_TSZH_ERROR_REQUIRED_VERSION', array("#VER#" => self::MIN_VERSION)));

			return false;
		}

		$this->setInstallParam('checkEdition', true);
		$checkEdition = $this->getInstallParam('checkEdition');
		$checkEdition = $checkEdition ? $this->checkEdition() : null;

		if (empty($this->errors) && is_callable($checkEdition) && !$checkEdition('start'))
		{
			$this->errors = array(Loc::getMessage("TSZH_ERROR_EDITION_NOT_FOUND"));

			return false;
		}

		$this->InstallDB();
		$this->InstallEvents();
		$this->InstallFiles();

		if (!empty($this->errors))
		{
			/*
			$this->UnInstallDB();
			$this->UnInstallEvents();
			$this->UnInstallFiles();
			ModuleManager::unRegisterModule($this->MODULE_ID);
			*/

			$APPLICATION->ThrowException(implode("<br>", $this->errors));

			return false;
		}

		if (!$this->isModuleAutoInstall())
		{
			$APPLICATION->IncludeAdminFile(Loc::getMessage("TSZH_INST_TITLE"), __DIR__ . "/step1.php");
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	function InstallDB()
	{
		global $DB, $DBType;

		ModuleManager::registerModule($this->MODULE_ID);

		if (!$DB->Query("SELECT 'x' FROM b_tszh_charges WHERE 1=0", true))
		{
			$this->errors = $DB->RunSQLBatch($this->docRoot . "/bitrix/modules/{$this->MODULE_ID}/install/db/" . $DBType . "/install.sql");
		}

		if (Loader::includeModule($this->MODULE_ID))
		{
			$connection = Application::getInstance()->getConnection();
			if (!$connection->isTableExists(MonetaAdditionalInfoTable::getTableName()))
			{
				MonetaAdditionalInfoTable::getEntity()->createDbTable();
			}
		}

		$this->registerModuleHandlers();
		$this->addModuleAgents();

		return true;
	}

	// �������� ������ ������ �� �� � ������ ����������� ������

	/**
	 * @return bool|void
	 * @throws \Bitrix\Main\Db\SqlQueryException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	function UnInstallDB()
	{
		if ($this->getInstallParam('save_tables'))
		{
			$this->deactivateSubscribe();
		}
		else
		{
			$this->dropModuleTables();
		}

		\CAgent::RemoveModuleAgents($this->MODULE_ID);
		\CAdminNotify::DeleteByModule($this->MODULE_ID);
		$this->unRegisterModuleHandlers();

		ModuleManager::unRegisterModule($this->MODULE_ID);

		return true;
	}

	// ��������/���������� ����� � �������� �������� ���������

	/**
	 * @return bool
	 */
	function __InstallEvents()
	{
		global $APPLICATION;

		// ������ ���� ������, ��������������� �� ������
		$arSites = $this->getSiteList();

		// �������� ����� �������� ������� � �������� �������� ��-��������� ��� ���� ������
		$rsLanguages = CLanguage::GetList($b = "", $o = "");
		$obEventType = new CEventType();
		$obEventMessage = new CEventMessage();
		while ($arLang = $rsLanguages->Fetch())
		{
			if (!in_array($arLang["LID"], Array('ru', 'ua')))
			{
				continue;
			}

			// ����������� �������� ��������� ��� ������� �����
			IncludeModuleLangFile(__DIR__ . '/events/set_events.php', $arLang["LID"]);
			$arEventTypes = array();
			include(__DIR__ . '/events.php');

			foreach ($arEventTypes as $strEventName => $arEventTemplates)
			{
				$arEventTypeFields = Array(
					"LID" => $arLang["LID"],
					"EVENT_NAME" => $strEventName,
					"NAME" => Loc::getMessage($strEventName . '_TITLE'),
					"DESCRIPTION" => Loc::getMessage($strEventName . '_TEXT'),
				);
				$arEventType = CEventType::GetList(Array("EVENT_NAME" => $strEventName, 'LID' => $arLang['LID']))->Fetch();
				if (is_array($arEventType))
				{
					$bSuccess = $obEventType->Update(Array("ID" => $arEventType["ID"]), $arEventTypeFields);
				}
				else
				{
					$bSuccess = $obEventType->Add($arEventTypeFields) > 0;
					if ($bSuccess)
					{
						// ��������/���������� �������� �������� ��� ���� ������ ����� �����
						if (array_key_exists($arLang["LID"], $arSites) && count($arSites[$arLang["LID"]]) > 0)
						{
							foreach ($arEventTemplates as $arTemplate)
							{
								$arTemplate['EVENT_NAME'] = $strEventName;
								$arTemplate['LID'] = $arSites[$arLang["LID"]];

								if (!array_key_exists('EMAIL_FROM', $arTemplate))
								{
									$arTemplate['EMAIL_FROM'] = '#DEFAULT_EMAIL_FROM#';
								}
								if (!array_key_exists('EMAIL_TO', $arTemplate))
								{
									$arTemplate['EMAIL_TO'] = '#DEFAULT_EMAIL_FROM#';
								}
								if (!array_key_exists('BODY_TYPE', $arTemplate))
								{
									$arTemplate['BODY_TYPE'] = 'text';
								}
								if (!array_key_exists('ACTIVE', $arTemplate))
								{
									$arTemplate['ACTIVE'] = 'Y';
								}

								$bSuccess = $obEventMessage->Add($arTemplate) > 0;

								if (!$bSuccess)
								{
									$APPLICATION->ThrowException(Loc::getMessage("TSZH_ERROR_INSTALLING_EVENT_MESSAGES") . $obEventType->LAST_ERROR);

									return false;
								}
							}
						}
					}
				}

				if (!$bSuccess)
				{
					$APPLICATION->ThrowException(Loc::getMessage("TSZH_ERROR_INSTALLING_EVENT_TYPES") . $obEventType->LAST_ERROR);

					return false;
				}
			}
		}

		return true;
	}

	/**
	 * @return bool|void
	 */
	function InstallEvents()
	{
		foreach ($this->userGroups as $user_group)
		{
			$group_id = $this->createUserGroup($user_group);

			if ($group_id !== false && $user_group == 'TSZH_MEMBERS')
			{
				$this->setTszhMembersGroupOption($group_id);
			}

			if ($group_id !== false && $user_group == 'CITRUS_TSZH_1C_EXCHANGE')
			{
				$this->setTszhExchangeGroupRight($group_id);
			}
		}

		$this->setPersonalPathInCompositeExcludeMask();

		return self::__InstallEvents();
	}

	/**
	 * @param $group_string_id
	 *
	 * @return bool|int|mixed|string
	 */
	protected function createUserGroup($group_string_id)
	{
		$rsGroup = \CGroup::GetListEx(Array('ID' => 'DESC'), Array('STRING_ID' => $group_string_id), false, Array('nTopCount' => 1));

		if ($rsGroup->SelectedRowsCount() <= 0)
		{
			$arFields = Array(
				'ACTIVE' => 'Y',
				'C_SORT' => 100,
				'NAME' => Loc::getMessage('CITRUS_' . $group_string_id . '_GROUP_NAME'),
				'STRING_ID' => $group_string_id,
				'DESCRIPTION' => '',
				'USER_ID' => array(),
			);
			$obGroup = new CGroup();
			if (!($group_id = $obGroup->Add($arFields)))
			{
				$GLOBALS['APPLICATION']->ThrowException(Loc::getMessage('CITRUS_TSZH_ERROR_CREATING_USER_GROUP', array(
						'#NAME#' => $arFields['NAME'],
						'#ID#' => $arFields['STRING_ID'],
					)) . ': ' . $obGroup->LAST_ERROR);

				return false;
			}
		}
		else
		{
			$arFields = $rsGroup->GetNext();
			$group_id = $arFields['ID'];
		}

		return $group_id;
	}

	/**
	 * @return bool|void
	 */
	function UnInstallEvents()
	{
		$this->deletePersonalPathInCompositeExcludeMask();
		
		// ����������� ������� ���������� ���� � ������� ���������
		if ($this->getInstallParam('save_events'))
		{
			return true;
		}

		// ������ ����������������� ���� �������� ������� � �� �������
		if (!empty($this->getInstallParam('tszh_ids')))
		{
			$arCustomSubscribeTypes = $this->getCustomSubscribeTypes();
			$this->deleteCustomSubscribeEvents($arCustomSubscribeTypes);
			$this->deleteCustomSubscribeTypes($arCustomSubscribeTypes);
		}

		$arEventTypes = self::__GetEventTypes();
		$arEventNames = array_keys($arEventTypes);

		// �������� ������������ �������� �������� ��������� ��� ����� �������� ������� ������
		$rsEventMessages = CEventMessage::GetList($v1, $v2, array(
			'EVENT_NAME' => $arEventNames,
		));
		while ($arEventMessage = $rsEventMessages->Fetch())
		{
			CEventMessage::Delete($arEventMessage['ID']);
		}

		// �������� ������������ ����� �������� �������
		foreach ($arEventNames as $strEventType)
		{
			CEventType::Delete($strEventType);
		}

		return true;
	}

	// ������������� �������� ��������
	function installProductSubscribes()
	{
		$res = CModule::includeModule($this->MODULE_ID);
		if ($res)
		{
			CTszhSubscribe::installProductSubscribes();
		}
	}

	// ����������� ������ ������

	/**
	 * @return bool|void
	 * @throws Exception
	 */
	function InstallFiles()
	{
		CopyDirFiles(__DIR__ . "/admin", $this->docRoot . "/bitrix/admin", true, true);
		CopyDirFiles(__DIR__ . "/gadgets", $this->docRoot . "/bitrix/gadgets", true, true);
		CopyDirFiles(__DIR__ . "/tools", $this->docRoot . "/bitrix/tools", true, true);
		CopyDirFiles(__DIR__ . "/components", $this->docRoot . "/bitrix/components", true, true);
		CopyDirFiles(__DIR__ . "/templates/.default", $this->docRoot . "/bitrix/templates/.default", true, true);
		CopyDirFiles(__DIR__ . "/templates/citrus_tszh_subscribe", $this->docRoot . "/bitrix/templates/citrus_tszh_subscribe", true, true);

		CopyDirFiles(__DIR__ . "/wizards", $this->docRoot . "/bitrix/wizards/", true, true);
		CopyDirFiles(__DIR__ . "/themes/", $this->docRoot . "/bitrix/themes/", true, true);
		CopyDirFiles(__DIR__ . "/js/", $this->docRoot . "/bitrix/js/" . $this->MODULE_ID . "/", true, true);

		if (!$this->installGadgets('DEMODATA'))
		{
			throw new Exception(Loc::getMessage('CITRUS_TSZH_ERROR_INSTALL_GADGETS'));
		}

		if (!$this->installGadgets('TSZHACCESS'))
		{
			throw new Exception(Loc::getMessage('CITRUS_TSZH_ERROR_INSTALL_GADGETS'));
		}

		// ������������� �������� �������� ������ ��� ������ ������������� ��������� ������, ��������, �� ������� bitrixlabs.ru
		$this->installProductSubscribes();

		return true;
	}

	// �������� ������ ������

	/**
	 * @return bool|void
	 */
	function UnInstallFiles()
	{
		DeleteDirFiles(__DIR__ . "/admin", $this->docRoot . "/bitrix/admin");
		DeleteDirFiles(__DIR__ . "/gadgets", $this->docRoot . "/bitrix/gadgets");
		DeleteDirFiles(__DIR__ . "/tools", $this->docRoot . "/bitrix/tools");
		DeleteDirFiles(__DIR__ . "/themes/.default/", $this->docRoot . "/bitrix/themes/.default");//css
		DeleteDirFilesEx("/bitrix/themes/.default/icons/{$this->MODULE_ID}/");//icons
		DeleteDirFilesEx("/bitrix/images/{$this->MODULE_ID}/");//images
		DeleteDirFilesEx("/bitrix/js/{$this->MODULE_ID}/");
		DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/news/citrus.tszh.1c/");
		DeleteDirFilesEx("/bitrix/templates/citrus_tszh_subscribe/");

		$this->unInstallGadgets('DEMODATA');
		$this->unInstallGadgets('TSZHACCESS');

		return true;
	}

	// �������� ������ (���������� ���������)

	/**
	 * @return bool|mixed|void
	 * @throws \Bitrix\Main\LoaderException
	 */
	function DoUninstall()
	{
		global $APPLICATION;

		$APPLICATION->ResetException();

		if (!check_bitrix_sessid())
		{
			return false;
		}

		$step = IntVal($_REQUEST['step']);

		if ($step < 2)
		{
			$APPLICATION->IncludeAdminFile(Loc::getMessage("TSZH_UNINST_TITLE"), __DIR__ . "/uninst1.php");
		}
		elseif ($step == 2)
		{
			$arTszhIds = $this->getTszhIdList();

			$this->setInstallParam('save_tables', intval($_REQUEST['save_tables']))
			     ->setInstallParam('save_events', intval($_REQUEST['save_events']))
			     ->setInstallParam('tszh_ids', $arTszhIds);

			$this->UnInstallFiles();
			$this->UnInstallEvents();
			$this->UnInstallDB();


			$APPLICATION->IncludeAdminFile(Loc::getMessage("TSZH_UNINST_TITLE"), __DIR__ . "/uninst2.php");
		}

		return true;
	}

	/**
	 * @return array
	 */
	function GetModuleRightList()
	{
		return array(
			'reference_id' => array(
				'D',
				'E',
				'R',
				'U',
				'W',
			),
			'reference' => array(
				Loc::getMessage("CITRUS_TSZH_MODULE_RIGHT_D"),
				Loc::getMessage("CITRUS_TSZH_MODULE_RIGHT_E"),
				Loc::getMessage("CITRUS_TSZH_MODULE_RIGHT_R"),
				Loc::getMessage("CITRUS_TSZH_MODULE_RIGHT_U"),
				Loc::getMessage("CITRUS_TSZH_MODULE_RIGHT_W"),
			),
		);
	}

	/**
	 * @param $gadgets_id
	 *
	 * @return bool
	 */
	protected function installGadgets($gadgets_id)
	{
		$gdid = $gadgets_id . "@" . rand();
		$arUserOptions = CUserOptions::GetOption("intranet", "~gadgets_admin_index", false, false);
		if (!$this->isGadgetInstalled($gadgets_id, $arUserOptions))
		{
			foreach ($arUserOptions[0]["GADGETS"] as $tempid => $tempgadget)
			{
				if ($tempgadget["COLUMN"] == 0)
				{
					$arUserOptions[0]["GADGETS"][$tempid]["ROW"]++;
				}
			}

			$arUserOptions[0]["GADGETS"][$gdid] = array("COLUMN" => 0, "ROW" => 0, 'HIDE' => 'N');
			CUserOptions::SetOption("intranet", "~gadgets_admin_index", $arUserOptions);
		}

		return true;
	}

	/**
	 * @param $gadgets_id
	 */
	protected function uninstallGadgets($gadgets_id)
	{
		$arUserOptions = CUserOptions::GetOption("intranet", "~gadgets_admin_index", false, false);
		foreach ($arUserOptions[0]["GADGETS"] as $tempid => $tempgadget)
		{
			if (!(strcasecmp(substr($tempid, 0, 8), $gadgets_id)))
			{
				unset($arUserOptions[0]["GADGETS"][$tempid]);
			}
		}
		CUserOptions::SetOption("intranet", "~gadgets_admin_index", $arUserOptions);
	}

	/**
	 * @param $group_id
	 */
	protected function setTszhMembersGroupOption($group_id)
	{
		COption::SetOptionInt($this->MODULE_ID, "user_group_id", $group_id);
	}

	/**
	 * @param $group_id
	 */
	protected function setTszhExchangeGroupRight($group_id)
	{
		global $APPLICATION;
		$APPLICATION->SetGroupRight($this->MODULE_ID, $group_id, "U");
	}

	protected function setPersonalPathInCompositeExcludeMask($personal_path = "/personal/")
	{
		$personal_path = $personal_path . "*;";
		$settings = Helper::getOptions();
		if (strripos($settings["EXCLUDE_MASK"], $personal_path) === false)
		{
			$settings["EXCLUDE_MASK"] .= $personal_path;
		}

		return Helper::setOptions($settings);
	}

	protected function deletePersonalPathInCompositeExcludeMask($personal_path = "/personal/")
	{
		$personal_path = $personal_path . "*;";
		$settings = Helper::getOptions();

		$settings["EXCLUDE_MASK"] = str_ireplace($personal_path, '', $settings["EXCLUDE_MASK"]);

		return Helper::setOptions($settings);
	}

	protected function registerModuleHandlers()
	{
		$eventManager = EventManager::getInstance();
		$eventManager->registerEventHandler("main", "OnBeforeProlog", $this->MODULE_ID, "CTszhPublicHelper", "ShowPanel");
		$eventManager->registerEventHandler("main", "OnProlog", $this->MODULE_ID, "CTszhPublicHelper", "OnPrologHandler");
		$eventManager->registerEventHandler("main", "OnBuildGlobalMenu", $this->MODULE_ID, "CTszhPublicHelper", "OnBuildGlobalMenuHandler");
		$eventManager->registerEventHandler("main", "OnEventLogGetAuditTypes", $this->MODULE_ID, "CTszhPublicHelper", "OnEventLogGetAuditTypesHandler");
		$eventManager->registerEventHandler("main", "OnBeforeUserUpdate", $this->MODULE_ID, "CTszhPublicHelper", "OnBeforeUserUpdateHandler");
		$eventManager->registerEventHandler("main", "OnAfterUserUpdate", $this->MODULE_ID, "CTszhPublicHelper", "OnAfterUserUpdateHandler");
		$eventManager->registerEventHandler("main", "OnAfterUserDelete", $this->MODULE_ID, "CTszhSubscribe", "onAfterUserDelete");
		$eventManager->registerEventHandler("main", "OnAfterUserAuthorize", $this->MODULE_ID, "CTszhPublicHelper", "OnAfterUserAuthorizeHandler");
		// �������� ��������� �� ����� �������� ����� (�������� ���������)
		$eventManager->registerEventHandler("main", "OnBeforeEventAdd", $this->MODULE_ID, "CTszhSubscribe", "OnBeforeEventAdd");
		$eventManager->registerEventHandler("search", "OnAfterIndexAdd", $this->MODULE_ID, "CTszhPublicHelper", "OnAfterIndexAddUpdate");
		$eventManager->registerEventHandler("search", "OnAfterIndexUpdate", $this->MODULE_ID, "CTszhPublicHelper", "OnAfterIndexAddUpdate");
		$eventManager->registerEventHandler("iblock", "OnAfterIBlockElementAdd", $this->MODULE_ID, "CTszhPublicHelper", "OnAfterIBlockElementAddHandler");
		$eventManager->registerEventHandler("iblock", "OnAfterIBlockElementUpdate", $this->MODULE_ID, "CTszhPublicHelper", "OnAfterIBlockElementUpdateHandler");
		// ��� �������� ���������� ��������
		$eventManager->registerEventHandler("iblock", "OnIBlockPropertyBuildList", $this->MODULE_ID, "CTszhIBlockPropertySubscribeSend", "GetUserTypeDescription");
	}

	protected function unRegisterModuleHandlers()
	{
		$eventManager = EventManager::getInstance();
		$eventManager->unRegisterEventHandler("main", "OnBeforeProlog", $this->MODULE_ID, "CTszhPublicHelper", "ShowPanel");
		$eventManager->unRegisterEventHandler("main", "OnProlog", $this->MODULE_ID, "CTszhPublicHelper", "OnPrologHandler");
		$eventManager->unRegisterEventHandler("main", "OnBuildGlobalMenu", $this->MODULE_ID, "CTszhPublicHelper", "OnBuildGlobalMenuHandler");
		$eventManager->unRegisterEventHandler("main", "OnEventLogGetAuditTypes", $this->MODULE_ID, "CTszhPublicHelper", "OnEventLogGetAuditTypesHandler");
		$eventManager->unRegisterEventHandler("main", "OnBeforeUserUpdate", $this->MODULE_ID, "CTszhPublicHelper", "OnBeforeUserUpdateHandler");
		$eventManager->unRegisterEventHandler("main", "OnAfterUserUpdate", $this->MODULE_ID, "CTszhPublicHelper", "OnAfterUserUpdateHandler");
		$eventManager->unRegisterEventHandler("main", "OnAfterUserDelete", $this->MODULE_ID, "CTszhSubscribe", "onAfterUserDelete");
		$eventManager->unRegisterEventHandler("main", "OnAfterUserAuthorize", $this->MODULE_ID, "CTszhPublicHelper", "OnAfterUserAuthorizeHandler");
		$eventManager->unRegisterEventHandler("main", "OnBeforeEventAdd", $this->MODULE_ID, "CTszhSubscribe", "OnBeforeEventAdd");
		$eventManager->unRegisterEventHandler("iblock", "OnAfterIBlockElementAdd", $this->MODULE_ID, "CTszhPublicHelper", "OnAfterIBlockElementAddHandler");
		$eventManager->unRegisterEventHandler("search", "OnAfterIndexAdd", $this->MODULE_ID, "CTszhPublicHelper", "OnAfterIndexAddUpdate");
		$eventManager->unRegisterEventHandler("search", "OnAfterIndexUpdate", $this->MODULE_ID, "CTszhPublicHelper", "OnAfterIndexAddUpdate");
		$eventManager->unRegisterEventHandler("iblock", "OnAfterIBlockElementUpdate", $this->MODULE_ID, "CTszhPublicHelper", "OnAfterIBlockElementUpdateHandler");
		$eventManager->unRegisterEventHandler("iblock", "OnIBlockPropertyBuildList", $this->MODULE_ID, "CTszhIBlockPropertySubscribeSend", "GetUserTypeDescription");
	}

	protected function addModuleAgents()
	{
		CAgent::AddAgent("CTszhSubscribe::SubscribeAgent();", $this->MODULE_ID, "N", 60);
		CAgent::AddAgent("CTszh::checkAgent();", $this->MODULE_ID, "N");
		CAgent::AddAgent("CTszhAutoUpdater::updateAgent();", $this->MODULE_ID, "N");
		CAgent::AddAgent("CTszh::cleanupLogFilesAgent();", $this->MODULE_ID, "N", 86400);
	}

	/**
	 * @return array
	 */
	protected function getCustomSubscribeTypes()
	{
		$arCustomSubscribeTypes = array();
		foreach (CTszhSubscribe::getSubscribeClasses() as $className)
		{
			foreach ($this->getInstallParam('tszh_ids') as $tszhId)
			{
				$eventType = $className::EVENT_TYPE;
				CTszhSubscribe::getCustomEventTypeAndName(array("ID" => $tszhId), $eventType);
				$arCustomSubscribeTypes[] = $eventType;
			}
		}

		return $arCustomSubscribeTypes;
	}

	/**
	 * @param $arCustomSubscribeTypes
	 *
	 * @return bool
	 */
	protected function deleteCustomSubscribeEvents($arCustomSubscribeTypes)
	{
		$rsEventMessages = CEventMessage::GetList($v1, $v2, array(
			"EVENT_NAME" => $arCustomSubscribeTypes,
		));
		while ($arEventMessage = $rsEventMessages->Fetch())
		{
			CEventMessage::Delete($arEventMessage["ID"]);
		}

		return true;
	}

	/**
	 * @param $arCustomSubscribeTypes
	 */
	protected function deleteCustomSubscribeTypes($arCustomSubscribeTypes)
	{
		$oEventType = new CEventType();
		$rsEventTypes = CEventType::GetList(array());
		while ($arType = $rsEventTypes->fetch())
		{
			if (in_array($arType["EVENT_NAME"], $arCustomSubscribeTypes))
			{
				$oEventType->delete($arType["EVENT_NAME"]);
			}
		}
	}

	protected function deactivateSubscribe()
	{
		global $DB;

		$DB->Query("UPDATE " . CTszhSubscribeTable::getTableName() . " SET running = 'N'", false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
	}

	/**
	 * @throws \Bitrix\Main\Db\SqlQueryException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	protected function dropModuleTables()
	{
		global $DB, $DBType;

		$this->errors = $DB->RunSQLBatch($this->docRoot . "/bitrix/modules/{$this->MODULE_ID}/install/db/" . $DBType . "/uninstall.sql");

		if (Loader::includeModule($this->MODULE_ID))
		{
			$connection = Application::getInstance()->getConnection();
			if ($connection->isTableExists(MonetaAdditionalInfoTable::getTableName()))
			{
				$connection->dropTable(MonetaAdditionalInfoTable::getTableName());
			}
		}
	}

	/**
	 * @return array
	 * @throws \Bitrix\Main\LoaderException
	 */
	protected function getTszhIdList()
	{
		$arTszhIds = array();
		if (Loader::includeModule($this->MODULE_ID))
		{
			$rsTszhs = \CTszh::getList(array(), array(), false, false, array("ID"));
			while ($arTszh = $rsTszhs->fetch())
			{
				$arTszhIds[] = $arTszh["ID"];
			}
			unset($rsTszhs);
		}

		return $arTszhIds;
	}

	/**
	 * @return mixed
	 */
	protected function isBXVersionRequiredError()
	{
		return version_compare(SM_VERSION, self::MIN_VERSION, "<");
	}

	/**
	 * @return array
	 */
	protected function getSiteList()
	{
		$arSites = array();
		$rsSites = \CSite::GetList($by, $order);
		while ($arSite = $rsSites->Fetch())
		{
			if (!in_array($arSite["LANGUAGE_ID"], Array('ru', 'ua')))
			{
				continue;
			}
			$arSites[$arSite["LANGUAGE_ID"]][] = $arSite["LID"];
		}

		return $arSites;
	}

	/**
	 * @return bool
	 */
	protected function isModuleAutoInstall()
	{
		return defined('TSZH_MODULES_AUTO_INSTALL') && TSZH_MODULES_AUTO_INSTALL === true;
	}

	/**
	 * @return Closure
	 */
	protected function checkEdition()
	{
		return function ($minEdition) {
			if (function_exists('tszhCheckMinEdition'))
			{
				return tszhCheckMinEdition($minEdition);
			}

			$arEditionModules = array(
				'start' => array("ru" => "citrus.tszhstart", "ua" => "vdgb.tszhstartua"),
				'standard' => array("ru" => "citrus.tszhstandard", "ua" => "vdgb.tszhstandardua"),
				'smallbusiness' => array("ru" => "citrus.tszhsb", "ua" => "vdgb.tszhsbua"),
				'expert' => array("ru" => "citrus.tszhe", "ua" => "vdgb.tszheua"),
				'business' => array("ru" => "citrus.tszhb", "ua" => "vdgb.tszhbua"),
			);
			if (!array_key_exists($minEdition, $arEditionModules))
			{
				throw new Exception('Unknown edition specified!');
			}

			$bWasMinEdition = false;
			foreach ($arEditionModules as $strEdition => $arModuleIDs)
			{
				if ($strEdition == $minEdition)
				{
					$bWasMinEdition = true;
				}
				foreach ($arModuleIDs as $strRegion => $strModuleID)
				{
					if ($bWasMinEdition && IsModuleInstalled($strModuleID) && CModule::IncludeModule($strModuleID))
					{
						return true;
					}
				}
			}

			return false;
		};
	}

	/**
	 * @param $gadgets_id
	 * @param $arUserOptions
	 *
	 * @return bool
	 */
	protected function isGadgetInstalled($gadgets_id, $arUserOptions)
	{
		foreach ($arUserOptions[0]["GADGETS"] as $tempid => $tempgadget)
		{
			if (!(strcasecmp(substr($tempid, 0, 8), $gadgets_id)))
			{
				return true;
			}
		}

		return false;
	}

	function __GetEventTypes()
	{
		$arEventTypes = array();
		include(__DIR__ . '/events.php');
		return $arEventTypes;
	}
}