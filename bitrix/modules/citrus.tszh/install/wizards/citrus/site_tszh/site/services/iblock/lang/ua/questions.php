<?
$MESS["WZD_OPTION_QUESTIONS_1"] = "Питання";
$MESS["WZD_OPTION_QUESTIONS_2"] = "Опублікувати в розділі «Питання-відповідь» ";
$MESS["WZD_OPTION_QUESTIONS_3"] = "Рубрика";
$MESS["WZD_OPTION_QUESTIONS_4"] = "Питання";
$MESS["WZD_OPTION_QUESTIONS_5"] = "*Автор питання";
$MESS["WZD_OPTION_QUESTIONS_6"] = "Адреса";
$MESS["WZD_OPTION_QUESTIONS_7"] = "Телефон";
$MESS["WZD_OPTION_QUESTIONS_8"] = "E-Mail";
$MESS["WZD_OPTION_QUESTIONS_9"] = "Текст питання";
$MESS["WZD_OPTION_QUESTIONS_10"] = "Відповідь";
$MESS["WZD_OPTION_QUESTIONS_11"] = "Автор відповіді";
$MESS["WZD_OPTION_QUESTIONS_12"] = "Текст відповіді";
?>