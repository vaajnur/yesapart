<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!tszhCheckMinEdition('standard'))
	return;

$wizard =& $this->GetWizard();

// copy files
$sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH."/public/".LANGUAGE_ID."/");
$destPath = WIZARD_SITE_PATH;
// ���� ��������������� ���������� ������
if (WIZARD_TEMPLATE_ID == 'citrus_tszh_adaptive')
{
	$destPath .= 'epassports/';
	$adaptive_tmp = 'epassports/';
}
// ���� ��������������� ������ ������
else
{
	$adaptive_tmp = '';
}
CopyDirFiles($sourcePath, $destPath, true, true);


//Add menu items
// ���� ��������������� ���������� ������
if (WIZARD_TEMPLATE_ID == 'citrus_tszh_adaptive')
{
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/epassports/.show_add.menu.php", Array(
		GetMessage("F_TSZH_DEBTORS_MENUITEM"),
		WIZARD_SITE_DIR . $adaptive_tmp . "debtors/",
		Array(),
		Array(),
		""
	), WIZARD_SITE_ID, -1);
}
else
{
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/.top.menu.php", Array(
		GetMessage("F_TSZH_DEBTORS_MENUITEM"),
		WIZARD_SITE_DIR . $adaptive_tmp . "debtors/",
		Array(),
		Array(),
		""
	), WIZARD_SITE_ID, -1);
}

$arSubscribes = CTszhSubscribe::getSubscribes4Admin(false, $wizard);
$arDebtorsParams = $arSubscribes["CTszhSubscribeDebtors"]["SETTINGS_COLLECTED"]["PARAMS"];
CTszhSubscribeDebtors::checkParams($arDebtorsParams, 0, true);

$connectOrchidComponentTszhDolg = <<<PHP
<?\$APPLICATION->IncludeComponent("citrus:tszh.dolg", "orchid_default",
	array(
		"MAX_COUNT" => "15",
		"MIN_DEBT" => "#DEBTORS_MIN_SUM#",
		"DAYS_2_PAY" => "#DEBTORS_DAY#",
		"FILTER_NAME" => "",
		"GROUP_BY" => "HOUSE",
		"GROUP_BY_SUMMARY" => "SUM",
		"GROUP_BY_SUMMARY_FIELD" => "DEBT_END",
		"COLS_COUNT" => "3",
		"COL_0" => "STREET",
		"COL_1" => "HOUSE",
		"COL_2" => "DEBT_END",
		"ACCESS_FIELDS" => array(
			0 => "XML_ID",
			1 => "USER_LAST_NAME",
			2 => "USER_NAME",
			3 => "USER_SECOND_NAME",
			4 => "USER_FULL_NAME",
			5 => "USER_FULL_ADDRESS",
			6 => "FLAT",
		),
		"ACCESS_GROUPS" => array(
			0 => "1",
		),
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "��������",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
	), false
);?>
PHP;

$connectLotosComponentTszhDolg = <<<PHP
<?\$APPLICATION->IncludeComponent("citrus:tszh.dolg", ".default",
	array(
		"MAX_COUNT" => "15",
		"MIN_DEBT" => "#DEBTORS_MIN_SUM#",
		"DAYS_2_PAY" => "#DEBTORS_DAY#",
		"FILTER_NAME" => "",
		"GROUP_BY" => "HOUSE",
		"GROUP_BY_SUMMARY" => "SUM",
		"GROUP_BY_SUMMARY_FIELD" => "DEBT_END",
		"COLS_COUNT" => "3",
		"COL_0" => "STREET",
		"COL_1" => "HOUSE",
		"COL_2" => "DEBT_END",
		"ACCESS_FIELDS" => array(
			0 => "XML_ID",
			1 => "USER_LAST_NAME",
			2 => "USER_NAME",
			3 => "USER_SECOND_NAME",
			4 => "USER_FULL_NAME",
			5 => "USER_FULL_ADDRESS",
			6 => "FLAT",
		),
		"ACCESS_GROUPS" => array(
			0 => "1",
		),
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "��������",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
	), false
);?>
PHP;


$arReplaceMacros = Array(
	"SITE_DIR" => WIZARD_SITE_DIR,
	"SITE_NAME" => htmlspecialcharsbx($wizard->GetVar("siteName")),
	"SITE_EMAIL" => htmlspecialcharsbx($wizard->GetVar("siteEmail")),
	"SITE_PHONE" => htmlspecialcharsbx($wizard->GetVar("siteTelephone")),
	"SITE_ADDRESS" => htmlspecialcharsbx($wizard->GetVar("siteAddress")),
	"DEBTORS_MIN_SUM" => $arDebtorsParams["MIN_SUM"],
	"DEBTORS_DAY" => $arDebtorsParams["DATE"],
	"CONNECT_COMPONENT_TSZHDOLG" => (isAdaptiveTszhTemplate?$connectOrchidComponentTszhDolg:$connectLotosComponentTszhDolg)
);
WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH . '/' . $adaptive_tmp . 'debtors/', $arReplaceMacros);