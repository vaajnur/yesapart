<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!tszhCheckMinEdition('expert'))
	return;

$wizard =& $this->GetWizard();

// copy files
$sourcePath = str_replace("//", "/", $_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/vdgb.operator/install/public/".LANGUAGE_ID."/");
$destPath = str_replace("//", "/", WIZARD_SITE_PATH . '/operator/');
CopyDirFiles($sourcePath, $destPath, true, true);

?>