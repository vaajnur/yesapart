<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "���������� � ������� ������� ��� �������� �������");
$APPLICATION->SetPageProperty("description", "���������� � ������� ������� ��� �������� ������� � ���������������� ��������.");
$APPLICATION->SetPageProperty("keywords", "���������� ������� ������� �������� �������� ���");
$APPLICATION->SetTitle("���������� � ������� ������� ��� �������� � �������� ������� � ���������������� ��������");
?>
<h3>1. ����� ����������</h3>
<ol style="list-style-type: none;">
	<li><p>1.1. ����� ��  �������� ������������ ���������� � ����������� ������� � ��������������� ����� �����  (����� � ������ � �������), �������� ������ � �������� ������������ ����� �������, �������������� ���������� ��������� � ��������� ������ �����������, ������ ������ �������� ������������, ��������� ����, ��� ����������� ��� ������������� ������ � ��� ������������� ���������, ������ ������������ �������� �������������, ������ ���� ������������  ���� �������, � �������� ���� ������� �����.</p></li>
	<li><p>1.2. ������� ����� ���������� ��������� ���������� ���� ����������, � ��� �� ������ ����������� ���������� � ������� �������� ������������.</p></li>
	<li>1.3. ��������� ��������� �����, ������� ����� �������� � ������������� ������ � ����� ����������, ��������:
		<img src="/images/fireman.jpg" width="150" height="181" style="float: left; margin: 8px;" />
		<ol style="list-style-type: none;">
			<li><p>1.3.1. ������������ �������� ���������� �������� ������������ ��� ��������������  ���������, � ��� ����� ���� ����� � �����;</p></li>
			<li><p>1.3.2. ������������ ���������� ������������ ��� ������������� ������ �������� ��������� (�����, ���� � �.�.), ������������� �������� � ���������;</p></li>
			<li><p>1.3.3. ��������� ���������� ��� �������� ������� ������� � ����������, � ��� �� ��� ���������� ����������� �����;</p></li>
			<li><p>1.3.4. ���������� ��������  ������� �� �������� �������, �������������� ������������� � ��������������� ������.</p></li>
		</ol>
	</li>
	<li><p>1.4. ������������ �������  �������� ����������� ������������� �������. ������ �� ������  ��������� �������, ������� ����� �������� � ������������� ������.</p></li>
	<li>1.5. ����������� ������� � ������ ������������� ������:
		<ol style="list-style-type: none;">
			<li><p>1.5.1. ��������������� �������� � ������������� ������ �� ���������� �������� 01 ��� �� ���������� �������� 010, � ��������������� �������-������������ ������, ������� �����, ����� ������������� ������, ������� ���������, � ����� ��������� �������������� ���������� � ������.</p></li>
			<li><p>1.5.2. ��� ����������� ���������� � ������� ������ ���������� ���������� �������������, ����������� ��� ���� ������ ������������ � ������������ ������ ���, � ��� �� ������������� ������  ���  � ������������.</p></li>
			<li><p>1.5.3. ��������� ������������ ������������ ������� ������ � ������������ �����.</p></li>
			<li><p>1.5.4. ������������� ������������ ������� ������ � ������������ ����� � �����, ������� ��������� ��� ����� ���������� � ���� ���������, � ����� �������� � ���������� �������������.</p></li>
		</ol>
	</li>
	<li><br />
		1.6. ���� ���������� ��������� �������:
		<ol style="list-style-type: none;">
			<li><p>1.6.1. � ������ ������������� ��������� ��������� ������������ ������������ ������������� � ������������ �����;</p></li>
			<li><p>1.6.2. ��� ��������� ��������� ����������� � �� ��������� ������; ���� ���� �����������, ����� � ����� ���������, ������, ��������� ������������� � ������� ������������, ������� ���� � ����� � ��������;</p></li>
			<li><p>1.6.3. �������������� ���������� �� ����� ��������� (���������, ���������� �������), �������� � ������ ������. ���� ��� �������� ���������� � ����������� � ��������. ���������� ������ ��������� ������������� ���������� � ������ ����� � ����� ���������������. ������� ���� ������������ ��� ����, ����� � �������� ����� ����, ��� ����� ������ ������������ � ������.</p></li>
			<li><p style="color: #f00; font-weight: bold;">1.6.4. �� � ���� ������ �� ����������� ���� �� ����� ������.</p></li>
		</ol>
	</li>
</ol>

<h3>2. ���������� ���������� ����� �����, ������ � ���������</h3>
<ol style="list-style-type: none;">
	<li><p>2.1. ������ � ���������� ���� � �������, ���������� � ���������� ������������� ���������� ��������� ���, ����� ���������� ������ �������� �������;</p></li>
	<li><p>2.2. ����������� ������������� ��������� ��������� ���, ����� �������� ���������� � ������ ����,  ����� ������� �������� ������� � �������, ���������� ��� ���������� �������������;</p></li>
	<li><p>2.3. ������������ ������������ �����  � ���������  � �  ���������� ������ �����������: ��������� � ����� ���������, ������������� ������ � ��.��������, ���������� ����������� � ����������� ��������� � ��������� ������������� ������������ �����������������  ����������;</p></li>
	<li>2.4. � ����� ������ �����������:
		<ol style="list-style-type: none;">
			<li><p>2.4.1. ����������� ���������������� ���������, � ������� ������������ �������������,  ���������������������  � ������� ��������;</p></li>
			<li><p>2.4.2. ������������ ������� � �������, � ����� ����������� ���������, � �����, ����������������� ������������ ��������;</p></li>
			<li><p>2.4.3. ����������� ������ ������� ���������� � ����������, ������� �� �������� ���������������� ������������� �� ������ ���������, �����  ��������� � ���������� ������;</p></li>
			<li><p>2.4.4. ������� ������� �������, � ��� �� ��������������������� � ������� �������� � ��������, �� ��������� ������, ��������, �������� � �������;</p></li>
			<li><p>2.4.5. ��� ������ ��������� ������������ ��������������������� ��������, �� ��������������� ��� �����;</p></li>
			<li><p>2.4.6. �������� ��������������������� � ������� �������� � �����������;</p></li>
			<li><p>2.4.7. ��������� ��� ��������� ������� ������, ��������������������� ��������� � ������, ������������  ���  ������;</p></li>
			<li><p>2.4.8. ������������ � ������� ���������� ��������, ��������� � ���������� ��������, ��������������� � ������������� ������� ������� ����������;</p></li>
			<li><p>2.4.9. ������� ����������� �������� � ������������� ��������� � �������� ��� ����, ����������������� �������������� � ������������ ��������� ��������;</p></li>
			<li><p>2.4.10. ��������� ��� ��������� ������������, ���� � ������, ���� ��� �� ��������� ������������ ���������, � ��� �� �������������������, ���������� � ����, ���� ���������� �� ������������ ��� ���������;</p></li>
			<li><p>2.4.11. ������������ ������������ ������������ ������� � ��������, ������������� ����;</p></li>
			<li><p>2.4.12. ��������� ������� ��������� �� ������������ �������� � ������������, � ����� �����  0,5 ������ �� ������������� ��������;</p></li>
			<li><p>2.4.13. ���������� �������� ������������ � ������� ��������� ����;</p></li>
			<li><p>2.4.14. ������������ ������������ � �������� �����, �� �������� ������� ������������;</p></li>
			<li><p>2.4.15. ����������� ����������� ��� ������������ �������, ������������� ��������� ��� ������ ������������, ����������������� ��� ������������� � ����� ����� � ����������;</p></li>
			<li><p>2.4.16. ������������ ����������� ������������� ������� � �������������� ������������� �������������� ������������;</p></li>
			<li><p>2.4.17. ��������� ��������������� ��� �������������� ������������� ���������������������;</p></li>
			<li><p>2.4.18. ������������ ��������������� � ����������� ���������, � ��� �� ����������� ����������, ��������� ������� ���������� �������������.</p></li>
		</ol>
	</li>
	<li>2.5. �� ������������� ����� �����������:
		<ol style="list-style-type: none;">
			<li><p>2.5.1. ������������� ����� � ���������, ���������� ��� ����������� ������� ������������� �����������  �������� ���� � ������������� ���������� �������;</p></li>
			<li><p>2.5.2. ��������� ��������, ������ � ������������, ���� ��� ��������� ������ ���� ���������;</p></li>
			<li><p>2.5.3. ����������� ������ � ��������, � ��� �� ������� ��������� ��������� �� ���������� �������;</p></li>
			<li><p>2.5.4. ������������ ������������ ���������� ������������� ��� ��������� �����, ���� �� �������� ��� �������, � ��� �� �������� � ������� ������ � ������ �� �������� ������������� ��������;</p></li>
			<li><p>2.5.5. �� ����������� ������������� ��� ��������� ��������� (���, ��� �� ������������� ��� ��������� ������ ����������) ������������� ��������, ����, �������� �� �������� � �������;</p></li>
		</ol>
	</li>
</ol>

<h3>3. ��������� - ����������� ������� � ������������</h3>
<ol style="list-style-type: none;">
	<li><p>3.1. ������������ ����� ����, ���� 9-�� ������, ����������� ��������������� ��������� ����������� ������ � ������������, ������������� ��������� �������� ���� (������������) � ���������������� �������������;</p></li>
	<li><p>3.2. ����������� �������� ������������ ���������� ���������-����������� ������ � ������������ � ������� �������;</p></li>
	<li><p>3.3. ���������-����������� ������� ��������������� � ������������ � ����������� ������������� (�����������) �������������;</p></li>
	<li><p>3.4. ������� ���������� ���������� � ����������� ���������-����������� ������ � ������� ��������� � ������������� ������������ ����� � �������������� ������ � ������������;</p></li>
</ol>

<h3>4. ������������</h3>
<ol style="list-style-type: none;">
	<li><p>4.1. � ��������� ������������� ��������� ��������� ������ ��������������� ����������� ������ � ������������ (������� ����������) � ��������� ������� ������������� (�������������);</p></li>
	<li><p>4.2. �����, ��������� ����������� � ��������, ������������� ���������� ���� ������������ ���������� �������������� ������ ������� ������� �� ������ ������ � ������������� ���������.</p></li>
</ol>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>