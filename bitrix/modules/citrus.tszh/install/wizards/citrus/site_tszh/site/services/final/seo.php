<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!defined("WIZARD_SITE_ID") || !defined("WIZARD_SITE_DIR"))
	throw new \Exception("No wizard constants defined");

// ��� ��������� ������ ������ �� ������ (� ��������) �� ����� ��������� robots.txt � sitemap
if (WIZARD_SITE_DIR !== '/')
{
	unlink(WIZARD_SITE_PATH . DIRECTORY_INDEX . 'robots.txt');
	return;
}

\Citrus\Tszh\Wizard\Seo::save($this->GetWizard());