<?
$MESS["F_TICKETS_MENU"] = "Мои заявки";
$MESS["F_TICKETS_HOME"] = "Подать заявку в Аварийно-диспетчерскую службу";
$MESS["GROUP_TSZH_MEMBERS"] = "Жильцы";
$MESS["GROUP_TSZH_SUPPORT_ADMINISTRATORS"] = "Диспетчеры «Аварийно-диспетчерской службы»";
$MESS["GROUP_TSZH_SUPPORT_CONTRACTORS"] = "Подрядные организации «Аварийно-диспетчерской службы»";
$MESS["SUP_DEF_SLA_NAME"] = "По умолчанию";
$MESS["SUP_DEF_COMMON"] = "Общее";
$MESS["SUP_DEF_LOW"] = "Низкая";
$MESS["SUP_DEF_MIDDLE"] = "Средняя";
$MESS["SUP_DEF_HIGH"] = "Высокая";
$MESS["SUP_DEF_REQUEST_ACCEPTED"] = "Новая заявка";
$MESS["SUP_DEF_PROBLEM_SOLVING_IN_PROGRESS"] = "Принята";
$MESS["SUP_DEF_COULD_NOT_BE_SOLVED"] = "Не представляется возможным выполнить";
$MESS["SUP_DEF_SUCCESSFULLY_SOLVED"] = "Завершено";
$MESS["SUP_DEF_ANSWER_SUITS_THE_NEEDS"] = "Результат устраивает";
$MESS["SUP_DEF_ANSWER_IS_NOT_COMPLETE"] = "Недостаточно полно";
$MESS["SUP_DEF_ANSWER_DOES_NOT_SUIT"] = "Результат не устраивает";
$MESS["SUP_DEF_E_MAIL"] = "E-Mail";
$MESS["SUP_DEF_PHONE"] = "Телефон";
$MESS["SUP_DEF_FORUM"] = "Форум";
$MESS["SUP_DEF_EASY"] = "Легкий";
$MESS["SUP_DEF_MEDIUM"] = "Средний";
$MESS["SUP_DEF_HARD"] = "Высокий";
$MESS["CTT_NEW_TICKET"] = "Новая заявка";
$MESS["CTT_NEW_TICKET_D"] = "Заявка подана жильцом через личный кабинет на сайте
--
Заявка еще не рассмотрена";
$MESS["CTT_TICKET_DELEGATED"] = "Передана на исполнение";
$MESS["CTT_TICKET_DELEGATED_D"] = "Диспетчер назначил подрядную организацию
--
Заявка передана в подрядную организацию";
$MESS["CTT_TICKET_ACCEPTED"] = "Принята на исполнение";
$MESS["CTT_TICKET_ACCEPTED_D"] = "Подрядная организация приняла заявку
--
Заявка находится в стадии выполнения";
$MESS["CTT_TICKET_DONE"] = "Выполнена";
$MESS["CTT_TICKET_DONE_D"] = "Подрядная организация отчиталась о выполнении
--
Заявка выполнена";
$MESS["CTT_TICKET_CLOSED"] = "Завершена";
$MESS["CTT_TICKET_CLOSED_D"] = "Диспетчер принял справку о выполнении
--
Заявка закрыта";
?>