<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

CTszhFunctionalityController::checkSubscribe(false, $isSubscribeEdition);
if (!$isSubscribeEdition)
	return;

/** @var CWizardBase $wizard */
$wizard =& $this->GetWizard();

// copy files
$sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH."/public/".LANGUAGE_ID."/");
$destPath = WIZARD_SITE_PATH;
CopyDirFiles($sourcePath, $destPath, true, true);

if (isAdaptiveTszhTemplate())
{
    $connectOrchidComponentSubscription = <<<PHP
<?\$APPLICATION->IncludeComponent("citrus:tszh.subscription", "orchid_default", 
	array(),
	false
);?>
PHP;
    CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "/personal/subscription/index.php", array("CONNECT_COMPONENT_SUBSCRIPTION" => $connectOrchidComponentSubscription));
}
else
{
    $connectLotosComponentSubscription = <<<PHP
<?\$APPLICATION->IncludeComponent("citrus:tszh.subscription", ".default", 
	array(),
	false
);?>
PHP;
    CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "/personal/subscription/index.php", array("CONNECT_COMPONENT_SUBSCRIPTION" => $connectLotosComponentSubscription));
}

// add menu item
WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/personal/.show_add.menu.php", Array(
	GetMessage("WIZ_TSZH_SUBSCRIBE_SUBSCRIPTION_CONTROL"),
	WIZARD_SITE_DIR . "personal/subscription/",
	Array(),
	Array(),
	""
), WIZARD_SITE_ID, -1);
WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/personal/.section.menu.php", Array(
	GetMessage("WIZ_TSZH_SUBSCRIBE_SUBSCRIPTION_CONTROL"),
	WIZARD_SITE_DIR . "personal/subscription/",
	Array(),
	Array("class"=>"leftmenu__img-subscriptions"),
	""
), WIZARD_SITE_ID, -1);

// ��������� ��������� ��������
$arSubscribes = CTszhSubscribe::getSubscribes4Admin(false, $wizard);
foreach ($arSubscribes as $className => $arSubscribe)
{
	if ($arSubscribe["SETTINGS_COLLECTED"]["ACTIVE"] != "Y")
	{
		$arSubscribe["SETTINGS"]["ACTIVE"]["value"] = "N";
		unset($arSubscribe["SETTINGS"]["PARAMS"]);
	}

	$className::saveSettings($arSubscribe["ID"], $arSubscribe["SETTINGS"]);
}
