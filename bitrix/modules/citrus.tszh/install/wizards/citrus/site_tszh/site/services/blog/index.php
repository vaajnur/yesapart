<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!tszhCheckMinEdition('standard'))
{
	return;
}

if (!CModule::IncludeModule('blog'))
{
	return;
}

if (isAdaptiveTszhTemplate())
{
    // add menu item
    (new WizardServices)->AddMenuItem(WIZARD_SITE_DIR . "/epassports/.show_add.menu.php", array(
        GetMessage("BLOG_DEMO_BOTTOM_MENU"),
        WIZARD_SITE_DIR . "blog/",
        array(),
        array(),
        "",
    ), WIZARD_SITE_ID, -1);
}
else
{
    // add menu item
    (new WizardServices)->AddMenuItem(WIZARD_SITE_DIR . "/photogallery/.show_add.menu.php", array(
        GetMessage("BLOG_DEMO_BOTTOM_MENU"),
        WIZARD_SITE_DIR . "blog/",
        array(),
        array(),
        "",
    ), WIZARD_SITE_ID, -1);
    (new WizardServices)->AddMenuItem(WIZARD_SITE_DIR . "/photogallery/.section.menu.php", array(
        GetMessage("BLOG_DEMO_BOTTOM_MENU"),
        WIZARD_SITE_DIR . "blog/",
        array(),
        array(),
        "",
    ), WIZARD_SITE_ID, -1);
}

if (COption::GetOptionString("citrus.tszh", "wizard_blog_installed", "N", WIZARD_SITE_ID) == "Y")
{
    return;
}

$siteID = WIZARD_SITE_ID;
$siteName = htmlspecialcharsbx($wizard->GetVar("siteName"));

$dbSite = CSite::GetByID($siteID);
if ($arSite = $dbSite->Fetch())
{
	$LID = $arSite["LANGUAGE_ID"];
}
if (strlen($LID) <= 0)
{
	$LID = "ru";
}

$dbGroup = CBlogGroup::GetList(array("ID" => "ASC"), array("SITE_ID" => $siteID));
if (!$dbGroup->Fetch())
{
	COption::SetOptionString('blog', 'avatar_max_size', '30000');
	COption::SetOptionString('blog', 'avatar_max_width', '50');
	COption::SetOptionString('blog', 'avatar_max_height', '50');
	COption::SetOptionString('blog', 'image_max_width', '600');
	COption::SetOptionString('blog', 'image_max_height', '600');
	COption::SetOptionString('blog', 'allow_alias', 'Y');
	COption::SetOptionString('blog', 'block_url_change', 'Y');
	COption::SetOptionString('blog', 'GROUP_DEFAULT_RIGHT', 'D');
	COption::SetOptionString('blog', 'show_ip', 'Y');
	COption::SetOptionString('blog', 'enable_trackback', 'N');
	COption::SetOptionString('blog', 'allow_html', 'Y');
	COption::SetOptionString('blog', 'parser_nofollow', 'Y');

	/*if(IsModuleInstalled("socialnetwork"))
		$SocNetGroupID = CBlogGroup::Add(array("SITE_ID" => $siteID, "NAME" => GetMessage("BLOG_DEMO_GROUP_SOCNET")));*/

	$groups = $groupID = CBlogGroup::Add(array("SITE_ID" => $siteID, "NAME" => GetMessage("BLOG_DEMO_GROUP_1")));
	$groups .= ",";
	$groups .= CBlogGroup::Add(array("SITE_ID" => $siteID, "NAME" => GetMessage("BLOG_DEMO_GROUP_2")));
	$groups .= ",";
	$groups .= CBlogGroup::Add(array("SITE_ID" => $siteID, "NAME" => GetMessage("BLOG_DEMO_GROUP_3")));
	$groups .= ",";
	$groups .= CBlogGroup::Add(array("SITE_ID" => $siteID, "NAME" => GetMessage("BLOG_DEMO_GROUP_4")));
	$groups .= ",";
	$groups .= CBlogGroup::Add(array("SITE_ID" => $siteID, "NAME" => GetMessage("BLOG_DEMO_GROUP_5")));
	$groups .= ",";
	$groups .= CBlogGroup::Add(array("SITE_ID" => $siteID, "NAME" => GetMessage("BLOG_DEMO_GROUP_6")));
	$groups .= ",";
	$groups .= CBlogGroup::Add(array("SITE_ID" => $siteID, "NAME" => GetMessage("BLOG_DEMO_GROUP_7")));

	$blogID = CBlog::Add(
		array(
			"NAME" => GetMessage("BLOG_DEMO_BLOG_NAME", array("#SITE_NAME#" => $siteName)),
			"DESCRIPTION" => GetMessage("BLOG_DEMO_BLOG_NAME", array("#SITE_NAME#" => $siteName)),
			"GROUP_ID" => $groupID,
			"ENABLE_IMG_VERIF" => 'Y',
			"EMAIL_NOTIFY" => 'Y',
			"ENABLE_RSS" => "Y",
			"ALLOW_HTML" => "Y",
			"URL" => "admin-blg-" . $siteID,
			"ACTIVE" => "Y",
			"=DATE_CREATE" => $DB->GetNowFunction(),
			"=DATE_UPDATE" => $DB->GetNowFunction(),
			"OWNER_ID" => 1,
			"PERMS_POST" => array("1" => BLOG_PERMS_READ, "2" => BLOG_PERMS_READ),
			"PERMS_COMMENT" => array("1" => BLOG_PERMS_READ, "2" => BLOG_PERMS_WRITE),
		)
	);

	$friends = CBlogUserGroup::Add(array(
		"NAME" => GetMessage("BLOG_DEMO_FRIENDS"),
		"BLOG_ID" => $blogID,
	));

	CBlogUserGroupPerms::Add(
		array(
			"BLOG_ID" => $blogID,
			"USER_GROUP_ID" => $friends,
			"PERMS_TYPE" => "P",
			"PERMS" => "I",
			"AUTOSET" => "N",
		)
	);
	CBlogUserGroupPerms::Add(
		array(
			"BLOG_ID" => $blogID,
			"USER_GROUP_ID" => $friends,
			"PERMS_TYPE" => "C",
			"PERMS" => "P",
			"AUTOSET" => "N",
		)
	);

	$categoryID[] = CBlogCategory::Add(array("BLOG_ID" => $blogID, "NAME" => GetMessage("BLOG_DEMO_CATEGORY_1")));
	$categoryID[] = CBlogCategory::Add(array("BLOG_ID" => $blogID, "NAME" => GetMessage("BLOG_DEMO_CATEGORY_2")));

	$postID = CBlogPost::Add(
		array(
			"TITLE" => GetMessage("BLOG_DEMO_MESSAGE_TITLE_1"),
			"DETAIL_TEXT" => GetMessage("BLOG_DEMO_MESSAGE_BODY_1"),
			"DETAIL_TEXT_TYPE" => "text",
			"BLOG_ID" => $blogID,
			"AUTHOR_ID" => 1,
			"=DATE_CREATE" => $DB->GetNowFunction(),
			"=DATE_PUBLISH" => $DB->GetNowFunction(),
			"PUBLISH_STATUS" => BLOG_PUBLISH_STATUS_PUBLISH,
			"ENABLE_TRACKBACK" => 'N',
			"ENABLE_COMMENTS" => 'N',
			"CATEGORY_ID" => implode(",", $categoryID),
			"PERMS_POST" => array(1 => BLOG_PERMS_READ, 2 => BLOG_PERMS_READ),
			"PERMS_COMMENT" => array(1 => BLOG_PERMS_WRITE, 2 => BLOG_PERMS_READ),
		)
	);

	foreach ($categoryID as $v)
	{
		CBlogPostCategory::Add(array("BLOG_ID" => $blogID, "POST_ID" => $postID, "CATEGORY_ID" => $v));
	}

	$arImage = CFile::MakeFilearray(WIZARD_SERVICE_ABSOLUTE_PATH . '/images/' . GetMessage("BLOG_DEMO_IMG"));
	$arImage["MODULE_ID"] = "blog";
	$arFields = array(
		"BLOG_ID" => $blogID,
		"POST_ID" => $postID,
		"USER_ID" => 1,
		"=TIMESTAMP_X" => $DB->GetNowFunction(),
		"TITLE" => GetMessage("BLOG_DEMO_IMG_TITLE"),
		"IMAGE_SIZE" => $arImage["size"],
		"FILE_ID" => $arImage,
	);
	CBlogImage::Add($arFields);

	CBlogComment::Add(array(
		"TITLE" => GetMessage("BLOG_DEMO_COMMENT_TITLE"),
		"POST_TEXT" => GetMessage("BLOG_DEMO_COMMENT_BODY"),
		"BLOG_ID" => $blogID,
		"POST_ID" => $postID,
		"PARENT_ID" => 0,
		"AUTHOR_ID" => 1,
		"DATE_CREATE" => ConvertTimeStamp(false, "FULL"),
		"AUTHOR_IP" => "192.168.0.1",
	));

	CBlogSitePath::Add(array("SITE_ID" => $siteID, "PATH" => WIZARD_SITE_DIR . "blog/#blog#/", "TYPE" => "B"));
	CBlogSitePath::Add(array("SITE_ID" => $siteID, "PATH" => WIZARD_SITE_DIR . "blog/#blog#/#post_id#.php", "TYPE" => "P"));
	CBlogSitePath::Add(array("SITE_ID" => $siteID, "PATH" => WIZARD_SITE_DIR . "blog/user/#user_id#.php", "TYPE" => "U"));
}

// copy files
$sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH . "/public/" . LANGUAGE_ID . "/");
$destPath = WIZARD_SITE_PATH;
CopyDirFiles($sourcePath, $destPath, true, true);

// replace macros
$arReplace = array(
	"SEF_FOLDER" => WIZARD_SITE_DIR . 'blog/',
	"GROUPS_ID" => $groups,
);
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "blog/index.php", $arReplace);



// add url rewrite rule
$arUrlRewrite = array();
if (file_exists(WIZARD_SITE_ROOT_PATH . "/urlrewrite.php"))
{
	include(WIZARD_SITE_ROOT_PATH . "/urlrewrite.php");
}
$arNewUrlRewrite = array(
	array(
		"CONDITION" => "#^" . WIZARD_SITE_DIR . "blog/#",
		"RULE" => "",
		"ID" => "bitrix:blog",
		"PATH" => WIZARD_SITE_DIR . "blog/index.php",
	),
);

foreach ($arNewUrlRewrite as $arUrl)
{
	if (!in_array($arUrl, $arUrlRewrite))
	{
		CUrlRewriter::Add($arUrl);
	}
}

COption::SetOptionString("citrus.tszh", "wizard_blog_installed", "Y", false, WIZARD_SITE_ID);
?>