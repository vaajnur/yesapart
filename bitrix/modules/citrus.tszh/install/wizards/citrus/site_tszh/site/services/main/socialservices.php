<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!defined("WIZARD_SITE_ID"))
	return;

if (CModule::IncludeModule('socialservices'))
{
	$oAuthManager = new CSocServAuthManager();
	$suffix = '_bx_site_' . WIZARD_SITE_ID;
	$arServices = array_fill_keys(array_keys($oAuthManager->GetAuthServices($suffix)), 'Y');
	COption::SetOptionString("socialservices", "auth_services".$suffix, serialize($arServices));

	// use on sites
	$arUseOnSites = unserialize(COption::GetOptionString("socialservices", "use_on_sites", ""));
	if (!is_array($arUseOnSites))
		$arUseOnSites = Array();
	$arUseOnSites[WIZARD_SITE_ID] = 'Y';
	COption::SetOptionString("socialservices", "use_on_sites", serialize($arUseOnSites));
}

?>