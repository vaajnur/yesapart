<?
$MESS["AUTH_CHANGE_PASSWORD"] = "Cambiando contraseГ±a";
$MESS["AUTH_LOGIN"] = "Inicio de sesiГіn:";
$MESS["AUTH_CHECKWORD"] = "Cadena de verificaciГіn:";
$MESS["AUTH_NEW_PASSWORD"] = "Nueva contraseГ±a:";
$MESS["AUTH_NEW_PASSWORD_CONFIRM"] = "ConfirmciГіn de contraseГ±a:";
$MESS["AUTH_CHANGE"] = "Cambiar contraseГ±a";
$MESS["AUTH_REQ"] = "Campos requeridos";
$MESS["AUTH_AUTH"] = "AutorizaciГіn";
$MESS["AUTH_NEW_PASSWORD_REQ"] = "Nueva contraseГ±a:";
$MESS["AUTH_SECURE_NOTE"] = "La contraseГ±a serГЎ encriptada antes de ser enviada. Esto prevendrГЎ que a contraseГ±a aparezca en un formulario abierto sobre los canales de transmisiГіn de la data.";
$MESS["AUTH_NONSECURE_NOTE"] = "La contraseГ±a serГЎ enviada en un formulario abierto. Habilitar JavaScript en su navegador web para habilitar el encriptamiento de la contraseГ±a.";
$MESS["system_auth_captcha"] = "Introduzca los caracteres que ve en la imagen:";
?>