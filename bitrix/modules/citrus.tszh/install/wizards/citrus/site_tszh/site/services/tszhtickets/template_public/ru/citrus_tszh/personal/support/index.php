<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мои заявки");
?><?$APPLICATION->IncludeComponent("citrus:support.ticket", "request", array(
	"SEF_MODE" => "Y",
	"SEF_FOLDER" => "#SITE_DIR#personal/support/",
	"TICKETS_PER_PAGE" => "50",
	"MESSAGES_PER_PAGE" => "20",
	"SET_PAGE_TITLE" => "Y",
	"EDIT_FIELDS" => array(
		0 => "MESSAGE",
		1 => "CLOSE",
		2 => "",
	),
	"SHOW_FIELDS" => array(
		0 => "OWNER",
		1 => "SOURCE",
		2 => "DATE_CREATE",
		3 => "CREATED",
		4 => "DATE_CLOSE",
		5 => "STATUS",
		6 => "CATEGORY",
		7 => "TIME_TO_SOLVE",
		8 => "CRITICALITY",
		9 => "RESPONSIBLE",
		10 => "",
	),
	"SEF_URL_TEMPLATES" => array(
		"ticket_list" => "index.php",
		"ticket_edit" => "#ID#.php",
	)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>