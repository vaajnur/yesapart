<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!defined("WIZARD_TEMPLATE_ID"))
{
	return;
}

$wizard =& $this->GetWizard();

if ($wizard->GetVar("can_send_updatesinfo") == "Y")
{
	if ($wizard->GetVar("make_group_settings") == "Y")
	{
		$group = new CGroup;
		$arFields['SECURITY_POLICY'] = 'a:14:{s:15:"SESSION_TIMEOUT";s:2:"15";s:15:"SESSION_IP_MASK";s:15:"255.255.255.255";s:13:"MAX_STORE_NUM";s:1:"1";s:13:"STORE_IP_MASK";s:15:"255.255.255.255";s:13:"STORE_TIMEOUT";s:4:"4320";s:17:"CHECKWORD_TIMEOUT";s:2:"60";s:15:"PASSWORD_LENGTH";s:2:"10";s:18:"PASSWORD_UPPERCASE";s:1:"Y";s:18:"PASSWORD_LOWERCASE";s:1:"Y";s:15:"PASSWORD_DIGITS";s:1:"Y";s:20:"PASSWORD_PUNCTUATION";s:1:"Y";s:14:"LOGIN_ATTEMPTS";s:1:"3";s:20:"BLOCK_LOGIN_ATTEMPTS";s:1:"0";s:10:"BLOCK_TIME";s:0:"";}';
		$group->Update(1, $arFields);
	}
	if ($wizard->GetVar("set_error_reporting") == "Y")
	{
		COption::SetOptionInt('main', 'error_reporting', 0);
	}
	if ($wizard->GetVar("set_event") == "Y")
	{
		COption::SetOptionInt('main', 'event_log_cleanup_days', 21);
		COption::SetOptionString('main', 'event_log_logout', 'Y');
		COption::SetOptionString('main', 'event_log_login_success', 'Y');
		COption::SetOptionString('main', 'event_log_login_fail', 'Y');
		COption::SetOptionString('main', 'event_log_register', 'Y');
		COption::SetOptionString('main', 'event_log_register_fail', 'Y');
		COption::SetOptionString('main', 'event_log_password_request', 'Y');
		COption::SetOptionString('main', 'event_log_password_change', 'Y');
		COption::SetOptionString('main', 'event_log_user_edit', 'Y');
		COption::SetOptionString('main', 'event_log_user_delete', 'Y');
		COption::SetOptionString('main', 'event_log_user_groups', 'Y');
		COption::SetOptionString('main', 'event_log_group_policy', 'Y');
		COption::SetOptionString('main', 'event_log_module_access', 'Y');
		COption::SetOptionString('main', 'event_log_file_access', 'Y');
		COption::SetOptionString('main', 'event_log_task', 'Y');
		COption::SetOptionString('main', 'event_log_marketplace', 'Y');
		COption::SetOptionString('main', 'user_profile_history', 'Y');
		COption::SetOptionString('main', 'component_managed_cache_on', 'Y');
		COption::SetOptionString('main', 'component_cache_on', 'Y');
	}


	if ($wizard->GetVar("set_cdn_active") == "Y")
	{
		$siteID = $wizard->GetVar("siteID");
		$serverName = $wizard->GetVar('serverName');

		if (!CModule::IncludeModule("bitrixcloud"))
		{
			$strError = GetMessage("MODULE_INCLUDE_ERROR");
		}
		else
		{
			$cdn_config = CBitrixCloudCDNConfig::getInstance()->loadFromOptions();
			$cdn_config->setSites(array('admin', $siteID));
			$cdn_config->setDomain($serverName);
			$cdn_config->setOptimization(true);
			$cdn_config->setKernelRewrite(true);
			$cdn_config->setContentRewrite(true);
			$cdn_config->saveToOptions();
			CBitrixCloudCDN::SetActive(true, true);
		}
	}
	if ($wizard->GetVar("set_composite_auto") == "Y")
	{
		Bitrix\Main\Composite\Helper::setEnabled(true);
		$compositeOptions = array(
			"AUTO_COMPOSITE" => "Y",
		);
		Bitrix\Main\Composite\Helper::setOptions($compositeOptions);
		Bitrix\Main\Config\Option::set("main", "~show_composite_banner", "N");
	}

	file_put_contents($_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/php_interface/this_site_support.php',
		'<a href="https://otr-soft.ru">otr-soft.ru</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="adm-main-support-link" href="https://otr-soft.ru/techsupport.php#5">������������</a>');

	/*if ($wizard->GetVar("set_cache_auto") == "Y")
	{*/
		function IntOption($name, $def = 0)
		{
			static $CACHE;
			$name .= '_auto';

			if (!$CACHE[$name])
			{
				$CACHE[$name] = COption::GetOptionInt("main", $name, $def);
			}

			return $CACHE[$name];
		}
	//}
	function IntOptionSet($name, $val)
	{
		$val = intval($val);
		COption::SetOptionInt('main', $name . '_auto', $val);
	}



	if ($wizard->GetVar("make_auto_backup") == "Y")
	{
	$strError = "";
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/backup.php");

	$bMcrypt = function_exists('mcrypt_encrypt') || function_exists('openssl_encrypt');
	$bBitrixCloud = $bMcrypt;
	if (!CModule::IncludeModule('bitrixcloud'))
	{
		$bBitrixCloud = false;
		$strError = GetMessage("MODULE_INCLUDE_ERROR");
	}
	elseif (!CModule::IncludeModule('clouds'))
	{
		$bBitrixCloud = false;
		$strError = GetMessage("MODULE_INCLUDE_ERROR");
	}

	if (!$strError)
	{
		$server_name = COption::GetOptionString("main", "server_name", "");
		if (!$server_name)
		{
			$server_name = $_SERVER['HTTP_HOST'];
		}
		$server_name = rtrim($server_name, '/');
		if (!preg_match('/^[a-z0-9\.\-]+$/i', $server_name)) // cyrillic domain hack
		{
			$converter = new CBXPunycode(defined('BX_UTF') && BX_UTF === true ? 'UTF-8' : 'windows-1251');
			$host = $converter->Encode($server_name);
			if (!preg_match('#--p1ai$#', $host)) // trying to guess
			{
				$host = $converter->Encode(\Bitrix\Main\Text\Encoding::convertEncoding($server_name, 'utf-8', 'windows-1251'));
			}
			$server_name = $host;
		}
		$url = (CMain::IsHTTPS() ? "https://" : "http://") . $server_name;


		$dump_auto_set = 2;
		$dump_auto_time = '02:30';
		$dump_auto_interval = 7;
		$dump_encrypt_key = $wizard->GetVar('siteEmail');
		$dump_bucket_id = -1;

		CPasswordStorage::Set('dump_temporary_cache', $dump_encrypt_key);
		IntOptionSet("dump_bucket_id", $dump_bucket_id);

		if ($bBitrixCloud)
		{
			CBitrixCloudBackup::getInstance()->deleteBackupJob();
		}

		$t = preg_match('#^([0-9]{2}):([0-9]{2})$#', $dump_auto_time, $regs) ? $regs[1] * 60 + $regs[2] : 0;
		IntOptionSet("dump_auto_time", $t);
		$sec = $t * 60;

		IntOptionSet("dump_auto_interval", $dump_auto_interval);
		COption::SetOptionInt('main', 'last_backup_start_time', 0);
		COption::SetOptionInt('main', 'last_backup_end_time', 0);

		$start_time = time();
		$min_left = $t - date('H') * 60 - date('i');
		if ($min_left < -60)
		{
			$start_time += 86400;
			$day = 'TOMORROW';
			$w = date('w', time() + 86400);
		}
		else
		{
			$day = 'TODAY';
			$w = date('w');
		}

		// converting time to UTC
		$diff = (CTimeZone::Enabled() ? CTimeZone::GetOffset() : 0) + date('Z');
		$sec -= $diff;
		if ($sec < 0)
		{
			$sec += 86400;
			$w = ($w - 1) % 7;
		}
		elseif ($sec >= 86400)
		{
			$sec -= 86400;
			$w = ($w + 1) % 7;
		}

		switch ($dump_auto_interval)
		{
			case 1:
				$arWeekDays = array(0, 1, 2, 3, 4, 5, 6);
				break;
			case 2:
				if ($w % 2)
				{
					$arWeekDays = array(1, 3, 5);
				}
				else
				{
					$arWeekDays = array(0, 2, 4, 6);
				}
				break;
			case 3:
				$arWeekDays = array($w, ($w + 3) % 7);
				sort($arWeekDays);
				break;
			default: // 7
				$arWeekDays = array($w);
		}

		$backup_secret_key = randString(16);
		CPasswordStorage::Set('backup_secret_key', $backup_secret_key);
		$strError = CBitrixCloudBackup::getInstance()->addBackupJob(
			$backup_secret_key,
			$url,
			$sec,
			$arWeekDays
		);

		if (!$strError)
		{
			$dump_site_id = array();
			$res = CSite::GetList($by = 'sort', $order = 'asc', array('ACTIVE' => 'Y'));
			while ($f = $res->Fetch())
			{
				$root = rtrim($f['ABS_DOC_ROOT'], '/');
				if (is_dir($root))
				{
					$dump_site_id[] = $f['ID'];
				}
			}
			IntOptionSet("dump_auto_enable", $dump_auto_set);
			COption::SetOptionString("main", "dump_site_id" . "_auto", serialize($dump_site_id));
			IntOptionSet("dump_delete_old", '4');
			IntOptionSet("dump_old_cnt", '2');
		}
	}
}

/*
  ��-�� �������� � ������������ ������ php �� ������� � ������������� ������� ���������� ����������� �������� ���������������
	if ($wizard->GetVar("make_backup") == "Y")
	{
		$command = 'php -f ' . $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/tools/backup.php';
		if (substr(php_uname(), 0, 7) == "Windows"){
			pclose(popen("start /B ". $command, "r"));
		}
		else {
			exec($command . " > /dev/null &");
		}

	}
*/

}
