<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "������� �������� �������");
$APPLICATION->SetTitle("�������-�������");

if ($_REQUEST['success'] != 1):

?><?$APPLICATION->IncludeComponent(
	"citrus:support.faq",
	"qa",
	Array(
		"IBLOCK_TYPE" => "services",
		"IBLOCK_ID" => "#QUESTIONS_IBLOCK_ID#",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600000",
		"AJAX_MODE" => "N",
		"SEF_MODE" => "Y",
		"SECTION" => "-",
		"EXPAND_LIST" => "Y",
		"AJAX_OPTION_SHADOW" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"SEF_FOLDER" => "#SITE_DIR#questions-and-answers/",
		"SEF_URL_TEMPLATES" => Array(
			"section" => "#SECTION_ID#/",
			"detail" => "#SECTION_ID#/#ELEMENT_ID#/"
		),
		"VARIABLE_ALIASES" => Array(
			"section" => Array(),
			"detail" => Array(),
		)
	)
);?><?

endif;

?>
<a name="ask"></a>
<h2>������ <em>�������</em></h2>
<?$APPLICATION->IncludeComponent("citrus:iblock.element.add.form", ".default", array(
	"IBLOCK_TYPE" => "services",
	"IBLOCK_ID" => "#QUESTIONS_IBLOCK_ID#",
	"STATUS_NEW" => "ANY",
	"LIST_URL" => "",
	"USE_CAPTCHA" => "Y",
	"USER_MESSAGE_EDIT" => "������, ���� ��������� ��������.<br />����� �������: #ID#, �� ������ ��������� ���� ������ �� ��� <a href=\"#SITE_DIR#questions-and-answers/?check=#ID#\">����������</a>",
	"USER_MESSAGE_ADD" => "������, ���� ��������� ��������.<br />����� �������: #ID#, �� ������ ��������� ���� ������ �� ��� <a href=\"#SITE_DIR#questions-and-answers/?check=#ID#\">����������</a>",
	"DEFAULT_INPUT_SIZE" => "40",
	"RESIZE_IMAGES" => "N",
	"PROPERTY_CODES" => array(
		0 => "NAME",
		1 => "PREVIEW_TEXT",
		2 => '#PROPERTY_author_address#',
		3 => '#PROPERTY_author_phone#',
		4 => '#PROPERTY_author_email#',
	),
	"PROPERTY_CODES_REQUIRED" => array(
		0 => "NAME",
	),
	"PROPERTY_CODES_EMAIL" => array(
		0 => "#PROPERTY_author_email#",
	),
	"CHECK_EMAIL" => "Y",
	"GROUPS" => array(
		0 => "2",
	),
	"STATUS" => "INACTIVE",
	"ELEMENT_ASSOC" => "CREATED_BY",
	"MAX_USER_ENTRIES" => "100000",
	"MAX_LEVELS" => "100000",
	"LEVEL_LAST" => "N",
	"MAX_FILE_SIZE" => "0",
	"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
	"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
	"SEF_MODE" => "N",
	"SEF_FOLDER" => "#SITE_DIR#questions-and-answers/148/",
	"CUSTOM_TITLE_NAME" => "��������, ���� �����",
	"CUSTOM_TITLE_TAGS" => "",
	"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
	"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
	"CUSTOM_TITLE_IBLOCK_SECTION" => "",
	"CUSTOM_TITLE_PREVIEW_TEXT" => "���� �������",
	"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
	"CUSTOM_TITLE_DETAIL_TEXT" => "",
	"CUSTOM_TITLE_DETAIL_PICTURE" => ""
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>