<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

$wizard =& $this->GetWizard();

if (tszhCheckMinEdition('smallbusiness'))
{
	if (IsModuleInstalled('vdgb.tszhepasport'))
	{
        $sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH . "/public/" . LANGUAGE_ID . "/epassports/");
        $destPath = str_replace("//", "/", WIZARD_SITE_PATH . '/epassports/');
        CopyDirFiles($sourcePath, $destPath, true, true);
	}

    // ���� ��������������� ���������� ������
    if (isAdaptiveTszhTemplate())
    {
        $connectOrchidComponentEpassport = <<<PHP
<?\$APPLICATION->IncludeComponent(
	"vdgb:tszhepasport", 
	"orchid_default",
	array(
		"CACHE_TIME" => "300",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => ".default",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FILE" => "",
		"MAPCONTROLS" => array(
			0 => "searchControl",
			1 => "trafficControl",
			2 => "zoomControl",
			3 => "typeSelector",
			4 => "rulerControl",
			5 => "fullscreenControl",
		),
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "�������",
		"YANDEX_CLUSTERIZATION" => "Y"
	),
	false
);?>
PHP;
        CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"] . "/epassports/index.php", array("CONNECT_COMPONENT_EPASSPORT" => $connectOrchidComponentEpassport));

        WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/epassports/.show_add.menu.php", Array(
            GetMessage("F_TSZH_EPASPORTS_SHOW_ADD_MENUITEM"),
            WIZARD_SITE_DIR . 'epassports',
            Array(),
            Array(),
            "",
        ), false, 0);
    }
// ���� ��������������� ������ ������
    else
    {
        $connectLotosComponentEpassport = <<<PHP
<?\$APPLICATION->IncludeComponent(
	"vdgb:tszhepasport", 
	".default",
	array(
		"CACHE_TIME" => "300",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => ".default",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FILE" => "",
		"MAPCONTROLS" => array(
			0 => "searchControl",
			1 => "trafficControl",
			2 => "zoomControl",
			3 => "typeSelector",
			4 => "rulerControl",
			5 => "fullscreenControl",
		),
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "�������",
		"YANDEX_CLUSTERIZATION" => "Y"
	),
	false
);?>
PHP;
        CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"] . "/epassports/index.php", array("CONNECT_COMPONENT_EPASSPORT" => $connectLotosComponentEpassport));

        WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/documents/.show_add.menu.php", Array(
            GetMessage("F_TSZH_EPASPORTS_MENUITEM"),
            WIZARD_SITE_DIR . 'epassports/',
            Array(),
            Array(),
            "",
        ), false, -1);
    }
}
