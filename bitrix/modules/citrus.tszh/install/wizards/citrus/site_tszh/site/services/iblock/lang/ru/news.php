<?
$MESS["WZD_OPTION_NEWS_1"] = "Новость";
$MESS["WZD_OPTION_NEWS_2"] = "Публикуется на сайте";
$MESS["WZD_OPTION_NEWS_3"] = "Дата новости";
$MESS["WZD_OPTION_NEWS_4"] = "*Заголовок";
$MESS["WZD_OPTION_NEWS_5"] = "Источник";
$MESS["WZD_OPTION_NEWS_6"] = "Краткое описание";
$MESS["WZD_OPTION_NEWS_7"] = "Полное описание";
$MESS["WZD_OPTION_NEWS_8"] = "Картинка";
$MESS["WZD_OPTION_NEWS_9"] = "Теги";
$MESS["WZD_OPTION_NEWS_10"] = "Свойства";
$MESS["WZD_OPTION_NEWS_11"] = "Заголовок окна браузера";
$MESS["WZD_OPTION_NEWS_12"] = "Ключевые слова";
$MESS["WZD_OPTION_NEWS_13"] = "Описание";
$MESS["WZD_OPTION_NEWS_14"] = "Показывать только зарегистрированным пользователям";
$MESS["WZD_OPTION_NEWS_15"] = "Выполнить рассылку";
