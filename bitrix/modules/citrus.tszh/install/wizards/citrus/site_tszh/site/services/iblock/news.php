<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if(!CModule::IncludeModule("iblock"))
	return;

if(COption::GetOptionString("citrus.tszh", "wizard_installed", "N", WIZARD_SITE_ID) == "Y")
	return;

$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/news-news.xml";
$iblockCode = "news_".WIZARD_SITE_ID;
$iblockType = "news";

$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType, "SITE_ID" => WIZARD_SITE_ID));
$iblockID = false;
if ($arIBlock = $rsIBlock->Fetch())
{
	$iblockID = (int)$arIBlock["ID"];
	if (WIZARD_REINSTALL_DATA)
	{
		CIBlock::Delete($arIBlock["ID"]);
		$iblockID = false;
	}
}

if($iblockID == false)
{
	$iblockID = WizardServices::ImportIBlockFromXML(
		$iblockXMLFile,
		"news",
		$iblockType,
		WIZARD_SITE_ID,
		$permissions = Array(
			"1" => "X",
			"2" => "R"
		)
	);

	if ($iblockID < 1)
		return;

	//IBlock fields
	$iblock = new CIBlock;

	$arFields = Array(
		"ACTIVE" => "Y",
		"FIELDS" => array ( 'IBLOCK_SECTION' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'ACTIVE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'Y', ), 'ACTIVE_FROM' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '=now', ), 'ACTIVE_TO' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SORT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '0', ), 'NAME' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '', ), 'PREVIEW_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'FROM_DETAIL' => 'Y', 'SCALE' => 'Y', 'WIDTH' => 150, 'HEIGHT' => 150, 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, 'DELETE_WITH_DETAIL' => 'N', 'UPDATE_WITH_DETAIL' => 'N', 'USE_WATERMARK_TEXT' => 'N', 'WATERMARK_TEXT' => '', 'WATERMARK_TEXT_FONT' => '', 'WATERMARK_TEXT_COLOR' => '', 'WATERMARK_TEXT_SIZE' => '', 'WATERMARK_TEXT_POSITION' => 'tl', 'USE_WATERMARK_FILE' => 'N', 'WATERMARK_FILE' => '', 'WATERMARK_FILE_ALPHA' => '', 'WATERMARK_FILE_POSITION' => 'tl', 'WATERMARK_FILE_ORDER' => NULL, ), ), 'PREVIEW_TEXT_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'PREVIEW_TEXT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'DETAIL_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'SCALE' => 'Y', 'WIDTH' => 400, 'HEIGHT' => 400, 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, 'USE_WATERMARK_TEXT' => 'N', 'WATERMARK_TEXT' => '', 'WATERMARK_TEXT_FONT' => '', 'WATERMARK_TEXT_COLOR' => '', 'WATERMARK_TEXT_SIZE' => '', 'WATERMARK_TEXT_POSITION' => 'tl', 'USE_WATERMARK_FILE' => 'N', 'WATERMARK_FILE' => '', 'WATERMARK_FILE_ALPHA' => '', 'WATERMARK_FILE_POSITION' => 'tl', 'WATERMARK_FILE_ORDER' => NULL, ), ), 'DETAIL_TEXT_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'html', ), 'DETAIL_TEXT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'XML_ID' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'CODE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'UNIQUE' => 'N', 'TRANSLITERATION' => 'N', 'TRANS_LEN' => 100, 'TRANS_CASE' => '', 'TRANS_SPACE' => false, 'TRANS_OTHER' => false, 'TRANS_EAT' => 'Y', 'USE_GOOGLE' => 'N', ), ), 'TAGS' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_NAME' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '', ), 'SECTION_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'FROM_DETAIL' => 'N', 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => '', 'COMPRESSION' => '', 'DELETE_WITH_DETAIL' => 'N', 'UPDATE_WITH_DETAIL' => 'N', 'USE_WATERMARK_TEXT' => 'N', 'WATERMARK_TEXT' => '', 'WATERMARK_TEXT_FONT' => '', 'WATERMARK_TEXT_COLOR' => '', 'WATERMARK_TEXT_SIZE' => '', 'WATERMARK_TEXT_POSITION' => 'tl', 'USE_WATERMARK_FILE' => 'N', 'WATERMARK_FILE' => '', 'WATERMARK_FILE_ALPHA' => '', 'WATERMARK_FILE_POSITION' => 'tl', 'WATERMARK_FILE_ORDER' => NULL, ), ), 'SECTION_DESCRIPTION_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'SECTION_DESCRIPTION' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_DETAIL_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => '', 'COMPRESSION' => '', 'USE_WATERMARK_TEXT' => 'N', 'WATERMARK_TEXT' => '', 'WATERMARK_TEXT_FONT' => '', 'WATERMARK_TEXT_COLOR' => '', 'WATERMARK_TEXT_SIZE' => '', 'WATERMARK_TEXT_POSITION' => 'tl', 'USE_WATERMARK_FILE' => 'N', 'WATERMARK_FILE' => '', 'WATERMARK_FILE_ALPHA' => '', 'WATERMARK_FILE_POSITION' => 'tl', 'WATERMARK_FILE_ORDER' => NULL, ), ), 'SECTION_XML_ID' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_CODE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'UNIQUE' => 'N', 'TRANSLITERATION' => 'N', 'TRANS_LEN' => 100, 'TRANS_CASE' => '', 'TRANS_SPACE' => false, 'TRANS_OTHER' => false, 'TRANS_EAT' => 'Y', 'USE_GOOGLE' => 'N', ), ), 'LOG_SECTION_ADD' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => NULL, ), 'LOG_SECTION_EDIT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => NULL, ), 'LOG_SECTION_DELETE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => NULL, ), 'LOG_ELEMENT_ADD' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => NULL, ), 'LOG_ELEMENT_EDIT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => NULL, ), 'LOG_ELEMENT_DELETE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => NULL, ), 'XML_IMPORT_START_TIME' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => NULL, 'VISIBLE' => 'N', ), ),
		"CODE" => "news",
		"XML_ID" => $iblockCode,
		//"NAME" => "[".WIZARD_SITE_ID."] ".$iblock->GetArrayByID($iblockID, "NAME")
	);

	$iblock->Update($iblockID, $arFields);
}

$dbSite = CSite::GetByID(WIZARD_SITE_ID);
if($arSite = $dbSite -> Fetch())
	$lang = $arSite["LANGUAGE_ID"];
if(strlen($lang) <= 0)
	$lang = "ru";

WizardServices::IncludeServiceLang("news.php", $lang);

$arFormFields = array (
  'tabs' => 'edit1--#--' . GetMessage("WZD_OPTION_NEWS_1") . '--,--ACTIVE--#--' . GetMessage("WZD_OPTION_NEWS_2") . '--,--ACTIVE_FROM--#--' . GetMessage("WZD_OPTION_NEWS_3") . '--,--NAME--#--' . GetMessage("WZD_OPTION_NEWS_4") . '--,--PROPERTY_SOURCE--#--' . GetMessage("WZD_OPTION_NEWS_5") . '--,--PREVIEW_TEXT--#--' . GetMessage("WZD_OPTION_NEWS_6") . '--,--DETAIL_TEXT--#--' . GetMessage("WZD_OPTION_NEWS_7") . '--,--DETAIL_PICTURE--#--' . GetMessage("WZD_OPTION_NEWS_8") . '--,--TAGS--#--' . GetMessage("WZD_OPTION_NEWS_9") . '--,--edit1_csection1--#----' . GetMessage("WZD_OPTION_NEWS_10") . '--,--PROPERTY_BROWSER_TITLE--#--' . GetMessage("WZD_OPTION_NEWS_11") . '--,--PROPERTY_KEYWORDS--#--' . GetMessage("WZD_OPTION_NEWS_12") . '--,--PROPERTY_DESCRIPTION--#--' . GetMessage("WZD_OPTION_NEWS_13") . '--,--PROPERTY_citrusTszhSubscribeSend--#--' . GetMessage("WZD_OPTION_NEWS_15") . '--;--',
);

$rsProperties = CIBlockProperty::GetList(Array(), Array(
	"IBLOCK_ID" => $iblockID,
));
while ($arProperty = $rsProperties->Fetch()) {
	if (strlen($arProperty["CODE"]) > 0)
		$arFormFields['tabs'] = str_ireplace('PROPERTY_' . $arProperty["CODE"], 'PROPERTY_' . $arProperty["ID"], $arFormFields['tabs']);
}
CUserOptions::SetOption("form", "form_element_".$iblockID, $arFormFields);
CUserOptions::SetOption("list", "tbl_iblock_list_".md5($iblockType.".".$iblockID), array (
  'columns' => 'NAME,ACTIVE,DATE_ACTIVE_FROM,TIMESTAMP_X,ID',
  'by' => 'date_active_from',
  'order' => 'desc',
  'page_size' => '20',
));

CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/news/index.php", array("NEWS_IBLOCK_ID" => $iblockID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/sect_right.php", array("NEWS_IBLOCK_ID" => $iblockID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/index_inc.php", array("NEWS_IBLOCK_ID" => $iblockID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/_index.php", array("NEWS_IBLOCK_ID" => $iblockID));

COption::SetOptionInt('citrus.tszh', "news.iblock", $iblockID, "", WIZARD_SITE_ID);

\Citrus\Tszh\Wizard\Seo::setIblockSettings($iblockID, true, true, false, true);