<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!CModule::IncludeModule("citrus.tszh"))
{
	return;
}

/*if(COption::GetOptionString("citrus.tszh", "wizard_tszh_installed", "N", WIZARD_SITE_ID) == "Y")
	return;*/

$wizard =& $this->GetWizard();

$dbSite = CSite::GetByID(WIZARD_SITE_ID);
$arSite = $dbSite->Fetch();
if ($arSite)
{
	$lang = $arSite["LANGUAGE_ID"];
}
if (strlen($lang) <= 0)
{
	$lang = "ru";
}

WizardServices::IncludeServiceLang("step.php", $lang);

$arUserTypes = Array(
	array(
		'ENTITY_ID' => 'TSZH_ACCOUNT',
		'FIELD_NAME' => 'UF_REGCODE',
		'USER_TYPE_ID' => 'string',
		'XML_ID' => '',
		'SORT' => '100',
		'MULTIPLE' => 'N',
		'MANDATORY' => 'N',
		'SHOW_FILTER' => 'E',
		'SHOW_IN_LIST' => 'Y',
		'EDIT_IN_LIST' => 'Y',
		'IS_SEARCHABLE' => 'N',
		'SETTINGS' =>
			array(
				'SIZE' => 30,
				'ROWS' => 1,
				'REGEXP' => '',
				'MIN_LENGTH' => 0,
				'MAX_LENGTH' => 0,
				'DEFAULT_VALUE' => '',
			),
		'EDIT_FORM_LABEL' => Array(
			$lang => GetMessage('TSZH_SERVICE_REGCODE'),
		),
		'LIST_COLUMN_LABEL' => Array(
			$lang => GetMessage('TSZH_SERVICE_REGCODE2'),
		),
		'LIST_FILTER_LABEL' => Array(
			$lang => GetMessage('TSZH_SERVICE_REGCODE2'),
		),
	),
	array(
		'ENTITY_ID' => 'TSZH_ACCOUNT',
		'FIELD_NAME' => 'UF_DEBTORS_EXCLUDED',
		'USER_TYPE_ID' => 'boolean',
		'XML_ID' => '',
		'SORT' => '100',
		'MULTIPLE' => 'N',
		'MANDATORY' => 'N',
		'SHOW_FILTER' => 'E',
		'SHOW_IN_LIST' => 'Y',
		'EDIT_IN_LIST' => 'Y',
		'IS_SEARCHABLE' => 'N',
		'SETTINGS' =>
			array(
				'DEFAULT_VALUE' => 'N',
			),
		'EDIT_FORM_LABEL' => Array(
			$lang => GetMessage('UF_DEBTORS_EXCLUDED'),
		),
		'LIST_COLUMN_LABEL' => Array(
			$lang => GetMessage('UF_DEBTORS_EXCLUDED2'),
		),
		'LIST_FILTER_LABEL' => Array(
			$lang => GetMessage('UF_DEBTORS_EXCLUDED2'),
		),
	),
);
foreach ($arUserTypes as $arUserTypeEntity)
{
	$arExistingUserType = CUserTypeEntity::GetList(Array(), Array(
		"ENTITY_ID" => $arUserTypeEntity["ENTITY_ID"],
		"FIELD_NAME" => $arUserTypeEntity["FIELD_NAME"],
	))->Fetch();
	$userTypeEntity = new CUserTypeEntity();
	if (is_array($arExistingUserType))
	{
		$bSuccess = $userTypeEntity->Update($arExistingUserType['ID'], $arUserTypeEntity);
	}
	else
	{
		$bSuccess = $userTypeEntity->Add($arUserTypeEntity) > 0;
	}
	if (!$bSuccess)
	{
		$strError = GetMessage("TSZH_SERIVCE_ERROR");
		if ($ex = $APPLICATION->GetException())
		{
			$strError .= ': ' . $ex->GetString();
		}
		ShowError($strError);
		die();
	}
}

$arVars = Array(
	"org_title" => 'NAME',
    "org_type" => 'ORGANIZATION_TYPE',
	"org_inn" => "INN",
	"org_kpp" => "KPP",
	"org_rsch" => "RSCH",
	"org_bank" => "BANK",
	"org_ksch" => "KSCH",
	"org_bik" => "BIK",
	"org_kbk" => "KBK",
	"org_oktmo" => "OKTMO",
	"org_is_budget" => "IS_BUDGET",
);

$arFields = Array(
	"NAME" => GetMessage("TSZH_NAME_DEFAULT"),
	"SITE_ID" => WIZARD_SITE_ID,
	"ADDRESS" => $wizard->GetVar("siteAddress"),
	"LEGAL_ADDRESS" => $wizard->GetVar("LEGAL_ADDRESS"),
	"PHONE" => $wizard->GetVar("siteTelephone"),
	"PHONE_DISP" => $wizard->GetVar("siteTelephoneDisp"),
	"MONETA_ENABLED" => $wizard->GetVar("MONETA_ENABLED"),
	"MONETA_OFFER" => $wizard->GetVar("MONETA_OFFER"),
	"MONETA_NO_OFFER" => $wizard->GetVar("MONETA_NO_OFFER"),
	"MONETA_EMAIL" => $wizard->GetVar("MONETA_EMAIL"),
	"HEAD_NAME" => $wizard->GetVar("HEAD_NAME"),
	"OFFICE_HOURS" => $wizard->GetVar("OFFICE_HOURS"),
	"EMAIL" => $wizard->GetVar("siteEmail"),
	"RECEIPT_TEMPLATE" => $wizard->GetVar("RECEIPT_TEMPLATE"),
);
foreach ($arVars as $strVar => $field)
{
	$value = $wizard->GetVar($strVar);
	COption::SetOptionString("citrus.tszh", $strVar, $value);
	if (strlen($value) > 0)
	{
		$arFields[$field] = $value;
	}
}

unset($_SESSION[$wizard->solutionName]["demoTszhID"]);
unset($_SESSION[$wizard->solutionName]["importError"]);
if ($arTszh = CTszh::GetList(Array(), Array("SITE_ID" => WIZARD_SITE_ID))->Fetch())
{
	$tszhID = $arTszh["ID"];
	CTszh::Update($tszhID, $arFields);
}
else
{
	$tszhID = CTszh::Add($arFields);
	if (intval($tszhID) > 0)
	{
		$_SESSION[$wizard->solutionName]['demoTszhID'] = $tszhID;
	}
}

// �������������� ����������� � ������� ������.�� � ��������� � �����������
if (\Bitrix\Main\Loader::includeModule('citrus.tszhpayment')
	&& $arFields['MONETA_ENABLED'] == 'Y'
	&& $arFields['MONETA_NO_OFFER'] == 'Y'
	&& method_exists('CTszhPaymentGateway', 'registerMonetaProfile'))
{
	$registerRequest = \CTszhPaymentGateway::getInstance()->registerMonetaProfile($tszhID);
	// todo �������� ������ � ������ �������
}

// download demo data
if ($wizard->GetVar("download_demodata") == "Y")
{
	$demo = new \Citrus\Tszh\Demodata();
	try
	{
		$demo->installForTszh($tszhID);
	}
	catch (Exception $e)
	{
		ShowError($e->getMessage());
		die();
	}
}

if (!isAdaptiveTszhTemplate())
{
	$acc_choice = <<<PHP
<?
\$APPLICATION->IncludeComponent("citrus:tszh.account.choice","",	Array(),false);
?>
PHP;

	$confirm_acc = <<<PHP
<?
\$APPLICATION->IncludeComponent("citrus:tszh.account.confirm","lotos_default",	Array(),false);
?>
PHP;

	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/personal/sect_left-menu-inc.php", array("ACCOUNT_CHOISE" => $acc_choice));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/personal/confirm-account/index.php", array("ACCOUNT_CONFIRM" => $confirm_acc));
}else{
	$confirm_acc_adaptive = <<<PHP
<?
\$APPLICATION->IncludeComponent("citrus:tszh.confirm_adaptive",".default",	Array(),false);
?>
PHP;
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/personal/sect_left-menu-inc.php", array("ACCOUNT_CHOISE" => ""));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/personal/confirm-account/index.php", array("ACCOUNT_CONFIRM" => $confirm_acc_adaptive));
}
WizardServices::SetFilePermission(WIZARD_SITE_DIR . 'personal', Array(
	COption::GetOptionString("citrus.tszh", "user_group_id", 3, WIZARD_SITE_ID) => 'R',
	'*' => 'D',
));
WizardServices::SetFilePermission(WIZARD_SITE_DIR . 'personal/confirm-account', Array('2' => 'R'));
//WizardServices::IncludeServiceLang("type.php", $languageID);
$arReceiptTmplate = explode("/", $wizard->GetVar("RECEIPT_TEMPLATE"));
$receiptComponentTemplate = strval(array_pop($arReceiptTmplate));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "/personal/receipt/index.php", array("RECEIPT_COMPONENT_TEMPLATE" => $receiptComponentTemplate));

if (tszhCheckMinEdition('standard'))
{
	// copy payment_receive.php and /images
	$sourceDir = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH . "/public/" . LANGUAGE_ID . "/");
	$destDir = str_replace("//", "/", WIZARD_SITE_PATH . "/");
	CopyDirFiles($sourceDir, $destDir, true, true);
}

COption::SetOptionString("citrus.tszh", "wizard_tszh_installed", "Y", false, WIZARD_SITE_ID);

?>