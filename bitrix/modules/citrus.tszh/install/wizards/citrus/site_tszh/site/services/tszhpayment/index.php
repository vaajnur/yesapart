<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!CModule::IncludeModule("citrus.tszhpayment"))
{
	return;
}

$wizard =& $this->GetWizard();

// copy files
$sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH . "/public/" . LANGUAGE_ID . "/");
$destPath = WIZARD_SITE_PATH;
CopyDirFiles($sourcePath, $destPath, true, true);

if (isAdaptiveTszhTemplate()) {
    $connectOrchidComponentPayment = <<<PHP
<?
\$APPLICATION->IncludeComponent(
    "citrus:tszh.payment",
    "orchid_default",
    Array(
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600"
    ),
    false
);
?>
PHP;
    CWizardUtil::ReplaceMacros($_SERVER['DOCUMENT_ROOT'] . "/personal/payment/index.php", array("CONNECT_COMPONENT_PAYMENT" => $connectOrchidComponentPayment));
} else {
    $connectLotosComponentPayment = <<<PHP
<?
\$APPLICATION->IncludeComponent(
            "citrus:tszh.payment",
            ".default",
            Array(
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600"
            ),
            false
        );
?>
PHP;
    CWizardUtil::ReplaceMacros($_SERVER['DOCUMENT_ROOT'] . "/personal/payment/index.php", array("CONNECT_COMPONENT_PAYMENT" => $connectLotosComponentPayment));
}

//Add menu items
WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/.section.menu.php", Array(
	GetMessage("F_TSZH_PAYMENT_MENUITEM"),
	WIZARD_SITE_DIR . "personal/payment/",
	Array(),
	Array("class"=>"pay_utilities leftmenu__img-payment"),
	""
), WIZARD_SITE_ID, -1);

//Add menu items
WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/personal/.section.menu.php", Array(
	GetMessage("F_TSZH_PAYMENT_MENUITEM_PERSONAL"),
	WIZARD_SITE_DIR . "personal/payment/",
	Array(),
	Array("class"=>"pay_utilities leftmenu__img-payment"),
	""
), WIZARD_SITE_ID, -1);
WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/personal/.show_add.menu.php", Array(
	GetMessage("F_TSZH_PAYMENT_MENUITEM_PERSONAL"),
	WIZARD_SITE_DIR . "personal/payment/",
	Array(),
	Array("class" => "pay_utilities"),
	"",
), WIZARD_SITE_ID, -1);
WizardServices::SetFilePermission(WIZARD_SITE_DIR . "/personal/payment/", Array("*" => "R"));


if (COption::GetOptionString("citrus.tszhpayment", "wizard_installed", "N", WIZARD_SITE_ID) == "Y")
{
	return;
}


//WizardServices::IncludeServiceLang("type.php", $languageID);

$demoTszhID = intval($_SESSION[$wizard->solutionName]['demoTszhID']);
if ($demoTszhID > 0 && !CTszhFunctionalityController::isUkrainianVersion())
{
	/*	$arPaySystems = Array(
			Array(
				'CURRENCY' => 'RUB',
				'NAME' => GetMessage("TP_SBERBANK"),
				'ACTIVE' => 'Y',
				'SORT' => '200',
				'DESCRIPTION' => '<img width="190" height="49" src="' . WIZARD_SITE_DIR . 'include/sberbank.png" title="' . GetMessage("TP_SBERBANK") . '" /><br />' . GetMessage("TP_SBERBANK_TEXT") . '<br />',
				'ACTION_FILE' => '/bitrix/modules/citrus.tszhpayment/ru/payment/sberbank_new',
				'RESULT_FILE' => NULL,
				'NEW_WINDOW' => 'Y',
				'PARAMS' => serialize(array(
					'COMPANY_NAME' => array('TYPE' => 'TSZH', 'VALUE' => 'NAME'),
					'INN' => array('TYPE' => 'TSZH', 'VALUE' => 'INN'),
					'KPP' => array('TYPE' => 'TSZH', 'VALUE' => 'KPP'),
					'SETTLEMENT_ACCOUNT' => array('TYPE' => 'TSZH', 'VALUE' => 'RSCH'),
					'BANK_NAME' => array('TYPE' => 'TSZH', 'VALUE' => 'BANK'),
					'BANK_BIC' => array('TYPE' => 'TSZH', 'VALUE' => 'BIK'),
					'BANK_COR_ACCOUNT' => array('TYPE' => 'TSZH', 'VALUE' => 'KSCH'),
					'ORDER_ID' => array('TYPE' => 'PAYMENT', 'VALUE' => 'ID'),
					'DATE_INSERT' => array('TYPE' => 'PAYMENT', 'VALUE' => 'DATE_INSERT'),
					'PAYER_CONTACT_PERSON' => array('TYPE' => 'USER', 'VALUE' => 'FULL_NAME'),
					'PAYER_ZIP_CODE' => array('TYPE' => 'USER', 'VALUE' => 'PERSONAL_ZIP'),
					'PAYER_COUNTRY' => array('TYPE' => 'USER', 'VALUE' => 'PERSONAL_COUNTRY'),
					'PAYER_CITY' => array('TYPE' => 'USER', 'VALUE' => 'PERSONAL_CITY'),
					'PAYER_ADDRESS_FACT' => array('TYPE' => 'ACCOUNT', 'VALUE' => 'FULL_ADDRESS'),
					'SHOULD_PAY' => array('TYPE' => 'PAYMENT', 'VALUE' => 'SHOULD_PAY')
				)),
				'HAVE_PAYMENT' => 'Y',
				'HAVE_ACTION' => 'N',
				'HAVE_RESULT' => 'N',
				'HAVE_PREPAY' => 'N',
				'HAVE_RESULT_RECEIVE' => 'N',
				'ENCODING' => 'windows-1251',
			),
		);

		foreach ($arPaySystems as $arPaySystem) {
			$arPaySystem["LID"] = WIZARD_SITE_ID;
			$arPaySystem["TSZH_ID"] = $demoTszhID;
			CTszhPaySystem::Add($arPaySystem);
		}
	*/
	CTszhPaySystem::fillPredefinedPaySystems($demoTszhID);
}


COption::SetOptionString("citrus.tszhpayment", "wizard_installed", "Y", false, WIZARD_SITE_ID);

?>