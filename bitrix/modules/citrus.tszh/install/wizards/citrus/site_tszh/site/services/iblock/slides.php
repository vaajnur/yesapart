<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!CModule::IncludeModule("iblock"))
{
	return;
}

$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH . "/xml/" . LANGUAGE_ID . "/services-slides.xml";
$iblockCode = "slides_" . WIZARD_SITE_ID;
$iblockType = "services";

$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType, "SITE_ID" => WIZARD_SITE_ID));
$iblockID = false;
if ($arIBlock = $rsIBlock->Fetch())
{
	$iblockID = (int)$arIBlock["ID"];
	if (WIZARD_REINSTALL_DATA)
	{
		CIBlock::Delete($arIBlock["ID"]);
		$iblockID = false;
	}
}

if ($iblockID == false)
{
	$iblockID = WizardServices::ImportIBlockFromXML(
		$iblockXMLFile,
		"slides",
		$iblockType,
		WIZARD_SITE_ID,
		$permissions = Array(
			"1" => "X",
			"2" => "R",
		)
	);

	if ($iblockID < 1)
	{
		return;
	}

	//IBlock fields
	$iblock = new CIBlock;

	$arFields = Array(
		"ACTIVE" => "Y",
		"FIELDS" => array(
			'IBLOCK_SECTION' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '',),
			'ACTIVE' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'Y',),
			'ACTIVE_FROM' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '',),
			'ACTIVE_TO' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '',),
			'SORT' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '0',),
			'NAME' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '',),
			'PREVIEW_PICTURE' => array(
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => array(
					'FROM_DETAIL' => 'N',
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => 'resample',
					'COMPRESSION' => 95,
					'DELETE_WITH_DETAIL' => 'N',
					'UPDATE_WITH_DETAIL' => 'N',
					'USE_WATERMARK_TEXT' => 'N',
					'WATERMARK_TEXT' => '',
					'WATERMARK_TEXT_FONT' => '',
					'WATERMARK_TEXT_COLOR' => '',
					'WATERMARK_TEXT_SIZE' => '',
					'WATERMARK_TEXT_POSITION' => 'tl',
					'USE_WATERMARK_FILE' => 'N',
					'WATERMARK_FILE' => '',
					'WATERMARK_FILE_ALPHA' => '',
					'WATERMARK_FILE_POSITION' => 'tl',
					'WATERMARK_FILE_ORDER' => null,
				),
			),
			'PREVIEW_TEXT_TYPE' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text',),
			'PREVIEW_TEXT' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '',),
			'DETAIL_PICTURE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array(
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => 'resample',
					'COMPRESSION' => 95,
					'USE_WATERMARK_TEXT' => 'N',
					'WATERMARK_TEXT' => '',
					'WATERMARK_TEXT_FONT' => '',
					'WATERMARK_TEXT_COLOR' => '',
					'WATERMARK_TEXT_SIZE' => '',
					'WATERMARK_TEXT_POSITION' => 'tl',
					'USE_WATERMARK_FILE' => 'N',
					'WATERMARK_FILE' => '',
					'WATERMARK_FILE_ALPHA' => '',
					'WATERMARK_FILE_POSITION' => 'tl',
					'WATERMARK_FILE_ORDER' => null,
				),
			),
			'DETAIL_TEXT_TYPE' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text',),
			'DETAIL_TEXT' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '',),
			'XML_ID' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '',),
			'CODE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array(
					'UNIQUE' => 'N',
					'TRANSLITERATION' => 'N',
					'TRANS_LEN' => 100,
					'TRANS_CASE' => 'L',
					'TRANS_SPACE' => '-',
					'TRANS_OTHER' => '-',
					'TRANS_EAT' => 'Y',
					'USE_GOOGLE' => 'N',
				),
			),
			'TAGS' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '',),
			'SECTION_NAME' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '',),
			'SECTION_PICTURE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array(
					'FROM_DETAIL' => 'N',
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => 'resample',
					'COMPRESSION' => 95,
					'DELETE_WITH_DETAIL' => 'N',
					'UPDATE_WITH_DETAIL' => 'N',
					'USE_WATERMARK_TEXT' => 'N',
					'WATERMARK_TEXT' => '',
					'WATERMARK_TEXT_FONT' => '',
					'WATERMARK_TEXT_COLOR' => '',
					'WATERMARK_TEXT_SIZE' => '',
					'WATERMARK_TEXT_POSITION' => 'tl',
					'USE_WATERMARK_FILE' => 'N',
					'WATERMARK_FILE' => '',
					'WATERMARK_FILE_ALPHA' => '',
					'WATERMARK_FILE_POSITION' => 'tl',
					'WATERMARK_FILE_ORDER' => null,
				),
			),
			'SECTION_DESCRIPTION_TYPE' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text',),
			'SECTION_DESCRIPTION' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '',),
			'SECTION_DETAIL_PICTURE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array(
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => 'resample',
					'COMPRESSION' => 95,
					'USE_WATERMARK_TEXT' => 'N',
					'WATERMARK_TEXT' => '',
					'WATERMARK_TEXT_FONT' => '',
					'WATERMARK_TEXT_COLOR' => '',
					'WATERMARK_TEXT_SIZE' => '',
					'WATERMARK_TEXT_POSITION' => 'tl',
					'USE_WATERMARK_FILE' => 'N',
					'WATERMARK_FILE' => '',
					'WATERMARK_FILE_ALPHA' => '',
					'WATERMARK_FILE_POSITION' => 'tl',
					'WATERMARK_FILE_ORDER' => null,
				),
			),
			'SECTION_XML_ID' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '',),
			'SECTION_CODE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array(
					'UNIQUE' => 'N',
					'TRANSLITERATION' => 'N',
					'TRANS_LEN' => 100,
					'TRANS_CASE' => 'L',
					'TRANS_SPACE' => '-',
					'TRANS_OTHER' => '-',
					'TRANS_EAT' => 'Y',
					'USE_GOOGLE' => 'N',
				),
			),
			'LOG_SECTION_ADD' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => null,),
			'LOG_SECTION_EDIT' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => null,),
			'LOG_SECTION_DELETE' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => null,),
			'LOG_ELEMENT_ADD' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => null,),
			'LOG_ELEMENT_EDIT' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => null,),
			'LOG_ELEMENT_DELETE' => array('IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => null,),
		),
		"CODE" => "slides",
		"XML_ID" => $iblockCode,
		//"NAME" => "[".WIZARD_SITE_ID."] ".$iblock->GetArrayByID($iblockID, "NAME")
	);

	$iblock->Update($iblockID, $arFields);

	$arReplace = array(
		'#SITE_NAME#' => $wizard->GetVar("siteName"),
		'#SITE_DIR#' => WIZARD_SITE_DIR,
	);

	$rsElements = CIBlockElement::GetList(array(), array('ACTIVE' => "Y"), false, false, array("ID", 'PREVIEW_TEXT'));
	while ($arElement = $rsElements->GetNext())
	{
		$arPreviewText = str_replace(array_keys($arReplace), array_values($arReplace), $arElement['PREVIEW_TEXT']);

		$el = new CIBlockElement();
		$el->Update($arElement['ID'], array('PREVIEW_TEXT' => $arPreviewText));
	}
}

$dbSite = CSite::GetByID(WIZARD_SITE_ID);
if ($arSite = $dbSite->Fetch())
{
	$lang = $arSite["LANGUAGE_ID"];
}
if (strlen($lang) <= 0)
{
	$lang = "ru";
}

WizardServices::IncludeServiceLang("slides.php", $lang);

$arFormFields = array(
	'tabs' => GetMessage("WZD_OPTION_SLIDES_1"),
);

$rsProperties = CIBlockProperty::GetList(Array(), Array(
	"IBLOCK_ID" => $iblockID,
));
while ($arProperty = $rsProperties->Fetch())
{
	if (strlen($arProperty["CODE"]) > 0)
	{
		$arFormFields['tabs'] = str_ireplace('PROPERTY_' . $arProperty["CODE"], 'PROPERTY_' . $arProperty["ID"], $arFormFields['tabs']);
	}
}
CUserOptions::SetOption("form", "form_element_" . $iblockID, $arFormFields);
CUserOptions::SetOption("list", "tbl_iblock_list_" . md5($iblockType . "." . $iblockID), array(
	'columns' => 'NAME,ACTIVE,DATE_ACTIVE_FROM,TIMESTAMP_X,ID,PREVIEW_PICITURE,PREVIEW_TEXT',
	'by' => 'sort',
	'order' => 'asc',
	'page_size' => '20',
));

CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "/_index.php", array("SLIDES_IBLOCK_ID" => $iblockID));

COption::SetOptionInt('citrus.tszh', "slides.iblock", $iblockID, "", WIZARD_SITE_ID);

\Citrus\Tszh\Wizard\Seo::setIblockSettings($iblockID, false);