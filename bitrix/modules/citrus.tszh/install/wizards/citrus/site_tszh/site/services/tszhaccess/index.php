<?

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!CModule::includeModule('otr.jkhaccess'))
{
    return;
}

$wizard =& $this->GetWizard();

// copy files

CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true, true);


$sourcePath = str_replace("//", "/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/otr.jkhaccess/install/public/" . LANGUAGE_ID . "/");
$destPath = WIZARD_SITE_PATH;

CopyDirFiles($sourcePath, $destPath, true, true);

$public_dir = "personal/access";
$workplace_dir = "workplace_access";

$arReplaceMacros = array("SITE_DIR" => WIZARD_SITE_DIR);
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . $public_dir."/index.php", $arReplaceMacros);
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . $workplace_dir."/index.php", $arReplaceMacros);

/*���� ��� ������������ */
WizardServices::AddMenuItem(WIZARD_SITE_DIR . "personal/.show_add.menu.php", Array(Loc::GetMessage('OTR_ADD_JKH_ACCESS_PUBLIC_MENU'), WIZARD_SITE_DIR . $public_dir . "/", Array(),  Array("class"=>"access_utilities"), ""), WIZARD_SITE_DIR);
WizardServices::AddMenuItem(WIZARD_SITE_DIR . "personal/.section.menu.php", Array(Loc::GetMessage('OTR_ADD_JKH_ACCESS_PUBLIC_MENU'), WIZARD_SITE_DIR . $public_dir . "/", Array(),  Array("class"=>"access_utilities leftmenu__img-access"), ""), WIZARD_SITE_DIR);

/*���� ��� ������*/
$str = "CSite::InGroup(array(CGroup::GetIDByCode('JKH_ACCESS_ADMINISTRATORS')))";
$temp = COption::GetOptionString("main", "wizard_template_id", "citrus_tszh_adaptive", WIZARD_SITE_ID);
if ($temp == 'citrus_tszh_adaptive')
{
	WizardServices::AddMenuItem(
		WIZARD_SITE_DIR . ".top_adaptive.menu.php",
		Array(
			Loc::GetMessage('OTR_TSZH_ACCESS_ADMIN_INSTALL_MENU_ITEM_VALUE'),
			WIZARD_SITE_DIR . $workplace_dir . "/",
			Array(),
			Array(),
			$str,
		),
		false, -1);
}
elseif ($temp == "tszh_lotos" || $temp == "tszh_three" || $temp == "tszh_one")
{
	WizardServices::AddMenuItem(
		WIZARD_SITE_DIR . "personal/.show_add.menu.php",
		Array(
			Loc::GetMessage('OTR_TSZH_ACCESS_ADMIN_INSTALL_MENU_ITEM_VALUE'),
			WIZARD_SITE_DIR . $workplace_dir . "/",
			Array(),
			Array(),
			$str,
		),
		false, -1);
}
else {
	WizardServices::AddMenuItem(
		WIZARD_SITE_DIR . ".top.menu.php",
		Array(
			Loc::GetMessage('OTR_TSZH_ACCESS_ADMIN_INSTALL_MENU_ITEM_VALUE'),
			WIZARD_SITE_DIR . $workplace_dir . "/",
			Array(),
			Array(),
			$str,
		),
		false, -1);
}

WizardServices::SetFilePermission(WIZARD_SITE_DIR . "/" . $public_dir , Array("*" => "R"));

global $APPLICATION;

$arUserGroups = Array(
	"TSZH_MEMBERS" => Array(
		"ACTIVE" => "Y",
		"NAME" => GetMessage("GROUP_JKH_MEMBERS"),
		"STRING_ID" => "TSZH_MEMBERS",
		"C_SORT" => 100,
		"DESCRIPTION" => "",
	),
	"JKH_ACCESS_ADMINISTRATORS" => Array(
		"ACTIVE" => "Y",
		"NAME" => GetMessage("GROUP_JKH_ACCESS_ADMINISTRATORS"),
		"STRING_ID" => "JKH_ACCESS_ADMINISTRATORS",
		"C_SORT" => 350,
		"DESCRIPTION" => "",
	),
);
$arUserGroupsRights = Array(
	"TSZH_MEMBERS" => 'R',
	"JKH_ACCESS_ADMINISTRATORS" => 'W',
);
$arWorkplaceAccessRights = Array(
	"JKH_ACCESS_ADMINISTRATORS" => 'R',
);
$arUserGroupsID = Array();

// ����� ��� ������������ ������
foreach ($arUserGroups as $code => $arUserGroup)
{
	$rsGroups = CGroup::GetList($by, $order, array("STRING_ID" => $code));
	if ($arGroup = $rsGroups->Fetch())
	{
		$arUserGroupsID[$code] = $arGroup['ID'];
	}
}

// �������� ������, ������� ��� ���
foreach ($arUserGroups as $code => $arUserGroup)
{
	if (!array_key_exists($code, $arUserGroupsID))
	{
		$obGroup = new CGroup;
		if (!($arUserGroupsID[$code] = $obGroup->Add($arUserGroup)))
		{
			$GLOBALS['APPLICATION']->ThrowException(Loc::getMessage('CITRUS_JKH_ERROR_CREATING_USER_GROUP', array(
					'#NAME#' => $arUserGroup['NAME'],
					'#ID#' => $arUserGroup['STRING_ID'],
				)) . ': ' . $arUserGroupsID[$code]->LAST_ERROR);
			return false;
		}

	}
}

// ��������� ���� �� ������
$arWorkplaceAccess = Array('*' => 'D');
foreach ($arUserGroupsID as $code => $groupID)
{
	if ($groupID > 0)
	{
		$APPLICATION->SetGroupRight('citrus.tszhtickets', $groupID, $arUserGroupsRights[$code]);
		if (array_key_exists($code, $arWorkplaceAccessRights))
		{
			$arWorkplaceAccess[$groupID] = $arWorkplaceAccessRights[$code];
		}
	}
}
WizardServices::SetFilePermission(WIZARD_SITE_DIR . 'workplace', $arWorkplaceAccess);

$arUrlRewrite = array();
if (file_exists(WIZARD_SITE_ROOT_PATH . "/urlrewrite.php"))
{
	include(WIZARD_SITE_ROOT_PATH . "/urlrewrite.php");
}
$arNewUrlRewrite[] = array(
	"CONDITION" => "#^" . WIZARD_SITE_DIR . "personal". "/#",
	"RULE" => "",
	"ID" => "otr:jkhaccess",
	"PATH" => WIZARD_SITE_DIR . "personal" . "/index.php",
);

$arNewUrlRewrite[] = array(
	"CONDITION" => "#^" . WIZARD_SITE_DIR . "workplace_access" . "/#",
	"RULE" => "",
	"ID" => "otr:jkhaccess",
	"PATH" => WIZARD_SITE_DIR . "workplace_access" . "/index.php",
);


foreach ($arNewUrlRewrite as $arUrl)
{
	if (!in_array($arUrl, $arUrlRewrite))
	{
		CUrlRewriter::Add($arUrl);
	}
}

?>