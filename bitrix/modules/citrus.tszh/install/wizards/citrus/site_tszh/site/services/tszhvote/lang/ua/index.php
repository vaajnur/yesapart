<?
$MESS["VOTING_INSTALL_II_VOTE"] = "Опитування";
$MESS["VOTING_INSTALL_MENU_ITEM"] = "Опитування";
$MESS["VOTING_INSTALL_CHANNEL_PERSONAL"] = "Опитування в особистому кабінеті";
$MESS["VOTING_INSTALL_VOTE_PERSONAL_VOTE_TITLE"] = "Як ви оцінюєте роботу ліфтового господарства, який обслуговує наш будинок? ";
$MESS["VOTING_INSTALL_VOTE_QUESTION4"] = "Як ви оцінюєте роботу ліфтового господарства, який обслуговує наш будинок? ";
$MESS["VOTING_INSTALL_VOTE_ANSWER4_1"] = "На “відмінно”, я дуже задоволений";
$MESS["VOTING_INSTALL_VOTE_ANSWER4_2"] = "Непагано, але ліфт часто не працює";
$MESS["VOTING_INSTALL_VOTE_ANSWER4_3"] = "Я не бачу результатів роботи";
$MESS["VOTING_INSTALL_VOTE_ANSWER4_4"] = "Необхідно терміново міняти ліфтерів";
$MESS["VOTING_INSTALL_VOTE_ANSWER4_5"] = "Необхідно терміново міняти ліфтерів";
$MESS["VOTING_MENU_1"] = "Брати участь в голосуванні і багато іншого";
$MESS["VOTING_TITLE"] = "Голосування";
?>