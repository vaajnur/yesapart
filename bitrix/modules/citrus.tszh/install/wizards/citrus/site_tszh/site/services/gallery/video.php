<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!tszhCheckMinEdition('standard'))
	return;

if(!CModule::IncludeModule("iblock"))
	return;

$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/services-video.xml";
$iblockCode = "video_".WIZARD_SITE_ID;
$iblockType = "services";

$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType, "SITE_ID" => WIZARD_SITE_ID));
$iblockID = false;
if ($arIBlock = $rsIBlock->Fetch())
{
	$iblockID = (int)$arIBlock["ID"];
	if (WIZARD_REINSTALL_DATA)
	{
		CIBlock::Delete($arIBlock["ID"]);
		$iblockID = false;
	}
}

if($iblockID == false)
{
	$iblockID = WizardServices::ImportIBlockFromXML(
		$iblockXMLFile,
		"video",
		$iblockType,
		WIZARD_SITE_ID,
		$permissions = Array(
			"1" => "X",
			"2" => "R"
		)
	);

	if ($iblockID < 1)
		return;

	//IBlock fields
	$iblock = new CIBlock;

	$arFields = Array(
		"ACTIVE" => "Y",
		"FIELDS" => array ( 'IBLOCK_SECTION' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'ACTIVE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'Y', ), 'ACTIVE_FROM' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'ACTIVE_TO' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SORT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '0', ), 'NAME' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '', ), 'PREVIEW_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'FROM_DETAIL' => 'N', 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, 'DELETE_WITH_DETAIL' => 'N', 'UPDATE_WITH_DETAIL' => 'N', ), ), 'PREVIEW_TEXT_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'PREVIEW_TEXT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'DETAIL_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, ), ), 'DETAIL_TEXT_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'DETAIL_TEXT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'XML_ID' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'CODE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'UNIQUE' => 'N', 'TRANSLITERATION' => 'N', 'TRANS_LEN' => 100, 'TRANS_CASE' => 'L', 'TRANS_SPACE' => '_', 'TRANS_OTHER' => '_', 'TRANS_EAT' => 'Y', 'USE_GOOGLE' => 'N', ), ), 'TAGS' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_NAME' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '', ), 'SECTION_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'FROM_DETAIL' => 'N', 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, 'DELETE_WITH_DETAIL' => 'N', 'UPDATE_WITH_DETAIL' => 'N', ), ), 'SECTION_DESCRIPTION_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'SECTION_DESCRIPTION' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_DETAIL_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, ), ), 'SECTION_XML_ID' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_CODE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'UNIQUE' => 'N', 'TRANSLITERATION' => 'N', 'TRANS_LEN' => 100, 'TRANS_CASE' => 'L', 'TRANS_SPACE' => '_', 'TRANS_OTHER' => '_', 'TRANS_EAT' => 'Y', 'USE_GOOGLE' => 'N', ), ), 'LOG_SECTION_ADD' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => NULL, ), 'LOG_SECTION_EDIT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => NULL, ), 'LOG_SECTION_DELETE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => NULL, ), 'LOG_ELEMENT_ADD' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => NULL, ), 'LOG_ELEMENT_EDIT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => NULL, ), 'LOG_ELEMENT_DELETE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => NULL, ), ),
		"CODE" => "video",
		"XML_ID" => $iblockCode,
		//"NAME" => "[".WIZARD_SITE_ID."] ".$iblock->GetArrayByID($iblockID, "NAME")
	);

	$iblock->Update($iblockID, $arFields);
}

$dbSite = CSite::GetByID(WIZARD_SITE_ID);
if($arSite = $dbSite -> Fetch())
	$lang = $arSite["LANGUAGE_ID"];
if(strlen($lang) <= 0)
	$lang = "ru";

WizardServices::IncludeServiceLang("video.php", $lang);

$arFormFields = array (
  'tabs' => 'edit1--#--����������--,--ACTIVE--#--����� ������������--,--ACTIVE_FROM--#--���� ����������--,--SORT--#--������ ����������--,--NAME--#--*���������--,--PREVIEW_PICTURE--#--��������--,--PROPERTY_22--#--����--,--PROPERTY_23--#--������������--;--edit2--#--�������--,--SECTIONS--#--�������--;--edit3--#--�������������--,--CODE--#--���������� ���--,--TAGS--#--����--;--',
);

$rsProperties = CIBlockProperty::GetList(Array(), Array(
	"IBLOCK_ID" => $iblockID,
));
$arProperties = Array();
while ($arProperty = $rsProperties->Fetch()) {
	if (strlen($arProperty["CODE"]) > 0) {
		$arFormFields['tabs'] = str_ireplace('PROPERTY_' . $arProperty["CODE"], 'PROPERTY_' . $arProperty["ID"], $arFormFields['tabs']);
		$arProperties[] = $arProperty;
	}
}
CUserOptions::SetOption("form", "form_element_".$iblockID, $arFormFields);

$sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH."/public/".LANGUAGE_ID."/video/");
$destPath = WIZARD_SITE_PATH;
CopyDirFiles($sourcePath, $destPath, true, true);

$arReplaceMacros = array("VIDEO_IBLOCK_ID" => $iblockID, "SITE_DIR" => WIZARD_SITE_DIR);
foreach ($arProperties as $arProperty) {
	$arReplaceMacros['VIDEO_PROPERTY_' . $arProperty["CODE"]] = $arProperty["ID"];
}
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."video/index.php", $arReplaceMacros);

$arMenuItem = Array(
		GetMessage("WZD_VIDEO_MENU_ITEM"),
		WIZARD_SITE_DIR . "video/",
		Array(),
		Array(),
		""
	);
if (isAdaptiveTszhTemplate())
{
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . '/epassports/.show_add.menu.php', $arMenuItem,  WIZARD_SITE_ID, -1);

	$fp = fopen(WIZARD_SITE_PATH . "video/sect_right.php", "w");
	fwrite($fp, "");
	fclose($fp);
}
else
{
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . '/photogallery/.show_add.menu.php', $arMenuItem,  WIZARD_SITE_ID, -1);
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . '/photogallery/.section.menu.php', $arMenuItem,  WIZARD_SITE_ID, -1);
}

$arUrlRewrite = array();
if (file_exists(WIZARD_SITE_ROOT_PATH."/urlrewrite.php"))
{
	include(WIZARD_SITE_ROOT_PATH."/urlrewrite.php");
}
$arNewUrlRewrite = array(
	array(
		"CONDITION"	=>	"#^".WIZARD_SITE_DIR."video/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:news",
		"PATH"	=>	WIZARD_SITE_DIR."video/index.php",
	),
);
foreach ($arNewUrlRewrite as $arUrl)
{
	if (!in_array($arUrl, $arUrlRewrite))
	{
		CUrlRewriter::Add($arUrl);
	}
}

$arReplaceMacros = Array(
	"SITE_DIR" => WIZARD_SITE_DIR,
	"SITE_NAME" => htmlspecialcharsbx($wizard->GetVar("siteName")),
	"SITE_EMAIL" => htmlspecialcharsbx($wizard->GetVar("siteEmail")),
	"SITE_PHONE" => htmlspecialcharsbx($wizard->GetVar("siteTelephone")),
	"SITE_ADDRESS" => htmlspecialcharsbx($wizard->GetVar("siteAddress")),
);
$wizard =& $this->GetWizard();
WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH . '/video/', $arReplaceMacros);

\Citrus\Tszh\Wizard\Seo::setIblockSettings($iblockID, true, true, false, true);