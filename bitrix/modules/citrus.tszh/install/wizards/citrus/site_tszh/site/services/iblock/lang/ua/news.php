<?
$MESS["WZD_OPTION_NEWS_1"] = "Новина";
$MESS["WZD_OPTION_NEWS_2"] = "Публікується на сайті";
$MESS["WZD_OPTION_NEWS_3"] = "Дата новини";
$MESS["WZD_OPTION_NEWS_4"] = "*Заголовок";
$MESS["WZD_OPTION_NEWS_5"] = "Джерело";
$MESS["WZD_OPTION_NEWS_6"] = "Стислий опис";
$MESS["WZD_OPTION_NEWS_7"] = "Повний опис";
$MESS["WZD_OPTION_NEWS_8"] = "Картинка";
$MESS["WZD_OPTION_NEWS_9"] = "Теги";
$MESS["WZD_OPTION_NEWS_10"] = "Властивості";
$MESS["WZD_OPTION_NEWS_11"] = "Заголовок вікна браузера ";
$MESS["WZD_OPTION_NEWS_12"] = "Ключові слова";
$MESS["WZD_OPTION_NEWS_13"] = "Опис";
$MESS["WZD_OPTION_NEWS_14"] = "Показувати тільки зареєстрованим користувачам ";
$MESS["WZD_OPTION_NEWS_15"] = "Виконати розсилку";
