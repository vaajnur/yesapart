<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "��������� ���� ������� ��������");
$APPLICATION->SetTitle("#SITE_NAME#");
?>
<?$APPLICATION->IncludeComponent(
	"citrus:news.list",
	"slider",
	Array(
		"SHOW_NEXT_PREV" => "N",
		"SHOW_PAGINATION" => "Y",
		"WIDTH" => "605",
		"HEIGHT" => "396",
		"DELAY" => "5000",
		"SPEED" => "350",
		"HOVER_PAUSE" => "Y",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "services",
		"IBLOCK_ID" => "#SLIDES_IBLOCK_ID#",
		"NEWS_COUNT" => "6",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ID",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array("PREVIEW_PICTURE"),
		"PROPERTY_CODE" => array(),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "������",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N"
	),
	false
);?>
<div class="our">
	<h1>��� ���� ��</h1>

    <p>�� ��� ����� ��� �� �������� ����� ���� ������� ��������!</p>
	<p>#SITE_NAME# ���� ������� � ��������� �� �������������� �������� ������� � ���������� ���������� � 2003 ����. �� ����� ������ � ��������� ����������� ����������� 257 �������� �������, � �� ����� 25000 ����� �������.</p>
	<p>�������� ����� ���� �������� � ��������� ������� ������ ����������� ������ �������.</p>
	<p>������ 300 ������������������� �������� #SITE_NAME# ����� ����������� ����������� ����������� ��������� ���������� ��� ��������.</p>
	<p>� ����� �������� ������� ��������, ����� � ���� ��������� � ��������� ����� ��������� ������, ���� �������� ���������� ������ � �������� ����� ������.</p>
	<p>��������� ��������, ����������� ����� ���������, �: ��������� ������������������, ����������, ����������������, �������������� ����������, �����������, �����������, ��������, <nobr>���������-���������</nobr> ����.</p>

	<div class="our-a"><a href="#SITE_DIR#about/">����������</a></div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>