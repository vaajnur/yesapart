<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!defined("WIZARD_SITE_ID"))
{
	return;
}

if (!defined("WIZARD_SITE_DIR"))
{
	return;
}

// copy files
$sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH . "/php_interface/");
$destPath = str_replace("//", "/", WIZARD_SITE_ROOT_PATH . '/bitrix/php_interface/');
CopyDirFiles($sourcePath, $destPath, true, true);

COption::SetOptionString("main", "save_original_file_name", "Y");
COption::SetOptionString("iblock", "use_htmledit", "Y");

COption::SetOptionString("main", "captcha_registration", "Y", false);
COption::SetOptionString("main", "auth_comp2", "Y", false);
COption::SetOptionString("main", "new_user_registration", "N", false);
//�������� �������� �� ������������ email � ������� ������
COption::SetOptionString("main", "new_user_email_uniq_check", "Y");

COption::SetOptionString("main", "map_top_menu_type", "top");
COption::SetOptionString("main", "map_left_menu_type", "section");

/*if(COption::GetOptionString("citrus.tszh", "wizard_installed", "N", WIZARD_SITE_ID) == "Y")
	return;*/

function CopyPublicFiles($fromPath)
{
	$handle = @opendir($fromPath);
	if ($handle)
	{
		while ($file = readdir($handle))
		{
			/**
			 * .htaccess �� ����� ��� ��������� ������ ������ �� ����� ������ �� ����� ������������ � ����� � ���������� �������
			 */
			if (in_array($file, array(".", "..")) || WIZARD_SITE_DIR !== '/' && $file == '.htaccess')
			{
				continue;
			}

			CopyDirFiles(
				$fromPath . $file,
				str_replace('//', '/', WIZARD_SITE_PATH . "/" . $file),
				$rewrite = true,
				$recursive = true,
				$delete_after_copy = false
			);
		}
	}
	else
	{
		echo "Error installing public files.";
	}
}

$sitePublicPath = str_replace("//", "/", WIZARD_ABSOLUTE_PATH . "/site/public/" . LANGUAGE_ID . "/");
CopyPublicFiles($sitePublicPath);

$templatePublicPath = str_replace("//", "/", WIZARD_ABSOLUTE_PATH . "/site/template_public/" . LANGUAGE_ID . "/" . WIZARD_TEMPLATE_ID . '/');
if (file_exists($templatePublicPath) && is_dir($templatePublicPath))
{
	CopyPublicFiles($templatePublicPath);
}

/** @var $wizard CWizardBase */
$arVars = $wizard->GetVars();
$arContacts = array_merge($arVars, Array(
	"SITE_DIR" => WIZARD_SITE_DIR,
	"SITE_NAME" => htmlspecialcharsbx($wizard->GetVar("siteName")),
	"SITE_EMAIL" => htmlspecialcharsbx($wizard->GetVar("siteEmail")),
	"SITE_PHONE" => htmlspecialcharsbx($wizard->GetVar("siteTelephone")),
	"SITE_ADDRESS" => htmlspecialcharsbx($wizard->GetVar("siteAddress")),
));
WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, $arContacts);

file_put_contents(
	WIZARD_SITE_PATH . '/robots.txt',
	str_replace(
		'#serverName#',
		$arVars["serverName"],
		file_get_contents(WIZARD_SITE_PATH . '/robots.txt')
	)
);

//copy(WIZARD_THEME_ABSOLUTE_PATH."/favicon.ico", WIZARD_SITE_PATH."favicon.ico");

$arUrlRewrite = array();
if (file_exists(WIZARD_SITE_ROOT_PATH . "/urlrewrite.php"))
{
	include(WIZARD_SITE_ROOT_PATH . "/urlrewrite.php");
}

$arNewUrlRewrite = array(
	array(
		"CONDITION" => "#^" . WIZARD_SITE_DIR . "personal/announcements/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR . "personal/announcements/index.php",
	),
	array(
		"CONDITION" => "#^" . WIZARD_SITE_DIR . "questions-and-answers/#",
		"RULE" => "",
		"ID" => "bitrix:support.faq",
		"PATH" => WIZARD_SITE_DIR . "questions-and-answers/index.php",
	),
	array(
		"CONDITION" => "#^" . WIZARD_SITE_DIR . "news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR . "news/index.php",
	),
	array(
		"CONDITION" => "#^" . WIZARD_SITE_DIR . "documents/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR . "documents/index.php",
	),
);


foreach ($arNewUrlRewrite as $arUrl)
{
	if (!in_array($arUrl, $arUrlRewrite))
	{
		CUrlRewriter::Add($arUrl);
	}
}

function ___writeToAreasFile($fn, $text)
{
	if (file_exists($fn) && !is_writable($fn) && defined("BX_FILE_PERMISSIONS"))
	{
		@chmod($fn, BX_FILE_PERMISSIONS);
	}

	$fd = @fopen($fn, "wb");
	if (!$fd)
	{
		return false;
	}

	if (false === fwrite($fd, $text))
	{
		fclose($fd);

		return false;
	}

	fclose($fd);

	if (defined("BX_FILE_PERMISSIONS"))
	{
		@chmod($fn, BX_FILE_PERMISSIONS);
	}
}

CheckDirPath(WIZARD_SITE_PATH . "include/");

$wizard =& $this->GetWizard();
___writeToAreasFile(WIZARD_SITE_PATH . "include/company_name.php", $wizard->GetVar("siteName"));
___writeToAreasFile(WIZARD_SITE_PATH . "include/address.php", $wizard->GetVar("siteAddress"));
___writeToAreasFile(WIZARD_SITE_PATH . "include/telephone.php", $wizard->GetVar("siteTelephone"));
___writeToAreasFile(WIZARD_SITE_PATH . "include/telephone_disp.php", $wizard->GetVar("siteTelephoneDisp"));


$siteName = $wizard->GetVar("siteName");
$arHeaderTitle = explode(' ', $siteName);
$strHeaderTitle = $arHeaderTitle[0] . " <em>" . implode(' ', array_slice($arHeaderTitle, 1)) . "</em>";
$strHeaderTitle = "<a href=\"" . WIZARD_SITE_DIR . "\">{$strHeaderTitle}</a>\n";
___writeToAreasFile(WIZARD_SITE_PATH . "include/title.php", $strHeaderTitle);

CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "/include/title_top.php", Array("SITE_NAME" => $wizard->GetVar("siteName"), "SITE_DIR" => WIZARD_SITE_DIR, "SITE_TEMPLATE_DIR" => "/bitrix/templates/" . WIZARD_TEMPLATE_ID . "_" . WIZARD_THEME_ID . "/"));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "/include/phone.php", Array("PHONE" => $wizard->GetVar("siteTelephone"), "PHONE_DISP" => $wizard->GetVar("siteTelephoneDisp")));
// CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "/bitrix/templates/" . WIZARD_TEMPLATE_ID . "_" . WIZARD_THEME_ID . "/include_areas/title_top.php", Array("SITE_NAME" => $wizard->GetVar("siteName"), "SITE_DIR" => WIZARD_SITE_DIR));

$dbSite = CSite::GetByID(WIZARD_SITE_ID);
if ($arSite = $dbSite->Fetch())
{
	$lang = $arSite["LANGUAGE_ID"];
}
if (strlen($lang) <= 0)
{
	$lang = "ru";
}

WizardServices::IncludeServiceLang("files.php", $lang);

$arUserTypes = Array(
	array(
		'ENTITY_ID' => 'USER',
		'FIELD_NAME' => 'UF_TSZH_EML_CNFRM_RQ',
		'USER_TYPE_ID' => 'boolean',
		'XML_ID' => '',
		'SORT' => '100',
		'MULTIPLE' => 'N',
		'MANDATORY' => 'N',
		'SHOW_FILTER' => 'I',
		'SHOW_IN_LIST' => 'Y',
		'EDIT_IN_LIST' => 'Y',
		'IS_SEARCHABLE' => 'N',
		'SETTINGS' =>
			array(
				'DEFAULT_VALUE' => 0,
				'DISPLAY' => 'CHECKBOX',
			),
		'EDIT_FORM_LABEL' => Array(
			$lang => GetMessage('MAIN_SERVICE_EMAIL_CONFIRMATION_REQ'),
		),
		'LIST_COLUMN_LABEL' => Array(
			$lang => GetMessage('MAIN_SERVICE_EMAIL_CONFIRMATION_REQ'),
		),
		'LIST_FILTER_LABEL' => Array(
			$lang => GetMessage('MAIN_SERVICE_EMAIL_CONFIRMATION_REQ'),
		),
	),
);
foreach ($arUserTypes as $arUserTypeEntity)
{
	$arExistingUserType = CUserTypeEntity::GetList(Array(), Array(
		"ENTITY_ID" => $arUserTypeEntity["ENTITY_ID"],
		"FIELD_NAME" => $arUserTypeEntity["FIELD_NAME"],
	))->Fetch();
	$userTypeEntity = new CUserTypeEntity();
	if (is_array($arExistingUserType))
	{
		$bSuccess = $userTypeEntity->Update($arExistingUserType['ID'], $arUserTypeEntity);
	}
	else
	{
		$bSuccess = $userTypeEntity->Add($arUserTypeEntity) > 0;
	}
	if (!$bSuccess)
	{
		$strError = GetMessage("MAIN_SERIVCE_ERROR");
		if ($ex = $APPLICATION->GetException())
		{
			$strError .= ': ' . $ex->GetString();
		}
		ShowError($strError);
		die();
	}
}

?>