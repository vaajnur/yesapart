<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

$wizard =& $this->GetWizard();

if (!tszhCheckMinEdition('smallbusiness'))
{
	if (IsModuleInstalled('vdgb.tszhhouses'))
	{
        $sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH . "/public/" . LANGUAGE_ID . "/houses/");
        $destPath = str_replace("//", "/", WIZARD_SITE_PATH . '/epassports/');
        CopyDirFiles($sourcePath, $destPath, true, true);
    }

    // ���� ��������������� ���������� ������
    if (isAdaptiveTszhTemplate())
    {
        WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/epassports/.show_add.menu.php", Array(
            GetMessage("F_TSZH_EPASPORTS_SHOW_ADD_MENUITEM"),
            WIZARD_SITE_DIR . 'epassports',
            Array(),
            Array(),
            "",
        ), false, 0);

        $connectOrchidComponentHouses = <<<PHP
<?\$APPLICATION->IncludeComponent(
    "vdgb:tszh.houses",
    "orchid_default",
    array(
        "CACHE_TIME" => "7200",
        "CACHE_TYPE" => "A",
        "COMPONENT_TEMPLATE" => ".default",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_TOP_PAGER" => "Y",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "���",
        "SELECT_FIELDS" => array(
            0 => "TYPE",
            1 => "ADDRESS_USER_ENTERED",
            2 => "YEAR_OF_BUILT",
            3 => "FLOORS",
            4 => "PORCHES",
            5 => "AREA",
        ),
        "SORT_BY" => "AREA",
        "SORT_ORDER" => "ASC",
        "TSZH_COUNT" => "20",
        "TSZH_FIELD" => "5",
        "TSZH_ID" => "0"
    ),
    false
);?>
PHP;
        CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/epassports/index.php", array("CONNECT_COMPONENT_HOUSES" => $connectOrchidComponentHouses));
    }
// ���� ��������������� ������ ������
    else
    {
        WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/documents/.show_add.menu.php", Array(
            GetMessage("F_TSZH_EPASPORTS_MENUITEM"),
            WIZARD_SITE_DIR . 'epassports/',
            Array(),
            Array(),
            "",
        ), false, -1);

        $connectLotosComponentHouses = <<<PHP
<?\$APPLICATION->IncludeComponent(
    "vdgb:tszh.houses",
    ".default",
    array(
        "CACHE_TIME" => "7200",
        "CACHE_TYPE" => "A",
        "COMPONENT_TEMPLATE" => ".default",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_TOP_PAGER" => "Y",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "���",
        "SELECT_FIELDS" => array(
            0 => "TYPE",
            1 => "ADDRESS_USER_ENTERED",
            2 => "YEAR_OF_BUILT",
            3 => "FLOORS",
            4 => "PORCHES",
            5 => "AREA",
        ),
        "SORT_BY" => "AREA",
        "SORT_ORDER" => "ASC",
        "TSZH_COUNT" => "20",
        "TSZH_FIELD" => "5",
        "TSZH_ID" => "0"
    ),
    false
);?>
PHP;
        CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/epassports/index.php", array("CONNECT_COMPONENT_HOUSES" => $connectLotosComponentHouses));
    }
}

