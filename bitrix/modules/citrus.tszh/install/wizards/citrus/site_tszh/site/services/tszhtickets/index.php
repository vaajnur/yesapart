<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!(tszhCheckMinEdition('expert') || (IsModuleInstalled('vdgb.tszhads') && CModule::IncludeModule('vdgb.tszhads'))))
{
	return;
}

if (!CModule::IncludeModule("citrus.tszhtickets"))
{
	return;
}


// fill dictionaries
$arDictionaryItems = Array(
	array(
		'NAME' => GetMessage('SUP_DEF_COMMON'),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'C',
		'C_SORT' => 100,
		'EVENT1' => 'ticket',
		'EVENT2' => 'common',
		"SET_AS_DEFAULT" => "Y",
		"TIME_TO_SOLVE" => "48",
		"SID" => "common",
	),

	array(
		'NAME' => GetMessage('SUP_DEF_LOW'),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'K',
		'C_SORT' => 100,
		"SID" => "low",
	),
	array(
		'NAME' => GetMessage('SUP_DEF_MIDDLE'),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'K',
		'C_SORT' => 200,
		"SID" => "middle",
	),
	array(
		'NAME' => GetMessage('SUP_DEF_HIGH'),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'K',
		'C_SORT' => 300,
		"SID" => "high",
	),

	array(
		'NAME' => GetMessage("CTT_NEW_TICKET"),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'S',
		'SID' => 'new',
		'C_SORT' => 100,
		"DESCR" => GetMessage("CTT_NEW_TICKET_D"),
		"SET_AS_DEFAULT" => "Y",
	),
	array(
		'NAME' => GetMessage("CTT_TICKET_DELEGATED"),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'S',
		'SID' => 'delegated',
		'C_SORT' => 200,
		"DESCR" => GetMessage("CTT_TICKET_DELEGATED_D"),
	),
	array(
		'NAME' => GetMessage("CTT_TICKET_ACCEPTED"),
		'arrSITE' => array(WIZARD_SITE_ID),
		"SID" => 'accepted',
		'C_TYPE' => 'S',
		'C_SORT' => 250,
		"DESCR" => GetMessage("CTT_TICKET_ACCEPTED_D"),
	),
	array(
		'NAME' => GetMessage("CTT_TICKET_DONE"),
		'arrSITE' => array(WIZARD_SITE_ID),
		"SID" => 'done',
		'C_TYPE' => 'S',
		'C_SORT' => 300,
		"DESCR" => GetMessage("CTT_TICKET_DONE_D"),
	),
	array(
		'NAME' => GetMessage("CTT_TICKET_CLOSED"),
		'arrSITE' => array(WIZARD_SITE_ID),
		"SID" => 'closed',
		'C_TYPE' => 'S',
		"DESCR" => GetMessage("CTT_TICKET_CLOSED_D"),
		'C_SORT' => 400,
	),

	array(
		'NAME' => GetMessage('SUP_DEF_ANSWER_SUITS_THE_NEEDS'),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'M',
		'C_SORT' => 100,
		"SID" => "suits_the_needs",
	),
	array(
		'NAME' => GetMessage('SUP_DEF_ANSWER_IS_NOT_COMPLETE'),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'M',
		'C_SORT' => 200,
		"SID" => "is_not_complete",
	),
	array(
		'NAME' => GetMessage('SUP_DEF_ANSWER_DOES_NOT_SUIT'),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'M',
		'C_SORT' => 300,
		"SID" => "does_not_suit",
	),

	array(
		'NAME' => GetMessage('SUP_DEF_E_MAIL'),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'SR',
		'C_SORT' => 100,
		'SID' => 'email',
	),
	array(
		'NAME' => GetMessage('SUP_DEF_PHONE'),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'SR',
		'C_SORT' => 200,
		"SID" => 'phone',
	),
	array(
		'NAME' => GetMessage('SUP_DEF_FORUM'),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'SR',
		'C_SORT' => 300,
		"SID" => 'forum',
	),

	array(
		'NAME' => GetMessage('SUP_DEF_EASY'),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'D',
		'C_SORT' => 100,
		"SID" => 'easy',
	),
	array(
		'NAME' => GetMessage('SUP_DEF_MEDIUM'),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'D',
		'C_SORT' => 200,
		"SID" => 'medium',
	),
	array(
		'NAME' => GetMessage('SUP_DEF_HARD'),
		'arrSITE' => array(WIZARD_SITE_ID),
		'C_TYPE' => 'D',
		'C_SORT' => 300,
		"SID" => 'hard',
	),
);
$GLOBALS['APPLICATION']->ResetException();
foreach ($arDictionaryItems as $arDictionaryItem)
{
	$rs = CTszhTicketDictionary::GetBySID($arDictionaryItem["SID"], $arDictionaryItem["C_TYPE"], WIZARD_SITE_ID);
	if ($rs->SelectedRowsCount() > 0)
	{
		continue;
	}

	if (!CTszhTicketDictionary::Add($arDictionaryItem))
	{
		if ($ex = $GLOBALS['APPLICATION']->GetException())
		{
			echo $ex->GetString();
		}
		else
		{
			echo "Error creating dictionary entry!";
		}
		die();
	}
}


// copy files
if (isAdaptiveTszhTemplate())
{
	$templateID = 'citrus_tszh_adaptive';
}
else
{
	$templateID = 'citrus_tszh';
}
$sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH . "/template_public/" . LANGUAGE_ID . "/" . $templateID . "/");
$destPath = WIZARD_SITE_PATH;
CopyDirFiles($sourcePath, $destPath, true, true);

CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "personal/support/index.php", array("SITE_DIR" => WIZARD_SITE_DIR));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "workplace/index.php", array("SITE_DIR" => WIZARD_SITE_DIR));

if (isAdaptiveTszhTemplate())
{
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "personal/support/ticket/index.php", array("SITE_DIR" => WIZARD_SITE_DIR));
}

/*//Add top menu items
WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/.top.menu.php", Array(
	GetMessage("F_TICKETS_MENU"),
	WIZARD_SITE_DIR . "personal/support/",
	Array(),
	Array(),
	""
), WIZARD_SITE_ID, -1);*/

//Add personal menu items
$arPersonalMenuItems = Array(
	Array(
		GetMessage("F_TICKETS_MENU"),
		WIZARD_SITE_DIR . "personal/support/",
		Array(),
		Array("class" => "service leftmenu__img-requests"),
		"",
	),
);
foreach ($arPersonalMenuItems as $arMenuItem)
{
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . "personal/.section.menu.php", $arMenuItem, WIZARD_SITE_ID, -1);
	// WizardServices::AddMenuItem(WIZARD_SITE_DIR . "personal/.show_add.menu.php", $arMenuItem, WIZARD_SITE_ID, -1);
}
WizardServices::AddMenuItem(WIZARD_SITE_DIR . "personal/.show_add.menu.php", Array(
	GetMessage("F_TICKETS_MENU"),
	WIZARD_SITE_DIR . "personal/support/",
	Array(),
	Array("class" => "service"),
	"",
), WIZARD_SITE_ID, -1
);
//Add root menu items
$arPersonalMenuItems = Array(
	Array(
		GetMessage("F_TICKETS_HOME"),
		WIZARD_SITE_DIR . "personal/support/0.php",
		Array(),
		Array("alt" => "1"),
		"",
	),
);
foreach ($arPersonalMenuItems as $arMenuItem)
{
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/.section.menu.php", $arMenuItem, WIZARD_SITE_ID, -1);
}


if (COption::GetOptionString("citrus.tszhtickets", "wizard_installed", "N", WIZARD_SITE_ID) == "Y")
{
	return;
}


$arUserGroups = Array(
	"TSZH_MEMBERS" => Array(
		"ACTIVE" => "Y",
		"NAME" => GetMessage("GROUP_TSZH_MEMBERS"),
		"STRING_ID" => "TSZH_MEMBERS",
		"C_SORT" => 100,
		"DESCRIPTION" => "",
	),
	"TSZH_SUPPORT_ADMINISTRATORS" => Array(
		"ACTIVE" => "Y",
		"NAME" => GetMessage("GROUP_TSZH_SUPPORT_ADMINISTRATORS"),
		"STRING_ID" => "TSZH_SUPPORT_ADMINISTRATORS",
		"C_SORT" => 250,
		"DESCRIPTION" => "",
	),
	"TSZH_SUPPORT_CONTRACTORS" => Array(
		"ACTIVE" => "Y",
		"NAME" => GetMessage("GROUP_TSZH_SUPPORT_CONTRACTORS"),
		"STRING_ID" => "TSZH_SUPPORT_CONTRACTORS",
		"C_SORT" => 300,
		"DESCRIPTION" => "",
	),
);
$arUserGroupsRights = Array(
	"TSZH_MEMBERS" => 'R',
	"TSZH_SUPPORT_ADMINISTRATORS" => 'W',
	"TSZH_SUPPORT_CONTRACTORS" => 'T',
);
$arWorkplaceAccessRights = Array(
	"TSZH_SUPPORT_ADMINISTRATORS" => 'R',
	"TSZH_SUPPORT_CONTRACTORS" => 'R',
);
$arUserGroupsID = Array();

// ����� ��� ������������ ������
foreach ($arUserGroups as $code => $arUserGroup)
{
	$rsGroups = CGroup::GetList($by, $order, array("STRING_ID" => $code));
	if ($arGroup = $rsGroups->Fetch())
	{
		$arUserGroupsID[$code] = $arGroup['ID'];
	}
}

// �������� ������, ������� ��� ���
foreach ($arUserGroups as $code => $arUserGroup)
{
	if (!array_key_exists($code, $arUserGroupsID))
	{
		$obGroup = new CGroup;
		$arUserGroupsID[$code] = $obGroup->Add($arUserGroup);
	}
}

// ��������� ���� �� ������
$arWorkplaceAccess = Array('*' => 'D');
foreach ($arUserGroupsID as $code => $groupID)
{
	if ($groupID > 0)
	{
		$APPLICATION->SetGroupRight('citrus.tszhtickets', $groupID, $arUserGroupsRights[$code]);
		if (array_key_exists($code, $arWorkplaceAccessRights))
		{
			$arWorkplaceAccess[$groupID] = $arWorkplaceAccessRights[$code];
		}
	}
}
WizardServices::SetFilePermission(WIZARD_SITE_DIR . 'workplace', $arWorkplaceAccess);


$arUrlRewrite = array();
if (file_exists(WIZARD_SITE_ROOT_PATH . "/urlrewrite.php"))
{
	include(WIZARD_SITE_ROOT_PATH . "/urlrewrite.php");
}
if (isAdaptiveTszhTemplate())
{
	$arNewUrlRewrite[] = array(
		"CONDITION" => "#^" . WIZARD_SITE_DIR . "personal/support/ticket/#",
		"RULE" => "",
		"ID" => "citrus:support.ticket",
		"PATH" => WIZARD_SITE_DIR . "personal/support/ticket/index.php",
	);
}
else
{
	$arNewUrlRewrite[] = array(
		"CONDITION" => "#^" . WIZARD_SITE_DIR . "personal/support/#",
		"RULE" => "",
		"ID" => "citrus:support.request.chat",
		"PATH" => WIZARD_SITE_DIR . "personal/support/index.php",
	);
}
$arNewUrlRewrite[] = array(
	"CONDITION" => "#^" . WIZARD_SITE_DIR . "workplace/#",
	"RULE" => "",
	"ID" => "citrus:support.ticket",
	"PATH" => WIZARD_SITE_DIR . "workplace/index.php",
);
foreach ($arNewUrlRewrite as $arUrl)
{
	if (!in_array($arUrl, $arUrlRewrite))
	{
		CUrlRewriter::Add($arUrl);
	}
}

COption::SetOptionString("citrus.tszhtickets", "wizard_installed", "Y", false, WIZARD_SITE_ID);

?>
