<?
$MESS["SERVICE_MAIN_SETTINGS"] = "Настройки сайта";
$MESS["SERVICE_IBLOCK"] = "Информационные блоки";
$MESS["SERVICE_IBLOCK_DEMO_DATA"] = "Новости, полезная информация, объявления и «вопрос-ответ»";
$MESS["SERVICE_GALLERY_DEMO_DATA"] = "Фото и видеогалерея";
$MESS["SERVICE_TSZH_DEMO_DATA"] = "Настройка модуля «1С: Сайт ЖКХ»";
$MESS["SERVICE_FORUM_DEMO_DATA"] = "Форум";
$MESS["SERVICE_BLOG_DEMO_DATA"] = "Блог";
$MESS["SERVICE_TSZHTICKETS_DEMO_DATA"] = "Аварийно-диспетчерская служба";
$MESS["SERVICE_VOTE_DEMO_DATA"] = "Голосования";
$MESS["SERVICE_TSZHPAYMENT_DEMO_DATA"] = "Оплата услуг на сайте";
$MESS["SERVICE_TSZH_SUBSCRIBE"] = "Рассылки";
$MESS["SERVICE_TSZH_DEBTORS"] = "Список должников";
$MESS["SERVICE_TSZH_OPERATOR"] = "Операторы приема платежей";
$MESS["SERVICE_TSZH_EPASPORT"] = "Электронные паспорта";
$MESS["SERVICE_TSZH_CHART"] = "Графики";
$MESS["SERVICE_TSZH_ACCESS"] = "Пропуска";
$MESS["SERVICE_FINISH"] = "Завершение настройки";
