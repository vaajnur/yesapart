<?
$MESS["wiz_structure_data"] = "Установить демонстрационные данные сайта";
$MESS["wiz_restructure_data"] = "Переустановить демонстрационные данные сайта";
$MESS["WIZ_COMPANY_NAME"] = "Название сайта или организации";
$MESS["WIZ_COMPANY_NAME_DEF"] = "УК «ЖилКом»";
$MESS["WIZ_COMPANY_TELEPHONE"] = "Контактный телефон";
$MESS["WIZ_COMPANY_TELEPHONE_DEF"] = "8 (836) 230-40-09";
$MESS["WIZ_COMPANY_TELEPHONE_DISP"] = "Телефон диспетчерской";
$MESS["WIZ_COMPANY_TELEPHONE_DISP_DEF"] = "8 (836) 255-55-55";
$MESS["WIZ_COMPANY_EMAIL"] = "Контактный E-mail";
$MESS["WIZ_TSZH_MONETA_EMAIL"] = "E-mail для взаимодействия с сервисом Монета.ру";
$MESS["WIZ_TSZH_MONETA_EMAIL_INFO"] = "Данный e-mail адрес будет использован для регистрации в сервисе Монета.ру. Он должен быть уникальным и изменить его можно будет только при обращении в Монета.ру";
$MESS["WIZ_COMPANY_SITE_EMAIL_WRONG"] = "Указан некорректный E-mail";
$MESS["WIZ_COMPANY_ADDRESS"] = "Адрес организации (фактический)";
$MESS["WIZ_COMPANY_SERVER_NAME"] = "Адрес сайта";
$MESS["WIZ_COMPANY_SERVER_NAME_TIP"] = "Укажите адрес сайта (доменное имя) без указания www или http://";
$MESS["WIZ_COMPANY_SERVER_NAME_TEXT"] = "<i>Важно ввести реальный адрес для правильной работы рассылок и модуля поисковой оптимизации.<br><br>Если вы устанавливаете тестовую версию, укажите реальный адрес, который будет использоваться в будущем.</i>";
$MESS["WIZ_COMPANY_SERVER_NAME_WRONG"] = "Адрес сайта указан не верно. Пожалуйста проверьте правильность заполенния этого поля.";
$MESS["WIZ_COMPANY_ADDRESS_DEF"] = "Россия, 424004, г. Йошкар-Ола, ул. Волкова, д. 68";
$MESS["WIZ_COMPANY_MISSING_FIELD"] = "Не заполнено обязательное поле: #FIELD#";
$MESS["WIZ_EMPTY_FIELD"] = "Не заполнено поле «#FIELD#»";
$MESS["WIZ_INVALID_MONTH_DAY_FIELD"] = "Число месяца в поле «#FIELD#» должно быть в диапазоне от 1 до 31 включительно";
$MESS["wiz_site_name"] = "1С: Сайт ЖКХ";
$MESS["WIZ_STEP_SITE_SET"] = "Информация о сайте";
$MESS["WIZ_TSZH_OF_NAME"] = "Название организации";
$MESS["WIZ_TSZH_OF_NAME_DEF"] = "УК \"ЖилКом\"";
$MESS["WIZ_TSZH_BANK_TITLE"] = "Банковские реквизиты";
$MESS["WIZ_TSZH_BANK"] = "Банк";
$MESS["WIZ_TSZH_BANK_DEF"] = "ОАО \"Сбербанк России\", г. Москва";
$MESS["WIZ_TSZH_ORGANIZATION_TYPE"] = "Тип организации";
$MESS["WIZ_TSZH_INN"] = "ИНН";
$MESS["WIZ_TSZH_KPP"] = "КПП";
$MESS["WIZ_TSZH_KBK"] = "КБК";
$MESS["WIZ_TSZH_OKTMO"] = "ОКТМО";
$MESS["WIZ_TSZH_RSCH"] = "Расчетный счет";
$MESS["WIZ_TSZH_KSCH"] = "Корр. счет";
$MESS["WIZ_TSZH_BIK"] = "БИК";
$MESS["WIZ_TSZH_SELECT_DEFAULT"] = "Выбери тип организации";
$MESS["WIZ_TSZH_SOLE_TRADER"] = "Индивидуальный предприниматель";
$MESS["WIZ_TSZH_LEGAL_PERSONALITY"] = "Юридическое лицо";
$MESS["WIZ_TSZH_IS_BUDGET"] = "Бюджетное учреждение";
$MESS["WIZ_TSZH_IS_BUDGET_NOTE"] = "Необходимо заполнить КБК и ОКТМО для объекта управления, если оплата услуг производится на расчетный счет бюджетной организаций";
$MESS["WIZ_TSZH_DOWNLOAD_DEMODATA"] = "Установить демонстрационные данные";

$MESS["WIZ_TSZH_ORG_TYPE_ERROR_REQ"] = "Не выбран «Тип организации».";
$MESS["WIZ_TSZH_INN_ERROR_REQ"] = "Не заполнено поле «ИНН».";
$MESS["WIZ_TSZH_INN_ERROR_INVALID"] = "В поле «ИНН» введено некорректное значение.";
$MESS["WIZ_TSZH_KPP_ERROR_REQ"] = "Не заполнено поле «КПП».";
$MESS["WIZ_TSZH_KPP_ERROR_INVALID"] = "В поле «КПП» введено некорректное значение.";

$MESS["WIZ_TSZH_IMPORT_ERROR_XML_FILE_NOT_FOUND"] = "файл с демо-данными #FILE# не найден";
$MESS["WIZ_TSZH_IMPORT_ERROR_CREATE_TABLES"] = "не удалось создать временные таблицы в базе данных";
$MESS["WIZ_TSZH_IMPORT_ERROR_XML_FILE_OPEN"] = "не удалось открыть файл с демо-данными #FILE# для чтения";
$MESS["WIZ_TSZH_IMPORT_ERROR_READ_XML"] = "не удалось загрузить демо-данные из xml-файла в базу данных";
$MESS["WIZ_TSZH_IMPORT_ERROR_INDEX_TABLES"] = "не удалось проиндексировать временные таблицы базы данных";
$MESS["WIZ_TSZH_IMPORT_ERROR_PROCESS_PERIOD"] = "не удалось создать период для демонстрационных лицевых счетов";
$MESS["CITRUS_WIZARD_IMPORT_ERROR"] = "<p style=\"color: red\";>Произошла ошибка при импорте демо-данных: #ERROR#.<br>Демо-данные не загружены.</p>";

$MESS["WIZ_STEP_TSZH"] = "Информация об объекте управления";
$MESS["WIZ_TSZH_SUBSCRIBE"] = "Рассылки";
$MESS["WIZ_TSZH_USE_DEBT_SUBSCRIPTION"] = "Рассылать уведомления должникам";
$MESS["WIZ_TSZH_DEBT_SUBCRIPTION_DESC"] = "Владельцам лицевых счетов, имеющих задолженность, будут высылылаться письма с уведомлением о задолженности.";
$MESS["WIZ_TSZH_USE_RECEIPT_SUBSCRIPTION"] = "Рассылать квитанции на оплату по электронной почте";
$MESS["WIZ_TSZH_RECEIPT_SUBCRIPTION_DESC"] = "Квитанции на оплату будут рассылаться владельцам лицевых счетов ежемесячно по электронной почте, по мере их загрузки из 1С или другой учетной системы.";
$MESS["WIZ_TSZH_MIN_DEBT"] = "Минимальная сумма задолженности";
$MESS["WIZ_TSZH_DEBT_DATE"] = "День месяца для рассылки уведомлений";
$MESS["WIZ_TSZH_RECEIPT_DATE"] = "День месяца для рассылки квитанций";
$MESS["WIZ_TSZH_MIN_DEBT_CURRENCY"] = "руб.";
$MESS["WIZ_TSZH_SUBSCRIBE_METERS_ACTIVE"] = "Рассылать уведомления о необходимости ввода показаний счетчиков";
$MESS["WIZ_TSZH_SUBSCRIBE_METERS_DESC"] = "В указанный день месяца всем владельцам лицевых счетов будут отправлены уведомления с напоминанием о необходимости ввести показания счетчиков на сайте.";
$MESS["WIZ_TSZH_SUBSCRIBE_METERS_DATE"] = "День месяца для рассылки напоминаний";
$MESS["TSZH_MODULE_INCLUDE_ERROR"] = "Не установлен модуль 1С: Сайт ЖКХ";
$MESS["TSZH_FINISH_WIZARD"] = "Закончить работу мастера";
$MESS["WIZ_TSZH_RECEIPT"] = "Шаблон квитанции";
$MESS["WIZ_TSZH_RECEIPT_NEW"] = "По постановлению №354";
$MESS["WIZ_TSZH_RECEIPT_1161"] = "По форме ЕПД для МО №1161";
$MESS["WIZ_TSZH_RECEIPT_DEFAULT"] = "По умолчанию";
$MESS["WIZ_TSZH_PAYMENT"] = "Оплата";
$MESS["WIZ_TSZH_PAYMENT_MONETA_ENABLED"] = "<p style=\"margin-left: 25px;\">Разрешить возможность приёма платежей через платёжный сервис \"НКО&nbsp;\"МОНЕТА\" лицензия ЦБ РФ №3508-К от 02 июля 2012 года</p>";
$MESS["WIZ_TSZH_PAYMENT_MONETA_AGREEMENT"] = "<p style=\"margin-left: 25px;\">Подтверждаю свое согласие на&nbsp;присоединении к&nbsp;<a href=\"https://moneta.ru/info/public/merchants/b2boffer.pdf\" target=\"_blank\">Договору НКО &laquo;МОНЕТА&raquo; (ООО)</a>, с&nbsp;<a href=\"https://www.moneta.ru/info/public/merchants/b2btariffs.pdf\" target=\"_blank\">Тарифами</a> и&nbsp;<a href=\"#moneta-procedure\" style=\"text-decoration: none; border-bottom: 1px dashed #2770ba\">Порядком действий</a> ознакомлен и&nbsp;согласен</p>";
$MESS["WIZ_TSZH_PAYMENT_MONETA_PROCEDURE"] = "
<p><strong>Порядок действий</strong></p>
<ol>
	<li><p>На&nbsp;указанный Вами при установке <nobr>e-mail</nobr> (#EMAIL#) будет направлено письмо с&nbsp;реквизитами доступа в&nbsp;Личный кабинет НКО &laquo;МОНЕТА&raquo; (ООО). Войдите в&nbsp;Личный кабинет и&nbsp;заполните все требуемые поля. После первого входа рекомендуем изменить пароль.</p></li>
	<li><p>После заполнения и&nbsp;подтверждения всех обязательных полей в&nbsp;формах Личного кабинета&nbsp;&mdash; будет автоматически сформировано Заявление о&nbsp;присоединении к&nbsp;условиям договора. Скачайте Заявление в&nbsp;разделе &laquo;Документы&raquo;, распечатайте, подпишите и&nbsp;отправьте в&nbsp;наш адрес &laquo;Заявление о&nbsp;присоединении&raquo; по&nbsp;адресу: <i>424020, Российская Федерация, Республика Марий Эл, г.&nbsp;<nobr>Йошкар-Ола</nobr>, ул.&nbsp;Анциферова, дом 7, корпус &laquo;А&raquo; для НКО &laquo;МОНЕТА&raquo; (ООО)</i>, телефон для курьерской службы <nobr>(8362) 45-43-83</nobr>.</p></li>
	<li><p>По&nbsp;умолчанию доступны следующие способы: <i>банковские карты</i>, <i>Монета.ру</i>, <i>Яндекс.Деньги</i>. Если в&nbsp;течение 30 календарных дней не&nbsp;будет получено письменное подтверждение согласия с&nbsp;условиями Договора НКО &laquo;МОНЕТА&raquo; (ООО), он&nbsp;может быть расторгнут.</p></li>
</ol>
<p><small>Примечание: Денежные средства перечисляются на&nbsp;ваш банковский счет не&nbsp;позднее первого рабочего дня, следующего за&nbsp;днем получения НКО &laquo;МОНЕТА&raquo; (ООО) распоряжения Плательщика. Вознаграждение за&nbsp;осуществление Переводов указано в&nbsp;Тарифах. В&nbsp;случае вашего присоединения к&nbsp;Договору НКО &laquo;МОНЕТА&raquo; (ООО): комиссия НКО &laquo;МОНЕТА&raquo; (ООО) удерживается из&nbsp;суммы, подлежащей перечислению на&nbsp;ваш расчетный счет; комиссия с&nbsp;Плательщика не&nbsp;удерживается.<br><br>Расторжение Договора с НКО «МОНЕТА» (ООО) возможно путем направления соответствующего письменного уведомления.</small></p>
";
$MESS["WIZ_TSZH_HEAD_NAME"] = "ФИО руководителя";
$MESS["WIZ_TSZH_LEGAL_ADDRESS"] = "Юридический адрес";

$MESS["WIZ_ERROR_REQ_MONETA_SCHEME"] = "Не выбрана схема подключения платёжного сервиса \"МОНЕТА.РУ\"";
$MESS["WIZ_STEP_MONETA"] = "Настройка приёма платежей через сайт";
$MESS["WIZ_STEP_ADDITIONAL_SETTINGS"] = "Прочие настройки";
$MESS["WIZ_TSZH_OFFICE_HOURS"] = "Расписание работы";

$MESS["WIZ_REWRITE_WARNING"] = "Внимание!";
$MESS["WIZ_REWRITE_WARNING_TEXT"] = "Повторный запуск мастера настройки приведет к перезаписи стандартного шаблона сайта, статических страниц, редактируемых областей публичной части и	пунктов меню. Их содержимое вернется к типовому контенту.<br><br>Перед выполнением этой процедуры настоятельно рекомендуем сделать <b>полную резервную копию</b> сайта.";
$MESS["CITRUS_WIZARD_SITEMAP_INFO"] = "<p>Для поисковой оптимизации рекомендуется создать карту сайту (Sitemap). Вы можете сделать это в любое время на странице <a href=\"/bitrix/admin/seo_sitemap.php?lang=ru\">Сервисы — Поисковая оптимизация — Настройка sitemap.xml</a>, нажав кнопку <i>Запустить</i> у автоматически созданной настройки.</p><p><a href=\"/bitrix/admin/seo_sitemap.php?lang=ru\"><img src=\"/bitrix/wizards/citrus/site_tszh/images/sitemap.png\" alt=\"Запуск генерации Sitemap\"></a></p>";
$MESS["WIZ_SITE_SETTINGS_NOTE"] = "Все указанные данные будут опубликованы на сайте и использоваться для связи с вами.";
$MESS["WIZ_SITE_SETTINGS_DATA_SOURSE"] = "Для быстрого заполнения данных загрузите бриф с нужными данными.";
$MESS["WIZ_SITE_SETTINGS_DATA_SOURSE_HELP"] = "Образец файла можно запросить тут: 
+7 (495) 777-25-43, +7 (836) 249-46-89,  <a href=\"mailto:otr@rarus.ru\">otr@rarus.ru</a>";

$MESS["WIZ_ERROR_REQ_MONETA_EMAIL"] = "E-mail для взаимодействия с сервисом Монета.ру указан не верно";

$MESS["WIZ_ERROR_SUBSCRIBE_SETTING"] = "Ошибка настройки рассылки «#SUBSCRIBE#»: #ERROR#";

$MESS["WIZ_MONETA_ENABLED_AUTO_INFO"] = "Нажимая кнопку \"Далее\" ВЫ разрешаете возможность приёма платежей через платёжный сервис \"НКО&nbsp;\"МОНЕТА\" лицензия ЦБ РФ №3508-К от 02 июля 2012 года";
$MESS["WIZ_MONETA_ENABLED_HOW_TO_DISABLE"] = "<p style='color: #000;'>Отключить данную возможность для объекта управления можно после установки сайта в административном разделе <b>Сервисы</b> - <b>ЖКХ</b> - <b>Объекты управления</b>. Для этого нужно убрать галочку, разрешающую прием платежей, в форме редактирования объекта управления.</p>";

$MESS["WIZ_MAKE_BACKUP_TITLE"] = "Резервное копирование";
$MESS["WIZ_MAKE_BACKUP"] = "Запустить фоновое создание резервной копий после окончания установки шаблона";
$MESS["WIZ_MAKE_AUTO_BACKUP"] = "Запустить регулярное создание резервной копий после окончания установки шаблона";
$MESS["WIZ_MAKE_DIFF_SETTINGS"] = "Дополнительные настройки сайта ЖКХ";
$MESS["WIZ_MAKE_EVENTS"] = "Установить настройки журнала событий";
$MESS["WIZ_MAKE_ERROR_REPORTING"] = "Отключить вывод ошибок";
$MESS["WIZ_MAKE_CDN_ACTIVE"] = "Включить ускорение CDN";
$MESS["WIZ_MAKE_COMPOSITE_AUTO"] = "Включить автокомпозит";
$MESS["WIZ_MAKE_CACHE_AUTO"] = "Включить автокэширование";
$MESS["WIZ_MAKE_GROUP_SETTINGS"] = "Включить настройки политики безопасности в группе \"Администраторы\"";


$MESS["WIZ_SHEET_BY_NAME"] = "Заполнение данных";
$MESS["WIZ_SHEET_IMPORT_SUCCESS_ALL"] = "Данные успешно заполнены";
$MESS["WIZ_SHEET_IMPORT_SUCCESS"] = "Все остальные данные были успешно заполнены";
$MESS["WIZ_WORKSHEET_MISSING"] = "Не найден лист с названием: ";
$MESS["WIZ_SHEET_FILE_ERROR"] = "Некорректный формат файла";
