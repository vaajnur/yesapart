<?
$MESS["SERVICE_MAIN_SETTINGS"] = "Налаштування сайту";
$MESS["SERVICE_IBLOCK"] = "Інформаційні блоки";
$MESS["SERVICE_GALLERY_DEMO_DATA"] = "Фото та відеогалерея";
$MESS["SERVICE_IBLOCK_DEMO_DATA"] = "Новини, корисна інформація, оголошення ти \"питання-відповідь\"";
$MESS["SERVICE_TSZH_DEMO_DATA"] = "Налаштування модуля 1С: Сайт ЖКГ";
$MESS["SERVICE_FORUM_DEMO_DATA"] = "Форум";
$MESS["SERVICE_BLOG_DEMO_DATA"] = "Блог";
$MESS["SERVICE_TSZHTICKETS_DEMO_DATA"] = "Аварійно-диспечерська служба";
$MESS["SERVICE_VOTE_DEMO_DATA"] = "Голосування";
$MESS["SERVICE_TSZHPAYMENT_DEMO_DATA"] = "Сплата послуг на сайті";
$MESS["SERVICE_TSZH_SUBSCRIBE"] = "Розсилки";
$MESS["SERVICE_TSZH_DEBTORS"] = "список боржників";
$MESS["SERVICE_TSZH_OPERATOR"] = "Оператори прийому платежів";
$MESS["SERVICE_FINISH"] = "Завершення настройки";