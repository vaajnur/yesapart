<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 22.03.2018 12:36
 */

class TszhSettingsStep extends CWizardStep
{
	function InitStep()
	{
		$this->SetStepID("tszh_settings");
		$this->SetTitle(GetMessage("WIZ_STEP_TSZH"));

		$this->SetPrevStep("site_settings");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
		$this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));

		$wizard =& $this->GetWizard();
		$wizard->solutionName = "site_tszh";

		$wizard->SetDefaultVar("monetaStepEnabled", CModule::IncludeModule("citrus.tszhpayment") && !CTszhFunctionalityController::isUkrainianVersion());
		$this->SetNextStep($wizard->GetVar("monetaStepEnabled", true) ? "moneta_settings" : "additional_settings");

		$siteID = $wizard->GetVar("siteID");
		if ($siteID !== null && CModule::IncludeModule("citrus.tszh") && !$_SESSION['WIZARD_data_from_file'])
		{
			$arTszh = CTszh::GetList(Array(), Array("SITE_ID" => $siteID), false, false, Array("*", "UF_*"))->Fetch();
			if (!is_array($arTszh))
			{
				$arTszh = Array(
                    "ORGANIZATION_TYPE" => "2",
                    "NAME" => GetMessage("WIZ_TSZH_OF_NAME_DEF"),
					"INN" => "1234567890",
					"KPP" => "123456789",
					"KBK" => "00000000000000000000",
					"OKTMO" => "00000000000",
					"RSCH" => "0000 0000 0000 0000 0000",
					"BANK" => GetMessage("WIZ_TSZH_BANK_DEF"),
					"KSCH" => "30101 810 4 0000 0000225",
					"BIK" => "04 00 99 050 000",
					"HEAD_NAME" => "",
					"LEGAL_ADDRESS" => $wizard->GetVar('siteAddress', true),
					"OFFICE_HOURS" => "",
					"MONETA_ENABLED" => "",
					"MONETA_OFFER" => "1",
					"MONETA_EMAIL" => $wizard->GetVar('siteEmail', true),
					"RECEIPT_TEMPLATE" => BX_ROOT . "/components/citrus/tszh.receipt/templates/post-354",
					"IS_BUDGET" => "",
				);
			}
			// todo �������� ���������� ������� � ��������� ����� �������� ����������, ���������� �� ������� ����
			$wizard->SetDefaultVars(
				Array(
				    "org_type" => $arTszh["ORGANIZATION_TYPE"],
                    "org_title" => $arTszh["NAME"],
					"org_inn" => $arTszh["INN"],
					"org_kpp" => $arTszh["KPP"],
					"org_kbk" => $arTszh["KBK"],
					"org_oktmo" => $arTszh["OKTMO"],
					"org_rsch" => $arTszh["RSCH"],
					"org_bank" => $arTszh["BANK"],
					"org_ksch" => $arTszh["KSCH"],
					"org_bik" => $arTszh["BIK"],
					"org_is_budget" => $arTszh["IS_BUDGET"],
				)
			);
			$setDefaultTszh = array(
				"HEAD_NAME",
				"LEGAL_ADDRESS",
				"OFFICE_HOURS",
				"MONETA_ENABLED",
				"MONETA_OFFER",
				"MONETA_EMAIL",
				"RECEIPT_TEMPLATE",
			);
			foreach ($setDefaultTszh as $field)
			{
				if (isset($arTszh[$field]))
				{
					$wizard->SetDefaultVar($field, $arTszh[$field]);
				}
			}
		}
	}

	function ShowStep()
	{
		global $APPLICATION;

		$wizard =& $this->GetWizard();

		$this->content .= '<div class="wizard-input-form">
			<div class="wizard-input-form-block">
				<h4><label for="org_title">' . GetMessage("WIZ_TSZH_OF_NAME") . '</label></h4>
				<div class="wizard-input-form-block-content">
					<div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'org_title', array(
				"style" => "width:100%",
				"id" => "org_title",
			)) . '</div>
				</div>
			</div>
		    
			<div class="wizard-input-form-block">
				<h4><label>' . GetMessage("WIZ_TSZH_BANK_TITLE") . '</label></h4>
				<div class="wizard-input-form-block-content">
					<table class="data-table-no-border">
					    <tr>
							<th width="">' . GetMessage("WIZ_TSZH_ORGANIZATION_TYPE") . ':</th>
							<td width="">
							    <div class="wizard-input-form-field wizard-input-form-field-select">
							        ' . $this->ShowSelectField(
							            'org_type',
                                        array(
                                            "2"=> GetMessage("WIZ_TSZH_LEGAL_PERSONALITY"),
                                            "3"=> GetMessage("WIZ_TSZH_IS_BUDGET"),
                                            "1"=> GetMessage("WIZ_TSZH_SOLE_TRADER")
                                        ),
                                        array(
                                            "id" => "tszh-field__select-org-type",
                                            "style" => "width:100%")
                                    ) . '
                                </div>
                            </td>
						</tr>
						<tr id="tr_INN">
							<th width="35%">' . GetMessage("WIZ_TSZH_INN") . ':</th>
							<td width="65%"><div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'org_inn', array("style" => "width:100%")) . '</div></td>
						</tr>
						<tr id="tr_KPP">
							<th>' . GetMessage("WIZ_TSZH_KPP") . ':</th>
							<td><div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'org_kpp', array("style" => "width:100%")) . '</div></td>
						</tr>
						<tr id="tr_RSCH">
							<th>' . GetMessage("WIZ_TSZH_RSCH") . ':</th>
							<td><div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'org_rsch', array("style" => "width:100%")) . '</div></td>
						</tr>
						<tr id="tr_BANK">
							<th>' . GetMessage("WIZ_TSZH_BANK") . ':</th>
							<td><div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'org_bank', array("style" => "width:100%")) . '</div></td>
						</tr>
						<tr id="tr_KSCH">
							<th>' . GetMessage("WIZ_TSZH_KSCH") . ':</th>
							<td><div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'org_ksch', array("style" => "width:100%")) . '</div></td>
						</tr>
						<tr id="tr_BIK">
							<th>' . GetMessage("WIZ_TSZH_BIK") . ':</th>
							<td><div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'org_bik', array("style" => "width:100%")) . '</div></td>
						</tr>
						<tr id="tr_IS_BUDGET">
							<th>' . GetMessage("WIZ_TSZH_IS_BUDGET") . ':</th>
							<td><div class="wizard-input-form-field">' . $this->ShowCheckboxField('org_is_budget', 'Y', array('id' => 'tszh-field__is-budget')) . '</div></td>
						</tr>
						<tr id="tr_KBK">
							<th>' . GetMessage("WIZ_TSZH_KBK") . ':</th>
							<td><div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'org_kbk', array("style" => "width:100%")) . '</div></td>
						</tr>
						<tr id="tr_OKTMO">
							<th>' . GetMessage("WIZ_TSZH_OKTMO") . ':</th>
							<td><div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'org_oktmo', array("style" => "width:100%")) . '</div></td>
						</tr>
						<tr>
							<th>' . GetMessage("WIZ_TSZH_DOWNLOAD_DEMODATA") . ':</th>
							<td><div class="wizard-input-form-field">' . ($wizard->GetVar("_download_demodata", true ) == "N" ? $this->ShowCheckboxField('download_demodata', 'N', array('id' => 'tszh-field__download_demodata')) : $this->ShowCheckboxField('download_demodata', 'Y', array('id' => 'tszh-field__download_demodata', 'checked' => true))) . '</div></td>
						</tr>
						<tr>
							<th></th>
							<td>' . (new \Citrus\Tszh\Demodata())->showNoteForWizard() . '</td>
						</tr>
					</table>
				</div>
			</div>
		    
			<div class="wizard-input-form-block">
				<h4><label for="HEAD_NAME">' . GetMessage("WIZ_TSZH_HEAD_NAME") . '</label></h4>
				<div class="wizard-input-form-block-content">
					<div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'HEAD_NAME', array(
				"style" => "width:100%",
				"id" => "HEAD_NAME",
			)) . '</div>
				</div>
			</div>
		    
			<div class="wizard-input-form-block">
				<h4><label for="LEGAL_ADDRESS">' . GetMessage("WIZ_TSZH_LEGAL_ADDRESS") . '</label></h4>
				<div class="wizard-input-form-block-content">
					<div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'LEGAL_ADDRESS', array(
				"style" => "width:100%",
				"id" => "LEGAL_ADDRESS",
			)) . '</div>
				</div>
			</div>';

		$this->content .= '<script>
//              ����� ���� �������� �����, ����� �������! 
//			     var is_budget = document.getElementById("tszh-field__is-budget");
//			     is_budget.addEventListener("change", function(){
//			         document.getElementById("tr_KBK").style.display = document.getElementById("tr_OKTMO").style.display = is_budget.checked ? "table-row" : "none";
//			     });
//			     var event = new Event("change");
//			     is_budget.dispatchEvent(event);
			     
			     var organizationTypeSelect = document.getElementById("tszh-field__select-org-type");
			     
			     function hideOrganizationForm(organizationTypeSelect) {
			         
			         var selectedIndex = organizationTypeSelect.options.selectedIndex;
			         var selectedValue = organizationTypeSelect.options[selectedIndex].value;
			         
			         if ( selectedValue == "0") {
			             document.getElementById("tr_INN").style.display = "none";    
			             document.getElementById("tr_KPP").style.display = "none";    
			             document.getElementById("tr_RSCH").style.display = "none";    
			             document.getElementById("tr_BANK").style.display = "none";    
			             document.getElementById("tr_KSCH").style.display = "none";    
			             document.getElementById("tr_BIK").style.display = "none";    
			             document.getElementById("tr_IS_BUDGET").checked = false;
			             document.getElementById("tr_IS_BUDGET").style.display = "none";    
			             document.getElementById("tr_KBK").style.display = "none";    
			             document.getElementById("tr_OKTMO").style.display = "none";  
			         } else if (selectedValue == "1") {
			             document.getElementById("tr_INN").style.display = "table-row";    
			             document.getElementById("tr_KPP").style.display = "none";    
			             document.getElementById("tr_RSCH").style.display = "table-row";    
			             document.getElementById("tr_BANK").style.display = "table-row";    
			             document.getElementById("tr_KSCH").style.display = "table-row";    
			             document.getElementById("tr_BIK").style.display = "table-row";    
			             document.getElementById("tr_IS_BUDGET").checked = false;
			             document.getElementById("tr_IS_BUDGET").style.display = "none";    
			             document.getElementById("tr_KBK").style.display = "none";    
			             document.getElementById("tr_OKTMO").style.display = "none";    
			         } else if (selectedValue == "2") {
			             document.getElementById("tr_INN").style.display = "table-row";
			             document.getElementById("tr_KPP").style.display = "table-row";
			             document.getElementById("tr_RSCH").style.display = "table-row";    
			             document.getElementById("tr_BANK").style.display = "table-row";    
			             document.getElementById("tr_KSCH").style.display = "table-row";    
			             document.getElementById("tr_BIK").style.display = "table-row";
			             document.getElementById("tr_IS_BUDGET").checked = false;
			             document.getElementById("tr_IS_BUDGET").style.display = "none";
			             document.getElementById("tr_KBK").style.display = "none";
			             document.getElementById("tr_OKTMO").style.display = "none"; 
			         } else if (selectedValue == "3") {
			             document.getElementById("tr_INN").style.display = "table-row";
			             document.getElementById("tr_KPP").style.display = "table-row";
			             document.getElementById("tr_RSCH").style.display = "table-row";    
			             document.getElementById("tr_BANK").style.display = "table-row";    
			             document.getElementById("tr_KSCH").style.display = "table-row";    
			             document.getElementById("tr_BIK").style.display = "table-row";
			             document.getElementById("tr_IS_BUDGET").checked = true;
			             document.getElementById("tr_IS_BUDGET").style.display = "none";
			             document.getElementById("tr_KBK").style.display = "table-row";
			             document.getElementById("tr_OKTMO").style.display = "table-row";
			         }
			     }
			     
			     hideOrganizationForm(organizationTypeSelect);
			     organizationTypeSelect.addEventListener("change", function() {
			        hideOrganizationForm(organizationTypeSelect);    
			     });
			</script>';

		ob_start();
		$APPLICATION->IncludeComponent(
			"citrus:tszh.office_hours.edit",
			"",
			Array(
				"OFFICE_HOURS" => $wizard->GetVar("OFFICE_HOURS", true),
				//"FROM_WIZARD" => "Y",
			),
			false
		);
		$officeHoursHtml = ob_get_contents();
		ob_end_clean();

		$this->content .= '
			<div class="wizard-input-form-block">
				<h4><label>' . GetMessage("WIZ_TSZH_OFFICE_HOURS") . '</label></h4>
				<div class="wizard-input-form-block-content" style="text-align: center;">' . $officeHoursHtml . '</div>
			</div>
		</div>';
	}

	function OnPostForm()
	{
		$wizard =& $this->GetWizard();

		$arErrors = array();
		$arOfficeHoursErrors = array();

		$inn = $wizard->getVar("org_inn");
		$kpp = $wizard->getVar("org_kpp");
		$orgType = $wizard->getVar("org_type");
        if ($orgType == "1") {
            if (empty($inn)) {
                $arErrors[] = array(
                    "text" => getMessage("WIZ_TSZH_INN_ERROR_REQ"),
                    "id" => "org_inn",
                );
            } else if (!preg_match('/^[\d]{12}$/', $inn)) {
                $arErrors[] = array(
                    "text" => getMessage("WIZ_TSZH_INN_ERROR_INVALID"),
                    "id" => "org_inn",
                );
            }
        } else if ($orgType == "2" || $orgType == "3") {
            if (empty($inn)) {
                $arErrors[] = array(
                    "text" => getMessage("WIZ_TSZH_INN_ERROR_REQ"),
                    "id" => "org_inn",
                );
            } else if (!preg_match('/^[\d]{10}$/', $inn)) {
                $arErrors[] = array(
                    "text" => getMessage("WIZ_TSZH_INN_ERROR_INVALID"),
                    "id" => "org_inn",
                );
            }

            if (empty($kpp)) {
                $arErrors[] = array(
                    "text" => getMessage("WIZ_TSZH_KPP_ERROR_REQ"),
                    "id" => "org_kpp",
                );
            } else if (!preg_match('/^[0-9]{9}$/', $kpp)) {
                $arErrors[] = array(
                    "text" => getMessage("WIZ_TSZH_KPP_ERROR_INVALID"),
                    "id" => "org_kpp",
                );
            }
        }

		$wizard->SetVar("OFFICE_HOURS", $_POST["OFFICE_HOURS"]);
		CBitrixComponent::includeComponentClass("citrus:tszh.office_hours.edit");
		$arOfficeHours = $_POST["OFFICE_HOURS"];
		if (CCitrusTszhOfficeHoursEdit::checkValues($arOfficeHours, $arOfficeHoursErrors))
		{
			$wizard->SetVar("OFFICE_HOURS", $arOfficeHours);
		}

		$arErrors = array_merge($arErrors, $arOfficeHoursErrors);
		if (!empty($arErrors))
		{
			foreach ($arErrors as $arError)
			{
				$this->SetError($arError["text"], $arError["id"]);
			}
		}
	}
}