<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 22.03.2018 12:36
 */

if (ini_get('mbstring.func_overload') & 2) {
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/wizards/citrus/site_tszh/classes/Classes_overload2/PHPExcel.php");
} else {
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/wizards/citrus/site_tszh/classes/Classes_overload0/PHPExcel.php");
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/wizards/citrus/site_tszh/classes/idna_convert.class.php");


class SiteSettingsStep extends CSiteSettingsWizardStep
{
	function InitStep()
	{
		$wizard =& $this->GetWizard();
		$wizard->solutionName = "site_tszh";
		parent::InitStep();

		$this->SetNextStep("tszh_settings");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
		$this->SetTitle(GetMessage("WIZ_STEP_SITE_SET"));

		$siteID = $wizard->GetVar("siteID");
		if (COption::GetOptionString("citrus.tszh", "wizard_tszh_installed", "N") == "Y")
		{
			$this->SetNextStep("data_install");
		}
		else
		{
			$this->SetNextStep("tszh_settings");
		}

		$site = CSite::GetByID($siteID)->Fetch();
		$email = $site["EMAIL"] ? $site["EMAIL"] : COption::GetOptionString("main", "email_from", "N", $siteID);

		if ($_SESSION['WIZARD_data_from_file'])
		{
			$wizard->defaultVars = [];
		}
		else
		{
			$wizard->SetDefaultVars(
				Array(
					"siteName" => $this->GetFileContent(WIZARD_SITE_PATH . "include/company_name.php", GetMessage("WIZ_COMPANY_NAME_DEF")),
					"siteTelephone" => $this->GetFileContent(WIZARD_SITE_PATH . "include/telephone.php", GetMessage("WIZ_COMPANY_TELEPHONE_DEF")),
					"siteTelephoneDisp" => $this->GetFileContent(WIZARD_SITE_PATH . "include/telephone_disp.php", GetMessage("WIZ_COMPANY_TELEPHONE_DISP_DEF")),
					"siteEmail" => strlen($email) > 0 ? $email : 'info@' . $_SERVER["SERVER_NAME"],
					"siteAddress" => $this->GetFileContent(WIZARD_SITE_PATH . "include/address.php", GetMessage("WIZ_COMPANY_ADDRESS_DEF")),
					"serverName" => SITE_SERVER_NAME ? SITE_SERVER_NAME : (COption::GetOptionString('main', 'server_name', $_SERVER['HTTP_HOST'])),
				)
			);
		}
	}

	function ShowStep()
	{
		$this->content .= '<div class="wizard-input-form">';
		$this->content .= '<p>' . GetMessage("WIZ_SITE_SETTINGS_NOTE") . '</p>';

		$this->content .= '<div style="border: 1px solid #c5d4a7;padding: 10px;margin-left: 30px;width: 84%;">';
		$this->content .= '<p>' . GetMessage("WIZ_SITE_SETTINGS_DATA_SOURSE") . '</p>';
		$this->content .= '<div class="wizard-input-form">';
		$this->content .= CFile::InputFile("FILE_ID", 20, null);
		$this->content .= '</div>';

		if ($_SESSION['WIZARD_data_from_file'])
		{
			$wizard = $this->GetWizard();
			$wizard->defaultVars = [];
		}
		if ($this->site_settings_stepErrors)
		{
			$stepErrors = implode('<br>', $this->site_settings_stepErrors);
		}
		if ($stepErrors)
		{
			$this->content .= '<br><span style="color:#FF0000">' . $stepErrors . '</span>';
			$this->content .= '<br><br><span style="color:green">' . GetMessage("WIZ_SHEET_IMPORT_SUCCESS") . '</span>';
		}
		elseif ($this->site_settings_sheet_success)
		{
			$this->content .= '<br><span style="color:green">' . $this->site_settings_sheet_success . '</span>';
		}

		$this->content .= '<p>' . GetMessage("WIZ_SITE_SETTINGS_DATA_SOURSE_HELP") . '</p>';
		$this->content .= '</div>';
		$this->content .= '
		<script>
		function handleFileSelect(evt) {
			SubmitForm("next");
		   }
		 document.getElementsByName("FILE_ID")[0].addEventListener("change", handleFileSelect,false);
		</script>
		';

		$this->content .= '
		<div class="wizard-input-form-block">
			<h4><label for="siteName">' . GetMessage("WIZ_COMPANY_NAME") . '</label></h4>
			<div class="wizard-input-form-block-content">
				<div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'siteName', array(
				"style" => "width:100%",
				"id" => "siteName",
			)) . '</div>
			</div>
		</div>';
		$this->content .= '
		<div class="wizard-input-form-block">
			<h4><label for="siteTelephone">' . GetMessage("WIZ_COMPANY_TELEPHONE") . '</label></h4>
			<div class="wizard-input-form-block-content">
				<div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'siteTelephone', array(
				"style" => "width:100%",
				"id" => "siteTelephone",
			)) . '</div>
			</div>
		</div>';
		$this->content .= '
		<div class="wizard-input-form-block">
			<h4><label for="siteTelephoneDisp">' . GetMessage("WIZ_COMPANY_TELEPHONE_DISP") . '</label></h4>
			<div class="wizard-input-form-block-content">
				<div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'siteTelephoneDisp', array(
				"style" => "width:100%",
				"id" => "siteTelephoneDisp",
			)) . '</div>
			</div>
		</div>';
		$this->content .= '
		<div class="wizard-input-form-block">
			<h4><label for="siteEmail">' . GetMessage("WIZ_COMPANY_EMAIL") . '</label></h4>
			<div class="wizard-input-form-block-content">
				<div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'siteEmail', array(
				"style" => "width:100%",
				"id" => "siteEmail",
			)) . '</div>
			</div>
		</div>';
		$this->content .= '
		<div class="wizard-input-form-block">
			<h4><label for="siteAddress">' . GetMessage("WIZ_COMPANY_ADDRESS") . '</label></h4>
			<div class="wizard-input-form-block-content">
				<div class="wizard-input-form-field wizard-input-form-field-textarea">' . $this->ShowInputField('textarea', 'siteAddress', array(
				"style" => "width:100%",
				"rows" => "2",
				"id" => "siteAddress",
			)) . '</div>
			</div>
		</div>';
		$this->content .= '
		<div class="wizard-input-form-block">
			<h4><label for="siteAddress">' . GetMessage("WIZ_COMPANY_SERVER_NAME") . '</label></h4>
			<div class="wizard-input-form-block-content">
				<p>' . GetMessage("WIZ_COMPANY_SERVER_NAME_TIP") . '</p>
				<div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'serverName', array(
				"style" => "width:100%",
				"id" => "serverName",
			)) . '</div>
				<p>' . GetMessage("WIZ_COMPANY_SERVER_NAME_TEXT") . '</p>
			</div>
		</div>';
		$this->content .= '</div>';
	}

	function OnPostForm()
	{
		$wizard =& $this->GetWizard();

		$file = $_FILES["FILE_ID"];

		if (($_FILES["FILE_ID"]["type"] == "application/vnd.ms-excel") || ($_FILES["FILE_ID"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") || (strripos($file["name"], "xls") > 0))
		{
			PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			$reader = PHPExcel_IOFactory::createReaderForFile($file["tmp_name"]);
			$reader->setReadDataOnly(true);
			$excel = $reader->load($file["tmp_name"]);

			if (LANG_CHARSET == "windows-1251")
			{
				$objWorksheet = $excel->getSheetByName(iconv('cp1251', 'utf-8', GetMessage("WIZ_SHEET_BY_NAME")));
			}
			else
			{
				$objWorksheet = $excel->getSheetByName(GetMessage("WIZ_SHEET_BY_NAME"));
			}

			//   $objWorksheet = iconv('cp1251','utf-8', "���������� ������");

			$this->site_settings_stepErrors = [];
			unset($this->site_settings_sheet_success);

			if (!$objWorksheet)
			{
				$this->site_settings_stepErrors[] = GetMessage("WIZ_WORKSHEET_MISSING") . '"' . GetMessage("WIZ_SHEET_BY_NAME") . '"';
			}
			else
			{
				/*
				"siteName" => getCell('U32') => �������� ����������� (����� ������������ �� ������� �������� �����),
				"siteAddress" => getCell('V33') => ����� ����������� ����������� (������, ������,...),
				"siteTelephone" => getCell('V34') => ������� ����������� (� ��������� ���� ������),
				"siteEmail" => getCell('V35') => E-mail �����������,
				"siteTelephoneDisp" => getCell('U36') => ������� �������������,
				"serverName" => getCell('W13') => ���� � ��� ���� ������������ ����, ������� �������� ����� � ������ � ��������� ������������,
				"org_title" => getCell('U32') => �������� ����������� (����� ������������ �� ������� �������� �����),
				"org_inn" => getCell('V38') => ���,
				"org_kpp" => getCell('U39') => ���,
				"org_rsch" => getCell('U40') => ��������� ����,
				"org_bank" => getCell('U41') => ����,
				"org_ksch" => getCell('U42') => ����.����:,
				"org_bik" => getCell('U43') => ���,
				"HEAD_NAME" => getCell('U44') => ��� ������������,
				"LEGAL_ADDRESS" => getCell('U45') => �����������  �����,
				"MONETA_EMAIL" => getCell('U64') => ������� E-mail ��� �������������� � �������� ������.��,
				"_download_demodata" => "N",

				"tszh_subscribe_Debtors_ACTIVE" => getCell('AN73') == "1" ? "Y" : "N"  => ��������� ����������� ��������� (��/���),
				"tszh_subscribe_Debtors_param_DATE" => getCell('AB75') => ��������� �����������, ���� ����� ������������� ������ ��������� ��� ����� (���):,
				"tszh_subscribe_Debtors_param_MIN_SUM" => getCell('AB74') => ������� ����� ������ ��� �������� �����������:,

				"tszh_subscribe_MeterValues_ACTIVE" => getCell('AN77') == "1" ? "Y" : "N" =>  ������� ����� ������ ��� �������� �����������:,
				"tszh_subscribe_MeterValues_param_DATE" => getCell('AB78') => ������� ����� ������ ��� �������� �����������:,

				"tszh_subscribe_MeterVerifications_ACTIVE" => getCell('AN80') == "1" ? "Y" : "N" => ��������� ����������� � ���� ������� ��������� (��/���)         ,
				"tszh_subscribe_MeterVerifications_param_CHECK_INTERVAL" => getCell('AB81') => ������� �� ������� ���� �� ����������� ���� ������� ������� �������� �����������:,

				"tszh_subscribe_Receipts_ACTIVE" => getCell('AN84') == "1" ? "Y" : "N" => ��������� ��������� �� ������ �� ����������� ����� (��/���)
				*/

				$file_data = Array(
					"siteName" => $objWorksheet->getCell('U32')->getValue(),
					"siteAddress" => $objWorksheet->getCell('V33')->getValue(),
					"siteTelephone" => $objWorksheet->getCell('V34')->getValue(),
					"siteEmail" => $objWorksheet->getCell('V35')->getValue(),
					"siteTelephoneDisp" => $objWorksheet->getCell('U36')->getValue(),
					"serverName" => $objWorksheet->getCell('W13')->getValue(),
				);

				if (LANG_CHARSET == "windows-1251")
				{
					foreach ($file_data as $key => $value)
					{
						if ($key != "serverName")
						{
							$file_data[$key] = iconv("UTF-8", "CP1251", $value);
						}
					}
				}

				foreach ($file_data as $key => $data)
				{
					$wizard->SetVar($key, $data);
				}


				/*====================== �������� SITE SETTINGS===============*/
				static $requiredFields = Array(
					"siteName" => "WIZ_COMPANY_NAME",
					"siteTelephone" => "WIZ_COMPANY_TELEPHONE",
					"siteTelephoneDisp" => "WIZ_COMPANY_TELEPHONE_DISP",
					"siteEmail" => "WIZ_COMPANY_EMAIL",
					"siteAddress" => "WIZ_COMPANY_ADDRESS",
					"serverName" => "WIZ_COMPANY_SERVER_NAME",
				);

				$values = array();
				foreach ($requiredFields as $field => $titleMess)
				{
					$values[$field] = trim($file_data[$field]);
					if (strlen($values[$field]) <= 0)
					{
						$this->site_settings_stepErrors[] = GetMessage("WIZ_COMPANY_MISSING_FIELD", array("#FIELD#" => GetMessage($titleMess)));

					}
				}

				$serverName = $file_data["serverName"];
				$siteUrl = (CMain::IsHTTPS() ? "https" : "http") . '://' . $serverName;
				if (preg_match('#^(http|https)://([A-Z0-9][A-Z0-9_-]*(?:.[A-Z0-9][A-Z0-9_-]*)+):?(d+)?/?$#i', $siteUrl) !== 1)
				{
					//  ��� ���������� ���������
					$idn = new idna_convert(array('idn_version' => 2008));
					$punycode = isset($serverName) ? stripslashes($serverName) : '';
					$punycode = (stripos($punycode, 'xn--') !== false) ? $idn->decode($punycode) : $idn->encode($punycode);

					$siteUrl_punycode = (CMain::IsHTTPS() ? "https" : "http") . '://' . $punycode;

					if (preg_match('#^(http|https)://([A-Z0-9][A-Z0-9_-]*(?:.[A-Z0-9][A-Z0-9_-]*)+):?(d+)?/?$#i', $siteUrl_punycode) !== 1)
					{
						$this->site_settings_stepErrors[] = GetMessage("WIZ_COMPANY_SERVER_NAME_WRONG");

					}
					else
					{
						if (LANG_CHARSET == "windows-1251")
						{
							$wizard->SetVar("serverName", iconv("UTF-8", "CP1251", $punycode));
						}
						else
						{
							$wizard->SetVar("serverName", $punycode);
						}
					}

				}

				if (preg_match('#^www\.#i', $serverName))
				{
					$this->site_settings_stepErrors[] = GetMessage("WIZ_COMPANY_SERVER_NAME_WRONG");
				}
				if (!CTszh::checkEmail($file_data["siteEmail"]))
				{
					$this->site_settings_stepErrors[] = GetMessage("WIZ_COMPANY_SITE_EMAIL_WRONG");
				}

				/*====================== �������� TSZH SETTINGS===============*/
				$file_data_tszh_settings = Array(
					"org_title" => $objWorksheet->getCell('U32')->getValue(),
					"org_inn" => $objWorksheet->getCell('V38')->getValue(),
					"org_kpp" => $objWorksheet->getCell('U39')->getValue(),
					"org_rsch" => $objWorksheet->getCell('U40')->getValue(),
					"org_bank" => $objWorksheet->getCell('U41')->getValue(),
					"org_ksch" => $objWorksheet->getCell('U42')->getValue(),
					"org_bik" => $objWorksheet->getCell('U43')->getValue(),
					"HEAD_NAME" => $objWorksheet->getCell('U44')->getValue(),
					"LEGAL_ADDRESS" => $objWorksheet->getCell('U45')->getValue(),
					"MONETA_EMAIL" => $objWorksheet->getCell('U64')->getValue(),
					"_download_demodata" => "N",
				);

				if (LANG_CHARSET == "windows-1251")
				{
					foreach ($file_data_tszh_settings as $key => $value)
					{
						$file_data_tszh_settings[$key] = iconv("UTF-8", "CP1251", $value);
					}
				}

				foreach ($file_data_tszh_settings as $key => $data)
				{
					$wizard->SetVar($key, $data);
				}

				$arErrors = array();
				$inn = $file_data_tszh_settings["org_inn"];
				if (empty($inn))
				{
					$arErrors[] = array(
						"text" => getMessage("WIZ_TSZH_INN_ERROR_REQ"),
						"id" => getMessage("org_inn"),
					);
				}
				elseif (!preg_match('/^[\d]{10}$/', $inn))
				{
					$arErrors[] = array(
						"text" => getMessage("WIZ_TSZH_INN_ERROR_INVALID"),
						"id" => getMessage("org_inn"),
					);
				}

				/*====================== �������� ADDITIONALS SETTINGS===============*/
				$file_data_addition_settings = Array(
					"tszh_subscribe_Debtors_ACTIVE" => $objWorksheet->getCell('AN73')->getValue() == "1" ? "Y" : "N",
					"tszh_subscribe_Debtors_param_DATE" => $objWorksheet->getCell('AB75')->getValue(),
					"tszh_subscribe_Debtors_param_MIN_SUM" => $objWorksheet->getCell('AB74')->getValue(),

					"tszh_subscribe_MeterValues_ACTIVE" => $objWorksheet->getCell('AN77')->getValue() == "1" ? "Y" : "N",
					"tszh_subscribe_MeterValues_param_DATE" => $objWorksheet->getCell('AB78')->getValue(),

					"tszh_subscribe_MeterVerifications_ACTIVE" => $objWorksheet->getCell('AN80')->getValue() == "1" ? "Y" : "N",
					"tszh_subscribe_MeterVerifications_param_CHECK_INTERVAL" => $objWorksheet->getCell('AB81')->getValue(),

					"tszh_subscribe_Receipts_ACTIVE" => $objWorksheet->getCell('AN84')->getValue() == "1" ? "Y" : "N",
				);

				foreach ($file_data_addition_settings as $key => $data)
				{
					$wizard->SetVar($key, $data);
				}

				$arSubscribes = CTszhSubscribe::getSubscribes4Admin(false, $wizard);
				foreach ($arSubscribes as $className => $arSubscribe)
				{
					if ($arSubscribe["SETTINGS_COLLECTED"]["ACTIVE"] != "Y")
					{
						continue;
					}

					try
					{
						$className::checkParams($arSubscribe["SETTINGS_COLLECTED"]["PARAMS"], 0, false, true);
					}
					catch (\Bitrix\Main\ArgumentException $e)
					{
						$this->site_settings_stepErrors[] = getMessage("WIZ_ERROR_SUBSCRIBE_SETTING", array(
							"#SUBSCRIBE#" => $arSubscribe["NAME"],
							"#ERROR#" => $e->getMessage(),
						));
					}
				}
			}
			if (isset($this->site_settings_stepErrors))
			{
				$this->site_settings_sheet_success = getMessage("WIZ_SHEET_IMPORT_SUCCESS_ALL");
			}
			$_SESSION['WIZARD_data_from_file'] = true;
			$wizard->currentStepID = "site_settings";
		}
		elseif (is_set($_FILES["FILE_ID"]) && ($_FILES["FILE_ID"]["type"] != "")) {
			$_SESSION['WIZARD_data_from_file'] = false;
			unset($_FILES["FILE_ID"]);
			$this->SetError(GetMessage("WIZ_SHEET_FILE_ERROR"));
		}
		else
		{
			static $requiredFields = Array(
				"siteName" => "WIZ_COMPANY_NAME",
				"siteTelephone" => "WIZ_COMPANY_TELEPHONE",
				"siteTelephoneDisp" => "WIZ_COMPANY_TELEPHONE_DISP",
				"siteEmail" => "WIZ_COMPANY_EMAIL",
				"siteAddress" => "WIZ_COMPANY_ADDRESS",
				"serverName" => "WIZ_COMPANY_SERVER_NAME",
			);
			$values = array();
			foreach ($requiredFields as $field => $titleMess)
			{
				$values[$field] = trim($wizard->GetVar($field));
				if (strlen($values[$field]) <= 0)
				{
					$this->SetError(GetMessage("WIZ_COMPANY_MISSING_FIELD", array("#FIELD#" => GetMessage($titleMess))), $field);
				}
			}

			$serverName = $wizard->GetVar('serverName');
			$siteUrl = (CMain::IsHTTPS() ? "https" : "http") . '://' . $serverName;
			if (preg_match('#^(http|https)://([A-Z0-9][A-Z0-9_-]*(?:.[A-Z0-9][A-Z0-9_-]*)+):?(d+)?/?$#i', $siteUrl) !== 1)
			{
				$this->SetError(GetMessage("WIZ_COMPANY_SERVER_NAME_WRONG"), "serverName");
			}
			if (preg_match('#^www\.#i', $serverName))
			{
				$this->SetError(GetMessage("WIZ_COMPANY_SERVER_NAME_WRONG"), "serverName");
			}

			if (!CTszh::checkEmail($wizard->GetVar('siteEmail')))
			{
				$this->SetError(GetMessage("WIZ_COMPANY_SITE_EMAIL_WRONG"), "siteEmail");
			}
		}

	}

}