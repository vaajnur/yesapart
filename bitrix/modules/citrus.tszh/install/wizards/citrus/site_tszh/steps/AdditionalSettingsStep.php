<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 22.03.2018 12:38
 */

class AdditionalSettingsStep extends CWizardStep
{
	function InitStep()
	{
		$this->SetStepID("additional_settings");
		$this->SetTitle(GetMessage("WIZ_STEP_ADDITIONAL_SETTINGS"));

		$this->SetNextStep("data_install");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
		$this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));

		$wizard =& $this->GetWizard();
		$wizard->solutionName = "site_tszh";

		$this->SetPrevStep($wizard->GetVar("monetaStepEnabled", true) ? "moneta_settings" : "tszh_settings");

		if (CTszhFunctionalityController::checkSubscribe(false, $isSubscribeEdition) && $isSubscribeEdition)
		{
			$arSubscribes = CTszhSubscribe::getSubscribes4Admin(false, $wizard);
			foreach ($arSubscribes as $className => $arSubscribe)
			{
				foreach ($arSubscribe["SETTINGS"] as $code => $arSetting)
				{
					if ($code == "PARAMS")
					{
						foreach ($arSetting as $paramCode => $arParam)
						{
							if (!$arParam["readOnly"] && !$arParam["notShowInWizard"])
							{
								$wizard->SetDefaultVar($arParam["key"], $arParam["value"]);
							}
						}
					}
					else
					{
						$wizard->SetDefaultVar($arSetting["key"], $arSetting["value"]);
					}
				}
			}
		}
	}

	private function citrusTszhShowSubscribeInput($arSetting, $isChecked = false, $onChange = false)
	{
		if ($arSetting["type"] == "checkbox")
		{
			$methodName = "showCheckboxField";

			$arAddParams = array("id" => $arSetting["key"]);
			if (strlen($onChange))
			{
				$arAddParams["onchange"] = $onChange;
			}

			$arParams = array($arSetting["key"], "Y", $arAddParams + ($isChecked ? array("checked" => "checked") : array()));
		}
		elseif (in_array($arSetting["type"], array("int", "float", "string", "currency")))
		{
			$methodName = "showInputField";
			$arParams = array(
				"text",
				$arSetting["key"],
				array_merge($arSetting["htmlParams"], array(/*"style" => "width: 50px",*/
					"id" => $arSetting["key"],
				)),
			);
		}

        if ( is_callable(array($this, $methodName))) { $result = call_user_func_array(array($this, $methodName), $arParams); }

        return $result;
	}

	private function CanSendUpdatesInfo()
	{
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/update_client.php");
		$errorMessage = "";
		$arUpdateList  = CUpdateClient::GetUpdatesList($errorMessage, LANG, $stableVersionsOnly = "Y")['CLIENT'];
		$bFullVersion = ($arUpdateList !== false && isset($arUpdateList["CLIENT"]) && ($arUpdateList["CLIENT"][0]["@"]["ENC_TYPE"] == "F" || $arUpdateList["CLIENT"][0]["@"]["ENC_TYPE"] == "E" || $arUpdateList["CLIENT"][0]["@"]["ENC_TYPE"] == "T"));
		//return $bFullVersion;
		return true;
	}


	function ShowStep()
	{
		$wizard =& $this->GetWizard();

		$this->content .= '<div class="wizard-input-form">';
		$this->content .= '<div class="wizard-input-form-block">
			<h4><label>' . GetMessage("WIZ_TSZH_RECEIPT") . '</label></h4>
			<div class="wizard-input-form-block-content">
				<div class="wizard-input-form-field wizard-input-form-field-select">
					' . $this->ShowSelectField("RECEIPT_TEMPLATE", CTszhReceiptTemplateProcessor::getTemplatesForSelect()) . '
				</div>
			</div>
		</div>';

		if (CTszhFunctionalityController::checkSubscribe(false, $isSubscribeEdition) && $isSubscribeEdition)
		{
			$this->content .= '
			<div class="wizard-input-form-block">
				<h4><label>' . GetMessage("WIZ_TSZH_SUBSCRIBE") . '</label></h4>';

			$arSubscribes = CTszhSubscribe::getSubscribes4Admin(false, $wizard, $wizard->GetVar("siteID"));
			foreach ($arSubscribes as $className => $arSubscribe)
			{
				$this->content .= '<div class="wizard-input-form-block-content">';

				$activeId = false;
				$onChangeFuncName = false;
				$arParamIds = array();
				foreach ($arSubscribe["SETTINGS"] as $code => $arSetting)
				{
					if ($code == "PARAMS")
					{
						$arHtml = array();
						foreach ($arSetting as $paramCode => $arParam)
						{
							if ($arParam["readOnly"] || $arParam["notShowInWizard"])
							{
								continue;
							}

							$arParamIds[] = $arParam["key"];

							$isChecked = $onChange = false;
							if ($arParam["type"] == "checkbox")
							{
								$isChecked = $wizard->GetVar($arParam["key"], true) == "Y";
							}
							$html = '<label>' . $arParam["name"] . ": " . $this->citrusTszhShowSubscribeInput($arParam, $isChecked, $onChange) . '</label>';

							$arHtml[] = $html;
						}
						$this->content .= implode("<br>", $arHtml);
					}
					else
					{
						$isChecked = $onChange = false;
						if ($code == "ACTIVE")
						{
							$activeId = $arSetting["key"];
							$onChangeFuncName = "window.wizardCheckSubscribe" . $className::CODE . "Inputs";
							$onChange = $onChangeFuncName . "()";
							$isChecked = $wizard->GetVar($arSetting["key"], true) == "Y";
						}
						$this->content .= '<label>' . $this->citrusTszhShowSubscribeInput($arSetting, $isChecked, $onChange) . ' ' . $arSetting["name-wizard"] . '</label>';
						if ($code == "ACTIVE")
						{
							$this->content .= '<p style="color: #999; margin: .3em 0;">' . $arSubscribe["NOTE"] . '</p>';
						}
					}
				}

				if (strlen($activeId) && strlen($onChangeFuncName))
				{
					$this->content .= "
						<script type=\"text/javascript\">
							{$onChangeFuncName} = function() {\n";

					foreach ($arParamIds as $paramId)
					{
						$this->content .= "document.getElementById('{$paramId}').disabled = !document.getElementById('{$activeId}').checked;\n";
					}

					$this->content .= "}
							window.setTimeout('{$onChangeFuncName}()', 1);
						</script>";
				}

                $this->content .= '</div>';
            }
		}

        if ($this->CanSendUpdatesInfo()) {
            //	$this->content .= '<div class="wizard-input-form-block">
            //	<h4><label>' . GetMessage("WIZ_MAKE_BACKUP_TITLE") . '</label></h4>
            //	<div class="wizard-input-form-block-content">';
            $this->content .= '<input name="__wiz_can_send_updatesinfo" value="Y" type="hidden">';

            //	$this->content .= '<div class="wizard-input-form-field" style="float: left;">' . $this->ShowCheckboxField('make_backup', 'Y', array('id' => 'tszh-field__make_backup', 'checked' => true)) . '</div>';
            //	$this->content .= '<p style="color: #999; margin: .3em 0;">' . GetMessage("WIZ_MAKE_BACKUP") . '</p>';
            //	$this->content .= '</div>';
            //	$this->content .= '</div>';
            $this->content .= '<div class="wizard-input-form-block">
				<h4><label>' . GetMessage("WIZ_MAKE_DIFF_SETTINGS") . '</label></h4>
				<div class="wizard-input-form-block-content">';

            $this->content .= '<input name="__wiz_can_send_updatesinfo" value="Y" type="hidden">';
            $this->content .= '<div class="wizard-input-form-field" style="float: left;">' . $this->ShowCheckboxField('set_event', 'Y', array('id' => 'tszh-field__set_event', 'checked' => true)) . '</div>';
            $this->content .= '<p style="color: #999; margin: .3em 0;">' . GetMessage("WIZ_MAKE_EVENTS") . '</p>';
            $this->content .= '</div>';

            $this->content .= '<div class="wizard-input-form-block-content">';
            $this->content .= '<input name="__wiz_can_send_updatesinfo" value="Y" type="hidden">';
            $this->content .= '<div class="wizard-input-form-field" style="float: left;">' . $this->ShowCheckboxField('set_error_reporting', 'Y', array('id' => 'tszh-field__set_error_reporting', 'checked' => true)) . '</div>';
            $this->content .= '<p style="color: #999; margin: .3em 0;">' . GetMessage("WIZ_MAKE_ERROR_REPORTING") . '</p>';
            $this->content .= '</div>';

            $this->content .='<div class="wizard-input-form-block-content">';
            $this->content .= '<input name="__wiz_can_send_updatesinfo" value="Y" type="hidden">';
            $this->content .= '<div class="wizard-input-form-field" style="float: left;">' . $this->ShowCheckboxField('make_auto_backup', 'Y', array('id' => 'tszh-field__make_auto_backup', 'checked' => true)) . '</div>';
            $this->content .= '<p style="color: #999; margin: .3em 0;">' . GetMessage("WIZ_MAKE_AUTO_BACKUP") . '</p>';
            $this->content .= '</div>';

            $this->content .='<div class="wizard-input-form-block-content">';
            $this->content .= '<div class="wizard-input-form-field" style="float: left;">' . $this->ShowCheckboxField('make_group_settings', 'Y', array('id' => 'tszh-field__make_group_settings', 'checked' => true)) . '</div>';
            $this->content .= '<p style="color: #999; margin: .3em 0;">' . GetMessage("WIZ_MAKE_GROUP_SETTINGS") . '</p>';
            $this->content .= '</div>';

            $this->content .='<div class="wizard-input-form-block-content">';
            $this->content .= '<div class="wizard-input-form-field" style="float: left;">' . $this->ShowCheckboxField('set_cdn_active', 'Y', array('id' => 'tszh-field__set_cdn_active', 'checked' => true)) . '</div>';
            $this->content .= '<p style="color: #999; margin: .3em 0;">' . GetMessage("WIZ_MAKE_CDN_ACTIVE") . '</p>';
            $this->content .= '</div>';

            $this->content .='<div class="wizard-input-form-block-content">';
            $this->content .= '<div class="wizard-input-form-field" style="float: left;">' . $this->ShowCheckboxField('set_composite_auto', 'Y', array('id' => 'tszh-field__set_composite_auto', 'checked' => true)) . '</div>';
            $this->content .= '<p style="color: #999; margin: .3em 0;">' . GetMessage("WIZ_MAKE_COMPOSITE_AUTO") . '</p>';
            $this->content .= '</div>';

            /*$this->content .='<div class="wizard-input-form-block-content">';
            $this->content .= '<div class="wizard-input-form-field" style="float: left;">' . $this->ShowCheckboxField('set_cache_auto', 'Y', array('id' => 'tszh-field__set_cache_auto', 'checked' => true)) . '</div>';
            $this->content .= '<p style="color: #999; margin: .3em 0;">' . GetMessage("WIZ_MAKE_CACHE_AUTO") . '</p>';
            $this->content .= '</div>';*/

            $this->content .= '</div>';
        }

		$this->content .= '</div>';
	}

	function OnPostForm()
	{
		$wizard =& $this->GetWizard();

		if (!CTszhFunctionalityController::checkSubscribe(false, $isSubscribeEdition) || !$isSubscribeEdition)
		{
			return;
		}

		$arSubscribes = CTszhSubscribe::getSubscribes4Admin(false, $wizard);
		foreach ($arSubscribes as $className => $arSubscribe)
		{
			if ($arSubscribe["SETTINGS_COLLECTED"]["ACTIVE"] != "Y")
			{
				continue;
			}

			try
			{
				$className::checkParams($arSubscribe["SETTINGS_COLLECTED"]["PARAMS"], 0, false, true);
			}
			catch (\Bitrix\Main\ArgumentException $e)
			{
				$this->SetError(getMessage("WIZ_ERROR_SUBSCRIBE_SETTING", array(
					"#SUBSCRIBE#" => $arSubscribe["NAME"],
					"#ERROR#" => $e->getMessage(),
				)), $e->getParameter());
			}
		}
	}
}