<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class ContractorsTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> XML_ID string(255) mandatory
 * <li> TSZH_ID int mandatory
 * <li> EXECUTOR string(1) mandatory
 * <li> NAME string(100) mandatory
 * <li> SERVICES string(255) optional
 * <li> ADDRESS string(255) optional
 * <li> PHONE string(50) optional
 * <li> BILLING string(255) optional
 * <li> INN string(12) optional
 * <li> KPP string(9) optional
 * <li> BANK string(100) optional
 * <li> BIK string(16) optional
 * <li> RSCH string(24) optional
 * <li> KSCH string(24) optional
 * </ul>
 *
 * @package Bitrix\Tszh
 **/
class ContractorsTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_contractors';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_ID_FIELD'),
			),
			'XML_ID' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateXmlId'),
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_XML_ID_FIELD'),
			),
			'TSZH_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_TSZH_ID_FIELD'),
			),
			'EXECUTOR' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateExecutor'),
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_EXECUTOR_FIELD'),
			),
			'NAME' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateName'),
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_NAME_FIELD'),
			),
			'SERVICES' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateServices'),
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_SERVICES_FIELD'),
			),
			'ADDRESS' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateAddress'),
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_ADDRESS_FIELD'),
			),
			'PHONE' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validatePhone'),
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_PHONE_FIELD'),
			),
			'BILLING' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateBilling'),
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_BILLING_FIELD'),
			),
			'INN' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateInn'),
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_INN_FIELD'),
			),
			'KPP' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateKpp'),
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_KPP_FIELD'),
			),
			'BANK' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateBank'),
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_BANK_FIELD'),
			),
			'BIK' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateBik'),
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_BIK_FIELD'),
			),
			'RSCH' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateRsch'),
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_RSCH_FIELD'),
			),
			'KSCH' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateKsch'),
				'title' => Loc::getMessage('CONTRACTORS_ENTITY_KSCH_FIELD'),
			),
		);
	}

	/**
	 * Returns validators for XML_ID field.
	 *
	 * @return array
	 */
	public static function validateXmlId()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for EXECUTOR field.
	 *
	 * @return array
	 */
	public static function validateExecutor()
	{
		return array(
			new Main\Entity\Validator\Length(null, 1),
		);
	}

	/**
	 * Returns validators for NAME field.
	 *
	 * @return array
	 */
	public static function validateName()
	{
		return array(
			new Main\Entity\Validator\Length(null, 100),
		);
	}

	/**
	 * Returns validators for SERVICES field.
	 *
	 * @return array
	 */
	public static function validateServices()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for ADDRESS field.
	 *
	 * @return array
	 */
	public static function validateAddress()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for PHONE field.
	 *
	 * @return array
	 */
	public static function validatePhone()
	{
		return array(
			new Main\Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for BILLING field.
	 *
	 * @return array
	 */
	public static function validateBilling()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for INN field.
	 *
	 * @return array
	 */
	public static function validateInn()
	{
		return array(
			new Main\Entity\Validator\Length(null, 12),
		);
	}

	/**
	 * Returns validators for KPP field.
	 *
	 * @return array
	 */
	public static function validateKpp()
	{
		return array(
			new Main\Entity\Validator\Length(null, 9),
		);
	}

	/**
	 * Returns validators for BANK field.
	 *
	 * @return array
	 */
	public static function validateBank()
	{
		return array(
			new Main\Entity\Validator\Length(null, 100),
		);
	}

	/**
	 * Returns validators for BIK field.
	 *
	 * @return array
	 */
	public static function validateBik()
	{
		return array(
			new Main\Entity\Validator\Length(null, 16),
		);
	}

	/**
	 * Returns validators for RSCH field.
	 *
	 * @return array
	 */
	public static function validateRsch()
	{
		return array(
			new Main\Entity\Validator\Length(null, 24),
		);
	}

	/**
	 * Returns validators for KSCH field.
	 *
	 * @return array
	 */
	public static function validateKsch()
	{
		return array(
			new Main\Entity\Validator\Length(null, 24),
		);
	}
}