<?php

namespace Citrus\Tszh;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;
use \Citrus\Tszh\Entity\DataManager;

Loc::loadMessages(__FILE__);

/**
 * Class HouseTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> TSZH_ID int mandatory
 * <li> AREA double optional
 * <li> FLATS_AREA double optional
 * <li> COMMON_AREA double optional
 * <li> REGION string(50) optional
 * <li> DISTRICT string(50) optional
 * <li> CITY string(50) optional
 * <li> SETTLEMENT string(50) optional
 * <li> STREET string(50) optional
 * <li> HOUSE string(50) optional
 * </ul>
 *
 * @package Bitrix\Tszh
 **/
class HouseTable extends DataManager
{
	/**
	 * Return user field entity
	 *
	 * @return string|null
	 */
	public static function getUfId()
	{
		return 'TSZH_HOUSE';
	}

	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_house';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{

		$sqlHelper = Application::getConnection()->getSqlHelper();

		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('HOUSE_ENTITY_ID_FIELD'),
			),
			'SITE_ID' => array(
				'data_type' => 'string',
				'required' => false,
				'title' => Loc::getMessage('HOUSE_ENTITY_SITE_ID_FIELD'),
			),
			'TSZH_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('HOUSE_ENTITY_TSZH_ID_FIELD'),
			),
			'EXTERNAL_ID' => array(
				'data_type' => 'string',
				'required' => false,
				'title' => Loc::getMessage('HOUSE_ENTITY_EXTERNAL_ID_FIELD'),
			),
			'AREA' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('HOUSE_ENTITY_AREA_FIELD'),
			),
			'ROOMS_AREA' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('HOUSE_ENTITY_ROOMS_AREA_FIELD'),
			),
			'COMMON_PLACES_AREA' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('HOUSE_ENTITY_COMMON_PLACES_AREA_FIELD'),
			),
			'REGION' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateRegion'),
				'title' => Loc::getMessage('HOUSE_ENTITY_REGION_FIELD'),
			),
			'DISTRICT' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateDistrict'),
				'title' => Loc::getMessage('HOUSE_ENTITY_DISTRICT_FIELD'),
			),
			'CITY' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateCity'),
				'title' => Loc::getMessage('HOUSE_ENTITY_CITY_FIELD'),
			),
			'SETTLEMENT' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateSettlement'),
				'title' => Loc::getMessage('HOUSE_ENTITY_SETTLEMENT_FIELD'),
			),
			'STREET' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateStreet'),
				'title' => Loc::getMessage('HOUSE_ENTITY_STREET_FIELD'),
			),
			'HOUSE' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateHouse'),
				'title' => Loc::getMessage('HOUSE_ENTITY_HOUSE_FIELD'),
			),
			'FULL_ADDRESS' => array(
				'data_type' => 'string',
				'expression' => array(
					$sqlHelper->getConcatFunction("CITY", "', '", "STREET", "', '", "HOUSE")
				),
				'title' => Loc::getMessage('HOUSE_ENTITY_FULL_ADDRESS_FIELD'),
			),
            'ZIP' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateZIP'),
                'title' => Loc::getMessage('HOUSE_ENTITY_ZIP_FIELD'),
            ),

			'TYPE' => array(
				'data_type' => 'string',
				/*'expression' => array(
                    'IF(TYPE="A","'.Loc::getMessage('HOUSE_ENTITY_TYPE_MKD').'", "'.Loc::getMessage('HOUSE_ENTITY_TYPE_GD').'")'
                ),*/
				'title' => Loc::getMessage('HOUSE_ENTITY_TYPE_FIELD2'),
			),
			'ADDRESS_USER_ENTERED' => array(
				'data_type' => 'string',
				/*'validation' => array(__CLASS__, 'validateAddress'),*/
				'title' => Loc::getMessage('HOUSE_ENTITY_ADDRESS_FIELD'),
			),
			'ADDRESS_VIEW_FULL' => array(
				'data_type' => 'string',
				/*'validation' => array(__CLASS__, 'validateAddress'),*/
				'title' => Loc::getMessage('HOUSE_ENTITY_ADDRESS_VIEW_FULL_FIELD'),
			),
			'YEAR_OF_BUILT' => array(
				'data_type' => 'integer',
				//				'validation' => array(__CLASS__, 'validateDate'),
				'title' => Loc::getMessage('HOUSE_ENTITY_YEAR_OF_BUILT_FIELD'),
			),
			'DATE_OF_COMMISSIONING' =>  array(
				'data_type' => 'string',				
				'title' => Loc::getMessage('HOUSE_ENTITY_DATE_OF_COMMISSIONING_FIELD'),
			),
			'DATE_OF_MAINTENANCE' =>  array(
				'data_type' => 'string',
				'title' => Loc::getMessage('HOUSE_ENTITY_DATE_OF_MAINTENANCE_FIELD'),
			),
			'FLOORS' => array(
				'data_type' => 'integer',
				//				'validation' => array(__CLASS__, 'validateInt'),
				'title' => Loc::getMessage('HOUSE_ENTITY_FLOORS_FIELD'),
			),
			'PORCHES' => array(
				'data_type' => 'integer',
				//				'validation' => array(__CLASS__, 'validateInt'),
				'title' => Loc::getMessage('HOUSE_ENTITY_PORCHES_FIELD'),
			),

            'BANK' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateBank'),
                'title' => Loc::getMessage('HOUSE_ENTITY_BANK_FIELD'),
            ),
            'BIK' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateBIK'),
                'title' => Loc::getMessage('HOUSE_ENTITY_BIK_FIELD'),
            ),
            'RS' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateRS'),
                'title' => Loc::getMessage('HOUSE_ENTITY_RS_FIELD'),
            ),
            'KS' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateKS'),
                'title' => Loc::getMessage('HOUSE_ENTITY_KS_FIELD'),
            ),

            'OVERHAUL_BANK' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateOVERHAUL_Bank'),
                'title' => Loc::getMessage('HOUSE_ENTITY_OVERHAUL_BANK_FIELD'),
            ),
            'OVERHAUL_BIK' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateOVERHAUL_BIK'),
                'title' => Loc::getMessage('HOUSE_ENTITY_OVERHAUL_BIK_FIELD'),
            ),
            'OVERHAUL_RS' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateOVERHAUL_RS'),
                'title' => Loc::getMessage('HOUSE_ENTITY_OVERHAUL_RS_FIELD'),
            ),
            'OVERHAUL_KS' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateOVERHAUL_KS'),
                'title' => Loc::getMessage('HOUSE_ENTITY_OVERHAUL_KS_FIELD'),
            ),
		);
	}

	/**
	 * Returns validators for REGION field.
	 *
	 * @return array
	 */
	public static function validateRegion()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for DISTRICT field.
	 *
	 * @return array
	 */
	public static function validateDistrict()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for CITY field.
	 *
	 * @return array
	 */
	public static function validateCity()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for SETTLEMENT field.
	 *
	 * @return array
	 */
	public static function validateSettlement()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for STREET field.
	 *
	 * @return array
	 */
	public static function validateStreet()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for HOUSE field.
	 *
	 * @return array
	 */
	public static function validateHouse()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}

    /**
     * Returns validators for BANK field.
     *
     * @return array
     */
    public static function validateBank()
    {
        return array(
            new Entity\Validator\Length(null, 100),
        );
    }

    /**
     * Returns validators for BIK field.
     *
     * @return array
     */
    public static function validateBIK()
    {
        return array(
            new Entity\Validator\Length(null, 16),
        );
    }

    /**
     * Returns validators for RS field.
     *
     * @return array
     */
    public static function validateRS()
    {
        return array(
            new Entity\Validator\Length(null, 24),
        );
    }

    /**
     * Returns validators for KS field.
     *
     * @return array
     */
    public static function validateKS()
    {
        return array(
            new Entity\Validator\Length(null, 24),
        );
    }

    /**
     * Returns validators for OVERHAUL_BANK field.
     *
     * @return array
     */
    public static function validateOVERHAUL_Bank()
    {
        return array(
            new Entity\Validator\Length(null, 100),
        );
    }

    /**
     * Returns validators for OVERHAUL_BIK field.
     *
     * @return array
     */
    public static function validateOVERHAUL_BIK()
    {
        return array(
            new Entity\Validator\Length(null, 16),
        );
    }

    /**
     * Returns validators for OVERHAUL_RS field.
     *
     * @return array
     */
    public static function validateOVERHAUL_RS()
    {
        return array(
            new Entity\Validator\Length(null, 24),
        );
    }

    /**
     * Returns validators for OVERHAUL_KS field.
     *
     * @return array
     */
    public static function validateOVERHAUL_KS()
    {
        return array(
            new Entity\Validator\Length(null, 24),
        );
    }

    /**
     * Returns validators for ZIP field.
     *
     * @return array
     */
    public static function validateZIP()
    {
        return array(
            new Entity\Validator\Length(null, 20),
        );
    }
}