<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class MetersValuesTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> METER_ID int mandatory
 * <li> VALUE1 double mandatory
 * <li> VALUE2 double mandatory
 * <li> VALUE3 double mandatory
 * <li> AMOUNT1 double optional
 * <li> AMOUNT2 double optional
 * <li> AMOUNT3 double optional
 * <li> MODIFIED_BY int mandatory
 * <li> TIMESTAMP_X datetime mandatory
 * <li> MODIFIED_BY_OWNER bool optional default 'N'
 * <li> ACCOUNT_ID int mandatory
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class MetersValuesTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_meters_values';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('METERS_VALUES_ENTITY_ID_FIELD'),
			),
			'METER_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('METERS_VALUES_ENTITY_METER_ID_FIELD'),
			),
			'VALUE1' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('METERS_VALUES_ENTITY_VALUE1_FIELD'),
			),
			'VALUE2' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('METERS_VALUES_ENTITY_VALUE2_FIELD'),
			),
			'VALUE3' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('METERS_VALUES_ENTITY_VALUE3_FIELD'),
			),
			'AMOUNT1' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('METERS_VALUES_ENTITY_AMOUNT1_FIELD'),
			),
			'AMOUNT2' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('METERS_VALUES_ENTITY_AMOUNT2_FIELD'),
			),
			'AMOUNT3' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('METERS_VALUES_ENTITY_AMOUNT3_FIELD'),
			),
			'MODIFIED_BY' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('METERS_VALUES_ENTITY_MODIFIED_BY_FIELD'),
			),
			'TIMESTAMP_X' => array(
				'data_type' => 'datetime',
				'required' => true,
				'title' => Loc::getMessage('METERS_VALUES_ENTITY_TIMESTAMP_X_FIELD'),
			),
			'MODIFIED_BY_OWNER' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('METERS_VALUES_ENTITY_MODIFIED_BY_OWNER_FIELD'),
			),
			new \Bitrix\Main\Entity\ReferenceField(
				'ACCOUNT',
				'Citrus\Tszh\MetersAccountsTable',
				array('=this.METER_ID' => 'ref.METER_ID'),
				array('join_type' => 'LEFT')
			),
			new \Bitrix\Main\Entity\ExpressionField(
				'ACCOUNT_ID',
				'%s',
				'ACCOUNT.ACCOUNT_ID'
			),
		);
	}
}