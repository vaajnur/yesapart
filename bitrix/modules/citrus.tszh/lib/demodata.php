<?php
/**
 * @author Maxim Cherepinin <cherma@rarus.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 06.07.2018 10:17
 */

namespace Citrus\Tszh;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\ArgumentOutOfRangeException;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Bitrix\Main\Web\Json;
use Vdgb\Tszhepasport\EpasportTable;

/**
 * Class Demodata
 * @package Citrus\Tszh
 */
class Demodata
{
	private $demodata_site_url = 'http://demodata.demo-jkh.ru';

	private $demodata_service_path = '/bitrix/tools/otr_tszh_demodata_service.php';

	private $demo_perfix = 'demodata-';

	private $accounts;
	private $houses;
	private $contractors;
	private $services;
	private $periods;
	private $meters;
	private $tszh_id;
	private $syncData;

	/**
	 * @param $tszh_id
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function installForTszh($tszh_id)
	{
		if (!isset($tszh_id))
		{
			throw new \Exception(Loc::getMessage("CITRUS_TSZH_DEMODATA_ERROR_TSZH_ID_IS_NOT_SET"));
		}
		else
		{
			$this->tszh_id = (int)$tszh_id;
		}

		try
		{
			$arData = $this->parseData();
		}
		catch (\Exception $e)
		{
			throw new \Exception(Loc::getMessage('CITRUS_TSZH_DEMODATA_ERROR_PARSE_DATA') . $e->getMessage());
		}

		if (!empty($arData))
		{
			$this->houses = $arData['houses'];
			$this->contractors = $arData['contractors'];
			$this->services = $arData['services'];
			$this->periods = $arData['periods'];
			$this->meters = $arData['meters'];
			$this->accounts = $arData['accounts'];
		}

		if (is_array($this->houses) && count($this->houses) > 0)
		{
			$this->addHouses();
		}

		if (is_array($this->contractors) && count($this->contractors) > 0)
		{
			$this->addContractors();
		}

		if (is_array($this->services) && count($this->services) > 0)
		{
			$this->addServices();
		}

		if (is_array($this->periods) && count($this->periods) > 0)
		{
			$this->addPeriods();
		}

		if (is_array($this->meters) && count($this->meters) > 0)
		{
			$this->addMeters();
		}

		if (is_array($this->accounts) && count($this->accounts) > 0)
		{
			$this->addAccounts();
		}

		return true;
	}

	/**
	 * @return array|mixed
	 * @throws \Exception
	 */
	private function parseData()
	{
		$url = $this->demodata_site_url . $this->demodata_service_path;

		$client = new \BaseJsonRpcClient($url);
		$result = $client->getTszhDemoData();

		if ($result->HasError())
		{
			throw new \Exception($result->Error['message']);
		}
		else
		{
			return Json::decode($result->Result);
		}

		// return array();
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	private function addHouses()
	{
		foreach ($this->houses as $house)
		{
			$arAddFields = $house;
			$arAddFields['TSZH_ID'] = $this->tszh_id;
			$arAddFields['EXTERNAL_ID'] = $this->demo_perfix . $house['EXTERNAL_ID'];
			unset($arAddFields['ID']);
			unset($arAddFields['SITE_ID']);
			unset($arAddFields['epasport']);

			try
			{
				$result = HouseTable::add($arAddFields);
			}
			catch (\Exception $e)
			{
				throw new \Exception($e->getMessage());
			}

			if ($result->isSuccess())
			{
				$this->syncData['houses'][$house['ID']] = $result->getId();

				if (!empty($house['epasport']) && Loader::includeModule('vdgb.tszhepasport'))
				{
					foreach ($house['epasport'] as $epasport)
					{
						$arEpasport = $epasport;
						$arEpasport['EP_HOUSE_ID'] = $result->getId();
						unset($arEpasport['EP_ID']);
						unset($arEpasport['DATE_CREATE']);
						unset($arEpasport['DATE_UPDATE']);

						EpasportTable::add($arEpasport);
					}
				}
			}
			else
			{
				throw new \Exception($result->getErrorMessages());
			}
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	private function addContractors()
	{
		foreach ($this->contractors as $contractor)
		{
			$arAddFields = $contractor;
			$arAddFields['TSZH_ID'] = $this->tszh_id;
			$arAddFields['XML_ID'] = $this->demo_perfix . $contractor['XML_ID'];
			unset($arAddFields['ID']);

			$ID = \CTszhContractor::Add($arAddFields);

			if ($ID > 0)
			{
				$this->syncData['contractors'][$contractor['ID']] = $ID;
			}
			else
			{
				throw new \Exception(Loc::getMessage("CITRUS_TSZH_DEMODATA_ERROR_ADDING_CONTRACTORS"));
			}
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	private function addServices()
	{
		foreach ($this->services as $service)
		{
			$arAddFields = $service;
			$arAddFields['TSZH_ID'] = $this->tszh_id;
			$arAddFields['XML_ID'] = $this->demo_perfix . $service['XML_ID'];
			unset($arAddFields['ID']);

			$ID = \CTszhService::Add($arAddFields);

			if ($ID > 0)
			{
				$this->syncData['services'][$service['ID']] = $ID;
			}
			else
			{
				throw new \Exception(Loc::getMessage("CITRUS_TSZH_DEMODATA_ERROR_ADDING_SERVICES"));
			}
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	private function addPeriods()
	{
		foreach ($this->periods as $period)
		{
			$arAddFields = $period;
			$arAddFields['TSZH_ID'] = $this->tszh_id;
			unset($arAddFields['ID']);

			$ID = \CTszhPeriod::Add($arAddFields);

			if ($ID > 0)
			{
				$this->syncData['periods'][$period['ID']] = $ID;
			}
			else
			{
				throw new \Exception(Loc::getMessage("CITRUS_TSZH_DEMODATA_ERROR_ADDING_PERIODS"));
			}
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	private function addMeters()
	{
		foreach ($this->meters as $meter)
		{
			$meterValues = $meter['values'];
			$meterAccounts = $meter['ACCOUNT_ID'];
			$arMeterAccounts = explode('|', $meterAccounts);
			$arAddFields = $meter;
			$arAddFields['MODIFIED_BY'] = '';
			$arAddFields['XML_ID'] = $this->demo_perfix . $meter['XML_ID'];
			unset($arAddFields['ID']);
			unset($arAddFields['ACCOUNT_ID']);
			unset($arAddFields['values']);

			$ID = \CTszhMeter::Add($arAddFields);

			if ($ID > 0)
			{
				$this->syncData['meters'][$meter['ID']] = $ID;

				if (is_array($arMeterAccounts) && count($arMeterAccounts) > 0)
				{
					foreach ($arMeterAccounts as $account)
					{
						$this->syncData['accounts_meters'][$account][] = $ID;
					}
				}

				foreach ($meterValues as $value)
				{
					$arAddValue = $value;
					$arAddValue['METER_ID'] = $ID;
					unset($value['ID']);
					unset($value['DEC_PLACES']);
					unset($value['VALUES_COUNT']);
					unset($value['CAPACITY']);

					\CTszhMeterValue::Add($arAddValue);
				}
			}
			else
			{
				throw new \Exception(Loc::getMessage("CITRUS_TSZH_DEMODATA_ERROR_ADDING_METERS"));
			}
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 * @throws \Exception
	 */
	private function addAccounts()
	{
		foreach ($this->accounts as $account)
		{
			$arAddFields = $account;
			$arAddFields['TSZH_ID'] = $this->tszh_id;
			$arAddFields['XML_ID'] = $this->demo_perfix . $account['XML_ID'];
			$arAddFields['HOUSE_ID'] = $this->syncData['houses'][$account['HOUSE_ID']];
			unset($arAddFields['ID']);
			unset($arAddFields['receipts']);

			$ID = \CTszhAccount::Add($arAddFields);

			if ($ID > 0)
			{
				// $this->syncData['periods'][$period['ID']] = $ID;

				$this->bindMeters($account, $ID);

				foreach ($account['receipts'] as $receipt)
				{
					$arReceipt = $receipt;
					$arReceipt['PERIOD_ID'] = $this->syncData['periods'][$receipt['PERIOD_ID']];
					$arReceipt['ACCOUNT_ID'] = $ID;
					unset($arReceipt['ID']);
					unset($arReceipt['charges']);
					unset($arReceipt['barcodes']);
					unset($arReceipt['contractors']);

					$receiptID = \CTszhAccountPeriod::Add($arReceipt);

					if (!empty($receipt['contractors']))
					{
						foreach ($receipt['contractors'] as $contractor)
						{
							$arContractor = $contractor;
							$arContractor['ACCOUNT_PERIOD_ID'] = $receiptID;
							$arContractor['CONTRACTOR_ID'] = $this->syncData['contractors'][$contractor['CONTRACTOR_ID']];
							unset($arContractor['ID']);
							\CTszhAccountContractor::Add($arContractor);
						}
					}

					if (!empty($receipt['barcodes']))
					{
						foreach ($receipt['barcodes'] as $barcode)
						{
							$arBarcode = $barcode;
							$arBarcode['ACCOUNT_PERIOD_ID'] = $receiptID;
							\CTszhBarCode::Add($arBarcode);
						}
					}

					foreach ($receipt['charges'] as $charge)
					{
						$arCharge = $charge;
						$arCharge['ACCOUNT_ID'] = $ID;
						$arCharge['PERIOD_ID'] = $this->syncData['periods'][$receipt['PERIOD_ID']];
						$arCharge['SERVICE_ID'] = $this->syncData['services'][$charge['SERVICE_ID']];
						$arCharge['ACCOUNT_PERIOD_ID'] = $receiptID;
						unset($arCharge['ID']);
						unset($arCharge['HMETER_ID']);
						unset($arCharge['HMETER_IDS']);
						unset($arCharge['METER_IDS']);

						$chargeID = \CTszhCharge::Add($arCharge);

						if (!empty($arCharge['HMETER_ID']) && (int)$arCharge['HMETER_ID'] > 0)
						{
							\CTszhCharge::bindMeter($chargeID, $this->syncData['meters'][$arCharge['HMETER_ID']]);
						}

						if (!empty($arCharge['HMETER_IDS']) && strlen($arCharge['HMETER_IDS']) > 0)
						{
							$arTmpMeters = explode('|', $arCharge['HMETER_IDS']);
							foreach ($arTmpMeters as $meter)
							{
								\CTszhCharge::bindMeter($chargeID, $this->syncData['meters'][$meter]);
							}
						}

						if (!empty($arCharge['METER_IDS']) && strlen($arCharge['METER_IDS']) > 0)
						{
							$arTmpMeters = explode('|', $arCharge['METER_IDS']);
							foreach ($arTmpMeters as $meter)
							{
								\CTszhCharge::bindMeter($chargeID, $this->syncData['meters'][$meter]);
							}
						}
					}
				}
			}
			else
			{
				throw new \Exception(Loc::getMessage("CITRUS_TSZH_DEMODATA_ERROR_ADDING_ACCOUNTS"));
			}
		}

		return true;
	}

	/**
	 * @param $account
	 * @param $ID
	 *
	 * @return mixed
	 */
	private function bindMeters($account, $ID)
	{
		if (isset($this->syncData['accounts_meters'][$account['ID']]) && !empty($this->syncData['accounts_meters'][$account['ID']]))
		{
			foreach ($this->syncData['accounts_meters'][$account['ID']] as $meter)
			{
				\CTszhMeter::bindAccount($meter, $ID);
			}
		}

		return true;
	}

	/**
	 * @param $tszh_id
	 *
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function getDemoData($tszh_id)
	{
		$arDemoData = array();

		$rsAccounts = \CTszhAccount::GetList(array(), array('%XML_ID' => $this->demo_perfix, 'TSZH_ID' => $tszh_id), false, false, array('ID'));
		if ($rsAccounts->SelectedRowsCount() > 0)
		{
			$arDemoData['accounts'] = $rsAccounts->SelectedRowsCount();
			$arAccountsIDs = array();
			$arAccount = $rsAccounts->Fetch();
			while ($arAccount)
			{
				$arAccountsIDs[] = $arAccount['ID'];
				$arAccount = $rsAccounts->Fetch();
			}
			if (!empty($arAccountsIDs))
			{
				$rsReceipts = \CTszhAccountPeriod::GetList(array(), array('@ACCOUNT_ID' => $arAccountsIDs), false, array('ID', 'ACCOUNT_ID'));
				if ($rsReceipts->SelectedRowsCount() > 0)
				{
					$arDemoData['receipts'] = $rsReceipts->SelectedRowsCount();
				}
			}
		}

		$rsContractors = \CTszhContractor::GetList(array(), array('%XML_ID' => $this->demo_perfix, 'TSZH_ID' => $tszh_id), false, false, array('ID'));
		if ($rsContractors->SelectedRowsCount() > 0)
		{
			$arDemoData['contractors'] = $rsContractors->SelectedRowsCount();
		}

		$rsServices = \CTszhService::GetList(array(), array('%XML_ID' => $this->demo_perfix, 'TSZH_ID' => $tszh_id), false, false, array('ID'));
		if ($rsServices->SelectedRowsCount() > 0)
		{
			$arDemoData['services'] = $rsServices->SelectedRowsCount();
		}

		$rsMeters = \CTszhMeter::GetList(array(), array('%XML_ID' => $this->demo_perfix, 'TSZH_ID' => $tszh_id), false, false, array('ID'));
		if ($rsMeters->SelectedRowsCount() > 0)
		{
			$arDemoData['meters'] = $rsMeters->SelectedRowsCount();
		}

		$rsHouses = HouseTable::getList(array(
				'filter' => array('%EXTERNAL_ID' => $this->demo_perfix, 'TSZH_ID' => $tszh_id),
			)
		);
		if ($rsHouses->getSelectedRowsCount() > 0)
		{
			$arDemoData['houses'] = $rsHouses->getSelectedRowsCount();
		}

		return $arDemoData;
	}

	/**
	 * @return string
	 */
	public function getDownloadNoteBlock()
	{
		return Loc::getMessage("CITRUS_TSZH_DEMODATA_NOTE_BLOCK");
	}

	/**
	 * @return string
	 */
	public function getDownloadButtonHtml()
	{
		global $APPLICATION;

		return '<a href="' . $APPLICATION->GetCurPageParam('action=download_demodata&sessid=' . bitrix_sessid()) . '" class="adm-btn adm-btn-green" title="' . Loc::getMessage('CITRUS_TSZH_DEMODATA_DOWNLOAD_BUTTON_TITLE') . '" id="btn_green">' . Loc::getMessage('CITRUS_TSZH_DEMODATA_DOWNLOAD_BUTTON_TITLE') . '</a>';
	}

	/**
	 * @param $arDemoData
	 */
	public function showDeleteNoteBlock($arDemoData)
	{
		$strDemoDataList = $this->getDemoDataList($arDemoData);

		\CAdminMessage::ShowMessage(array(
			"DETAILS" => Loc::getMessage('CITRUS_TSZH_DEMODATA_DELETE_NOTE_P')
			             . $strDemoDataList
			             . Loc::getMessage('CITRUS_TSZH_DEMODATA_DELETE_NOTE_END')
			             . $this->getDeleteButtonHtml(),
			"TYPE" => "ERROR",
			// "MESSAGE" => Loc::getMessage("CITRUS_TSZH_DEMODATA_DELETE_NOTE_MESSAGE"),
			"HTML" => true,
		));
	}

	/**
	 * @return string
	 */
	public function getDeleteButtonHtml()
	{
		global $APPLICATION;

		return '<a href="' . $APPLICATION->GetCurPageParam('action=delete_demodata&sessid=' . bitrix_sessid()) . '" class="adm-btn adm-btn-delete" title="' . Loc::getMessage('CITRUS_TSZH_DEMODATA_DELETE_BUTTON_TITLE') . '" id="btn_green">' . Loc::getMessage('CITRUS_TSZH_DEMODATA_DELETE_BUTTON_TITLE') . '</a>';

	}

	/**
	 * @param $arDemoData
	 *
	 * @return string
	 */
	protected function getDemoDataList($arDemoData)
	{
		$arDemoDataEntity = array(
			'contractors',
			'houses',
			'accounts',
			'meters',
			'services',
			'receipts',
		);

		$strDemoDataList = '<ul>';

		foreach ($arDemoDataEntity as $entity)
		{
			if (isset($arDemoData[$entity]))
			{
				$strDemoDataList .= $this->getListItem($entity, $arDemoData[$entity]);
			}
		}
		$strDemoDataList .= '</ul>';

		return $strDemoDataList;
	}

	/**
	 * @param $entity
	 * @param $count
	 *
	 * @return string
	 */
	protected function getListItem($entity, $count)
	{
		return Loc::getMessage('CITRUS_TSZH_DEMODATA_DEMO_DATA_LIST_ITEM_ENTITY_' . $entity, array('#COUNT#' => (int)$count));
	}

	/**
	 * @param $tszh_id
	 *
	 * @return bool
	 * @throws ArgumentOutOfRangeException
	 */
	public function deleteDemoData($tszh_id)
	{
		$this->tszh_id = $tszh_id;

		$arEntity = array(
			'\CTszhContractor',
			'\CTszhService',
			'\CTszhMeter',
		);
		$this->deleteDemoDataAccounts();
		foreach ($arEntity as $entity)
		{
			$this->deleteDemoDataByEntity($entity);
		}
		$this->deleteDemoDataHouses();

		return true;
	}

	/**
	 * @param $arPeriods
	 *
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	protected function clearEmptyPeriods($arPeriods)
	{
		foreach ($arPeriods as $period_id)
		{
			$rsReceipts = \CTszhAccountPeriod::GetList(array(), array('PERIOD_ID' => $period_id), false, array('ID'));
			if ($rsReceipts->SelectedRowsCount() == 0)
			{
				\CTszhPeriod::Delete($period_id);
			}
		}
	}

	/**
	 * @param $entityClassName
	 */
	protected function deleteDemoDataByEntity($entityClassName)
	{
		$result = call_user_func_array(array($entityClassName, 'GetList'), array(
			array(),
			array('%XML_ID' => $this->demo_perfix, 'TSZH_ID' => $this->tszh_id),
			false,
			false,
			array('ID'),
		));

		if ($result !== false)
		{
			$arResult = $result->Fetch();
			while ($arResult)
			{
				call_user_func_array(array($entityClassName, 'Delete'), array($arResult["ID"]));
				$arResult = $result->Fetch();
			}
		}
	}

	/**
	 * @return void
	 * @throws ArgumentOutOfRangeException
	 */
	protected function deleteDemoDataAccounts()
	{
		$rsAccounts = \CTszhAccount::GetList(array(), array('%XML_ID' => $this->demo_perfix, 'TSZH_ID' => $this->tszh_id), false, false, array('ID'));
		$arPeriods = array();
		$arAccount = $rsAccounts->Fetch();
		while ($arAccount)
		{
			$rsReceipts = \CTszhAccountPeriod::GetList(array(), array('ACCOUNT_ID' => $arAccount['ID']), false, false, array(
				'ID',
				'ACCOUNT_ID',
				'PERIOD_ID',
			));

			$arReceipt = $rsReceipts->Fetch();
			while ($arReceipt)
			{
				$arPeriods[] = $arReceipt['PERIOD_ID'];
				$arReceipt = $rsReceipts->Fetch();
			}

			\CTszhAccount::Delete($arAccount['ID']);

			$arAccount = $rsAccounts->Fetch();
		}

		$this->clearEmptyPeriods(array_unique($arPeriods));
	}

	/**
	 * @throws ArgumentException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 * @throws \Exception
	 */
	protected function deleteDemoDataHouses()
	{
		$rsHouses = HouseTable::getList(array(
				'filter' => array('%EXTERNAL_ID' => $this->demo_perfix, 'TSZH_ID' => $this->tszh_id),
				'select' => array('ID'),
			)
		);

		$arHouse = $rsHouses->fetch();
		while ($arHouse)
		{
			HouseTable::delete($arHouse['ID']);

			if (Loader::includeModule('vdgb.tszhepasport'))
			{

				$rsEpasport = EpasportTable::getList(array(
					'filter' => array('EP_HOUSE_ID' => $arHouse['ID']),
					'select' => array('EP_ID'),
				));
				$arEpasport = $rsEpasport->fetch();
				while ($arEpasport)
				{
					EpasportTable::delete($arEpasport['EP_ID']);
					$arEpasport = $rsEpasport->fetch();
				}
			}

			$arHouse = $rsHouses->fetch();
		}
	}

	/**
	 * @param $arStatus
	 */
	public function setStatus($arStatus)
	{
		if (is_array($arStatus) && !empty($arStatus))
		{
			$_SESSION['OTR.TSZH.DEMODATA.SERVICE']['STATUS']['IS_SET'] = true;
			foreach ($arStatus as $status)
			{
				$_SESSION['OTR.TSZH.DEMODATA.SERVICE']['STATUSES'][] = $status;
			}
		}
	}

	public function showStatusMessage()
	{
		$arStatus = $this->getStatus();
		if (is_array($arStatus))
		{
			foreach ($arStatus as $status)
			{
				\CAdminMessage::ShowMessage(array(
					"TYPE" => "OK",
					"MESSAGE" => Loc::getMessage("CITRUS_TSZH_DEMODATA_DEMO_DATA_STATUS_MESSAGE_" . $status),
					"HTML" => false,
				));
			}

			$this->clearStatus();
		}
	}

	public function showNoteForWizard()
	{
		return Loc::getMessage('CITRUS_TSZH_DEMODATA_NOTE_BLOCK_WIZARD');
	}

	/**
	 * @return array|bool
	 */
	protected function getStatus()
	{
		if (isset($_SESSION['OTR.TSZH.DEMODATA.SERVICE'])
		    && $_SESSION['OTR.TSZH.DEMODATA.SERVICE']['STATUS']['IS_SET'] == true)
		{
			$arStatus = array();
			foreach ($_SESSION['OTR.TSZH.DEMODATA.SERVICE']['STATUSES'] as $status)
			{
				$arStatus[] = $status;
			}

			return $arStatus;
		}

		return false;
	}

	protected function clearStatus()
	{
		unset($_SESSION['OTR.TSZH.DEMODATA.SERVICE']);
	}
}