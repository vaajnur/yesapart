<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class ServicesTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ACTIVE string(1) mandatory
 * <li> UNITS string(10) optional
 * <li> TSZH_ID string(50) mandatory
 * <li> XML_ID string(50) mandatory
 * <li> NAME string(255) mandatory
 * <li> NORM double optional
 * <li> TARIFF double optional
 * <li> TARIFF2 double optional
 * <li> TARIFF3 double optional
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class ServicesTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_services';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('SERVICES_ENTITY_ID_FIELD'),
			),
			'ACTIVE' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateActive'),
				'title' => Loc::getMessage('SERVICES_ENTITY_ACTIVE_FIELD'),
			),
			'UNITS' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateUnits'),
				'title' => Loc::getMessage('SERVICES_ENTITY_UNITS_FIELD'),
			),
			'TSZH_ID' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateTszhId'),
				'title' => Loc::getMessage('SERVICES_ENTITY_TSZH_ID_FIELD'),
			),
			'XML_ID' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateXmlId'),
				'title' => Loc::getMessage('SERVICES_ENTITY_XML_ID_FIELD'),
			),
			'NAME' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateName'),
				'title' => Loc::getMessage('SERVICES_ENTITY_NAME_FIELD'),
			),
			'NORM' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_ENTITY_NORM_FIELD'),
			),
			'TARIFF' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_ENTITY_TARIFF_FIELD'),
			),
			'TARIFF2' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_ENTITY_TARIFF2_FIELD'),
			),
			'TARIFF3' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_ENTITY_TARIFF3_FIELD'),
			),
            'IS_INSURANCE' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateActive'),
                'title' => Loc::getMessage('SERVICES_ENTITY_ACTIVE_FIELD'),
            ),
		);
	}

	/**
	 * Returns validators for ACTIVE field.
	 *
	 * @return array
	 */
	public static function validateActive()
	{
		return array(
			new Main\Entity\Validator\Length(null, 1),
		);
	}

	/**
	 * Returns validators for UNITS field.
	 *
	 * @return array
	 */
	public static function validateUnits()
	{
		return array(
			new Main\Entity\Validator\Length(null, 10),
		);
	}

	/**
	 * Returns validators for TSZH_ID field.
	 *
	 * @return array
	 */
	public static function validateTszhId()
	{
		return array(
			new Main\Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for XML_ID field.
	 *
	 * @return array
	 */
	public static function validateXmlId()
	{
		return array(
			new Main\Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for NAME field.
	 *
	 * @return array
	 */
	public static function validateName()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}
}