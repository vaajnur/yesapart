<?php
namespace Citrus\Tszh\Subscribe;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;

/**
 * Class CTszhSubscribeTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> CODE string(50) mandatory
 * <li> TSZH_ID int optional
 * <li> ACTIVE bool optional default 'N'
 * <li> RUNNING bool optional default 'N'
 * <li> PARAMS string optional
 * <li> LAST_EXEC datetime optional
 * <li> NEXT_EXEC datetime optional
 * </ul>
 *
 **/
class CTszhSubscribeTable extends Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_subscribe';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('CITRUS_TSZH_SUBSCRIBE_ENTITY_ID_FIELD'),
			),
			'CODE' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateCode'),
				'title' => Loc::getMessage('CITRUS_TSZH_SUBSCRIBE_ENTITY_CODE_FIELD'),
			),
			'TSZH_ID' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('CITRUS_TSZH_SUBSCRIBE_ENTITY_TSZH_ID_FIELD'),
			),
			'ACTIVE' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('CITRUS_TSZH_SUBSCRIBE_ENTITY_ACTIVE_FIELD'),
			),
			'RUNNING' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('CITRUS_TSZH_SUBSCRIBE_ENTITY_RUNNING_FIELD'),
			),
			'PARAMS' => array(
				'data_type' => 'text',
				'serialized' => true,
				'title' => Loc::getMessage('CITRUS_TSZH_SUBSCRIBE_ENTITY_PARAMS_FIELD'),
			),
			'LAST_EXEC' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('CITRUS_TSZH_SUBSCRIBE_ENTITY_LAST_EXEC_FIELD'),
			),
			'NEXT_EXEC' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('CITRUS_TSZH_SUBSCRIBE_ENTITY_NEXT_EXEC_FIELD'),
			),
		);
	}

	/**
	 * Returns validators for CODE field.
	 *
	 * @return array
	 */
	public static function validateCode()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}

	public static function onBeforeUpdate(Entity\Event $event)
	{
		\CTszhSubscribe::propagateSubscribeEvent(__FUNCTION__, $event);
	}

	public static function onBeforeDelete(Entity\Event $event)
	{
		\CTszhSubscribe::propagateSubscribeEvent(__FUNCTION__, $event);
	}
}