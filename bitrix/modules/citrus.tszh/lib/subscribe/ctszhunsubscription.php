<?php

namespace Citrus\Tszh\Subscribe;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;

/**
 * Class CTszhUnsubscriptionTable
 *
 * Fields:
 * <ul>
 * <li> SUBSCRIBE_CODE string(50) mandatory
 * <li> USER_ID int mandatory
 * </ul>
 *
 **/
class CTszhUnsubscriptionTable extends Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_subscribe_unsubscription';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'SUBSCRIBE_CODE' => array(
				'data_type' => 'string',
				'primary' => true,
				'validation' => array(__CLASS__, 'validateSubscribeCode'),
				'title' => Loc::getMessage('CITRUS_TSZH_UNSUBSCRIPTION_ENTITY_SUBSCRIBE_CODE_FIELD'),
			),
			'USER_ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'title' => Loc::getMessage('CITRUS_TSZH_UNSUBSCRIPTION_ENTITY_USER_ID_FIELD'),
			),
		);
	}

	/**
	 * Returns validators for SUBSCRIBE_CODE field.
	 *
	 * @return array
	 */
	public static function validateSubscribeCode()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}
}