<?php
namespace Citrus\Tszh\Entity;

/**
 * @package
 * @subpackage
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2016 vdgb-soft.ru
 * @date 08.02.2017 13:11
 */

abstract class DataManager extends \Bitrix\Main\Entity\DataManager
{
	/**
	 * @var array|null
	 */
	private static $editableMap = null;

	/**
	 * ���������� ������ � ������� ����� ��������
	 * @return array
	 */
	public static function getFieldNames()
	{
		return array_keys(self::getEditableFields());
	}

	/**
	 * @param string $fieldName
	 * @return string
	 * @throws \Exception
	 */
	public static function getFieldTitle($fieldName)
	{
		$map = static::getMap();
		if (array_key_exists($fieldName, $map))
			return $map[$fieldName]['title'];
		else
			throw new ArgumentException('Unknown $fieldName specified', 'fieldName');
	}

	/**
	 * ���������� ������ ������������� ����� ���������� ������ getMap()
	 * @return array
	 */
	public static function getEditableFields()
	{
		if (!isset(self::$editableMap))
		{
			self::$editableMap = array();
			$map = static::getMap();
			foreach ($map as $fieldName => $fieldSettings)
			{
				if (!array_key_exists('expression', $fieldSettings))
					self::$editableMap[$fieldName] = $fieldSettings;
			}
		}

		return self::$editableMap;
	}
}