<?php
/**
 * @author Maxim Cherepinin <cherma@rarus.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 03.07.2018 9:50
 */

namespace Citrus\Tszh\DemoData;

use \Bitrix\Main\Localization\Loc;

/**
 * Class Meters
 * @package Citrus\Tszh\DemoData
 */
class Meters extends DemoData
{
	/**
	 * Meters constructor.
	 */
	public function __construct()
	{
		$this->data = array(
			array(
				'ACTIVE' => 'Y',
				'HOUSE_METER' => 'N',
				'XML_ID' => 'demodata-meter01',
				'NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_METERS_NAME_01"),
				'NUM' => '',
				'SERVICE_ID' => '',
				'SERVICE_NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_METERS_NAME_01"),
				'VALUES_COUNT' => '1',
				'DEC_PLACES' => '3',
				'CAPACITY' => '3',
				'SORT' => '100',
				'VERIFICATION_DATE' => '',
			),
			array(
				'ACTIVE' => 'Y',
				'HOUSE_METER' => 'N',
				'XML_ID' => 'demodata-meter01',
				'NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_METERS_NAME_02"),
				'NUM' => '',
				'SERVICE_ID' => '',
				'SERVICE_NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_METERS_NAME_02"),
				'VALUES_COUNT' => '1',
				'DEC_PLACES' => '2',
				'CAPACITY' => '6',
				'SORT' => '100',
				'VERIFICATION_DATE' => '',
			),
			array(
				'ACTIVE' => 'Y',
				'HOUSE_METER' => 'N',
				'XML_ID' => 'demodata-meter01',
				'NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_METERS_NAME_03"),
				'NUM' => '',
				'SERVICE_ID' => '',
				'SERVICE_NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_METERS_NAME_03"),
				'VALUES_COUNT' => '1',
				'DEC_PLACES' => '2',
				'CAPACITY' => '6',
				'SORT' => '100',
				'VERIFICATION_DATE' => '',
			),
			[
				'ACTIVE' => 'Y',
				'HOUSE_METER' => 'N',
				'XML_ID' => 'demodata-meter01',
				'NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_METERS_NAME_04"),
				'NUM' => '',
				'SERVICE_ID' => '',
				'SERVICE_NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_METERS_NAME_04"),
				'VALUES_COUNT' => '1',
				'DEC_PLACES' => '1',
				'CAPACITY' => '4',
				'SORT' => '100',
				'VERIFICATION_DATE' => '',
			],
		);
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getMeterDataByID($id)
	{
		return isset($this->data[$id]) ? $this->data[$id] : false;
	}
}