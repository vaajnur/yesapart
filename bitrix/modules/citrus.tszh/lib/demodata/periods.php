<?php
/**
 * @author Maxim Cherepinin <cherma@rarus.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 03.07.2018 9:56
 */

namespace Citrus\Tszh\DemoData;

/**
 * Class Periods
 * @package Citrus\Tszh\DemoData
 */
class Periods extends DemoData
{
	/**
	 * @var array
	 */
	protected $data = array(
		'TSZH_ID' => '',
		'DATE' => '',
		'TIMESTAMP_X' => '',
		'ACTIVE' => '',
		'ONLY_DEBT' => '',
	);
}