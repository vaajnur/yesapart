<?php
/**
 * @author Maxim Cherepinin <cherma@rarus.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 03.07.2018 9:48
 */

namespace Citrus\Tszh\DemoData;
use Bitrix\Main\Localization\Loc;

/**
 * Class Services
 * @package Citrus\Tszh\DemoData
 */
class Services extends DemoData
{
	/**
	 * Services constructor.
	 */
	public function __construct()
	{
		$this->data = array(
			array(
				'ACTIVE' => 'Y',
				'TSZH_ID' => '',
				'XML_ID' => 'demodata-main-service01',
				'NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_SERVICES_NAME_01"),
				'NORM' => '45',
				'TARIFF' => '3,15',
				'TARIFF2' => '0',
				'TARIFF3' => '0',
				'UNITS' => Loc::getMessage("CITRUS_TSZH_DEMODATA_SERVICES_UNITS_01"),
				'IS_INSURANCE' => 'N',
			),
			array(
				'ACTIVE' => 'Y',
				'TSZH_ID' => '',
				'XML_ID' => 'demodata-main-service02',
				'NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_SERVICES_NAME_02"),
				'NORM' => '5',
				'TARIFF' => '93.58',
				'TARIFF2' => '0',
				'TARIFF3' => '0',
				'UNITS' => Loc::getMessage("CITRUS_TSZH_DEMODATA_SERVICES_UNITS_01"),
				'IS_INSURANCE' => 'N',
			),
			array(
				'ACTIVE' => 'Y',
				'TSZH_ID' => '',
				'XML_ID' => 'demodata-main-service03',
				'NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_SERVICES_NAME_03"),
				'NORM' => '10',
				'TARIFF' => '19.85',
				'TARIFF2' => '0',
				'TARIFF3' => '0',
				'UNITS' => Loc::getMessage("CITRUS_TSZH_DEMODATA_SERVICES_UNITS_01"),
				'IS_INSURANCE' => 'N',
			),
			array(
				'ACTIVE' => 'Y',
				'TSZH_ID' => '',
				'XML_ID' => 'demodata-main-service04',
				'NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_SERVICES_NAME_04"),
				'NORM' => '100',
				'TARIFF' => '3.45',
				'TARIFF2' => '0',
				'TARIFF3' => '0',
				'UNITS' => Loc::getMessage("CITRUS_TSZH_DEMODATA_SERVICES_UNITS_04"),
				'IS_INSURANCE' => 'N',
			),
			array(
				'ACTIVE' => 'Y',
				'TSZH_ID' => '',
				'XML_ID' => 'demodata-overhaul-service01',
				'NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_SERVICES_NAME_05"),
				'NORM' => '0',
				'TARIFF' => '152.20',
				'TARIFF2' => '0',
				'TARIFF3' => '0',
				'UNITS' => Loc::getMessage("CITRUS_TSZH_DEMODATA_SERVICES_UNITS_05"),
				'IS_INSURANCE' => 'N',
			),
		);
	}
}