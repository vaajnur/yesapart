<?php
/**
 * @author Maxim Cherepinin <cherma@rarus.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 02.07.2018 15:22
 */

namespace Citrus\Tszh\DemoData;

use \Bitrix\Main\Localization\Loc;

/**
 * Class Account
 * @package Citrus\Tszh\DemoData
 */
class Account extends DemoData
{
	/**
	 * Account constructor.
	 */
	public function __construct()
	{
		$this->data = array(
			'CURRENT' => 'Y',
			'USER_ID' => '',
			'TSZH_ID' => '',
			'TYPE' => '1',
			'XML_ID' => '2011067001',
			'EXTERNAL_ID' => 'demodata-account',
			'NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_ACCOUNT_NAME"),
			'HOUSE_ID' => '',
			'FLAT' => '1',
			'FLAT_ABBR' => Loc::getMessage("CITRUS_TSZH_DEMODATA_ACCOUNT_FLAT_ABBR"),
			'FLAT_TYPE' => Loc::getMessage("CITRUS_TSZH_DEMODATA_ACCOUNT_FLAT_TYPE"),
			'AREA' => '54.2',
			'LIVING_AREA' => '51.6',
			'PEOPLE' => '4',
			'REGISTERED_PEOPLE' => '2',
			'EXEMPT_PEOPLE' => '0',
		);
	}
}