<?php
/**
 * @package citrus
 * @subpackage tszh
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2016 vdgb-soft.ru
 * @date 08.12.2016 17:07
 */

namespace Citrus\Tszh;

use Bitrix\Main\Application;
use Citrus\Tszh\MonetaAdditionalInfoTable;


class MonetaAdditionalInfoHelper
{
	private static $validator_class_name_array_id = 0;

	private static $validator_method_name_array_id = 1;

	private static $validator_object_id = 0;

	/**
	 * ����� ������� �������� ��� �������������� � ���������������� ����� �� �������� �������������� �����������
	 */
	public static function showMonetaAdditionalFileds($tszh_id = false)
	{
		// ������� �������� �����, ���� ������ ���������� ����������
		if ($tszh_id !== false)
		{
			$obMonetaAdditionalInfo = MonetaAdditionalInfoTable::getById($tszh_id);
			$arMonetaAdditionalFields = $obMonetaAdditionalInfo->fetch();
		}

		// ������� ������ ����� ������� ����� �������������
		$arFields = MonetaAdditionalInfoTable::getEditableFields();

		// ������ ��������� ������� ��� ������ � ���������������� �����
		$html = '<table>';

		foreach ($arFields as $key => $field)
		{
			if ($field['primary'])
			{
				continue;
			}

			if (isset($field['validation']) && is_array($field['validation']))
			{
				$class = $field['validation'][self::$validator_class_name_array_id];
				$method = $field['validation'][self::$validator_method_name_array_id];
				$size = call_user_func($class . '::' . $method);
				$maxlength = $size[self::$validator_object_id]->getMax();
				$max_size = ($maxlength > 100 ? '100' : $maxlength);
			}

			$require = '';
			if ($field['required'])
			{
				$require = 'font-weight:bold;';
			}

			$html .= '<tr>';

			$html .= "<td style='text-align: right;{$require}'>{$field['title']}</td>";
			$html .= "<td><input type='text' name='MONETA_ADDITIONAL_INFO_{$key}' value='' maxlength='{$maxlength}' size='{$max_size}'></td>";

			$html .= '</tr>';

		}

		$html .= '</table>';

		echo $html;
	}

	public static function saveMonetaAdditionalFileds()
	{
		// $request = Application::getInstance()->getContext()->getRequest();
	}

	public static function drawMonetaAdditionalForm($arFields = array())
	{
		if (!is_array($arFields) || count($arFields))
		{
			return '';
		}

		foreach ($arFields as $fieldName => $field)
		{

		}
	}

	public static function drawMonetaAdditionalTable($arFields)
	{
		$table_html = '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="adm-detail-content-table edit-table">';
		foreach ($arFields as $field)
		{
			$table_html .= self::drawField($field);
		}
		$table_html .= '</table>';

		return $table_html;
	}

	protected static function drawField($field)
	{
		if (!isset($field['type']))
		{
			return 'The field ' . $field['column_name'] . 'has no type';
		}

		$type = $field['type'];

		if ($type === 'section')
		{
			$table_row_html = '<tr class="heading">';
			$table_row_html .= '<td colspan="2">' . $field['title'] . '</td>';
			$table_row_html .= '</tr>';
		}
		else
		{
			$table_row_html = '<tr id="moneta_' . $field['column_name'] . '">';
			$table_row_html .= self::drawFieldTitle($field);
			$table_row_html .= '<td>';
			switch ($type)
			{
				case 'integer':
				case 'string':
					$input = array(
						'type' => 'text',
						'name' => $field['column_name'],
					);
					$input['size'] = isset($field['size']) ? (int)$field['size'] : 50;
					$input['id'] = isset($field['id']) ? (string)$field['size'] : 'field_' . $field['column_name'];
					if (isset($field['maxlength']))
					{
						$input['maxlength'] = (int)$field['maxlength'];
					}
					if (isset($field['value']))
					{
						$input['value'] = $field['value'];
					}
					if (isset($field['placeholder']))
					{
						$input['placeholder'] = $field['placeholder'];
					}
					if (isset($field['class']))
					{
						$input['class'] = $field['class'];
					}
					$table_row_html .= self::drawInput($input);
					break;
				case 'enum':
					$select = array(
						'name' => $field['column_name'],
						'options' => $field['values'],
					);
					$select['id'] = isset($field['id']) ? (string)$field['size'] : 'field_' . $field['column_name'];
					if (isset($field['value']))
					{
						$input['value'] = $field['value'];
					}
					if (isset($field['default_value']))
					{
						$input['default_value'] = $field['default_value'];
					}
					if (isset($field['class']))
					{
						$input['class'] = $field['class'];
					}
					$table_row_html .= self::drawSelect($select);
					break;
				case 'date':
					$table_row_html .= CalendarDate($field['column_name'], $field['value']);
					break;
				default:
					break;
			}
			$table_row_html .= '</td>';
			$table_row_html .= '</tr>';
		}

		return $table_row_html;
	}

	protected static function drawFieldTitle($field)
	{
		$row_title_html = '<td width="40%">';
		if($field['required'])
		{
			$row_title_html .= '<span style="font-weight: bold">' . $field['title'] . '</span>';
		}
		else
		{
			$row_title_html .= $field['title'];
		}
		$row_title_html .= '</td>';

		return $row_title_html;
	}

	protected static function drawInput($field)
	{
		$input_attr = array();
		foreach ($field as $attr_name => $attr_value)
		{
			$input_attr[] = $attr_name . '="' . $attr_value . '"';
		}

		return '<input ' . implode($input_attr) . ' />';
	}

	protected static function drawSelect($field)
	{
		$select_attr = array();
		foreach ($field as $attr_name => $attr_value)
		{
			if ($attr_name == 'options')
			{
				continue;
			}
			$select_attr[] = $attr_name . '="' . $attr_value . '"';
		}

		$select_html = '<select ' . implode($select_attr) . ' >';

		foreach ($field['options'] as $option_value => $option_text)
		{
			$select_html .= '<option value="' . $option_value . '" ' . ($option_value == $field['value'] ? 'selected' : '') . '>' . $option_text . '</option>';
		}

		$select_html .= '</select>';

		return $select_html;
	}

	public static function fillFieldValues(&$arFields = array(), $arValues = array())
	{
		if(empty($arValues))
		{
			return false;
		}

		foreach ($arFields as &$arTab)
		{
			foreach ($arTab as &$field)
			{
				if ($field['type'] == 'date')
				{
					$field['value'] = $arValues[$field['column_name']]->format('d.m.Y');
				}
				elseif ($field['type'] == 'datetime')
				{
					$field['value'] = $arValues[$field['column_name']]->format('d.m.Y H:i:s');
				}
				else
				{
					$field['value'] = $arValues[$field['column_name']];
				}
			}
		}

		return true;
	}
}