<?

namespace Citrus\Tszh\Types;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * ���� ������� ������ (����������� ����, ���������� ����)
 */
final class AccountType extends Enum
{
	const INDIVIDUAL	= 1;
	const LEGAL_ENTITY	= 2;

	/**
	 * ��������� �������� ���������� ����
	 *
	 * @param string $name
	 * @return string
	 */
	public static function getTitle($name)
	{
		return Loc::getMessage("CITRUS_TSZH_ACCOUNT_TYPE_" . $name);
	}

	/**
	 * ��������� ������ ���������� ��� ��������
	 *
	 * @return array
	 */
	public static function getTitles()
	{
		$titles = array();
		foreach (self::getConstants() as $name=>$val) {
			$titles[$val] = self::getTitle($name);
		}
		return $titles;
	}
}
