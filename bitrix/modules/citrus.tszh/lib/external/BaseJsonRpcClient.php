<?php
/**
 * Base JSON-RPC 2.0 Client
 * @package    Eaze
 * @subpackage Model
 * @author     Sergeyfast
 * @link       http://www.jsonrpc.org/specification
 */
class BaseJsonRpcClient {

	/**
	 * Use Objects in Result
	 * @var bool
	 */
	public $UseObjectsInResults = false;

	/**
	 * Server URL
	 * @var string
	 */
	public $serverUrl = '';

	/**
	 * Current Request id
	 * @var int
	 */
	private $id = 1;

	/**
	 * Is Batch Call Flag
	 * @var bool
	 */
	private $isBatchCall = false;

	/**
	 * Batch Calls
	 * @var BaseJsonRpcCall[]
	 */
	private $batchCalls = array();

	/**
	 * Batch Notifications
	 * @var BaseJsonRpcCall[]
	 */
	private $batchNotifications = array();

	/**
	 * Create New JsonRpc client
	 * @param string $serverUrl
	 * @return BaseJsonRpcClient
	 */
	public function __construct( $serverUrl ) {
		$this->serverUrl = $serverUrl;
	}


	/**
	 * Get Next Request Id
	 * @param bool $isNotification
	 * @return int
	 */
	protected function getRequestId( $isNotification = false ) {
		return $isNotification ? null : $this->id++;
	}


	/**
	 * Begin Batch Call
	 * @return bool
	 */
	public function BeginBatch() {
		if ( !$this->isBatchCall ) {
			$this->batchNotifications = array();
			$this->batchCalls         = array();
			$this->isBatchCall        = true;
			return true;
		}

		return false;
	}


	/**
	 * Commit Batch
	 */
	public function CommitBatch() {
		$result = false;
		if ( !$this->isBatchCall || ( !$this->batchCalls && !$this->batchNotifications ) ) {
			return $result;
		}

		$result = $this->processCalls( array_merge( $this->batchCalls, $this->batchNotifications ) );
		$this->RollbackBatch();

		return $result;
	}


	/**
	 * Rollback Calls
	 * @return bool
	 */
	public function RollbackBatch() {
		$this->isBatchCall = false;
		$this->batchCalls  = array();

		return true;
	}


	/**
	 * Process Call
	 * @param string $method
	 * @param array  $parameters
	 * @param int    $id
	 * @return mixed
	 */
	protected function call( $method, $parameters = array(), $id = null ) {
		$call = new BaseJsonRpcCall( $method, $parameters, $id );
		if ( $this->isBatchCall ) {
			if ( $call->Id ) {
				$this->batchCalls[$call->Id] = $call;
			} else {
				$this->batchNotifications[] = $call;
			}
		} else {
			$this->processCalls( array( $call ) );
		}

		return $call;
	}


	/**
	 * Process Magic Call
	 * @param string $method
	 * @param array $parameters
	 * @return BaseJsonRpcCall
	 */
	public function __call( $method, $parameters = array() ) {
		return $this->call( $method, $parameters, $this->getRequestId() );
	}


	/**
	 * Process Calls
	 * @param BaseJsonRpcCall[] $calls
	 * @return mixed
	 */
	protected function processCalls( $calls ) {
		// Prepare Data
		$singleCall = !$this->isBatchCall ? reset( $calls ) : null;
		$result     = $this->batchCalls ? array_values( array_map( 'BaseJsonRpcCall::GetCallData', $calls ) ) : BaseJsonRpcCall::GetCallData( $singleCall );

		$http = new CHTTP();
		$http->follow_redirect = true;
		$http->http_timeout = 10;
		$http->SetAdditionalHeaders(array( 'Content-Type' => 'application/json' ));
		if (!$http->HTTPQuery("POST", $this->serverUrl, json_encode($result)))
		{
			$result = array(
				'error' => array(
					'message' => $http->errstr . ($http->errno ? ' (' . $http->errno . ')' : ''),
					'code' => $http->errno,
				),
			);
			if ( $singleCall )
				$singleCall->SetResult( $result, $this->UseObjectsInResults );
			return false;
		}
		$data = json_decode( $http->result, ! $this->UseObjectsInResults );

		// Process Results for Batch Calls
		if ( $this->batchCalls ) {
			foreach ( $data as $dataCall ) {
				// Problem place?
				$key  = $this->UseObjectsInResults ? $dataCall->id : $dataCall['id'];
				$this->batchCalls[$key]->SetResult( $dataCall, $this->UseObjectsInResults );
			}
		} else {
			// Process Results for Call
			$singleCall->SetResult( $data, $this->UseObjectsInResults );
		}

		return true;
	}
}


/**
 * Base Json Rpc Call
 * @package    Eaze
 * @subpackage Model
 * @author     Sergeyfast
 * @link       http://www.jsonrpc.org/specification
 */
class BaseJsonRpcCall {

	/** @var int */
	public $Id;

	/** @var string */
	public $Method;

	/** @var array */
	public $Params;

	/** @var array */
	public $Error;

	/** @var mixed */
	public $Result;


	/**
	 * Has Error
	 * @return bool
	 */
	public function HasError() {
		if (!empty( $this->Error ))
			return true;
		if (is_null($this->Result))
		{
			$this->Error = Array(
				'message' => 'Empty result',
				'code' => 0,
			);
			return true;
		}
		return false;
	}


	/**
	 * @param string $method
	 * @param array  $params
	 * @param string $id
	 */
	public function __construct( $method, $params, $id ) {
		$this->Method = $method;
		$this->Params = $params;
		$this->Id     = $id;
	}


	/**
	 * Get Call Data
	 * @param BaseJsonRpcCall $call
	 * @return array
	 */
	public static function GetCallData( BaseJsonRpcCall $call ) {
		$params = $call->Params;
		global $APPLICATION;
		if (isset($APPLICATION) && is_object($APPLICATION))
			$params = $APPLICATION->ConvertCharsetArray($params, SITE_CHARSET, 'utf-8');
		return array(
			'jsonrpc'  => '2.0'
		, 'id'     => $call->Id
		, 'method' => $call->Method
		, 'params' => $params
		);
	}


	/**
	 * Set Result
	 * @param mixed $data
	 * @param bool  $useObjects
	 */
	public function SetResult( $data, $useObjects = false ) {
		global $APPLICATION;
		if (isset($APPLICATION) && is_object($APPLICATION))
			$data = $APPLICATION->ConvertCharsetArray($data, 'utf-8', SITE_CHARSET);
		if ( $useObjects ) {
			$this->Error  = property_exists( $data, 'error' ) ? $data->error : null;
			$this->Result = property_exists( $data, 'result' ) ? $data->result : null;
		} else {
			$this->Error  = isset( $data['error'] ) ? $data['error'] : null;
			$this->Result = isset( $data['result'] ) ? $data['result'] : null;
		}
	}
}

