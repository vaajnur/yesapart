<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class ChargesMetersTable
 *
 * Fields:
 * <ul>
 * <li> CHARGE_ID int mandatory
 * <li> METER_ID int mandatory
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class ChargesMetersTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_charges_meters';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'CHARGE_ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'title' => Loc::getMessage('CHARGES_METERS_ENTITY_CHARGE_ID_FIELD'),
			),
			'METER_ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'title' => Loc::getMessage('CHARGES_METERS_ENTITY_METER_ID_FIELD'),
			),
		);
	}
}