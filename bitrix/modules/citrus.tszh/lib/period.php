<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class PeriodTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> TSZH_ID int mandatory
 * <li> DATE string(50) mandatory
 * <li> TIMESTAMP_X datetime mandatory default 'CURRENT_TIMESTAMP'
 * <li> ACTIVE bool optional default 'Y'
 * <li> ONLY_DEBT bool optional default 'N'
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class PeriodTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_period';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('PERIOD_ENTITY_ID_FIELD'),
			),
			'TSZH_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('PERIOD_ENTITY_TSZH_ID_FIELD'),
			),
			'DATE' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateDate'),
				'title' => Loc::getMessage('PERIOD_ENTITY_DATE_FIELD'),
			),
			'TIMESTAMP_X' => array(
				'data_type' => 'datetime',
				'required' => true,
				'title' => Loc::getMessage('PERIOD_ENTITY_TIMESTAMP_X_FIELD'),
			),
			'ACTIVE' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('PERIOD_ENTITY_ACTIVE_FIELD'),
			),
			'ONLY_DEBT' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('PERIOD_ENTITY_ONLY_DEBT_FIELD'),
			),
		);
	}

	/**
	 * Returns validators for DATE field.
	 *
	 * @return array
	 */
	public static function validateDate()
	{
		return array(
			new Main\Entity\Validator\Length(null, 50),
		);
	}
}