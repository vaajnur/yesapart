<?php

namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\ORM\Fields\Relations\OneToMany;

Loc::loadMessages(__FILE__);

/**
 * Class ServicesPeriodTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> COMPONENT bool optional default 'N'
 * <li> GROUP string(50) optional
 * <li> DEBT_ONLY bool optional default 'N'
 * <li> SERVICE_ID int optional
 * <li> ACCOUNT_PERIOD_ID int optional
 * <li> TIMESTAMP_X datetime mandatory default 'CURRENT_TIMESTAMP'
 * <li> AMOUNT double optional
 * <li> AMOUNTN int optional
 * <li> SUMM double optional
 * <li> SUMM_PAYED double optional
 * <li> SUMM2PAY double optional
 * <li> SORT int mandatory
 * <li> CORRECTION double optional
 * <li> COMPENSATION double optional
 * <li> HAMOUNT double optional
 * <li> HAMOUNTN int optional
 * <li> HNORM double optional
 * <li> HSUMM double optional
 * <li> HSUMM2PAY double optional
 * <li> HMETER_ID int mandatory
 * <li> VOLUMEP double optional
 * <li> VOLUMEH double optional
 * <li> VOLUMEA double optional
 * <li> PENALTIES double optional
 * <li> DEBT_BEG double optional
 * <li> DEBT_END double optional
 * <li> X_FIELDS unknown optional
 * <li> AMOUNT_NORM double optional
 * <li> RATE string(50) optional
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class ServicesPeriodTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_services_period';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_ID_FIELD'),
			),
			'COMPONENT' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_COMPONENT_FIELD'),
			),
			'GROUP' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateGroup'),
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_GROUP_FIELD'),
			),
			'GUID' => array(
				'data_type' => 'string',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_GUID_FIELD'),
			),
			'DEBT_ONLY' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_DEBT_ONLY_FIELD'),
			),
			'SERVICE_ID' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_SERVICE_ID_FIELD'),
			),
			'ACCOUNT_PERIOD_ID' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_ACCOUNT_PERIOD_ID_FIELD'),
			),
			'TIMESTAMP_X' => array(
				'data_type' => 'datetime',
				'required' => true,
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_TIMESTAMP_X_FIELD'),
			),
			'AMOUNT' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_AMOUNT_FIELD'),
			),
			'AMOUNTN' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_AMOUNTN_FIELD'),
			),
			'SUMM' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_SUMM_FIELD'),
			),
			'SUMM_PAYED' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_SUMM_PAYED_FIELD'),
			),
			'SUMM2PAY' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_SUMM2PAY_FIELD'),
			),
			'SORT' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_SORT_FIELD'),
			),
			'CORRECTION' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_CORRECTION_FIELD'),
			),
			'COMPENSATION' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_COMPENSATION_FIELD'),
			),
			'HAMOUNT' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_HAMOUNT_FIELD'),
			),
			'HAMOUNTN' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_HAMOUNTN_FIELD'),
			),
			'HNORM' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_HNORM_FIELD'),
			),
			'HSUMM' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_HSUMM_FIELD'),
			),
			'HSUMM2PAY' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_HSUMM2PAY_FIELD'),
			),
			'HMETER_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_HMETER_ID_FIELD'),
			),
			'VOLUMEP' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_VOLUMEP_FIELD'),
			),
			'VOLUMEH' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_VOLUMEH_FIELD'),
			),
			'VOLUMEA' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_VOLUMEA_FIELD'),
			),
			'PENALTIES' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_PENALTIES_FIELD'),
			),
			'DEBT_BEG' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_DEBT_BEG_FIELD'),
			),
			'DEBT_END' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_DEBT_END_FIELD'),
			),
			'X_FIELDS' => array(
				'data_type' => 'UNKNOWN',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_X_FIELDS_FIELD'),
			),
			'AMOUNT_NORM' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_AMOUNT_NORM_FIELD'),
			),
			'RATE' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateRate'),
				'title' => Loc::getMessage('SERVICES_PERIOD_ENTITY_RATE_FIELD'),
			),
			(new OneToMany('DETAILS_PAYMENTS', DetailsPaymentsTable::class, 'SERVICES_PERIOD'))
				->configureJoinType('left'),

		);
	}

	/**
	 * Returns validators for GROUP field.
	 *
	 * @return array
	 */
	public static function validateGroup()
	{
		return array(
			new Main\Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for RATE field.
	 *
	 * @return array
	 */
	public static function validateRate()
	{
		return array(
			new Main\Entity\Validator\Length(null, 50),
		);
	}
}