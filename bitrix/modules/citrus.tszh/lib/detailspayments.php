<?php

namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Entity,
	Bitrix\Main\ORM\Fields\Relations\Reference,
	Bitrix\Main\ORM\Query\Join;

Loc::loadMessages(__FILE__);

/**
 * Class DetailsPaymentsTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> GUID string(500) mandatory
 * <li> NAME string(500) mandatory
 * <li> SUM_TO_PAY double mandatory
 * <li> SERVICES_PERIOD_ID int mandatory
 * </ul>
 *
 * @package Bitrix\Tszh
 **/
class DetailsPaymentsTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_details_payments';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return [
			new Entity\IntegerField('ID', [
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('DETAILS_PAYMENTS_ENTITY_ID_FIELD'),
			]),
			new Entity\StringField('GUID', [

				'required' => true,
				'validation' => [__CLASS__, 'validateGuid'],
				'title' => Loc::getMessage('DETAILS_PAYMENTS_ENTITY_GUID_FIELD'),
			]),
			new Entity\StringField('NAME', [
				'required' => true,
				'validation' => [__CLASS__, 'validateName'],
				'title' => Loc::getMessage('DETAILS_PAYMENTS_ENTITY_NAME_FIELD'),
			]),
			new Entity\FloatField('SUM_TO_PAY', [
				'required' => true,
				'title' => Loc::getMessage('DETAILS_PAYMENTS_ENTITY_SUM_TO_PAY_FIELD'),
			]),
			new Entity\IntegerField('SERVICES_PERIOD_ID', [
				'required' => true,
				'title' => Loc::getMessage('DETAILS_PAYMENTS_ENTITY_SERVICES_PERIOD_ID_FIELD'),
			]),
			(new Reference(
				'SERVICES_PERIOD',
				ServicesPeriodTable::class,
				Join::on('this.SERVICES_PERIOD_ID', 'ref.ID')
			))
				->configureJoinType('inner'),
		];
	}

	/**
	 * Returns validators for GUID field.
	 *
	 * @return array
	 */
	public static function validateGuid()
	{
		return [
			new Main\Entity\Validator\Length(null, 500),
		];
	}

	/**
	 * Returns validators for NAME field.
	 *
	 * @return array
	 */
	public static function validateName()
	{
		return [
			new Main\Entity\Validator\Length(null, 500),
		];
	}
}

