<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

/**
 * Class AccountsTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> CURRENT bool optional default 'N'
 * <li> TYPE int optional default 1
 * <li> USER_ID int optional
 * <li> TSZH_ID int optional
 * <li> XML_ID string(100) mandatory
 * <li> EXTERNAL_ID string(255) optional
 * <li> NAME string(100) optional
 * <li> HOUSE_ID int optional
 * <li> FLAT string(10) mandatory
 * <li> FLAT_ABBR string(20) optional
 * <li> AREA double mandatory
 * <li> LIVING_AREA double mandatory
 * <li> PEOPLE int mandatory
 * <li> FLAT_TYPE string(20) optional
 * <li> FLAT_PROPERTY_TYPE string(20) optional
 * <li> REGISTERED_PEOPLE int optional
 * <li> EXEMPT_PEOPLE int optional
 * </ul>
 *
 * @package Bitrix\Tszh
 **/
class AccountsTable extends Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_accounts';
	}

	public static function getUfId()
	{
		return 'TSZH_ACCOUNT';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		$sqlHelper = Application::getConnection()->getSqlHelper();

		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_ID_FIELD'),
			),
			'CURRENT' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_CURRENT_FIELD'),
			),
			'TYPE' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_TYPE_FIELD'),
			),
			'USER_ID' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_USER_ID_FIELD'),
			),
			'TSZH_ID' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_TSZH_ID_FIELD'),
			),
			'XML_ID' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateXmlId'),
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_XML_ID_FIELD'),
			),
			'EXTERNAL_ID' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateExternalId'),
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_EXTERNAL_ID_FIELD'),
			),
			'ELS_MAIN' => array(
				'data_type' => 'string',
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_ELS_MAIN_FIELD'),
			),
			'ELS_OVERHAUL' => array(
				'data_type' => 'string',
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_ELS_OVERHAUL_FIELD'),
			),
			'IZHKU_MAIN' => array(
				'data_type' => 'string',
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_IZHKU_MAIN_FIELD'),
			),
			'IZHKU_OVERHAUL' => array(
				'data_type' => 'string',
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_IZHKU_OVERHAUL_FIELD'),
			),
			'ACTIVE' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateExternalId'),
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_ACTIVE_FIELD'),
			),
			'NAME' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateName'),
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_NAME_FIELD'),
			),
			'HOUSE_ID' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_HOUSE_ID_FIELD'),
			),
			'FLAT' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateFlat'),
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_FLAT_FIELD'),
			),
			'FLAT_ABBR' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateFlatAbbr'),
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_FLAT_ABBR_FIELD'),
			),
			'AREA' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_AREA_FIELD'),
			),
			'LIVING_AREA' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_LIVING_AREA_FIELD'),
			),
			'PEOPLE' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_PEOPLE_FIELD'),
			),
			'FLAT_TYPE' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateFlatType'),
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_FLAT_TYPE_FIELD'),
			),
			'FLAT_PROPERTY_TYPE' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateFlatPropertyType'),
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_FLAT_PROPERTY_TYPE_FIELD'),
			),
			'REGISTERED_PEOPLE' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_REGISTERED_PEOPLE_FIELD'),
			),
			'EXEMPT_PEOPLE' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('ACCOUNTS_ENTITY_EXEMPT_PEOPLE_FIELD'),
			),
			new \Bitrix\Main\Entity\ReferenceField(
				'HOUSE',
				'Citrus\Tszh\HouseTable',
				array('=this.HOUSE_ID' => 'ref.ID'),
				array('join_type' => 'LEFT')
			),
			new \Bitrix\Main\Entity\ReferenceField(
				'USER',
				'Bitrix\Main\UserTable',
				array('=this.USER_ID' => 'ref.ID'),
				array('join_type' => 'LEFT')
			),
			new \Bitrix\Main\Entity\ExpressionField(
				'FULL_ADDRESS',
				'%s',
				'HOUSE.FULL_ADDRESS'
			),
			new \Bitrix\Main\Entity\ExpressionField(
				'FULL_NAME',
				$sqlHelper->getConcatFunction('%s', "' '", "%s", "' '", "%s"),
				array('USER.LAST_NAME', 'USER.NAME', 'USER.SECOND_NAME')
			)
		);
	}

	/**
	 * Returns validators for XML_ID field.
	 *
	 * @return array
	 */
	public static function validateXmlId()
	{
		return array(
			new Main\Entity\Validator\Length(null, 100),
		);
	}

	/**
	 * Returns validators for EXTERNAL_ID field.
	 *
	 * @return array
	 */
	public static function validateExternalId()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for NAME field.
	 *
	 * @return array
	 */
	public static function validateName()
	{
		return array(
			new Main\Entity\Validator\Length(null, 100),
		);
	}

	/**
	 * Returns validators for FLAT field.
	 *
	 * @return array
	 */
	public static function validateFlat()
	{
		return array(
			new Main\Entity\Validator\Length(null, 10),
		);
	}

	/**
	 * Returns validators for FLAT_ABBR field.
	 *
	 * @return array
	 */
	public static function validateFlatAbbr()
	{
		return array(
			new Main\Entity\Validator\Length(null, 20),
		);
	}

	/**
	 * Returns validators for FLAT_TYPE field.
	 *
	 * @return array
	 */
	public static function validateFlatType()
	{
		return array(
			new Main\Entity\Validator\Length(null, 20),
		);
	}

	/**
	 * Returns validators for FLAT_PROPERTY_TYPE field.
	 *
	 * @return array
	 */
	public static function validateFlatPropertyType()
	{
		return array(
			new Main\Entity\Validator\Length(null, 20),
		);
	}
}