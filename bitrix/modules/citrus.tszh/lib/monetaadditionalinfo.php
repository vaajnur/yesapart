<?php
/**
 * @package
 * @subpackage
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2016 vdgb-soft.ru
 * @date 08.12.2016 15:07
 */

namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Entity,
	Bitrix\Main\Localization\Loc;
use Bitrix\Main\Entity\Event;

Loc::loadMessages(__FILE__);

/**
 * Class MonetaAdditionalInfoTable
 *
 * Fields:
 * <ul>
 * <li> TSZH_ID int mandatory
 * <li> ACTUAL_ADDRESS string(255) mandatory
 * <li> FIO_CONTACT string(255) mandatory
 * <li> PHONE_CONTACT string(255) mandatory
 * <li> FINANCE_EMAIL string(255) mandatory
 * <li> TECHNICAL_EMAIL string(255) mandatory
 * </ul>
 *
 * @package Bitrix\Tszh
 **/
class MonetaAdditionalInfoTable extends Main\Entity\DataManager implements IEntity
{
	/**
	 * @var array|null
	 */
	private static $editableMap = null;

	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_moneta_additional_info';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			new Entity\IntegerField('TSZH_ID', array(
				'required' => true,
				'primary' => true,
				// 'autocomplete' => true,
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_TSZH_ID_FIELD'),
			)),

			// Organization personal
			new Entity\IntegerField('PLANNED_TURNOVERS', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_PLANNED_TURNOVERS_FIELD'),
				'required' => true,
			)),
			new Entity\EnumField('POSITION_DIRECTOR', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_POSITION_DIRECTOR_FIELD'),
				'required' => true,
				'values' => array(
					'DIRECTOR',
					'GENERAL_DIRECTOR',
					'EXECUTIVE_DIRECTOR',
					'OTHER',
				),
			)),
			new Entity\StringField('POSITION_DIRECTOR_DETAILS', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_POSITION_DIRECTOR_DETAILS_FIELD'),
			)),
			new Entity\EnumField('ACTING_DOCUMENT', array(
				'required' => true,
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_ACTING_DOCUMENT_FIELD'),
				'values' => array(
					'ARTICLES_OF_ASSOCIATION',
					'POWER_OF_ATTORNEY',
					'OTHER',
				),
			)),
			new Entity\StringField('ACTING_DOCUMENT_DETAILS', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_ACTING_DOCUMENT_DETAILS_FIELD'),
			)),
			new Entity\StringField('ATTORNEY_NUMBER', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_ATTORNEY_NUMBER_FIELD'),
			)),
			new Entity\DateField('ATTORNEY_DATE_FROM', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_ATTORNEY_DATE_FROM_FIELD'),
			)),
			new Entity\DateField('ATTORNEY_DATE_TO', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_ATTORNEY_DATE_TO_FIELD'),
			)),
			new Entity\StringField('AGREEMENT_SIGNER_FIO', array(
				'required' => true,
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_AGREEMENT_SIGNER_FIO_FIELD'),
			)),
			new Entity\DateField('REGISTRATION_DATE', array(
				'required' => true,
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_REGISTRATION_DATE_FIELD'),
			)),
			new Entity\StringField('REGISTRATION_AUTHORITY', array(
				'required' => true,
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_REGISTRATION_AUTHORITY_FIELD'),
			)),
			new Entity\StringField('FIO_ACCOUNTANT', array(
				'required' => true,
				'validation' => array(__CLASS__, 'validateFioContact'),
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_FIO_ACCOUNTANT_FIELD'),
			)),
			new Entity\StringField('FIO_CONTACT', array(
				'required' => true,
				'validation' => array(__CLASS__, 'validateFioContact'),
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_FIO_CONTACT_FIELD'),
			)),
			/*new Entity\StringField('FIO_CONTACT', array(
				'required' => true,
				'validation' => array(__CLASS__, 'validateFioContact'),
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_FIO_CONTACT_FIELD'),
			)),*/

			// Director
			new Entity\StringField('DIRECTOR_NAME', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_DIRECTOR_NAME_FIELD'),
				'required' => true,
			)),
			new Entity\DateField('DIRECTOR_DATE_OF_BIRTH', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_DIRECTOR_DATE_OF_BIRTH_FIELD'),
				'required' => true,
			)),
			new Entity\StringField('DIRECTOR_PLACE_OF_BIRTH', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_DIRECTOR_PLACE_OF_BIRTH_FIELD'),
				'required' => true,
			)),
			new Entity\StringField('DIRECTOR_LEGAL_ADDRESS', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_DIRECTOR_LEGAL_ADDRESS_FIELD'),
				'required' => true,
			)),
			new Entity\StringField('DIRECTOR_POST_ADDRESS', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_DIRECTOR_POST_ADDRESS_FIELD'),
				'required' => true,
			)),
			new Entity\StringField('DIRECTOR_PHONE_CONTACT', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_DIRECTOR_PHONE_CONTACT_FIELD'),
				'required' => true,
			)),
			new Entity\IntegerField('DIRECTOR_IDENTITY_DOCUMENT_SERIES', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_DIRECTOR_IDENTITY_DOCUMENT_SERIES_FIELD'),
				'required' => true,
			)),
			new Entity\IntegerField('DIRECTOR_IDENTITY_DOCUMENT_NUMBER', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_DIRECTOR_IDENTITY_DOCUMENT_NUMBER_FIELD'),
				'required' => true,
			)),
			new Entity\DateField('DIRECTOR_IDENTITY_DOCUMENT_ISSUE_DATE', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_DIRECTOR_IDENTITY_DOCUMENT_ISSUE_DATE_FIELD'),
				'required' => true,
			)),
			new Entity\StringField('DIRECTOR_IDENTITY_DOCUMENT_ISSUED_BUREAU', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_DIRECTOR_IDENTITY_DOCUMENT_ISSUED_BUREAU_FIELD'),
				'required' => true,
			)),

			// Contacts
			new Entity\StringField('LEGAL_ADDRESS', array(
				'required' => true,
				'validation' => array(__CLASS__, 'validateActualAddress'),
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_LEGAL_ADDRESS_FIELD'),
			)),
			new Entity\StringField('POST_ADDRESS', array(
				'required' => true,
				'validation' => array(__CLASS__, 'validateActualAddress'),
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_POST_ADDRESS_FIELD'),
			)),
			new Entity\StringField('PHONE_CONTACT', array(
				'required' => true,
				// 'validation' => array(__CLASS__, 'validatePhoneContact'),
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_PHONE_CONTACT_FIELD'),
			)),
			new Entity\StringField('PHONE_ACCOUNTANT', array(
				'required' => true,
				// 'validation' => array(__CLASS__, 'validatePhoneContact'),
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_PHONE_ACCOUNTANT_FIELD'),
			)),
			new Entity\StringField('PHONE_SUPPORT', array(
				'required' => true,
				// 'validation' => array(__CLASS__, 'validatePhoneContact'),
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_PHONE_SUPPORT_FIELD'),
			)),
			/*new Entity\StringField('CONTACT_EMAIL', array(
				'required' => true,
				// 'validation' => array(__CLASS__, 'validatePhoneContact'),
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_CONTACT_EMAIL_FIELD'),
			)),*/
			new Entity\StringField('FINANCE_EMAIL', array(
				'required' => true,
				'validation' => array(__CLASS__, 'validateFinanceEmail'),
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_FINANCE_EMAIL_FIELD'),
			)),
			new Entity\StringField('TECHNICAL_EMAIL', array(
				'required' => true,
				'validation' => array(__CLASS__, 'validateTechnicalEmail'),
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_TECHNICAL_EMAIL_FIELD'),
			)),

			// Finance
			new Entity\StringField('CAPITAL_TYPE', array(
				'required' => true,
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_CAPITAL_TYPE_FIELD'),
			)),
			new Entity\IntegerField('PAID_CAPITAL_SIZE', array(
				'required' => true,
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_PAID_CAPITAL_SIZE_FIELD'),
			)),
			new Entity\IntegerField('REGISTERED_CAPITAL_SIZE', array(
				'required' => true,
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_REGISTERED_CAPITAL_SIZE_FIELD'),
			)),
			new Entity\EnumField('BUDGET_ARREARS_ABSENCE', array(
				'required' => true,
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_BUDGET_ARREARS_ABSENCE_FIELD'),
				'values' => array(
					// 'NONE' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_BUDGET_ARREARS_ABSENCE_FIELD_VALUE_NONE'),
					'NONE',
					// 'EXIST' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_BUDGET_ARREARS_ABSENCE_FIELD_VALUE_EXIST'),
					'EXIST',
				),
			)),

			// Beneficiary
			new Entity\StringField('BENEFICIARY_NAME', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_BENEFICIARY_NAME_FIELD'),
			)),
			new Entity\DateField('BENEFICIARY_DATE_OF_BIRTH', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_BENEFICIARY_DATE_OF_BIRTH_FIELD'),
			)),
			new Entity\StringField('BENEFICIARY_PLACE_OF_BIRTH', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_BENEFICIARY_PLACE_OF_BIRTH_FIELD'),
			)),
			new Entity\StringField('BENEFICIARY_LEGAL_ADDRESS', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_BENEFICIARY_LEGAL_ADDRESS_FIELD'),
			)),
			new Entity\StringField('BENEFICIARY_POST_ADDRESS', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_BENEFICIARY_POST_ADDRESS_FIELD'),
			)),
			new Entity\StringField('BENEFICIARY_PHONE_CONTACT', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_BENEFICIARY_PHONE_CONTACT_FIELD'),
			)),
			new Entity\IntegerField('BENEFICIARY_IDENTITY_DOCUMENT_SERIES', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_BENEFICIARY_IDENTITY_DOCUMENT_SERIES_FIELD'),
			)),
			new Entity\IntegerField('BENEFICIARY_IDENTITY_DOCUMENT_NUMBER', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_BENEFICIARY_IDENTITY_DOCUMENT_NUMBER_FIELD'),
			)),
			new Entity\DateField('BENEFICIARY_IDENTITY_DOCUMENT_ISSUE_DATE', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_BENEFICIARY_IDENTITY_DOCUMENT_ISSUE_DATE_FIELD'),
			)),
			new Entity\StringField('BENEFICIARY_IDENTITY_DOCUMENT_ISSUED_BUREAU', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_BENEFICIARY_IDENTITY_DOCUMENT_ISSUED_BUREAU_FIELD'),
			)),

			// Docs
			new Entity\StringField('PARTNER_AGREEMENT_FILE', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_PARTNER_AGREEMENT_FILE_FIELD'),
			)),
			new Entity\DateField('PARTNER_AGREEMENT_SENT_DATE', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_PARTNER_AGREEMENT_SENT_DATE_FIELD'),
			)),
			new Entity\StringField('PARTNER_AGREEMENT_SENT_METHOD', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_PARTNER_AGREEMENT_SENT_METHOD_FIELD'),
			)),

			new Entity\BooleanField('DATA_SENDED', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_DATA_SENDED_FIELD'),
				'values' => array('Y', 'N'),
				'default_value' => 'N'
			)),
			new Entity\EnumField('STATUS', array(
				'title' => Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_STATUS_FIELD'),
				'values' => array('N', 'V', 'W', 'Y'),
				'default_value' => 'N',
			)),

			// reference tszh table
			new Entity\ReferenceField(
				'TSZH',
				'Citrus\Tszh\Tszh',
				array('=this.TSZH_ID' => 'ref.ID')
			),
		);
	}

	/**
	 * Returns validators for ACTUAL_ADDRESS field.
	 *
	 * @return array
	 */
	public static function validateActualAddress()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for FIO_CONTACT field.
	 *
	 * @return array
	 */
	public static function validateFioContact()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for PHONE_CONTACT field.
	 *
	 * @return array
	 */
	public static function validatePhoneContact()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for FINANCE_EMAIL field.
	 *
	 * @return array
	 */
	public static function validateFinanceEmail()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for TECHNICAL_EMAIL field.
	 *
	 * @return array
	 */
	public static function validateTechnicalEmail()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * ���������� ������ � ������� ����� ��������
	 * @return array
	 */
	public static function getFieldNames()
	{
		return array_keys(static::getEditableFields());
	}

	/**
	 * @param string $fieldName
	 *
	 * @return string
	 * @throws \Exception
	 */
	public static function getFieldTitle($fieldName)
	{
		$map = static::getMap();
		if (array_key_exists($fieldName, $map))
		{
			return $map[$fieldName]['title'];
		}
		else
		{
			throw new ArgumentException('Unknown $fieldName specified', 'fieldName');
		}
	}

	/**
	 * ���������� ������ ������������� ����� ���������� ������ getMap()
	 * @return array
	 */
	public static function getEditableFields()
	{
		if (!isset(static::$editableMap))
		{
			static::$editableMap = array();
			$map = static::getMap();
			foreach ($map as $field)
			{
				if ($field instanceof Entity\ExpressionField
				    || $field instanceof Entity\ReferenceField
				)
				{
					continue;
				}

				static::$editableMap[$field->getName()] = array(
					'title' => $field->getTitle(),
					'column_name' => $field->getColumnName(),
					'primary' => $field->isPrimary(),
					'autocomlite' => $field->isAutocomplete(),
					'required' => $field->isRequired(),
					'type' => $field->getDataType(),
					'validators' => $field->getValidators(),
				);

				if ($field instanceof Entity\EnumField
				    || $field instanceof Entity\BooleanField
				)
				{
					$arValues = $field->getValues();
					foreach ($arValues as $val)
					{
						static::$editableMap[$field->getName()]['values'][$val] = Loc::getMessage('MONETA_ADDITIONAL_INFO_ENTITY_' . $field->getColumnName() . '_FIELD_VALUE_' . $val);
					}
					// static::$editableMap[$field->getName()]['values'] = $field->getValues();
				}

			}
		}

		return static::$editableMap;
	}

	public static function onAfterAdd(Event $event)
	{
		static::sendAddInfo($event);
	}

	public static function onAfterUpdate(Event $event)
	{
		static::sendAddInfo($event);
	}

	private static function sendAddInfo(Event $event)
	{
		if (Main\Loader::includeModule('citrus.tszhpayment'))
		{
			$data = $event->getParameter("fields");
			$arSendData = static::prepareSendData($data);
			\CTszhPaymentGatewayMoneta::getInstance()->saveAddInfo($arSendData);
		}
	}

	private static function prepareSendData($data)
	{
		$arSendData = array();
		foreach ($data as $key => $field)
		{
			if ($field instanceof \Bitrix\Main\Type\Date
			    || $field instanceof \Bitrix\Main\Type\DateTime)
			{
				$arSendData[$key] = $field->format('d.m.Y');
			}
			else
			{
				$arSendData[$key] = $field;
			}
		}
		return $arSendData;
	}
}