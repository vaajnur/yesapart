<?php
namespace Citrus\Tszh;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class ConfirmTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ACCOUNT_ID string(100) optional
 * <li> IP string(50) optional
 * <li> DATE string(500) optional
 * <li> CONFIRM_CHECK string(50) optional
 * </ul>
 *
 * @package Bitrix\Tszh
 **/
class ConfirmTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_tszh_confirm';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('CONFIRM_ENTITY_ID_FIELD'),
            ),
            'USER_ID' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateUserId'),
                'title' => Loc::getMessage('CONFIRM_ENTITY_USER_ID_FIELD'),
            ),
            'IP' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateIp'),
                'title' => Loc::getMessage('CONFIRM_ENTITY_IP_FIELD'),
            ),
            'DATE' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateDate'),
                'title' => Loc::getMessage('CONFIRM_ENTITY_DATE_FIELD'),
            ),
            'CONFIRM_CHECK' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateConfirmCheck'),
                'title' => Loc::getMessage('CONFIRM_ENTITY_CONFIRM_CHECK_FIELD'),
            ),
            'URL' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateUrl'),
                'title' => Loc::getMessage('CONFIRM_ENTITY_URL_FIELD'),
            ),
            'ACCOUNT_NAME' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateAccountName'),
                'title' => Loc::getMessage('CONFIRM_ENTITY_ACCOUNT_NAME_FIELD'),
            ),
        );
    }

    /**
     * Returns validators for ACCOUNT_ID field.
     *
     * @return array
     */
    public static function validateUserId()
    {
        return array(
            new Main\Entity\Validator\Length(null, 100),
        );
    }

    /**
     * Returns validators for IP field.
     *
     * @return array
     */
    public static function validateIp()
    {
        return array(
            new Main\Entity\Validator\Length(null, 50),
        );
    }

    /**
     * Returns validators for DATE field.
     *
     * @return array
     */
    public static function validateDate()
    {
        return array(
            new Main\Entity\Validator\Length(null, 500),
        );
    }

    /**
     * Returns validators for CONFIRM_CHECK field.
     *
     * @return array
     */
    public static function validateConfirmCheck()
    {
        return array(
            new Main\Entity\Validator\Length(null, 50),
        );
    }

    public static function validateUrl()
    {
        return array(
            new Main\Entity\Validator\Length(null, 500),
        );
    }
    public static function validateAccountName()
    {
        return array(
            new Main\Entity\Validator\Length(null, 500),
        );
    }
}