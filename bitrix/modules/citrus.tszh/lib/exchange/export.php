<?

namespace Citrus\Tszh\Exchange;

abstract class Export
{
	protected $filter = array();

	protected $state;
	protected $tszhId;
	protected $output;
	protected $closeHandle;

	protected $timeLimit = false;

	/**
	 * @param array $state ���������� ��� �������� ���������� �������� ��������� ����� ������ ������ ������
	 * @param null $output
	 * @throws \Exception
	 */
	function __construct(&$state, $output = null)
	{
		if (!is_array($state))
		{
			throw new \Exception("\$state must be an array");
		}

		if ($output)
		{
			if (is_resource($output))
				throw new \Exception('$output must be a resource');
			$this->output = $output;
		}
		else
		{
			$this->output = fopen('php://output', 'w');
			$this->closeHandle = true;
		}

		$this->state = &$state;
	}

	function __destruct()
	{
		if ($this->closeHandle and is_resource($this->output))
			fclose($this->output);
	}

	/**
	 * @param int $tszhId
	 * @return $this
	 * @throws \Exception
	 */
	public function setOrg($tszhId)
	{
		if (!is_numeric($tszhId) || intval($tszhId) <= 0)
		{
			throw new \Exception('Invalid $tszhId argument');
		}

		$this->tszhId = intval($tszhId);
		$this->filter["TSZH_ID"] = $this->tszhId;

		return $this;
	}

	/**
	 * @param int $lastId
	 * @param null|Resource $outputHandle
	 * @return int
	 * @throws \Exception
	 */
	public function run($lastId, $outputHandle = null)
	{
		return 0;
	}
}