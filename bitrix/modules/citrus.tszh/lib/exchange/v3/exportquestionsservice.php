<?
namespace Citrus\Tszh\Exchange\v3;

use Citrus\Tszh\Exchange\ServiceBase;
use Citrus\Tszh\Exchange\Facade;

/**
 * ��������� �������� �������� � ����� ������-������
 *
 * @package Citrus\Tszh\Controller\v3
 */
// TODO ���� ����� ������ ���? ������ ��� http://dezmaryino.ru/
class ExportQuestionsService extends ServiceBase
{
	/**
	 * @throws \Exception
	 */
	public function run()
	{
		global $DB;

		if (!\CModule::IncludeModule("iblock"))
		{
			throw new \Exception(GetMessage("TSZH_1C_ERROR_IBLOCK_MODULE"));
		}

		$arTszh = Facade::getOrg();
		if (!is_array($arTszh) || IntVal($arTszh["ID"]) <= 0)
		{
			throw new \Exception(GetMessage("TSZH_1C_ERROR_ORG_NOT_FOUND"));
		}


		$type = 'questions';
		\COption::SetOptionString("citrus.tszh", "last_{$type}_export_time_" . $arTszh['ID'], time());
		$nLastExport = \COption::GetOptionString("citrus.tszh", "last_{$type}_export_time_committed_" . $arTszh['ID'], 0);
		$strDateFormat = $DB->DateFormatToPHP(\CSite::GetDateFormat("FULL"));
		$strLastExport = IntVal($nLastExport) > 0 ? date($strDateFormat, $nLastExport) : false;

		$arParams['IBLOCK_ID'] = \COption::GetOptionInt('citrus.tszh', 'questions.iblock', 0, $arTszh["SITE_ID"]);
		if ($arParams["IBLOCK_ID"] <= 0)
		{
			throw new \Exception(GetMessage("TSZH_1C_QUESTIONS_EXPORT_IBLOCK_ERROR"));
		}

		$arFilter = array(
			"IBLOCK_ID" => $arParams['IBLOCK_ID'],
		);
		if ($strLastExport)
		{
			$arFilter['>DATE_CREATE'] = $strLastExport;
		}
		$rsElement = \CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, Array());
		$arResult = Array();
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($obElement = $rsElement->GetNextElement())
		{
			$arElement = $obElement->GetFields();
			$arElement["PROPERTY"] = $obElement->GetProperties();
			$arResult[] = $arElement;
		}

		echo
			'<?xml version="1.0" encoding="windows-1251"?>' . "\n" .
			'<questions version="' . $this->facade->getVersion() . '">' . "\n";

		foreach ($arResult
				 as
				 $Items)
		{
			foreach ($Items['PROPERTY']
					 as
					 $Item)
			{
				$SetProperty[$Item['CODE']] = $Item['VALUE'];
			}
			echo "\t<question id=\"{$Items['ID']}\" person=\"{$Items['CREATED_USER_NAME']}\" address=\"{$SetProperty['author_address']}\" phone=\"{$SetProperty['author_phone']}\" email=\"{$SetProperty['author_email']}\">\n";
			echo $Items['PREVIEW_TEXT'] . "\n";
			echo "\t</question>\n";
		}
		echo '</questions>';

		$this->facade->xmlOutput = true;
	}
}