<?
namespace Citrus\Tszh\Exchange\v3;

use Citrus\Tszh\Exchange\ServiceBase;
use Citrus\Tszh\Exchange\Facade;

/**
 * ��������� �������� �������� �� ���������
 * ?mode=export&type=accexistence
 * http://wiki.citrus-soft.ru/dev/export-accexistence
 *
 * @package Citrus\Tszh\Controller\v3
 */
class ExportAccexistenceService extends ServiceBase
{
	public function run()
	{
		global $APPLICATION;

		if (!\CModule::IncludeModule("citrus.tszh"))
		{
			throw new \Exception(GetMessage("TSZH_1C_ERROR_MODULE"));
		}

		$arTszh = Facade::getOrg();
		if (!is_array($arTszh) || IntVal($arTszh["ID"]) <= 0)
		{
			throw new \Exception(GetMessage("TSZH_1C_ERROR_ORG_NOT_FOUND"));
		}

		$filename = "/upload/tmp_export_" . date('Y-m-d_Hms') . ".xml";
		$absFilename = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . '/' . $filename);

		require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/classes/general/tszh_export.php");

		$type = 'accexistence';
		\COption::SetOptionString("citrus.tszh", "last_{$type}_export_time_" . $arTszh['ID'], time());

		if (!\CTszhExport::DoExportAccess($arTszh["INN"], $_SERVER['DOCUMENT_ROOT'] . $filename, Array("TSZH_ID" => $arTszh['ID'])))
		{
			$ex = $APPLICATION->GetException();
			if ($ex)
			{
				throw new \Exception(GetMessage("TSZH_1C_ACCEXISTENCE_EXPORT_ERROR") . $ex->GetString());
			}
			else
			{
				throw new \Exception(GetMessage("TSZH_1C_ACCEXISTENCE_EXPORT_ERROR"));
			}
		}

		echo file_get_contents($absFilename);
		unlink($absFilename);
		$this->facade->xmlOutput = true;
	}
}
