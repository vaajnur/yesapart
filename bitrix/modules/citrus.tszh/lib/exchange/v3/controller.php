<?

namespace Citrus\Tszh\Exchange\v3;

use Citrus\Tszh\Exchange\ControllerInterface;
use Citrus\Tszh\Exchange\Facade;
use Citrus\Tszh\Exchange\ServiceBase;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * ��������� ����� ������ 3 � ������
 *
 * @package Citrus\Tszh\Controller\v3
 */
class Controller implements ControllerInterface
{
	protected $facade = null;
	protected $objects = array();

	/**
	 * @param Facade $facade
	 */
	function __construct(Facade $facade)
	{
		$this->facade = $facade;
	}

	/**
	 * @param string $mode
	 * @param string $service
	 * @return string
	 */
	protected function getServiceClassname($mode, $service)
	{
		return '\\' . __NAMESPACE__ . '\\' . ucfirst($mode) . ucfirst($service) . 'Service';
	}

	/**
	 * @param string $mode
	 * @param string $service
	 * @return bool
	 */
	protected function hasObject($mode, $service)
	{
		return array_key_exists($mode, $this->objects) && array_key_exists($service, $this->objects[$mode]);
	}

	/**
	 * @param string $mode
	 * @param string $service
	 * @return bool
	 */
	public function hasService($mode, $service)
	{
		return class_exists($this->getServiceClassname($mode, $service));
	}

	/**
	 * @param string $mode
	 * @param string $service
	 * @return ServiceBase
	 */
	public function getService($mode, $service)
	{
		if (!$this->hasService($mode, $service))
		{
			throw new \InvalidArgumentException(Loc::getMessage("CITRUS_TSZH_1C_ERROR_SERVICE_ERROR", array(
				'#MODE#' => $mode,
				'#SERVICE#' => $service,
				'#VERSION#' => $this->facade->getVersion(),
			)));
		}

		if (!$this->hasObject($mode, $service))
		{
			$classname = $this->getServiceClassname($mode, $service);
			$this->objects[$mode][$service] = new $classname($this, $this->facade);
		}

		return $this->objects[$mode][$service];
	}

	/**
	 * @param string $mode
	 * @param string $service
	 */
	public function processRequest($mode, $service)
	{
		$this->getService($mode, $service)->run();
	}
}
