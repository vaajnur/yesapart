<?
namespace Citrus\Tszh\Exchange\v3;

use Citrus\Tszh\Exchange\ServiceBase;
use Citrus\Tszh\Exchange\Facade;

/**
 * ��������� �������� �������� ��������
 * ?mode=export&type=payments
 * http://wiki.citrus-soft.ru/dev/export-payments
 *
 * @package Citrus\Tszh\Controller\v3
 */
class ExportPaymentsService extends ServiceBase
{
	public function run()
	{
		if (!\CModule::IncludeModule("citrus.tszhpayment"))
			throw new \Exception(GetMessage("TSZH_1C_ERROR_PAYMENT_MODULE"));

		$arTszh = Facade::getOrg();
		$dateFrom = Facade::dateFrom();
		$dateTo = Facade::dateTo();

		if (!is_array($arTszh) || IntVal($arTszh["ID"]) <= 0)
			throw new \Exception(GetMessage("TSZH_1C_ERROR_ORG_NOT_FOUND"));

		$filename = "/upload/tszh_exchange/tmp_payments_export_" . date('Y-m-d_Hms') . ".xml";
		$absFilename = $_SERVER['DOCUMENT_ROOT'] . $filename;
		CheckDirPath($absFilename);

		$arFilter = Array(
			"TSZH_ID" => $arTszh['ID'],
			"PAYED" => "Y",
		);
		if ($dateFrom)
			$arFilter[">DATE_PAYED"] = $dateFrom;
		if ($dateTo)
			$arFilter["<=DATE_PAYED"] = $dateTo;

		$NS = Array(
			"filename" => $filename,
			"stepTime" => 0,
			"tszh" => $arTszh,
		);

		\CTszhPaymentsExport::ProcessExport($arFilter, 0, $NS);

		echo file_get_contents($absFilename);
		unlink($absFilename);
		$this->facade->xmlOutput = true;
	}
}

