<?
namespace Citrus\Tszh\Exchange\v3;

use Citrus\Tszh\Exchange\ServiceBase;
use Citrus\Tszh\Exchange\Facade;

/**
 * ��������� �������� ��������� � ���
 * ?mode=export&type=tickets
 * http://wiki.citrus-soft.ru/dev/export-tickets
 *
 * @package Citrus\Tszh\Controller\v3
 */
class ExportTicketsService extends ServiceBase
{
	public function run()
	{
		global $DB;

		if ($this->facade->processConfirmRequest())
			return;

		$filename = "/upload/tmp_export_" . date('Y-m-d_Hms') . ".xml";
		$absFilename = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . '/' . $filename);

		if (!\CModule::IncludeModule("citrus.tszhtickets"))
		{
			throw new \Exception(GetMessage("TSZH_1C_ERROR_TICKETS_MODULE"));
		}

		$arTszh = Facade::getOrg();


		if (!is_array($arTszh) || IntVal($arTszh["ID"]) <= 0)
		{
			throw new \Exception(GetMessage("TSZH_1C_ERROR_ORG_NOT_FOUND"));
		}

		$type = 'tickets';
		\COption::SetOptionString("citrus.tszh", "last_{$type}_export_time_" . $arTszh['ID'], time());
		$nLastExport = \COption::GetOptionString("citrus.tszh", "last_{$type}_export_time_committed_" . $arTszh['ID'], 0);
		$strDateFormat = $DB->DateFormatToPHP(\CSite::GetDateFormat("FULL"));
		$strLastExport = IntVal($nLastExport) > 0 ? date($strDateFormat, $nLastExport) : false;

		$fp = fopen($absFilename, "wb");
		if ($fp)
		{
			$obExport = new \CTszhTicketsExport();
			$NS = array(
				"STEP" => 0,
				//"URL_DATA_FILE" => $_REQUEST["URL_DATA_FILE"],
				"ONLY_OPENED" => false,
				"STATUSES" => false,
				"next_step" => array(),
				"SITE_ID" => $arTszh['SITE_ID'],
			);
			if ($strLastExport)
			{
				$NS['DATE_TIMESTAMP_1'] = $strLastExport;
			}

			if ($obExport->Init($fp, $NS["next_step"]))
			{
				$obExport->StartExport();
				$obExport->ExportDictionaries();
				$obExport->ExportTickets(0, 0, $NS);
				$obExport->EndExport();
			}
			else
			{
				throw new \Exception(GetMessage("TSZH_1C_TICKET_EXPORT_INIT_ERROR"));
			}
		}
		else
		{
			throw new \Exception(GetMessage("TSZH_1C_ERROR_FILE_OPEN"));
		}


		echo file_get_contents($absFilename);
		unlink($absFilename);
		$this->facade->xmlOutput = true;
	}
}