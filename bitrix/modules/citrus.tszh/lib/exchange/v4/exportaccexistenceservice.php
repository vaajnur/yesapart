<?
namespace Citrus\Tszh\Exchange\v4;

use Citrus\Tszh\Exchange\ServiceBase;
use \Bitrix\Main\Localization\Loc;
use Citrus\Tszh\Exchange\v4\Formats\Accexistence;

Loc::loadMessages(__FILE__);

/**
 * ��������� �������� �������� �� ���������
 * ?mode=export&type=accexistence
 * http://wiki.citrus-soft.ru/dev/export-accexistence
 *
 * @package Citrus\Tszh\Controller\v4
 */
class ExportAccexistenceService extends ServiceBase
{
	public function run()
	{
		$org = $this->facade->getOrg();

		$state = &$_SESSION["CITRUS.TSZH.ACCEXISTENCE.EXPORT"];
		$state = Array(
			"tszh" => $org,
		);

		$export = new Accexistence($_SESSION["CITRUS.TSZH.ACCEXISTENCE.EXPORT"]);
		$export->setOrg($org["ID"]);
		$export->run();

		$this->facade->xmlOutput = true;
	}
}

