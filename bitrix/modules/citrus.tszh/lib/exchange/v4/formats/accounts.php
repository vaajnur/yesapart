<?

namespace Citrus\Tszh\Exchange\v4\Formats;

use \Bitrix\Main\ArgumentOutOfRangeException;
use \Bitrix\Main\ArgumentTypeException;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Type\Date;
use \Citrus\Tszh\Exchange\ApplicationException;
use \Citrus\Tszh\Exchange\FormatException;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\ArgumentException;
use \Citrus\Tszh\HouseTable;
use \Citrus\Tszh\Types\ReceiptType;
use \Citrus\Tszh\Types\XmlNode;
use \Vdgb\Tszhepasport\EpasportTable;
use \Citrus\Tszh\DetailsPaymentsTable;

Loc::loadMessages(__FILE__);

/**
 * ��������� �������� ������ ������� ������ ������ 4
 *
 * @package Citrus\Tszh\Exchange\v4\Formats
 */
class Accounts
{
	const MAX_ERRORS = 40;

	protected $supportedVersion = 4;

	/** @var string NEW_EXTERNAL_ID_SEPARATOR ������������ ��� ���������� �������� (�����������) � ���� �������� ����� � �������� ORG->PersAcc->kod_ls_new */
	const NEW_EXTERNAL_ID_SEPARATOR = " ";

	/** @var array ������ ��� ���������� ����� ������ � ������ */
	protected $state;

	/** @var int ID ������� ���������� */
	protected $tszhID = false;

	/** @var string ID ����� ������� ���������� */
	protected $siteID = false;

	/** @var string ��� ������� */
	protected $sPeriod = false;

	/** @var int ID ������� */
	protected $periodID = false;

	protected $updateMode = false;
	protected $onlyDebt = false;
	protected $createUsers = false;
	protected $depersonalize = false;
	protected $arTszh = Array();
	protected $createTszh = false;
	protected $tszhName = false;
	protected $inn = false;

	/** @var array �������� ���� ���������, ������� ���� ������� �� ���� ���� (������ ������� 1�, ������ �16052) */
	var $deletedMeters = array();

	/** @var int ������ ����� �� XML */
	var $version = 1;

	/** @var bool ������������ �� ������� ����� �������� ��������� (� ������ ������� ������ ������ �������� ��������� ��������� ���������� ��� 0, ���� ���� ������� ���������� -- ������ �������� �������� ����� ������������� ��� ������) */
	var $strict = false;

	/** @var string ������� ������ �� XML (http://wiki.citrus-soft.ru/dev/import-accounts#��������-���������_��������-������) */
	var $filetype = "calculations";

	/** @var array ������� ���� �� ���������� X � ��������� */
	var $xFields = array();

	/**
	 * @var string ��� ������� � ������� XML-�����
	 */
	protected $tableName;

	/**
	 * @var bool �������� ��������� ������ ��� ����������� ������������ ��������� ���������� ������
	 */
	protected $useSessions;

	/**
	 * @var string � ������ ������������� ������ �������� SQL-������� �������������� � ������� ������
	 */
	protected $andWhereThisSession;

	/**
	 * @var string
	 */
	protected $sessionId;

	/**
	 * @var \CIBlockXMLFile
	 */
	protected $xmlFile;

	/**
	 * @var int ����� �� ����� ���������� ������ ���� � ��������
	 */
	protected $timeLimit;

	/**
	 * @var float Timestamp ������� ������ ���������
	 */
	protected $startTime = null;

	/**
	 * @param array $state ���������� ��� �������� �������� ���������, �������� ������ ����������� ����� ������ �������� (������)
	 * @param int $timeLimit ����� �� ����� ���������� ������ ���� � ��������
	 * @param bool $useSessions �������� ��������� ������ ��� ����������� ������������ ��������� ���������� ������
	 * @param string $tableName ��� ������� � ������� XML-�����
	 *
	 * @throws ArgumentException
	 */
	function __construct(&$state, $timeLimit = 0, $useSessions = false, $tableName = 'b_tszh_xml_storage')
	{
		if (strlen(trim($tableName)) == 0)
		{
			throw new ArgumentException("\$tableName must contain a non-empty string");
		}
		if (!isset($state) || !is_array($state))
		{
			throw new ArgumentException('Array variable must be passed in $state parameter');
		}

		$this->useSessions = $useSessions;
		$this->tableName = $tableName . ($useSessions ? '_m' : '');
		$this->state = &$state;
		$this->state["startTime"] = microtime(true);

		// $this->setStartTime();

		if ($timeLimit > 0)
		{
			$this->timeLimit = $timeLimit;
		}
		else
		{
			$this->timeLimit = 368 * 24 * 60 * 60; // ���� ��� ;)
		}

		$this->xmlFile = new \CIBlockXMLFile($this->tableName);
		if ($this->useSessions)
		{
			$this->sessionId = substr(session_id(), 0, 32);
			if (!$this->state["tablesCreated"])
			{
				// ������� ������ ������ � ������� ������
				// $this->clearSession();
			}
			$this->xmlFile->StartSession($this->sessionId);
			$this->state["tablesCreated"] = true;
		}
		elseif (!isset($this->state["tablesCreated"]))
		{
			$this->xmlFile->DropTemporaryTables();
			$this->xmlFile->CreateTemporaryTables();
			$this->state["tablesCreated"] = true;
		}
		$this->andWhereThisSession = $this->useSessions ? " and SESS_ID='" . $this->sessionId . "'" : '';

		if ($this->state["PERIOD_ID"] > 0)
		{
			$this->periodID = $this->state["PERIOD_ID"];
		}
		if (array_key_exists('FILE_PERIOD', $this->state) && strlen($this->state["FILE_PERIOD"]) > 0)
		{
			$this->sPeriod = $this->state["FILE_PERIOD"];
		}
		$this->updateMode = &$this->state["UPDATE_MODE"] == 'Y';
		$this->onlyDebt = &$this->state["ONLY_DEBT"];
		$this->createUsers = &$this->state["CREATE_USERS"];
		//$this->updateUsers = &$this->next_step["UPDATE_USERS"];
		$this->depersonalize = $this->state["DEPERSONALIZE"];

		$this->version = &$this->state["VERSION"];
		$this->strict = &$this->state["STRICT"];
		$this->filetype = &$this->state["FILETYPE"];

		$this->tszhID = &$this->state["TSZH"];
		$this->siteID = &$this->state["SITE_ID"];

		$this->createTszh = &$this->state["CREATE_TSZH"];
		$this->tszhName = &$this->state["TSZH_NAME"];
		$this->inn = &$this->state["INN"];

		if (intval($this->tszhID) > 0)
		{
			$this->arTszh = \CTszh::GetByID($this->tszhID);
		}

		$this->filetype = "calculations";
	}

	/**
	 * ����������� ������ �� XML � �����, �������� ����������
	 *
	 * @param string $value ������ � ������ �� XML
	 *
	 * @return float �������������� �����
	 * @throws FormatException
	 */
	protected function formatNumber($value, $key)
	{
		if (!is_numeric($value))
		{
			throw new FormatException('Incorrect float: ' . $key . '="' . $value . '"');
		}

		return floatval($value);
	}

	/**
	 * ������� � �������� ������ �� XML � ����
	 * �������� � ������������ �� �������
	 *
	 * @param resource $fp
	 *
	 * @return bool true ���� ���� ��� �������� ��������� � false ���� ���������� ���������� �������� � ������� ����� ��� ���
	 * @throws ArgumentTypeException
	 */
	public function readXml($fp)
	{
		if (!is_resource($fp))
		{
			throw new ArgumentTypeException('Argument $fp must contain valid resouce');
		}

		$done = $this->xmlFile->ReadXMLToDatabase($fp, $this->state, $this->timeLimit);
		// �������� ������� ����� ��������� �������� ������
		if ($done && !$this->useSessions)
		{
			$this->xmlFile->IndexTemporaryTables();
		}

		return $done;
	}

	/**
	 * @return \CIBlockXMLFile
	 */
	public function getXml()
	{
		return $this->xmlFile;
	}

	protected function resetX()
	{
		$this->xFields = array();
	}

	/**
	 * ��������� ���� � ������ ����� �� ��������� X
	 *
	 * @param string $field
	 */
	protected function addX($field)
	{
		$this->xFields[$field] = true;
	}

	/**
	 * ���������� ������ ����� (����� �������) �� ��������� X
	 *
	 * @return string
	 */
	protected function getX()
	{
		return implode(',', array_keys($this->xFields));
	}

	/**
	 * @param string $key �������� ����� (�������� ����)
	 * @param array|XmlNode $values ������ �������� ���������
	 * @param bool $number ���� true, �� ��������� ����� ��������� ��� ���� (������� ������ �������, �������� ������� �� �����)
	 * @param bool|string $field ��� ���� (� ���� ������ � API ������)
	 *
	 * @return bool|float|string
	 * @throws FormatException
	 */
	protected function getValue($key, $values, $number = false, $field = false)
	{
		if ($values instanceof XmlNode)
		{
			$value = isset($values[$key]) ? $values[$key] : false;
		}
		else
		{
			$value = array_key_exists($key, $values) ? $values[$key] : false;
		}
		if (strtolower($value) == 'x')
		{
			if ($field)
			{
				$this->addX($field);
			}

			return $number ? false : 'X';
		}

		if ($number && $value !== false)
		{
			return $this->formatNumber($value, $key);
		}
		else
		{
			return $value;
		}
	}

	/**
	 * ��������� ���������������� �������
	 *
	 * @param string $prefix ������� � ��������� ���� XML (��������, uf_ ��� puf_)
	 * @param XmlNode $arAttrs ������ ��������� �� XML
	 * @param array $arFields ������ �������������� ����� ��� ����������/����������
	 *
	 * @internal param string $entity �������� ���������������� �����
	 */
	protected function processUserFields($prefix, $arAttrs, &$arFields)
	{
		$prefix = ToLower($prefix);
		$prefixLen = strlen($prefix);
		$prefixLen = $prefixLen > 0 ? $prefixLen : false;

		foreach ($arAttrs as $attrName => $attrValue)
		{
			// ���������� ������ ���� � ��������� ���������
			if ($prefixLen)
			{
				if (strlen($attrName) > $prefixLen && ToLower(substr($attrName, 0, $prefixLen)) == $prefix)
				{
					$fieldName = strtoupper(str_replace($prefix, 'UF_', $attrName));
					$fieldValue = $attrValue;
					$arFields[strtoupper($fieldName)] = $fieldValue;
				}
			}
			else
			{
				$fieldName = strtoupper('UF_' . $attrName);
				$fieldValue = $attrValue;
				$arFields[strtoupper($fieldName)] = $fieldValue;
			}
		}
	}

	/**
	 * �������� ������ ����� �������������
	 * ������: pluralForm(5, '�����', '�����', '������');
	 *
	 * @param int $n
	 * @param string $f1
	 * @param string $f2
	 * @param string $f5
	 *
	 * @return string
	 */
	protected function pluralForm($n, $f1, $f2, $f5)
	{
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20)
		{
			return $f5;
		}
		if ($n1 > 1 && $n1 < 5)
		{
			return $f2;
		}
		if ($n1 == 1)
		{
			return $f1;
		}

		return $f5;
	}

	/**
	 * ���������� ����� ������ � ������� ������ ������
	 *
	 * @return bool|string
	 */
	public static function GetErrors()
	{
		if (count($_SESSION['TSZH_IMPORT_ERRORS']) > 0)
		{
			return implode('<br />', $_SESSION['TSZH_IMPORT_ERRORS']);
		}

		return false;
	}

	public static function ResetErrors()
	{
		$_SESSION['TSZH_IMPORT_ERRORS'] = Array();
	}

	/**
	 * @param string $strError
	 * @param bool|array|XmlNode $arXMLElement
	 */
	protected function ImportError($strError, $arXMLElement = false)
	{
		global $APPLICATION;

		/** @noinspection PhpAssignmentInConditionInspection */
		if ($ex = $APPLICATION->GetException())
		{
			$strError .= ': ' . $ex->GetString();
		}

		$strError = strip_tags($strError);

		if (count($_SESSION['TSZH_IMPORT_ERRORS']) < self::MAX_ERRORS)
		{
			if (defined("TSZH_IMPORT_DEBUG") && is_array($arXMLElement) && !empty($arXMLElement))
			{
				$strError .= ' (' . var_export($arXMLElement, 1) . ')';
			}
			$_SESSION['TSZH_IMPORT_ERRORS'][] = $strError;
		}
		elseif (count($_SESSION['TSZH_IMPORT_ERRORS']) <= self::MAX_ERRORS)
		{
			$_SESSION['TSZH_IMPORT_ERRORS'][] = GetMessage("TI_SHOWN_FIRST_ERRORS") . ' ' . self::MAX_ERRORS . ' ' . self::pluralForm(self::MAX_ERRORS, GetMessage("TI_SHOWN_ERRORS1"), GetMessage("TI_SHOWN_ERRORS3"), GetMessage("TI_SHOWN_ERRORS5"));
		}

		$APPLICATION->ResetException();
	}

	/**
	 * ������ ��� ������� � ��������� ������ �������
	 * @return bool
	 * @throws FormatException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 * @throws \Exception
	 */
	public function ProcessPeriod()
	{
		global $APPLICATION, $DB;

		$this->state["SERVICES"] = array();

		$xmlRootId = intval($this->useSessions ? $this->xmlFile->GetSessionRoot() : 1);
		$dbOrg = $this->getXml()->GetList(array(), array("NAME" => "org", "ID" => $xmlRootId));
		if ($dbOrg->SelectedRowsCount() <= 0)
		{
			throw new FormatException('CITRUS_TSZH_ORG_ELEMENT_NOT_FOUND');
		}

		$org = new XmlNode($dbOrg->Fetch());
		$this->state["XML_ACCOUNTS_PARENT"] = $org->getId();

		$this->version = $org['version'];
		$this->strict = true;

		if ($this->version != $this->supportedVersion)
		{
			throw new FormatException('CITRUS_TSZH_ERROR_WRONG_VERSION', array(
				'#GIVEN#' => $this->version,
				'#MUSTBE#' => $this->supportedVersion,
			));
		}

		if ($this->filetype !== $org['filetype'])
		{
			throw new FormatException('CITRUS_TSZH_ERROR_WRONG_FILETYPE', array(
				'#GIVEN#' => $org['filetype'],
				'#MUSTBE#' => $this->filetype,
			));
		}

		$dateTimestamp = $org->getDate('filedate', true);
		$this->sPeriod = date('Y-m-d', $dateTimestamp);
		$this->state['FILE_PERIOD'] = date('Y-m', $dateTimestamp);

		$this->onlyDebt = !isset($org['ready']) || $org['ready'] != 1;

		// inn
		if (!preg_match('/^[\d]+$/', $org['inn']))
		{
			throw new FormatException('CITRUS_TSZH_ERROR_WRONG_INN', array('#GIVEN#' => $org['inn']));
		}

		$arTszhFilter = array(
			'INN' => $org['inn'],
		);

		if (strlen($this->tszhID))
		{
			$arTszhFilter['ID'] = $this->tszhID;
		}

		$arTszh = \CTszh::GetList(
			array(),
			$arTszhFilter,
			false,
			array("nTopCount" => 1)
		)->Fetch();

		if (is_array($arTszh) && !empty($arTszh) && intval($arTszh["ID"]) > 0)
		{
			$this->arTszh = $arTszh;
			$this->tszhID = $arTszh["ID"];
			$this->siteID = $arTszh["SITE_ID"];
		}
		elseif ($this->createTszh === "Y")
		{
			if (strlen($this->siteID) <= 0)
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_CREATE_REQ_SITE_ID"));

				return false;
			}
			if (strlen($this->tszhName) <= 0)
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_CREATE_REQ_NAME"));

				return false;
			}

			$arTszhFields = array(
				"SITE_ID" => $this->siteID,
				"NAME" => htmlspecialcharsBack($this->tszhName),
				"INN" => $org['inn'],
			);

			$tszhID = \CTszh::Add($arTszhFields);
			if ($tszhID > 0)
			{
				$this->arTszh = \CTszh::GetByID($tszhID);
				$this->tszhID = $this->arTszh["ID"];
				$this->siteID = $this->arTszh["SITE_ID"];
			}
			else
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_CREATE"));

				return false;
			}
		}
		elseif (!\CModule::IncludeModule("vdgb.portaljkh"))
		{
			$this->siteID = "";
			$this->inn = htmlspecialcharsEx($org['inn']);
			$this->tszhName = "";
			if (isset($org["name"]))
			{
				$this->tszhName = htmlspecialcharsEx($org["name"]);
			}
			$this->createTszh = "Q";

			// ���������� ��� �������� �� 1� - ������ ���������� ������
			$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_WITH_INN_NOT_EXISTS", array("#INN#" => $org['inn'])));

			return true;
		}
		else
		{
			$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_WITH_INN_NOT_EXISTS", array("#INN#" => $org['inn'])));

			return false;
		}

		$attrsMap = array(
			'inn' => 'INN',
			'kpp' => 'KPP',
			'rs' => 'RSCH',
			'bank' => 'BANK',
			'ks' => 'KSCH',
			'bik' => 'BIK',
			'additional_info_main' => 'ADDITIONAL_INFO_MAIN',
			'additional_info_overhaul' => "ADDITIONAL_INFO_OVERHAUL",
			'annotation_main' => "ANNOTATION_MAIN",
			'annotation_overhaul' => "ANNOTATION_OVERHAUL",
		);

		/*
		 * https://cp.vdgb-soft.ru/company/personal/user/203/tasks/task/view/15882/
		 * 4�� �������������� ��������, ������� ���������� � ���� org.
		 *
		 * additional_info_main - �������������� �������� ����������� ��� �������� ���������.
		 * additional_info_overhaul - �������������� �������� ����������� ��� ��������� �� ���������.
		 * annotation_main - ���������� ��� �������� ���������.
		 * annotation_overhaul - ���������� ��� ��������� �� ���������.
		 *
		 * ��� ����, ��� ����, ����� �������� ��������� ������� ��� ��������, ������,
		 * ������������ � ������ ���������, ���������� � ������� Base64 � �������� ���������� ANSI(��1251).
		 * ��� ����������� � Base64
		 * '==' �� ����� ������ ������ ���������,
		 * '/' �������� �� '_',
		 * '+' �������� �� '-'.
		 */

		$attrsBase64 = array(
			'additional_info_main',
			'additional_info_overhaul',
			'annotation_main',
			'annotation_overhaul',
		);
		foreach ($org as $key => $value)
		{
			if (array_key_exists($key, $attrsMap))
			{
				if (in_array($key, $attrsBase64))
				{
					$value = base64_decode(str_replace(array('-', '_'), array('+', '/'), $value));
					if (SITE_CHARSET == "UTF-8")
					{
						$value = $APPLICATION->ConvertCharset($value, 'cp1251', SITE_CHARSET);
					}
				}
				$arUpdateOrg[$attrsMap[$key]] = $value;
			}
		}
		if (!empty($arUpdateOrg))
		{
			if (!\CTszh::Update($this->tszhID, $arUpdateOrg))
			{
				$ex = $APPLICATION->GetException();
				if ($ex)
				{
					throw new \Exception($ex->GetString());
				}
				else
				{
					throw new \Exception("CTszh::Update() failed");
				}
			}
		}

		/*
		 * TODO
		 * ������� ���, ����� $this->updateMode �� ��������� � �������� ������.
		 */

		if ($this->updateMode)
		{
			$rsPeriod = \CTszhPeriod::GetList(Array("DATE" => "DESC"), Array(
				"TSZH_ID" => $this->tszhID,
				"MONTH" => substr($this->sPeriod, 0, 7),
			));
			/** @noinspection PhpAssignmentInConditionInspection */
			if ($arPeriod = $rsPeriod->Fetch())
			{
				$this->periodID = $arPeriod['ID'];
				$this->state["PERIOD_ID"] = $this->periodID;
			}
			else
			{
				$this->updateMode = false;
			}
		}

		$arPeriodFields = Array(
			"DATE" => $this->sPeriod,
			"TSZH_ID" => $this->tszhID,
			"ACTIVE" => "Y",
			"ONLY_DEBT" => $this->onlyDebt ? "Y" : "N",
		);
		if ($this->updateMode)
		{
			if (!\CTszhPeriod::Update($this->periodID, $arPeriodFields))
			{
				throw new ApplicationException("CITRUS_TSZH_PERIOD_ADD_UPDATE_ERROR");
			}
		}
		else
		{
			$this->periodID = \CTszhPeriod::Add($arPeriodFields);
			if ($this->periodID <= 0)
			{
				throw new ApplicationException("CITRUS_TSZH_PERIOD_ADD_UPDATE_ERROR");
			}
			$this->state["PERIOD_ID"] = $this->periodID;
		}

		if ($this->periodID <= 0)
		{
			$APPLICATION->ThrowException(GetMessage("TI_ERROR_PERIOD_ADD_ERROR"));

			return false;
		}

		$this->state["ADDTITIONAL_PERIODS_EXISTS"] = $this->addititionalPeriodsExists();

		if ($this->state["XML_ACCOUNTS_PARENT"])
		{
			$parent_main_period = $DB->Query("select ID from " . $this->tableName . " where (NAME='accs') AND (PARENT_ID='" . $org->getId() . "')" . $this->andWhereThisSession)->Fetch();
			$ar = $DB->Query("select count(*) C from " . $this->tableName . " where (NAME='acc') AND (PARENT_ID='" . $parent_main_period['ID'] . "')" . $this->andWhereThisSession)->Fetch();
			$this->state["DONE"]["ALL"] = $ar["C"];
		}
		else
		{
			throw new \LogicException("Unexpected next_step[\"XML_ACCOUNTS_PARENT\"]!");
		}

		$this->__processContractors();

		$this->ResetErrors();

		return true;
	}

	/**
	 * ��������� ������ �����������
	 * org -> contractors
	 *
	 * @return bool|void
	 * @throws ApplicationException
	 * @throws ArgumentOutOfRangeException
	 * @throws FormatException
	 */
	protected function __processContractors()
	{
		if (!isset($this->state['CONTRACTORS']))
		{
			if (!$this->state["XML_ACCOUNTS_PARENT"])
			{
				throw new \LogicException("XML_ACCOUNTS_PARENT is not set");
			}

			$contractors = $this->getXml()->GetList(
				array(),
				array("NAME" => "contractors", "PARENT_ID" => $this->state["XML_ACCOUNTS_PARENT"])
			)->Fetch();
			if (!is_array($contractors))
			{
				return;
			}
			$this->state["CONTRACTORS"] = array(
				"XML_PARENT" => $contractors["ID"],
				"LAST_ID" => 0,
				"MAP" => array(),
			);
		}

		if (!$this->state["CONTRACTORS"]["XML_PARENT"])
		{
			return true;
			//			throw new \LogicException('$this->state["CONTRACTORS"]["XML_PARENT"] is not set');
		}

		$contractors = $this->GetAllChildrenNested($this->state["CONTRACTORS"]["XML_PARENT"]);
		if (!isset($contractors->contractor))
		{
			return;
		}

		/** @var XmlNode $contractor */
		foreach ($contractors->contractor as $contractor)
		{
			$executor = "N";
			if (isset($contractor['executor']))
			{
				switch ($contractor['executor'])
				{
					case 1:
						$executor = "Z";
						break;
					case 2:
						$executor = "Y";
						break;
					default:
						$this->ImportError(GetMessage("TI_WRONG_CONTRACTOR_EXECUTOR"), $contractor);

						return;
						break;
				}
			}
			$arContractorFields = Array(
				"TSZH_ID" => $this->tszhID,
				"XML_ID" => $contractor['id'],
				"EXECUTOR" => $executor,
				"NAME" => $contractor['name'],
				"SERVICES" => $contractor->get('services'),

				"INN" => $contractor->get('inn'),
				"KPP" => $contractor->get('kpp'),
				"BANK" => $contractor->get('bank'),
				"BIK" => $contractor->get('bik'),
				"KSCH" => $contractor->get('ks'),
				"RSCH" => $contractor->get('rs'),

				"ADDRESS" => $contractor->get('address'),
				"PHONE" => $contractor->get('phone'),

				'BILLING' => (string)$contractor,
			);
			$this->processUserFields('uf_', $contractor, $arContractorFields);

			$existingContractor = \CTszhContractor::GetList(Array(), Array(
				"TSZH_ID" => $this->tszhID,
				"XML_ID" => $arContractorFields["XML_ID"],
			))->Fetch();
			if (is_array($existingContractor))
			{
				$contractorId = $existingContractor['ID'];
				if (!\CTszhContractor::Update($contractorId, $arContractorFields))
				{
					throw new ApplicationException('CITRUS_TSZH_ERROR_UPDATING_CONTRACTOR');
				}
			}
			else
			{
				$contractorId = \CTszhContractor::Add($arContractorFields);
				if ($contractorId <= 0)
				{
					throw new ApplicationException('CITRUS_TSZH_ERROR_UPDATING_CONTRACTOR');
				}
			}
			$this->state["CONTRACTORS"]["MAP"][$contractor['id']] = $contractorId;
		}
	}

	/**
	 * ��������� �������� ��� ������
	 * ����������� �� ������ �������� ������ ���������
	 *
	 * @param XmlNode $service
	 *
	 * @return string
	 * @throws FormatException
	 */
	protected static function __makeServiceExternalID($service)
	{
		$ar = Array(
			$service->get('units'),
			$service['name'],
			$service->getFloat('norm', false, true),
			$service->get('tarif', false),
		);
		$str = implode('|', $ar);
		$md5 = md5($str);

		return $md5;
	}

	public function ImportServices()
	{
		global $DB;
		$arServices = array();

		$rsItems = $DB->Query("select * from " . $this->tableName . " where ((NAME='service' or NAME='service_debt') or NAME='service_insurance')" . $this->andWhereThisSession);
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($xmlTag = $rsItems->Fetch())
		{
			try
			{
				$xmlTag = new XmlNode($xmlTag);
			}
			catch (\Exception $e)
			{
				$this->ImportError("Error initialize xmlTag");
			}
			$serviceFields = Array(
				'ACTIVE' => 'Y',
				'UNITS' => isset($xmlTag['units']) ? $this->getValue("units", $xmlTag) : false,
				'TSZH_ID' => $this->tszhID,
				'XML_ID' => isset($xmlTag['id']) ? $xmlTag['id'] : false,
				'NAME' => $xmlTag['name'],
				'NORM' => isset($xmlTag['norm']) ? $this->getValue("norm", $xmlTag, true) : false,
			);

			try
			{
				$serviceFields["XML_ID"] = self::__makeServiceExternalID($xmlTag);
			}
			catch (FormatException $e)
			{
				$this->ImportError("Error creating service external ID");
			}
			$arServices[$serviceFields["XML_ID"]] = $serviceFields;
		}

		foreach ($arServices as $XML_ID => $serviceFields)
		{
			$existingService = \CTszhService::GetList(Array(), Array(
				"XML_ID" => $XML_ID,
				"TSZH_ID" => $this->tszhID,
			))->Fetch();

			if (is_array($existingService))
			{
				$serviceId = $existingService['ID'];
				try
				{
					\CTszhService::Update($serviceId, $serviceFields);
				}
				catch (ArgumentOutOfRangeException $e)
				{
					$this->ImportError("Error updating service");
				}
			}
			else
			{
				$serviceId = \CTszhService::Add($serviceFields);
			}

			if ($serviceId)
			{
				$this->state["SERVICES"][$XML_ID] = $serviceId;
			}
			else
			{
				/*
				 *  TODO ������� ������� ��� ��������� �������, ����� ������ ������� ������ �� ���������� ���������� �������.
				 *
				 */
				//$rsItems = $DB->Query("TRUNCATE TABLE ".$this->tableName);
				$this->ImportError("Error adding service " . $serviceFields["NAME"], $xmlTag);

				return;
			}
		}

	}

	/**
	 * ��������� ������ �������� XML-���������
	 *
	 * @param array|int $parent
	 * @param string|bool $tagName
	 *
	 * @return XmlNode
	 * @throws \Exception
	 */
	protected function GetAllChildrenNested($parent, $tagName = false)
	{
		global $DB;

		$result = $parent;

		//So we get not parent itself
		if (!is_array($result))
		{
			$rs = $DB->Query("select ID, NAME, LEFT_MARGIN, RIGHT_MARGIN, ATTRIBUTES from " . $this->tableName . " where (ID = " . intval($result) . ')' . $this->andWhereThisSession);
			$result = $rs->Fetch();
			if (!$result)
			{
				throw new \InvalidArgumentException('Incorrect $parent');
			}

			$result["~attr"] = unserialize($result['ATTRIBUTES']);
			$result["~children"] = Array();
		}

		//Array of the references to the $arParent array members with xml_id as index.
		$arIndex = array($result["ID"] => &$result);
		$rs = $DB->Query("select * from " . $this->tableName . " where (LEFT_MARGIN between " . ($result["LEFT_MARGIN"] + 1) . " and " . ($result["RIGHT_MARGIN"] - 1) . ($tagName ? " AND NAME='" . $DB->ForSql($tagName) . "'" : '') . ')' . $this->andWhereThisSession . " order by ID");
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($ar = $rs->Fetch())
		{
			$ar["~attr"] = unserialize($ar['ATTRIBUTES']);
			unset($ar["ATTRIBUTES"]);

			if (isset($ar["VALUE_CLOB"]))
			{
				$ar["VALUE"] = $ar["VALUE_CLOB"];
			}

			$parent_id = $ar["PARENT_ID"];
			if (!is_array($arIndex[$parent_id]))
			{
				$arIndex[$parent_id] = array();
			}

			$arIndex[$ar['ID']] = $ar;
			$arIndex[$ar["PARENT_ID"]]['~children'][$ar['NAME']][] = &$arIndex[$ar['ID']];
		}

		return new XmlNode($result);
	}

	/**
	 * ��������� ������ �����
	 */
	public function importHouses()
	{
		global $DB;

		if (!isset($this->state['HOUSES']))
		{
			if (!$this->state["XML_ACCOUNTS_PARENT"])
			{
				throw new \LogicException("XML_ACCOUNTS_PARENT is not set");
			}

			$houses = $this->getXml()->GetList(
				array(),
				array("NAME" => "houses", "PARENT_ID" => $this->state["XML_ACCOUNTS_PARENT"])
			)->Fetch();
			if (!is_array($houses))
			{
				throw new \Exception("houses element missing");
			}
			$this->state["HOUSES"] = array(
				"XML_PARENT" => $houses["ID"],
				"LAST_ID" => 0,
				"COUNTS" => 0,
			);
		}

		if (!$this->state["HOUSES"]["XML_PARENT"])
		{
			return true;
			//			throw new \LogicException('$this->state["HOUSES"]["XML_PARENT"] is not set');
		}

		$dbHouses = $DB->Query("select ID, NAME, LEFT_MARGIN, RIGHT_MARGIN, ATTRIBUTES from " . $this->tableName . " where (NAME='house' and PARENT_ID = " . $this->state['HOUSES']["XML_PARENT"] . ')' . $this->andWhereThisSession . " AND ID > " . intval($this->state["HOUSES"]["LAST_ID"]) . " order by ID");
		$countProcessed = 0;
		/** @noinspection PhpAssignmentInConditionInspection */
		while (!$this->timeIsUp() && $house = $dbHouses->Fetch())
		{
			$xmlElement = $this->GetAllChildrenNested($house);
			$this->importHouse($xmlElement);

			$this->state["HOUSES"]["LAST_ID"] = $house["ID"];
			$this->state["HOUSES"]["COUNTS"]++;
			$countProcessed++;
		}

		return $dbHouses->SelectedRowsCount() == $countProcessed;
	}

	/**
	 * ������������ ������� � �����
	 *
	 * @param XmlNode $house ������ XML-��������
	 *
	 * @throws ArgumentException
	 * @throws \Exception
	 */
	protected function importHouse($house)
	{
		if (!isset($house->address) || !is_array($house->address))
		{
			throw new FormatException('CITRUS_TSZH_MISSING_HOUSE_ADDRESS');
		}
		/** @var XmlNode $address */
		$address = array_shift($house->address);

		$addressFields = array(
			"TSZH_ID" => $this->tszhID,
			"REGION" => $address->get('region'),
			"DISTRICT" => $address->get('district'),
			"CITY" => $address->get('city'),
			"SETTLEMENT" => $address->get('settlement'),
			"STREET" => $address->get('street'),
			"HOUSE" => $address->get('house'),
			"ZIP" => $address->get('index'),
			"ADDRESS_USER_ENTERED" => $address->get('address_view'),
			"ADDRESS_VIEW_FULL" => $address->get('address_view_full'),
		);

		// ��������� ��������� ��� �������� ���������
		if (isset($house->fund))
		{
			$fund = array_shift($house->fund);
			$fundFields = array(
				"BANK" => $fund->get('bank'),
				"BIK" => $fund->get('bik'),
				"RS" => $fund->get('rs'),
				"KS" => $fund->get('ks'),
			);
		}
		else
		{
			$fundFields = array(
				"BANK" => "",
				"BIK" => "",
				"RS" => "",
				"KS" => "",
			);
		}

		// ��������� ��������� ��� ��������� �� ���. �������
		if (isset($house->overhaul_fund))
		{
			$overhaul_fund = array_shift($house->overhaul_fund);
			$overhaul_fundFields = array(
				"OVERHAUL_BANK" => $overhaul_fund->get('bank'),
				"OVERHAUL_BIK" => $overhaul_fund->get('bik'),
				"OVERHAUL_RS" => $overhaul_fund->get('rs'),
				"OVERHAUL_KS" => $overhaul_fund->get('ks'),
			);
		}
		else
		{
			$overhaul_fundFields = array(
				"OVERHAUL_BANK" => "",
				"OVERHAUL_BIK" => "",
				"OVERHAUL_RS" => "",
				"OVERHAUL_KS" => "",
			);
		}

		$houseMandatoryFields = array(
			"EXTERNAL_ID" => $house['id'],
			"AREA" => $house->get('area'),
			"ROOMS_AREA" => $house->get('flats_area'),
			"COMMON_PLACES_AREA" => $house->get('common_area'),
			"TYPE" => $house->get('type'),
			"YEAR_OF_BUILT" => $house->get('year_of_built'),
			"FLOORS" => $house->get('floors'),
			"PORCHES" => $house->get('porches'),
		);

		$houseOptionalFileds = array();

		if ($date_of_commissioning = $house->getDate('date_of_commissioning'))
		{
			$houseOptionalFileds['DATE_OF_COMMISSIONING'] = \Bitrix\Main\Type\DateTime::createFromTimestamp($date_of_commissioning);
		}

		if ($date_of_maintenance = $house->getDate('date_of_maintenance'))
		{
			$houseOptionalFileds["DATE_OF_MAINTENANCE"] = \Bitrix\Main\Type\DateTime::createFromTimestamp($date_of_maintenance);
		}

		$houseFields = array_merge($addressFields, $overhaul_fundFields, $fundFields, $houseMandatoryFields, $houseOptionalFileds);


		/**
		 * ��� ������������� �� ������ ��������� ������ (����� �� ���� ������� ����� �����), ������� ������������ �� ����� ������ ��� ������������� ���������� ���� ���� �� XML
		 */
		$houseFilter = array();
		foreach ($addressFields as $key => $value)
		{
			$houseFilter['=' . $key] = $value;
		}

		$existingHouse = HouseTable::getList(array(
			"filter" => array(
				'LOGIC' => 'OR',
				array(
					'=EXTERNAL_ID' => $house->get('id'),
					"=TSZH_ID" => $this->tszhID,
				),
				$houseFilter,
			),
			"select" => array("ID"),
		))->fetch();

		if (is_array($existingHouse))
		{
			$result = HouseTable::update($existingHouse["ID"], $houseFields);
		}
		else
		{
			$result = HouseTable::add($houseFields);
		}
		if (!$result->isSuccess())
		{
			throw new FormatException("CITRUS_TSZH_HOUSE_ERROR", array('#ERROR#' => implode("\n", $result->getErrorMessages())));
		}

		$existingHouse = HouseTable::getList(array(
			"filter" => $houseFilter,
			"select" => array("ID"),
		))->fetch();
		/*
		 * E��� ���������� ������ ����������� ���������, ���������� � ���� ������ �������������� ������ �� �����, ������� ����������� ��������
		 * ���� ������ ��� ������ ���� ��� ����, �� ��������� ��
		 */
		if (Loader::IncludeModule("vdgb.tszhepasport"))
		{
			$html = "";
			if (isset($house->contents))
			{
				$content_array = $house->contents[0]->content;
				while (!empty($content_array))
				{
					$shifted_html = array_shift($content_array);
					$attributes = $shifted_html->GetAttributes();
					$html .= $attributes['name'] . " || " . $attributes['type'] . " || " . html_entity_decode(trim((string)$shifted_html)) . " || ";
				}
				$add_info_Fields = array("EP_HTML" => $html);
			}

			if (isset($house->add_info))
			{
				$add_info = array_shift($house->add_info);
				$add_info_Fields = array(
					"EP_SERIES" => $add_info->get('series'),
					"EP_WEAR" => $add_info->get('wear'),
					"EP_LIVING_ROOMS" => $add_info->get('living_rooms'),
					"EP_NONLIVING_ROOMS" => $add_info->get('nonliving_rooms'),
					"EP_ACCOUNTS" => $add_info->get('accounts'),
                    "EP_PEOPLE" => $add_info->get('people'),
					"EP_PEOPLE_ACCOUNTS" => $add_info->get('people_accounts'),
					"EP_COMPANY_ACCOUNTS" => $add_info->get('company_accounts'),
					//"EP_YEAR_OF_COMMISSIONING" => $add_info->get('year_of_commissioning'), //������ ������ �������
					"EP_REG_PEOPLE" => $add_info->get('reg_people'),
					"EP_HTML" => $html,
				);
			}

			$add_info_Fields = array_merge($add_info_Fields, array(
				"EP_HOUSE_ID" => $existingHouse["ID"],
			));
			$epasportFilter = array();
			$epasportFilter['=EP_HOUSE_ID'] = $existingHouse["ID"];

			$existing_epasport_House = EpasportTable::getList(array(
				"filter" => $epasportFilter,
				"select" => array("EP_ID"),
			))->fetch();
			/*���� ���� �����-�� �������������� ���������� � ����� ������ ������������, ���������/��������� ������ � �� */
			if (isset($add_info_Fields))
			{
				if (is_array($existing_epasport_House))
				{
					$add_info_Fields["DATE_UPDATE"] = \Bitrix\Main\Type\DateTime::createFromTimestamp(time());
					$result = EpasportTable::update($existing_epasport_House["EP_ID"], $add_info_Fields);
				}
				else
				{
					$add_info_Fields["DATE_CREATE"] = $add_info_Fields["DATE_UPDATE"] = \Bitrix\Main\Type\DateTime::createFromTimestamp(time());
					$result = EpasportTable::add($add_info_Fields);
				}

				if (!$result->isSuccess())
				{
					$this->state['HOUSES']['WARNINGS'][] = $result->getErrorMessages();
					$_SESSION['BX_CITRUS_TSZH_IMPORT_RESPONSE']['details']['houses_additional'] = false;
				}
				else
				{
					$this->state['HOUSES']['HOUSES_ADDITIONAL'] = true;
					$_SESSION['BX_CITRUS_TSZH_IMPORT_RESPONSE']['details']['houses_additional'] = true;
				}
			}
		}

		$this->state["HOUSES"]["MAP"][$house["id"]] = $result->getId();
		if (isset($house->meter))
		{
			$counter = array();
			$this->__processMeters(false, $house->meter, $counter);
		}
	}

	/**
	 * ��������� ������ ������� ������
	 *
	 * @return array
	 * @throws ArgumentOutOfRangeException
	 * @throws FormatException
	 * @throws \Exception
	 */
	public function ImportAccounts()
	{
		global $DB;

		if (!isset($this->state['ACCOUNTS']))
		{
			if (!$this->state["XML_ACCOUNTS_PARENT"])
			{
				throw new \LogicException("XML_ACCOUNTS_PARENT is not set");
			}

			$accs = $this->getXml()->GetList(
				array(),
				array("NAME" => "accs", "PARENT_ID" => $this->state["XML_ACCOUNTS_PARENT"])
			)->Fetch();
			$this->state["ACCOUNTS"] = array(
				"XML_PARENT" => $accs["ID"],
				"LAST_ID" => 0,
				"COUNTS" => 0,
			);
		}

		if (!$this->state["ACCOUNTS"]["XML_PARENT"])
		{
			return true;
			// throw new \LogicException('$this->state["ACCOUNTS"]["XML_PARENT"] is not set');
		}

		$counter = array();
		$dbAccs = $this->getXml()->GetList(
			array(
				"ID" => "ASC",
			),
			array(
				"NAME" => "acc",
				"PARENT_ID" => $this->state["ACCOUNTS"]["XML_PARENT"],
				">ID" => $this->state["ACCOUNTS"]["LAST_ID"],
			),
			array(
				"ID",
				"NAME",
				"LEFT_MARGIN",
				"RIGHT_MARGIN",
				"ATTRIBUTES",
			)
		);
		/** @noinspection PhpAssignmentInConditionInspection */
		while (!$this->timeIsUp() && $acc = $dbAccs->Fetch())
		{
			$accNode = $this->GetAllChildrenNested($acc);
			$ID = $this->ImportAccount($accNode, $counter);

			if ($ID)
			{
				if ($this->useSessions)
				{
					$DB->Query("INSERT INTO " . $this->tableName . " (SESS_ID, PARENT_ID, LEFT_MARGIN) values ('" . $DB->ForSql($this->sessionId) . "', 0, " . $ID . ")");
				}
				else
				{
					$DB->Query("INSERT INTO " . $this->tableName . " (PARENT_ID, LEFT_MARGIN) values (0, " . $ID . ")");
				}
			}

			$this->state["ACCOUNTS"]["COUNTS"]++;
			$this->state["ACCOUNTS"]["LAST_ID"] = $acc["ID"];
		}

		return $counter;
	}

	public function ImportAdditionalPeriods()
	{
		if (!isset($this->state['ADDTITIONAL_PERIODS']))
		{
			if (!$this->state["XML_ACCOUNTS_PARENT"])
			{
				throw new \LogicException("XML_ACCOUNTS_PARENT is not set");
			}

			$dbAddititionlaPeriods = $this->getXml()->GetList(
				array(),
				array("NAME" => "additional_period", "PARENT_ID" => $this->state["XML_ACCOUNTS_PARENT"])
			);

			while ($arAddititionalPeriod = $dbAddititionlaPeriods->Fetch())
			{
				$accs = $this->getXml()->GetList(
					array(),
					array('NAME' => 'accs', 'PARENT_ID' => $arAddititionalPeriod['ID']),
					array('ID')
				)->Fetch();

				$attr = unserialize($arAddititionalPeriod['ATTRIBUTES']);

				$this->state['ADDITITIONAL_PERIODS'][] = array(
					'ACCS_XML_PARENT' => $accs['ID'],
					'PERIOD_DATE' => date("Y-m-d", strtotime($attr['period_date'])),
					'LAST_ID' => 0,
					'COUNTS' => 0,
					'TOTAL_COUNT' => 0,
				);
			}
		}

		do
		{
			if (!isset($this->state['CURRENT_ADDITITIONAL_PERIOD']))
			{
				$period_data = array_shift($this->state['ADDITITIONAL_PERIODS']);
				$period_date = new \DateTime($period_data['PERIOD_DATE']);
				$arAddititionalPeriodFields = array(
					'TSZH_ID' => $this->tszhID,
					'DATE' => $period_date->format('Y-m-d'),
					'MONTH' => $period_date->format('Y-m'),
					'ACTIVE' => 'Y',
					'ONLY_DEBT' => 'Y',
				);

				//�������� �������� �������, ���� �� ����� ���� ����� ������, �� ������ ��������� ���
				$rsAddPeriods = \CTszhPeriod::GetList(array(),
					array(
						'TSZH_ID' => $this->tszhID,
						'MONTH' => $period_date->format('Y-m'),
						//'ONLY_DEBT' => 'Y',
					)
				);

				$curAddPeriod = null;
				while ($arAddPeriod = $rsAddPeriods->Fetch())
				{
					if (is_null($curAddPeriod) || ($arAddPeriod["ONLY_DEBT"] == "Y"))
					{
						$curAddPeriod = $arAddPeriod;
					}
				}

				/*
				 * TODO
				 * ������� � ����������� � $this->updateMode ��� ��� �������� �������.
				 * �� ������ ������ ��� ����������, �.�. $this->updateMode ����� ������� � ������ ��-�� �����, ��� ���
				 * ������� ��� ���������� � ���������� ��������� �����.
				 */

				if ((intval($curAddPeriod["ID"]) > 0) && (intval($this->periodID) != intval($curAddPeriod["ID"])))
				{
					$period_data['SITE_PERIOD_ID'] = $curAddPeriod["ID"];
					\CTszhPeriod::Update($curAddPeriod["ID"], $arAddititionalPeriodFields);
				}
				else
				{
					$period_data['SITE_PERIOD_ID'] = \CTszhPeriod::Add($arAddititionalPeriodFields);
				}

				$this->state['CURRENT_ADDITITIONAL_PERIOD'] = &$period_data;
			}
			else
			{
				$period_data = $this->state['CURRENT_ADDITITIONAL_PERIOD'];
			}

			$dbAccs = $this->getXml()->GetList(
				array(
					"ID" => "ASC",
				),
				array(
					"NAME" => "acc",
					"PARENT_ID" => $period_data["ACCS_XML_PARENT"],
					">ID" => $period_data["LAST_ID"],
				),
				array(
					"ID",
					"NAME",
					"ATTRIBUTES",
					"LEFT_MARGIN",
					"RIGHT_MARGIN",
				)
			);

			if ($period_data["LAST_ID"] == 0)
			{
				$period_data['TOTAL_COUNT'] = $dbAccs->SelectedRowsCount();
			}

			$this->periodID = $period_data['SITE_PERIOD_ID'];

			while (!$this->timeIsUp() && $acc = $dbAccs->Fetch())
			{
				$accNode = $this->GetAllChildrenNested($acc);
				$ID = $this->ImportAccount($accNode, $counter, false);

				$this->state['CURRENT_ADDITITIONAL_PERIOD']['COUNTS']++;
				$this->state['CURRENT_ADDITITIONAL_PERIOD']['LAST_ID'] = $ID;
			}

			if ($period_data['COUNTS'] == $period_data['TOTAL_COUNT'])
			{
				unset($this->state['CURRENT_ADDITITIONAL_PERIOD']);
			}
		}
		while (is_array($this->state['ADDITITIONAL_PERIODS'])
		       && count($this->state['ADDITITIONAL_PERIODS']) > 0
		       && !$this->timeIsUp());

		if ($this->timeIsUp())
		{
			return false;
		}

		return true;
	}

	/**
	 * ��������� ������ ������� �������� ���� �/� �� �����
	 * (������ �12276)
	 *
	 * @param int $accountId ID ��������� �������� �����
	 * @param string $newExternalId ����� ������� ���
	 *
	 * @throws ArgumentOutOfRangeException
	 * @throws ArgumentTypeException
	 */
	protected function mergeAccount($accountId, $newExternalId)
	{
		// ����� ������� ��� ������� �� ���� ������: �������� � �� �� 1�, ����������� ��������
		// ����� ����� � ���������� ������ �������� �������� ���� (� ��������� � ���)
		$parts = explode(self::NEW_EXTERNAL_ID_SEPARATOR, $newExternalId, 2);
		\CTszhAccount::mergeAccounts($accountId, false, array(
			$newExternalId, // �� ������ ������ ����� ������� � ������������
			$parts[1], // ��� ��������
			$parts[0] . $parts[1], // � ���������
		));
	}

	/**
	 * @param int $receiptType
	 * @param int $accountId
	 * @param XmlNode $receipt
	 * @param array $counter
	 *
	 * @throws FormatException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 * @throws \Exception
	 */
	protected function processReceipt($receiptType, $accountId, $receipt, &$counter)
	{
		$acc = $receipt->getParent();
		$receiptFields = Array(
			'TYPE' => $receiptType,
			'ACCOUNT_ID' => $accountId,
			'PERIOD_ID' => $this->periodID,
			'IPD' => $receipt->get('ipd'),
			'DEBT_BEG' => $receipt->getFloat('debt_beg', false),
			'DEBT_END' => $receipt->getFloat('debt_end', false),
			'SUMM_TO_PAY' => $receipt->getFloat('sum_to_pay', false),
			'SUM_PAYED' => $receipt->getFloat('sum_payed', false),
			'DEBT_PREV' => $receipt->getFloat('debt_prev'),
			'PREPAYMENT' => $receipt->getFloat('prepayment'),
			'LAST_PAYMENT' => $receipt->getDate('last_payment_date') !== false ? ConvertTimeStamp($receipt->getDate('last_payment_date'), "SHORT") : "",
			'BARCODE' => '',
			'BARCODES' => isset($receipt->barcode) ? $this->__processBarcodes($receipt->barcode) : array(),
			'BARCODES_INSURANCE' => isset($receipt->barcode_insurance) ? $this->__processBarcodesInsurance($receipt->barcode_insurance) : array(),
			'CREDIT_PAYED' => $receipt->getFloat('credit_payed'),
			'SUMM_PAYED_INSURANCE' => $receipt->getFloat('sum_payed_insurance'),
			'SUM_TO_PAY_WITH_INSURANCE' => $receipt->getFloat('sum_to_pay_with_insurance'),
		);

		// ���������������� ���� ���������
		$this->processUserFields('uf_', $receipt, $receiptFields);

		if ($this->updateMode)
		{
			$rsOldAccountPeriod = \CTszhAccountPeriod::GetList(array(), array(
				"ACCOUNT_ID" => $accountId,
				"PERIOD_ID" => $this->periodID,
				"TYPE" => $receiptType,
			), false, false, array("*"));
			/** @noinspection PhpAssignmentInConditionInspection */
			if ($arOldAccountPeriod = $rsOldAccountPeriod->Fetch())
			{
				\CTszhAccountPeriod::Delete($arOldAccountPeriod['ID']);
				// ���� ��������� ���� ������� �� e-mail, �� �������� ������� � �������� �������� �� ����� ��������� � �����
				if ($arOldAccountPeriod["IS_SENT"] == "Y")
				{
					$receiptFields["DATE_SENT"] = $arOldAccountPeriod["DATE_SENT"];
				}
			}
		}
		$accountPeriodId = \CTszhAccountPeriod::Add($receiptFields);
		if ($accountPeriodId <= 0)
		{
			$counter["ERR"]++;
			$this->ImportError(str_replace('#ACCOUNT#', $acc['name'], GetMessage("TI_ERROR_ADD_ACCOUNT_PERIOD")), $acc);

			return;
		}

		$accountFields['ID'] = $accountId;

		// ����������
		if (isset($receipt->service))
		{
			$this->__processCharges($accountId, $accountPeriodId, $receipt->service, $counter);
		}
		if (isset($receipt->service_debt))
		{
			$this->__processCharges($accountId, $accountPeriodId, $receipt->service_debt, $counter, $debt = true);
		}
		if ((isset($receipt->service_insurance)))
		{
			$this->__processCharges($accountId, $accountPeriodId, $receipt->service_insurance, $counter, $debt = false, $insurance = true);
		}
		if (isset($receipt->contractor))
		{
			$this->__processAccountContractors($accountId, $receipt->contractor, $accountPeriodId);
		}
		if (isset($receipt->correction))
		{
			$this->__processAccountCorrections($accountId, $receipt->correction, $accountPeriodId);
		}
		if (isset($receipt->credit))
		{
			$this->__processAccountInstallments($receipt->credit, $accountPeriodId);
		}
	}

	/**
	 * ��������� �������� �����
	 * ORG -> PersAcc
	 *
	 * @param XmlNode $acc
	 * @param array $counter
	 *
	 * @return bool
	 * @throws FormatException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 * @throws \Exception
	 */
	protected function ImportAccount($acc, &$counter, $bUpdateAcounts = true)
	{
		$accountExternalID = trim($acc['id']);
		$deleteAccount = isset($acc['del']);

		// �������� �������� ����� � ��������� del
		if (!isset($acc['del']))
		{
			// ���������� ����� �������� ����� �� ��������, ��������� �� �
			// $nameLs = trim($acc['name']);

			$nameLs = trim($acc->get('name', false));

			if (strlen($nameLs) > 0)
			{
				$nPos = strpos($nameLs, GetMessage("TI_N_SIGN"));
				if ($nPos !== false)
				{
					$nameLs = trim(substr($nameLs, $nPos + 1));
				}

				$accountNumber = $nameLs;
			}

			if ($this->depersonalize)
			{
				$accountName = false;
			}
			else
			{
				$accountName = trim($acc->get('owner'));
			}
		}

		if (strlen($accountExternalID) <= 0)
		{
			$this->ImportError(GetMessage("TI_WRONG_ACCOUNT_EXTERNAL_ID"), $acc);
			$bError = true;
		}

		$newExternalIdParts = array();
		if (isset($acc['kod_ls']))
		{
			$newExternalId = $acc['kod_ls'];
			$newExternalIdParts = explode(self::NEW_EXTERNAL_ID_SEPARATOR, $newExternalId, 2);
			if (count($newExternalIdParts) !== 2)
			{
				$this->ImportError(GetMessage("TI_WRONG_ACCOUNT_KOD_LS_NEW", array("#ACCOUNT#" => isset($accountNumber) ? $accountNumber : '')), $acc);
				$bError = true;
			}
		}
		else
		{
			$newExternalId = false;
		}

		if ($bError)
		{
			$counter["ERR"]++;

			return false;
		}

		// ������� ����� �� id
		$dbAccount = \CTszhAccount::GetList(Array("ID" => "ASC"), Array(
			"EXTERNAL_ID" => $accountExternalID,
			"TSZH_ID" => $this->tszhID,
		));

		// ���� �� ������� ������� ������ �� id, �� ����� �� kod_ls
		if ($dbAccount->SelectedRowsCount() == 0)
		{
			if ($newExternalId && is_array($newExternalIdParts)) // ��. ����� self::mergeAccount()
			{
				$_external = array(
					// $accountExternalID, // �� id
					$newExternalId, // �� kod_ls
					$newExternalIdParts[1], // ��� ��������
					// $newExternalIdParts[0] . $newExternalIdParts[1] // ������� + ��� (��� �����������)
				);
			}
			/*else
			{
				$_external = $accountExternalID;
			}*/

			$dbAccount = \CTszhAccount::GetList(Array("ID" => "ASC"), Array(
				"EXTERNAL_ID" => $_external,
				"TSZH_ID" => $this->tszhID,
			));
		}
		/** @noinspection PhpAssignmentInConditionInspection */

		// ����������� �������� ����� � ��������� del
		if ($deleteAccount)
		{
			$deleteFields = Array(
				'ACTIVE' => 'N',

			);
			/** @noinspection PhpAssignmentInConditionInspection */
			if ($account = $dbAccount->Fetch())
			{
				/*
				$userID = $account['USER_ID'];
				$bUserExists = is_array(\CUser::GetByID($userID)->Fetch());
				*/

				if (\CTszhAccount::Update($account['ID'], $deleteFields))
				{
					/*
					if ($bUserExists)
					{
						\CUser::Delete($userID);
					}
					*/
					$counter["DEL"]++;
				}
			}

			return false;
		}

		if ($bUpdateAcounts)
		{
			/** @var XmlNode $room */
			$room = array_shift($acc->room);

			$accountFields = Array(
				'XML_ID' => $accountNumber,
				'EXTERNAL_ID' => $accountExternalID,
				'ELS_MAIN' => $acc->get('els_main'),
				'ELS_OVERHAUL' => $acc->get('els_overhaul'),
				'IZHKU_MAIN' => $acc->get('izhku_main'),
				'IZHKU_OVERHAUL' => $acc->get('izhku_overhaul'),
				'TYPE' => $acc['type'],
				'TSZH_ID' => $this->tszhID,
				'NAME' => $accountName,
				'FLAT' => $room['num'],
				'FLAT_ABBR' => $room->get('abbr'),
				'AREA' => $room->getFloat('area', true),
				'LIVING_AREA' => $room->getFloat('habarea', true),
				'PEOPLE' => $room->getInt('people', true),
				'FLAT_TYPE' => $room->get('type'),
				'FLAT_PROPERTY_TYPE' => $room->get('property_type'),
				'REGISTERED_PEOPLE' => $room->getInt('reg'),
				'EXEMPT_PEOPLE' => $room->getInt("comp"),
			);
			if (array_key_exists($acc["house_id"], $this->state["HOUSES"]["MAP"]))
			{
				$accountFields["HOUSE_ID"] = $this->state["HOUSES"]["MAP"][$acc["house_id"]];
			}
			else
			{
				throw new FormatException('CITRUS_TSZH_INCORRECT_HOUSE_ID', array(
					'#GIVEN#' => $acc["house_id"],
					'#ATTR#' => showArrayToString($accountFields),
				));
			}

			// ���������������� ����
			$this->processUserFields('uf_', $acc, $accountFields);

			/** @noinspection PhpAssignmentInConditionInspection */
			if ($account = $dbAccount->Fetch())
			{
				$accountId = $account['ID'];

				// ��-�� ����������� ����� ���������� �������� ����� ���� ������� ���� � id = 100 � kod_ls = 1 100
				// ��� ������������� ���������� ����� ������� �����
				/* if ($newExternalId)
				{
					self::mergeAccount($accountId, $newExternalId);
				}*/

				$success = \CTszhAccount::Update($accountId, $accountFields, ConvertTimeStamp(MakeTimeStamp($this->sPeriod, 'YYYY-MM-DD'), 'FULL'));

				if ($success)
				{
					$counter["UPD"]++;
				}
				else
				{
					$counter["ERR"]++;
					$this->ImportError(GetMessage("TI_ERROR_UPDATE_ACCOUNT"), $acc);
				}
			}
			// ���������� ������ �������� �����
			else
			{
				$accountId = \CTszhAccount::Add($accountFields, false, ConvertTimeStamp(MakeTimeStamp($this->sPeriod, 'YYYY-MM-DD'), 'FULL'));
				$success = $accountId > 0;

				// ��-�� ����������� ����� ���������� �������� ����� ���� ������� ���� � id = 100 � kod_ls = 1 100
				// ��� ������������� ���������� ����� ������� �����
				/*if ($newExternalId && $success)
				{
					self::mergeAccount($accountId, $newExternalId);
				}*/

				if ($success)
				{
					$counter["ADD"]++;
				}
				else
				{
					$counter["ERR"]++;
					$this->ImportError(str_replace('#ACCOUNT_ID#', $accountNumber, GetMessage("TI_ERROR_ADD_ACCOUNT")), $acc);
				}
			}
		}
		else
		{
			if ($account = $dbAccount->Fetch())
			{
				$accountId = $account['ID'];
				$success = true;
			}
		}


		if ($success)
		{
			// ��������
			if (isset($acc->meter))
			{
				$this->__processMeters($accountId, $acc->meter, $counter);
			}

			$receiptTypes = ReceiptType::getConstants();
			foreach ($receiptTypes as $receiptTypeCode => $receiptTypeValue)
			{
				$receiptTypeCode = strtolower($receiptTypeCode);
				if (isset($acc->$receiptTypeCode))
				{
					$receipt = array_shift($acc->$receiptTypeCode);
					$this->processReceipt($receiptTypeValue, $accountId, $receipt, $counter);
				}
			}
		}

		return $accountId;
	}

	/**
	 * ��������� ������ �������� ����� �� �����������
	 * ORG -> PersAcc -> contractor
	 *
	 * @param int $accountId
	 * @param array $contractorsArray
	 * @param int $receiptId
	 *
	 * @return bool
	 * @throws FormatException
	 */
	protected function __processAccountContractors($accountId, $contractorsArray, $receiptId)
	{
		// delete old
		$rs = \CTszhAccountContractor::GetList(Array(), Array(
			"ACCOUNT_PERIOD_ID" => $receiptId,
		), false, false, Array('ID'));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($ar = $rs->Fetch())
		{
			\CTszhAccountContractor::Delete($ar['ID']);
		}

		/**
		 * @var int $idx
		 * @var XmlNode $contractor
		 */
		foreach ($contractorsArray as $idx => $contractor)
		{
			if (!array_key_exists($contractor['id'], $this->state['CONTRACTORS']['MAP']))
			{
				$this->ImportError(getMessage("CITRUS_TSZH_ERROR_ADD_ACCOUNT_CONTRACTOR_NOT_FOUND", array(
					"#ID#" => $contractor['id'],
					"#ACCOUNT_ID#" => $contractor->getParent()->getParent()->get('name'),
				)), $contractor);

				return false;
			}

			$contractorId = $this->state['CONTRACTORS']['MAP'][$contractor["id"]];
			if (isset($contractor->barcode))
			{
				foreach ($contractor->barcode as $idx => $barcode)
				{
					$returnValue = Array(
						"TYPE" => $barcode['type'],
						"VALUE" => trim((string)$barcode),
					);
				}

			}
			$arFields = Array(
				"ACCOUNT_PERIOD_ID" => $receiptId,
				"CONTRACTOR_ID" => $contractorId,
				"SUMM" => $contractor["sum_to_pay"],
				"DEBT_BEG" => isset($contractor["debt_beg"]) ? $contractor["debt_beg"] : false,
				"SUMM_PAYED" => isset($contractor["sum_payed"]) ? $contractor["sum_payed"] : false,
				"PENALTIES" => isset($contractor["peni"]) ? $contractor["peni"] : false,
				"SUMM_CHARGED" => isset($contractor["sum"]) ? $contractor["sum"] : false,
				"RECEIPT_ORDER" => isset($contractor["num"]) ? $contractor["num"] : false,
				"SERVICES" => isset($contractor["services"]) ? $contractor["services"] : false,
				"BARCODE_TYPE" => isset($returnValue["TYPE"]) ? $returnValue["TYPE"] : "",
				"BARCODE_VALUE" => isset($returnValue["VALUE"]) ? $returnValue["VALUE"] : "",
				"IS_OVERHAUL" => isset($contractor["is_overhaul"]) ? $contractor["is_overhaul"] : false,
			);

			$id = \CTszhAccountContractor::Add($arFields);
			if ($id <= 0)
			{
				$this->ImportError(getMessage("TI_ERROR_ADD_ACCOUNT_CONTRACTOR", array(
					"#ACCOUNT_ID#" => $contractor->getParent()->getParent()->offsetGet('name'),
					"#ID#" => $accountId,
				)), $arFields);
			}
		}

		return true;
	}

	/**
	 * ��������� ������������ �� �������� �����
	 *
	 * @param int $accountID
	 * @param array $correctionsArray
	 * @param int $accountPeriodID
	 *
	 * @throws ArgumentOutOfRangeException
	 * @throws FormatException
	 */
	protected function __processAccountCorrections($accountID, $correctionsArray, $accountPeriodID)
	{
		// delete existing corrections
		$rs = \CTszhAccountCorrections::GetList(Array(), Array(
			"ACCOUNT_PERIOD_ID" => $accountPeriodID,
		), false, false, Array('ID'));

		/** @noinspection PhpAssignmentInConditionInspection */
		while ($ar = $rs->Fetch())
		{
			\CTszhAccountCorrections::Delete($ar['ID']);
		}

		/**
		 * @var int $idx
		 * @var XmlNode $correction
		 */
		foreach ($correctionsArray as $idx => $correction)
		{
			$contractorID = $this->state['CONTRACTORS']["MAP"][$correction['contractor']];
			$arFields = Array(
				"XML_ID" => $correction->get('id') ? $correction->get('id') : '',
				"ACCOUNT_ID" => $accountID,
				"ACCOUNT_PERIOD_ID" => $accountPeriodID,
				"CONTRACTOR_ID" => $contractorID,
				"SUMM" => $correction->getFloat('sum', true),
				"GROUNDS" => $correction->get('grounds') ? $correction->get('grounds') : '',
				"SERVICE" => $correction->get('name', true),
			);
			$id = \CTszhAccountCorrections::Add($arFields);
			if ($id <= 0)
			{
				$this->ImportError(str_replace('#ACCOUNT_ID#', $correction->getParent()->offsetGet('name'), GetMessage("TI_ERROR_ADD_CORRECTIONS")), $arFields);
			}
		}
	}

	/**
	 * ��������� ������ �� ��������� ��������� �����
	 * ORG -> PersAcc -> credit
	 *
	 * @param array $installmentsArray
	 * @param int $accountPeriodID
	 *
	 * @return bool
	 * @throws ArgumentOutOfRangeException
	 * @throws FormatException
	 */
	protected function __processAccountInstallments($installmentsArray, $accountPeriodID)
	{
		// delete old
		$rs = \CTszhAccountInstallments::GetList(Array(), Array(
			"ACCOUNT_PERIOD_ID" => $accountPeriodID,
		), false, false, Array("ID"));

		/** @noinspection PhpAssignmentInConditionInspection */
		while ($ar = $rs->Fetch())
		{
			\CTszhAccountInstallments::Delete($ar["ID"]);
		}

		/**
		 * @var int $idx
		 * @var XmlNode $installment
		 */
		foreach ($installmentsArray as $idx => $installment)
		{
			$contractorID = $this->state['CONTRACTORS']['MAP'][$installment["contractor"]];
			$arFields = Array(
				"ACCOUNT_PERIOD_ID" => $accountPeriodID,
				"CONTRACTOR_ID" => $contractorID,
				"SERVICE" => $installment["name"],
				"SUMM_PAYED" => $installment->getFloat("sum", true),
				"SUMM_PREV_PAYED" => $installment->getFloat("balance"),
				"PERCENT" => $installment->getFloat("rate"),
				"SUMM_RATED" => $installment->getFloat("sum_credit"),
				"SUMM2PAY" => $installment->getFloat("sum_to_pay"),
			);
			$id = \CTszhAccountInstallments::Add($arFields);
			if ($id <= 0)
			{
				$this->ImportError(getMessage("TI_ERROR_ADD_ACCOUNT_INSTALLMENT", array("#ACCOUNT_ID#" => $installment->getParent()->offsetGet('name'))), $arFields);
			}
		}

		return true;
	}

	/**
	 * ��������� ���������� ��� ����� �������� �����
	 * ORG -> PersAcc -> (service, service_debt ��� service_insurance)
	 *
	 * @param int $accountId ID �������� �����
	 * @param int $accountPeriodId ID ���������
	 * @param array $chargesArray ������ ���������� (������ �������� XmlNode)
	 * @param array $counter
	 * @param bool $debt true ���� �������������� �������� service_debt
	 * @param bool $insurance true ���� �������������� �������� service_insurance
	 *
	 * @throws FormatException
	 * @throws \Exception
	 */
	protected function __processCharges($accountId, $accountPeriodId, $chargesArray, &$counter, $debt = false, $insurance = false)
	{
		static $sortCounter = array();

		/**
		 * ����������� � ������ ������ �������� XmlNode ($this->state['HMETERS'])
		 * ������������ � __PHP_Incomplete_Class �.�. �� ������ ������������� ������ ����� XmlNode ��� �� ��� ��������
		 *
		 * ����� ������ ��� �������� �� ���������� serialize � unserialize ��� ���������� ������������� ��������
		 */
		if (is_array($this->state["HMETERS"]) && !empty($this->state["HMETERS"]))
		{
			foreach ($this->state["HMETERS"] as &$hMeterObject)
			{
				if (!is_object($hMeterObject) && gettype($hMeterObject) == 'object')
				{
					$hMeterObject = unserialize(serialize($hMeterObject));
				}
			}
			unset($hMeterObject);
		}


		/**
		 * @var int $idx
		 * @var XmlNode $charge
		 */

		foreach ($chargesArray as $idx => $charge)
		{
			$strServiceXML_ID = $arService["XML_ID"] = self::__makeServiceExternalID($charge);
			$serviceID = false;
			if (array_key_exists($strServiceXML_ID, $this->state['SERVICES']))
			{
				$serviceID = $this->state['SERVICES'][$strServiceXML_ID];
			}

			if ($serviceID > 0)
			{
				$this->resetX();
				$componentValues = array(1 => 'Y', 2 => 'Z');
				$componentAttr = $this->getValue('component', $charge, true);
				$arFields = Array(
					'SERVICE_ID' => $serviceID,
					'ACCOUNT_PERIOD_ID' => $accountPeriodId,
					'SORT' => (++$sortCounter[$accountPeriodId]) * 10,
					'AMOUNT' => $this->getValue('amount', $charge, false, 'AMOUNT') + floatval($charge->getFloat('hamount', false, true)),
					'RATE' => $this->getValue('tarif', $charge, false, 'TARIFF'),
					'SUMM' => ($insurance ? $this->getValue('sum', $charge, true, "SUMM") : $this->getValue('csum', $charge, true, 'SUMM')),
					'CORRECTION' => $this->getValue('correction', $charge, true, 'CORRECTION'),
					'GUID' => $this->getValue('id', $charge, false, 'GUID'),
					'COMPENSATION' => $this->getValue('compensation', $charge, true, 'COMPENSATION'),
					'SUMM_PAYED' => $this->getValue('sum_payed', $charge, true, 'SUMM_PAYED'),
					'SUMM2PAY' => $this->getValue('csum_to_pay', $charge, true, 'SUMM2PAY'),
                    'DEBT_OVERPAY' => $this->getValue('debt_view', $charge, true, 'DEBT_OVERPAY'),
					'DEBT_ONLY' => $debt ? 'Y' : 'N',
					'PENALTIES' => $this->getValue('peni', $charge, true, 'PENALTIES'),

					'HAMOUNT' => $this->getValue('hamount', $charge, true, 'HAMOUNT'),
					'HNORM' => $this->getValue('hnorm', $charge, true, 'HNORM'),
					'HSUMM' => $this->getValue('hsum', $charge, true, 'HSUMM'),
					'HSUMM2PAY' => $this->getValue('hsum_to_pay', $charge, true, 'HSUMM2PAY'),

					'COMPONENT' => in_array($componentAttr, array_keys($componentValues)) ? $componentValues[$componentAttr] : 'N',

					'DEBT_BEG' => $this->getValue('debt_beg', $charge, true, 'DEBT_BEG'),
					'DEBT_END' => $this->getValue('debt_end', $charge, true, 'DEBT_END'),

					'VOLUMEP' => $this->getValue('volumep', $charge, true, 'VOLUMEP'),
					'VOLUMEH' => $this->getValue('volumeh', $charge, true, 'VOLUMEH'),
					'VOLUMEA' => $this->getValue('volumea', $charge, true, 'VOLUMEA'),

					'GROUP' => isset($charge['group']) ? $charge['group'] : false,
					'AMOUNTN' => isset($charge['amountn']) ? $charge['amountn'] : false,
					'HAMOUNTN' => isset($charge['hamountn']) ? $charge['hamountn'] : false,
					'AMOUNT_NORM' => isset($charge['amount_norm']) ? $charge['amount_norm'] : false,

					'AMOUNT_VIEW' => isset($charge['amount_view']) ? $charge['amount_view'] : false,

					'RAISE_MULTIPLIER' => isset($charge['raise_multiplier']) ? $charge['raise_multiplier'] : false,
					'RAISE_SUM' => isset($charge['raise_sum']) ? $charge['raise_sum'] : false,
					'SUM_WITHOUT_RAISE' => isset($charge['sum_without_raise']) ? $charge['sum_without_raise'] : false,
					'CSUM_WITHOUT_RAISE' => isset($charge['csum_without_raise']) ? $charge['csum_without_raise'] : false,
					'IS_INSURANCE' => $insurance ? "Y" : "N",
					'CONTRACTOR_ID' => isset($charge['contractor']) ? $charge['contractor'] : "",
				);

				// ���������������� ���� ����������
				$this->processUserFields('uf_', $charge, $arFields);

				// ��������� ����������� � ���������� ��������
				$arHMeterIds = array();
				$arNotImportedHMeters = array();
				foreach ($charge as $attrName => $attrValue)
				{
					if (strlen($attrValue) <= 0)
					{
						continue;
					}

					if (preg_match('/^hmeter\d*$/', $attrName))
					{
						// 1� ��������� � ���� ��������� ��������� ��������, ���������� ����� �������� (������ �16052)
						if (array_key_exists($attrValue, $this->deletedMeters))
						{
							continue;
						}

						if (array_key_exists($attrValue, $this->state['HMETERS']))
						{
							$hMeter = $this->state['HMETERS'][$attrValue];
							if (is_array($hMeter) || is_object($hMeter))
							{
								$arNotImportedHMeters[] = $hMeter;
							}
							else
							{
								$arHMeterIds[] = $hMeter;
								$arFields["HMETER_IDS"][] = $hMeter;
							}
						}
						else
						{
							$counter["CHARGE_ERR"]++;
							$this->ImportError(GetMessage("CITRUS_TSZH_ERROR_WRONG_METER_ATTRIBUTE",
								array(
									"#ACCOUNT_ID#" => $charge->getParent()->getParent()->offsetGet('name'),
									"#SERVICE_NAME#" => $charge["name"],
									"#ATTR_NAME#" => $attrName,
									"#ATTR_VALUE#" => $attrValue,
								)),
								$charge
							);
						}
					}
					elseif (preg_match('/^meter\d*$/', $attrName))
					{
						// 1� ��������� � ���� ��������� ��������� ��������, ���������� ����� �������� (������ �16052)
						if (array_key_exists($attrValue, $this->deletedMeters))
						{
							continue;
						}

						if (array_key_exists($attrValue, $this->state['ACCOUNT_METERS']))
						{
							$arFields["METER_IDS"][] = $this->state['ACCOUNT_METERS'][$attrValue];
						}
						else
						{
							$counter["CHARGE_ERR"]++;
							$this->ImportError(GetMessage("CITRUS_TSZH_ERROR_WRONG_METER_ATTRIBUTE",
								array(
									"#ACCOUNT_ID#" => $charge->getParent()->getParent()->offsetGet('name'),
									"#SERVICE_NAME#" => $charge["name"],
									"#ATTR_NAME#" => $attrName,
									"#ATTR_VALUE#" => $attrValue,
								)),
								$charge
							);
						}
					}
				}

				// �������������� ����, ������� ����� ��������� X
				$additionalX = array("units" => "UNITS", "norm" => "NORM");
				foreach ($additionalX as $attr => $field)
				{
					$this->getValue($attr, $charge, false, $field);
				}
				// ==============================================
				$arFields["X_FIELDS"] = $this->getX();

				$chargeId = \CTszhCharge::Add($arFields);
				if ($chargeId > 0)
				{
					$isError = false;

					if (!empty($arNotImportedHMeters))
					{
						$this->__processMeters(false, $arNotImportedHMeters, $counter, $accountId);
						$arHMeterIds = array();
						foreach ($arNotImportedHMeters as $hmeter)
						{
							if (isset($this->state['HMETERS'][$hmeter["id"]]) && is_numeric($this->state['HMETERS'][$hmeter["id"]])
							    && ($hMeterId = $this->state['HMETERS'][$hmeter["id"]])
							)
							{
								$arHMeterIds[] = $hMeterId;
							}
						}
						if (!empty($arHMeterIds))
						{
							$res = \CTszhCharge::bindMeter($chargeId, $arHMeterIds);
							if (!$res)
							{
								$isError = true;
								$counter["CHARGE_ERR"]++;
								$this->ImportError(getMessage(
									"TI_ERROR_CHARGE_BIND_HMETERS",
									array(
										'#ACCOUNT_ID#' => $charge->getParent()->getParent()->offsetGet('name'),
										"#SERVICE_NAME#" => $charge['name'],
									)
								), $charge);
							}
						}
					}

					if (!$isError)
					{
						$counter["CHARGE_ADD"]++;
					}

					if (Loader::IncludeModule("citrus.tszhpayment"))
					{
						$this->__processDetailServicePayments($chargeId, $charge->getChilds()['details_for_payments']);
					}
				}
				else
				{
					$counter["CHARGE_ERR"]++;
					$this->ImportError(str_replace('#ACCOUNT_ID#', $charge->getParent()->offsetGet('name'), GetMessage("TI_ERROR_ADD_CHARGE")), $charge);
				}
			}
			else
			{
				$counter["CHARGE_ERR"]++;
				$this->ImportError(str_replace('#ACCOUNT_ID#', $charge->getParent()->offsetGet('name'), GetMessage("TI_ERROR_ADD_CHARGE_NO_SERVICE")), $charge);
			}
		}

	}

	/**
	 * @param int|bool $accountId Id �������� ����� ��� false ���� ������������� ����������� ������� �����
	 * @param array $arMeterXML ������ �������� XmlNode
	 * @param array $counter
	 * @param int|bool $hAccountId (��� ������� ����������� ���������)
	 *
	 * @throws FormatException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 * @throws \Exception
	 */
	protected function __processMeters($accountId, $arMeterXML, &$counter, $hAccountId = false)
	{
		global $DB;

		if (!is_array($arMeterXML) || empty($arMeterXML))
		{
			return;
		}

		$bHouseMeters = !$accountId;

		if (!$bHouseMeters)
		{
			$this->state['ACCOUNT_METERS'] = array();
		}

		/**
		 * @var int $idx
		 * @var XmlNode $meter
		 */
		foreach ($arMeterXML as $idx => $meter)
		{
			// ��������� ��������� ��������, ����������� � �/� �� ������ �����������?
			if (\COption::GetOptionString(TSZH_MODULE_ID, "shared_meters", "Y") == "Y")
			{
				$arFilter = array('XML_ID' => $meter['id']);
			}
			else
			{
				$arFilter = Array('TSZH_ID' => $this->tszhID, 'XML_ID' => $meter['id']);
			}

			if ($bHouseMeters)
			{
				$arFilter = array_merge($arFilter, Array("HOUSE_METER" => "Y"));
			}
			else
			{
				$arFilter = array_merge($arFilter, Array("HOUSE_METER" => "N"));
			}
			$exisingMeter = \CTszhMeter::GetList(Array(), $arFilter)->Fetch();

			// �������� ��������� � ��������� del
			if (isset($meter['del']))
			{
				// �������� ���� ��������� ���������, ����� ������������ �� � <item meter*> (������ �16052)
				$this->deletedMeters[$meter['id']] = true;

				if (is_array($exisingMeter))
				{
					//\CTszhMeter::Delete($exisingMeter["ID"], $accountId);
					\CTszhMeter::Update($exisingMeter["ID"], Array("ACTIVE" => "N"));
					$counter["METER_DEA"]++;
				}
				continue;
			}

			if ($bHouseMeters && $hAccountId !== false)
			{
				if (!is_array($exisingMeter))
				{
					$this->state['HMETERS'][$meter['id']] = $meter;
					continue;
				}
			}

			$values_count = $meter->getInt('values');
			if ($values_count < 0 || $values_count > 3)
			{
				throw new \Exception(Loc::getMessage("CITRUS_TSZH_WRONG_METER_VALUES_COUNT", array('#VALUE#' => $values_count)));
			}

			$meterFields = Array(
				'ACTIVE' => 'Y',
				'XML_ID' => $meter['id'],
				"SORT" => $idx * 10,
				'NAME' => $meter['name'],
				'NUM' => $meter->get('num'),
				'SERVICE_ID' => false,
				'SERVICE_NAME' => $meter['service_name'],
				'VALUES_COUNT' => $values_count,
				"HOUSE_METER" => $bHouseMeters ? "Y" : "N",
				"DEC_PLACES" => $meter->getInt('precision'),
				"CAPACITY" => $meter->getFloat('capacity'),
				"VERIFICATION_DATE" => $meter->getDate('verification_date') ? ConvertTimeStamp($meter->getDate('verification_date'), "SHORT") : "",
			);

			$meterID = false;
			if (!is_array($exisingMeter) || $exisingMeter['ID'] <= 0)
			{
				$meterID = \CTszhMeter::Add($meterFields);
				if ($meterID > 0)
				{
					if ($bHouseMeters)
					{
						\CTszhMeter::bindAccount($meterID, $hAccountId);
					}
					else
					{
						\CTszhMeter::bindAccount($meterID, $accountId);
					}
					$counter["METER_ADD"]++;
				}
				else
				{
					$counter["METER_ERR"]++;
					$this->ImportError(str_replace('#ACCOUNT_ID#', $meter->getParent()->offsetGet('name'), GetMessage($bHouseMeters ? "TI_ERROR_ADD_HMETER" : "TI_ERROR_ADD_METER")), $bException = true);
				}
			}
			else
			{
				if (\CTszhMeter::Update($exisingMeter['ID'], $meterFields))
				{
					$meterID = $exisingMeter['ID'];
					if ($bHouseMeters)
					{
						\CTszhMeter::bindAccount($meterID, $hAccountId);
					}
					else
					{
						\CTszhMeter::bindAccount($meterID, $accountId);
					}
					$counter["METER_UPD"]++;
				}
				else
				{
					$counter["METER_ERR"]++;
					$this->ImportError(str_replace('#ACCOUNT_ID#', '?', GetMessage($bHouseMeters ? "TI_ERROR_UPDATE_HMETER" : "TI_ERROR_UPDATE_METER", Array("#NAME#" => $meterFields['NAME']))), $bException = true);
				}
			}

			if ($meterID)
			{
				// save in session for future use (while importing charges)
				if ($bHouseMeters)
				{
					$this->state['HMETERS'][$meterFields['XML_ID']] = $meterID;
				}
				else
				{
					$this->state['ACCOUNT_METERS'][$meterFields['XML_ID']] = $meterID;
				}

				// add meter ID to "imported" list, will be used in ::Cleanup();
				if ($this->useSessions)
				{
					$DB->Query("INSERT INTO " . $this->tableName . " (SESS_ID, PARENT_ID, RIGHT_MARGIN) values ('" . $DB->ForSql($this->sessionId) . "', 0, " . $meterID . ")");
				}
				else
				{
					$DB->Query("INSERT INTO " . $this->tableName . " (PARENT_ID, RIGHT_MARGIN) values (0, " . $meterID . ")");
				}

				// �������� � ���������� ��������� ���������������� ����
				$this->processUserFields('vuf_', $meter, $curValueFields);

				// ��������� ��������� �� ��������� ����� ���� � ����� ����������� ��� ���� (������ ����� ��������� ��������� �� ������� ���� � � ����� ������ ���� �����)
				$hasCurValue = isset($meter['val1']) && ($meter['val1'] <> 0 || isset($meter["date"]));
				// ������� ���������
				// =================
				if ($hasCurValue)
				{
					// ���� ���������
					$curValueTime = $meter->getDate('date', true);
					$curValueFields = array(
						"METER_ID" => $meterID,
						"TIMESTAMP_X" => ConvertTimeStamp($curValueTime, "FULL"),
					);

					$arExistingCurValue = \CTszhMeterValue::GetList(
						Array(),
						Array(
							"METER_ID" => $meterID,
							"TIMESTAMP_X" => $curValueFields["TIMESTAMP_X"],
						),
						false,
						array("nTopCount" => 1),
						Array('ID')
					)->Fetch();
					$curValueId = is_array($arExistingCurValue) ? $arExistingCurValue["ID"] : false;

					for ($i = 1; $i <= $values_count; $i++)
					{
						$curValueFields["VALUE$i"] = $meter->getFloat('val' . $i, true);
						if (false !== $meter->getFloat("charge" . $i))
						{
							$curValueFields["AMOUNT$i"] = $meter->getFloat("charge" . $i);
						}
					}

					if ($curValueId)
					{
						$success = \CTszhMeterValue::Update($curValueId, $curValueFields);
					}
					else
					{
						$success = \CTszhMeterValue::Add($curValueFields) > 0;
					}

					if ($success)
					{
						$counter["METER_VALUE_ADD"]++;
					}
					else
					{
						$counter["METER_VALUE_ERR"]++;
						$this->ImportError(str_replace('#METER_XML_ID#', $meterFields["XML_ID"], GetMessage("TI_ERROR_UPDATE_METER_VALUE")), $bException = true);
					}

				}
			}
		}
	}

	/**
	 * @param \ArrayIterator $barcodes
	 *
	 * @return array
	 * @internal param int $accountPeriodID
	 */
	protected function __processBarcodes($barcodes)
	{
		$returnValue = array();
		/**
		 * @var int $idx
		 * @var XmlNode $barcode
		 */
		foreach ($barcodes as $idx => $barcode)
		{
			$returnValue[] = Array(
				"TYPE" => $barcode['type'],
				"VALUE" => trim((string)$barcode),
			);
		}

		return $returnValue;
	}

	/**
	 * @param \ArrayIterator $barcodes
	 *
	 * @return array
	 * @internal param int $accountPeriodID
	 */
	protected function __processBarcodesInsurance($barcodes)
	{
		$returnValue = array();
		/**
		 * @var int $idx
		 * @var XmlNode $barcode
		 */
		foreach ($barcodes as $idx => $barcode)
		{
			$returnValue[] = Array(
				"TYPE" => $barcode['type'],
				"VALUE" => trim((string)$barcode),
			);
		}

		return $returnValue;
	}

	protected function __processDetailServicePayments($serviceID = 0, $arDetails)
	{
		if ((count($arDetails) > 0) && ($serviceID > 0))
		{

			foreach ($arDetails as $detail)
			{
				$arField = [
					'GUID' => $detail->get('id'),
					'NAME' => $detail->get('name'),
					'SUM_TO_PAY' => $detail->get('sum_to_pay'),
					'SERVICES_PERIOD_ID' => $serviceID,
				];

				$prevId = intval(DetailsPaymentsTable::getList([
					'select' => ['id'],
					'filter' => [
						'GUID' => $arField['GUID'],
					],
				])->fetch());

				if ($prevId)
				{
					$result = DetailsPaymentsTable::update($prevId, $arField);
				}
				else
				{
					$result = DetailsPaymentsTable::add(
						$arField
					);
				}
				if (!$result->isSuccess())
				{
					throw new \LogicException("DetailsPaymentsTable: \r\n".implode($result->getErrorMessages(),';'));
				}
			}

		}

		return true;
	}

	/**
	 *
	 * @param string $action
	 *
	 * @return array
	 * @throws \Exception
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public function Cleanup($action)
	{
		global $DB;

		$counter = array();

		if (!in_array($this->filetype, array("accounts", "calculations")))
		{
			return $counter;
		}

		if ($this->updateMode && $action != "D" && $action != "A")
		{
			return $counter;
		}

		if ($this->onlyDebt)
		{
			return $counter;
		}

		$bDelete = $action == "D";
		$bDeactivate = $action == "A";

		//This will protect us from deactivating when next_step is lost
		if (intval($this->state["ACCOUNTS_IMPORT_STARTED"] < 1))
		{
			return $counter;
		}

		if (!array_key_exists("LAST_METER_ID", $this->state))
		{
			// deactivate/delete accounts/users
			$arFilter = array(
				">ID" => $this->state["LAST_ID"],
				"TSZH_ID" => $this->tszhID,
			);
			if (!$bDelete)
			{
				$arFilter["USER_ACTIVE"] = "Y";
			}

			$obAccount = new \CTszhAccount();
			$rsAccount = $obAccount->GetList(
				Array("ID" => "asc"),
				$arFilter,
				false, false,
				Array("ID", "USER_ID", "USER_ACTIVE", "TSZH_ID")
			);

			$obUser = new \CUser();
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($arAccount = $rsAccount->Fetch())
			{
				$rs = $DB->Query("select ID from " . $this->tableName . " where (PARENT_ID+0 = 0 AND LEFT_MARGIN = " . $arAccount["ID"] . ')' . $this->andWhereThisSession);
				if (!$rs->Fetch())
				{
					if ($bDelete)
					{
						\CTszhAccount::Delete($arAccount["ID"]/*, true*/);
						$counter["DEL"]++;
					}

					/*if ($bDeactivate && $arAccount["USER_ID"] != 1) // won't deactivate admin account
					{
						//CTszhAccount::Update($arAccount["ID"], Array("ACTIVE" => "N"));
						$obUser->Update($arAccount["USER_ID"], Array("ACTIVE" => "N"));
						$counter["DEA"]++;
					}*/
				}
				else
				{
					$counter["NON"]++;
				}

				$this->state["LAST_ID"] = $arAccount["ID"];

				if ($this->timeIsUp())
				{
					return $counter;
				}
			}
		}

		// deactivate/delete meters
		$arFilter = array(
			">ID" => $this->state["LAST_METER_ID"],
			"TSZH_ID" => $this->tszhID,
		);
		$obMeter = new \CTszhMeter;
		$rsMeter = $obMeter->GetList(
			Array("ID" => "asc"),
			$arFilter,
			false, false,
			Array("ID", "TSZH_ID")
		);

		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arMeter = $rsMeter->Fetch())
		{
			$rs = $DB->Query("select ID from " . $this->tableName . " where (ID > 1 and PARENT_ID+0 = 0 AND RIGHT_MARGIN = " . $arMeter["ID"] . ')' . $this->andWhereThisSession);
			if (!$rs->Fetch())
			{
				if ($bDelete)
				{
					\CTszhMeter::Delete($arMeter["ID"], false);
					$counter["METER_DEL"]++;
				}
				elseif ($bDeactivate)
				{
					$obMeter->Update($arMeter["ID"], Array("ACTIVE" => "N"));
					$counter["METER_DEA"]++;
				}
			}
			else
			{
				$counter["METER_NON"]++;
			}

			$this->state["LAST_METER_ID"] = $arMeter["ID"];

			if ($this->timeIsUp())
			{
				return $counter;
			}
		}

		return $counter;
	}

	public function clearSession()
	{
		global $DB;

		if (!$this->useSessions)
		{
			return;
		}

		// ������ ��� ������ ������� ������
		if ($DB->TableExists($this->tableName))
		{
			$DB->Query("DELETE from " . $this->tableName . " WHERE SESS_ID = '" . $DB->ForSQL($this->sessionId) . "'");

			// ��������� ������� ������ 1 ����
			$this->xmlFile->EndSession();
		}
	}

	/**
	 * �������� ������������ email
	 *
	 * @param string $email
	 *
	 * @return bool
	 */
	protected function check_email($email)
	{
		$email = trim($email);

		if (preg_match("#.*?[<\[\(](.*?)[>\]\)].*#i", $email, $arr) && strlen($arr[1]) > 0)
		{
			$email = $arr[1];
		}

		if (preg_match('/^[a-z0-9\._-]+@[a-z0-9\._-]+\.(xn--)?[a-z0-9]{2,}$/i', $email))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * ������������� ����� ������ ���������. �������� ������������ ��������� ���� �� ������ ����������� �������
	 */
	protected function setStartTime()
	{
		if (isset($this->startTime))
		{
			return;
		}

		if (defined("START_EXEC_PROLOG_BEFORE_1"))
		{
			list($usec, $sec) = explode(" ", START_EXEC_PROLOG_BEFORE_1);
			$this->startTime = $sec + $usec;
		}
		else
		{
			$this->startTime = microtime(true);
		}
	}

	/**
	 * ��������� �� ������� �� �����, ���������� �� ���� ��� (���) ������
	 * @return bool
	 * @throws \Exception ���� ����� ���� �� ��� ������ ����� setStartTime
	 */
	protected function timeIsUp()
	{
		return (microtime(true) - (float)$this->state['startTime']) > (float)$this->timeLimit;
	}

	/**
	 * @return bool
	 */
	private function addititionalPeriodsExists()
	{
		global $DB;

		$countAdditionalPeriods = $DB->Query("select ID from " . $this->tableName . " where (NAME='additional_period') " . $this->andWhereThisSession)->SelectedRowsCount();

		if ($countAdditionalPeriods > 0)
		{
			return true;
		}

		return false;
	}

}