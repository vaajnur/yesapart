<?

namespace Citrus\Tszh\Exchange\v4\Formats;

use Citrus\Tszh\Exchange\Export;

/**
 * ����� ��� �������� �������� �� ���������
 * http://wiki.citrus-soft.ru/dev/export-accexistence
 *
 * @package Citrus\Tszh\Exchange\v4\Formats
 */
class Accexistence extends Export
{
	/**
	 * ��������� ���������� ���� � �������
	 *
	 * @param array $filter
	 * @return $this
	 */
	public function filter($filter)
	{
		$this->filter = array_merge($this->filter, $filter);
		return $this;
	}

	/**
	 * @param array $filter
	 * @return $this
	 */
	public function setFilter($filter)
	{
		$this->filter = $filter;
		return $this;
	}

	/**
	 * @param null $lastId
	 *
	 * @return int|null
	 */
	public function run($lastId = null)
	{
		$startTime = microtime(1);

		@set_time_limit(0);
		@ignore_user_abort(true);

		if (!is_array($this->filter))
		{
			$this->filter = Array();
		}

		$isAutoRegistrationEnabled = \CTszhAccount::isAutoRegistrationEnabled();

		if ($lastId <= 0)
		{
			$xml = '<?xml version="1.0" encoding="' . SITE_CHARSET . '"?>' . "\n";

			$inn = htmlspecialcharsbx($this->state['tszh']['INN']);
			$xml .= '<org inn="' . $inn . '" filedate="' . Xml::formatDateTime() . '" filetype="accexistence" version="4" need_logins="' . intval($isAutoRegistrationEnabled) . '">' . "\n";
		}
		else
		{
			$xml = '';
		}

		$filter = array_merge(
			$this->filter,
			array(
				">ID" => $lastId,
			)
		);
		// ��������� ������ ������� �����, ������� ��������� �������
		if ($isAutoRegistrationEnabled)
			$filter["!USER_LOGIN"] = false; // USER_LOGIN ������������ ��� �������� ������������� ���������� ��� �/� ������������
		else
			$filter["!UF_REGCODE"] = '';

		$rsAccount = \CTszhAccount::GetList(
			array("ID" => "ASC"),
			$filter,
			false,
			false,
			array("ID", "EXTERNAL_ID", "USER_LOGIN", "USER_EMAIL", "USER_PERSONAL_PHONE", "TSZH_SITE", "UF_REGCODE")
		);

		global $cnt, $total;
		$cnt = 0;
		$total = $rsAccount->SelectedRowsCount();
		$arAccounts = array();
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arAccount = $rsAccount->Fetch())
		{
			$lastId = $arAccount["ID"];

			$arAccounts[$arAccount["ID"]] = array(
				"id" => $arAccount["EXTERNAL_ID"],
			);
			if (!\CTszhSubscribe::isDummyMail($arAccount["USER_EMAIL"], $arAccount["TSZH_SITE"]))
				$arAccounts[$arAccount["ID"]]["email"] = $arAccount["USER_EMAIL"];

			if ($arAccount["USER_PERSONAL_PHONE"]) $arAccounts[$arAccount["ID"]]["phone"] = $arAccount["USER_PERSONAL_PHONE"];

			$cnt++;

			if ($this->timeLimit && microtime(1) - $startTime >= $this->timeLimit)
				break;
		}

		foreach ($arAccounts as $accountID => $arAccount)
		{
			$xml .= "\t<acc";
			foreach ($arAccount as $name => $val)
			{
				$val = htmlspecialcharsbx($val);
				$xml .= " $name=\"$val\"";
			}
			$xml .= "></acc>\n";
		}

		if ($cnt == $total)
			$xml .= "</org>\n";

		fputs($this->output, $xml);

		if ($cnt == $total)
			return true;
		else
			return $lastId;
	}
}
