<?

namespace Citrus\Tszh\Exchange\v4\Formats;

use Citrus\Tszh\Exchange\Export;
use Citrus\Tszh\AccountsTable;

/**
 * ����� ��� �������� ��������� ���������
 * http://wiki.citrus-soft.ru/dev/export-meters
 *
 * @package Citrus\Tszh\Exchange\v4\Formats
 */
class MeterValues extends Export
{
	protected $filter = array();

	protected $valueFilter = array();

	/**
	 * ��������� ���������� ���� � �������
	 *
	 * @param array $filter
	 *
	 * @return $this
	 */
	public function filter($filter)
	{
		$this->filter = array_merge($this->filter, $filter);

		return $this;
	}

	/**
	 * @param array $filter
	 *
	 * @return $this
	 */
	public function setFilter($filter)
	{
		$this->filter = $filter;

		return $this;
	}

	/**
	 * ��������� ���������� ���� � ������� �� ����������
	 *
	 * @param array $filter
	 *
	 * @return $this
	 */
	public function valueFilter($filter)
	{
		$this->valueFilter = array_merge($this->valueFilter, $filter);

		return $this;
	}

	/**
	 * @param array $filter
	 *
	 * @return $this
	 */
	public function setValueFilter($filter)
	{
		$this->valueFilter = $filter;

		return $this;
	}


	/**
	 * @param null|int $lastId
	 *
	 * @return int
	 */
	public function run($lastId = null)
	{
		$startTime = microtime(1);
		@set_time_limit(0);
		@ignore_user_abort(true);

		if ($this->state['last_id'] <= 0)
		{
			$xml = '<?xml version="1.0" encoding="' . SITE_CHARSET . '"?>' . "\n";
			$attr_multipart = '';

			if ($this->state['multipart'])
			{
				$attr_multipart .= ' multipart="1"';
			}
			else
			{
				$attr_multipart .= ' ';
			}

			$inn = htmlspecialcharsbx($this->state['tszh']['INN']);
			$xml .= '<org inn="' . $inn . '" filedate="' . Xml::formatDateTime() . '" filetype="meters" version="4"' . $attr_multipart . '>' . "\n";
		}
		else
		{
			$xml = '';
		}

		do
		{
			$users = Array();
			$rsAccount = AccountsTable::getList(
				array(
					'select' => array("ID", "EXTERNAL_ID"),
					'order' => array("ID" => "ASC"),
					'filter' => array_merge(
						$this->filter,
						array(
							">ID" => $this->state['last_id'],
						)),
					'limit' => $this->state['multipart'] ? $this->state["limit"] : false
				)
			);

			/** @noinspection PhpAssignmentInConditionInspection */
			while ($arAccount = $rsAccount->Fetch())
			{
				$this->state['last_id'] = $arAccount['ID'];

				$users[$arAccount['ID']] = Array(
					'attributes' => Array(
						'id' => $arAccount['EXTERNAL_ID'],
					),
					'meters' => Array(),
					'meters_history' => Array(),
				);
				/*foreach ($arAccount as $key => $value)
				{
					if (strpos($key, 'UF_') === 0 && !empty($value))
						$users[$arAccount['ID']]['attributes'][strtolower($key)] = htmlspecialcharsbx($value);
				}*/

				$this->state['count']++;

				/*if ($this->timeLimit && microtime(1) - $startTime >= $this->timeLimit)
					break;*/
			}
			unset($rsAccount);

			if (count($users) != 0)
			{
				/*
				 * TODO ���������� �� ORM, ����� ��� ��������� ����� �������� ������� � ���������;
				 */
				$rsMeters = \CTszhMeter::GetList(
					array(),
					array("@ACCOUNT_ID" => array_keys($users), "ACTIVE" => "Y", "HOUSE_METER" => "N"),
					false,
					false,
					array("ID", "XML_ID", "NAME", "VALUES_COUNT", "ACCOUNT_ID")
				);
				$arMeters = array();
				/** @noinspection PhpAssignmentInConditionInspection */
				while ($arMeter = $rsMeters->getNext())
				{
					$arMeters[$arMeter["ID"]] = $arMeter;
				}
				unset($rsMeters);


				$rsMetersValues = \CTszhMeterValue::GetList(
					array("TIMESTAMP_X" => "ASC", "ID" => "ASC"),
					array_merge($this->valueFilter, Array("@METER_ID" => array_keys($arMeters))),
					false,
					false,
					array("ID", "VALUE1", "VALUE2", "VALUE3", "TIMESTAMP_X", "METER_ID")
				);
				/** @noinspection PhpAssignmentInConditionInspection */
				while ($arValue = $rsMetersValues->GetNext(true, false))
				{
					$arMeter = $arMeters[$arValue["METER_ID"]];
					$arFields = Array(
						"id" => $arMeter["XML_ID"],
						"name" => $arMeter["NAME"],
						"values" => $arMeter["VALUES_COUNT"],
						"date" => Xml::formatDateTime(MakeTimeStamp($arValue["TIMESTAMP_X"])),
					);
					for ($i = 1; $i <= max($arMeter["VALUES_COUNT"], 2); $i++)
					{
						$arFields["val" . $i] = $arValue["VALUE" . $i];
					}
					foreach ($arMeter["ACCOUNT_ID"] as $accountID)
					{
						if (is_set($users, $accountID))
						{
							$users[$accountID]["meters"][$arValue["METER_ID"]] = $arFields;
						}
					}
				}
				unset($rsMetersValues);

				foreach ($users as $userID => $arUser)
				{
					if (count($arUser['meters']) <= 0)
					{
						continue;
					}

					$xml .= "\t<acc";
					foreach ($arUser['attributes'] as $name => $val)
					{
						$val = htmlspecialcharsbx($val);
						$xml .= " $name=\"$val\"";
					}
					$xml .= ">\n";

					foreach ($arUser['meters'] as $arMeter)
					{

						$xml .= "\t\t<meter";
						foreach ($arMeter as $name => $val)
						{
							$val = htmlspecialcharsbx($val);
							$xml .= " $name=\"$val\"";
						}
						$xml .= " />\n";

					}

					$xml .= "\t</acc>\n";
				}
			}
			if (count($users) == 0)
			{
				$xml .= "</org>\n";
				$this->state['final'] = true;
			}
			else
			{
				if (!$this->state['multipart'])
				{
					$xml .= "</org>\n";
				}
			}
		}
		while (strlen($xml) <= 0);

		fputs($this->output, $xml);

		return $lastId;
	}
}