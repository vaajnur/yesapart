<?

namespace Citrus\Tszh\Exchange\v4\Formats;

use Citrus\Tszh\Exchange\Export;
use Citrus\Tszhpayment\PaymentTable;
use Citrus\Tszhpayment\PaymentServicesTable;

if (\CModule::IncludeModule('otr.sale'))
{
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/otr.sale/include.php");
}

/**
 * ����� ��� �������� ������ �� �������� ��������
 * http://wiki.citrus-soft.ru/dev/export-payments
 *
 * @package Citrus\Tszh\Exchange\v4\Formats
 */
class Payments extends Export
{
	protected $filter = array(
		"!ACCOUNT_ID" => false,
		"!PS_ACTION_FILE" => false,
	);

	/**
	 * ��������� ���������� ���� � �������
	 *
	 * @param array $filter
	 *
	 * @return $this
	 */
	public function filter($filter)
	{
		$this->filter = array_merge($this->filter, $filter);

		return $this;
	}

	/**
	 * @param array $filter
	 *
	 * @return $this
	 */
	public function setFilter($filter)
	{
		$this->filter = $filter;

		return $this;
	}

	/**
	 * @param null $lastId
	 *
	 * @return int|null
	 * @throws \Exception
	 */
	public function run($lastId = null)
	{
		if (!\CModule::IncludeModule("citrus.tszhpayment"))
		{
			throw new \Exception('Failed to include citrus.tszhpayment module');
		}

		$startTime = microtime(1);
		@set_time_limit(0);
		@ignore_user_abort(true);

		$this->state['tszh'] = \CTszh::GetByID($this->tszhId);
		$inn = $this->state['tszh']['INN'];

		$arTszhFilter = array(
			'INN' => $this->state['tszh']['INN'],
		);

		if (strlen($this->tszhId))
		{
			$arTszhFilter['ID'] = $this->tszhId;
		}

		$rsTshz = \CTszh::GetList(array(), $arTszhFilter, false, false, array('ID'));
		$arTszhList = array();
		while ($arTszh = $rsTshz->Fetch())
		{
			$arTszhList[] = $arTszh['ID'];
		}

		$this->filter['@TSZH_ID'] = $arTszhList;
		unset($this->filter['TSZH_ID']);

		// ������ ���
		if (!$lastId)
		{
			// ���������� ���� ������ ���������� �������
			$lastPayment = \CTszhPayment::GetList(
				Array("DATE_PAYED" => "DESC"),
				$this->filter,
				false,
				array('nTopCount' => 1),
				Array("ID", "DATE_PAYED")
			)->Fetch();
			$date = is_array($lastPayment) ? MakeTimeStamp($lastPayment["DATE_PAYED"]) : false;
			$date = Xml::formatDateTime($date);
			fputs($this->output, "<?xml version=\"1.0\" encoding=\"" . SITE_CHARSET . "\"?>\n<payments inn=\"$inn\" filetype=\"payments\" filedate=\"$date\" version=\"4\">\n");

			$count = \CTszhPayment::GetList(array(), $this->filter, Array("COUNT" => "ID"))->Fetch();
			$this->state['progress'] = Array(
				'total' => $count["ID"],
				'done' => 0,
				'remaining' => $count["ID"],
			);
			$lastId = 0;
		}

		$filter = array_merge($this->filter, Array(">ID" => $lastId));

		/*$dbPayments = \CTszhPayment::GetList(Array("ID" => "ASC"), $filter, false, false, Array(
			"ID",
			"DATE_PAYED",
			"SUMM",
			"ACCOUNT_XML_ID",
			"ACCOUNT_EXTERNAL_ID",
			"PSYS_FEE",
			"PS_ACTION_FILE",
			"C_ACCOUNT",
			"INSURANCE_INCLUDED",
			"IS_OVERHAUL",
			"IS_PENALTY",
		));*/

		$dbPayments = PaymentTable::getList(
			[
				'select' => ['*', 'PS_' => 'PAY_SYSTEM'],
				'filter' => $filter,
				'order' => ['ID' => 'ASC'],
			]
		);

		/** @noinspection PhpAssignmentInConditionInspection */
		while ($payment = $dbPayments->fetch())
		{
			$lastId = $payment["ID"];
			$attr = Array(
				//"acc_id" => $payment["ACCOUNT_EXTERNAL_ID"],
				"acc_id" => $payment["C_ACCOUNT"],
				"date" => Xml::formatDateTime(MakeTimeStamp($payment["DATE_PAYED"])),
				"sum" => $payment["SUMM"],
				"ps" => basename($payment["PS_ACTION_FILE"]),
			);
			if (is_set($payment, "PSYS_FEE") && $payment["PSYS_FEE"] > 0)
			{
				$attr["fee"] = $payment["PSYS_FEE"];
			}
			if (is_set($payment, "INSURANCE_INCLUDED"))
			{
				$attr["insurance_included"] = $payment["INSURANCE_INCLUDED"];
			}
			if (is_set($payment, "IS_OVERHAUL"))
			{
				$attr["is_overhaul"] = $payment["IS_OVERHAUL"] == 'Y' ? 1 : 0;
			}
			if (is_set($payment, "IS_PENALTY"))
			{
				$attr["is_penalty"] = $payment["IS_PENALTY"] == 'Y' ? 1 : 0;
			}
			if (is_set($payment, "EMAIL"))
			{
				$attr["email"] = $payment["EMAIL"];
			}

			$paymentString = "\t<payment";
			foreach ($attr as $k => $v)
			{
				$paymentString .= " $k=\"$v\"";
			}
			//$paymentString .= " />\n";
			$paymentString .= ">\n";

			$dbService = PaymentServicesTable::getList(
				[
					'select' => ['GUID', 'SERVICE_NAME', 'SUMM_PAYED'],
					'filter' => ['PAYMENT_ID' => $payment['ID']],
					'order' => ['ID' => 'asc'],
				]
			);
			if ($dbService->getSelectedRowsCount() > 0)
			{
				$paymentString .= "\t\t<services>\n";
				while ($service = $dbService->fetch())
				{
					if($service['SUMM_PAYED']>0)
					{
						$paymentString .= "\t\t\t<service id=\"{$service['GUID']}\" sum_payed=\"{$service['SUMM_PAYED']}\">\n";
					}
				}
				$paymentString .= "\t\t</services>\n";

			}
			if (\CModule::IncludeModule('otr.sale'))
			{
				$check = \Otr\Sale\Cashbox\Internals\CashboxCheckTable::getList(
					array(
						'order' => array('ID' => 'DESC'),
						'filter' => array('PAYMENT_ID' => $payment['ID']),
						'limit' => 1,

					)
				);
				while ($checkcahbox = $check->fetchAll())
				{
					$check2cahbox = $checkcahbox;
				}
				$cahbox = \Otr\Sale\Cashbox\Internals\CashboxTable::getList(
					array(
						'order' => array('ID' => 'DESC'),
						'filter' => array('ID' => $check2cahbox['0']["CASHBOX_ID"]),
					)
				);
				while ($cahboxs = $cahbox->fetchAll())
				{
					$cahboxCurrent = $cahboxs;
				}
				/*if ($check2cahbox->getSelectedRowsCount() > 0)
				{*/
				/*if (is_set($check2cahbox['0'], "LINK_PARAMS"))
				{*/
				$cattr["doc_date"] = $check2cahbox['0']["LINK_PARAMS"]["doc_time"];

				$cattr["zn_fn"] = $check2cahbox['0']["LINK_PARAMS"]["fn_number"];

				$cattr["zn_kkt"] = $cahboxCurrent['0']["NUMBER_KKM"];
				$cattr["ofd_url"] = $cahboxCurrent['0']["OFD"];
				$cattr["org_inn"] = $inn;
				$cattr["org_sno"] = "";
				$cattr["site_address"] = $_SERVER["SERVER_NAME"];

				$cattr["fp_num"] = $check2cahbox['0']["LINK_PARAMS"]["fiscal_doc_attribute"];

				$cattr["check_id"] = $check2cahbox['0']["LINK_PARAMS"]["fiscal_doc_number"];

				$cattr["doc_type"] = $check2cahbox['0']["TYPE"];

				$cattr["type_cashless "] = "1";

				$cattr["payment_type"] = $check2cahbox['0']["LINK_PARAMS"]["calculation_attribute"];
				//}

				$paymentString .= "\t\t<check";
			foreach ($cattr as $k => $v)
			{
				$paymentString .= " $k=\"$v\"";
			}
				$paymentString .= " />\n";
			//fputs($this->output, $checkString);
		}
		$paymentString .= "\t</payment>\n";
		fputs($this->output, $paymentString);

		$this->state['progress']['done']++;
		$this->state['progress']['remaining']--;
		if ($this->state['stepTime'] && microtime(1) - $startTime >= $this->state['stepTime'])
		{
			break;
		}
	}
		if ($this->state['progress']['remaining'] <= 0)
		{
			$this->state['progress']['remaining'] = 0;
			fputs($this->output, "</payments>");
		}

		return $lastId;
	}
}
