<?
namespace Citrus\Tszh\Exchange\v4;

use Citrus\Tszh\Exchange\ServiceBase;
use \Bitrix\Main\Localization\Loc;
use Citrus\Tszh\Exchange\v4\Formats\MeterValues;
use Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

/**
 * ��������� �������� ��������� ���������
 * ?mode=export&type=meters
 * http://wiki.citrus-soft.ru/dev/export-payments
 *
 * @package Citrus\Tszh\Controller\v4
 */
class ExportMetersService extends ServiceBase
{
	// �������� ��� ����������� �������� �� ������
	private static $multipart = 1;

	public function run()
	{
		$org = $this->facade->getOrg();
		$dateFrom = $this->facade->dateFrom();
		$dateTo = $this->facade->dateTo();

		$filter = Array(
			"TSZH_ID" => $org['ID'],
		);
		$valueFilter = array();
		if ($dateFrom)
		{
			$valueFilter[">=TIMESTAMP_X"] = $dateFrom;
		}
		if ($dateTo)
		{
			$valueFilter["<=TIMESTAMP_X"] = $dateTo;
		}
		if (is_set($_GET, 'owner') && ($owner = $_GET['owner']))
		{
			if (!in_array($owner, array("0", "1"), true))
			{
				throw new \Exception(GetMessage("CITRUS_TSZH_EXCHANGE_WRONG_PARAM", array("#PARAM#" => "owner")));
			}
			$valueFilter['MODIFIED_BY_OWNER'] = $_GET["owner"] == 1 ? "Y" : "N";
		}

		//������� ���������
		$request = Application::getInstance()->getContext()->getRequest();

		$state = &$_SESSION["CITRUS.TSZH.METERS.EXPORT"];
		if (!is_array($state))
		{
			$state = Array(
				"stepTime" => 0,
				"limit" => 500,
				"tszh" => $org,
				'last_id' => 0,
				'multipart' => (int)$request->get('multipart') == (int)self::$multipart/* ? true : false*/, // �������� �� �����
				'success' => false,
				'final' => false,
				'count' => 0,
				'total' => 0,
			);
		}

		$export = new MeterValues($_SESSION["CITRUS.TSZH.METERS.EXPORT"]);
		$export->setOrg($org["ID"])->filter($filter)->valueFilter($valueFilter);
		if (!$state['final'])
		{
			$export->run();
		}
		else
		{
			$state['success'] = true;
		}

		// ������� success ��� �������� �� ������ � ������ �� ������ ���������� �� ������
		if ($state['multipart'] && $state['success'])
		{
			unset($_SESSION["CITRUS.TSZH.METERS.EXPORT"]);
			echo 'success';
		}
		else
		{
			if ($state['multipart'])
			{
				$this->facade->textOutput = true;
			}
			else
			{
				unset($_SESSION["CITRUS.TSZH.METERS.EXPORT"]);
				$this->facade->xmlOutput = true;
			}
		}
	}
}

