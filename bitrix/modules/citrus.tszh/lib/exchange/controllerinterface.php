<?

namespace Citrus\Tszh\Exchange;

/**
 * Interface ControllerInterface
 * @package Citrus\Tszh\Controller
 */
interface ControllerInterface
{
	/**
	 * @param Facade $facade
	 */
	function __construct(Facade $facade);

	/**
	 * @param string $mode
	 * @param string $service
	 * @return bool
	 */
	public function hasService($mode, $service);

	/**
	 * @param string $mode
	 * @param string $service
	 * @return ServiceBase
	 */
	public function getService($mode, $service);

	/**
	 * @param string $mode
	 * @param string $service
	 */
	public function processRequest($mode, $service);
}