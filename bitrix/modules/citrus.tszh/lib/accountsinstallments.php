<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class AccountsInstallmentsTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ACCOUNT_PERIOD_ID int mandatory
 * <li> SERVICE string(255) mandatory
 * <li> SUMM_PAYED double mandatory
 * <li> SUMM_PREV_PAYED double mandatory
 * <li> PERCENT double mandatory
 * <li> SUMM_RATED double mandatory
 * <li> SUMM2PAY double mandatory
 * <li> CONTRACTOR_ID int mandatory
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class AccountsInstallmentsTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_accounts_installments';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('ACCOUNTS_INSTALLMENTS_ENTITY_ID_FIELD'),
			),
			'ACCOUNT_PERIOD_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_INSTALLMENTS_ENTITY_ACCOUNT_PERIOD_ID_FIELD'),
			),
			'SERVICE' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateService'),
				'title' => Loc::getMessage('ACCOUNTS_INSTALLMENTS_ENTITY_SERVICE_FIELD'),
			),
			'SUMM_PAYED' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_INSTALLMENTS_ENTITY_SUMM_PAYED_FIELD'),
			),
			'SUMM_PREV_PAYED' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_INSTALLMENTS_ENTITY_SUMM_PREV_PAYED_FIELD'),
			),
			'PERCENT' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_INSTALLMENTS_ENTITY_PERCENT_FIELD'),
			),
			'SUMM_RATED' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_INSTALLMENTS_ENTITY_SUMM_RATED_FIELD'),
			),
			'SUMM2PAY' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_INSTALLMENTS_ENTITY_SUMM2PAY_FIELD'),
			),
			'CONTRACTOR_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_INSTALLMENTS_ENTITY_CONTRACTOR_ID_FIELD'),
			),
		);
	}

	/**
	 * Returns validators for SERVICE field.
	 *
	 * @return array
	 */
	public static function validateService()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}
}