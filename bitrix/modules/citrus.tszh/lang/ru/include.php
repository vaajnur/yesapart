<?
$MESS["TSZH_ERROR_NO_SITE_ID"] = "Не указана привязка к сайту";
$MESS["TSZH_DELETE_OK"] = "Записи удалены";
$MESS["TSZH_DELETE_ERROR"] = "Ошибка удаления записи.";
$MESS["TSZH_DELETE_ITEM_NOT_FOUND"] = "Элемент с кодом #ID# не найден.";
$MESS["TSZH_GROUP_SAVE_ERROR"] = "Ошибка обновления записи.";
$MESS["TSZH_GROUP_SAVE_ERROR_ITEM_NOT_FOUND"] = "Запись не найдена";
$MESS["TSZH_GROUP_EDIT_TITLE"] = "Изменить";
$MESS["TSZH_GROUP_DELETE_TITLE"] = "Удалить";
$MESS["TSZH_GROUP_DELETE_CONFIRM"] = "Вы действительно хотите удалить эту запись?";
$MESS["TSZH_VIEW_TITLE"] = "Просмотреть";
$MESS["TSZH_ADD_ITEM"] = "Добавить";
$MESS["TSZH_ADD_ITEM_TITLE"] = "Добавить новую запись";
$MESS["TSZH_FIND"] = "Найти";
$MESS["TSZH_FIND_TITLE"] = "Значение для поиска";
$MESS["TSZH_EDIT_PAGE_TITLE"] = "Редактирование записи №#ID#";
$MESS["TSZH_VIEW_PAGE_TITLE"] = "Просмотр записи №#ID#";
$MESS["TSZH_ADD_PAGE_TITLE"] = "Добавление записи";
$MESS["TSZH_EDIT_ITEM_NOT_FOUND"] = "Запись не найдена";
$MESS["TSZH_EDIT_ERRORS"] = "Произошли следующие ошибки";
$MESS["TSZH_ITEMS_LIST"] = "Список";
$MESS["TSZH_ITEMS_LIST_TITLE"] = "Список записей";
$MESS["TSZH_EDIT_SETTING"] = "Настройки";
$MESS["TSZH_EDIT_SETTING_TITLE"] = "Настрока полей формы";
$MESS["TSZH_ERROR_NO_NAME"] = "Не указано название";
$MESS["TSZH_FILTER_FROM"] = "с";
$MESS["TSZH_FILTER_TO"] = "по";
$MESS["TSZH_FILTER_LIST_ALL"] = "(все)";
$MESS["TSZH_YES"] = "Да";
$MESS["TSZH_NO"] = "Нет";
?>