<?
$MESS["TSZH_ERROR_NO_SITE_ID"] = "Не указана привязка к сайту";
$MESS["TSZH_ERROR_WRONG_SITE_ID"] = "Сайт не существует";
$MESS["TSZH_ERROR_NO_NAME"] = "Не указано название ТСЖ";
$MESS["TSZH_ERROR_WRONG_SUBSCRIBE_DOLG_DATE"] = "Неверно указана дата отправки уведомлений о задолженности на вкладке «Рассылки» (число месяца)";
$MESS["TSZH_ERROR_WRONG_SUBSCRIBE_METERS_DATE"] = "Неверно указана дата отправки уведомлений о необходимости ввода показаний счетчиков на вкладке «Рассылки» (число месяца)";
$MESS["TSZH_ERROR_WRONG_METER_VALUES_START_DATE"] = "Неверно указана дата начала периода, в котором разрешен ввод показаниий счетчиков на вкладке «Рассылки» (число месяца)";
$MESS["TSZH_ERROR_WRONG_METER_VALUES_END_DATE"] = "Неверно указана дата окончания периода, в котором разрешен ввод показаниий счетчиков на вкладке «Рассылки» (число месяца)";
$MESS["TSZH_ERROR_WRONG_EMAIL"] = "E-Mail указан не верно";
$MESS["CITRUS_TSZH_ERORR_NOTIFY"] = "Значения некоторых полей #NAME# указаны не верно. Возможны проблемы с проведением платежей. <a href=\"/bitrix/admin/tszh_edit.php?ID=#ID#&lang=ru\">Исправить</a>";
$MESS["CITRUS_TSZH_ERROR_EMPTY_FIELD"] = "Не заполнено поле &laquo;<i>#FIELD#</i>&raquo;.";
$MESS["CITRUS_TSZH_ERROR_DEFAULT_FIELD"] = "В поле &laquo;<i>#FIELD#</i>&raquo; указано значение по умолчанию.";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_IP_INN"] = "<i>ИНН</i> должен содержать 12 цифр";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_INN"] = "<i>ИНН</i> должен содержать 10 цифр";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_KPP"] = "<i>КПП</i> должен содержать 9 цифр";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_RSCH"] = "<i>Расчетный счет</i> должен содержать 20 цифр";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_BIK"] = "<i>БИК</i> должен содержать 9 цифр";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_INN_CHECK"] = "<i>ИНН</i> указан не верно";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_RSCH_CHECK"] = "<i>Расчетный счет</i> и/или <i>БИК</i> указаны не верно, проверьте правильность значений в этих полях";
$MESS["CITRUS_TSZH_ERROR_EMAIL_SUBJECT"] = "Пожалуйста, проверьте реквизиты вашей организации ЖКХ на сайте";
$MESS["CITRUS_TSZH_ERROR_EMAIL_BODY"] = "Здравствуйте.

Реквизиты одной или нескольких организаций на сайте указаны не верно.
Вы можете перейти по ссылке на сайт и исправить указанные поля.

";
$MESS["CITRUS_TSZH_ERROR_INVALID_SITE_URL"] = "В настройках сайта указан некорректный <i>URL сервера</i> (<a href=\"#URL#\" target=\"_blank\">исправить</a>)";
$MESS["CITRUS_TSZH_ERROR_INVALID_URL"] = "В настройках Главного модуля указан некорректный <i>URL сайта</i> (<a href=\"#URL#\" target=\"_blank\">исправить</a>)";
$MESS["CITRUS_TSZH_ERROR_WRONG_URL"] = "Указанный адрес (<i>#URL#</i>) не соответствует текущему (<i>#CURRENT_URL#</i>).<br>Пожалуйста, проверьте правильность этой настройки.";
$MESS["CITRUS_TSZH_ERROR_DEMO_DATA"] = "В списке лицевых счетов содержатся демонстрационные данные.<br>Рекомендуем вам удалить их и провести загрузку из учетной программы.";

$MESS["CITRUS_TSZH_F_NAME"] = "Название";
$MESS["CITRUS_TSZH_F_INN"] = "ИНН";
$MESS["CITRUS_TSZH_F_KPP"] = "КПП";
$MESS["CITRUS_TSZH_F_RSCH"] = "Расчетный счет";
$MESS["CITRUS_TSZH_F_BANK"] = "Банк";
$MESS["CITRUS_TSZH_F_BIK"] = "БИК";
$MESS["CITRUS_TSZH_F_MONETA_EMAIL"] = "E-mail для взаимодействия с сервисом Монета.ру";
$MESS["CITRUS_TSZH_F_EMAIL"] = "E-mail организации для отображения в контактах";
$MESS["CITRUS_TSZH_F_HEAD_NAME"] = "ФИО руководителя";
$MESS["CITRUS_TSZH_F_LEGAL_ADDRESS"] = "Юридический адрес";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_MONETA_EMAIL"] = "E-mail для взаимодействия с сервисом Монета.ру указан не верно";

?>