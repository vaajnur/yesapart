<?
$MESS["CITRUS_TSZH_SUBSCRIBE_MIN_EDITION"] = "Стандарт";

$MESS["CITRUS_TSZH_SUBSCRIBE_ATTACHED_RECEIPT"] = "Квитанция находится во вложении к этому письму.";
$MESS["CITRUS_TSZH_SUBSCRIBE_PDF"] = "Рассылка квитанций в PDF";
$MESS["CITRUS_TSZH_SUBSCRIBE_PDF_ERROR"] = "При отправке произошла ошибка";
$MESS["CITRUS_TSZH_SUBSCRIBE_PDF_DOM_NOT_LOADED"] = "Не загружено расширение <a href=\"http://php.net/manual/ru/book.dom.php\" target=\"_blank\">DOM</a>";
$MESS["CITRUS_TSZH_SUBSCRIBE_PDF_GD_NOT_LOADED"] = "Не загружено расширение <a href=\"http://php.net/manual/ru/book.image.php\" target=\"_blank\">GD</a>";
$MESS["CITRUS_TSZH_SUBSCRIBE_PDF_ERRORS"] = "Рассылка в формате PDF недоступна. Для её работы необходимо наличие дополнительных модулей PHP.<br>Пожалуйста, обратитесь к своему хостинг-провайдеру или системному администратору.<br>";

$MESS["CITRUS_TSZH_SUBSCRIBE_ERROR_ADD_SUBSCRIBE"] = 'Ошибка: не удалось добавить рассылку [#SUBSCRIBE_CODE#] "#SUBSCRIBE_NAME#" для организации [#TSZH_ID#] "#TSZH_NAME#". Данные рассылки: #SUBSCRIBE_ROW#.';

$MESS["CITRUS_TSZH_SUBSCRIBE_VDGB_PHONE_MOSCOW_NAME"] = "тел. Москва, М.О.";
$MESS["CITRUS_TSZH_SUBSCRIBE_VDGB_PHONE_REGIONS_NAME"] = "тел. Регионы";

$MESS["CITRUS_TSZH_SUBSCRIBE_FOR"] = "для";

$MESS["CITRUS_TSZH_SUBSCRIBE_addCustomEvent_ERROR_UNSUPPORTED_LANGUAGE"] = "Ошибка: неподдерживаемый язык: #LANG#";
$MESS["CITRUS_TSZH_SUBSCRIBE_addCustomEvent_ERROR_UNKNOWN_EVENT"] = "Ошибка: неизвестный тип почтового события: #EVENT_TYPE#";
$MESS["CITRUS_TSZH_SUBSCRIBE_addCustomEvent_ERROR_ADD_EVENT"] = "Ошибка при добавлении типа почтового события #EVENT_TYPE#: #ERROR#";
$MESS["CITRUS_TSZH_SUBSCRIBE_addCustomEvent_ERROR_ADD_EVENT"] = "Ошибка при добавлении шаблона почтового события #EVENT_TYPE#: #ERROR#";

$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_ID_FIELD"] = "Id";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_CODE_FIELD"] = "Код";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_TSZH_ID_FIELD"] = "Id ТСЖ";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_ACTIVE_FIELD"] = "Активность";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_RUNNING_FIELD"] = "Флаг выполнения";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_PARAMS_FIELD"] = "Параметры";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_LAST_EXEC_FIELD"] = "Дата последнего запуска";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_NEXT_EXEC_FIELD"] = "Дата следующего запуска";

$MESS["CITRUS_TSZH_UNSUBSCRIPTION_ENTITY_SUBSCRIBE_CODE_FIELD"] = "Код рассылки";
$MESS["CITRUS_TSZH_UNSUBSCRIPTION_ENTITY_USER_ID_FIELD"] = "Id пользователя";

$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_DESCRIPTION"] = "Выполнить рассылку";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_VALUE_YES"] = "Да";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_VALUE_NO"] = "Нет";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_TITLE"] = "При установленной галочке рассылка начнётся после сохранения элемента\n(рассылка доступна, начиная с редакции «#EDITION#» продукта «#PRODUCT#»)";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS"] = "Статус";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_EMPTY"] = "не рассылалась";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_SENDING"] = "рассылается";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_SENT"] = "разослана";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_INTERRAPTED"] = "прервана";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_ERROR"] = "ошибка";
