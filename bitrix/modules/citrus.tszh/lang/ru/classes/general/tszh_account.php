<?
$MESS["TSZH_HOUSE_FLAT_ABBR"] = "кв.";
$MESS["TSZH_ERROR_NO_USER"] = "Не указан пользователь.";
$MESS["TSZH_ERROR_EXTERNAL_XML_ID_DUPLICATED"] = "Лицевой счет с таким внешним кодом уже существует.";
$MESS["TSZH_ERROR_NO_TSZH_ID"] = "Не указана привязка к ТСЖ";
$MESS["CITRUS_TSZH_ACCOUNT_ERROR_WRONG_TYPE"] = "Указан некорректный тип лицевого счета (#TYPE#)";

$MESS["TSZH_ACCOUNT_DELETE_METERS"] = "- удалены счетчики (#COUNT#);\n";
$MESS["TSZH_ACCOUNT_DELETE_HISTORY"] = "- удалена история;\n";
$MESS["TSZH_ACCOUNT_DELETE_PERIODS"] = "- удалены квитанции (#COUNT#);\n";
$MESS["TSZH_ACCOUNT_DELETE"] = "Удаление лицевого счета №#XML_ID# (ID: #ID#, EXTERNAL_ID: #EXTERNAL_ID#):\n";
$MESS["TSZH_ACCOUNT_DELETE_USER"] = "- удален пользователь (#USER_ID#);\n";
$MESS["TSZH_ACCOUNT_DELETE_UF_FIELDS"] = "- удалены пользовательские поля;\n";
$MESS["TSZH_ACCOUNT_PRIVATE"] = "Дом";