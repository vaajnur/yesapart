<?
$MESS["TSZH_ERROR_CONTRACTORS_NO_TSZH_ID"] = "Не указан объект управления";
$MESS["TSZH_ERROR_CONTRACTORS_NO_NAME"] = "Не указано наименование организации-поставщика";
$MESS["TSZH_ERROR_AC_NEG_SUMM"] = "Сумма по поставщику не может быть отрицательной";
$MESS["TSZH_ERROR_CONTRACTORS_WRONG_EXECUTOR"] = "Не верно указано значение поля «Исполнитель» (EXECUTOR)";
