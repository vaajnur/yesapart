<?
$MESS["Common-ERROR-PARAM-MISSING"] = "Не задано значение параметра «#PARAM#»";

$MESS["Receipts-NAME"] = "Квитанции";
$MESS["Receipts-NOTE-ADMIN"] = "Квитанции рассылаются собственникам по мере их добавления на сайт (или загрузки из 1С или другой учётной системы).";
$MESS["Receipts-DESCR-PUBLIC"] = "Высылается письмо с квитанцией на оплату услуг ЖКХ.";
$MESS["Receipts-SETTING-ACTIVE-NAME"] = "Активна";
$MESS["Receipts-SETTING-ACTIVE-NAME-WIZARD"] = "Рассылать квитанции на оплату по электронной почте";
$MESS["Receipts-PARAM-SEND_ONLY_CURRENT_PERIOD-NAME"] = "Рассылать квитанции только за текущий период*";
$MESS["Receipts-PARAM-SEND_ONLY_CURRENT_PERIOD-NOTE"] = "* Если этот параметр отключен, то будет происходить рассылка всех загружаемых квитанций на сайт. Не смотря на то, какой период загружается в данный момент.";
$MESS["Receipts-PARAM-SEND_PDF-NAME"] = "Рассылать в формате PDF**";
$MESS["Receipts-PARAM-SEND_PDF-ERROR"] = "** Рассылка в формате PDF недоступна. Для её работы необходимо наличие дополнительных модулей PHP.<br>Пожалуйста, обратитесь к своему хостинг-провайдеру или системному администратору.<br>";
$MESS["Receipts-PARAM-SEND_PDF-NOTE"] = "** <span style=\"color: red;\">Внимание!</span> Функция рассылки квитанций в PDF является экспериментальной, создает повышенную нагрузку на сервер и может приводить к формированию файлов с неточным отображением фрагментов квитанции.<br><br>Необходимо <a href=\"https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=37&LESSON_ID=5507\" target=\"_blank\">настроить работу агентов по cron</a>, а также адаптировать шаблоны квитанций, проверить внешний вид формирующихся PDF-файлов перед использованием этой функции.";
$MESS["Receipts-PARAM-LAST_SENT_DATE-NAME"] = "Дата отправки последнего письма";
$MESS["Receipts-SETTING-LAST_EXEC-NAME"] = "Время последнего запуска";
$MESS["Receipts-SETTING-LAST_EXEC-NAME-WIZARD"] = "Время последнего запуска";
$MESS["Receipts-SETTING-NEXT_EXEC-NAME"] = "Время следующего запуска";
$MESS["Receipts-SETTING-NEXT_EXEC-NAME-WIZARD"] = "Время следующего запуска";
