<?
$MESS["Common-ERROR-PARAM-MISSING"] = "Не задано значение параметра «#PARAM#»";

$MESS["MeterVerifications-NAME"] = "Напоминания о поверке счётчиков";
$MESS["MeterVerifications-NOTE-ADMIN"] = "За указанное количество дней до наступления даты поверки счётчика его владельцу отправится уведомление с напоминанием о ней.";
$MESS["MeterVerifications-DESCR-PUBLIC"] = "При приближении даты поверки счётчика высылается письмо с напоминанием о ней.";
$MESS["MeterVerifications-SETTING-ACTIVE-NAME"] = "Активна";
$MESS["MeterVerifications-SETTING-ACTIVE-NAME-WIZARD"] = "Рассылать напоминания о дате поверки счётчиков";
$MESS["MeterVerifications-PARAM-CHECK_INTERVAL-NAME"] = "За сколько дней до наступления даты поверки следует высылать напоминание";
$MESS["MeterVerifications-PARAM-CHECK_INTERVAL-ERROR-INVALID"] = "Значение параметра «#PARAM#» должно быть больше нуля";
$MESS["MeterVerifications-SETTING-LAST_EXEC-NAME"] = "Время последнего запуска";
$MESS["MeterVerifications-SETTING-LAST_EXEC-NAME-WIZARD"] = "Время последнего запуска";
$MESS["MeterVerifications-SETTING-NEXT_EXEC-NAME"] = "Время следующего запуска";
$MESS["MeterVerifications-SETTING-NEXT_EXEC-NAME-WIZARD"] = "Время следующего запуска";