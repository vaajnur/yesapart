<?
$MESS["Common-ERROR-PARAM-MISSING"] = "Не задано значение параметра «#PARAM#»";

$MESS["Debtors-NAME"] = "Уведомления о задолженности";
$MESS["Debtors-NOTE-ADMIN"] = "В указанный день месяца владельцам лицевых счетов, имеющим задолженность, высылаются письма с уведомлением о ней.";
$MESS["Debtors-DESCR-PUBLIC"] = "При наличии задолженности, превышающей определённый размер, высылается письмо с уведомлением о ней.";
$MESS["Debtors-SETTING-ACTIVE-NAME"] = "Активна";
$MESS["Debtors-SETTING-ACTIVE-NAME-WIZARD"] = "Рассылать уведомления должникам";
$MESS["Debtors-PARAM-DATE-NAME"] = "День месяца для рассылки";
$MESS["Debtors-PARAM-DATE-ERROR-RANGE"] = "Значение параметра «#PARAM#» должно быть в диапазоне от 1 до 31 включительно";
$MESS["Debtors-PARAM-MIN_SUM-NAME"] = "Минимальная сумма задолженности для отправки уведомления, руб.";
$MESS["Debtors-PARAM-MIN_SUM-ERROR-MIN"] = "Значение параметра «#PARAM#» должно быть не менее одной копейки (0.01)";
$MESS["Debtors-SETTING-LAST_EXEC-NAME"] = "Время последнего запуска";
$MESS["Debtors-SETTING-LAST_EXEC-NAME-WIZARD"] = "Время последнего запуска";
$MESS["Debtors-SETTING-NEXT_EXEC-NAME"] = "Время следующего запуска";
$MESS["Debtors-SETTING-NEXT_EXEC-NAME-WIZARD"] = "Время следующего запуска";


$MESS["MeterValues-NAME"] = "Напоминания о необходимости передачи показаний счётчиков";
$MESS["MeterValues-NOTE-ADMIN"] = "В указанный день месяца владельцам лицевых счетов отправляются уведомления с напоминанием о необходимости ввести показания счётчиков на сайте (если они не введены).";
$MESS["MeterValues-DESCR-PUBLIC"] = "При отсутствии показаний счётчиков высылается письмо с напоминанием о необходимости ввести их на сайте.";
$MESS["MeterValues-SETTING-ACTIVE-NAME"] = "Активна";
$MESS["MeterValues-SETTING-ACTIVE-NAME-WIZARD"] = "Рассылать уведомления о необходимости ввода показаний счётчиков";
$MESS["MeterValues-PARAM-DATE-NAME"] = "День месяца для рассылки";
$MESS["MeterValues-PARAM-DATE-ERROR-RANGE"] = "Значение параметра «#PARAM#» должно быть в диапазоне от 1 до 31 включительно";
$MESS["MeterValues-PARAM-LAST_PERIOD-NAME"] = "Последний период, по которому отправлены уведомления";
$MESS["MeterValues-SETTING-LAST_EXEC-NAME"] = "Время последнего запуска";
$MESS["MeterValues-SETTING-LAST_EXEC-NAME-WIZARD"] = "Время последнего запуска";
$MESS["MeterValues-SETTING-NEXT_EXEC-NAME"] = "Время следующего запуска";
$MESS["MeterValues-SETTING-NEXT_EXEC-NAME-WIZARD"] = "Время следующего запуска";

$MESS["MeterVerifications-NAME"] = "Напоминания о поверке счётчиков";
$MESS["MeterVerifications-NOTE-ADMIN"] = "За указанное количество дней до наступления даты поверки счётчика его владельцу отправится уведомление с напоминанием о ней.";
$MESS["MeterVerifications-DESCR-PUBLIC"] = "При приближении даты поверки счётчика высылается письмо с напоминанием о ней.";
$MESS["MeterVerifications-SETTING-ACTIVE-NAME"] = "Активна";
$MESS["MeterVerifications-SETTING-ACTIVE-NAME-WIZARD"] = "Рассылать напоминания о дате поверки счётчиков";
$MESS["MeterVerifications-PARAM-CHECK_INTERVAL-NAME"] = "За сколько дней до наступления даты поверки следует высылать напоминание";
$MESS["MeterVerifications-PARAM-CHECK_INTERVAL-ERROR-INVALID"] = "Значение параметра «#PARAM#» должно быть больше нуля";
$MESS["MeterVerifications-SETTING-LAST_EXEC-NAME"] = "Время последнего запуска";
$MESS["MeterVerifications-SETTING-LAST_EXEC-NAME-WIZARD"] = "Время последнего запуска";
$MESS["MeterVerifications-SETTING-NEXT_EXEC-NAME"] = "Время следующего запуска";
$MESS["MeterVerifications-SETTING-NEXT_EXEC-NAME-WIZARD"] = "Время следующего запуска";

$MESS["Receipts-NAME"] = "Квитанции";
$MESS["Receipts-NOTE-ADMIN"] = "Квитанции рассылаются собственникам по мере их добавления на сайт (или загрузки из 1С или другой учётной системы).";
$MESS["Receipts-DESCR-PUBLIC"] = "Высылается письмо с квитанцией на оплату услуг ЖКХ.";
$MESS["Receipts-SETTING-ACTIVE-NAME"] = "Активна";
$MESS["Receipts-SETTING-ACTIVE-NAME-WIZARD"] = "Рассылать квитанции на оплату по электронной почте";
$MESS["Receipts-PARAM-SEND_ONLY_CURRENT_PERIOD-NAME"] = "Рассылать квитанции только за текущий период*";
$MESS["Receipts-PARAM-SEND_ONLY_CURRENT_PERIOD-NOTE"] = "* Если этот параметр отключен, то будет происходить рассылка всех загружаемых квитанций на сайт. Не смотря на то, какой период загружается в данный момент.";
$MESS["Receipts-PARAM-SEND_PDF-NAME"] = "Рассылать в формате PDF**";
$MESS["Receipts-PARAM-SEND_PDF-ERROR"] = "** Рассылка в формате PDF недоступна. Для её работы необходимо наличие дополнительных модулей PHP.<br>Пожалуйста, обратитесь к своему хостинг-провайдеру или системному администратору.<br>";
$MESS["Receipts-PARAM-SEND_PDF-NOTE"] = "** <span style=\"color: red;\">Внимание!</span> Функция рассылки квитанций в PDF является экспериментальной, создает повышенную нагрузку на сервер и может приводить к формированию файлов с неточным отображением фрагментов квитанции.<br><br>Необходимо <a href=\"https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=37&LESSON_ID=5507\" target=\"_blank\">настроить работу агентов по cron</a>, а также адаптировать шаблоны квитанций, проверить внешний вид формирующихся PDF-файлов перед использованием этой функции.";
$MESS["Receipts-PARAM-LAST_SENT_DATE-NAME"] = "Дата отправки последнего письма";
$MESS["Receipts-SETTING-LAST_EXEC-NAME"] = "Время последнего запуска";
$MESS["Receipts-SETTING-LAST_EXEC-NAME-WIZARD"] = "Время последнего запуска";
$MESS["Receipts-SETTING-NEXT_EXEC-NAME"] = "Время следующего запуска";
$MESS["Receipts-SETTING-NEXT_EXEC-NAME-WIZARD"] = "Время следующего запуска";

$MESS["News-ELEMENT-ERROR-addAgent"] = "ошибка: не удалось добавить рассылающий агент";
$MESS["News-ELEMENT-DESCR-sending"] = "рассылается";
$MESS["News-ELEMENT-DESCR-interrupted"] = "рассылка прервана администратором";
$MESS["News-ELEMENT-DESCR-sent"] = "разослана #DATE#";
