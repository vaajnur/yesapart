<?
$MESS["TSZH_ERROR_INSTALLMENTS_NO_ACCOUNT_PERIOD_ID"] = "Не указана квитанция для рассрочки";
$MESS["TSZH_ERROR_INSTALLMENTS_NO_SERVICE"] = "Не указана услуга рассрочки";
$MESS["TSZH_ERROR_INSTALLMENTS_NO_SUMM_PAYED"] = "Не указана сумма оплаты за период рассрочки";
$MESS["TSZH_ERROR_INSTALLMENTS_NO_SUMM_PREV_PAYED"] = "Не указана сумма оплаты за предыдущие периоды рассрочки";
$MESS["TSZH_ERROR_INSTALLMENTS_NO_PERCENT"] = "Не указан процент рассрочки";
$MESS["TSZH_ERROR_INSTALLMENTS_NO_SUMM_RATED"] = "Не указана сумма процентов рассрочки";
$MESS["TSZH_ERROR_INSTALLMENTS_NO_SUMM2PAY"] = "Не указана сумма к оплате за рассрочку";
