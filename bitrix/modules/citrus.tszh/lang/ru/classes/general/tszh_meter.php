<?
$MESS["ERROR_TSZH_METER_WRONG_ACCOUNT"] = "Ошибка, привязка к лицевому счету указана неверно.";
$MESS["ERROR_TSZH_METER_LARGE_DEC_PLACES"] = "Количество знаков после запятой не может быть больше 8.";
$MESS["ERROR_TSZH_METER_LARGE_CAPACITY"] = "Количество разрядов счетчика больше 20 не поддерживается";
$MESS["ERROR_TSZH_METER_INVALID_VERIFICATION_DATE"] = "Указана некорректная дата поверки счётчика";
