<?
$MESS ['TSZH_SERVICE_NO_CHARGE_ID'] = "Не указан ID начисления";
$MESS ['ERROR_TSZH_SERVICE_NO_TSZH'] = "Не указана привязка к ТСЖ. Пожалуйста, проверьте файл выгрузки. 
\r\n Перед следующим обменом необходимо вручную почистить таблицы обмена, используя следующие команды SQL: \r\nTRUNCATE TABLE b_tszh_xml_storage_m;\r\nTRUNCATE TABLE b_tszh_xml_storage; \r\n
Данные команды можно выполнить здесь: Настройки > Инструменты > SQL запрос";
$MESS ['ERROR_TSZH_SERVICE_NO_NAME'] = "Не указано наименование услуги #ATTR#";