<?
$MESS["TSZH_ERROR_CURRENCIES_NO_ID"] = "Не заполнено обязательное поле «Код валюты»";
$MESS["TSZH_ERROR_CURRENCIES_WRONG_ID"] = "Не корректно указано поле «Код валюты». Должна быть указан строка из трех латинских букв (стандарт ISO 4217)";
$MESS["TSZH_ERROR_CURRENCIES_NO_NAME"] = "Не заполнено обязательное поле «Наименование»";
$MESS["TSZH_ERROR_CURRENCIES_NO_SHORT"] = "Не заполнено обязательное поле «Краткое обозначение»";
$MESS["TSZH_ERROR_CURRENCIES_NO_FULL"] = "Не заполнено обязательное поле «Полное обозначение»";
