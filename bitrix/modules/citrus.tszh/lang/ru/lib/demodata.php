<?
$MESS["CITRUS_TSZH_DEMODATA_ERROR_TSZH_ID_IS_NOT_SET"] = "Не указан объект управления";
$MESS["CITRUS_TSZH_DEMODATA_ERROR_PARSE_DATA"] = "Ошибка при получении данных с сервиса: ";
$MESS["CITRUS_TSZH_DEMODATA_DOWNLOAD_BUTTON_TITLE"] = "Загрузить демо-данные";
$MESS["CITRUS_TSZH_DEMODATA_DELETE_BUTTON_TITLE"] = "Удалить демо-данные";
$MESS["CITRUS_TSZH_DEMODATA_NOTE_BLOCK"] = "Демо-данные для выбранного объекта управления отсутствуют:
<ul>
	<li>Поставщики</li>
	<li>Дома (в том числе электронный паспорт, если у Вас установлен модуль 1С: Сайт ЖКХ. Дома в управлении)</li>
	<li>Лицевые счета</li>
	<li>Счетчики и показания</li>
	<li>Услуги</li>
	<li>Квитанции</li>
</ul>
<p>При помощи демо-данных Вы можете быстро наполнить сайт информацией и лучше ознакомиться с его возможностями.</p>
<p>Демо-данные будут помечены специальным образом. Вы сможете их удалить в любой момент, не затронув рабочие.</p>
";
$MESS["CITRUS_TSZH_DEMODATA_NOTE_BLOCK_WIZARD"] = "
	<p style='font-size: 12px'>При помощи демо-данных Вы можете быстро наполнить сайт информацией и лучше ознакомиться с его возможностями.</p>
	<p style='font-size: 12px'>Демо-данные будут помечены специальным образом. Вы сможете их удалить в любой момент, не затронув рабочие.</p>
";
$MESS["CITRUS_TSZH_DEMODATA_DELETE_NOTE_MESSAGE"] = "Для корректной работы сайта, желательно удалить демонстрационные данные";
$MESS["CITRUS_TSZH_DEMODATA_DELETE_NOTE_P"] = "<p>Для выбранного объекта управления ранее были загружены следующие демонстрационные данные:</p>";
$MESS["CITRUS_TSZH_DEMODATA_DEMO_DATA_LIST_ITEM_ENTITY_contractors"] = "<li>Поставщики(#COUNT#)</li>";
$MESS["CITRUS_TSZH_DEMODATA_DEMO_DATA_LIST_ITEM_ENTITY_houses"] = "<li>Дома(#COUNT#)</li>";
$MESS["CITRUS_TSZH_DEMODATA_DEMO_DATA_LIST_ITEM_ENTITY_accounts"] = "<li>Лицевые счета(#COUNT#)</li>";
$MESS["CITRUS_TSZH_DEMODATA_DEMO_DATA_LIST_ITEM_ENTITY_meters"] = "<li>Счетчики(#COUNT#)</li>";
$MESS["CITRUS_TSZH_DEMODATA_DEMO_DATA_LIST_ITEM_ENTITY_services"] = "<li>Услуги(#COUNT#)</li>";
$MESS["CITRUS_TSZH_DEMODATA_DEMO_DATA_LIST_ITEM_ENTITY_receipts"] = "<li>Квитанции(#COUNT#)</li>";
$MESS["CITRUS_TSZH_DEMODATA_DELETE_NOTE_END"] = "<p>Удалить демо-данные можно, нажав на кнопку ниже. Рабочие данные не будут затронуты и останутся без изменений.</p>
<p>Если вы опасаетесь потери данных, можете сделать архивную копию сайта перед удалением.</p>";
$MESS["CITRUS_TSZH_DEMODATA_ERROR_ADDING_CONTRACTORS"] = "Ошибка при добавлении поставщиков";
$MESS["CITRUS_TSZH_DEMODATA_ERROR_ADDING_SERVICES"] = "Ошибка при добавлении услуг";
$MESS["CITRUS_TSZH_DEMODATA_ERROR_ADDING_PERIODS"] = "Ошибка при добавлении периодов";
$MESS["CITRUS_TSZH_DEMODATA_ERROR_ADDING_METERS"] = "Ошибка при добавлении счетчиков";
$MESS["CITRUS_TSZH_DEMODATA_ERROR_ADDING_ACCOUNTS"] = "Ошибка при добавлении лицевых счетов";
$MESS["CITRUS_TSZH_DEMODATA_DEMO_DATA_STATUS_MESSAGE_download"] = "Загрузка демо-данных выполнена успешно";
$MESS["CITRUS_TSZH_DEMODATA_DEMO_DATA_STATUS_MESSAGE_delete"] = "Удаление демо-данных выполнено успешно";
