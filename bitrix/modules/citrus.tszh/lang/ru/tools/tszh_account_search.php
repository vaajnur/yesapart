<?

$MESS ['TSZH_FLT_ID'] = "ID";
$MESS ['TSZH_FLT_ACCOUNT_ID'] = "ID лицевого счета";
$MESS ['TSZH_FLT_USER_ID'] = "Владелец";
$MESS ['TSZH_FLT_XML_ID'] = "Номер лицевого счета";
$MESS ['TSZH_FLT_TSZH_ID'] = "Организация";
$MESS ['TSZH_FLT_NAME'] = "ФИО";
$MESS ['TSZH_FLT_CITY'] = "Город";
$MESS ['TSZH_FLT_DISTRICT'] = "Район";
$MESS ['TSZH_FLT_REGION'] = "Регион";
$MESS ['TSZH_FLT_SETTLEMENT'] = "Населенный пункт";
$MESS ['TSZH_FLT_STREET'] = "Улица";
$MESS ['TSZH_FLT_HOUSE'] = "Дом";
$MESS ['TSZH_FLT_FLAT'] = "Квартира";
$MESS ['TSZH_FLT_AREA'] = "Общая площадь";
$MESS ['TSZH_FLT_LIVING_AREA'] = "Жилая площадь";
$MESS ['TSZH_FLT_PEOPLE'] = "Кол-во проживающих людей";

$MESS ['TSZH_FLT_TSZH_ID'] = "ТСЖ";
$MESS ['TSZH_F_ALL'] = "(все)";

$MESS ['TSZH_PAGE_TITLE'] = "Выбор лицевого счета";
$MESS ['TSZH_FLT_SEARCH'] = "Поиск";
$MESS ['TSZH_FLT_SEARCH_TITLE'] = "Поиск лицевого счета";

$MESS ['TSZH_SELECT'] = "Выбрать";
$MESS ['TSZH_EDIT_ACCOUNT'] = "Перейти к редактированию лицевого счета";



?>