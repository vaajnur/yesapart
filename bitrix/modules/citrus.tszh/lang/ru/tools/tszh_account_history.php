<?
$MESS["AE_VIEW_PROFILE"] = "Перейти к просмотру профиля";
$MESS["AEF_CURRENT"] = "Текущий";
$MESS["AEF_USER_ID"] = "Пользователь";
$MESS["AEF_TSZH_ID"] = "Объект управления";
$MESS["AEF_XML_ID"] = "Номер лицевого счета";
$MESS["AEF_EXTERNAL_ID"] = "Внешний код";
$MESS["AEF_NAME"] = "ФИО владельца";
$MESS["AEF_DISTRICT"] = "Район";
$MESS["AEF_REGION"] = "Регион";
$MESS["AEF_CITY"] = "Город";
$MESS["AEF_SETTLEMENT"] = "Населенный пункт";
$MESS["AEF_STREET"] = "Улица";
$MESS["AEF_HOUSE"] = "Дом";
$MESS["AEF_FLAT"] = "Номер квартиры";
$MESS["AEF_FLAT_ABBR"] = "Помещение (с обозначением)";
$MESS["AEF_AREA"] = "Общая площадь";
$MESS["AEF_C_AREA"] = "Общая площадь";
$MESS["AEF_L_AREA"] = "Жилая площадь";
$MESS["AEF_LIVING_AREA"] = "Жилая площадь";
$MESS["AEF_PEOPLE"] = "Количество проживающих людей";
$MESS["AEF_USER_FIELDS"] = "Дополнительные поля";
$MESS["AEF_TIMESTAMP_X"] = "Дата";
$MESS["AEF_FIELDS"] = "Новые значения полей";
$MESS["AEF_HISTORY_RECORDS"] = "История изменений";
$MESS["AL_CNT_TOTAL"] = "Всего";
$MESS["AL_CNT_SELECTED"] = "Выбрано";
$MESS["AEF_DELETE_ERROR"] = "Ошибка удаления";
$MESS["AEF_DELETE_ERROR_PERMISSION"] = "Недостаточно прав";
?>