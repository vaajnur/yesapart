<?

$MESS ['TS_ERROR_DELETE'] = "Ошибка удаления услуги #ID#";
$MESS ['TS_ERROR_UPDATE'] = "Ошибка изменения услуги #ID#";
$MESS ['TS_NAV_TITLE'] = "Услуги";
$MESS ['TS_F_ACTIVE'] = "Активность";
$MESS ['TS_F_XML_ID'] = "Символьный код";
$MESS ['TS_F_NAME'] = "Наименование";
$MESS ['TS_F_NORM'] = "Норма";
$MESS ['TS_F_UNITS'] = "Ед. изм.";
$MESS ['TS_F_TARIFF'] = "Дневное показание";
$MESS ['TS_F_TARIFF2'] = "Ночное показание";
$MESS ['TS_F_TARIFF3'] = "Пиковое показание";
$MESS ['TS_F_TSZH'] = "Объект управления";
$MESS ['TS_YES'] = "Да";
$MESS ['TS_NO'] = "Нет";
$MESS ['TS_ALL'] = "все";
$MESS ['TS_M_EDIT'] = "Редактировать";
$MESS ['TS_M_DELETE'] = "Удалить";
$MESS ['TS_M_DELETE_CONFIRM'] = "Вы действительно хотите удалить услугу?";
$MESS ['TS_CNT_TOTAL'] = "Всего";
$MESS ['TS_CNT_SELECTED'] = "Выбрано";
$MESS ['TS_C_ADD_SERVICE'] = "Добавить услугу";
$MESS ['TS_C_ADD_SERVICE_TITLE'] = "Добавить услугу";
$MESS ['TS_PAGE_TITLE'] = "Услуги";
$MESS ['TS_F_ID'] = "ID услуги";
$MESS ['TS_F_ACTIVE'] = "Активность";
$MESS ['TS_F_NAME'] = "Наименование";
$MESS ['TS_FIND'] = "Найти";
$MESS ['TS_CURRENCY'] = "руб.";
$MESS ['TS_VIEW_TSZH'] = "Просмотр данных ТСЖ";
?>