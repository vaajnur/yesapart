<?
$MESS["TSZH_ERROR_UPDATE"] = "Ошибка при обновлении объекта управления";
$MESS["TSZH_ERROR_UPDATE_NOT_FOUND"] = "Ошибка при обновлении, объект управления не найдено";
$MESS["TSZH_ERROR_DELETE"] = "Ошибка при удалении объекта управления";
$MESS["TSZH_NAV"] = "Объекты управления";
$MESS["TSZH_F_NAME"] = "Название";
$MESS["TSZH_F_SITE_ID"] = "Сайт";
$MESS["TSZH_F_CODE"] = "Код";
$MESS["TSZH_F_TIMESTAMP"] = "Дата изменения";
$MESS["TSZH_F_INN"] = "ИНН";
$MESS["TSZH_F_KPP"] = "КПП";
$MESS["TSZH_F_RSCH"] = "Расчетный счет";
$MESS["TSZH_F_BANK"] = "Банк";
$MESS["TSZH_F_KSCH"] = "Кассовый счет";
$MESS["TSZH_F_BIK"] = "БИК";
$MESS["TSZH_F_ACCOUNTS"] = "Лицевые счета";
$MESS["TSZH_F_SERVICES"] = "Услуги";
$MESS["TSZH_F_PERIODS"] = "Периоды";
$MESS["TSZH_F_METER_VALUES_START_DATE"] = "Начало периода ввода показаний";
$MESS["TSZH_F_METER_VALUES_END_DATE"] = "Окончание периода ввода показаний";
$MESS["TSZH_M_EDIT"] = "Изменить";
$MESS["TSZH_M_COPY"] = "Копировать";
$MESS["TSZH_M_ACCOUNTS"] = "Лицевые счета";
$MESS["TSZH_M_PERIODS"] = "Периоды";
$MESS["TSZH_M_DELETE"] = "Удалить";
$MESS["TSZH_M_DELETE_CONFIRM"] = "Вы действительно хотите удалить объект управления и все его лицевые счета?";
$MESS["TSZH_CNT_TOTAL"] = "Всего";
$MESS["TSZH_CNT_SELECTED"] = "Выделено";
$MESS["TSZH_C_ADD"] = "Добавить";
$MESS["TSZH_PAGE_TITLE"] = "Список";
$MESS["TSZH_FIND"] = "Найти";
$MESS["TSZH_F_ALL"] = "(все)";
$MESS["TSZH_ACCOUNTS_LIST"] = "Перейти к просмотру лицевых счетов";
$MESS["TSZH_SERVICES_LIST"] = "Перейти к просмотру услуг";
$MESS["TSZH_PERIODS_LIST"] = "Перейти к просмотру периодов";
?>