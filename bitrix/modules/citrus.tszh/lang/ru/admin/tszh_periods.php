<?

$MESS ['TP_ERROR_UPDATE'] = "Ошибка при обновлении периода";
$MESS ['TP_ERROR_UPDATE_NOT_FOUND'] = "Ошибка при обновлении, строка не существует";
$MESS ['TP_ERROR_DELETE'] = "Ошибка удаления";
$MESS ['TP_PERIODS_NAV'] = "Периоды";
$MESS ['TP_F_ACTIVE'] = "Активность";
$MESS ['TP_F_ONLY_DEBT'] = "Без начислений";
$MESS ['TP_F_PERIOD'] = "Период";
$MESS ['TP_F_DATE'] = "Дата периода";
$MESS ['TP_F_TIMESTAMP_X'] = "Дата изменения";
$MESS ['TP_F_ACCOUNTS'] = "Лицевые счета";
$MESS ['TP_F_TSZH_ID'] = "Объект управления";
$MESS ['TP_F_ALL'] = "(все)";
$MESS ['TP_ACCOUNTS_LIST'] = "список";
$MESS ['TP_M_EDIT'] = "Редактировать";
$MESS ['TP_M_ACCOUNTS'] = "Лицевые счета";
$MESS ['TP_M_DELETE'] = "Удалить";
$MESS ['TP_M_DELETE_CONFIRM'] = "Вы действительно хотите удалить период?";
$MESS ['TP_CNT_TOTAL'] = "Всего";
$MESS ['TP_CNT_SELECTED'] = "Выбрано";

$MESS ['TP_YES'] = "Да";
$MESS ['TP_NO'] = "Нет";
$MESS ['TP_ALL'] = "все";

$MESS ['TP_C_ADD_PERIOD'] = "Добавить период";
$MESS ['TP_C_ADD_PERIOD_TITLE'] = "Добавление периода";
$MESS ['TP_PAGE_TITLE'] = "Периоды";
$MESS ['TP_FIND'] = "Найти";

?>