<?
$MESS ['TPE_TAB1'] = "Период";
$MESS ['TPE_TAB1_TITLE'] = "Период";
$MESS ['TPE_NO_PERMISSIONS'] = "Недостаточно прав для редактирования периодов";
$MESS ['TPE_ERROR_NOT_FOUND'] = "Период #ID# не найден";
$MESS ['TPE_ERROR_WRONG_DATE'] = "Дата периода введена неверно";
$MESS ['TPE_ERROR_SAVE'] = "При сохранении периода произошла ошибка";
$MESS ['TPE_PAGE_TITLE_EDIT'] = "Изменение периода ##ID#";
$MESS ['TPE_PAGE_TITLE_ADD'] = "Добавление периода";
$MESS ['TPE_ERRORS_HAPPENED'] = "Произошли следующие ошибки";
$MESS ['TPE_M_PERIODS_LIST'] = "Список периодов";
$MESS ['TPE_M_PERIODS_LIST_TITLE'] = "Список периодов";
$MESS ['TPE_M_PERIODS_ADD'] = "Добавить период";
$MESS ['TPE_M_PERIODS_ADD_TITLE'] = "Добавить период";
$MESS ['TPE_M_PERIODS_DELETE'] = "Удалить период";
$MESS ['TPE_M_PERIODS_DELETE_CONFIRM'] = "Вы действительно хотите удалить период?";
$MESS ['TPE_M_SETTINGS'] = "Настроить";
$MESS ['TPE_M_SETTINGS_TITLE'] = "Настроить поля формы";
$MESS ['TPE_F_TIMESTAMP_X'] = "Дата последнего изменения";
$MESS ['TPE_F_DATE'] = "Дата периода";
$MESS ['TPE_F_TSZH_ID'] = "Объект управления";
$MESS ['TPE_F_ACTIVE'] = "Период активен";
$MESS ['TPE_F_ONLY_DEBT'] = "Без начислений";
$MESS ['TPE_ONLY_DEBT_DESCR'] = "Если галочка установлена, начисления и квитанция по периоду не будут отображаться жильцам.<br />Можно использовать для предваритальной публикации задолженности, до подготовки квитанций.";

?>