<?
$MESS["TSZH_EDIT_TAB1"] = "Объект управления";
$MESS["TSZH_EDIT_TAB1_TITLE"] = "Информация об объекте управления";
$MESS["TSZH_EDIT_TAB2"] = "Рассылки";
$MESS["TSZH_EDIT_TAB2_TITLE"] = "Настройки рассылок";
$MESS["TE_ERROR_NO_PERMISSIONS"] = "Недостаточно прав для редактирования объекта управления";
$MESS["TE_ERROR_TSZH_NOT_FOUND"] = "Объект управления не найден";
$MESS["TE_ERROR_SAVE"] = "Ошибка сохранения";
$MESS["TE_TITLE_EDIT"] = "Редактирование объекта управления №#ID#";
$MESS["TE_TITLE_ADD"] = "Добавление объекта управления";
$MESS["TE_ERROR_NO_ADD_PERMISSIONS"] = "Недостаточно прав для добавления объекта управления";
$MESS["TE_ERROR_HAPPENED"] = "Произошли следующие ошибки";
$MESS["TE_M_ACCOUNT_LIST"] = "Список объектов управления";
$MESS["TE_M_ACCOUNT_LIST_TITLE"] = "Перейти к списку объектов управления";
$MESS["TE_M_ADD_TSZH"] = "Добавить объект управления";
$MESS["TE_M_ADD_TSZH_TITLE"] = "Добавить объект управления";
$MESS["TE_M_DELETE_TSZH"] = "Удалить";
$MESS["TE_M_DELETE_TSZH_CONFIRM"] = "Вы действительно хотитие удалить объект управления и все его лицевые счета?";
$MESS["TE_FORM_SETTING"] = "Настройки";
$MESS["TE_FORM_SETTING_TITLE"] = "Настройки формы";
$MESS["TEF_USER_FIELDS"] = "Дополнительные поля";
$MESS["TEF_TIMESTAMP_X"] = "Дата последнего изменения";
$MESS["TEF_NAME"] = "Название";
$MESS["TEF_SITE_ID"] = "Сайт";
$MESS["TEF_CODE"] = "Символьный код";
$MESS["TEF_CONFIRM_PARAM"] = "Включить получение согласия пользователей на обработку персональных данных";
$MESS["TEF_CONFIRM_TEXT"] = "Текст согласия";
$MESS["TEF_CONFIRM_HELP"] = "Ниже вы можете внести свой текст для получения согласия с жильцов.</br>";
$MESS["TEF_CONFIRM_HELP_TEXT"] = "Например:</br><i>Я согласен(на) и даю право  \"НАЗВАНИЕ ОРГАНИЗАЦИИ\" хранить и обрабатывать</br> направленные мною в электронном виде персональные данные</br> с соблюдением требований российского законодательства о персональных данных.</i>";
$MESS["TEF_CONFIRM_INFO"] = "Информация для согласия на обработку персональных данных";
$MESS["TE_M_CONFIRM_TEXT"] = "не заполнен текст согласия на ОПД</br>";
$MESS["TEF_INN"] = "ИНН";
$MESS["TEF_KPP"] = "КПП";
$MESS["TEF_KBK"] = "КБК";
$MESS["TEF_OKTMO"] = "ОКТМО";
$MESS["TEF_RSCH"] = "Расчетный счет";
$MESS["TEF_BANK"] = "Банк";
$MESS["TEF_KSCH"] = "Корреспондентский счет";
$MESS["TEF_BIK"] = "БИК";
$MESS["CITRUS_TSZH_IS_BUDGET"] = "Бюджетное учреждение";
$MESS["CITRUS_TSZH_IS_BUDGET_ANNOTATION"] = "Необходимо заполнить <b>КБК</b> и <b>ОКТМО</b> для объекта управления, если оплата услуг производится на расчетный счет бюджетной организаций";
$MESS["TEF_ORGANIZATION_TYPE"] = "Тип организации";
$MESS["TEF_SOLE_TRADER"] = "Индивидуальный предприниматель";
$MESS["TEF_LEGAL_PERSONALITY"] = "Юридическое лицо";
$MESS["TEF_BANK_TITLE"] = "Реквизиты организации";
$MESS["TEF_ADDITIONAL"] = "Дополнительная информация";
$MESS["TEF_HEAD_NAME"] = "ФИО руководителя";
$MESS["TEF_LEGAL_ADDRESS"] = "Юридический адрес";
$MESS["TEF_SUBSCRIBE_ACTIVE"] = "Активна";
$MESS["TEF_SUBSCRIBE_SEND_PDF"] = "Рассылать в формате PDF";
$MESS["TEF_SUBSCRIBE_SEND_PDF_WARNING"] = "<span style=\"color: red;\">Внимание!</span> Функция рассылки квитанций в PDF является экспериментальной, создает повышенную нагрузку на сервер и может приводить к формированию файлов с неточным отображением фрагментов квитанции.<br><br>Необходимо <a href=\"https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=37&LESSON_ID=5507\" target=\"_blank\">настроить работу агентов по cron</a>, а также адаптировать шаблоны квитанций, проверить внешний вид формирующихся PDF-файлов перед использованием этой функции.";
$MESS["TEF_SUBSCRIBE_DATE"] = "День месяца для рассылки";
$MESS["TEF_SUBSCRIBE_LAST_PERIOD"] = "Дата отправки последнего письма";
$MESS["TEF_SUBSCRIBE_DOLG_TITLE"] = "Рассылка уведомлений должникам";
$MESS["TEF_SUBSCRIBE_METERS_TITLE"] = "Рассылка уведомлений о необходимости ввода показаний счетчиков";
$MESS["TEF_SUBSCRIBE_RECEIPT_TITLE"] = "Рассылка квитанций владельцам лицевых счетов";
$MESS["TEF_SUBSCRIBE_DOLG_MIN_SUM"] = "Минимальная сумма задолженности для отправки уведомления";
$MESS["TEF_RECEIPT_INFO"] = "Информация для квитанций";
$MESS["TEF_ADDRESS"] = "Фактический адрес";
$MESS["TEF_EMAIL"] = "E-mail организации для отображения в контактах";
$MESS["TEF_MONETA_EMAIL"] = "E-mail для взаимодействия с сервисом Монета.ру";
$MESS["TEF_PHONE"] = "Телефон, факс";
$MESS["TEF_PHONE_DISP"] = "Диспетчерская";
$MESS["TEF_RECEIPT_TEMPLATE"] = "Шаблон квитанций";
$MESS["TEF_METER_VALUES_PERIOD"] = "Период, в котором разрешен ввод показаний счетчиков";
$MESS["TEF_METER_VALUES_START_DATE"] = "Начало периода (день месяца)";
$MESS["TEF_METER_VALUES_END_DATE"] = "Окончание периода (день месяца)";
$MESS["TSZH_VALIDATION_FAILED"] = "При проверке полей произошли ошибки";
$MESS["TSZH_VALIDATION_FAILED_NOTE"] = "Для корректной работы с платежными функциями настоятельно рекомендуется исправить эти реквизиты.";
$MESS["CITRUS_TSZH_PAYMENT_MONETA_ENABLED"] = "Разрешить возможность приёма платежей через платёжный сервис \"НКО&nbsp;\"МОНЕТА\" (ООО) лицензия ЦБ РФ №3508-К от 02 июля 2012 года";
$MESS["TE_ERROR_MONETA_SCHEME_NOT_CHOSEN"] = "Не выбрана схема подключения платёжного сервиса &laquo;МОНЕТА.РУ&raquo;";
$MESS["TEF_MONETA_SECTION"] = "Приём платежей через платёжный сервис \"НКО&nbsp;\"МОНЕТА\" (ООО)";
$MESS["TEF_OVERHAUL_OFF"] = "Показывать данные по капитальному ремонту на странице начислений и оплат";
$MESS["TEF_OFFICE_HOURS"] = "Расписание работы";
$MESS["OFFICE_HOURS_FORM"] = "Форма редактирования расписания";
$MESS["TE_ERROR_SUBSCRIBE"] = "Ошибка настройки рассылки «#SUBSCRIBE#»: #ERROR#";

$MESS["TEF_SUBSCRIBE_EVENT_LABEL"] = "Специальное почтовое событие";
$MESS["TEF_SUBSCRIBE_EVENT_NOT_EXISTS"] = "Не создано";
$MESS["TEF_SUBSCRIBE_EVENT_BUTTON"] = "Создать";
$MESS["TEF_SUBSCRIBE_EVENT_EDIT_LINK"] = "Редактировать почтовый шаблон";

$MESS['TEF_CUSTOM_PAYMENT'] = 'Настройка оплаты по типам квитанций';
$MESS['TEF_ENABLE_CUSTOM_PAYMENT'] = 'Активировать ограничение оплат по типам квитанций';
$MESS['TEF_PAYMENT_RECEIPT_TYPES'] = 'Доступные для оплаты типы квитанций';
$MESS["CUSTOM_PAYMENT_ANNOTATION"] = "Настройка оплаты по типам квитанций позволяет определить типы квитанций для оплаты.
При установленном флажке \"Активировать ограничение оплат по типам квитанций\" можно выбрать тип квитанции, по которой жилец будет выполнять оплату.
Если флаг не установлен, то жилец может выполнить оплату по любому из 4-х типов квитанции.";

$MESS["MONETA_OFFER_ADDITITIONAL_INFO_DIALOG_TITLE"] = "Дополнительные данные для подключения системы Монета.ру";
$MESS["MONETA_OFFER_ADDITITIONAL_INFO_PARTNER_AGREEMENT_DIALOG_TITLE"] = "Заявление";
$MESS["MONETA_OFFER_ADDITITIONAL_INFO_POPUP_CONTENT"] = "Для подлкючения к сервису оплаты Монета.ру по 1 схеме нужно сохранить объект управления.";
$MESS["MONETA_OFFER_ADDITITIONAL_INFO_POPUP_TITLEBAR"] = "Внимание";

$MESS["MONETA_OFFER_ADDITITIONAL_INFO_TABLE_HEAD"] = "Дополнительная информация<br/>о подключении к 1 схеме";
$MESS["MONETA_OFFER_ADDITITIONAL_INFO_EDIT_LINK"] = "<a href=\"#URL#\">Изменить данные</a>";
$MESS["MONETA_OFFER_ADDITITIONAL_STATUS"] = "Статус";
$MESS["MONETA_OFFER_ADDITITIONAL_STATUS_NOT_ACTIVE"] = "Не подключено";
$MESS["MONETA_OFFER_ADDITITIONAL_STATUS_ON_VERIFICATION"] = "Данные на проверке";
$MESS["MONETA_OFFER_ADDITITIONAL_STATUS_WITHOUT_DOCS"] = "Подключено, ожидает заявления";
$MESS["MONETA_OFFER_ADDITITIONAL_STATUS_ACTIVE"] = "Подключено";
$MESS["MONETA_OFFER_ADDITITIONAL_DOCS"] = "Документы";
$MESS["MONETA_OFFER_ADDITITIONAL_DOCS_NOTE"] = "Необходимо <a href=\"#URL#\">скачать</a> Заявление о заключении Договора (ваши реквизиты уже внесены), распечатать, подписать и поставить печать, а затем выслать Заявление по адресу: <b>НКО «МОНЕТА» (ООО): 424000, Российская Федерация, Республика Марий Эл, г. Йошкар-Ола, ул. Гоголя, д. 2, строение «А».</b>";
$MESS["MONETA_OFFER_ADDITITIONAL_INFO_TABS"] = "<ul>
													<li>Личные данные</li>
													<li>Контакты</li>
													<li>Руководитель</li>
													<li>Финансовое положение</li>
													<li>Профиль бенефициара</li>";

// валидация ИНН в форме редактирования
$MESS['TSZH_JS_VALIDATION_INN_EMPTY'] = 'ИНН пуст';
$MESS['TSZH_JS_VALIDATION_INN_CAN_CONSIST_ONLY_OF_DIGITS'] = 'ИНН может состоять только из цифр';
$MESS['TSZH_JS_VALIDATION_INN_CAN_CONSIST_ONLY_10_12_DIGITS'] = 'ИНН может состоять только из 10 или 12 цифр';
$MESS['TSZH_JS_VALIDATION_INN_WRONG_CHECK_NUMBER'] = 'Неправильное контрольное число';

$MESS['TEF_MONETA_EMAIL_INFO_ISSET'] = 'Данный адрес электронной почты был использован для регистрации в сервисе Монета.ру.';
$MESS['TEF_MONETA_EMAIL_INFO_USSET'] = 'Для регистрации в сервисе Монета.ру необходимо указать адрес электронной почты. После сохранения e-mail адреса вы сможете подключить один из вариантов оплаты на сайте.';

$MESS['TEF_CUSTOM_PAYMENT'] = 'Настройка оплаты по типам квитанций';
$MESS['TEF_ENABLE_CUSTOM_PAYMENT'] = 'Активировать ограничение оплат по типам квитанций';
$MESS['TEF_PAYMENT_RECEIPT_TYPES'] = 'Доступные для оплаты типы квитанций';

$MESS["TEF_ADDITIONAL_INFO_MAIN"] = 'Дополнительные контакты организации, которые могут быть выведены в основной квитанции';
$MESS["TEF_ADDITIONAL_INFO_OVERHAUL"] = 'Дополнительные контакты организации, которые могут быть выведены в квитанции на капремонт';
$MESS["TEF_ANNOTATION_MAIN"] = 'Примечание - дополнительная информация, которая может быть выведена в нижней части основной квитанции';
$MESS["TEF_ANNOTATION_OVERHAUL"] = 'Примечание - дополнительная информация, которая может быть выведена в нижней части квитанции на капремонт';
$MESS["TE_ERROR_WRONG_RSCH"] = "Указанный расчетный счет (#RSCH#) не может быть использован для оплаты на сайте";
$MESS["TSZH_EDIT_CONTEX_MENU_DOWNLOAD_DEMO"] = "Загрузить демо-данные";
$MESS["CITRUS_TSZH_IS_MONETA_ANNOTATION"] = "<b>Внимание!</b> При смене банковских реквизитов для оплаты, просьба обязательно отправить актуальные реквизиты на электронную почту <a href=\"mailto:otr@rarus.ru\">otr@rarus.ru</a> для корректировки настройки получения платежей";