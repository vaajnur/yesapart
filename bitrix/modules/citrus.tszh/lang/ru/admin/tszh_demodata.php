<?
$MESS["TSZH_DEMODATA"] = "Загрузка демо-данных";
$MESS["TSZH_ERROR_MESSAGE_DETAILS"] = "Добавьте их в разделе <a href=\"/bitrix/admin/tszh_list.php?lang=#LANG#\">«Объекты управления»</a>, прежде чем загружать демо-данные.";
$MESS["TSZH_NOT_FOUND"] = "На сайте не найдены организации.";
$MESS["TSZH_OBJECT"] = "Объект управления";
