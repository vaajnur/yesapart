<?
$MESS ['APL_ERROR_UPDATE'] = "Ошибка при обновлении записи";
$MESS ['APL_ERROR_UPDATE_NOT_FOUND'] = "Ошибка при обновлении, строка не существует";


$MESS ['APL_PAGE_TITLE'] = "Квитанции";

$MESS ['APL_ERROR_F_AREA'] = "Значение общей площади «с» должно быть не больше значения «по».";
$MESS ['APL_ERROR_F_LAREA'] = "Значение жилой площади «с» должно быть не больше значения «по».";
$MESS ['APL_ERROR_F_PEOPLE'] = "Значение кол-ва проживающих «с» должно быть не больше значения «по».";
$MESS ['APL_ERROR_F_FLAT'] = "Значение номера квартиры «с» должно быть не больше значения «по».";
$MESS ['APL_ERROR_F_HOUSE'] = "Значение номера дома «с» должно быть не больше значения «по».";
$MESS ['APL_ERROR_DELETE'] = "Ошибка удаления квитанции счета #ID#.";
$MESS ['APL_ALL'] = "все";

$MESS ['APL_NAV_TITLE'] = "Квитанции";
$MESS ['APL_PERIOD'] = "Период";
$MESS ['APL_ACCOUNT'] = "Лицевой счет";
$MESS ['APL_DEBT_BEG'] = "Задолженность на начало";
$MESS ['APL_DEBT_END'] = "Задолженность на конец";
$MESS ['APL_SUM_PAYED'] = "Оплачено";
$MESS ['APL_SUMM_TO_PAY'] = "Итого к оплате";
$MESS ['APL_BARCODE'] = "ШтрихКод";
$MESS ['APL_ADDRESS'] = "Адрес";
$MESS ['APL_AREA'] = "Общая площадь";
$MESS ['APL_LAREA'] = "Жилая площадь";
$MESS ['APL_PEOPLE'] = "Кол-во проживающих";
$MESS ['APL_F_DEBT_BEG'] = "Задолженность на начало периода";
$MESS ['APL_F_DEBT_END'] = "Задолженность на конец периода";
$MESS ['APL_DATE_SENT'] = "Дата отправки квитанции";

$MESS ['APL_EDIT_ACCOUNT'] = "Изменить лицевой счет";
$MESS ['APL_EDIT_RECEIPT'] = "Изменить квитанцию";
$MESS ['APL_CURRENCY'] = "р.";

$MESS ['APL_EDIT'] = "Редактировать";
$MESS ['APL_DELETE'] = "Удалить";
$MESS ['APL_DELETE_CONFIRM'] = "Вы действительно хотите удалить квитанцию?";
$MESS ['APL_CNT_TOTAL'] = "Всего";
$MESS ['APL_CNT_SELECTED'] = "Выбрано";

$MESS ['APL_ACTION_RECEIPT'] = "Печать квитанции";
$MESS ['APL_PERIOD_ADD'] = "Добавить";
$MESS ['APL_PERIOD_ADD_TITLE'] = "Добавление квитанции";

$MESS ['APL_F_ACCOUNT'] = "Лицевой счет";
$MESS ['APL_F_ACCOUNT_NUMBER'] = "Номер лицевого счета";
$MESS ['APL_F_PERIOD'] = "Период";
$MESS ['APL_F_TYPE'] = "Тип";
$MESS ['APL_F_IPD'] = "Идентификатор платежного документа";
$MESS ['APL_F_FIO'] = "ФИО пользователя";
$MESS ['APL_F_ADDRESS'] = "Адрес (город, улица, дом или квартира)";
$MESS ['APL_F_AREA'] = "Общая площадь";
$MESS ['APL_F_LAREA'] = "Жилая площадь";
$MESS ['APL_F_PEOPLE'] = "Количество проживающих";
$MESS ['APL_F_CITY'] = "Город";
$MESS ['APL_F_DISTRICT'] = "Район";
$MESS ['APL_F_REGION'] = "Регион";
$MESS ['APL_F_SETTLEMENT'] = "Населенный пункт";
$MESS ['APL_F_STREET'] = "Улица";
$MESS ['APL_F_HOUSE'] = "Номер дома";
$MESS ['APL_F_FLAT'] = "Номер квартиры";
$MESS ['APL_F_FIND'] = "Найти";

$MESS ['APL_F_FROM'] = "с";
$MESS ['APL_F_TO'] = "по";
$MESS ['APL_GA_PRINT'] = "Печать квитанций";

$MESS ['APL_GA_SEND'] = "Разослать квитанции";
$MESS ['APL_GA_SEND_NOT_SENT'] = "Разослать не отправленные";
$MESS ['APL_SEND'] = "Выслать квитанцию на e-mail";
$MESS ['APL_SEND_CONFIRM'] = "Выслать квитанцию на e-mail?";

$MESS ['APL_GA_IN_SEND_QUEUE'] = "В очереди на отправку";

$MESS["CITRUS_TSZH_CONTEXT_PRINT"] = "Распечатать квитанцию";
