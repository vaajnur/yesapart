<?
$MESS["TP_ERROR_UPDATE"] = "Помилка при оновленні періоду ";
$MESS["TP_ERROR_UPDATE_NOT_FOUND"] = "Помилка при оновленні, рядок не існує";
$MESS["TP_ERROR_DELETE"] = "Помилка відалення";
$MESS["TP_PERIODS_NAV"] = "Періоди";
$MESS["TP_F_ACTIVE"] = "Активність";
$MESS["TP_F_ONLY_DEBT"] = "База нарахування";
$MESS["TP_F_PERIOD"] = "Період";
$MESS["TP_F_DATE"] = "Дата періода";
$MESS["TP_F_TIMESTAMP_X"] = "Дата змін";
$MESS["TP_F_ACCOUNTS"] = "Особові рахунки";
$MESS["TP_F_TSZH_ID"] = "ТСЖ";
$MESS["TP_F_ALL"] = "(все)";
$MESS["TP_ACCOUNTS_LIST"] = "список";
$MESS["TP_M_EDIT"] = "Редагувати";
$MESS["TP_M_ACCOUNTS"] = "Особові рахунки";
$MESS["TP_M_DELETE"] = "Видалити";
$MESS["TP_M_DELETE_CONFIRM"] = "Ви дійсно бажаєте видалити період?";
$MESS["TP_CNT_TOTAL"] = "Всього";
$MESS["TP_CNT_SELECTED"] = "Обрано";
$MESS["TP_YES"] = "Так";
$MESS["TP_NO"] = "Ні";
$MESS["TP_ALL"] = "все";
$MESS["TP_C_ADD_PERIOD"] = "Додати період";
$MESS["TP_C_ADD_PERIOD_TITLE"] = "Додавання періоду";
$MESS["TP_PAGE_TITLE"] = "Періоди";
$MESS["TP_FIND"] = "Знайти";
?>