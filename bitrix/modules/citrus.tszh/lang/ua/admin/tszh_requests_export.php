<?
$MESS["TSZH_EXPORT_ERROR"] = "Помилка експорта (недостотньо прав)";
$MESS["TSZH_EXPORT"] = "Експорт...";
$MESS["TSZH_EXPORT_DICTIONARIES"] = "Експорт словників";
$MESS["TSZH_EXPORT_DICTIONARIES_PROGRESS"] = "Експорт словників";
$MESS["TSZH_EXPORT_DICTIONARIES_DONE"] = "Словники експортовані";
$MESS["TSZH_EXPORT_DONE"] = "Експорт закінчений";
$MESS["TSZH_EXPORT_DONE_REQUESTS"] = "Експортовано звернень: # COUNT # ";
$MESS["TSZH_EXPORT_REQUESTS"] = "Експорт звернень";
$MESS["TSZH_EXPORT_REQUESTS_PROGRESS"] = "Експортовано звернень: # COUNT # ";
$MESS["TSZH_EXPORT_TITLE"] = "Експорт звернень";
$MESS["TSZH_EXPORT_TAB"] = "Експорт";
$MESS["TSZH_EXPORT_TAB_TITLE"] = "Налаштування експорта";
$MESS["TSZH_EXPORT_ONLY_OPENED"] = "Експортувати тільки відкриті звернення";
$MESS["TSZH_EXPORT_FILE_ERROR"] = "Помилка відкриття файла.";
$MESS["TSZH_EXPORT_SECTIONS"] = "Експорт розділів каталога.";
$MESS["TSZH_EXPORT_SECTIONS_PROGRESS"] = "Експортовано # COUNT # розділів каталога.";
$MESS["TSZH_EXPORT_METADATA_DONE"] = "Метадані каталогу експортовані. ";
$MESS["TSZH_EXPORT_URL_DATA_FILE"] = "Файл для вивантаження";
$MESS["TSZH_EXPORT_OPEN"] = "Відкрити...";
$MESS["TSZH_EXPORT_IBLOCK_ID"] = "Інформаційний блок";
$MESS["TSZH_EXPORT_START_EXPORT"] = "Експортувати";
$MESS["TSZH_EXPORT_STOP_EXPORT"] = "Зупинити експорт";
$MESS["TSZH_EXPORT_INTERVAL"] = "Тривалість кроку в секундах (0 - виконувати експорт за один крок) ";
$MESS["TSZH_EXPORT_FILE_NAME_ERROR"] = "Ім'я файла (папки) може складатися тільки з символів латинського алфавіту, цифр, пропусків, а також символів: !#\$%&()[]{}+-.;=@^_~";
$MESS["TSZH_EXPORT_ACCESS_DENIED"] = "Доступ заборонений.";
?>