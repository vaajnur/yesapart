<?
$MESS["AL_F_FROM"] = "с";
$MESS["AL_F_TO"] = "по";

$MESS["AL_ERROR_AREA"] = "Значення загальної площі «з» має бути не більше значення «по». ";
$MESS["AL_ERROR_LAREA"] = "Значення житлової площі «з» має бути не більше значення «по». ";
$MESS["AL_ERROR_HOUSE_AREA"] = "Значення загальної площі будівлі «з» повинно бути не більше значення «по».";
$MESS["AL_ERROR_HOUSE_ROOMS_AREA"] = "Значення площі приміщень будівлі «з» повинно бути не більше значення «по».";
$MESS["AL_ERROR_HOUSE_COMMON_PLACES_AREA"] = "Значення площі МЗК «з» повинно бути не більше значення «по».";
$MESS["AL_ERROR_PEOPLE"] = "Значення кількості мешканців «з» має бути не більше значення «по». ";
$MESS["AL_ERROR_REGISTERED_PEOPLE"] = "Значення кількості зареєстрованих «з» повинно бути не більше значення «по».";
$MESS["AL_ERROR_EXEMPT_PEOPLE"] = "Значення кількості пільговиків «з» повинно бути не більше значення «по».";
$MESS["AL_ERROR_UPDATE"] = "Помилка при оновленні запису ";
$MESS["AL_ERROR_UPDATE_NOT_FOUND"] = "Помилка при оновленні, рядок не існує";
$MESS["AL_ERROR_DELETE"] = "Помилка видалення";
$MESS["AL_NAV_ACCOUNTS"] = "Особові рахунки";
$MESS["AL_F_USER_ID"] = "Власник";
$MESS["AL_F_TSZH_ID"] = "ТСЖ";
$MESS["AL_FIND"] = "Знайти";
$MESS["AL_F_XML_ID"] = "Номер особового рахунку";
$MESS["AL_F_ADDRESS"] = "Адреса";
$MESS["AL_F_CITY"] = "Місто";
$MESS["AL_F_DISTRICT"] = "Район";
$MESS["AL_F_REGION"] = "Регіон";
$MESS["AL_F_SETTLEMENT"] = "Населений пункт";
$MESS["AL_F_STREET"] = "Вулиця";
$MESS["AL_F_HOUSE"] = "Будинок";
$MESS["AL_F_FLAT"] = "Квартира";
$MESS["AL_F_FLAT_TYPE"] = "Тип квартири";
$MESS["AL_F_AREA"] = "Площа";
$MESS["AL_F_LAREA"] = "Житлова площа";
$MESS["AL_F_HOUSE_AREA"] = "Загальна площа будівлі";
$MESS["AL_F_HOUSE_ROOMS_AREA"] = "Площа приміщень будівлі";
$MESS["AL_F_HOUSE_COMMON_PLACES_AREA"] = "Площа МЗК";
$MESS["AL_F_PEOPLE"] = "Кількість мешканців";
$MESS["AL_F_REGISTERED_PEOPLE"] = "Зареєстровано людей";
$MESS["AL_F_EXEMPT_PEOPLE"] = "Пільговиків";
$MESS["AL_F_FIO"] = "ПІБ користувача";
$MESS["AL_F_FULL_ADDRESS"] = "Адреса (місто, вулиця, будинок або квартира)";
$MESS["AL_F_COMMON_AREA"] = "Загальна площа";
$MESS["AL_VIEW_PROFILE"] = "Перейти до перегляду профіля";
$MESS["AL_M_EDIT"] = "Редагувати";
$MESS["AL_M_PERIODS"] = "Періоди";
$MESS["AL_M_DELETE"] = "Видалити";
$MESS["AL_M_DELETE_CONFIRM"] = "Ви дійсно бажаєте видалити елемент?";
$MESS["AL_CNT_TOTAL"] = "Всього";
$MESS["AL_CNT_SELECTED"] = "Обрано";
$MESS["AL_C_ADD_ACCOUNT"] = "Додати особовий рахунок";
$MESS["AL_C_ADD_ACCOUNT_TITLE"] = "Додавання особового рахунку";
$MESS["AL_PAGE_TITLE"] = "Особові рахунки";
$MESS["AL_VIEW_TSZH"] = "Перегляд інформації про ТСЖ";
$MESS["TSZH_F_ALL"] = "(все)";
$MESS["AL_F_FLAT_ABBR"] = "Приміщення";
?>