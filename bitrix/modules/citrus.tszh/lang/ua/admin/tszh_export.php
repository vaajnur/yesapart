<?
$MESS["TE_ERROR_EXPORT"] = "При експорті виникла помилка.";
$MESS["TE_PAGE_TITLE"] = "ТСЖ — Експорт";
$MESS["TE_TAB1"] = "параметри експорта";
$MESS["TE_TAB1_TITLE"] = "параметри експорта";
$MESS["TE_TAB2"] = "Експорт";
$MESS["TE_TAB2_TITLE"] = "Експорт";
$MESS["TE_EXPORT_DONE"] = "Експорт завершено";
$MESS["TE_DOWNLOAD_XML"] = "Завантажити XML-файл";
$MESS["TE_NEXT_BTN"] = "Далі &gt;&gt;";
$MESS["TE_DELETE_FILE_BTN"] = "Видалити експортований файл";
$MESS["TSZH_EXPORT_NO_TSZH_SELECTED"] = "Не обрано ТСЖ";
$MESS["TSZH_EXPORT_TSZH"] = "ТСЖ";
$MESS["TSZH_EXPORT_TEXT"] = "Виберіть організацію, для якої необхідно вивантажити показання лічильників та натисніть кнопку «<em> Далі </em>». ";
$MESS["TSZH_EXPORT_STEP_TIME"] = "Тривалість кроку в секундах <br /> ( 0 - виконувати експорт за один крок )";
$MESS["TSZH_EXPORT_PROGRESS"] = ". Вивантаження показань лічильників <br /> Залишилося вивантажити :#РАЗОМ#...";
$MESS["TSZH_EXPORT_FORMAT"] = "формат";
$MESS["TSZH_EXPORT_FORMAT_XML_DESC"] = "Вивантаження файлу у форматі XML , для подальшого завантаження в 1С : Облік в управляючих компаніях ЖКГ , ТСЖ і ЖБК.";
$MESS["TSZH_EXPORT_FORMAT_CSV_DESC"] = "Вивантаження файлу у форматі CSV , який можна відкрити в Microsoft Excel";
$MESS["TSZH_EXPORT_VALUES_BY_OWNER"] = "Вивантажувати тільки показання , інструменти введені користувачем на сайті:";
$MESS["TSZH_EXPORT_PERIOD"] = "Вивантажувати свідчення за період:";
$MESS["TSZH_EXPORT_ERROR_DATE_1"] = "Невірний формат першої дати !";
$MESS["TSZH_EXPORT_ERROR_DATE_2"] = "Невірний формат другої дати !";
$MESS["TSZH_EXPORT_ERROR_DATE_3"] = "Перша дата більше другий !";
?>