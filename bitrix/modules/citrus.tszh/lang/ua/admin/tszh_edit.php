<?
$MESS["TSZH_EDIT_TAB1"] = "ТСЖ";
$MESS["TSZH_EDIT_TAB1_TITLE"] = "Інформація про ТСЖ";
$MESS["TE_ERROR_NO_PERMISSIONS"] = "Недостатньо прав для редагування ТСЖ";
$MESS["TE_ERROR_TSZH_NOT_FOUND"] = "ТСЖ не знайдено";
$MESS["TE_ERROR_SAVE"] = "Помилка збереження ТСЖ";
$MESS["TE_TITLE_EDIT"] = "Редагування ТСЖ №#ID#";
$MESS["TE_TITLE_ADD"] = "Додавання ТСЖ";
$MESS["TE_ERROR_NO_ADD_PERMISSIONS"] = "Недостатньо прав для додавання ТСЖ";
$MESS["TE_ERROR_HAPPENED"] = "Відбулися наступні помилки";
$MESS["TE_M_ACCOUNT_LIST"] = "Список ТСЖ";
$MESS["TE_M_ACCOUNT_LIST_TITLE"] = "Перейти до списку ТСЖ";
$MESS["TE_M_ADD_TSZH"] = "Добавити ТСЖ";
$MESS["TE_M_ADD_TSZH_TITLE"] = "Добавити ТСЖ";
$MESS["TE_M_DELETE_TSZH"] = "Видалити";
$MESS["TE_M_DELETE_TSZH_CONFIRM"] = "Ви дійсно бажаєте видалити ТСЖ та всі його особові рахунки?";
$MESS["TE_FORM_SETTING"] = "Налаштування";
$MESS["TE_FORM_SETTING_TITLE"] = "Налаштування форми";
$MESS["TEF_USER_FIELDS"] = "Додаткові поля";
$MESS["TEF_TIMESTAMP_X"] = "Дата останньої зміни";
$MESS["TEF_NAME"] = "Назва";
$MESS["TEF_SITE_ID"] = "Сайт";
$MESS["TEF_CODE"] = "Символьний код";
$MESS["TEF_INN"] = "ІНПП";
$MESS["TEF_KPP"] = "КПП";
$MESS["TEF_RSCH"] = "Розрахунковий рахунок";
$MESS["TEF_BANK"] = "Банк";
$MESS["TEF_KSCH"] = "Кореспондентський рахунок ";
$MESS["TEF_BIK"] = "МФО";
$MESS["TEF_BANK_TITLE"] = "Банківські реквізити";
$MESS["TSZH_EDIT_TAB2"] = "Розсилання";
$MESS["TSZH_EDIT_TAB2_TITLE"] = "налаштування розсилок";
$MESS["TEF_ADDITIONAL"] = "Додаткова інформація";
$MESS["TEF_HEAD_NAME"] = "ПІБ керівника";
$MESS["TEF_LEGAL_ADDRESS"] = "Юридична адреса";
$MESS["TEF_SUBSCRIBE_ACTIVE"] = "активна";
$MESS["TEF_SUBSCRIBE_DATE"] = "День місяця для розсилки";
$MESS["TEF_SUBSCRIBE_LAST_PERIOD"] = "Дата останньою розсилки";
$MESS["TEF_SUBSCRIBE_DOLG_TITLE"] = "Розсилка повідомлень боржникам";
$MESS["TEF_SUBSCRIBE_METERS_TITLE"] = "Розсилка повідомлень про необхідність введення показань лічильників";
$MESS["TEF_SUBSCRIBE_RECEIPT_TITLE"] = "Розсилка квитанцій власникам особових рахунків";
$MESS["TEF_SUBSCRIBE_DOLG_MIN_SUM"] = "Мінімальна сума заборгованості для відправлення повідомлення";
$MESS["TEF_RECEIPT_INFO"] = "Інформація для квитанцій";
$MESS["TEF_ADDRESS"] = "Фактична адреса";
$MESS["TEF_EMAIL"] = "E-mail організації для відображення в контактах";
$MESS["TEF_PHONE"] = "Телефон , факс";
$MESS["TEF_PHONE_DISP"] = "диспетчерська";
$MESS["TEF_RECEIPT_TEMPLATE"] = "Шаблон квитанцій";
$MESS["TEF_RECEIPT_EPD1161_CONTACT_CENTER"] = "Контактний центр для квитанції epd-1161";
$MESS["TEF_METER_VALUES_PERIOD"] = "Період , в якому дозволений введення показань лічильників";
$MESS["TEF_METER_VALUES_START_DATE"] = "Початок періоду (день місяця)";
$MESS["TEF_METER_VALUES_END_DATE"] = "Закінчення періоду (день місяця)";
$MESS["TSZH_VALIDATION_FAILED"] = "При перевірці полів відбулися помилки";
$MESS["TSZH_VALIDATION_FAILED_NOTE"] = "Для коректної роботи з платіжними функціями настійно рекомендується виправити ці реквізити .";
$MESS["CITRUS_TSZH_PAYMENT_MONETA_ENABLED"] = "Разрешить оплату через сервис НКО &laquo;МОНЕТА&raquo; (ООО)";
$MESS["CITRUS_TSZH_PAYMENT_MONETA_OFFER"] = "Подтверждаю свое согласие на&nbsp;присоединении к&nbsp;<a href=\"https://moneta.ru/info/public/merchants/b2boffer.pdf\" target=\"_blank\">Договору НКО &laquo;МОНЕТА&raquo; (ООО)</a>, с&nbsp;<a href=\"https://www.moneta.ru/info/public/merchants/b2btariffs.pdf\" target=\"_blank\">Тарифами</a> и&nbsp;<a href=\"#\" style=\"text-decoration: none; border-bottom: 1px dashed #2770ba\" onclick=\"monetaProcedurePopup.show(); return false;\">Порядком действий</a> ознакомлен и&nbsp;согласен";
$MESS["CITRUS_TSZH_PAYMENT_MONETA_PROCEDURE"] = "
<p><strong>Порядок действий</strong></p>
<ol>
	<li><p>На&nbsp;указанный Вами при установке <nobr>e-mail</nobr> (#EMAIL#) будет направлено письмо с&nbsp;реквизитами доступа в&nbsp;Личный кабинет НКО &laquo;МОНЕТА&raquo; (ООО). Войдите в&nbsp;Личный кабинет и&nbsp;заполните все требуемые поля. После первого входа рекомендуем изменить пароль.</p></li>
	<li><p>После заполнения и&nbsp;подтверждения всех обязательных полей в&nbsp;формах Личного кабинета&nbsp;&mdash; будет автоматически сформировано Заявление о&nbsp;присоединении к&nbsp;условиям договора. Скачайте Заявление в&nbsp;разделе &laquo;Документы&raquo;, распечатайте, подпишите и&nbsp;отправьте в&nbsp;наш адрес &laquo;Заявление о&nbsp;присоединении&raquo; по&nbsp;адресу: <i>424020, Российская Федерация, Республика Марий Эл, г.&nbsp;<nobr>Йошкар-Ола</nobr>, ул.&nbsp;Анциферова, дом 7, корпус &laquo;А&raquo; для НКО &laquo;МОНЕТА&raquo; (ООО)</i>, телефон для курьерской службы <nobr>(8362) 45-43-83</nobr>.</p></li>
	<li><p>По&nbsp;умолчанию доступны следующие способы: <i>банковские карты</i>, <i>Монета.ру</i>, <i>Яндекс.Деньги</i>. Если в&nbsp;течение 30 календарных дней не&nbsp;будет получено письменное подтверждение согласия с&nbsp;условиями Договора НКО &laquo;МОНЕТА&raquo; (ООО), он&nbsp;может быть расторгнут.</p></li>
</ol>
<p><small>Примечание: Денежные средства перечисляются на&nbsp;ваш банковский счет не&nbsp;позднее первого рабочего дня, следующего за&nbsp;днем получения НКО &laquo;МОНЕТА&raquo; (ООО) распоряжения Плательщика. Вознаграждение за&nbsp;осуществление Переводов указано в&nbsp;Тарифах. В&nbsp;случае вашего присоединения к&nbsp;Договору НКО &laquo;МОНЕТА&raquo; (ООО): комиссия НКО &laquo;МОНЕТА&raquo; (ООО) удерживается из&nbsp;суммы, подлежащей перечислению на&nbsp;ваш расчетный счет; комиссия с&nbsp;Плательщика не&nbsp;удерживается.<br><br>Расторжение Договора с НКО «МОНЕТА» (ООО) возможно путем направления соответствующего письменного уведомления.</small></p>
";
$MESS["CITRUS_TSZH_PAYMENT_CLOSE"] = "Закрити";
$MESS["CITRUS_TSZH_NEED_EMAIL"] = "<span style=\"color: #f00;\">укажите email в настройках</span> <a target=\"_blank\" href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\">главного модуля</a>";

$MESS["TEF_OFFICE_HOURS"] = "Розклад роботи";

$MESS["TE_ERROR_SUBSCRIBE"] = "Помилка налаштування розсилки «#SUBSCRIBE#»: #ERROR#";

$MESS["TEF_SUBSCRIBE_EVENT_LABEL"] = "Спеціальне поштове подія";
$MESS["TEF_SUBSCRIBE_EVENT_NOT_EXISTS"] = "Не створено";
$MESS["TEF_SUBSCRIBE_EVENT_EDIT_LINK"] = "Редагувати поштовий шаблон";
$MESS["TEF_SUBSCRIBE_EVENT_BUTTON"] = "Створити";
