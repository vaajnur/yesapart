<?
$MESS["TSZH_CONTRACTORS_EDIT"] = "Постачальник послуг";
$MESS["TSZH_CONTRACTORS_EDIT_TITLE"] = "Постачальник послуг";
$MESS["TSZH_EDIT_PAGE_TITLE"] = "Редагування постачальника послуг №#ID#";
$MESS["TSZH_ADD_PAGE_TITLE"] = "Додавання постачальника послуг";
$MESS["TSZH_EDIT_ITEM_NOT_FOUND"] = "Постачальник послуг не знайдена";
$MESS["TSZH_ITEMS_LIST"] = "список постачальників";
$MESS["TSZH_ITEMS_LIST_TITLE"] = "Список постачальників послуг";
$MESS["TSZH_F_ID"] = "ID";
$MESS["TSZH_F_XML_ID"] = "зовнішній код";
$MESS["TSZH_F_TSZH_ID"] = "об'єкт управління";
$MESS["TSZH_F_EXECUTOR"] = "виконавець";
$MESS["TSZH_F_NAME"] = "Найменування";
$MESS["TSZH_F_ADDRESS"] = "Адреса";
$MESS["TSZH_F_SERVICES"] = "послуги";
$MESS["TSZH_F_PHONE"] = "Телефон";
$MESS["TSZH_F_BILLING"] = "Платіжні реквізити";
$MESS["TSZH_USER_FIELDS"] = "користувацькі властивості";
?>