<?
$MESS["TPE_TAB1"] = "Період";
$MESS["TPE_TAB1_TITLE"] = "Період";
$MESS["TPE_NO_PERMISSIONS"] = "Недостатньо прав для редагування періодів";
$MESS["TPE_ERROR_NOT_FOUND"] = "Період #ID# не знайдено";
$MESS["TPE_ERROR_WRONG_DATE"] = "Дата періоду введена невірно";
$MESS["TPE_ERROR_SAVE"] = "При збереженні періоду сталася помилка";
$MESS["TPE_PAGE_TITLE_EDIT"] = "Зміна періоду ##ID#";
$MESS["TPE_PAGE_TITLE_ADD"] = "Додавання періода";
$MESS["TPE_ERRORS_HAPPENED"] = "Відбулися наступні помилки ";
$MESS["TPE_M_PERIODS_LIST"] = "Список періодів";
$MESS["TPE_M_PERIODS_LIST_TITLE"] = "Список періодів";
$MESS["TPE_M_PERIODS_ADD"] = "Додати період";
$MESS["TPE_M_PERIODS_ADD_TITLE"] = "Додати період";
$MESS["TPE_M_PERIODS_DELETE"] = "Видалити період";
$MESS["TPE_M_PERIODS_DELETE_CONFIRM"] = "Ви дійсно бажаєте видалити період?";
$MESS["TPE_M_SETTINGS"] = "Налаштувати";
$MESS["TPE_M_SETTINGS_TITLE"] = "Налаштувати поля форми";
$MESS["TPE_F_TIMESTAMP_X"] = "Дата останньої зміни";
$MESS["TPE_F_DATE"] = "Дата періода";
$MESS["TPE_F_TSZH_ID"] = "ТСЖ";
$MESS["TPE_F_ACTIVE"] = "Період активний";
$MESS["TPE_F_ONLY_DEBT"] = "Без нарахувань";
$MESS["TPE_ONLY_DEBT_DESCR"] = "Якщо галочка встановлена, нарахування і квитанція по періоду не будуть відображатися мешканцям. <br /> Можна використовувати для попередної публікації заборгованості, до підготовки квитанцій. ";
?>