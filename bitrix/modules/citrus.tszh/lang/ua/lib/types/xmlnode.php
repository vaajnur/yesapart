<?
$MESS["CITRUS_TSZH_INCORRECT_DATETIME"] = "Указано некорректное занчение даты/времени (атрибут #ATTR#): #VALUE##";
$MESS["CITRUS_TSZH_XML_MISSING_REQUIRED_ATTR"] = "Элемент #ELEMENT# не содержит обязательного атрибута #NAME#";
$MESS["CITRUS_TSZH_XML_MISSING_REQUIRED_ELEMENT"] = "Отсутсвует обязательный дочерний элемент `#CHILD#` у `#PARENT#`";
$MESS["CITRUS_TSZH_XML_WRONG_INTEGER_ATTR"] = "Атрибут #NAME# элемента #ELEMENT# должен быть целым числом (указано значение `#VALUE#`)";
$MESS["CITRUS_TSZH_XML_WRONG_FLOAT_ATTR"] = "Атрибут #NAME# элемента #ELEMENT# должен быть числом (указано значение `#VALUE#`)";

