<?
$MESS["HOUSE_ENTITY_ID_FIELD"] = "ID дома";
$MESS["HOUSE_ENTITY_EXTERNAL_ID_FIELD"] = "Внешний код";
$MESS["HOUSE_ENTITY_TSZH_ID_FIELD"] = "Объект управления";
$MESS["HOUSE_ENTITY_AREA_FIELD"] = "Общая площадь";
$MESS["HOUSE_ENTITY_ROOMS_AREA_FIELD"] = "Площадь жилых помещений";
$MESS["HOUSE_ENTITY_COMMON_PLACES_AREA_FIELD"] = "Площадь мест общего пользования";
$MESS["HOUSE_ENTITY_REGION_FIELD"] = "Регион";
$MESS["HOUSE_ENTITY_DISTRICT_FIELD"] = "Район";
$MESS["HOUSE_ENTITY_CITY_FIELD"] = "Город";
$MESS["HOUSE_ENTITY_SETTLEMENT_FIELD"] = "Населенный пункт";
$MESS["HOUSE_ENTITY_STREET_FIELD"] = "Улица";
$MESS["HOUSE_ENTITY_HOUSE_FIELD"] = "Номер дома";
$MESS["HOUSE_ENTITY_FULL_ADDRESS_FIELD"] = "Адрес";

$MESS["HOUSE_ENTITY_BANK_FIELD"] = "Банк";
$MESS["HOUSE_ENTITY_BIK_FIELD"] = "БИК";
$MESS["HOUSE_ENTITY_RS_FIELD"] = "Расчетный счет";
$MESS["HOUSE_ENTITY_KS_FIELD"] = "Корреспондентский счет";

$MESS["HOUSE_ENTITY_OVERHAUL_BANK_FIELD"] = "Банк";
$MESS["HOUSE_ENTITY_OVERHAUL_BIK_FIELD"] = "БИК";
$MESS["HOUSE_ENTITY_OVERHAUL_RS_FIELD"] = "Расчетный счет";
$MESS["HOUSE_ENTITY_OVERHAUL_KS_FIELD"] = "Корреспондентский счет";

$MESS["HOUSE_ENTITY_ZIP_FIELD"] = "Почтовый индекс";