<?
$MESS["TSZH_FLT_ID"] = "ID";
$MESS["TSZH_FLT_XML_ID"] = "Номер особового рахунку";
$MESS["TSZH_FLT_NAME"] = "Найменування";
$MESS["TSZH_FLT_UNITS"] = "Од.вим.";
$MESS["TSZH_FLT_NORM"] = "Норма споживання";
$MESS["TSZH_FLT_TARIFF"] = "Тариф";
$MESS["TSZH_PAGE_TITLE"] = "Вибір послуги";
$MESS["TSZH_FLT_SEARCH"] = "Пошук";
$MESS["TSZH_FLT_SEARCH_TITLE"] = "Пошук послуги";
$MESS["TSZH_SELECT"] = "Вибрати";
$MESS["TSZH_EDIT_ACCOUNT"] = "Перейти до редагування послуги";
?>