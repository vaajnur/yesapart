<?
$MESS["TSZH_FLT_ID"] = "ID";
$MESS["TSZH_FLT_DATE"] = "Дата";
$MESS["TSZH_FLT_PERIOD"] = "Період";
$MESS["TSZH_FLT_TSZH_ID"] = "ТСЖ";
$MESS["TSZH_F_ALL"] = "(все)";
$MESS["TSZH_PAGE_TITLE"] = "Вибір періоду";
$MESS["TSZH_FLT_SEARCH"] = "Пошук";
$MESS["TSZH_FLT_SEARCH_TITLE"] = "Пошук періоду";
$MESS["TSZH_SELECT"] = "Вибрати";
$MESS["TSZH_EDIT_ACCOUNT"] = "Перейти до редагування періоду";
?>