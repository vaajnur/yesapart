<?
$MESS["TSZH_COST_FORMAT"] = "#A# <span class=\"currency\">грн.</span> #B# <span class=\"currency\">коп.</span>";
$MESS["TSZH_WIZ_BUTTON_NAME"] = "Майстер#BR#налаштування";
$MESS["TSZH_WIZ_BUTTON_DESCRIPTION"] = "Запустити майстер установки і налаштування сайту ТСЖ";

$MESS["TSZH_NOTIFY_DUMMY_MAIL_HEADER"] = "В Вашому профілі не заданий коректний e-mail";
$MESS["TSZH_NOTIFY_DUMMY_MAIL_BODY"] = "Будь ласка, вкажіть у Вашому <a href=\"#URL#\">профілі</a> коректний e-mail.";

$MESS["TSZH_CONFIRM_EMAIL_HEADER"] = "Підтвердження адреси e-mail";
$MESS["TSZH_CONFIRM_EMAIL_ERROR_USER_NOT_FOUND"] = "Користувач з ID #USER_ID# не знайдене.";
$MESS["TSZH_CONFIRM_EMAIL_ERROR_INVALID_HASH"] = "Підтверджуваний e-mail відрізняється від встановленого.";
$MESS["TSZH_CONFIRM_EMAIL_ERROR_UPDATE"] = "Помилка при збереженні підтвердження: #ERROR#.";
$MESS["TSZH_CONFIRM_EMAIL_SUCCEEDED"] = "Ваш e-mail #EMAIL# успішно підтверджений.";

$MESS["TSZH_UNSUBSCRIBE_HEADER"] = "Відписка від розсилки";
$MESS["TSZH_UNSUBSCRIBE_ERROR_SUBSCRIBE_NOT_FOUND"] = "Відписує розсилка не знайдено.";
$MESS["TSZH_UNSUBSCRIBE_ERROR_INVALID_HASH"] = "Відписує розсилка не відповідає вказаній.";
$MESS["TSZH_UNSUBSCRIBE_SUCCEEDED"] = "Ви відписалися від розсилки «#SUBSCRIBE#».";
