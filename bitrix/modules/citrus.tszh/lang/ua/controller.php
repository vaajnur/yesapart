<?
$MESS["TSZH_MODULE_EDITION_NOT_FOUND_ERROR"] = "Не визначена редакція продукту 1С: Сайт ЖКГ";
$MESS["TSZH_MODULE_EDITION_INCLUDE_ERROR"] = "Не встановлений модуль з редакцією продукту 1С: Сайт ЖКГ";
$MESS["TSZH_MODULE_DEMO"] = "Продукт «1С: Сайт ЖКГ» працює в демо -режимі.";
$MESS["TSZH_MODULE_DEMO_NOTICE"] = "<p style=\"padding: 1em; border: 1px solid #eee;\">По вопросам приобретения обращайтесь:<br>+7 (495) 777-25-43, +7 (836) 249-46-89<br><a href=\"mailto:clients@vdgb-soft.ru\">clients@vdgb-soft.ru</a><br><a href=\"http://www.vdgb-soft.ru/jsk/site_jkh/\" target=\"_blank\" rel=\"noindex nofollow\">www.vdgb-soft.ru/jsk/site_jkh/</a></p>";
$MESS["TSZH_MODULE_DEMO_NOTICE_ADMIN"] = "По вопросам приобретения и технической поддержки обращайтесь:<br>+7 (495) 777-25-43, +7 (836) 249-46-89<br><a href=\"mailto:clients@vdgb-soft.ru\">clients@vdgb-soft.ru</a><br><a href=\"http://www.vdgb-soft.ru/jsk/site_jkh/\" target=\"_blank\" rel=\"noindex nofollow\">www.vdgb-soft.ru/jsk/site_jkh/</a>";
$MESS["TSZH_MODULE_DEMO_EXPIRE_DATE"] = "<br><br>Пробный период истекает через #DAYS_LEFT#. Функционал сайта будет ограничен <b>#DATE#</b>.";
$MESS["TSZH_MODULE_DEMO_EXPIRED_ERROR"] = "Пробний період роботи продукту «1С: Сайт ЖКГ» закінчився.";
$MESS["CITRUS_DAYS_PLURAL"] = "день | дні | днів";

$MESS["CITRUS_TSZH_SUBSCRIBE_FCONTROLLER_SM_VERSION_ADMIN_HEADER"] = "Функціонал розсилок недоступний";
$MESS["CITRUS_TSZH_SUBSCRIBE_FCONTROLLER_SM_VERSION_ADMIN_TEXT"] = 'Для роботи функціоналу розсилок потрібна версія #MIN_VERSION# або вище головного модуля Бітрікс.<br>Будь ласка, <a href="/bitrix/admin/update_system.php?lang=ua" target="_blank">встановіть оновлення</a> головного модуля.';
$MESS["CITRUS_TSZH_FCONTROLLER_SUBSCRIBE_SM_VERSION_PUBLIC"] = "Функціонал розсилок недоступний: потрібна версія #MIN_VERSION# або вище головного модуля Бітрікс.";

$MESS["CITRUS_TSZHPAYMENT_LINK"] = "<p>Ви можете <a href=\"#LINK#\">оплатити комунальні послуги</a> онлайн.</p>";
?>