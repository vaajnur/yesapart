<?
$MESS["TSZH_ERROR_CONTRACTORS_NO_TSZH_ID"] = "Не вказаний об'єкт управління";
$MESS["TSZH_ERROR_CONTRACTORS_NO_NAME"] = "Не вказано найменування організації -постачальника";
$MESS["TSZH_ERROR_AC_NO_ACCOUNT_PERIOD"] = "Чи не вказана квитанція для суми по постачальнику";
$MESS["TSZH_ERROR_AC_NO_CONTRACTOR"] = "Не вказаний постачальник для суми по постачальнику";
$MESS["TSZH_ERROR_AC_NEG_SUMM"] = "Сума за постачальнику не може бути негативною";
?>