<?
$MESS["TSZH_ERROR_CURRENCIES_NO_ID"] = "Не заповнене обов'язкове поле «Код валюти»";
$MESS["TSZH_ERROR_CURRENCIES_WRONG_ID"] = "Чи не коректно вказано поле «Код валюти». Повинна бути вказаний рядок з трьох латинських букв (стандарт ISO 4217)";
$MESS["TSZH_ERROR_CURRENCIES_NO_NAME"] = "Не заповнене обов'язкове поле «Найменування»";
$MESS["TSZH_ERROR_CURRENCIES_NO_SHORT"] = "Не заповнене обов'язкове поле «Короткий позначення»";
$MESS["TSZH_ERROR_CURRENCIES_NO_FULL"] = "Не заповнене обов'язкове поле «Повне позначення»";
