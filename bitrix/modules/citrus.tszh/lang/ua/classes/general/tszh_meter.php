<?
$MESS["ERROR_TSZH_METER_WRONG_ACCOUNT"] = "Помилка, прив'язка до особового рахунку вказана невірно. ";
$MESS["ERROR_TSZH_METER_LARGE_DEC_PLACES"] = "Кількість знаків після коми не може бути більше 8 .";
$MESS["ERROR_TSZH_METER_INVALID_VERIFICATION_DATE"] = "Вказана неправильна дата повірки лічильника";
