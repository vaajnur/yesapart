<?
$MESS["CITRUS_TSZH_SUBSCRIBE_MIN_EDITION"] = "Стандарт";

$MESS["CITRUS_TSZH_SUBSCRIBE_ATTACHED_RECEIPT"] = "Квитанція знаходиться у вкладенні до цього листа.";
$MESS["CITRUS_TSZH_SUBSCRIBE_PDF"] = "Розсилка квитанцій в PDF";
$MESS["CITRUS_TSZH_SUBSCRIBE_PDF_ERROR"] = "При відправці сталася помилка";
$MESS["CITRUS_TSZH_SUBSCRIBE_PDF_DOM_NOT_LOADED"] = "Чи не завантажено розширення <a href=\"http://php.net/manual/ru/book.dom.php\" target=\"_blank\">DOM</a>";
$MESS["CITRUS_TSZH_SUBSCRIBE_PDF_GD_NOT_LOADED"] = "Чи не завантажено розширення <a href=\"http://php.net/manual/ru/book.image.php\" target=\"_blank\">GD</a>";
$MESS["CITRUS_TSZH_SUBSCRIBE_PDF_ERRORS"] = "Розсилка у форматі PDF недоступна. Для її роботи необхідна наявність додаткових модулів PHP.<br>Будь ласка, зверніться до свого хостинг-провайдеру або системного адміністратора.<br>";

$MESS["CITRUS_TSZH_SUBSCRIBE_ERROR_ADD_SUBSCRIBE"] = 'Помилка: не вдалося додати розсилку [#SUBSCRIBE_CODE#] "#SUBSCRIBE_NAME#" для організації [#TSZH_ID#] "#TSZH_NAME#". Дані розсилки: #SUBSCRIBE_ROW#.';

$MESS["CITRUS_TSZH_SUBSCRIBE_VDGB_PHONE_MOSCOW_NAME"] = "тел. Москва, М.О.";
$MESS["CITRUS_TSZH_SUBSCRIBE_VDGB_PHONE_REGIONS_NAME"] = "тел. Регіони";

$MESS["CITRUS_TSZH_SUBSCRIBE_FOR"] = "для";

$MESS["CITRUS_TSZH_SUBSCRIBE_addCustomEvent_ERROR_UNSUPPORTED_LANGUAGE"] = "Помилка: непідтримуваний мову: #LANG#";
$MESS["CITRUS_TSZH_SUBSCRIBE_addCustomEvent_ERROR_UNKNOWN_EVENT"] = "Помилка: невідомий тип поштового події: #EVENT_TYPE#";
$MESS["CITRUS_TSZH_SUBSCRIBE_addCustomEvent_ERROR_ADD_EVENT"] = "Помилка при додаванні типу поштового події #EVENT_TYPE#: #ERROR#";
$MESS["CITRUS_TSZH_SUBSCRIBE_addCustomEvent_ERROR_ADD_EVENT"] = "Помилка при додаванні шаблону поштового події #EVENT_TYPE#: #ERROR#";

$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_ID_FIELD"] = "Id";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_CODE_FIELD"] = "Код";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_TSZH_ID_FIELD"] = "Id ТВЖ";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_ACTIVE_FIELD"] = "Активність";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_RUNNING_FIELD"] = "Прапор виконання";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_PARAMS_FIELD"] = "Параметри";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_LAST_EXEC_FIELD"] = "Дата останнього запуску";
$MESS["CITRUS_TSZH_SUBSCRIBE_ENTITY_NEXT_EXEC_FIELD"] = "Дата наступного запуску";

$MESS["CITRUS_TSZH_UNSUBSCRIPTION_ENTITY_SUBSCRIBE_CODE_FIELD"] = "Код розсилки";
$MESS["CITRUS_TSZH_UNSUBSCRIPTION_ENTITY_USER_ID_FIELD"] = "Id користувача";

$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_DESCRIPTION"] = "Виконати розсилку";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_VALUE_YES"] = "Так";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_VALUE_NO"] = "Немає";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_TITLE"] = "При встановленій галочці розсилка почнеться після збереження елемента\n(розсилка доступна, починаючи з редакції «#EDITION#» продукту «#PRODUCT#»)";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS"] = "Статус";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_EMPTY"] = "не надсилали";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_SENDING"] = "розсилається";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_SENT"] = "розіслана";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_INTERRAPTED"] = "перервана";
$MESS["CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_ERROR"] = "помилка";
