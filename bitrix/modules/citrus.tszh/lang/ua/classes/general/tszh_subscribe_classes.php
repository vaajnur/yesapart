<?
$MESS["Common-ERROR-PARAM-MISSING"] = "Не задано значення параметра «#PARAM#»";

$MESS["Debtors-NAME"] = "Розсилка повідомлень боржникам";
$MESS["Debtors-NOTE-ADMIN"] = "У зазначений день місяця власникам особових рахунків, що мають заборгованість, висилаються листа з повідомленням про неї.";
$MESS["Debtors-DESCR-PUBLIC"] = "При наявності заборгованості, що перевищує певний розмір, висилається лист з повідомленням про неї.";
$MESS["Debtors-SETTING-ACTIVE-NAME"] = "Активна";
$MESS["Debtors-SETTING-ACTIVE-NAME-WIZARD"] = "Розсилати повідомлення боржникам";
$MESS["Debtors-PARAM-DATE-NAME"] = "День місяця для розсилки";
$MESS["Debtors-PARAM-DATE-ERROR-RANGE"] = "Значення параметра «#PARAM#» має бути в діапазоні від 1 до 31 включно";
$MESS["Debtors-PARAM-MIN_SUM-NAME"] = "Минимальная сумма задолженности для отправки уведомления, грн.";
$MESS["Debtors-PARAM-MIN_SUM-ERROR-MIN"] = "Значення параметра «#PARAM#» має бути не менше однієї копійки (0.01)";

$MESS["MeterValues-NAME"] = "Розсилка повідомлень про необхідність введення показань лічильників";
$MESS["MeterValues-NOTE-ADMIN"] = "У зазначений день місяця власникам особових рахунків відправляються повідомлення з нагадуванням про необхідність ввести показання лічильників на сайті (якщо вони не введені).";
$MESS["MeterValues-DESCR-PUBLIC"] = "При відсутності показань лічильників висилається лист з нагадуванням про необхідність ввести їх на сайті.";
$MESS["MeterValues-SETTING-ACTIVE-NAME"] = "Активна";
$MESS["MeterValues-SETTING-ACTIVE-NAME-WIZARD"] = "Розсилати повідомлення про необхідність введення показань лічильників";
$MESS["MeterValues-PARAM-DATE-NAME"] = "День місяця для розсилки";
$MESS["MeterValues-PARAM-DATE-ERROR-RANGE"] = "Значення параметра «#PARAM#» має бути в діапазоні від 1 до 31 включно";
$MESS["MeterValues-PARAM-LAST_PERIOD-NAME"] = "Останній період, за яким відправлені повідомлення";

$MESS["MeterVerifications-NAME"] = "Розсилка нагадувань про дату повірки лічильників";
$MESS["MeterVerifications-NOTE-ADMIN"] = "За вказану кількість днів до настання дати повірки лічильника його власнику відправиться повідомлення з нагадуванням про неї.";
$MESS["MeterVerifications-DESCR-PUBLIC"] = "При наближенні дати повірки лічильника висилається лист з нагадуванням про неї.";
$MESS["MeterVerifications-SETTING-ACTIVE-NAME"] = "Активна";
$MESS["MeterVerifications-SETTING-ACTIVE-NAME-WIZARD"] = "Розсилати нагадування про дату повірки лічильників";
$MESS["MeterVerifications-PARAM-CHECK_INTERVAL-NAME"] = "За скільки днів до настання дати повірки слід послати нагадування";
$MESS["MeterVerifications-PARAM-CHECK_INTERVAL-ERROR-INVALID"] = "Значення параметра «#PARAM#» має бути більше нуля";

$MESS["Receipts-NAME"] = "Розсилка квитанцій власникам особових рахунків";
$MESS["Receipts-NOTE-ADMIN"] = "Квитанції розсилаються власникам по мірі їх додавання на сайт (або завантаження з 1С або інший облікового системи).";
$MESS["Receipts-DESCR-PUBLIC"] = "Висилається лист з квитанцією на оплату послуг ЖКГ.";
$MESS["Receipts-SETTING-ACTIVE-NAME"] = "Активна";
$MESS["Receipts-SETTING-ACTIVE-NAME-WIZARD"] = "Розсилати квитанції на оплату по електронній пошті";
$MESS["Receipts-PARAM-SEND_PDF-NAME"] = "Розсилати у форматі PDF";
$MESS["Receipts-PARAM-SEND_PDF-ERROR"] = "Розсилка у форматі PDF недоступна. Для її роботи необхідна наявність додаткових модулів PHP.<br>Будь ласка, зверніться до свого хостинг-провайдеру або системного адміністратора.<br>";
$MESS["Receipts-PARAM-SEND_PDF-NOTE"] = "<span style=\"color: red;\">Увага!</span> Фунція розсилки квитанцій в PDF є експериментальною, створює підвищене навантаження на сервер і може призводити до формування файлів з неточним відображенням фрагментів квитанції.<br><br>Необхідно <a href=\"https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=37&LESSON_ID=5507\" target=\"_blank\">налаштувати роботу агентів з cron</a>, а також адаптувати шаблони квитанцій, перевірити зовнішній вигляд формуються PDF-файлів перед використанням цієї функції.";
$MESS["Receipts-PARAM-LAST_SENT_DATE-NAME"] = "Дата відправки останнього листа";

$MESS["News-ELEMENT-ERROR-addAgent"] = "помилка: не вдалося додати розсилає агент";
$MESS["News-ELEMENT-DESCR-sending"] = "розсилається";
$MESS["News-ELEMENT-DESCR-interrupted"] = "розсилка перервана адміністратором";
$MESS["News-ELEMENT-DESCR-sent"] = "розіслана #DATE#";
