<?
$MESS["TSZH_HOUSE_FLAT_ABBR"] = "кв.";
$MESS["TSZH_ERROR_NO_USER"] = "Не вказано користувача";
$MESS["TSZH_ERROR_EXTERNAL_XML_ID_DUPLICATED"] = "Особовий рахунок з таким зовнішнім кодом не існує";
$MESS["TSZH_ERROR_NO_TSZH_ID"] = "Не вказана прив'язка до ТСЖ";
$MESS["TSZH_ERROR_WRONG_ACCOUNT_ID"] = "Невірно вказано особовий рахунок.";
$MESS["TSZH_ERROR_WRONG_HISTORY_FIELDS"] = "Невірно вказані поля особового рахунку для історії";
$MESS["TSZH_ERROR_WRONG_HISTORY_DATE"] = "Невірно вказана дата запису для історії";
?>