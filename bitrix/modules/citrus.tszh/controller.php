<?

IncludeModuleLangFile(__FILE__);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/update_client_partner.php");

/**
 * �������������� ����������� � ����������� �� �������� � ������������ ������ ������ citrus.tszh
 */
class CTszhFunctionalityController
{
	/* Min �������� �������� ������ ��������, ����������� ����������� �������� */
	const SUBSCRIBE_MIN_SM_VERSION = "15.0.7";
	/* Min �������� ������ citrus.tszh, ���������� ���������� �������������� �������� */
	const SUBSCRIBE_MIN_TSZH_EDITION = "standard";

	private static $editionRegion = null;

	public static function GetEdition(&$edition = null)
	{
		$arEditionModules = Array(
			'start' => array("ru" => "citrus.tszhstart", "ua" => "vdgb.tszhstartua"),
			'standard' => array("ru" => "citrus.tszhstandard", "ua" => "vdgb.tszhstandardua"),
			'smallbusiness' => array("ru" => "citrus.tszhsb", "ua" => "vdgb.tszhsbua"),
			'expert' => array("ru" => "citrus.tszhe", "ua" => "vdgb.tszheua"),
			'business' => array("ru" => "citrus.tszhb", "ua" => "vdgb.tszhbua"),
		);

		$edition = false;
		$region = false;
		foreach ($arEditionModules as $strEdition => $arModuleIDs)
		{
			foreach ($arModuleIDs as $strRegion => $strModuleID)
			{
				if (IsModuleInstalled($strModuleID))
				{
					$edition = $strEdition;
					$region = $strRegion;
				}
			}
		}
		self::$editionRegion = $region;

		return $edition !== false ? $arEditionModules[$edition][$region] : false;
	}

	public static function CheckEdition($checkEdition = false)
	{
		static $demoNoticePrinted = false;

		if (defined("ADMIN_SECTION") && !class_exists("CAdminMessage"))
		{
			// ��� �������� ��������� �� ������, ��������� ��������� ����� ������������ �� ���������������� �����.
			// ���� ��� ���� ������� �������� ��� ����-������, ����� ��������� ������ Fatal error: Class 'CAdminMessage' not found,
			// �.�. ����� �� ���� ����� ���������� ��� �� ���������
			return true;
		}

		$edition = self::GetEdition($editionName);
		if (false === $edition)
		{
			if (defined("ADMIN_SECTION"))
			{
				echo CAdminMessage::ShowMessage(Array("DETAILS" => GetMessage("TSZH_MODULE_DEMO_NOTICE_ADMIN"), "TYPE" => "ERROR", "MESSAGE" => GetMessage("TSZH_MODULE_EDITION_NOT_FOUND_ERROR"), "HTML" => true));
			}
			else
			    {
                if (\Bitrix\Main\Config\Option::get('citrus.tszh', 'use_header_switcher_component',false, SITE_ID) === 'Y')
                {
                    if (!$demoNoticePrinted) {
                        echo GetMessage("TSZH_MODULE_EDITION_NOT_FOUND_ERROR_HEADER");
                        $demoNoticePrinted = true;
                    }
                }
                else {
                    if (!$demoNoticePrinted) {
                        ShowError(GetMessage("TSZH_MODULE_EDITION_NOT_FOUND_ERROR"));
                        $demoNoticePrinted = true;
                        }
                    }
                }
			return false;
		}

		if ($checkEdition && $checkEdition != $editionName)
			return false;

		switch (CModule::IncludeModuleEx($edition))
		{
			case MODULE_DEMO:
				if (defined("ADMIN_SECTION"))
				{
					$arMessage = Array(
						"DETAILS" => GetMessage("TSZH_MODULE_DEMO_NOTICE_ADMIN"),
						"TYPE" => "OK",
						"MESSAGE" => GetMessage("TSZH_MODULE_DEMO"),
						"HTML" => true,
					);

					$expireTimestamp = $GLOBALS["SiteExpireDate_" . str_replace(".", "_", $edition)];
					$installTimestamp = false;
					$rsModules = CModule::GetList();
					while ($arModule = $rsModules->Fetch())
						if ($arModule["ID"] == $edition)
							$installTimestamp = MakeTimeStamp($arModule["DATE_ACTIVE"], "YYYY-MM-DD HH:MI:SS");

					if ($installTimestamp && $expireTimestamp)
					{
						$strExpireDate = ConvertTimeStamp($expireTimestamp, "SHORT");
						$daysLeft = ceil(($expireTimestamp - time()) / (24 * 60 * 60));
						$strDaysLeft = "<b>$daysLeft</b> " . self::pluralForm($daysLeft, explode('|', GetMessage("CITRUS_DAYS_PLURAL")));
						$arMessage["DETAILS"] .= GetMessage("TSZH_MODULE_DEMO_EXPIRE_DATE", Array("#DATE#" => $strExpireDate, "#DAYS_LEFT#" => $strDaysLeft));
					}

					echo CAdminMessage::ShowMessage($arMessage);
				}
				else
				{
                    echo '<!--noindex-->';
                    if (\Bitrix\Main\Config\Option::get('citrus.tszh', 'use_header_switcher_component',false, SITE_ID) === 'Y')
                    {
                        if (!$demoNoticePrinted) {
                            echo GetMessage("TSZH_MODULE_DEMO_NOTICE_HEADER");
                            $demoNoticePrinted = true;
                        }

                    }
                    else {
                        if (!$demoNoticePrinted) {
                            ShowNote(GetMessage("TSZH_MODULE_DEMO"));
                            echo GetMessage("TSZH_MODULE_DEMO_NOTICE");
                            $demoNoticePrinted = true;
                        }
                    }
                    echo '<!--/noindex-->';
				}
				break;

			case MODULE_DEMO_EXPIRED:
				if (defined("ADMIN_SECTION"))
				{
					echo CAdminMessage::ShowMessage(Array("DETAILS" => GetMessage("TSZH_MODULE_DEMO_NOTICE_ADMIN"), "TYPE" => "MESSAGE", "MESSAGE" => GetMessage("TSZH_MODULE_DEMO_EXPIRED_ERROR"), "HTML" => true));
				}
				else
				{
                    echo '<!--noindex-->';
                    if (\Bitrix\Main\Config\Option::get('citrus.tszh', 'use_header_switcher_component',false, SITE_ID) === 'Y')
                    {
                        if (!$demoNoticePrinted) {
                            echo GetMessage("TSZH_MODULE_DEMO_NOTICE_EXPIRE_HEADER");
                            $demoNoticePrinted = true;
                        }
                    }
                    else {
                        if (!$demoNoticePrinted) {
                            ShowNote(GetMessage("TSZH_MODULE_DEMO_EXPIRED_ERROR"));
                            echo GetMessage("TSZH_MODULE_DEMO_NOTICE");
                            $demoNoticePrinted = true;
                        }
                    }
                    echo '<!--/noindex-->';
				}
				return false;

			case MODULE_NOT_FOUND:
				if (defined("ADMIN_SECTION"))
				{
					echo CAdminMessage::ShowMessage(Array("DETAILS" => GetMessage("TSZH_MODULE_DEMO_NOTICE_ADMIN"), "TYPE" => "ERROR", "MESSAGE" => GetMessage("TSZH_MODULE_EDITION_INCLUDE_ERROR"), "HTML" => true));
				}
				else
				{
                    echo '<!--noindex-->';
                    if (\Bitrix\Main\Config\Option::get('citrus.tszh', 'use_header_switcher_component',false, SITE_ID) === 'Y')
                    {
                        if (!$demoNoticePrinted) {
                            echo GetMessage("TSZH_MODULE_EDITION_INCLUDE_ERROR_HEADER");
                            $demoNoticePrinted = true;
                        }
                    }
                    else {
                        if (!$demoNoticePrinted) {
                            ShowError(GetMessage("TSZH_MODULE_EDITION_INCLUDE_ERROR"));
                            $demoNoticePrinted = true;
                        }
                    }
                    echo '<!--/noindex-->';
				}
				return false;

			case MODULE_INSTALLED:
			default:
				break;
		}

		return true;
	}

	/**
	 * ��������� min ����������� ��� ����������� �������� ������ �������� ������ (� ������ � ���������� ����� ������� ��������������),
	 * � ����� ������� ������ ��������, ���������� ���������� �������������� ��������
	 *
	 * @param bool $showNotice ����: ���� �� �������� ��������������
	 * @param bool $isSubscribeEdition ���������� �������: ����������� �������� ������ citrus.tszh, ���������� ���������� �������������� ��������
	 * @return bool true, ���� ����������� ������ ������ �������� ������
	 */
	public static function checkSubscribe($showNotice = false, &$isSubscribeEdition = null)
	{
		$isSubscribeSmVersion = checkVersion(SM_VERSION, self::SUBSCRIBE_MIN_SM_VERSION);
		$isSubscribeEdition = tszhCheckMinEdition(self::SUBSCRIBE_MIN_TSZH_EDITION);
		if ($showNotice && !$isSubscribeSmVersion && $isSubscribeEdition)
		{
			if (defined("ADMIN_SECTION") && ADMIN_SECTION === true)
			{
				$arMessage = Array(
						"TYPE" => "ERROR",
						"MESSAGE" => GetMessage("CITRUS_TSZH_FCONTROLLER_SUBSCRIBE_SM_VERSION_ADMIN_HEADER"),
						"DETAILS" => GetMessage("CITRUS_TSZH_FCONTROLLER_SUBSCRIBE_SM_VERSION_ADMIN_TEXT", array("#MIN_VERSION#" => self::SUBSCRIBE_MIN_SM_VERSION)),
						"HTML" => true,
				);
				CAdminMessage::ShowMessage($arMessage);
			}
			else
				ShowError(GetMessage("CITRUS_TSZH_FCONTROLLER_SUBSCRIBE_SM_VERSION_PUBLIC", array("#MIN_VERSION#" => self::SUBSCRIBE_MIN_SM_VERSION)));
		}

		return $isSubscribeSmVersion;
	}

	/**
	 * CTszhFunctionalityController::isUkrainianVersion()
	 * ����������, �������� �� ������� ��������� ������ citrus.tszh ���������� �������
	 *
	 * return true, ���� ������� ��������� ������ citrus.tszh �������� ���������� �������, ����� false
	 */
	public static function isUkrainianVersion()
	{
		if (is_null(self::$editionRegion))
			self::GetEdition();

		return self::$editionRegion == "ua";
	}

	/**
	 * ���������� ������ �� ����� ������
	 *
	 * @param mixed $paymentDescription - ������ ���������������� �������� (����������) ������� (���� �� �����, �� ����� ������������ �������� �� ���������)
	 * @param mixed $receiptPath - ������ URL ������ ��������� (���� �� �����, �� ����� ������������ �������� �� ���������)
	 * @return void
	 */
	public static function ShowPaymentForm($paymentDescription = false, $receiptPath = false)
	{
		if (!$GLOBALS["USER"]->IsAuthorized() || !CTszh::IsTenant())
			return false;

		// ���������� ������ ������
		if (CModule::IncludeModule("citrus.tszhpayment") && ($paymentPath = CTszhPaySystem::getPaymentPath(false, false)))
		{
			$arAccount = CTszhAccount::GetByUserID($GLOBALS["USER"]->GetID());
			$arLastPeriod = CTszhAccountPeriod::GetList(
				Array("PERIOD_DATE" => "DESC", "PERIOD_ID" => "DESC"),
				Array(
					'ACCOUNT_ID' => $arAccount["ID"],
					"PERIOD_TSZH_ID" => $arAccount["TSZH_ID"],
					"PERIOD_ACTIVE" => "Y",
				),
				false,
				Array('nTopCount' => 1),
				Array('ID', "DEBT_END")
			)->Fetch();

			$serverName = SITE_SERVER_NAME ? SITE_SERVER_NAME : (COption::GetOptionString('main', 'server_name', $_SERVER['HTTP_HOST']));
			if ($arLastPeriod && $arLastPeriod["DEBT_END"] > 0)
				echo GetMessage("CITRUS_TSZHPAYMENT_LINK", Array("#LINK#" => $paymentPath . '?summ=' . round($arLastPeriod["DEBT_END"])));
		}
		// TODO ������ ������ ������ �������� ������� �� ����������, ��� �������� ��� ������ ��������, ������� �� ���������� ������ ������
		elseif (self::CheckEdition('standard'))
		{
			$arAccount = CTszhAccount::GetByUserID($GLOBALS["USER"]->GetID());
			$arLastPeriod = CTszhAccountPeriod::GetList(
				Array("PERIOD_DATE" => "DESC", "PERIOD_ID" => "DESC"),
				Array(
					'ACCOUNT_ID' => $arAccount["ID"],
					"PERIOD_TSZH_ID" => $arAccount["TSZH_ID"],
					"PERIOD_ACTIVE" => "Y",
				),
				false,
				Array('nTopCount' => 1),
				Array('ID')
			)->Fetch();

			CTszhYandexPayment::ShowPaymentForm(isset($arLastPeriod["ID"]) ? $arLastPeriod["ID"] : false, $arAccount["ID"], $paymentDescription, $receiptPath);
		}
	}

	/**
	 * CTszhFunctionalityController::ReceivePayment()
	 * ��������� ���������� �� ������ �� ��������� ������ � ����������� �� �������� ������ citrus.tszh
	 */
	public static function ReceivePayment()
	{
		global $APPLICATION;

		if (!self::CheckEdition())
			return false;

		$paymentModule = CModule::IncludeModule("citrus.tszhpayment");

		if ($paymentModule)
		{
			$APPLICATION->IncludeComponent(
				"citrus:tszh.payment.receive",
				"",
				Array(
					"PAY_SYSTEM_ID" => $_REQUEST["paySystemID"],
				),
				false
			);
		}
		else
		{
			ShowError("citrus.tszhpayment module is not installed!");
		}
	}

	/**
	 * �������� ���������� �� ������ �������
	 */
	public static function isPortal()
	{
		return \CModule::IncludeModule("vdgb.portaltszh") && defined("TSZH_PORTAL") && TSZH_PORTAL === true;
	}

	/**
	 * @param bool $showNotify �������� ��������� ��������������
	 *
	 * @return bool ����� ���� ������� � �����������
	 */
	public static function CheckUpdatePeriod($showNotify = true)
	{
		if (class_exists('CUpdateClientPartner'))
		{
			$arUpdateList = CUpdateClientPartner::GetUpdatesList($errorMessage, LANG, "Y", array(), Array("fullmoduleinfo" => "Y"));
			$edition = self::GetEdition();

			//���� ������ ��������� � ���������� �������� ������ ��������
			if ($arUpdateList && ($edition != false))
			{
				//���������� ������ ��������� ��� ����������
				foreach ($arUpdateList["MODULE"] as $arUpdate)
				{
					$arModule = $arUpdate["@"];
					// ���� ����� ���� �������
					if ($edition == $arModule["ID"])
					{
						$dateNow = new DateTime("now");
						$dateTo = new DateTime($arModule["DATE_TO"]);

						// false - ���� ������ ���������� �����
						$result = $dateTo >= $dateNow;

						//��� ������������ ���������
						$tag = "EndUpdate";

						//������� �� ������ ���������
						CAdminNotify::DeleteByTag($tag);

						// ����� ���������, ���� ����������
						if ($showNotify)
						{
							// ������� (� ����) ����� ������� ����� � ��������� ����� ����������
							$daysTo = date_diff($dateNow, $dateTo)->days;

							// ������ ��������� � ����������� �� ��������� ����� ����������
							if ($result)
							{
								// ���, � ������� ��������� ����� ����������� � �������������� ������ ��� ������ � ���������
								$arLinkDaysTo = array(
									30 => "//vgkh.ru/~QNWHV",
									7 => "//vgkh.ru/~L8bL5",
								);

								$message = GetMessage("TSZH_MODULE_UPDATE_TO_END", Array(
									"#DATE#" => $arModule["DATE_TO"],
									"#LINK#" => $arLinkDaysTo[$daysTo],
								));
							}
							else
							{
								
								$arLinkDaysTo = array(
									15 => "//vgkh.ru/~QlFod",
									30 => "//vgkh.ru/~Cbn9O",
								);
								//��������� ����� ��������� ������ 60 ����
								if ($daysTo % 60 == 0)
								{
									$arLinkDaysTo[$daysTo] = "//vgkh.ru/~ksusy";
								}

								$message = GetMessage("TSZH_MODULE_UPDATE_END", Array(
									"#DATE#" => $arModule["DATE_TO"],
									"#LINK#" => $arLinkDaysTo[$daysTo],
								));
							}

							if (array_key_exists($daysTo, $arLinkDaysTo))
							{
								$arNotify = array(
									"MESSAGE" => $message,
									"TAG" => $tag,
									"ENABLE_CLOSE" => "Y",
								);

								CAdminNotify::add($arNotify);
							}

						}

						return $result;
					}
				}

			}
		}

		return false;
	}
}
?>