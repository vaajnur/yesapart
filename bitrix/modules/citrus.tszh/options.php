<?
/**
 * ������ ���ƻ
 * �������� �������� ������
 * @package tszh
 */

use Bitrix\Main\Config\Option;
use Citrus\Tszh\TszhTable;

require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/citrus.tszh/include.php");

$POST_RIGHT = $APPLICATION->GetGroupRight(TSZH_MODULE_ID);
if ($POST_RIGHT < "W")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/citrus.tszh/include.php");
IncludeModuleLangFile(__FILE__);

$module_id = 'citrus.tszh';

$arGROUPS = array();
$arGROUPS_tmp = array();
$z = CGroup::GetList($v1, $v2, array(
	"ACTIVE" => "Y",
	"ADMIN" => "N",
	"ANONYMOUS" => "N",
));
while ($zr = $z->Fetch())
{
	$ar = array();
	$ar["ID"] = intval($zr["ID"]);
	$ar["NAME"] = htmlspecialcharsbx($zr["NAME"])
	              . " [<a title=\"" . GetMessage("MAIN_USER_GROUP_TITLE")
	              . "\" href=\"/bitrix/admin/group_edit.php?ID="
	              . intval($zr["ID"]) . "&amp;lang=" . LANGUAGE_ID
	              . "\">" . intval($zr["ID"]) . "</a>]";
	$groups[$zr["ID"]] = "[" . $zr["ID"] . "] " . $zr["NAME"];
	$arGROUPS[] = $ar;
	$arGROUPS_tmp[] = array(
		'ID' => $ar["ID"],
		'NAME' => htmlspecialcharsbx($zr["NAME"]),
	);
}

$arServiceOptions = Array(
	"user_group_id",
	//	"meters_block_edit"
);

$arAllOptions = Array(
	GetMessage("TSZH_OPT_SYSTEM_SETTINGS"),
	Array(
		"user_group_id",
		GetMessage("TSZH_OPT_USER_GROUP"),
		"",
		Array("multiselectbox", $groups),
	),
	Array(
		"meters_block_edit",
		GetMessage("TSZH_OPT_METERS_BLOCK_EDIT"),
		"",
		Array("checkbox"),
	),
	Array(
		"tszh_module_caption",
		GetMessage("TSZH_OPT_MODULE_CAPTION"),
		"",
		Array("text", 20),
	),
);

if (CModule::IncludeModule("citrus.tszhpayment"))
{
	$arAllOptions = array_merge($arAllOptions, array(
		GetMessage("CITRUS_TSZH_PAYMENT_OPTIONS"),
		Array(
			"pay_to_executors_only",
			GetMessage("CITRUS_TSZH_PAYMENT_EXECUTORS_ONLY"),
			"",
			Array("checkbox"),
		),
		Array("note" => GetMessage("CITRUS_TSZH_PAYMENT_EXECUTORS_ONLY_DESC")),
	));
}

if (CTszhFunctionalityController::checkSubscribe(false, $isSubscribeEdition) && $isSubscribeEdition)
{
	$arAllOptions = array_merge($arAllOptions, array(
		GetMessage("CITRUS_TSZH_SUBSCRIBE_GROUP"),
		array(
			"subscribe_max_emails_per_hit",
			GetMessage("CITRUS_TSZH_SUBSCRIBE_MAX_EMAILS_PER_HIT"),
			"",
			Array("text", 5),
		),
		array(
			"subscribe_posting_interval",
			GetMessage("CITRUS_TSZH_SUBSCRIBE_POSTING_INTERVAL"),
			"",
			Array("text", 5),
		),
	));
}

$arAllOptions = array_merge($arAllOptions, array(
	GetMessage("CITRUS_TSZH_NOTIFICATION_GROUP"),
	array(
		"hidden_notification",
		GetMessage("CITRUS_TSZH_HIDDEN_NOTIFICATION"),
		"",
		Array("checkbox"),
	),
	array(
		"notification_dummyMail",
		GetMessage("CITRUS_TSZH_NOTIFICATION_DUMMYMAIL"),
		"",
		Array("checkbox"),
	),
	array(
		"notification_emptyPhone",
		GetMessage("CITRUS_TSZH_NOTIFICATION_EMPTYPHONE"),
		"",
		Array("checkbox"),
	),
	array(
		"notification_confirmAccount",
		GetMessage("CITRUS_TSZH_NOTIFICATION_CONFIRMACCOUNT"),
		"",
		Array("checkbox"),
	),
	Array("note" => GetMessage("CITRUS_TSZH_NOTIFICATION_CONFIRMACCOUNT_NOTE")),
	array(
		"personal_path",
		GetMessage("CITRUS_TSZH_PERSONAL_PATH"),
		"",
		Array(
			"text",
			20,
		),
	),
));

$arAllOptions = array_merge($arAllOptions, array(
	GetMessage("CITRUS_TSZH_POST354_GROUP"),
	Array(
		"post_354_ShowPenaltiesColumn",
		GetMessage("CITRUS_TSZH_POST354_SHOW_PENALTIES_COLUMN"),
		"",
		Array("checkbox"),
	),
	Array(
		"shared_meters",
		GetMessage("CITRUS_TSZH_SHARED_METERS"),
		"",
		Array(
			"checkbox",
		),
	),
	Array("note" => GetMessage("CITRUS_TSZH_SHARED_METERS_NOTE")),
));

// ��������� ��� ������� "������ �������"
$arEventLogOptions = array(
	\Bitrix\Main\Localization\Loc::getMessage('TSZH_OPTIONS__EVENT_LOG'),
	array(
		'event_log_account_add',
		\Bitrix\Main\Localization\Loc::getMessage('TSZH_OPTIONS__EVENT_LOG_ACCOUNT_ADD'),
		'',
		array('checkbox'),
	),
	array(
		'event_log_account_update',
		\Bitrix\Main\Localization\Loc::getMessage('TSZH_OPTIONS__EVENT_LOG_ACCOUNT_UPDATE'),
		'',
		array('checkbox'),
	),
	array(
		'event_log_account_delete',
		\Bitrix\Main\Localization\Loc::getMessage('TSZH_OPTIONS__EVENT_LOG_ACCOUNT_DELETE'),
		'',
		array('checkbox'),
	),
);

$aTabs = array(
	array(
		"DIV" => "edit1",
		"TAB" => GetMessage("MAIN_TAB_SET"),
		"ICON" => "tszh_settings",
		"TITLE" => GetMessage("MAIN_TAB_TITLE_SET"),
	),
	array(
		"DIV" => "edit2",
		"TAB" => GetMessage("MAIN_TAB_GROUPS"),
		"ICON" => "tszh_groups",
		"TITLE" => GetMessage("MAIN_TAB_TITLE_GROUPS"),
	),
	array(
		"DIV" => "edit3",
		"TAB" => GetMessage("MAIN_TAB_1C_EXCHANGE"),
		"ICON" => "tszh_1c_exchange",
		"TITLE" => GetMessage("MAIN_TAB_1C_EXCHANGE"),
	),
	array(
		"DIV" => "edit4",
		"TAB" => GetMessage("MAIN_TAB_EVENT_LOG"),
		"ICON" => "tszh_event_log",
		"TITLE" => GetMessage("MAIN_TAB_EVENT_LOG"),
	),
	array(
		"DIV" => "edit5",
		"TAB" => GetMessage("CITRUS_TSZH_ADDITION"),
		"ICON" => "tszh_addition",
		"TITLE" => GetMessage("CITRUS_TSZH_ADDITION"),
	),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);

ob_start();
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php");
$htmlGroupRights = ob_get_contents();
ob_end_clean();

$strRedirectURL = false;
if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && check_bitrix_sessid())
{
	if (strlen($RestoreDefaults) > 0)
	{
		// �������� ��������� ���������
		$arSaveOptions = Array();
		foreach ($arServiceOptions as $sServiceOption)
		{
			$Value = COption::GetOptionString('tszg', $sServiceOption);
			$arSaveOptions[$sServiceOption] = $Value;
		}
		COption::RemoveOption(TSZH_MODULE_ID);
		foreach ($arServiceOptions as $sServiceOption)
		{
			COption::SetOptionString(TSZH_MODULE_ID, $sServiceOption, $arSaveOptions[$sServiceOption]);
			$arSaveOptions[$sServiceOption] = $Value;
		}
	}
	else
	{
		foreach ($arAllOptions as $arOption)
		{
			__AdmSettingsSaveOption(TSZH_MODULE_ID, $arOption);
		}

		foreach ($arEventLogOptions as $arOption)
		{
			__AdmSettingsSaveOption(TSZH_MODULE_ID, $arOption);
		}

		COption::SetOptionString('citrus.tszh', '1c_exchange.UpdateMode', $_REQUEST['updateMode']);
		COption::SetOptionString('citrus.tszh', '1c_exchange.CreateUsers', $_REQUEST['createUsers']);
		//COption::SetOptionString('citrus.tszh', '1c_exchange.UpdateUsers', ($_REQUEST['dontUpdateUsers'] == "Y" ? "N" : "Y"));
		COption::SetOptionString('citrus.tszh', '1c_exchange.Action', $_REQUEST["outFileAction"]);
		COption::SetOptionString('citrus.tszh', '1c_exchange.Depersonalize', $_REQUEST['depersonalize']);
		COption::SetOptionInt('citrus.tszh', '1c_exchange.Interval', IntVal($_REQUEST["INTERVAL"]) > 0 ? IntVal($_REQUEST["INTERVAL"]) : 0);
		COption::SetOptionString('citrus.tszh', '1c_exchange.Debug', $_REQUEST['debug'] == "Y" ? "Y" : "N");
		COption::SetOptionInt('citrus.tszh', '1C_FILE_SIZE_LIMIT', IntVal($_REQUEST["FILESIZE_LIMIT"]) > 0 ? IntVal($_REQUEST["FILESIZE_LIMIT"]) : 0);

		COption::SetOptionString('citrus.tszh', 'input_phone_add', $_REQUEST["input_phone_add"] == "Y" ? "Y" : "N");
		COption::SetOptionString('citrus.tszh', 'input_phone_require', ($_REQUEST["input_phone_add"] == "Y") && ($_REQUEST["input_phone_require"] == "Y") ? "Y" : "N");

		// ��� ���� ����� ����������� �� ������� � �������� ��������� ����� ������� ��� ���������, ����� ��������� ��������
		$resTszh = TszhTable::getList(
			array(
				'select' => array('ID'),
			)
		);
		while ($arTszh = $resTszh->fetch())
		{
			CTszh::postValidate($arTszh['ID']);
		}
	}
	if (strlen($Update) > 0 && strlen($_REQUEST["back_url_settings"]) > 0)
	{
		LocalRedirect($_REQUEST["back_url_settings"]);
	}
	else
	{
		LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
	}
}

CTszhFunctionalityController::checkSubscribe(true);

$aMenu = array(
	array(
		"TEXT" => GetMessage("TSZH_OPT_IMPORT"),
		"LINK" => "tszh_import.php",
		"TITLE" => GetMessage("TSZH_OPT_IMPORT_TITLE"),
	),
	array(
		"TEXT" => GetMessage("TSZH_OPT_EXPORT"),
		"LINK" => "tszh_export.php",
		"TITLE" => GetMessage("TSZH_OPT_EXPORT_TITLE"),
	),
);
$context = new CAdminContextMenu($aMenu);
$context->Show();

$tabControl->Begin();
?>
	<form method="post" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?=urlencode($mid)?>&amp;lang=<?=LANGUAGE_ID?>">
		<?=bitrix_sessid_post();?>
		<?
		$tabControl->BeginNextTab();
		foreach ($arAllOptions as $arOption):
			__AdmSettingsDrawRow(TSZH_MODULE_ID, $arOption);
		endforeach;

		$tabControl->BeginNextTab();
		echo $htmlGroupRights;

		$tabControl->BeginNextTab();
		?>
		<tr valign="top" class="heading">
			<td colspan="2"><? echo GetMessage("TI_COMMON_TITLE") ?></td>
		</tr>
		<tr valign="top">
			<td><?=GetMessage("TI_FILESIZE")?>:</td>
			<td>
				<input type="text" name="FILESIZE_LIMIT" value="<?=COption::GetOptionInt('citrus.tszh', '1C_FILE_SIZE_LIMIT', 262144)?>"/>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="debug"><? echo GetMessage("CITRUS_TSZH_EXCHANGE_DEBUG") ?>:</label></td>
			<td><?
				$debug = COption::GetOptionString('citrus.tszh', '1c_exchange.Debug', "Y");
				?>
				<input type="checkbox" name="debug" id="debug" value="Y" <?=$debug == 'Y' ? 'checked="checked"' : ''?> /></label>
				<br>
				<?=GetMessage("CITRUS_TSZH_EXCHANGE_DEBUG_INFO")?>
			</td>
		</tr>
		<tr valign="top" class="heading">
			<td colspan="2"><? echo GetMessage("TI_IMPORT_TITLE") ?></td>
		</tr>
		<tr valign="top">
			<td><?=GetMessage("TI_UPDATE_MODE")?>:</td>
			<td>

				<label><input type="radio" name="updateMode" value=""<?
					if (!in_array(COption::GetOptionString('citrus.tszh', '1c_exchange.UpdateMode', "Y"), Array(
						"Y",
						"D",
					)))
					{
						echo ' checked="checked"';
					}
					?> /><? echo GetMessage("TI_UPDATE_MODE_NO") ?></label><br/>

				<label title="<?=GetMessage("TI_NOTE_1")?>"><input type="radio" name="updateMode" value="Y"<?
					if (COption::GetOptionString('citrus.tszh', '1c_exchange.UpdateMode', "Y") == 'Y')
					{
						echo ' checked="checked"';
					}
					?> /><? echo GetMessage("TI_UPDATE_MODE_TITLE") ?><span class="required"><sup>1</sup></span></label><br/>

				<label title="<?=GetMessage("TI_NOTE_2")?>"><input type="radio" name="updateMode" value="D"<?
					if (COption::GetOptionString('citrus.tszh', '1c_exchange.UpdateMode', "Y") == 'D')
					{
						echo ' checked="checked"';
					}
					?> /><? echo GetMessage("TI_UPDATE_MODE_ONLY_DEBT") ?><span class="required"><sup>2</sup></span></label><br/>
			</td>
		</tr>
		<tr valign="top">
			<td><?=GetMessage("TI_USERS")?>:</td>
			<td>
				<label style="margin-left: 7px" title="<?=GetMessage("TI_NOTE_3")?>"><input type="checkbox" name="createUsers" id="createUsers" value="Y"
				                                       onchange="CreateUsersChanged();"<?
					if (COption::GetOptionString('citrus.tszh', '1c_exchange.CreateUsers', "Y") == 'Y')
					{
						echo ' checked="checked"';
					}
					?> /><? echo GetMessage("TI_CREATE_USERS") ?><span class="required"><sup>3</sup></span></label><br/>
			</td>
		</tr>
		<tr valign="top">
			<td><? echo GetMessage("TI_ACTION") ?>:</td>
			<td><?
				$action = COption::GetOptionString('citrus.tszh', '1c_exchange.Action', "N");
				?>
				<input type="radio" name="outFileAction" value="N" id="outFileAction_N"<?=($action == 'N' ? ' checked="checked"' : '')?> /><label
						for="outFileAction_N"><? echo GetMessage("TI_ACTION_NONE") ?></label><br>
				<input type="radio" name="outFileAction" value="A" id="outFileAction_A"<?=($action == 'A' ? ' checked="checked"' : '')?> /><label
						for="outFileAction_A"><? echo GetMessage("TI_ACTION_DEACTIVATE") ?></label><br>
				<?
				if ($action == 'D')
				{
					?>
					<input type="radio" name="outFileAction" value="D" id="outFileAction_D"<?=($action == 'D' ? ' checked="checked"' : '')?> /><label
						for="outFileAction_D"><? echo GetMessage("TI_ACTION_DELETE") ?></label><br>
					<?
				}
				?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="depersonalize"><? echo GetMessage("TI_DEPERSONALIZE") ?>:</label></td>
			<td><?
				$depersonalize = COption::GetOptionString('citrus.tszh', '1c_exchange.Depersonalize', "N");
				?>
				<input type="checkbox" name="depersonalize" id="depersonalize"
				       value="Y" <?=$depersonalize == 'Y' ? 'checked="checked"' : ''?> /></label>
			</td>
		</tr>
		<tr>
			<td></td>
			<td align="left">
				<? echo BeginNote(); ?>
				<? echo GetMessage("TI_DEPERSONALIZE_ANNOTATION_1"); ?>
				<ul>
					<li>
						<? echo GetMessage("TI_DEPERSONALIZE_ANNOTATION_2"); ?>
					</li>
					<li>
						<? echo GetMessage("TI_DEPERSONALIZE_ANNOTATION_3"); ?>
					</li>
					<li>
						<? echo GetMessage("TI_DEPERSONALIZE_ANNOTATION_4"); ?>
					</li>
				</ul>
				<? echo EndNote(); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><? echo GetMessage("TI_INTERVAL") ?>:</td>
			<td><?
				$maxTime = intval(ini_get('max_execution_time'));
				$INTERVAL = COption::GetOptionInt('citrus.tszh', '1c_exchange.Interval', min($maxTime > 10 ? $maxTime - 5 : 10, 60));
				?>
				<input type="text" id="INTERVAL" name="INTERVAL" size="5" value="<? echo intval($INTERVAL) ?>">
			</td>
		</tr>
		<tr valign="top" class="heading">
			<td colspan="2"><? echo GetMessage("TI_ADDITIONAL_TITLE") ?></td>
		</tr>
		<tr valign="top">
			<td colspan="2">
				<? echo BeginNote(); ?>
				<span class="required"><sup>1</sup></span> <?=GetMessage('TI_NOTE_1')?><br/>
				<span class="required"><sup>2</sup></span> <?=GetMessage('TI_NOTE_2')?><br/>
				<? /* <span class="required"><sup>3</sup></span> <?=GetMessage('TI_NOTE_3')?><br /> */ ?>
				<? echo EndNote(); ?>
			</td>
		</tr>
		<?
		$tabControl->BeginNextTab();
		foreach ($arEventLogOptions as $arOption):
			__AdmSettingsDrawRow(TSZH_MODULE_ID, $arOption);
		endforeach;
		?>
		<? $tabControl->BeginNextTab(); ?>
		<tr class="heading">
			<td colspan="2">
				<?=GetMessage("CITRUS_TSZH_ADDITION_USERS_DATA")?>
			</td>
		</tr>
		<tr>
			<td width="50%" align="right">
				<?=GetMessage("CITRUS_TSZH_ADDITION_PHONE_ADD")?>
			</td>
			<td width="50%" align="left">
				<input id="input_phone_add" type="checkbox" size="40" value="Y"
					<?=COption::GetOptionString('citrus.tszh', 'input_phone_add', "N") == 'Y' ? ' checked' : ''?> name="input_phone_add"
					   onclick="ChangedDisabledOption('input_phone_add', 'input_phone_require')">
			</td>
		</tr>
		<tr>
			<td>
				<?=GetMessage("CITRUS_TSZH_ADDITION_PHONE_REQUIRE")?>
			</td>
			<td>
				<input id="input_phone_require" type="checkbox" size="40" value="Y"
					<?=COption::GetOptionString('citrus.tszh', 'input_phone_add', "N") == 'Y' ? '' : 'disabled'?>
					<?=COption::GetOptionString('citrus.tszh', 'input_phone_require', "N") == 'Y' ? ' checked' : ''?> name="input_phone_require">
			</td>
		</tr>
		<? $tabControl->Buttons(); ?>
		<input type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>" class="adm-btn-save">
		<input type="hidden" name="Update" value="Y">
		<? if (strlen($_REQUEST["back_url_settings"]) > 0): ?>
			<input type="button" name="Cancel" value="<?=GetMessage("MAIN_OPT_CANCEL")?>" title="<?=GetMessage("MAIN_OPT_CANCEL_TITLE")?>"
			       onclick="window.location = '<? echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
			<input type="hidden" name="back_url_settings" value="<?=htmlspecialcharsbx($_REQUEST["back_url_settings"])?>">
		<? endif ?>
		<input type="submit" name="RestoreDefaults" title="<? echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
		       OnClick="return confirm('<? echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
		       value="<? echo GetMessage("MAIN_RESTORE_DEFAULTS") ?>">
	</form>

	<script type="text/javascript">
        function ChangedDisabledOption(mainOption, disabledOption) {
            if (!document.getElementById(mainOption).checked)
                document.getElementById(disabledOption).disabled = true;
            else
                document.getElementById(disabledOption).disabled = false;

        }

        //<!--
        function CreateUsersChanged() {
            bCreateUsers = document.getElementById('createUsers').checked;
            // document.getElementById('dontUpdateUsers').disabled = !bCreateUsers;
        }

        CreateUsersChanged();
        //-->
	</script>
<? $tabControl->End(); ?>