<?

/**
 * tszhLookup()
 *
 * @param string $tagName ��� ���� ��� ����� ID ��������
 * @param integer $tagValue �������� ���� ��� ����� ID ��������
 * @param array $arParams ������ ����������
 * @return
 */
function tszhLookup($tagName, $tagValue, $arParams = Array(
	'type' => 'account',
	'selectedText' => '',
	'formName' => 'form1',
	'tagSize' => 3,
	'tagMaxLength' => '',
	'buttonValue' => '...',
	'tagClass' => 'typeinput',
	'buttonClass' => 'tablebodybutton',
)) {

	if (strlen($tagValue) > 0)	
		$tagValue = IntVal($tagValue);

	if (!is_array($arParams)) {
		$arParams = Array();
	}
	$arDefaultParams = Array(
		'type' => 'account',
		'selectedText' => '',
		'formName' => 'form1',
		'tagSize' => 3,
		'tagMaxLength' => '',
		'buttonValue' => '...',
		'tagClass' => 'typeinput',
		'buttonClass' => 'tablebodybutton',
	);
	foreach ($arDefaultParams as $strParam => $varParamValue) {
		if (!array_key_exists($strParam, $arParams) || strlen($arParams[$strParam]) <= 0) {
			$arParams[$strParam] = $varParamValue;
		}
	}

	switch ($arParams['type']) {
		case 'service':
			$arParams['selectPage'] = '/bitrix/admin/tszh_service_search.php';
			$arParams['getPage'] = '/bitrix/admin/tszh_get_service.php';
			break;

		case 'period':
			$arParams['selectPage'] = '/bitrix/admin/tszh_period_search.php';
			$arParams['getPage'] = '/bitrix/admin/tszh_get_period.php';
			break;

		case 'house':
			$arParams['selectPage'] = '/bitrix/admin/tszh_house_search.php';
			$arParams['getPage'] = '/bitrix/admin/tszh_get_house.php';
			break;

		case 'account':
		case 'default':
			$arParams['selectPage'] = '/bitrix/admin/tszh_account_search.php';
			$arParams['getPage'] = '/bitrix/admin/tszh_get_account.php';
			break;
	}


	global $APPLICATION;
	$tag_name_x = preg_replace("/([^a-z0-9]|\[|\])/is", "x", $tagName);
	$lang = LANG;
	if($APPLICATION->GetGroupRight("citrus.tszh") >= "R")
	{
		$strReturn = <<<HTML
<input type="text" name="{$tagName}" id="{$tagName}" value="{$tagValue}" size="{$arParams['tagSize']}" maxlength="{$arParams['tagMaxLength']}" class="{$arParams['tagClass']}">
<iframe style="width:0px; height:0px; border:0px" src="javascript:''" name="hiddenframe{$tagName}" id="hiddenframe{$tagName}"></iframe>
<input class="{$arParams['buttonClass']}" type="button" name="tszhLookup" id="tszhLookup" OnClick="window.open('{$arParams['selectPage']}?lang=$lang&FN={$arParams['formName']}&FC={$tagName}', '', 'scrollbars=yes,resizable=yes,width=760,height=500,top='+Math.floor((screen.height - 560)/2-14)+',left='+Math.floor((screen.width - 760)/2-5));" value="{$arParams['buttonValue']}">
<span id="div_{$tagName}">{$arParams['selectedText']}</span>
<script type="text/javascript">
HTML;

		if($arParams['selectedText']=="")
			$strReturn.= "var tv".$tag_name_x."='';\n";
		else
			$strReturn.= "var tv".$tag_name_x."='".CUtil::JSEscape($tagValue)."';\n";

		$strReturn.= "
function Ch".$tag_name_x."()
{
	var DV_".$tag_name_x.";
	DV_".$tag_name_x." = document.getElementById(\"div_".$tagName."\");
	if (tv".$tag_name_x."!=document.".$arParams['formName']."['".$tagName."'].value)
	{
		tv".$tag_name_x."=document.".$arParams['formName']."['".$tagName."'].value;
		if (tv".$tag_name_x."!='')
		{
			DV_".$tag_name_x.".innerHTML = '<i>".GetMessage("MAIN_WAIT")."</i>';
			document.getElementById(\"hiddenframe".$tagName."\").src='{$arParams['getPage']}?ID=' + tv".$tag_name_x."+'&strName=".$tagName."&lang=".LANG.(defined("ADMIN_SECTION") && ADMIN_SECTION===true?"&admin_section=Y":"")."';
		}
		else
			DV_".$tag_name_x.".innerHTML = '';
	}
	setTimeout(function(){Ch".$tag_name_x."()},1000);
}
Ch".$tag_name_x."();
//-->
</script>
";
	}
	else
	{
		$strReturn = <<<HTML
			<input type="text" name="$tagName" id="$tagName" value="$tagValue" size="{$arParams['tagSize']}" maxlength="{$arParams['strMaxLength']}">
			<input type="button" name="tszhLookup" id="tszhLookup" OnClick="window.open('{$arParams['searchPage']}?lang=$lang&FN={$arParams['formName']}&FC=$tagName', '', 'scrollbars=yes,resizable=yes,width=760,height=560,top='+Math.floor((screen.height - 560)/2-14)+',left='+Math.floor((screen.width - 760)/2-5));" value="{$arParams['buttonValue']}">
			{$arParams['selectedText']}
HTML;
	}
	return $strReturn;
}

/**
 * ����� ��� ������ � ��������� ��������� (��������� ���������� citrus:tszh.receipt)
 * ������������ ��� ��������� ������ ��������� �� ������� (�� �������� "���������" (tszh_account_period_list.php))
 *
 */
IncludeModuleLangFile(__FILE__);
class CTszhReceiptTemplateProcessor
{
	const AUTO_DETECT = "*AUTO*";

	const COMPONENT_TSZH = "citrus:tszh";

	const REL_PATH_RECEIPT = '/components/citrus/tszh.receipt';
	const REL_PATH_TSZH = '/components/citrus/tszh';

	private function getTemplateName($templateID)
	{
		$result = false;

		switch ($templateID)
		{
			case ".default":
				$result = getMessage("TRTP_TEMPLATE_NAME_DEAFAULT");
				break;

			case "post-354":
				$result = getMessage("TRTP_TEMPLATE_NAME_POST354");
				break;

			case "epd-1161":
				$result = getMessage("TRTP_TEMPLATE_NAME_EPD1161");
				break;
		}

		return $result;
	}

	private function checkDirEntry($entry, $entryFull)
	{
		return !in_array($entry, array(".", "..")) && @is_dir($entryFull);
	}

	private function getContainersFromThemes($siteFolder, $themesContainer)
	{
		$arResult = array();

		$path = $_SERVER['DOCUMENT_ROOT'] . $siteFolder . $themesContainer;
		$arEntries = scandir($path);
		foreach ($arEntries as $entry)
		{
			$entryFull = $path . "/" . $entry;
			if (!self::checkDirEntry($entry, $entryFull))
				continue;
        
			$part = "{$themesContainer}/{$entry}/citrus/tszh.receipt";
			$arResult[$part] = $siteFolder . $part;
		}

		return $arResult;
	}

	private function __getTemplates($receiptTemplatesFolder)
	{
		$arResult = array();

		$path = $_SERVER['DOCUMENT_ROOT'] . $receiptTemplatesFolder;

		if (!@is_dir($path))
			return $arResult;

		$arEntries = scandir($path);
		foreach ($arEntries as $entry)
		{
			$entryFull = $path . '/' . $entry;
			if (!self::checkDirEntry($entry, $entryFull))
				continue;

			// ����� ������� ������ ��������� ���� template.php
			$arTemplateEntries = scandir($entryFull);
			foreach ($arTemplateEntries as $templateEntry)
			{
				if ($templateEntry == 'template.php' && @is_file($entryFull . '/' . $templateEntry))
					$arResult[] = $entry;
			}
		}

		return $arResult;
	}

	private function __getTszhComponentTemplates($tszhTemplatesFolder)
	{
		$arResult = array();

		$path = $_SERVER['DOCUMENT_ROOT'] . $tszhTemplatesFolder;

		if (!@is_dir($path))
			return $arResult;

		$arEntries = scandir($path);
		foreach ($arEntries as $entry)
		{
			$entryFull = $path . '/' . $entry;
			if (!self::checkDirEntry($entry, $entryFull))
				continue;

			// ����� ������� ������ ��������� ���� tszh_personal_receipt.php
			$arTemplateEntries = scandir($entryFull);
			foreach ($arTemplateEntries as $templateEntry)
			{
				if ($templateEntry == 'tszh_personal_receipt.php' && @is_file($entryFull . '/' . $templateEntry))
					$arResult[] = $entry;
			}
		}

		return $arResult;
	}

	private function GetTszhComponentTemplates($siteID = false)
	{
		static $arTemplates = array();
		if (!array_key_exists($siteID, $arTemplates))
		{
			$rsSiteTemplates = CSiteTemplate::GetList();
			$arSiteTemplates = array();
			while ($arSiteTemplate = $rsSiteTemplates->Fetch())
			{
				$arSiteTemplates[$arSiteTemplate['ID']] = array(
					'ID' => $arSiteTemplate['ID'],
					'NAME' => $arSiteTemplate['NAME'],
					'FOLDER' => BX_PERSONAL_ROOT . "/templates/" . $arSiteTemplate['ID'],
					'TSZH_TEMPLATES' => array(),
				);
			}
			if ($siteID != false)
			{
				$rsDbSiteTemplates = CSite::GetTemplateList($siteID);
				$arDbSiteTemplateIDs = array();
				while ($arDbSiteTemplate = $rsDbSiteTemplates->Fetch())
				{
					$arDbSiteTemplateIDs[] = $arDbSiteTemplate["TEMPLATE"];
				}
    
				$arOldSiteTemplates = $arSiteTemplates;
				$arSiteTemplates = array();
				foreach ($arOldSiteTemplates as $templateID => $arTemplate)
				{
					if (in_array($templateID, $arDbSiteTemplateIDs))
					{
						$arSiteTemplates[$templateID] = $arTemplate;
					}
				}
			}

			// .default ������ �����
			$arSiteTemplates['.default'] = array(
				'ID' => '.default',
				'NAME' => '.default',
				'FOLDER' => BX_PERSONAL_ROOT . "/templates/.default",
				'TSZH_TEMPLATES' => array(),
			);
			// ������� ����������
			$arSiteTemplates[''] = array(
				'ID' => '',
				'NAME' => GetMessage('TRTP_COMPONENT_TEMPLATE'),
				'FOLDER' => '/bitrix' . self::REL_PATH_TSZH . '/templates',
				'TSZH_TEMPLATES' => array(),
			);

			$arResult = array();
			foreach ($arSiteTemplates as $id => $arSiteTemplate)
			{
				switch ($arSiteTemplate['ID'])
				{
					case "":
						$tszhTemplatesFolder = $arSiteTemplate['FOLDER'];
						break;

					default:
						$tszhTemplatesFolder = $arSiteTemplate['FOLDER'] . self::REL_PATH_TSZH;
				}

				$arTemplates = self::__getTszhComponentTemplates($tszhTemplatesFolder);
				foreach ($arTemplates as $template)
				{
					if ($arSiteTemplate['ID'] == "")
						$part = "";
					else
						$part = self::REL_PATH_TSZH;

					$arSiteTemplates[$id]['TSZH_TEMPLATES'][$template] = $part . '/' . $template;
				}
        
				if (!empty($arSiteTemplates[$id]['TSZH_TEMPLATES']))
					$arResult[$id] = $arSiteTemplates[$id];
			}

			$arTemplates[$siteID] = $arResult;
		}

		return $arTemplates[$siteID];
	}

	public static function CheckTemplate($templateFolder)
	{
		$result = false;

		if ($templateFolder == self::AUTO_DETECT)
			return true;

		$arTemplates = self::GetTemplates();

		foreach ($arTemplates as $arSiteTemplate)
		{
			foreach ($arSiteTemplate['RECEIPT_TEMPLATES'] as $receiptPath)
			{
				if ($templateFolder == $arSiteTemplate['FOLDER'] . $receiptPath)
				{
					$result = true;
					break;
				}
			}
		}

		return $result;
	}

	/**
	 * ���������� ������ ��������� �������� ���������� ���������
	 *
	 * @param bool $siteID
	 *
	 * @return mixed
	 */
	public function GetTemplates($siteID = false)
	{
		static $arTemplates = array();
		if ($siteID === false
		    || !array_key_exists($siteID, $arTemplates))
		{
			$rsSiteTemplates = CSiteTemplate::GetList();
			$arSiteTemplates = array();
			while ($arSiteTemplate = $rsSiteTemplates->Fetch())
			{
				$arSiteTemplates[$arSiteTemplate['ID']] = array(
					'ID' => $arSiteTemplate['ID'],
					'NAME' => $arSiteTemplate['NAME'],
					'FOLDER' => BX_PERSONAL_ROOT . "/templates/" . $arSiteTemplate['ID'],
					'RECEIPT_TEMPLATES' => array(),
				);
			}
			if ($siteID != false)
			{
				$rsDbSiteTemplates = CSite::GetTemplateList($siteID);
				$arDbSiteTemplateIDs = array();
				while ($arDbSiteTemplate = $rsDbSiteTemplates->Fetch())
				{
					$arDbSiteTemplateIDs[] = $arDbSiteTemplate["TEMPLATE"];
				}
    
				$arOldSiteTemplates = $arSiteTemplates;
				$arSiteTemplates = array();
				foreach ($arOldSiteTemplates as $templateID => $arTemplate)
				{
					if (in_array($templateID, $arDbSiteTemplateIDs))
					{
						$arSiteTemplates[$templateID] = $arTemplate;
					}
				}
			}

			// .default ������ �����
			$arSiteTemplates['.default'] = array(
				'ID' => '.default',
				'NAME' => '.default',
				'FOLDER' => BX_PERSONAL_ROOT . "/templates/.default",
				'RECEIPT_TEMPLATES' => array(),
			);
			// ������� ����������
			$arSiteTemplates[''] = array(
				'ID' => '',
				'NAME' => GetMessage('TRTP_COMPONENT_TEMPLATE'),
				'FOLDER' => '/bitrix' . self::REL_PATH_RECEIPT . '/templates',
				'RECEIPT_TEMPLATES' => array(),
			);

			$isIncludePortal = CModule::includeModule("vdgb.portaljkh");

			// ������� ���������� citrus:tszh
			if ($isIncludePortal)
			{
				$arSiteTemplates[self::COMPONENT_TSZH] = array(
					'ID' => self::COMPONENT_TSZH,
					'NAME' => GetMessage('TRTP_FROM_CITRUS:TSZH_COMPONENT_TEMPLATE'),
					'FOLDER' => '/bitrix' . self::REL_PATH_TSZH . '/templates',
					'RECEIPT_TEMPLATES' => array(),
				);
			}

			$arResult = array();
			foreach ($arSiteTemplates as $id => $arSiteTemplate)
			{
				$arReceiptTemplatesFolders = array();
				switch ($arSiteTemplate['ID'])
				{
					case "":
						$arReceiptTemplatesFolders[] = $arSiteTemplate['FOLDER'];
						break;

					case self::COMPONENT_TSZH:
						$arReceiptTemplatesFolders = self::getContainersFromThemes($arSiteTemplate['FOLDER'], "");
						break;

					default:
						$arReceiptTemplatesFolders[] = $arSiteTemplate['FOLDER'] . self::REL_PATH_RECEIPT;
						if ($isIncludePortal)
							$arReceiptTemplatesFolders = array_merge($arReceiptTemplatesFolders, self::getContainersFromThemes($arSiteTemplate['FOLDER'], self::REL_PATH_TSZH));
				}

				foreach ($arReceiptTemplatesFolders as $key => $receiptTemplatesFolder)
				{
					$arTemplates = self::__getTemplates($receiptTemplatesFolder);
					foreach ($arTemplates as $template)
					{
						if ($arSiteTemplate['ID'] == "")
							$part = "";
						elseif (preg_match('#/citrus/tszh\.receipt$#', $key))
							$part = $key;
						else
							$part = self::REL_PATH_RECEIPT;

						$arSiteTemplates[$id]['RECEIPT_TEMPLATES'][$template] = $part . '/' . $template;
					}
				}
        
				if (!empty($arSiteTemplates[$id]['RECEIPT_TEMPLATES']))
					$arResult[$id] = $arSiteTemplates[$id];
			}

			$arTemplates[$siteID] = $arResult;
		}

		return $arTemplates[$siteID];
	}

	public static function getTemplatesForSelect($arAdditionalItems = array())
	{
		$arResult = array();

		if (!is_array($arAdditionalItems))
			$arAdditionalItems = array($arAdditionalItems);

		if (in_array("FROM_COMPONENT_SETTINGS", $arAdditionalItems))
			$arResult[""] = getMessage("TRTP_FROM_COMPONENT_SETTINGS");
		if (in_array("AUTO_DETECT", $arAdditionalItems))
			$arResult[self::AUTO_DETECT] = getMessage("TRTP_AUTO_DETECT");

		$arReceiptTemplates = self::GetTemplates();
		foreach ($arReceiptTemplates as $siteTemplateID => $arSiteTemplate)
		{
			foreach ($arSiteTemplate['RECEIPT_TEMPLATES'] as $receiptTemplateID => $receiptTemplatePath)
			{
				$tszhTemplate = "";
				$isSiteTszhTemplate = false;
				if ($siteTemplateID == self::COMPONENT_TSZH)
				{
					$matchRes = preg_match("#" . self::REL_PATH_TSZH . "/templates/([^/]+)/#", $arSiteTemplate['FOLDER'] . $receiptTemplatePath, $arMatches);
				}
				else
				{
					$matchRes = preg_match("#/([^/]+)" . self::REL_PATH_TSZH . "/#", $arSiteTemplate['FOLDER'] . $receiptTemplatePath, $arMatches);
					$isSiteTszhTemplate = $matchRes;
				}
				if ($matchRes)
					$tszhTemplate = $arMatches[1];

				$value = $arSiteTemplate['FOLDER'] . $receiptTemplatePath;
				$templateName = self::getTemplateName($receiptTemplateID);
				$name = ($templateName ? "{$templateName} ({$receiptTemplateID})" : $receiptTemplateID) . ' &ndash; ' . (in_array($siteTemplateID, array("", self::COMPONENT_TSZH)) ? '' : "[{$siteTemplateID}]") . ' ' . preg_replace('~#TEMPLATE#~', $tszhTemplate, $arSiteTemplate['NAME']) . ($isSiteTszhTemplate ? '; ' . toLower(GetMessage('TRTP_FROM_CITRUS:TSZH_COMPONENT_TEMPLATE', array("#TEMPLATE#" => $tszhTemplate))) : '');
				$arResult[$value] = $name;
			}
		}

		return $arResult;
	}

	public static function getOrganizationType($ID)
    {
        $organizationType = CTszh::GetList(
            Array(),
            Array("ID" => $ID),
            false,
            Array(),
            Array("ORGANIZATION_TYPE")
        )->getNext();

        //1 - �������������� ��������������
        $arResult[1] = GetMessage("TEF_SOLE_TRADER");
        //2 - ����������� ����
        $arResult[2] = GetMessage("TEF_LEGAL_PERSONALITY");
        //3 - ��������� ����������
        $arResult[3] = GetMessage("CITRUS_TSZH_IS_BUDGET");

        return $arResult;
    }

	/**
	 * CTszhReceiptTemplateProcessor::getComponentTemplateDir()
	 * ���������� ���������� (�� ����� �����) ������������� �� �������� ����� ������� ���������� citrus:tszh.receipt
	 *
	 * @param array $arSite - ������, �������� ���� (������������ ������� CSite::getByID() ��� CSite::getList())
	 * @param string|bool $templatePath - �������������� ��������; ���� � ������� ��������� (������� ���������� citrus:tszh.receipt) �� ����� ����� (��������, "/bitrix/components/citrus/tszh.receipt/templates/post-354")
	 * @return string
	 */
	public static function getComponentTemplateDir($arSite, $templatePath = false)
	{
		if (strlen($templatePath) > 0)
		{
			$s = rtrim(str_replace('\\', '/', $templatePath), '/');
			if (self::CheckTemplate($s))
				return $s;
		}
		else
		{
			if (!is_array($arSite) || empty($arSite))
				return false;

			$receiptComponentPattern = '#\$APPLICATION->IncludeComponent\s*\(\s*[\'"]citrus\:tszh\.receipt[\'"]\s*,\s*[\'"]([^\'"]*)[\'"]#i';

			$tszhTemplateFolder = false;
			$templateName = false;
			$filename = str_replace('//', '/', $arSite["ABS_DOC_ROOT"] . $arSite["DIR"] . "personal/receipt/index.php");
			if (@is_file($filename))
			{
				if (preg_match($receiptComponentPattern, $GLOBALS["APPLICATION"]->GetFileContent($filename), $arMatches))
					$templateName = $arMatches[1];
			}
			elseif (CModule::includeModule("vdgb.portaljkh"))
			{
				$filename = str_replace('//', '/', $arSite["ABS_DOC_ROOT"] . $arSite["DIR"] . "org/index.php");
				if (@is_file($filename))
				{
					if (preg_match('#\$APPLICATION->IncludeComponent\s*\(\s*[\'"]citrus\:tszh[\'"]\s*,\s*[\'"]([^\'"]*)[\'"]#i', $GLOBALS["APPLICATION"]->GetFileContent($filename), $arMatches))
						$tszhTemplateName = $arMatches[1];
					else
						$tszhTemplateName = ".default";

					$tszhTemplateFolder = false;
					$arTszhTemplates = self::GetTszhComponentTemplates($arSite["SITE_ID"]);
					foreach ($arTszhTemplates as $templateID => $arTemplate)
					{
						foreach ($arTemplate["TSZH_TEMPLATES"] as $key => $value)
						{
							if ($key == $tszhTemplateName)
							{
								$tszhTemplateFolder = $arTemplate["FOLDER"] . $value;
								break 2;
							}
						}
					}
					if (strlen($tszhTemplateFolder) <= 0)
						return false;

					$filename = str_replace('//', '/', $_SERVER["DOCUMENT_ROOT"] . $tszhTemplateFolder . "/tszh_personal_receipt.php");
					if (@is_file($filename) && preg_match($receiptComponentPattern, $GLOBALS["APPLICATION"]->GetFileContent($filename), $arMatches))
					{
						$templateName = $arMatches[1];
					}
					else
					{
						$template = COption::GetOptionString("vdgb.portaljkh", "portalReceiptTemplate", "post-354");
						$arTemplate = explode("/", $template);
						$templateName = strval(array_pop($arTemplate));
					}
				}
			}

			if (strlen($templateName) <= 0)
				$templateName = ".default";

			$arTemplates = self::GetTemplates($arSite["SITE_ID"]);
			foreach ($arTemplates as $templateID => $arTemplate)
			{
				foreach ($arTemplate["RECEIPT_TEMPLATES"] as $key => $value)
				{
					if ($key == $templateName)
					{
						$folder = $arTemplate["FOLDER"] . $value;
						if (strlen($tszhTemplateFolder) > 0)
						{
							if (strpos($folder, $tszhTemplateFolder . "/") === 0)
								return $folder;
						}
						else
						{
							return $folder;
						}
					}
				}
			}
		}

		return false;
	}

	public static function getTemplatePathInfo($arSite, $templatePath = "", $tszhTemplatePath = "")
	{
		if ($templatePath == self::AUTO_DETECT)
			$templatePath = self::getComponentTemplateDir($arSite);

		if (strlen($templatePath) <= 0 || !self::checkTemplate($templatePath))
			$templatePath = "";

		if (strlen($tszhTemplatePath) > 0 && self::checkTemplate($tszhTemplatePath))
			$templatePath = $tszhTemplatePath;

		$arTemplatePath = explode("/", $templatePath);
		$templateName = strval(array_pop($arTemplatePath));

		$siteTemplateID = false;
		if (is_array($arTemplatePath) && count($arTemplatePath) > 4 && $arTemplatePath[1] == "bitrix" && $arTemplatePath[2] == "templates")
			$siteTemplateID = $arTemplatePath[3];

		return array(
			"TEMPLATE_PATH" => $templatePath,
			"TEMPLATE_NAME" => $templateName,
			"SITE_TEMPLATE_ID" => $siteTemplateID,
		);
	}
}
?>