<?
$MESS['CITRUS_TSZH_E_MODULE_NAME'] = '1C: Сайт ЖКХ';
$MESS['CITRUS_TSZH_E_MODULE_DESCRIPTION'] = 'Редакция «Эксперт»';
$MESS['CITRUS_TSZH_E_PARTNER_NAME'] = '1С-Рарус Тиражные решения';
$MESS['CITRUS_TSZH_E_PARTNER_URI'] = 'https://otr-soft.ru/';
$MESS['TSZH_ERROR_MODULE_LOAD'] = 'Ошибка загрузка модуля из Marketplace: #ERROR#';
$MESS['TSZH_ERROR_MODULE_OBJECT'] = 'Ошибка при установке модуля, объект модуля не создан';
$MESS['TSZH_ERROR_MODULE_DB'] = 'Ошибка на этапе установки БД модуля';
$MESS['TSZH_ERROR_MODULE_FILES'] = 'Ошибка копирования файлов модуля';
$MESS['TSZH_ERROR_INSTALLING_MODULE'] = 'Установка модуля #MODULE_ID#: ';
$MESS['TSZH_ERROR_AUTOINSTALLING_MODULE'] = 'Ошибка при автоматической установке модуля #MODULE#: #ERROR#';
$MESS['TSZH_AUTOINSTALLING_MODULE_SUCCESS'] = 'Модуль #MODULE# успешно установлен';
$MESS['TSZH_ERROR_REQUIRED_VERSION'] = 'Для работы модуля необходима версия Главного модуля не ниже #VER#. Пожалуйста, установите обновления платформы 1С-Битрикс.';
?>