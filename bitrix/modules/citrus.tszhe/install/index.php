<?php

require_once dirname(__DIR__) . '/lib/moduleinstaller.php';

global $DOCUMENT_ROOT, $MESS;

use Citrus\Tszh\ModuleInstaller;
use Bitrix\Main\EventManager;

IncludeModuleLangFile(__FILE__);

if (class_exists("citrus_tszhe"))
{
	return;
}

Class citrus_tszhe extends CModule
{
	public $MODULE_ID = "citrus.tszhe";
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;
	public $MODULE_CSS;
	public $MODULE_GROUP_RIGHTS = "N";

	const MAIN_MIN_VERSION = '14.0.0';

	static $arInstallModules = Array(
		'citrus.tszh',
		'vdgb.tszhvote',
		'citrus.tszhpayment',
		'citrus.tszhtickets',
		'vdgb.documents',
		'vdgb.tszhepasport',
		'otr.datavalidation',
		'otr.sale',
        'otr.tszhchart',
        'otr.tszhrecaptcha'
	);

	function citrus_tszhe()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path . "/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else
		{
			$this->MODULE_VERSION = "11.0";
			$this->MODULE_VERSION_DATE = "2011-11-08 9:20:00";
		}

		$this->MODULE_NAME = GetMessage("CITRUS_TSZH_E_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("CITRUS_TSZH_E_MODULE_DESCRIPTION");

		$this->PARTNER_NAME = GetMessage("CITRUS_TSZH_E_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("CITRUS_TSZH_E_PARTNER_URI");
	}

	/**
	 * @param $strModule
	 */
	private function logAutoinstallSuccess($strModule)
	{
		$logFields = Array(
			"SEVERITY" => "UNKNOWN",
			"AUDIT_TYPE_ID" => "TSZH_MODULE_AUTOINSTALL_SUCCESS",
			"MODULE_ID" => $this->MODULE_ID,
			"DESCRIPTION" => GetMessage("TSZH_AUTOINSTALLING_MODULE_SUCCESS", array('#MODULE#' => $strModule)),
			"ITEM_ID" => session_id(),
		);
		\CEventLog::Add($logFields);
	}

	/**
	 * @param $strModule
	 * @param $e
	 */
	private function logErrorAutoinstall($strModule, $error)
	{
		$logFields = Array(
			"SEVERITY" => "WARNING",
			"AUDIT_TYPE_ID" => "TSZH_MODULE_AUTOINSTALL_ERROR",
			"MODULE_ID" => $this->MODULE_ID,
			"DESCRIPTION" => GetMessage("TSZH_ERROR_AUTOINSTALLING_MODULE",
				array('#MODULE#' => $strModule, '#ERROR#' => $error)),
			"ITEM_ID" => session_id(),
		);
		\CEventLog::Add($logFields);
	}

	private function LoadModules()
	{
		@set_time_limit(0);
		foreach (self::$arInstallModules as $strModule)
		{
			try
			{
				ModuleInstaller::LoadAndInstallModule($strModule);
				$this->logAutoinstallSuccess($strModule);
			}
			catch (Exception $e)
			{
				ShowError(GetMessage("TSZH_ERROR_INSTALLING_MODULE"), Array("#MODULE_ID#" => $strModule) . $e->getMessage());
				$this->logErrorAutoinstall($strModule, $e->getMessage());
			}
		}

	}

	function InstallDB()
	{
		RegisterModule($this->MODULE_ID);

		$eventManager = EventManager::getInstance();
		$eventManager->registerEventHandler("main", "OnEventLogGetAuditTypes", $this->MODULE_ID, "Citrus\\Tszh\\ModuleInstaller", "OnEventLogGetAuditTypesHandler");

		return true;
	}

	function UnInstallDB()
	{
		$eventManager = EventManager::getInstance();
		$eventManager->unRegisterEventHandler("main", "OnEventLogGetAuditTypes", $this->MODULE_ID, "Citrus\\Tszh\\ModuleInstaller", "OnEventLogGetAuditTypesHandler");

		UnRegisterModule($this->MODULE_ID);

		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}


	// ����������� ������ ������
	function InstallFiles()
	{
		$this->LoadModules();

		return true;
	}

	// �������� ������ ������
	function UnInstallFiles()
	{
		return true;
	}

	// ��������� ������ (���������� ���������)
	function DoInstall()
	{
		global $APPLICATION;

		if (!check_bitrix_sessid())
		{
			return false;
		}

		if ($this->isBXVersionRequiredError())
		{
			$APPLICATION->ThrowException(GetMessage('TSZH_ERROR_REQUIRED_VERSION', array("#VER#" => self::MAIN_MIN_VERSION)));

			return false;
		}

		$this->InstallDB();
		$this->InstallEvents();
		$this->InstallFiles();
	}

	// �������� ������ (���������� ���������)
	function DoUninstall()
	{
		global $APPLICATION;

		if (!check_bitrix_sessid())
		{
			return false;
		}

		$step = IntVal($_REQUEST['step']);
		if ($step < 2)
		{
			$APPLICATION->IncludeAdminFile(GetMessage("inst_uninst_title"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/uninst1.php");
		}
		elseif ($step == 2)
		{
			$this->UnInstallDB();
			$this->UnInstallEvents();
			$this->UnInstallFiles();
			$GLOBALS["errors"] = $this->errors;
			$APPLICATION->IncludeAdminFile(GetMessage("inst_uninst_title"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/uninst2.php");
		}
	}

	/**
	 * @return mixed
	 */
	protected function isBXVersionRequiredError()
	{
		return version_compare(SM_VERSION, self::MAIN_MIN_VERSION, "<");
	}

}

?>