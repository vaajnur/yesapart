<?php
$MESS["VDGB_PORTAL_INSTALL_NAME"]                  = "1С: Сайт ЖКХ. Портал 2.0";
$MESS["VDGB_PORTAL_INSTALL_DESC"]                  = "Позволяет развернуть на сайте портал, содержащий сайты нескольких организаций ЖКХ.";
$MESS["VDGB_PORTAL_INSTALL_TITLE_INST"]            = "Установка модуля «1С: Сайт ЖКХ. Портал 2.0»";
$MESS["VDGB_PORTAL_INSTALL_TITLE_UNITST"]          = "Удаление модуля «1С: Сайт ЖКХ. Портал 2.0»";
$MESS["C_MODULE_PARTNER_NAME"]                     = "1С-Рарус Тиражные решения";
$MESS["C_MODULE_PARTNER_URI"]                      = "https://otr-soft.ru/";
$MESS["VDGB_PORTAL_ERROR_CITRUS_TSZH_NOT_INSTALL"] = "Модуль «1C: Сайт ЖКХ» не установлен";
$MESS["VDGB_PORTAL_ERROR_CITRUS_TSZH_MIN_VERSION"] = "Для работы модуля необходима версия модуля «1C: Сайт ЖКХ» не ниже #VER#. Пожалуйста, установите обновления.";
$MESS["VDGB_PORTAL_INSTALL_TITLE"]                 = "Установка модуля «1С: Сайт ЖКХ. Портал 2.0»";
$MESS["VDGB_PORTAL_UNINSTALL_TITLE"]               = "Удаление модуля «1С: Сайт ЖКХ. Портал 2.0»";
?>