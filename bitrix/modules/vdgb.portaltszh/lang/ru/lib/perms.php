<?
$MESS["PERMS_ENTITY_ID_FIELD"]          = "ID";
$MESS["PERMS_ENTITY_ENTITY_TYPE_FIELD"] = "Тип";
$MESS["PERMS_ENTITY_ENTITY_ID_FIELD"]   = "Идентификатор";
$MESS["PERMS_ENTITY_GROUP_ID_FIELD"]    = "Группа пользователей";
$MESS["PERMS_ENTITY_PERMS_FIELD"]       = "Доступ";
$MESS["PERMS_SET_ERROR_TSZH_ID_VALUE"]  = "Не верно указан ID объекта управления";
$MESS["PERMS_SET_ERROR_GROUP_ID_VALUE"] = "Не верно указан ID группы пользователей";
$MESS["PERMS_SET_ERROR_PERMS_VALUE"]    = "Не верно указаны права для группы";

$MESS["TSZH_ACCESS_D"] = "Нет доступа";
$MESS["TSZH_ACCESS_R"] = "Просмотр данных";
$MESS["TSZH_ACCESS_S"] = "Доступ к обмену";
$MESS["TSZH_ACCESS_W"] = "Изменение данных";
$MESS["TSZH_ACCESS_X"] = "Полный доступ (изменение прав доступа)";