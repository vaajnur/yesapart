<?php
$MESS["VDGB_PORTAL_GROUP_ADMIN"] = "[#TSZH_NAME#] Администраторы";
$MESS["VDGB_PORTAL_ERROR_CREATING_ADMIN_GROUP"] = "Ошибка при создании группы «[#TSZH_NAME#] Администраторы»";
$MESS["VDGB_PORTAL_ERROR_ADMIN_GROUP_EXIST"] = "Группа «[#TSZH_NAME#] Администраторы» уже существует";
?>