<?php
$MESS["VDGB_PORTAL_GROUP_ADMIN_SITE"] = "[#SITE_NAME#] Администраторы сайта";
$MESS["VDGB_PORTAL_ERROR_CREATING_ADMIN_SITE_GROUP"] = "Ошибка при создании группы «[#SITE_NAME#] Администраторы сайта»";
$MESS["VDGB_PORTAL_ERROR_ADMIN_ADMIN_SITE_EXIST"] = "Группа «[#SITE_NAME#] Администраторы сайта» уже существует";
?>