<?php
$MESS["VDGB_PORTAL_GROUP_1C_EXCHANGE"] = "[#TSZH_NAME#] Служебная группа для интеграция с 1С";
$MESS["VDGB_PORTAL_ERROR_CREATING_1C_EXCHANGE_GROUP"] = "Ошибка при создании группы «[#TSZH_NAME#] Служебная группа для интеграция с 1С»";
$MESS["VDGB_PORTAL_ERROR_1C_EXCHANGE_GROUP_EXIST"] = "Группа «[#TSZH_NAME#] Служебная группа для интеграция с 1С» уже существует";
?>