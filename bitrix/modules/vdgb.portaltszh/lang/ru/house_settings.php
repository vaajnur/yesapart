<?
$MESS["vdgb_portaltszh_TAB"]            = "Доступ";
$MESS["TSZH_RIGHTS_MODE_SECTION_TITLE"] = "Права доступа";
$MESS["TSZH_RIGHTS_MODE"]               = "Расширенное управление правами:";
$MESS["TSZH_RIGHTS_MODE_NOTE1"]         = "После того как вы снимите отметку с этого чекбокса, необходимо нажать кнопку \"Применить\". Далее настройте права доступа.";
$MESS["TSZH_RIGHTS_SECTION_TITLE"]      = "Права доступа";
$MESS["TSZH_RIGHTS_MODE_NOTE2"]         = "После того как вы отметите этот чекбокс, необходимо нажать кнопку \"Применить\". При этом текущие настройки будут сконвертированы в расширенные.";
$MESS["TSZH_DEFAULT_ACCESS_TITLE"]      = "Доступ по умолчанию";
$MESS["TSZH_ACCESS_WARNING"]            = "Внимание! Заданный уровень доступа переопределяется уровнем доступа по умолчанию.";
$MESS["TSZH_GROUP_ACCESS_TITLE"]        = "Доступ для групп пользователей";
$MESS["TSZH_DEFAULT_ACCESS"]            = "По умолчанию";
$MESS["TSZH_EVERYONE"]                  = "Для всех пользователей";
?>