<?php
namespace Vdgb\PortalTszh\Groups;

use Bitrix\Main\Localization\Loc;
use Citrus\Tszh\HouseTable;
use Vdgb\PortalTszh\PermsTable;

Loc::loadMessages(__FILE__);

class SiteAdmin extends Group
{
	private static $cur_site_id;

	private static $entity = 'house';

	/**
	 * ����� ��� ���� ������
	 * @return mixed
	 */
	protected function getType()
	{
		return "ADMIN_SITE";
	}

	/**
	 * ������ ���� �� ������
	 * @return mixed
	 */
	protected function getPermissionList()
	{
		return array(
			"iblock"             => "W",
			"blog"               => "R",
			"forum"              => "R",
			"citrus.tszh"        => "W",
			"citrus.tszhpayment" => "W",
			"citrus.tszhtickets" => "W"
		);
	}

	/**
	 * ���������� ����� ��� ���
	 * @return mixed
	 */
	protected function getTszhPermission()
	{
		return "R";
		/*�� ������������� ������ ��� �� ������� ��� ������ �������� ��� �����������*/
	}

	/**
	 * ���������� ����� ��� �����
	 * @return mixed
	 */
	protected function getHousePermission()
	{
		return "W";
	}

	/**
	 * ���������� ������ ���� ��� ������� ������� �������� ����� ������ �������
	 * @return mixed
	 */
	protected function getTaskList()
	{
		return array(
			"main"    => "P",
			"fileman" => "F",
		);
	}

	/**
	 * ���������� ������ ������ � ����� � �������
	 * @return mixed
	 */
	protected function getFilesToAccess()
	{
		return array(
			array("/bitrix/admin", "*", "R"),
			array("/", "index.php", "W"),
			array("/about", "*", "W"),
			array("/blog", "*", "W"),
			array("/contacts", "*", "W"),
			array("/debtors", "*", "W"),
			array("/dispatcher", "*", "W"),
			array("/documents", "*", "W"),
			array("/forum", "*", "W"),
			array("/news", "*", "W"),
			array("/operator", "*", "W"),
			array("/personal", "*", "W"),
			array("/photogallery", "*", "W"),
			array("/questions-and-answers", "*", "W"),
			array("/search", "*", "W"),
			array("/useful-info", "*", "W"),
			array("/video", "*", "W"),
			array("/workplace", "*", "W"),
		);
	}

	public static function create()
	{
		$rsSite = \CSite::GetList($by="sort", $order="desc");
		self::$group_id = 0;

		while($arSite = $rsSite->GetNext())
		{
			$site_id = $arSite['ID'];

			$group = new \CGroup();

			$rsGroup = $group->GetListEx(
				Array("ID" => "DESC"),
				Array("STRING_ID" => "TSZH_" . static::getType() . '_' . $site_id),
				false,
				Array("nTopCount" => 1)
			);

			if ($rsGroup->SelectedRowsCount() <= 0)
			{
				$arFields = Array(
					"ACTIVE" => "Y",
					"C_SORT" => 500 + $arSite["SORT"] * 10,
					"NAME" => Loc::getMessage("VDGB_PORTAL_GROUP_" . static::getType(), array("#SITE_NAME#" => $arSite['NAME'])),
					"STRING_ID" => "TSZH_" . static::getType() . '_' . $site_id,
					"DESCRIPTION" => "",
					"USER_ID" => array()
				);
				if (!($groupID = $group->Add($arFields)))
				{
					throw new \Exception(Loc::getMessage("VDGB_PORTAL_ERROR_CREATING_" . static::getType() . "_GROUP", array("#TSZH_NAME#" => $arSite['NAME'])) . ': ' . $group->LAST_ERROR);
				}

				self::$group_id = $groupID;
				self::$cur_site_id = $site_id;
			}

			if (self::$group_id) self::setPermission();
		}
	}

	protected function setIBlockPermission()
	{
		if (in_array("iblock", array_keys(static::getPermissionList())) && in_array("iblock", self::$installedModules))
		{
			if (!\CModule::IncludeModule("iblock"))
			{
				throw new Exception(Loc::getMessage("VDGB_PORTAL_ERROR_IBLOCK_NOT_INCLUDED"));
			}

			$rsIBlock = \CIBlock::GetList(array(), array("SITE_ID" => self::$cur_site_id));
			while ($arIBlock = $rsIBlock->GetNext())
			{
				$arIblockPerm = \CIBlock::GetGroupPermissions($arIBlock["ID"]);
				$arIblockPerm[self::$group_id] = "W";

				\CIBlock::SetPermission($arIBlock["ID"], $arIblockPerm);
			}
		}
	}

	protected function setPermission()
	{
		self::getCurrentModulesList();
		self::setModulePermission();
		self::setModuleTask();
		self::setForumPermission();
		self::setIBlockPermission();
		self::setFilesPermission();
		self::setHousePermission();
		//		self::setBlogPermission();
	}

	public static function delete()
	{
		$rsSite = \CSite::GetList($by="sort", $order="desc");

		while($arSite = $rsSite->GetNext())
		{
			$site_id = $arSite['ID'];

			$group = new \CGroup();
			$rsGroup = $group->GetListEx(
				Array("ID" => "DESC"),
				Array("STRING_ID" => "TSZH_" . static::getType() . '_' . $site_id),
				false,
				Array("nTopCount" => 1)
			);
			if ($rsGroup->SelectedRowsCount() > 0)
			{
				$arGroup = $rsGroup->Fetch();
				$groupID = $arGroup["ID"];

				$group->Delete($groupID);
			}
		}
	}

	protected static function setHousePermission()
	{
		$rsHouse = HouseTable::getList(array(
				'filter' => array(
					'SITE_ID' => self::$cur_site_id
				),
			)
		);

		while ($arHouse = $rsHouse->fetch())
		{
			PermsTable::setPermission(self::$entity, $arHouse['ID'], self::$group_id, self::getHousePermission());
		}
	}

	public static function getGroupID($site_id)
	{
		$arSite = \CSite::GetByID($site_id)->GetNext();

		$group = new \CGroup();
		$rsGroup = $group->GetListEx(
			Array("ID" => "DESC"),
			Array("STRING_ID" => "TSZH_" . static::getType() . '_' . $site_id),
			false,
			Array("nTopCount" => 1),
			array('ID')
		);

		if ($rsGroup->SelectedRowsCount() <= 0)
		{
			$arFields = Array(
				"ACTIVE" => "Y",
				"C_SORT" => 500 + $arSite["SORT"] * 10,
				"NAME" => Loc::getMessage("VDGB_PORTAL_GROUP_" . static::getType(), array("#SITE_NAME#" => $arSite['NAME'])),
				"STRING_ID" => "TSZH_" . static::getType() . '_' . $site_id,
				"DESCRIPTION" => "",
				"USER_ID" => array()
			);
			if (!($groupID = $group->Add($arFields)))
			{
				throw new \Exception(Loc::getMessage("VDGB_PORTAL_ERROR_CREATING_" . static::getType() . "_GROUP", array("#TSZH_NAME#" => $arSite['NAME'])) . ': ' . $group->LAST_ERROR);
			}
		}
		else
		{
			$arGroup = $rsGroup->GetNext();
			$groupID = $arGroup['ID'];
		}

		return $groupID;
	}

	/**
	 * ����� ������������� ����� �� ����� � �����
	 */
	protected function setFilesPermission()
	{
		$arFilesPerm = static::getFilesToAccess();

		if (!empty($arFilesPerm))
		{
			global $APPLICATION;

			foreach ($arFilesPerm as $arAccess)
			{
				$arSite = \CSite::getArrayByID(self::$cur_site_id);
				$path = $arSite["DIR"];

				$arCurPerm = self::getAccessArrTmp($path . $arAccess[0]);

				foreach ($arCurPerm[$arAccess[1]] as $group => $cur_perm)
				{
					$arPermission[$group] = $cur_perm;
				}

				$arPermission[self::$group_id] = $arAccess[2];

				if ($arAccess[1] === "*")
				{
					$accessFolder = $arAccess[0];
				}
				else
				{
					$accessFolder = $arAccess[0] . $arAccess[1];
				}

				$APPLICATION->SetFileAccessPermission(array(self::$cur_site_id, $path . $accessFolder), $arPermission);
			}
		}
	}
}

?>