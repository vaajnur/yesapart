<?php
namespace Vdgb\PortalTszh\Groups;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class TszhExchange extends Group
{
	/**
	 * ����� ��� ���� ������
	 * @return mixed
	 */
	protected function getType()
	{
		return "1C_EXCHANGE";
	}

	protected function getPermissionList()
	{
		return array(
			"citrus.tszh" => "U"
		);
	}

	/**
	 * ���������� ����� ��� ���
	 * @return mixed
	 */
	protected function getTszhPermission()
	{
		return "S";
	}

	/**
	 * ���������� ������ ���� ��� ������� ������� �������� ����� ������ �������
	 * @return mixed
	 */
	protected function getTaskList()
	{
		return array();
	}

	/**
	 * ���������� ������ ������ � ����� � �������
	 * @return mixed
	 */
	protected function getFilesToAccess()
	{
		return array();
	}
}

?>