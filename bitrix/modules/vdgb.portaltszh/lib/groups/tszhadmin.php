<?php
namespace Vdgb\PortalTszh\Groups;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class TszhAdmin extends Group
{
	/**
	 * ����� ��� ���� ������
	 * @return mixed
	 */
	protected function getType()
	{
		return "ADMIN";
	}

	/**
	 * ������ ���� �� ������
	 * @return mixed
	 */
	protected function getPermissionList()
	{
		return array(
			"iblock"             => "W",
			"blog"               => "R",
			"forum"              => "R",
			"citrus.tszh"        => "W",
			"citrus.tszhpayment" => "W",
			"citrus.tszhtickets" => "W"
		);
	}

	/**
	 * ���������� ����� ��� ���
	 * @return mixed
	 */
	protected function getTszhPermission()
	{
		return "W";
	}

	/**
	 * ���������� ������ ���� ��� ������� ������� �������� ����� ������ �������
	 * @return mixed
	 */
	protected function getTaskList()
	{
		return array(
			"main"    => "P",
			"fileman" => "F",
		);
	}

	/**
	 * ���������� ������ ������ � ����� � �������
	 * @return mixed
	 */
	protected function getFilesToAccess()
	{
		return array(
			array("/bitrix/admin", "*", "R"),
			array("/", "index.php", "W"),
			array("/about", "*", "W"),
			array("/blog", "*", "W"),
			array("/contacts", "*", "W"),
			array("/debtors", "*", "W"),
			array("/dispatcher", "*", "W"),
			array("/documents", "*", "W"),
			array("/forum", "*", "W"),
			array("/news", "*", "W"),
			array("/operator", "*", "W"),
			array("/personal", "*", "W"),
			array("/photogallery", "*", "W"),
			array("/questions-and-answers", "*", "W"),
			array("/search", "*", "W"),
			array("/useful-info", "*", "W"),
			array("/video", "*", "W"),
			array("/workplace", "*", "W"),
		);
	}
}

?>