<?php
namespace Vdgb\PortalTszh\Groups;

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Location\Exception;
use \Bitrix\Main\ModuleManager;
use Vdgb\PortalTszh\PermsTable;

Loc::loadMessages(__FILE__);

/**
 * ����� ��� �������� ����� �������������
 * Class Group
 * @package Vdgb\PortalTszh\Groups
 */
abstract class Group
{
	/**
	 * ������ ������ �� �����������
	 * @var
	 */
	private static $org = array();

	/**
	 * ������ ������������� �������
	 * @var
	 */
	protected static $installedModules = array();

	/**
	 * ������ ������������� ������
	 * @var
	 */
	protected static $group_id;

	/**
	 * ����� ��� ���� ������
	 * @return mixed
	 */
	abstract protected function getType();

	/**
	 * ���������� ������ ���� �� ������
	 * @return mixed
	 */
	abstract protected function getPermissionList();

	/**
	 * ���������� ����� ��� ���
	 * @return mixed
	 */
	abstract protected function getTszhPermission();

	/**
	 * ���������� ������ ���� ��� ������� ������� �������� ����� ������ �������
	 * @return mixed
	 */
	abstract protected function getTaskList();

	/**
	 * ���������� ������ ������ � ����� � �������
	 * @return mixed
	 */
	abstract protected function getFilesToAccess();

	/**
	 * TszhGroup constructor.
	 *
	 * @param $params
	 */
	private function __construct()
	{
	}

	/**
	 * ����� ������� ������
	 * @throws \Exception
	 */
	public static function create($org)
	{
		self::$org = $org;

		$group = new \CGroup();

		$rsGroup = $group->GetListEx(
			Array("ID" => "DESC"),
			Array("STRING_ID" => "TSZH_" . self::$org["ID"] . "_" . static::getType()), false, Array("nTopCount" => 1));
		$tszhName = isset(self::$org["~NAME"]) ? self::$org["~NAME"] : self::$org["NAME"];
		if ($rsGroup->SelectedRowsCount() <= 0)
		{
			$arFields = Array(
				"ACTIVE" => "Y",
				"C_SORT" => 1000 + self::$org["ID"] * 10,
				"NAME" => Loc::getMessage("VDGB_PORTAL_GROUP_" . static::getType(), array("#TSZH_NAME#" => $tszhName)),
				"STRING_ID" => "TSZH_" . self::$org["ID"] . "_" . static::getType(),
				"DESCRIPTION" => "",
				"USER_ID" => array()
			);
			if (!($groupID = $group->Add($arFields)))
			{
				throw new \Exception(Loc::getMessage("VDGB_PORTAL_ERROR_CREATING_" . static::getType() . "_GROUP", array("#TSZH_NAME#" => $tszhName)) . ': ' . $group->LAST_ERROR);
			}

			self::$group_id = $groupID;
		}
		else
		{
			throw new \Exception(Loc::getMessage("VDGB_PORTAL_ERROR_" . static::getType() . "_GROUP_EXIST", array("#TSZH_NAME#" => $tszhName)));
		}

		self::setPermission();
	}

	/**
	 * ������� ������, ��������� ��� ������� ����������
	 *
	 * @param $ID
	 */
	public static function delete($ID)
	{
		$group = new \CGroup();
		$rsGroup = $group->GetListEx(Array("ID" => "DESC"), Array("STRING_ID" => "TSZH_" . $ID . "_" . static::getType()), false, Array("nTopCount" => 1));
		if ($rsGroup->SelectedRowsCount() > 0)
		{
			$arGroup = $rsGroup->Fetch();
			$groupID = $arGroup["ID"];

			$group->Delete($groupID);
		}
	}

	/**
	 * ����� ������������� ����� �� �����
	 */
	protected function setPermission()
	{
		self::getCurrentModulesList();
		self::setModulePermission();
		self::setModuleTask();
		self::setForumPermission();
		self::setIBlockPermission();
		self::setTszhPermission();
		self::setFilesPermission();
//		self::setBlogPermission();
	}

	/**
	 * ����� ������������� ����� �� ������
	 */
	protected function setModulePermission()
	{
		global $APPLICATION;

		$arPerms = static::getPermissionList();
		$arModules = self::getCurrentModulesList();

		foreach ($arPerms as $module => $perm)
		{
			if (in_array($module, $arModules))
			{
				$APPLICATION->SetGroupRight($module, self::$group_id, $perm);
			}
		}
	}

	/**
	 * ������������� ����� �� ������, ������� ���������� ������ �������
	 */
	protected function setModuleTask()
	{
		$arTaskList = static::getTaskList();
		if (!empty($arTaskList))
		{
			foreach (static::getTaskList() as $module => $perm)
			{
				$taskID = \CTask::GetIdByLetter($perm, $module);
				$arTasks[$module] = $taskID;
				\CGroup::SetTasks(self::$group_id, $arTasks);
			}
		}
	}

	/**
	 * ����� ������������� ����� �� ������ �����, � �������� ��������� �����������
	 */
	protected function setForumPermission()
	{
		if (in_array("forum", self::$installedModules) && in_array("forum", array_keys(static::getPermissionList())))
		{
			global $DB;

			$strSql = "SELECT FS.FORUM_ID, FS.SITE_ID FROM b_forum2site FS WHERE FS.SITE_ID = '" . self::$org["SITE_ID"] . "'";
			$db_res = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			$arForum = array();
			while ($res = $db_res->Fetch())
			{
				$arForum[] = $res["FORUM_ID"];
			}

			if (!empty($arForum))
			{
				if (!\CModule::IncludeModule("forum"))
				{
					throw new Exception(Loc::getMessage("VDGB_PORTAL_ERROR_FORUM_NOT_INCLUDED"));
				}

				$obForum = new \CForumNew();
				$arUpdatePerms = array(self::$group_id => "U");
				foreach ($arForum as $forum_id)
				{
					$obForum->Update($forum_id, array("GROUP_ID" => $arUpdatePerms));
				}
			}
		}
	}

	/**
	 * ����� ������������ ����� �� ����� �����
	 */
	protected function setBlogPermission()
	{
		if (in_array("blog", self::$installedModules) && in_array("blog", array_keys(static::getPermissionList())))
		{
		}
	}

	/**
	 * ����� ������������� ����� �� ��������� ����� � �������� �������� �����������
	 */
	protected function setIBlockPermission()
	{
		if (in_array("iblock", array_keys(static::getPermissionList())) && in_array("iblock", self::$installedModules))
		{
			if (!\CModule::IncludeModule("iblock"))
			{
				throw new Exception(Loc::getMessage("VDGB_PORTAL_ERROR_IBLOCK_NOT_INCLUDED"));
			}

			$rsIBlock = \CIBlock::GetList(array(), array("SITE_ID" => self::$org["SITE_ID"]));
			while ($arIBlock = $rsIBlock->GetNext())
			{
				$arIblockPerm = \CIBlock::GetGroupPermissions($arIBlock["ID"]);
				$arIblockPerm[self::$group_id] = "W";

				\CIBlock::SetPermission($arIBlock["ID"], $arIblockPerm);
			}
		}
	}

	/**
	 * ����� ������������� ����� �� ������� ����������
	 */
	protected function setTszhPermission()
	{
		$entity = 'tszh';
		if (in_array("citrus.tszh", self::$installedModules) && in_array("citrus.tszh", array_keys(static::getPermissionList())))
		{
			PermsTable::setPermission($entity, self::$org["ID"], self::$group_id, static::getTszhPermission());
		}
	}

	/**
	 * ����� ������������� ����� �� ����� � �����
	 */
	protected function setFilesPermission()
	{
		$arFilesPerm = static::getFilesToAccess();

		if (!empty($arFilesPerm))
		{
			global $APPLICATION;

			foreach ($arFilesPerm as $arAccess)
			{
				$arSite = \CSite::getArrayByID(self::$org["SITE_ID"]);
				$path = $arSite["DIR"];

				$arCurPerm = self::getAccessArrTmp($path . $arAccess[0]);

				foreach ($arCurPerm[$arAccess[1]] as $group => $cur_perm)
				{
					$arPermission[$group] = $cur_perm;
				}

				$arPermission[self::$group_id] = $arAccess[2];

				if ($arAccess[1] === "*")
				{
					$accessFolder = $arAccess[0];
				}
				else
				{
					$accessFolder = $arAccess[0] . $arAccess[1];
				}

				$APPLICATION->SetFileAccessPermission(array(self::$org["SITE_ID"], $path . $accessFolder), $arPermission);
			}
		}
	}

	/**
	 * ����� ���������� ������ ������������� �������
	 *
	 * @return array
	 */
	protected function getCurrentModulesList()
	{
		if (empty(self::$installedModules))
		{
			$arModules = ModuleManager::getInstalledModules();
			foreach ($arModules as $module)
			{
				self::$installedModules[] = $module["ID"];
			}
		}

		return self::$installedModules;
	}

	/**
	 * ���������� ������ ���� ��� �����
	 *
	 * @param $path
	 * @return array
	 */
	protected function getAccessArrTmp($path)
	{
		$DOC_ROOT = Application::getInstance()->getDocumentRoot();
		$io = \CBXVirtualIo::GetInstance();
		if ($io->DirectoryExists($DOC_ROOT . $path))
		{
			if ($path === "/") $path = "";
			@include($io->GetPhysicalName($DOC_ROOT . $path . "/.access.php"));
			return $PERM;
		}
		return array();
	}

}

?>