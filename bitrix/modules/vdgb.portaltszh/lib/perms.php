<?php
namespace Vdgb\PortalTszh;

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class PermsTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ENTITY_TYPE string mandatory
 * <li> ENTITY_ID int mandatory
 * <li> GROUP_ID int mandatory
 * <li> PERMS string(1) mandatory
 * </ul>
 *
 * @package Bitrix\Tszh
 **/
class PermsTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_perms';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('PERMS_ENTITY_ID_FIELD'),
			),
			'ENTITY_TYPE' => array(
				'data_type' => 'string',
				'required' => true,
				'title' => Loc::getMessage('PERMS_ENTITY_ENTITY_TYPE_FIELD'),
			),
			'ENTITY_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('PERMS_ENTITY_ENTITY_ID_FIELD'),
			),
			'GROUP_ID' => array(
				'data_type' => 'string',
				'required' => true,
				'title' => Loc::getMessage('PERMS_ENTITY_GROUP_ID_FIELD'),
			),
			'PERMS' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validatePerms'),
				'title' => Loc::getMessage('PERMS_ENTITY_PERMS_FIELD'),
			),
		);
	}

	/**
	 * Returns validators for PERMS field.
	 *
	 * @return array
	 */
	public static function validatePerms()
	{
		return array(
			new Main\Entity\Validator\Length(null, 1),
		);
	}

	/**
	 * ������������� ����� �� ��������
	 *
	 * @param $entity
	 * @param $id
	 * @param $group_id
	 * @param $perms
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public static function setPermission($entity, $id, $group_id, $perms)
	{
		if (IntVal($id) <= 0)
		{
			throw new \Exception("PERMS_SET_ERROR_TSZH_ID_VALUE");
		}

		if (IntVal($group_id) <= 0 && $group_id != "*")
		{
			throw new \Exception("PERMS_SET_ERROR_GROUP_ID_VALUE");
		}

		$arPerms = array_keys(self::getTszhPermissionList());

		if (!in_array($perms, $arPerms))
		{
			throw new \Exception("PERMS_SET_ERROR_PERMS_VALUE");
		}

		$arPermsFields = array(
			"ENTITY_TYPE" => $entity,
			"ENTITY_ID" => $id,
			"GROUP_ID" => $group_id,
			"PERMS" => $perms,
		);

		$permission = self::getRow(
			array(
				"filter" => array(
					"ENTITY_TYPE" => $entity,
					"ENTITY_ID" => $id,
					"GROUP_ID" => $group_id
				)
			)
		);

		if (isset($permission) && is_array($permission))
		{
			$result = self::update($permission["ID"], $arPermsFields);
		}
		else
		{
			$result = self::add($arPermsFields);
		}

		return $result->isSuccess();
	}

	/**
	 * ������� ���� ��� ��������
	 *
	 * @param $entity
	 * @param $id
	 */
	public static function clearPermission($entity, $id)
	{
		switch ($entity)
		{
			case 'tszh':
				self::clearTszhPermission($id);
				break;
			case 'house':
				self::clearHousePermission($id);
				break;
		}
	}

	/**
	 * ������� ���� ��� �������� ����������
	 *
	 * @param $id
	 *
	 * @throws \Exception
	 */
	public static function clearTszhPermission($id)
	{
		if (IntVal($id) <= 0)
		{
			throw new \Exception("PERMS_SET_ERROR_TSZH_ID_VALUE");
		}

		global $DB;

		$strSql = "DELETE FROM b_tszh_perms WHERE ENTITY_TYPE = 'tszh' AND ENTITY_ID = " . $id;
		$db_res = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
	}

	/**
	 * ������� ���� ��� �����
	 *
	 * @param $id
	 *
	 * @throws \Exception
	 */
	public static function clearHousePermission($id)
	{
		if (IntVal($id) <= 0)
		{
			throw new \Exception("PERMS_SET_ERROR_HOUSE_ID_VALUE");
		}

		global $DB;

		$strSql = "DELETE FROM b_tszh_perms WHERE ENTITY_TYPE = 'house' AND ENTITY_ID = " . $id;
		$db_res = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
	}

	/**
	 * ��������� ���� ��-���������
	 *
	 * @param $entity
	 * @param $id
	 *
	 * @return mixed|string
	 */
	public static function getDefaultPermission($entity, $id)
	{
		$arDefault = self::getRow(
			array(
				"filter" => array(
					"ENTITY_TYPE" => $entity,
					"ENTITY_ID" => $id,
					"GROUP_ID" => "*"
				)
			)
		);

		if (isset($arDefault) && is_array($arDefault))
		{
			$default = $arDefault["PERMS"];
		}

		return isset($default) ? $default : "D";
	}

	/**
	 * ������ ��������� ���� ��� ��������
	 *
	 * @param string $entity
	 *
	 * @return array
	 */
	public static function getPermissionList($entity = 'tszh')
	{
		$permission = array();

		switch ($entity)
		{
			case 'tszh':
				$permission = self::getTszhPermissionList();
				break;
			case 'house':
				$permission = self::getHousePermissionList();
				break;
		}

		return $permission;
	}

	/**
	 * ������ ��������� ���� ��� �������� ����������
	 *
	 * @return array
	 */
	public static function getTszhPermissionList()
	{
		return array(
			"D" => GetMessage("TSZH_ACCESS_D"),
			"R" => GetMessage("TSZH_ACCESS_R"),
			"S" => GetMessage("TSZH_ACCESS_S"),
			"W" => GetMessage("TSZH_ACCESS_W"),
			"X" => GetMessage("TSZH_ACCESS_X")
		);
	}

	/**
	 * ������ ��������� ���� ��� �����
	 *
	 * @return array
	 */
	public static function getHousePermissionList()
	{
		return array(
			"D" => GetMessage("TSZH_ACCESS_D"),
			"R" => GetMessage("TSZH_ACCESS_R"),
			"W" => GetMessage("TSZH_ACCESS_W"),
			"X" => GetMessage("TSZH_ACCESS_X")
		);
	}

	/**
	 * ������ ��������� ���� ��� ������
	 *
	 * @param $ENTITY
	 * @param $ID
	 *
	 * @return array
	 */
	public static function getGroupPermissions($ENTITY, $ID)
	{
		$rsPerm = self::getList(array(
			'filter' => array(
				'ENTITY_TYPE' => $ENTITY,
				'ENTITY_ID' => $ID,
			)
		));

		$arGroupPerms = array();

		while ($arPerm = $rsPerm->fetch())
		{
			$arGroupPerms[ $arPerm["GROUP_ID"] ] = $arPerm["PERMS"];
		}

		return $arGroupPerms;
	}
}