<?
use \Bitrix\Main\Localization\Loc;
use \Vdgb\PortalTszh\PermsTable;

Loc::loadMessages(__FILE__);
$ID = IntVal($ID);
$ENTITY = 'tszh';

if ($ID > 0 && CModule::IncludeModule("citrus.tszh") && CModule::IncludeModule("vdgb.portaltszh"))
{
	$arPermType = PermsTable::getPermissionList($ENTITY);
	$perm       = PermsTable::getGroupPermissions($ENTITY, $ID);
	if (!array_key_exists(1, $perm))
	{
		$perm[1] = "X";
	}
	?>
	<tr class="heading">
		<td colspan="2"><? echo GetMessage("TSZH_DEFAULT_ACCESS_TITLE") ?></td>
	</tr>
	<tr>
		<td nowrap width="40%"><? echo GetMessage("TSZH_EVERYONE") ?> [<a class="tablebodylink"
		                                                                  href="/bitrix/admin/group_edit.php?ID=2&amp;lang=<?=LANGUAGE_ID?>">2</a>]:
		</td>
		<td width="60%">

			<select name="GROUP[2]" id="group_2">
				<?
				if ($bVarsFromForm)
				{
					$strSelected = $GROUP[2];
				}
				else
				{
					$strSelected = $perm[2];
				}
				foreach ($arPermType as $key => $val):
					?>
					<option value="<? echo $key ?>"<? if ($strSelected == $key)
						echo " selected" ?>><? echo htmlspecialcharsex($val) ?></option>
				<? endforeach ?>
			</select>

			<script type="text/javascript">
				function OnGroupChange(control, message) {
					var all = document.getElementById('group_2');
					var msg = document.getElementById(message);
					if (all && all.value >= control.value && control.value != '') {
						if (msg) msg.innerHTML = '<?echo CUtil::JSEscape(GetMessage("TSZH_ACCESS_WARNING"))?>';
					}
					else {
						if (msg) msg.innerHTML = '';
					}
				}
			</script>

		</td>
	</tr>
	<tr class="heading">
		<td colspan="2"><? echo GetMessage("TSZH_GROUP_ACCESS_TITLE") ?></td>
	</tr>
	<?
	$groups = CGroup::GetList($by = "sort", $order = "asc", Array("ID" => "~2"));
	while ($r = $groups->GetNext()):
		if ($bVarsFromForm)
		{
			$strSelected = $GROUP[ $r["ID"] ];
		}
		else
		{
			$strSelected = $perm[ $r["ID"] ];
		}

		if ($strSelected == "U" && !CModule::IncludeModule("workflow"))
		{
			$strSelected = "R";
		}

		if ($strSelected != "R" && $strSelected != "S" && $strSelected != "W" && $strSelected != "X" && $ID > 0 && !$bVarsFromForm)
		{
			$strSelected = "";
		}
		?>
		<tr>
			<td nowrap width="40%"><? echo $r["NAME"] ?> [<a class="tablebodylink"
			                                                 href="/bitrix/admin/group_edit.php?ID=<?=$r["ID"]?>&lang=<?=LANGUAGE_ID?>"><?=$r["ID"]?></a>]:
			</td>
			<td width="60%">

				<select name="GROUP[<? echo $r["ID"] ?>]"
				        OnChange="OnGroupChange(this, 'spn_group_<? echo $r["ID"] ?>');">
					<option value=""><? echo GetMessage("TSZH_DEFAULT_ACCESS") ?></option>
					<?
					foreach ($arPermType as $key => $val):
						?>
						<option value="<? echo $key ?>"<? if ($strSelected == $key)
							echo " selected" ?>><? echo htmlspecialcharsex($val) ?></option>
					<? endforeach ?>
				</select>
				<span id="spn_group_<? echo $r["ID"] ?>"></span>
			</td>
		</tr>
	<? endwhile ?>
	<?
}
?>