<?php
require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/citrus.tszh/include.php");

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");
//IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/citrus.tszh/include.php");
IncludeModuleLangFile(__FILE__);

$module_id = 'vdgb.portaltszh';

$arAllOptions = Array(
	Array("entity_type", GetMessage("TSZH_ENTITY_TYPE_SETTINGS"), "", Array("multiselectbox", $groups)),
);

$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("MAIN_TAB_SET"), "ICON" => "tszh_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_SET"))
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$strRedirectURL = false;
if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && check_bitrix_sessid())
{
	if (strlen($RestoreDefaults) > 0)
	{
		COption::RemoveOption($module_id);
	}
	else
	{
		COption::SetOptionString($module_id, 'entity_type', $_REQUEST['entity_type']);
	}
	if (strlen($Update) > 0 && strlen($_REQUEST["back_url_settings"]) > 0)
	{
		LocalRedirect($_REQUEST["back_url_settings"]);
	}
	else
	{
		LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
	}
}

$cur_entity_type = COption::GetOptionString($module_id, 'entity_type');

$aMenu = array();
$context = new CAdminContextMenu($aMenu);
$context->Show();

$tabControl->Begin();
?>
	<form method="post" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?=urlencode($mid)?>&amp;lang=<?=LANGUAGE_ID?>">
		<?=bitrix_sessid_post();?>
		<?
		$tabControl->BeginNextTab();
		?>
		<tr>
			<td width="40%">
				<?=GetMessage('TSZH_ENTITY_TYPE_SETTINGS')?>
			</td>
			<td>
				<select name="entity_type" id="">
					<option value="tszh" <?=$cur_entity_type == 'tszh' ? 'selected' : ''?>><?=GetMessage('TSZH_ENTITY_TYPE_TSZH')?></option>
					<option value="house" <?=$cur_entity_type == 'house' ? 'selected' : ''?>><?=GetMessage('TSZH_ENTITY_TYPE_HOUSE')?></option>
				</select>
			</td>
		</tr>
		<?
		$tabControl->Buttons(); ?>
		<input type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>" class="adm-btn-save">
		<input type="hidden" name="Update" value="Y">
		<? if (strlen($_REQUEST["back_url_settings"]) > 0): ?>
			<input type="button" name="Cancel" value="<?=GetMessage("MAIN_OPT_CANCEL")?>" title="<?=GetMessage("MAIN_OPT_CANCEL_TITLE")?>"
			       onclick="window.location='<? echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
			<input type="hidden" name="back_url_settings" value="<?=htmlspecialcharsbx($_REQUEST["back_url_settings"])?>">
		<? endif ?>
		<input type="submit" name="RestoreDefaults" title="<? echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
		       OnClick="return confirm('<? echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
		       value="<? echo GetMessage("MAIN_RESTORE_DEFAULTS") ?>">
	</form>
<?php $tabControl->End(); ?>