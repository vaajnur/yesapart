<?
$MESS["TSZH_ACT"] = "Активность";
$MESS["TSZH_KEY"] = "Ключ";
$MESS["TSZH_BADGE"] = "Логотип Google reCapcha 3";
$MESS["TSZH_SECRETKEY"] = "Секретный ключ";
$MESS["TSZH_SETTINGS"] = "Параметры 1С: Сайт ЖКХ. Google reCapcha 3 для - ";
$MESS["TSZH_MASK_EXCLUSION"] = "Маска исключения";
$MESS["TSZH_SETTINGS_SAVE_ERROR"] = "Для переключения режима активности модуля заполните поля 'Ключ' и 'Cекретный ключ'";
$MESS["TSZH_KEY_NOTE"] = "Для активации reCapcha на сайте перейдите по ссылке <a href=\"https://www.google.com/recaptcha/admin\" target=\"_blank\">www.google.com</a>, авторизуйтесь своем аккаунте Google. Введите название сайта и выберите тип reCapcha v3. Заполните поле «Домены». Нажмите на кнопку «Регистрация», после этого вы получите ключ и секретный ключ. Скопируйте их в соответствующие поля. </br></br> <b>Внимание:</b> модуль Google reCapcha 3 работает в фоновом режиме и постоянно следит за пользователями сайта. Поэтому физическое нахождение капчи в формах сайта более не требуется.";
$MESS["TSZH_BADGE_NOTE"] = "Отметьте, чтобы дать пользователям понять: на сайте работает reCapcha. Логотип reCapcha будет отображаться в правом нижнем углу сайта.";
$MESS["TSZH_UPDATE_NOTE"] = "Для корректной работы модуля необходимо:<br> 1. Обновить базовый модуль Сайта ЖКХ до версии 14.8.32;<br> 2. Обновить шаблон сайта ЖКХ, согласно <a href=\"https://vgkh.ru/faq-1s_sajt_zhkh/ustanovka_i_obnovlenie/obnovlenie-shablona-na-sayte-zhkkh/\" target=\"_blank\">инструкции</a>.";
?>