<?
global $MESS;
IncludeModuleLangFile(__FILE__);

Class otr_tszhrecaptcha extends CModule
{
	var $MODULE_ID = "otr.tszhrecaptcha";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";

	function otr_tszhrecaptcha()
	{
		$arModuleVersion = array();


		include __DIR__ . "/version.php";

		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

		$this->MODULE_NAME = GetMessage("OTR_TSZH_RECAPTCHA_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("OTR_TSZH_RECAPTCHA_MODULE_DESCRIPTION");
		$this->PARTNER_NAME = GetMessage("OTR_TSZH_RECAPTCHA_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("OTR_TSZH_RECAPTCHA_PARTNER_URI");
	}

	function InstallDB()
	{
		RegisterModule("otr.tszhrecaptcha");
		return true;
	}

	function UnInstallDB()
	{
		COption::RemoveOption("otr.tszhrecaptcha");
		UnRegisterModule("otr.tszhrecaptcha");
		return true;
	}

	function InstallEvents()
	{
		RegisterModuleDependences("main", "OnPageStart", "otr.tszhrecaptcha", "OtrTszhRecaptcha", "OnVerificContent");
		RegisterModuleDependences("main", "OnEndBufferContent", "otr.tszhrecaptcha", "OtrTszhRecaptcha", "OnAddContentCaptcha");
		return true;
	}

	function UnInstallEvents()
	{
		UnRegisterModuleDependences("main", "OnPageStart", "otr.tszhrecaptcha", "OtrTszhRecaptcha", "OnVerificContent");
		UnRegisterModuleDependences("main", "OnEndBufferContent", "otr.tszhrecaptcha", "OtrTszhRecaptcha", "OnAddContentCaptcha");
		return true;
	}

	function InstallFiles()
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/otr.tszhrecaptcha/install/js", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/otr.tszhrecaptcha/install/css", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/css", true, true);
		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFilesEx("/bitrix/js/otr.tszhrecaptcha/");
		DeleteDirFilesEx("/bitrix/css/otr.tszhrecaptcha/");
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;

		if (!IsModuleInstalled("otr.tszhrecaptcha"))
		{
			$this->InstallDB();
			$this->InstallEvents();
			$this->InstallFiles();
		}
	}

	function DoUninstall()
	{
		$this->UnInstallDB();
		$this->UnInstallEvents();
		$this->UnInstallFiles();
	}
}

?>