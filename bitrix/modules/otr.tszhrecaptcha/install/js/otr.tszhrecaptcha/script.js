"use strict";

var Recaptchafree = Recaptchafree || {};
Recaptchafree.items = new Array();
Recaptchafree.form_submit;

function find_and_hide(fields, className, tagName) {
    if (className) {
        if (fields) {
            var parent = BX.findParent(fields, {className: className});
            if (parent) BX.hide(parent);
        }
    }
    else if (tagName) {
        if (fields) {
            var parent = BX.findParent(fields, {tagName: tagName});
            if (parent) BX.hide(parent);
        }
    }

}

function RecaptchafreeSubmitForm(token) {
    if (Recaptchafree.form_submit !== undefined) {
        var x = document.createElement("INPUT"); // create token input
        x.setAttribute("type", "hidden");
        x.name = "g-recaptcha-response";
        x.value = token;
        Recaptchafree.form_submit.appendChild(x);  // append current form
        var elements = Recaptchafree.form_submit.elements;
        for (var i = 0; i < elements.length; i++) {
            if (elements[i].getAttribute("type") === "submit") {
                var submit_hidden = document.createElement("INPUT"); // create submit input hidden
                submit_hidden.setAttribute("type", "hidden");
                submit_hidden.name = elements[i].name;
                submit_hidden.value = elements[i].value;
                Recaptchafree.form_submit.appendChild(submit_hidden);  // append current form
            }
        }
        document.createElement('form').submit.call(Recaptchafree.form_submit); // submit form
    }
};

function onloadRecaptchafree() {
    if (document.addEventListener) {
        document.addEventListener('submit', function (e) {
            if (e.target && e.target.tagName === "FORM") {
                var g_recaptcha = e.target.querySelectorAll('div.g-recaptcha-3');
                if (g_recaptcha[0] !== undefined) {
                    var key_id = g_recaptcha[0].getAttribute("data-sitekey");
                    grecaptcha.execute(key_id, {action: 'contactForm'}).then(function (token) {
                        RecaptchafreeSubmitForm(token);
                    });
                    Recaptchafree.form_submit = e.target;
                    e.preventDefault();

                }
            }
        }, false);
    }
};

function RecaptchafreeSubmitFormForAjax(form, token) {
    Recaptchafree.form_submit = form;
    if (Recaptchafree.form_submit !== undefined) {
        var recp = $(Recaptchafree.form_submit).find("input[name = 'g-recaptcha-response']");
        if (recp[0]) recp[0].value = token;
        else {
            var x = document.createElement("INPUT");
            x.setAttribute("type", "hidden");
            x.name = "g-recaptcha-response";
            x.value = token;
            Recaptchafree.form_submit.appendChild(x);
        }
    }
};