<?

use Bitrix\Main\Page\Asset,
	Bitrix\Main\Application,
	Bitrix\Main\SystemException;


class OtrTszhRecaptcha
{
	public function FindParent($e)
	{
		$parent = $e->parent();
		$taqName = $parent->tag;
		if (($taqName != "tr") && ($taqName != "div") && ($taqName != "fieldset"))
		{
			$parent = $e->parent()->parent();
		}
		if ($parent)
		{
			$parent->setAttribute("style", "display:none");
		}
	}

	public function FindParentIblock_add($e)
	{
		$parent = $e->parent();
		$taqName = $parent->tag;

		if (($taqName == "div") && (($parent->getAttribute('class') == "ask-form__cell") || ($parent->getAttribute('class') == "window__input-name_pas")))
		{
			$parent = $e->parent()->parent();
			$taqName = $parent->tag;
			if (($parent) && (($parent->getAttribute('class') == "ask-form__row") || ($parent->getAttribute('class') == "window__block")))
			{
				$parent->setAttribute("style", "display:none");
			}
		}
		if ($taqName == "td")
		{
			$parent = $e->parent()->parent();
			if ($parent)
			{
				$parent->setAttribute("style", "display:none");
			}
		}
	}

	public function OnAddContentCaptcha(&$content)
	{
		if (!defined("ADMIN_SECTION"))
		{
			$arSettings = self::getParamSite();
			if ($arSettings["act"] == "Y" && $arSettings["key"] && $arSettings["secretkey"] && !self::checkMask($arSettings["mask_exclusion"]))
			{

				if (ini_get('mbstring.func_overload') & 2) {
					if (file_exists($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/citrus.tszh/cssin/vendor/simple_html_dom/simple_html_dom_overload_2.php'))
					{
						require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/citrus.tszh/cssin/vendor/simple_html_dom/simple_html_dom_overload_2.php');
					}
				} else {

					if (file_exists($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/citrus.tszh/cssin/vendor/simple_html_dom/simple_html_dom.php'))
					{
						require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/citrus.tszh/cssin/vendor/simple_html_dom/simple_html_dom.php');
					}
				}

				$dom_content = false;
				if ((ini_get('mbstring.func_overload') & 2)  && function_exists(str_get_html_overload2)) {
					$dom_content = str_get_html_overload2($content, $lowercase=true, $forceTagsClosed=true, $target_charset = DEFAULT_TARGET_CHARSET, $stripRN=false);
				}
				else  {
					$dom_content = str_get_html($content, $lowercase=true, $forceTagsClosed=true, $target_charset = DEFAULT_TARGET_CHARSET, $stripRN=false);
				}

				if ($dom_content)
				{
					/*
					form[name='iblock_add'] - ����� �������� ����� �� �������� ������ ����� ��� ������ �������
					div[id='contacts-page-feedback'] - ����� �������� ����� �� �������� �������� ��� ������ �������
					form[id='feedbackForm'] - ����� �������� ����� � ������ ������
					form[name='regform'] - ����� ����������� ��� ������ �������
					form[name='form_auth'] - ����� ����������� ��� ������ �������
					div[class='block-auth'] form[class='block-auth-form'] - ����� ����������� ���������� � ������
					form[class='forgot-password'] - ����� �������������� ������
					form[name='block-register-form'] - ����� ����������� � �������
					form[class='block-password-recovery-form'] - ����� �������������� ������ � �������
					div[id='window-auth'] form[class='block-auth-form'] - ����� ����������� � �������
					*/

					$e = $dom_content->find("div[id='contacts-page-feedback'] input[name='captcha_sid'], form[id='feedbackForm'] input[name='captcha_sid'], form[class='forgot-password'] input[name='captcha_sid']");
					foreach ($e as $e_arr)
					{
						OtrTszhRecaptcha::FindParent($e_arr);
					}
					$e = $dom_content->find("div[id='window-auth'] form[class='block-auth-form'] input[name='captcha_sid']");
					foreach ($e as $e_arr)
					{
						OtrTszhRecaptcha::FindParent($e_arr);
					}

					$e = $dom_content->find("form[name='regform'] input[name='captcha_sid']");
					foreach ($e as $e_arr)
					{
						OtrTszhRecaptcha::FindParent($e_arr);
						$bx_auth_title = $e_arr->parent()->parent()->parent()->parent()->firstChild();
						if ($bx_auth_title->getAttribute('class') == "bx-auth-title")
						{
							$bx_auth_title->setAttribute("style", "display:none");
						}
					}

					$e = $dom_content->find("form[name='block-register-form'] input[name='captcha_sid']");
					foreach ($e as $e_arr)
					{
						OtrTszhRecaptcha::FindParent($e_arr);
						$bx_auth_title = $e_arr->parent()->parent()->parent()->firstChild();
						if ($bx_auth_title->getAttribute('class') == "window__fieldgroup")
						{
							$bx_auth_title->setAttribute("style", "display:none");
						}
					}

					$e = $dom_content->find("form[name='iblock_add'] input[name='captcha_sid'], form[class='block-password-recovery-form'] input[name='captcha_sid']");
					foreach ($e as $e_arr)
					{
						OtrTszhRecaptcha::FindParentIblock_add($e_arr);
					}

					$e = $dom_content->find("div[id='contacts-page-feedback'] input[name='captcha_word'], form[id='feedbackForm'] input[name='captcha_word'], form[name='regform'] input[name='captcha_word'], form[class='forgot-password'] input[name='captcha_word'], form[name='block-register-form'] input[name='captcha_word'], form[class='block-password-recovery-form'] input[name='captcha_word']");
					foreach ($e as $e_arr)
					{
						$e_arr->setAttribute("value", substr($arSettings["key"], 0, 5));
						OtrTszhRecaptcha::FindParent($e_arr);
					}


					$e = $dom_content->find("form[name='form_auth'] input[name='captcha_word'] , div[class='block-auth'] form[class='block-auth-form'] input[name='captcha_word']");
					foreach ($e as $e_arr)
					{
						$e_arr->setAttribute("value", substr($arSettings["key"], 0, 5));
						$parent = $e_arr->parent();
						$taqName = $parent->tag;
						if (($taqName != "form") && ($taqName != "p")) OtrTszhRecaptcha::FindParent($e_arr);
						else $e_arr->setAttribute("style", "display:none");
					}


					$e = $dom_content->find("form[name='iblock_add'] input[name='captcha_word']");
					foreach ($e as $e_arr)
					{
						$e_arr->setAttribute("value", substr($arSettings["key"], 0, 5));
						OtrTszhRecaptcha::FindParentIblock_add($e_arr);
					}

					$e = $dom_content->find("div[id='window-auth'] form[class='block-auth-form'] input[name='captcha_word']");
					foreach ($e as $e_arr)
					{
						$e_arr->setAttribute("value", substr($arSettings["key"], 0, 5));
						OtrTszhRecaptcha::FindParent($e_arr);
					}


					$e = $dom_content->find("form[class='block-auth-form'] img[src*='captcha.php'], form[name='iblock_add'] img[src*='captcha.php'], div[id='contacts-page-feedback'] [src*='captcha.php'], form[id='feedbackForm'] [src*='captcha.php'], form[name='regform'] [src*='captcha.php'], form[name='form_auth'] [src*='captcha.php'], form[class='forgot-password'] [src*='captcha.php'], form[name='block-register-form'] [src*='captcha.php'], form[class='block-password-recovery-form'] [src*='captcha.php'], div[class='block-auth'] form[class='block-auth-form'] [src*='captcha.php']");
					foreach ($e as $e_arr)
					{
						if ($e_arr)
						{
							$e_arr->outertext = '<div class="g-recaptcha-3" data-sitekey="' . $arSettings["key"] . '"  data-callback="RecaptchafreeSubmitForm"></div>';
						}
					}

					$dom_content->_charset = 'UTF-8';
					$content = \Bitrix\Main\Text\Encoding::convertEncodingToCurrent($dom_content->save());
				}
			}
		}
	}

	public function OnVerificContent()
	{
		if (defined("ADMIN_SECTION"))
		{
			return;
		}
		$arSettings = self::getParamSite();

		if ($arSettings["act"] !== "Y")
		{
			return;
		}
		Asset::getInstance()->addJs('/bitrix/js/otr.tszhrecaptcha/script.js');
		Asset::getInstance()->addJs('https://www.google.com/recaptcha/api.js?onload=onloadRecaptchafree&render=' . $arSettings["key"]);

		$_SESSION["OTR_TSZH_RECAPTCHA_KEY"] = $arSettings["key"];

		if ($arSettings["badge"] !== "Y")
		{
			Asset::getInstance()->addCss('/bitrix/css/otr.tszhrecaptcha/style.css');
		}

		$arRequest = self::getParamRequest();
		if (empty($arRequest["captcha_sid"]) || empty($arRequest["g-recaptcha-response"]))
		{
			return;
		}

		$res = json_decode(self::getOutData($arRequest, $arSettings), true);

		try
		{
			if (($res['success']) && ($res['score'] > 0.5))
			{ // success unswer from google
				global $DB;
				$DB->PrepareFields("b_captcha");
				$arFields = array("CODE" => "'" . $DB->ForSQL(strtoupper($arRequest["captcha_word"]), 5) . "'");
				$DB->StartTransaction();
				$DB->Update("b_captcha", $arFields, "WHERE ID='" . $DB->ForSQL($arRequest["captcha_sid"], 32) . "'", $err_mess . __LINE__);
				$DB->Commit();
			}
			else
			{
				throw new SystemException("Please verify your reCAPTCHA");
			}
		}
		catch (SystemException $exception)
		{
			$exception->getMessage();
		}
		try
		{
			if (!is_array($res))
			{
				throw new SystemException("Not work \"curl_init\" or \"file_get_contents\" functions");
			}
		}
		catch (SystemException $exception)
		{
			$exception->getMessage();
		}
	}

	private function getParamRequest()
	{
		$arRequest = array();
		$context = Application::getInstance()->getContext();
		$request = $context->getRequest();
		if ($request->isPost())
		{
			$captcha_sid = $request->getPost("captcha_sid");
			$captcha_code = $request->getPost("captcha_code");
			$arRequest["captcha_sid"] = (!empty($captcha_sid)) ? $captcha_sid : $captcha_code;
			$arRequest["captcha_word"] = $request->getPost("captcha_word");
			$arRequest["g-recaptcha-response"] = $request->getPost("g-recaptcha-response");
		}
		else
		{
			$captcha_sid = $request->getQuery("captcha_sid");
			$captcha_code = $request->getQuery("captcha_code");
			$arRequest["captcha_sid"] = (!empty($captcha_sid)) ? $captcha_sid : $captcha_code;
			$arRequest["captcha_word"] = $request->getQuery("captcha_word");
			$arRequest["g-recaptcha-response"] = $request->getQuery("g-recaptcha-response");
		}
		return $arRequest;
	}


	private function getParamSite()
	{
		$settings = COption::GetOptionString("otr.tszhrecaptcha", "settings", false, SITE_ID);
		return unserialize($settings);
	}

	private function checkMask($mask_exc)
	{
		$request = Application::getInstance()->getContext()->getServer();
		$mask = explode(";", $mask_exc);
		$arMask = array_map(function ($n) {
			return trim($n);
		}, $mask);
		$reg = '%^' . implode('|', $arMask) . '%i';
		if ($request["REAL_FILE_PATH"])
		{
			$url = $request["REAL_FILE_PATH"];
		}
		else
		{
			$url = $request->getScriptName();
		}
		if (!preg_match($reg, $url))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	private function getOutData($arRequest, $arSettings)
	{
		$curlData = false;
		$google_url = "https://www.google.com/recaptcha/api/siteverify";
		$url = $google_url . "?secret=" . $arSettings["secretkey"] . "&response=" . $arRequest["g-recaptcha-response"];

		if (function_exists('curl_init'))
		{ // support curl
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_TIMEOUT, 10);
			$curlData = curl_exec($curl);
			curl_close($curl);
		}
		else
		{ // support file_get_contents
			$curlData = file_get_contents($url);
		}
		return $curlData;
	}
}