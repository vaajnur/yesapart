<?

if (!CModule::IncludeModule("citrus.tszh"))
{
	return false;
}

CModule::AddAutoloadClasses(
	'otr.tszhrecaptcha',
	array(
		'OtrTszhRecaptcha' => 'classes/general/otrtszhrecaptcha.php',
	)
);