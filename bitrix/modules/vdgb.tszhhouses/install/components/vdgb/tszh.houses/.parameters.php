<?
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

$arSorts = Array(
	"ASC" => Loc::getMessage('TSZH_DESC_ASC'),
	"DESC" => Loc::getMessage('TSZH_DESC_DESC'),
);

$arSortFields = Array(
	"ID" => 'ID',
	"AREA" => Loc::getMessage('TSZH_HOUSE_AREA'),
	"ADDRESS_USER_ENTERED" => Loc::getMessage('TSZH_HOUSE_ADDRESS'),
);
$arSelectFields = Array(
	"TYPE" => Loc::getMessage('TSZH_HOUSE_TYPE'),
	"ADDRESS_USER_ENTERED" => Loc::getMessage('TSZH_HOUSE_ADDRESS'),
	"YEAR_OF_BUILT" => Loc::getMessage('TSZH_HOUSE_YEAR_OF_BUILT'),
	"FLOORS" => Loc::getMessage('TSZH_HOUSE_FLOORS'),
	"PORCHES" => Loc::getMessage('TSZH_HOUSE_PORCHES'),
	"AREA" => Loc::getMessage('TSZH_HOUSE_AREA'),
);

// ������ �������� ����������
$rsItemTszh = CTszh::GetList(array(), array(), false, false, array("ID", "NAME")); // �������� ������ ���
$arTszhList[0] = Loc::getMessage('ALL_TSZH');
while ($arItemTszh = $rsItemTszh->GetNext())
{
	$arTszhList[$arItemTszh["ID"]] = "[" . $arItemTszh["ID"] . "] " . htmlspecialchars_decode($arItemTszh["NAME"]);
}

$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"TSZH_COUNT" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('TSZH_DESC_LIST_CONT'),
			"TYPE" => "STRING",
			"DEFAULT" => "20",
		),
		"SORT_BY" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => Loc::getMessage('TSZH_DESC_IBORD'),
			"TYPE" => "LIST",
			"DEFAULT" => "ID",
			"VALUES" => $arSortFields,
			"ADDITIONAL_VALUES" => "Y",
		),
		"TSZH_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('TSZH_DESC_TSZH'),
			"TYPE" => "LIST",
			"DEFAULT" => 0,
			"VALUES" => $arTszhList,
		),
		"SELECT_FIELDS" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => Loc::getMessage('TSZH_DESC_ISELECT'),
			"TYPE" => "LIST",
			"VALUES" => $arSelectFields,
			"ADDITIONAL_VALUES" => "N",
			"DEFAULT" => array("TYPE", "ADDRESS_USER_ENTERED", "YEAR_OF_BUILT", "FLOORS", "PORCHES", "AREA"),
			"SIZE" => 6,
			"MULTIPLE" => "Y",
		),
		"SORT_ORDER" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => Loc::getMessage('TSZH_DESC_IBBY'),
			"TYPE" => "LIST",
			"DEFAULT" => "DESC",
			"VALUES" => $arSorts,
		),
	),
);

?>