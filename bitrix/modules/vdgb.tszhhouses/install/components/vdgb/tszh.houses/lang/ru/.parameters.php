<?
$MESS ['IBLOCK_DETAIL_URL'] = "URL, ведущий на страницу с содержимым элемента раздела";
$MESS ['TSZH_DESC_ASC'] = "По возрастанию";
$MESS ['TSZH_DESC_DESC'] = "По убыванию";
$MESS ['TSZH_DESC_FID'] = "ID";
$MESS ['TSZH_DESC_FTSAMP'] = "Дата последнего изменения";
$MESS ['TSZH_DESC_IBORD'] = "Поле для сортировки домов ТСЖ";
$MESS ['TSZH_DESC_ISELECT'] = "Поля для показа в списке домов ТСЖ";
$MESS ['TSZH_DESC_IBBY'] = "Направление для сортировки домов ТСЖ";
$MESS ['TSZH_DESC_LIST_CONT'] = "Количество домов ТСЖ на странице";
$MESS ['ALL_TSZH'] = "(все организации)";
$MESS ['TSZH_DESC_PAGER_TSZH'] = "ТСЖ";
$MESS["TSZH_HOUSE_TYPE"] = "Тип";
$MESS["TSZH_HOUSE_ADDRESS"] = "Адрес";
$MESS["TSZH_HOUSE_YEAR_OF_BUILT"] = "Год постройки";
$MESS["TSZH_HOUSE_FLOORS"] = "Количество этажей";
$MESS["TSZH_HOUSE_PORCHES"] = "Количество подъездов";
$MESS["TSZH_HOUSE_AREA"] = "Общая площадь, кв.м.";
$MESS["TSZH_DESC_TSZH"] = "Объект управления";
?>