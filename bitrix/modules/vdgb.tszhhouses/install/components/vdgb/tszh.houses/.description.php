<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("COMPONENT_NAME"),
    "DESCRIPTION" => GetMessage("COMPONENT_NAME"),
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "citrus.tszh",
        "NAME" => GetMessage("COMPONENT_ROOT_SECTION"),
    ),
);

?>