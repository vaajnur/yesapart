<?php
$MESS["TSZH_HOUSE_TYPE"] = "Тип";
$MESS["TSZH_HOUSE_ADDRESS_USER_ENTERED"] = "Адрес";
$MESS["TSZH_HOUSE_YEAR_OF_BUILT"] = "Год<br/> постройки";
$MESS["TSZH_HOUSE_FLOORS"] = "Количество<br/> этажей";
$MESS["TSZH_HOUSE_PORCHES"] = "Количество<br/> подъездов";
$MESS["TSZH_HOUSE_AREA"] = "Общая<br/>площадь, кв.м.";
?>