<?php

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/install/wizard_sol/utils.php");

if (class_exists('vdgb_tszhhouses'))
{
	return;
}

class vdgb_tszhhouses extends CModule
{
	var $MODULE_ID = "vdgb.tszhhouses";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";

	function __construct()
	{
		$arModuleVersion = array();

		include(dirname(__FILE__) . "/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else
		{
			$this->MODULE_VERSION = "1.0.6";
			$this->MODULE_VERSION_DATE = "22.01.2018";
		}

		$this->MODULE_NAME = Loc::getMessage("TSZH_HOUSES_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("TSZH_HOUSES_MODULE_DESCRIPTION");

		$this->PARTNER_NAME = Loc::getMessage("TSZH_HOUSES_PARTNER_NAME");
		$this->PARTNER_URI = Loc::getMessage("TSZH_HOUSES_PARTNER_URI");

	}

	public function doInstall()
	{
		$this->installFiles();
		$this->installDB();
	}

	public function doUninstall()
	{
		$this->uninstallDB();
		$this->unInstallFiles();
	}

	public function installDB()
	{
		ModuleManager::registerModule($this->MODULE_ID);
	}

	public function uninstallDB()
	{
		ModuleManager::unRegisterModule($this->MODULE_ID);
	}

	public function installFiles()
	{
		CopyDirFiles(
			Application::getDocumentRoot() . "/bitrix/modules/" . $this->MODULE_ID . "/install/components",
			Application::getDocumentRoot() . "/bitrix/components/",
			true,
			true
		);

		if (tszhCheckMinEdition("standard") && !(tszhCheckMinEdition('smallbusiness')))
		{
			CopyDirFiles(
				Application::getDocumentRoot() . "/bitrix/modules/" . $this->MODULE_ID . "/install/public/ru/houses",
				Application::getDocumentRoot() . "/houses",
				true,
				true
			);

			\CAdminNotify::DeleteByTag("add_houses");
			\CAdminNotify::Add(
				array(
					"MESSAGE" => Loc::getMessage('TSZH_HOUSES_ADD_HOUSES_LINK'),
					"TAG" => "add_houses",
					"MODULE_ID" => "vdgb.tszhhouses",
					"ENABLE_CLOSE" => "Y",
				)
			);
		}
	}

	public function unInstallFiles()
	{
		DeleteDirFiles(
			Application::getDocumentRoot() . '/bitrix/modules/' . $this->MODULE_ID . '/install/components',
			Application::getDocumentRoot() . '/bitrix/components'
		);
		DeleteDirFiles(
			Application::getDocumentRoot() . '/bitrix/modules/' . $this->MODULE_ID . '/install/public/ru/houses',
			Application::getDocumentRoot() . '/houses'
		);
	}
}
