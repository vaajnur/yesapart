<?
/**
 * ������ ���ƻ
 * �������� ������� ��������� ����������� (�������������) �� 1�
 * @package tszh
*/

// ��������� ��� ����������� �����:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/include.php"); // ������������� ������
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/prolog.php"); // ������ ������

$bSupportIncluded = CModule::IncludeModule('citrus.tszhtickets');

if ($bSupportIncluded)
	tszhTicketsCheck();

// ��������� �������� ����
IncludeModuleLangFile(__FILE__);

set_time_limit(0);

$step = IntVal($_REQUEST['step']);
if ($step < 1 || $step > 2 || !check_bitrix_sessid()) {
	$step = 1;
}

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszhtickets");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT < "W")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$strError = false;

if (!$bSupportIncluded) {
	$strError .= GetMessage("CITRUS_TSZH_TICKETS_MODULE_NOT_FOUND");
}

// 123 466,67 => 123466.67
function citrusTszhTicketsFVal($value) {

	$val = str_replace(
		Array(',', ' ', ' '),
		Array('.', '', ''),
		$value
	);
	return floatval($val);

}

$arSupportTeamGroups = Array();

$strSupportTeamGroupIDs = implode(' | ', CTszhTicket::GetGroupsByRole(CTszhTicket::GetSupportTeamRoleID()));
$arFilter = Array(
    "ID"             => $strSupportTeamGroupIDs,
    "ACTIVE"         => "Y",
);
$rsGroups = CGroup::GetList(($by="c_sort"), ($order="desc"), $arFilter);
while ($arSupportTeamGroup = $rsGroups->GetNext()) {
	$arSupportTeamGroups[$arSupportTeamGroup['ID']] = $arSupportTeamGroup;
}
if (count($arSupportTeamGroups) <= 0) {
	$strError .= GetMessage("CITRUS_TSZH_TICKETS_GROUP_NOT_FOUND");
}


if (!$strError && $_SERVER["REQUEST_METHOD"] == "POST" && $step > 1 && check_bitrix_sessid()) {

	// ������ ��� ������� - �������� � ��������� ������
	if ($step == 2) {

		$support_group = IntVal($_REQUEST['support_group']);
		if ($support_group <= 0 || !array_key_exists($support_group, $arSupportTeamGroups)) {
			$strError .= GetMessage("CITRUS_TSZH_TICKETS_SPECIFIED_GROUP_NOT_FOUND", Array("#GROUP_ID#" => $support_group));
		}
		if (!is_uploaded_file($_FILES["import_file"]["tmp_name"])) {
			$strError .= GetMessage("CITRUS_TSZH_TICKETS_SPECIFY_FILE");
		}

		if (!$strError) {

			$file_contents = file_get_contents($_FILES['import_file']["tmp_name"]);

			// �������� ��� �������� �������, ������������� � ������������� �����:
			//  0 - ������ �� ������
			//  1 - ��������������
			//  2 - �������
			$missing_users_action = IntVal($_REQUEST['missing_users_action']);
			if ($missing_users_action <= 0 || $missing_users_action > 2)
				$missing_users_action = 1;

			try {

				$cnt_updated_users = 0;

				$xml = new XML($file_contents);

				/*
					�������� ������ ������� ������������� ������ ���������� �����������
				*/
				$arSupportUsers = Array();
				$arUsersFilter = Array(
					'GROUPS_ID' => $support_group,
				);
				$rsSupportUsers = CUser::GetList(($by="ID"), ($order="asc"), $arUsersFilter); // �������� �������������
				while ($arSupportUser = $rsSupportUsers->Fetch()) {
					$xml_id = strlen($arSupportUser['XML_ID']) > 0 ? $arSupportUser['XML_ID'] : $arSupportUser['ID'];
					$arSupportUsers[$xml_id] = $arSupportUser;
				}
				//echo '<pre>' . htmlspecialchars(print_r($arSupportUsers, true)) . '</pre>';

				$rsUsers = $xml->responsible_users;
				$arUpdateUsers = Array();
				foreach ($rsUsers->responsible_user as $arUser) {
					$email = trim($arUser['email']);
					if (strlen($email) <= 0 || !check_email($email)) {
						$email = 'support' . $arUser['kod'] . '@' . $_SERVER["SERVER_NAME"];
					}
					$full_name = trim($arUser['name']);

					$arNewUser = Array(
						'EMAIL' => $email,
						'XML_ID' => $arUser['kod'],
						'NAME' => $full_name
					);

					// ������ ���
					if (preg_match('/^(.*?)(\s+(.*?)(\s+(.*?))?)?$/', $full_name, $m)) {
						$arNewUser['LAST_NAME'] = $m[1];
						$arNewUser['NAME'] = $m[3];
						$arNewUser['SECOND_NAME'] = $m[5];
					}

					$arUpdateUsers[$arUser['kod']] = $arNewUser;
				}
				//echo '<pre>' . htmlspecialchars(print_r($arUpdateUsers, true)) . '</pre>';

				$_user = new CUser;
				foreach ($arUpdateUsers as $arUpdateUser) {

					$arFields = $arUpdateUser;

					if (array_key_exists($arUpdateUser['XML_ID'], $arSupportUsers)) {

						// ���������� ������������

						$arExistingUser = $arSupportUsers[$arUpdateUser['XML_ID']];
						unset($arSupportUsers[$arUpdateUser['XML_ID']]);

						$arFields['ACTIVE'] = 'Y';
						if ($_user->Update($arExistingUser['ID'], $arFields)) {
							$cnt_updated_users++;
						} else {
							//TODO: ��������� �� ������� ��� ���������� �������������
						}

					} else {

						// ���������� ������������
						$password = tszhGeneratePassword();

						$arFields['ACTIVE'] = 'Y';
						$arFields['GROUP_ID'] = Array($support_group);
						$arFields['LOGIN'] = 'support_' . $arFields['XML_ID'];
						$arFields['PASSWORD'] = $password;
						$arFields['CONFIRM_PASSWORD'] = $password;
						$arFields['ADMIN_NOTES'] = GetMessage("CITRUS_TSZH_TICKETS_USER_ADMIN_NOTES", Array("#PASSWORD#" => $password));

						$ID = $_user->Add($arFields);
						if ($ID) {
							$arFields['ID'] = $ID;
							$new_users[] = $arFields;
						} else {
							//TODO: ��������� �� ������� ��� ���������� �������������
						}

					}

				}


			} catch (Exception $e) {
				$strError .= GetMessage("CITRUS_TSZH_TICKETS_XML_PARSE_ERROR") . $e->getMessage() . "<br />";
			}

			if ($missing_users_action > 0) {

				// � $arSupportUsers �������� ������������, ������� �� ���� � ����� �������
				foreach ($arSupportUsers as $arUser) {

					//$arUserGroups = CUser::GetUserGroup($arUser['ID']);

					$cnt_missing_users++;

					switch ($missing_users_action) {
						case 1: // ��������������
							$obUserUpdate = new CUser;
							$arUserFields = Array( "ACTIVE" => 'N' );
							$obUserUpdate->Update($arUser['ID'], $arUserFields);
							break;

						case 2: // �������
							CUser::Delete($arUser['ID']);
							break;

						default:
							break;
					}
				}
			}
/*
			if ($strError)
				$DB->RollBack();
			else
				$DB->Commit();
*/
		}

		if (strlen($strError)>0)
			$step = 1;

	}

}
?>
<?

$APPLICATION->SetTitle(GetMessage("CITRUS_TSZH_TICKETS_IMPORT_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php"); // ������ ����� ������

echo $demoNotice;


//==========================================================================================
//==========================================================================================


CAdminMessage::ShowMessage($strError);

if ($bSupportIncluded):
?>
<form method="POST" action="<?echo $sDocPath?>?lang=<?echo LANG ?>" enctype="multipart/form-data" name="dataload" id="dataload">
<?=bitrix_sessid_post()?>
<?

// ����� ��������
//==========================================================================================
$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("CITRUS_TSZH_TICKETS_DATA_FILE"), "ICON" => "iblock", "TITLE" => GetMessage("CITRUS_TSZH_TICKETS_DATA_FILE")),
	array("DIV" => "edit2", "TAB" => GetMessage("CITRUS_TSZH_TICKETS_IMPORT"), "ICON" => "iblock", "TITLE" => GetMessage("CITRUS_TSZH_TICKETS_IMPORT")),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs, false);
$tabControl->Begin();
?>

<?
$tabControl->BeginNextTab();

if ($step == 1) {?>
	<tr>
		<td width="30%"><?=GetMessage("CITRUS_TSZH_TICKETS_XML_FILE")?>:</td>
		<td>
			<input type="file" name="import_file" />
		</td>
	</tr>
	<tr>
		<td width="30%"><?=GetMessage("CITRUS_TSZH_TICKETS_USERS_GROUP")?>:</td>
		<td>

<?

$arGroupReference = Array();
foreach ($arSupportTeamGroups as $ID => $arSupportTeamGroup) {
	$arGroupReference['REFERENCE'][] = $arSupportTeamGroup['REFERENCE'];
	$arGroupReference['REFERENCE_ID'][] = $arSupportTeamGroup['REFERENCE_ID'];
}
echo SelectBoxFromArray('support_group', $arGroupReference, '', '', '', false, 'dataload');

?>
		</td>
	</tr>
	<tr>
		<td style="vertical-align: top;"><?=GetMessage("CITRUS_TSZH_TICKETS_MISSING_ACTION")?>:</td>
		<td>
			<label><input type="radio" value="0" name="missing_users_action" /> <?=GetMessage("CITRUS_TSZH_TICKETS_NOTHING")?></label><br />
			<label><input type="radio" value="1" checked="checked" name="missing_users_action" /> <?=GetMessage("CITRUS_TSZH_TICKETS_DEACTIVATE")?></label><br />
			<label><input type="radio" value="2" name="missing_users_action" /> <?=GetMessage("CITRUS_TSZH_TICKETS_DELETE")?></label>
		</td>
	</tr>
<?
}

$tabControl->EndTab();
?>


<?
$tabControl->BeginNextTab();

if ($step == 2) {?>
	<tr class="heading">
		<td colspan="2"><?=GetMessage("CITRUS_TSZH_TICKETS_XML_FILE_TITLE")?></td>
	</tr>
	<tr>
		<td colspan="2"><?

			echo GetMessage("CITRUS_TSZH_TICKETS_UPDATED_USERS", Array("#CNT#" => $cnt_updated_users));

			$cnt = count($new_users);
			echo GetMessage("CITRUS_TSZH_TICKETS_IMPORTED_USERS", Array("#CNT#" => $cnt));
			if ($cnt > 0) {
				echo "(";
				foreach ($new_users as $idx=>$new_user) {
					echo "<a href=\"/bitrix/admin/user_edit.php?ID={$new_user['ID']}&lang=ru\">{$new_user['LOGIN']}</a>";
					if ($idx != count($new_users)-1)
						echo ", ";
				}
				echo ")<br />";
			}

			echo '<br />';

			if ($cnt_missing_users > 0)
				echo ($missing_users_action == 1 ? GetMessage("CITRUS_TSZH_TICKETS_DEACTIVATED") : GetMessage("CITRUS_TSZH_TICKETS_DELETED")) . GetMessage("CITRUS_TSZH_TICKETS_USERS_COUNT", Array("#CNT#" => $cnt_missing_users));

		?>
		</td>
	</tr>
<?
}

$tabControl->EndTab();
?>

<?
$tabControl->Buttons();
?>
<input type="hidden" name="step" value="<?=$step+1?>" />
<? if ($step < 2) { ?>
<input type="submit" name="submit_btn" value="<?=GetMessage("CITRUS_TSZH_TICKETS_BTN_NEXT")?>" />
<? } else { ?>
<input type="submit" name="submit_btn" value="<?=GetMessage("CITRUS_TSZH_TICKETS_BTN_FINISH")?>" />
<? } ?>

<?
$tabControl->End();
?>
</form>

<script language="JavaScript">
<!--
<?if ($step < 2):?>
tabControl.SelectTab("edit1");
tabControl.DisableTab("edit2");
<?elseif ($step >= 2):?>
tabControl.SelectTab("edit2");
tabControl.DisableTab("edit1");
<?endif;?>
//-->
</script>
<?
endif; // if ($bSupportIncluded)
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>
