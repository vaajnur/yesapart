<?
IncludeModuleLangFile(__FILE__);
if (!$USER->IsAuthorized())
	return false;
$SUP_RIGHT = $APPLICATION->GetGroupRight("citrus.tszhtickets");

if ($SUP_RIGHT > "T")
{
	$aMenu = array(
		"parent_menu" => "global_menu_services",
		"section" => "citrus.tszhtickets",
		"sort" => 12,
		"text" => GetMessage("SUP_CIT_SUPPORT"),
		"title" => GetMessage("SUP_CIT_SUPPORT_TITLE"),
		"icon" => "tszh_tickets_menu_icon",
		"page_icon" => "tszh_tickets_page_icon",
		"items_id" => "menu_tszh_tickets",
		"items" => array()
	);

	if ($SUP_RIGHT >= "T")
		$aMenu["items"][] = array(
			"text" => GetMessage("SUP_M_REPORT_TABLE"),
			"url" => "tszh_ticket_desktop.php?lang=" . LANGUAGE_ID . "&amp;set_default=Y",
			"more_url" => Array("tszh_ticket_desktop.php"),
			"title" => GetMessage("SUP_M_REPORT_TABLE_ALT")
		);

	$aMenu["items"][] = array(
		"text" => GetMessage("SUP_M_TICKETS"),
		"url" => "tszh_ticket_list.php?lang=" . LANGUAGE_ID . "&amp;set_default=Y",
		"more_url" => Array(
			"tszh_ticket_list.php",
			"tszh_ticket_edit.php",
			"tszh_ticket_message_edit.php"
		),
		"title" => GetMessage("SUP_M_TICKETS_ALT")
	);

	if ($SUP_RIGHT >= "T")
		$aMenu["items"][] = array(
			"text" => GetMessage("SUP_M_REPORT_GRAPH"),
			"url" => "tszh_ticket_report_graph.php?lang=" . LANGUAGE_ID . "&amp;set_default=Y",
			"more_url" => Array("tszh_ticket_report_graph.php"),
			"title" => GetMessage("SUP_M_REPORT_GRAPH_ALT")
		);

	if ($SUP_RIGHT >= "V")
	{
		$aMenu["items"][] = array(
			"text" => GetMessage("SUP_M_DICT"),
			"title" => GetMessage("SUP_M_DICT_TITLE"),
			"url" => "tszh_ticket_dict_index.php?lang=" . LANGUAGE_ID,
			"items_id" => "menu_tszh_tickets_dict",
			"page_icon" => "tszh_tickets_page_icon",
			"items" => Array(
				array(
					"text" => GetMessage("SUP_M_CATEGORY"),
					"url" => "tszh_ticket_dict_list.php?lang=" . LANGUAGE_ID . "&amp;find_type=C",
					"more_url" => Array(
						"tszh_ticket_dict_edit.php?find_type=C",
						"tszh_ticket_dict_list.php?find_type=C"
					),
					"title" => GetMessage("SUP_M_CATEGORY")
				),
				array(
					"text" => GetMessage("SUP_M_CRITICALITY"),
					"url" => "tszh_ticket_dict_list.php?lang=" . LANGUAGE_ID . "&amp;find_type=K",
					"more_url" => Array(
						"tszh_ticket_dict_edit.php?find_type=K",
						"tszh_ticket_dict_list.php?find_type=K"
					),
					"title" => GetMessage("SUP_M_CRITICALITY")
				),
				array(
					"text" => GetMessage("SUP_M_STATUS"),
					"url" => "tszh_ticket_dict_list.php?lang=" . LANGUAGE_ID . "&amp;find_type=S",
					"more_url" => Array(
						"tszh_ticket_dict_edit.php?find_type=S",
						"tszh_ticket_dict_list.php?find_type=S"
					),
					"title" => GetMessage("SUP_M_STATUS")
				),
				array(
					"text" => GetMessage("SUP_M_MARK"),
					"url" => "tszh_ticket_dict_list.php?lang=" . LANGUAGE_ID . "&amp;find_type=M",
					"more_url" => Array(
						"tszh_ticket_dict_edit.php?find_type=M",
						"tszh_ticket_dict_list.php?find_type=M"
					),
					"title" => GetMessage("SUP_M_MARK")
				),
				array(
					"text" => GetMessage("SUP_M_FUA"),
					"url" => "tszh_ticket_dict_list.php?lang=" . LANGUAGE_ID . "&amp;find_type=F",
					"more_url" => Array(
						"tszh_ticket_dict_edit.php?find_type=F",
						"tszh_ticket_dict_list.php?find_type=F"
					),
					"title" => GetMessage("SUP_M_FUA")
				),
				array(
					"text" => GetMessage("SUP_M_SOURCE"),
					"url" => "tszh_ticket_dict_list.php?lang=" . LANGUAGE_ID . "&amp;find_type=SR",
					"more_url" => Array(
						"tszh_ticket_dict_edit.php?find_type=SR",
						"tszh_ticket_dict_list.php?find_type=SR"
					),
					"title" => GetMessage("SUP_M_SOURCE")
				),

				array(
					"text" => GetMessage("SUP_M_DIFFICULTY"),
					"url" => "tszh_ticket_dict_list.php?lang=" . LANGUAGE_ID . "&amp;find_type=D",
					"more_url" => Array(
						"tszh_ticket_dict_edit.php?find_type=D",
						"tszh_ticket_dict_list.php?find_type=D"
					),
					"title" => GetMessage("SUP_M_DIFFICULTY_TITLE")
				),
			),
		);

		if ($SUP_RIGHT >= 'W')
		{
			$aMenu["items"][] = Array(
				"text" => GetMessage("CTT_EXPORT"),
				"url" => "tszh_ticket_export.php",
				"more_url" => array(),
				"title" => GetMessage("CTT_EXPORT_TITLE")
			);
		}

		if ($SUP_RIGHT >= 'W')
		{
			$aMenu["items"][] = Array(
				"text" => GetMessage("CTT_IMPORT"),
				"url" => "tszh_ticket_import_users.php",
				"more_url" => array(),
				"title" => GetMessage("CTT_IMPORT_TITLE")
			);
		}
	}
	return $aMenu;
}

return false;
?>
