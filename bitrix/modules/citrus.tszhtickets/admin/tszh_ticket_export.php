<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/tszh/classes/general/tickets_export.php");
//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/prolog.php");
IncludeModuleLangFile(__FILE__);

$bIsSupportInstalled = CModule::IncludeModule('citrus.tszhtickets');

tszhTicketsCheck();

// ������� ����� ������� �������� ������������ �� ������ ���������-������������� �������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszhtickets");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT < "W")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if(!isset($INTERVAL))
	$INTERVAL = 30;
else
	$INTERVAL = intval($INTERVAL);
if($INTERVAL <= 0)
	@set_time_limit(0);

$start_time = time();

$arErrors = array();
$arMessages = array();

$arStatuses = $arStatusSelected = Array();
$dbStatuses = CTszhTicketDictionary::GetList($by = 'sort', $order = 'asc', Array("TYPE" => "S"));
$bFirst = true;
while ($arStatus = $dbStatuses->GetNext()) {
	$arStatuses[$arStatus['ID']] = $arStatus['NAME'];
	if ($bFirst) {
		$bFirst = false;
	} else {
		$arStatusSelected[] = $arStatus['ID'];
	}
}

if($_REQUEST["Export"]=="Y" && (array_key_exists("NS", $_REQUEST) || array_key_exists("INTERVAL", $_REQUEST)))
{
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");
	if(array_key_exists("NS", $_REQUEST))
		$NS = unserialize($_REQUEST["NS"]);
	else {
		$NS = array(
			"STEP" => 0,
			"URL_DATA_FILE" => $_REQUEST["URL_DATA_FILE"],
			"ONLY_OPENED" => $_REQUEST['ONLY_OPENED'] ? true : false,
			"STATUSES" => is_array($_REQUEST['STATUSES']) ? $_REQUEST['STATUSES'] : false,
			"next_step" => array(),
		);
	}

	//We have to strongly check all about file names at server side
	$ABS_FILE_NAME = false;
	$WORK_DIR_NAME = false;
	if(isset($NS["URL_DATA_FILE"]) && (strlen($NS["URL_DATA_FILE"])>0))
	{
		$filename = trim(str_replace("\\", "/", trim($NS["URL_DATA_FILE"])), "/");
		if (preg_match('/[^a-zA-Z0-9\s!#\$%&\(\)\[\]\{\}+\.;=@\^_\~\/\\\\\-]/i', $filename))
		{
			$arErrors[] = GetMessage("TSZH_EXPORT_FILE_NAME_ERROR");
		}
		else
		{
			$FILE_NAME = rel2abs($_SERVER["DOCUMENT_ROOT"], "/".$filename);
			if((strlen($FILE_NAME) > 1) && ($FILE_NAME === "/".$filename))
			{
				$ABS_FILE_NAME = $_SERVER["DOCUMENT_ROOT"].$FILE_NAME;
				if (strtolower(substr($ABS_FILE_NAME, -4)) != ".xml")
					$ABS_FILE_NAME .= ".xml";
				$WORK_DIR_NAME = substr($ABS_FILE_NAME, 0, strrpos($ABS_FILE_NAME, "/")+1);
			}
		}
	} else {
		$arErrors[] = GetMessage("TSZH_EXPORT_NO_FILE");
	}

	if (!is_array($NS['STATUSES']) || count($NS['STATUSES']) <= 0) {
		$arErrors[] = GetMessage("TSZH_EXPORT_NO_STATUSES");
	}

	$fp = false;
	if(!check_bitrix_sessid())
	{
		$arErrors[] = GetMessage("TSZH_EXPORT_ACCESS_DENIED");
	}
	elseif($ABS_FILE_NAME && (count($arErrors) == 0))
	{
		if($NS["STEP"] < 1)
		{
			$_SESSION["BX_TSZH_REQUESTS_EXPORT"] = array(
				"work_dir" => false,
				"file_dir" => false,
			);
			if($fp = fopen($ABS_FILE_NAME, "wb"))
			{
/*				if(strtolower(substr($ABS_FILE_NAME, -4)) == ".xml")
				{
					if(@mkdir($DIR_NAME = substr($ABS_FILE_NAME, 0, -4)."_files", BX_DIR_PERMISSIONS));
					{
						$_SESSION["BX_TSZH_REQUESTS_EXPORT"]["work_dir"] = $WORK_DIR_NAME;
						$_SESSION["BX_TSZH_REQUESTS_EXPORT"]["file_dir"] = substr($DIR_NAME."/", strlen($WORK_DIR_NAME));
					}
				}*/
			}
			else
			{
				$arErrors[] = GetMessage("TSZH_EXPORT_FILE_ERROR");
			}
			$NS["STEP"]++;
		}
		elseif($NS["STEP"] < 4)
		{
			if($fp = fopen($ABS_FILE_NAME, "ab"))
			{
				$obExport = new CTszhTicketsExport;
				if($obExport->Init($fp, $NS["next_step"]))
				{
					if($NS["STEP"]==1)
					{
						$obExport->StartExport();
						$obExport->ExportDictionaries();
						$NS['STEP']++;
					}
					elseif($NS["STEP"]==2)
					{
						//$result = $obExport->ExportSections($_SESSION["BX_TSZH_REQUESTS_EXPORT"]["SECTION_MAP"], $start_time, $INTERVAL);
						$result = $obExport->ExportTickets($start_time, $INTERVAL, $NS);
						if($result)
						{
							$NS["TICKETS"] += $result;
						}
						else
						{
							$obExport->EndExport();
							$NS["STEP"]++;
						}
					} else
						$NS["STEP"]++;
					$NS["next_step"] = $obExport->next_step;
				}
				else
				{
					$arErrors[] = GetMessage("TSZH_EXPORT_IBLOCK_ERROR");
				}
			}
			else
			{
				$arErrors[] = GetMessage("TSZH_EXPORT_FILE_ERROR")."(1)";
			}
		}
	}
	elseif (count($arErrors) == 0)
	{
		$arErrors[] = GetMessage("TSZH_EXPORT_FILE_ERROR")."(2)";
	}

	if($fp)
		fclose($fp);

	foreach($arErrors as $strError)
		CAdminMessage::ShowMessage($strError);
	foreach($arMessages as $strMessage)
		CAdminMessage::ShowMessage(array("MESSAGE"=>$strMessage,"TYPE"=>"OK"));

	if(count($arErrors)==0):?>
<div class="notes">
<table cellspacing="0" cellpadding="0" border="0" class="notes">
	<tbody><tr class="top">
		<td class="left"><div class="empty"></div></td>
		<td><div class="empty"></div></td>
		<td class="right"><div class="empty"></div></td>
	</tr>
	<tr>
		<td class="left"><div class="empty"></div></td>
		<td class="content">
		<?if($NS["STEP"] < 3):?>
			<li><?
				echo GetMessage("TSZH_EXPORT");
			?></li>
			<li><?
				if($NS["STEP"] < 2)
					echo "<b>".GetMessage("TSZH_EXPORT_DICTIONARIES_PROGRESS")."</b>";
				else
					echo GetMessage("TSZH_EXPORT_DICTIONARIES_DONE", array("#COUNT#"=>intval($NS["REQUESTS"])));
			?></li>
			<li><?
				if($NS["STEP"] < 2)
					echo GetMessage("TSZH_EXPORT_REQUESTS");
				elseif($NS["STEP"] < 3)
					echo "<b>".GetMessage("TSZH_EXPORT_REQUESTS_PROGRESS", array("#COUNT#"=>intval($NS["TICKETS"])))."</b>";
				else
					echo GetMessage("TSZH_EXPORT_REQUESTS_PROGRESS", array("#COUNT#"=>intval($NS["TICKETS"])));
			?></li>
			<?if($NS["STEP"]>0):?>
				<input type="hidden" id="NS" name="NS" value="<?=htmlspecialchars(serialize($NS))?>" />
			<?endif?>
		<?else:?>
			<b><?echo GetMessage("TSZH_EXPORT_DONE")?></b><br>
			<?echo GetMessage("TSZH_EXPORT_DONE_REQUESTS", array("#COUNT#"=>intval($NS["TICKETS"])))?><br />
			<a href="<?=htmlspecialchars($NS['URL_DATA_FILE'])?>"><?=GetMessage("TSZH_EXPORT_DOWNLOAD_FILE")?></a>
		<?endif;?>
		</td>
		<td class="right"><div class="empty"></div></td>
	</tr>
	<tr class="bottom">
		<td class="left"><div class="empty"></div></td>
		<td><div class="empty"></div></td>
		<td class="right"><div class="empty"></div></td>
	</tr>
</tbody></table>
</div>
	<?endif;
	require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin_js.php");
}

$APPLICATION->SetTitle(GetMessage("TSZH_EXPORT_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

?>
<div id="export_result_div"></div>
<?
$aTabs = array(
	array(
		"DIV" => "edit1",
		"TAB" => GetMessage("TSZH_EXPORT_TAB"),
		"ICON" => "main_user_edit",
		"TITLE" => GetMessage("TSZH_EXPORT_TAB_TITLE"),
	),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);
?>
<script language="JavaScript" type="text/javascript">
<!--
var running = false;
function DoNext(bFirst)
{
	var queryString='Export=Y&lang=<?=LANG?>&<?echo bitrix_sessid_get()?>';
	if(bFirst)
	{
		queryString+='&URL_DATA_FILE='+jsUtils.urlencode(document.getElementById('URL_DATA_FILE').value);
		queryString+='&ONLY_OPENED='+(jsUtils.urlencode(document.getElementById('ONLY_OPENED').checked?'1':''));
		queryString+='&INTERVAL='+jsUtils.urlencode(document.getElementById('INTERVAL').value);


		var selObj = document.getElementById('STATUSES');
		for (i=0; i<selObj.options.length; i++) {
			if (selObj.options[i].selected) {
				queryString+='&STATUSES[]='+jsUtils.urlencode(selObj.options[i].value);
			}
		}

		//queryString+='&STATUSES[]='+jsUtils.urlencode(document.getElementById('STATUSES').value);
	}
	else
	{
		queryString+='&INTERVAL='+jsUtils.urlencode(document.getElementById('INTERVAL').value);
		queryString+='&NS='+jsUtils.urlencode(document.getElementById('NS').value);
	}

	CHttpRequest.Action = function(result)
	{
		CloseWaitWindow();
		document.getElementById('export_result_div').innerHTML = result;
		//Check if stop button was pressed
		if(running)
		{
			//Check if next step is needed
			if(document.getElementById('NS'))
				DoNext(false);
			else
				EndExport();
		}
	}
	ShowWaitWindow();
	CHttpRequest.Send('tszh_ticket_export.php?'+queryString);
}
function StartExport()
{
	running = true;
	document.getElementById('start_button').disabled=true;
	DoNext(true);
}
function EndExport()
{
	running = false;
	document.getElementById('start_button').disabled=false;
}
//-->
</script>
<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?lang=<?echo htmlspecialchars(LANG)?>" name="form1" id="form1" enctype="multipart/form-data">
<?
$tabControl->Begin();
$tabControl->BeginNextTab();
?>
	<tr valign="top">
		<td width="40%"><?echo GetMessage("TSZH_EXPORT_URL_DATA_FILE")?>:</td>
		<td width="60%">
			<input type="text" id="URL_DATA_FILE" name="URL_DATA_FILE" size="30" value="<?=htmlspecialchars($URL_DATA_FILE)?>" />
			<input type="button" value="<?echo GetMessage("TSZH_EXPORT_OPEN")?>" onclick="BtnClick()" />
			<?
			CAdminFileDialog::ShowScript
			(
				Array(
					"event" => "BtnClick",
					"arResultDest" => array("FORM_NAME" => "form1", "FORM_ELEMENT_NAME" => "URL_DATA_FILE"),
					"arPath" => array("SITE" => SITE_ID, "PATH" =>"/upload"),
					"select" => 'F',// F - file only, D - folder only
					"operation" => 'S',// O - open, S - save
					"showUploadTab" => true,
					"showAddToMenuTab" => false,
					"fileFilter" => 'xml',
					"allowAllFiles" => true,
					"SaveConfig" => true,
				)
			);
			?>
		</td>
	</tr>
	<tr valign="top">
		<td><label for="ONLY_OPENED"><?echo GetMessage("TSZH_EXPORT_ONLY_OPENED")?></label>:</td>
		<td><input type="checkbox" id="ONLY_OPENED" name="ONLY_OPENED" value="1" />	</td>
	</tr>
	<tr valign="top">
		<td><label for="STATUSES"><?echo GetMessage("TSZH_EXPORT_STATUSES")?></label>:</td>
		<td>
			<select name="STATUSES[]" id="STATUSES" multiple="multiple" size="5">
<?
				foreach ($arStatuses as $statusID=>$statusName) {
					?><option value="<?=$statusID?>"<?=(array_search($statusID, $arStatusSelected) !== false ? ' selected="selected"' : '')?>><?=$statusName?></option><?
				}
?>
			</select>
		</td>
	</tr>
	<tr valign="top">
		<td><?echo GetMessage("TSZH_EXPORT_INTERVAL")?>:</td>
		<td>
			<input type="text" id="INTERVAL" name="INTERVAL" size="5" value="<?echo intval($INTERVAL)?>">
		</td>
	</tr>
<?$tabControl->Buttons();?>
	<input type="button" id="start_button" value="<?echo GetMessage("TSZH_EXPORT_START_EXPORT")?>" onclick="StartExport();" />
	<input type="button" id="stop_button" value="<?echo GetMessage("TSZH_EXPORT_STOP_EXPORT")?>" onclick="EndExport();" />
<?$tabControl->End();?>
</form>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>
