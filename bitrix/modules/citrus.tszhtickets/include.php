<?

if (!CModule::IncludeModule("citrus.tszh"))
{
	return false;
}

define('TSZH_TICKETS', 'citrus.tszhtickets');

IncludeModuleLangFile(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhtickets/errors.php");

CModule::AddAutoloadClasses(
	"citrus.tszhtickets",
	array(
		"CAllTszhTicket" => "classes/general/CAllTszhTicket.php",
		"CAllTszhTicketDictionary" => "classes/general/CAllTszhTicketDictionary.php",
		"CAllTszhTicketReminder" => "classes/general/CAllTszhTicketReminder.php",
		"CTszhSupportEMail" => "classes/general/CTszhSupportEMail.php",
		"CTszhSupportUserGroup" => "classes/general/CTszhSupportUserGroup.php",
		"CTszhSupportUser2UserGroup" => "classes/general/CTszhSupportUser2UserGroup.php",

		"CTszhTicketsExport" => "classes/general/CTszhTicketsExport.php",

		"CTszhTicket" => "classes/mysql/CTszhTicket.php",
		"CTszhTicketDictionary" => "classes/mysql/CTszhTicketDictionary.php",
		"CTszhTicketReminder" => "classes/mysql/CTszhTicketReminder.php",
	)
);

function tszhTicketsCheck()
{
	global $demoNotice;

	ob_start();
	if (!CModule::IncludeModule('citrus.tszh'))
	{
		ob_end_clean();
		require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
		echo CAdminMessage::ShowMessage(
			Array(
				"DETAILS" => GetMessage("SUP_ERROR_TSZH_MODULE_NOT_FOUND"),
				"TYPE" => "MESSAGE",
				"MESSAGE" => GetMessage("SUP_ERROR"),
				"HTML" => false,
			));
		require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
		die();
	}
	elseif (!CTszhFunctionalityController::CheckEdition())
	{
		$c = ob_get_contents();
		ob_end_clean();
		require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
		echo $c;
		require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
		die();
	}
	$demoNotice = ob_get_contents();
	ob_end_clean();

}

?>