<?
$MESS["SUP_SE_TICKET_NEW_FOR_AUTHOR_TITLE"] = "Нове звернення (для автора)";
$MESS["SUP_SE_TICKET_NEW_FOR_AUTHOR_TEXT"] = "
#ID# - ID звернення
#NUMBER# - Номер звернення
#LANGUAGE_ID# - ідентифікатор мови сайту, до якого прив'язане повідомлення
#DATE_CREATE# - дата створення
#TIMESTAMP# - дата зміни
#DATE_CLOSE# - дата закриття
#TITLE# - заголовок звернення
#CATEGORY# - категорія звернення
#STATUS# - статус звернення
#CRITICALITY# - критичність звернення
#SOURCE# - джерело звернення (web, email, телефон, тощо)
#SPAM_MARK# - відмітка про спам
#MESSAGE_BODY# - текст повідомлення
#FILES_LINKS# - посилання на прикріплені файли
#ADMIN_EDIT_URL# - посилання для зміни звернення (до адміністративної частини)
#PUBLIC_EDIT_URL# - посилання для зміни звернення (до публічної частини)

#OWNER_EMAIL# - #OWNER_USER_EMAIL# і/або #OWNER_SID#
#OWNER_USER_ID# - ID автора звернення
#OWNER_USER_NAME# - ім'я автора звернення
#OWNER_USER_LOGIN# - логін автора звернення
#OWNER_USER_EMAIL# - e-mail автора звернення
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - довільний ідентифікатор автора звернення (e-mail, телефон, тощо)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# або #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_ID# - ID відповідального за звернення
#RESPONSIBLE_USER_NAME# - ім'я відповідальний за звернення
#RESPONSIBLE_USER_LOGIN# - логін відповідального за звернення
#RESPONSIBLE_USER_EMAIL# - e-mail відповідального за звернення
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - E-Mail всіх адміністраторів аварійно-диспетчерскої служби

#CREATED_USER_ID# - ID творця звернення
#CREATED_USER_LOGIN# - логін творця звернення
#CREATED_USER_EMAIL# - e-mail творця зверненя
#CREATED_USER_NAME# - ім'я творця звернення
#CREATED_MODULE_NAME# - ідентифікатор модуля, засобами якого було створено звернення
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME#

#SUPPORT_COMMENTS# - адміністративний коментар

";
$MESS["SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_TITLE"] = "Нове звернення (для аварійно-диспетчерської служби)";
$MESS["SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_TEXT"] = "
#ID# - ID звернення
#NUMBER# - Номер звернення
#LANGUAGE_ID# - ідентифікатор мови сайту, до якого прив'язане повідомлення
#DATE_CREATE# - дата створення
#TIMESTAMP# - дата зміни
#DATE_CLOSE# - дата закриття
#TITLE# - заголовок звернення
#CATEGORY# - категорія звернення
#STATUS# - статус звернення
#CRITICALITY# - критичність звернення
#SOURCE# - джерело звернення (web, email, телефон, тощо)
#SPAM_MARK# - відмітка про спам
#MESSAGE_BODY# - текст повідомлення
#FILES_LINKS# - посилання на прикріплені файли
#ADMIN_EDIT_URL# - посилання для зміни звернення (до адміністративної частини)
#PUBLIC_EDIT_URL# - посилання для зміни звернення (до публічної частини)

#OWNER_EMAIL# - #OWNER_USER_EMAIL# і/або #OWNER_SID#
#OWNER_USER_ID# - ID автора звернення
#OWNER_USER_NAME# - ім'я автора звернення
#OWNER_USER_LOGIN# - логін автора звернення
#OWNER_USER_EMAIL# - e-mail автора звернення
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - довільний ідентифікатор автора звернення (e-mail, телефон, тощо)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# або #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_ID# - ID відповідального за звернення
#RESPONSIBLE_USER_NAME# - ім'я відповідальний за звернення
#RESPONSIBLE_USER_LOGIN# - логін відповідального за звернення
#RESPONSIBLE_USER_EMAIL# - e-mail відповідального за звернення
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - E-Mail всіх адміністраторів аварійно-диспетчерскої служби

#CREATED_USER_ID# - ID творця звернення
#CREATED_USER_LOGIN# - логін творця звернення
#CREATED_USER_EMAIL# - e-mail творця зверненя
#CREATED_USER_NAME# - ім'я творця звернення
#CREATED_MODULE_NAME# - ідентифікатор модуля, засобами якого було створено звернення
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME#

#SUPPORT_COMMENTS# - адміністративний коментар

";
$MESS["SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_TITLE"] = "Звернення змінено співробітником аварійно-диспетчерської служби (для автора) ";
$MESS["SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_TEXT"] = "

#ID# - ID звернення
#NUMBER# - Номер звернення
#LANGUAGE_ID# - ідентифікатор мови сайту, до якого прив'язане повідомлення
#DATE_CREATE# - дата створення
#TIMESTAMP# - дата зміни
#DATE_CLOSE# - дата закриття
#TITLE# - заголовок звернення
#CATEGORY# - категорія звернення
#STATUS# - статус звернення
#CRITICALITY# - критичність звернення
#SOURCE# - джерело звернення (web, email, телефон, тощо)
#SPAM_MARK# - відмітка про спам
#MESSAGE_BODY# - текст повідомлення
#FILES_LINKS# - посилання на прикріплені файли
#ADMIN_EDIT_URL# - посилання для зміни звернення (до адміністративної частини)
#PUBLIC_EDIT_URL# - посилання для зміни звернення (до публічної частини)

#OWNER_EMAIL# - #OWNER_USER_EMAIL# і/або #OWNER_SID#
#OWNER_USER_ID# - ID автора звернення
#OWNER_USER_NAME# - ім'я автора звернення
#OWNER_USER_LOGIN# - логін автора звернення
#OWNER_USER_EMAIL# - e-mail автора звернення
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - довільний ідентифікатор автора звернення (e-mail, телефон, тощо)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# або #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_ID# - ID відповідального за звернення
#RESPONSIBLE_USER_NAME# - ім'я відповідальний за звернення
#RESPONSIBLE_USER_LOGIN# - логін відповідального за звернення
#RESPONSIBLE_USER_EMAIL# - e-mail відповідального за звернення
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - E-Mail всіх адміністраторів аварійно-диспетчерскої служби

#CREATED_USER_ID# - ID творця звернення
#CREATED_USER_LOGIN# - логін творця звернення
#CREATED_USER_EMAIL# - e-mail творця зверненя
#CREATED_USER_NAME# - ім'я творця звернення
#CREATED_MODULE_NAME# - ідентифікатор модуля, засобами якого було створено звернення
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME#

#MODIFIED_USER_ID# - ID людини, яка змінила звернення
#MODIFIED_USER_LOGIN# - логін людини, яка змінила звернення
#MODIFIED_USER_EMAIL# - EMail людини, яка змінила звернення
#MODIFIED_USER_NAME# - ім'я людини, яка змінила звернення
#MODIFIED_MODULE_NAME# - ідентифікатор модуля засобами якого було змінено звернення
#MODIFIED_TEXT# - [#MODIFIED_USER_ID#] (#MODIFIED_USER_LOGIN#) #MODIFIED_USER_NAME#

#MESSAGE_AUTHOR_USER_ID# - ID автора повідомлення
#MESSAGE_AUTHOR_USER_NAME# - ім'я автора повідомлення
#MESSAGE_AUTHOR_USER_LOGIN# - логін автора повідомлення
#MESSAGE_AUTHOR_USER_EMAIL# - email автора повідомлення
#MESSAGE_AUTHOR_TEXT# - [#MESSAGE_AUTHOR_USER_ID#] (#MESSAGE_AUTHOR_USER_LOGIN#) #MESSAGE_AUTHOR_USER_NAME#
#MESSAGE_AUTHOR_SID# - джовільний ідентифікатор автора повідомлення (email, телефон, тощо)
#MESSAGE_SOURCE# - джерело повідомлення
#MESSAGE_BODY# - текст повідомлення
#FILES_LINKS# - посилання на прикріплені файли

#SUPPORT_COMMENTS# - адміністративний коментар

";
$MESS["SUP_SE_TICKET_CHANGE_BY_AUTHOR_FOR_AUTHOR_TITLE"] = "#MODIFIED_USER_ID# - ID людини, яка змінила звернення";
$MESS["SUP_SE_TICKET_CHANGE_BY_AUTHOR_FOR_AUTHOR_TEXT"] = "#MODIFIED_USER_LOGIN# - логін людини, яка змінила повідомлення";
$MESS["SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_TITLE"] = "#MODIFIED_USER_EMAIL# - EMail людини, яка змінила повідомлення";
$MESS["SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_TEXT"] = "#MODIFIED_USER_NAME# - ім'я людини, яка змінила повідомлення";
$MESS["SUP_SE_TICKET_OVERDUE_REMINDER_TITLE"] = "#MODIFIED_MODULE_NAME# - ідентифікатор модуля засобами якого було змінено звернення";
$MESS["SUP_SE_TICKET_OVERDUE_REMINDER_TEXT"] = "#MODIFIED_TEXT# - [#MODIFIED_USER_ID#] (#MODIFIED_USER_LOGIN#) #MODIFIED_USER_NAME#";
$MESS["SUP_SE_TICKET_NEW_FOR_AUTHOR_MESSAGE"] = "#MESSAGE_AUTHOR_USER_ID# - ID автора повідомлення";
$MESS["SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_SUBJECT"] = "#MESSAGE_AUTHOR_USER_NAME# - ім'я автора повідомлення";
$MESS["SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_MESSAGE"] = "#MESSAGE_AUTHOR_USER_LOGIN# - логін автора повідомлення";
$MESS["SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_SUBJECT"] = "#MESSAGE_AUTHOR_USER_EMAIL# - email автора повідомлення";
$MESS["SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_MESSAGE"] = "#MESSAGE_AUTHOR_TEXT# - [#MESSAGE_AUTHOR_USER_I";
$MESS["SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_SUBJECT"] = "[##NUMBER#] #SERVER_NAME#: Зміни в повідомлені";
$MESS["SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_MESSAGE"] = "Зміни у зверненні # #NUMBER# до аварійно-диспетчерскої служби #SERVER_NAME#.
#SPAM_MARK#
#WHAT_CHANGE#
Тема: #TITLE#

Від кого: #MESSAGE_SOURCE##MESSAGE_AUTHOR_SID##MESSAGE_AUTHOR_TEXT#

>#MESSAGE_HEADER##FILES_LINKS##MESSAGE_BODY#
>#MESSAGE_FOOTER#

Автор    - #SOURCE##OWNER_SID##OWNER_TEXT#
Створено  - #CREATED_TEXT##CREATED_MODULE_NAME# [#DATE_CREATE#]
Змінено - #MODIFIED_TEXT##MODIFIED_MODULE_NAME# [#TIMESTAMP#]

Відповідальний     - #RESPONSIBLE_TEXT#
Категорія         - #CATEGORY#
Критичність       - #CRITICALITY#
Статус            - #STATUS#
Оцінка відповідей    - #RATE#

>====================== КОММЕНТАР ==================================#SUPPORT_COMMENTS#
>=====================================================================

Для перегляду і редагування звернення скористайтесь посиланням:
http://#SERVER_NAME##ADMIN_EDIT_URL#?ID=#ID#&lang=#LANGUAGE_ID#

Лист згенеровано автоматично.
";
$MESS["SUP_SE_TICKET_OVERDUE_REMINDER_SUBJECT"] = "[##NUMBER#] #SERVER_NAME#: Нагадування про необхідність відповіді";
$MESS["SUP_SE_TICKET_OVERDUE_REMINDER_MESSAGE"] = "Нагадування про необхідність відповіді у звернені # #NUMBER# до аварійно-диспетчерскої служби #SERVER_NAME#.

Коли буде прострочене - #EXPIRATION_DATE# (залишилось: #REMAINED_TIME#)

>================= ІНФОРМАЦІЯ ПРО ЗВЕРНЕННЯ ===========================

Тема    - #TITLE#

Автор   - #SOURCE##OWNER_SID##OWNER_TEXT#
Створено - #CREATED_TEXT##CREATED_MODULE_NAME# [#DATE_CREATE#]

Відповідальний     - #RESPONSIBLE_TEXT#
Категорія         - #CATEGORY#
Критичність       - #CRITICALITY#
Статус            - #STATUS#
Оцінка відповідей    - #RATE#

>================ ПОВІДОМЛЕННЯ, ЯКЕ ПОТРЕБУЄ ВІДПОВІДІ =========================
#MESSAGE_BODY#
>=====================================================================

Для перегляду і редагування звернення скористайтесь посиланням:
http://#SERVER_NAME##ADMIN_EDIT_URL#?ID=#ID#&lang=#LANGUAGE_ID#

Лист згенеровано автоматично.
";
?>