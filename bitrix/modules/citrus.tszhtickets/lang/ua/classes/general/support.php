<?
$MESS["SUP_ADD_MESSAGE_TO_TECHSUPPORT"] = "додавання повідомлення в аварійно-диспетчерську службу ";
$MESS["SUP_FORGOT_NAME"] = "Ви забули ввести поле \"Найменування\" ";
$MESS["SUP_INCORRECT_SID"] = "Невірний символьний код (допустимі тільки латинські букви, цифри, або символ \"_\") ";
$MESS["SUP_SID_ALREADY_IN_USE"] = "Для типу довідника \"# TYPE #\" і мови \"# LANG #\" даний мнемонічний код вже використовується в записі # # RECORD_ID #. ";
$MESS["SUP_UNKNOWN_ID"] = "Не існує запису з кодом # ID # ";
$MESS["SUP_ERROR_ADD_DICTONARY"] = "Помилка додавання запису. ";
$MESS["SUP_ERROR_UPDATE_DICTONARY"] = "Помилка оновлення запису. ";
$MESS["SUP_ERROR_GROUP_NOT_FOUND"] = "Група з таким ID не знайдена.";
$MESS["SUP_ERROR_GROUP_NAME_EMPTY"] = "Порожнє ім'я групи. ";
$MESS["SUP_ERROR_USER_ID_EMPTY"] = "Порожній ID користувача. ";
$MESS["SUP_ERROR_GROUP_ID_EMPTY"] = "Порожній ID группи.";
$MESS["SUP_ERROR_USERGROUP_EXISTS"] = "Цей користувач вже перебуває у цій групі. ";
$MESS["SUP_ERROR_NO_SUPPORT_USER"] = "Цей користувач не має відношення до аварійно-диспетчерській службі. ";
$MESS["SUP_ERROR_USER_NO_TEAM"] = "У цій групі можуть складатися тільки співробітники аварійно-диспетчерської служби. ";
$MESS["SUP_ERROR_USER_NO_CLIENT"] = "У цій групі можуть перебувати тільки клієнти аварійно-диспетчерської служби. ";
$MESS["SUP_ERROR_NO_GROUP"] = "Немає такої групи. ";
$MESS["SUP_ERROR_NO_USER"] = "Немає такого користувача. ";
$MESS["SUP_ERROR_EMPTY_TITLE"] = "Ви забули ввести поле \"Заголовок\" ";
$MESS["SUP_ERROR_EMPTY_MESSAGE"] = "Ви забули ввести поле \"Повідомлення\" ";
?>