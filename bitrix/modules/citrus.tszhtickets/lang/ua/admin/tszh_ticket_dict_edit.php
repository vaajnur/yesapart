<?
$MESS["SUP_FORGOT_NAME"] = "Ви забули ввести поле \"Найменування\" ";
$MESS["SUP_EDIT_RECORD"] = "Редагувати записи # #ID#";
$MESS["SUP_NEW_RECORD"] = "Новий запис довідника";
$MESS["SUP_RECORD"] = "Запис";
$MESS["SUP_RECORD_TITLE"] = "Параметри запису в довідникі";
$MESS["SUP_STAT"] = "Статистика";
$MESS["SUP_STAT_TITLE"] = "Реєстрація в статистиці";
$MESS["SUP_RECORDS_LIST"] = "Довідник";
$MESS["SUP_SITE"] = "Сайти:";
$MESS["SUP_ERROR"] = "Помилка при збереженні запису";
$MESS["SUP_TYPE"] = "Тип:";
$MESS["SUP_SORT"] = "Порядок сортування: ";
$MESS["SUP_NAME"] = "Заголовок:";
$MESS["SUP_DESCRIPTION"] = "Опис:";
$MESS["SUP_RESPONSIBLE"] = "Відповідальний:";
$MESS["SUP_EVENT_PARAMS"] = "Як реєструвати нові звернення в аварійно-диспетчерську службу в модулі статистики ";
$MESS["SUP_EVENT12"] = "event1, event2 — параметри, які ідентифікують тип події ";
$MESS["SUP_EVENT3"] = "event3 — додатковий параметр";
$MESS["SUP_INCORRECT_SID"] = "Невірний символьний код (допустимі тільки латинські букви, цифри, або символ \"_\") ";
$MESS["SUP_SID"] = "Символьний код:";
$MESS["SUP_SID_ALREADY_IN_USE"] = "Для типу довідника \"# TYPE #\" і мови \"# LANG #\" даний мнемонічний код вже використовується в записі # # RECORD_ID #. ";
$MESS["SUP_BY_DEFAULT"] = "При створенні нового звернення на сайті, вибирати за замовчуванням у спадному списку: ";
$MESS["SUP_TIME_TO_SOLVE"] = "Строк виконання:";
$MESS["SUP_CREATE_NEW_RECORD"] = "Створити новий запис";
$MESS["SUP_DELETE_RECORD"] = "Видалити запис";
$MESS["SUP_DELETE_RECORD_CONFIRM"] = "Ви дійсно бажаєте видалити цей запис довідника?";
?>