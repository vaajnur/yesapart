<?
$MESS["SUP_GE_TITLE_EDIT"] = "Редагування группи \"%GROUP_NAME%\"";
$MESS["SUP_GE_TITLE_NEW"] = "Додавання нової групи";
$MESS["SUP_GE_GROUP"] = "Група";
$MESS["SUP_GE_GROUP_TITLE"] = "Параметри групи";
$MESS["SUP_GE_NAME"] = "Назва групи";
$MESS["SUP_GE_SORT"] = "Сортування";
$MESS["SUP_GE_XML_ID"] = "XML_ID";
$MESS["SUP_GE_ERROR"] = "Помилка";
$MESS["SUP_GE_GROUPS_LIST"] = "Список груп";
$MESS["SUP_GE_USERGROUPS_LIST"] = "Список користувачів групи";
$MESS["SUP_GE_USERGROUPS_ADD"] = "Додати користувача до групи";
$MESS["SUP_GE_IS_TEAM_GROUP"] = "Група співробітників аврійно-диспетчерської служби";
$MESS["SUP_GE_GROUP_USERS"] = "Користувачі";
$MESS["SUP_GE_GROUP_USERS_TITLE"] = "Користувачі, які входять до групи";
$MESS["SUP_GE_ADD_MORE_USERS"] = "Додавати";
$MESS["SUP_GE_USER"] = "Користувач";
$MESS["SUP_GE_CAN_VIEW"] = "Може бачити повідомлення групи ";
?>