<?
$MESS["SUP_GL_TITLE"] = "Групи користувачів аварійно-диспетчерської служби ";
$MESS["SUP_GL_FLT_NAME"] = "Назва";
$MESS["SUP_GL_PAGES"] = "Групи";
$MESS["SUP_GL_NAME"] = "Назва";
$MESS["SUP_GL_SORT"] = "Сортування ";
$MESS["SUP_GL_XML_ID"] = "XML_ID";
$MESS["SUP_GL_EDIT"] = "Редагувати";
$MESS["SUP_GL_DELETE_CONFIRMATION"] = "Видалити групу?";
$MESS["SUP_GL_DELETE"] = "Видалити";
$MESS["SUP_GL_ADD"] = "Додати групу";
$MESS["SUP_GL_EXACT_MATCH"] = "Шукати точний збіг";
$MESS["SUP_GL_USERLIST"] = "Список користувачів групи";
$MESS["SUP_GL_USERADD"] = "Додати користувача до групи";
$MESS["SUP_GL_IS_TEAM_GROUP"] = "Група співробітників аврійно-диспетчерської служби";
$MESS["SUP_GL_FLT_IS_TEAM_GROUP"] = "Категорія";
$MESS["SUP_GL_FLT_IS_TEAM_GROUP_CN"] = "Категорія:";
$MESS["SUP_GL_FLT_SUPPORT"] = "Співробітники аварійно-диспетчерської служби";
$MESS["SUP_GL_FLT_CLIENT"] = "Клієнти аварійно-диспетчерської служби ";
?>