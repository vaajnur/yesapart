<?
$MESS["SUP_F_PERIOD"] = "Період";
$MESS["SUP_PAGE_TITLE"] = "Робочий стол";
$MESS["SUP_WRONG_DATE_FROM"] = "Введіть у фільтрі правильний “Період” “з”";
$MESS["SUP_WRONG_DATE_TILL"] = "Введіть у фільтрі правильний “Період” “по”";
$MESS["SUP_FROM_TILL_DATE"] = "У фільтрі \"Період\" \"по\" повинен бути більше ніж \"з\" ";
$MESS["SUP_ALL_TICKET"] = "сві звернення";
$MESS["SUP_OPEN_TICKET"] = "відкриті звернення";
$MESS["SUP_CLOSE_TICKET"] = "закриті звернення";
$MESS["SUP_OPEN"] = "Відкрито";
$MESS["SUP_OPEN_TOTAL"] = "Відкрито всього";
$MESS["SUP_CLOSE"] = "Закрито";
$MESS["SUP_TOTAL"] = "Всього";
$MESS["SUP_TOTAL_TICKETS"] = "Всього звернень";
$MESS["SUP_STATUS"] = "Статус";
$MESS["SUP_SERVER_TIME"] = "Час на сервері:";
$MESS["SUP_MARK"] = "Оцінка відповідей";
$MESS["SUP_CRITICALITY"] = "Критичність";
$MESS["SUP_DIFFICULTY"] = "Рівень складності";
$MESS["SUP_CATEGORY"] = "Категорія";
$MESS["SUP_SOURCE"] = "Джерело";
$MESS["SUP_MESSAGES"] = "Повідомлення";
$MESS["SUP_RESPONSIBLE"] = "Відповідальний";
$MESS["SUP_F_RESPONSIBLE"] = "Відповідальний";
$MESS["SUP_TICKETS1"] = "Звернень";
$MESS["SUP_MESSAGES1"] = "Повідомлень";
$MESS["SUP_FROM_OPEN_MESSAGES"] = "від кількості всіх повідомлень у відкритих зверненнях";
$MESS["SUP_FROM_CLOSE_MESSAGES"] = "від кількості всіх повідомлень у закритих зверненнях";
$MESS["SUP_FROM_ALL_MESSAGES"] = "від кількості всіх повідомлень";
$MESS["SUP_FROM_OPEN_TICKETS"] = "від кількості відкритих повідомлень";
$MESS["SUP_FROM_CLOSE_TICKETS"] = "від кількості закритих повідомлень";
$MESS["SUP_FROM_ALL_TICKETS"] = "від кількості всіх звернень";
$MESS["SUP_F_SHOW_MESSAGES"] = "Показати повідомлення <br> (в тому числі і прострочені) ";
$MESS["SUP_F_SHOW_MESSAGES_S"] = "Показати повідомлення";
$MESS["SUP_GREEN_ALT"] = "останній раз у зверненні писав співробітник аварійно-диспетчерської служби";
$MESS["SUP_RED_ALT"] = "останній раз у зверненні писав клієнт аварійно-диспетчерської служби";
$MESS["SUP_GREY_ALT"] = "звернення закрито";
$MESS["SUP_F_SITE"] = "Сайти";
$MESS["SUP_OVERDUE_MESSAGES"] = "Прострочені повідомлення";
?>