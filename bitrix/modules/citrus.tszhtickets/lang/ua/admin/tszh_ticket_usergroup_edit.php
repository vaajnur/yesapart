<?
$MESS["SUP_UGE_TITLE_ADD"] = "Додавання користувачів до групи";
$MESS["SUP_UGE_TAB1"] = "Користувач";
$MESS["SUP_UGE_TAB1_TITLE"] = "Користувач в групі";
$MESS["SUP_UGE_GROUP"] = "Група";
$MESS["SUP_UGE_USER"] = "Користувач";
$MESS["SUP_UGE_USER_CAN_READ_GROUP_MESS"] = "Користувач може читати звернення групи";
$MESS["SUP_GE_ERROR"] = "Помилка";
$MESS["SUP_UGE_NO_GROUP"] = "Групу не знайдено";
$MESS["SUP_UGE_ADD_MORE_USERS"] = "Додати";
?>