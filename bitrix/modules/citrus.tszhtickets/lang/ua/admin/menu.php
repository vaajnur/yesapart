<?
$MESS["SUP_CIT_SUPPORT"] = "ТВЖ: Аварійно-диспетчерська служба";
$MESS["SUP_M_TICKETS"] = "Звернення";
$MESS["SUP_M_TICKETS_ALT"] = "звернення відвідувачів до адміністрації сайту";
$MESS["SUP_M_REPORT_TABLE"] = "Робочий стол";
$MESS["SUP_M_REPORT_TABLE_ALT"] = "Таблиці розподілу відкритих і закритих звернень за різними параметрами ";
$MESS["SUP_M_REPORT_GRAPH"] = "Графіки";
$MESS["SUP_M_REPORT_GRAPH_ALT"] = "Звіт про роботу техпідтримки у вигляді графіку";
$MESS["SUP_M_CATEGORY"] = "Типи заявок";
$MESS["SUP_M_CRITICALITY"] = "Критичності";
$MESS["SUP_M_STATUS"] = "Статуси";
$MESS["SUP_M_MARK"] = "Оцінки відповідей";
$MESS["SUP_M_FUA"] = "Поширені відповіді ";
$MESS["SUP_M_SOURCE"] = "Джерела";
$MESS["SUP_M_DIFFICULTY"] = "Складність";
$MESS["SUP_M_DICT"] = "Довідники";
$MESS["SUP_CIT_SUPPORT_TITLE"] = "Управління аварійно-диспетчерської служби";
$MESS["SUP_M_DICT_TITLE"] = "Налаштування довідників ";
$MESS["SUP_M_DIFFICULTY_TITLE"] = "Рівень складності";
$MESS["SUP_M_GROUPS"] = "Групи";
$MESS["SUP_M_GROUPS_TITLE"] = "Редагування груп";
$MESS["SUP_M_USER_IN_GROUPS"] = "Користувачі в групах";
$MESS["SUP_M_USER_IN_GROUPS_TITLE"] = "Список користувачів у групах";
$MESS["CTT_EXPORT"] = "Експорт звернень";
$MESS["CTT_EXPORT_TITLE"] = "Експорт звернень";
$MESS["CTT_IMPORT"] = "Імпорт підрядних організацій";
$MESS["CTT_IMPORT_TITLE"] = "Імпорт підрядних організацій";
?>