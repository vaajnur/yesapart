<?
$MESS["SUP_ERROR_ACCESS_DENIED"] = "Доступ заборонений. ";
$MESS["SUP_FILTER_ERROR"] = "Помилка в фільтрі. ";
$MESS["SUP_ERROR_ATTACH_NOT_FOUND"] = "Файл не знайдено.";
$MESS["SUP_ERROR_INCORRECT_DATE_MODIFY_1"] = "Невірна дата модифікації \"з\". ";
$MESS["SUP_ERROR_INCORRECT_DATE_MODIFY_2"] = "Невірна дата модифікації \"по\". ";
$MESS["SUP_ERROR_INCORRECT_DATE_CREATE_1"] = "Невірна дата створення \"з\". ";
$MESS["SUP_ERROR_INCORRECT_DATE_CREATE_2"] = "Невірна дата створення \"по\". ";
$MESS["SUP_ERROR_INCORRECT_DATE_CREATE"] = "Невірна дата створення. ";
$MESS["SUP_ERROR_INCORRECT_DATE_MODIFY"] = "Невірна дата модифікації.";
$MESS["SUP_ERROR_INCORRECT_EMAIL"] = "Невірний EMail.";
$MESS["SUP_ERROR_REQUIRED_NAME"] = "Не вказана назва";
$MESS["SUP_ERROR_DELETE"] = "Помилка видалення.";
$MESS["SUP_ERROR_SAVE"] = "Помилка зміни запису ##ID#";
$MESS["SUP_ERROR_TSZH_MODULE_NOT_FOUND"] = "Модуль 1С: Сайт ЖКГ не встановлений.";
?>