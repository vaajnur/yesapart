<?
$MESS["CITRUS_TSZH_TICKETS_MODULE_NOT_FOUND"] = "Модуль «Аварийно-диспетчерская служба» не установлен.<br />";
$MESS["CITRUS_TSZH_TICKETS_GROUP_NOT_FOUND"] = "Группа с подрядными организациями не обнаружена.<br />";
$MESS["CITRUS_TSZH_TICKETS_SPECIFIED_GROUP_NOT_FOUND"] = "Указанная группа #GROUP_ID# не найдена.<br />";
$MESS["CITRUS_TSZH_TICKETS_SPECIFY_FILE"] = "Пожалуйста, укажите XML-файл";
$MESS["CITRUS_TSZH_TICKETS_USER_ADMIN_NOTES"] = "Пользователь импортирован из прикладного ПО, пароль сгенерирован автоматически.\n\nПароль — #PASSWORD#\n";
$MESS["CITRUS_TSZH_TICKETS_XML_PARSE_ERROR"] = "Ошибка при разборе XML-файла: ";
$MESS["CITRUS_TSZH_TICKETS_IMPORT_TITLE"] = "Импорт подрядных организаций";
$MESS["CITRUS_TSZH_TICKETS_DATA_FILE"] = "Файл данных";
$MESS["CITRUS_TSZH_TICKETS_IMPORT"] = "Импорт";
$MESS["CITRUS_TSZH_TICKETS_UPDATED_USERS"] = "Обновлено пользователей: <strong>#CNT#</strong><br />";
$MESS["CITRUS_TSZH_TICKETS_IMPORTED_USERS"] = "Импортировано новых пользователей: <strong>#CNT#</strong><br />";
$MESS["CITRUS_TSZH_TICKETS_DEACTIVATED"] = "Деактивировано";
$MESS["CITRUS_TSZH_TICKETS_DELETED"] = "Удалено";
$MESS["CITRUS_TSZH_TICKETS_USERS_COUNT"] = " пользователей: <strong>#CNT#<br />";
$MESS["CITRUS_TSZH_TICKETS_BTN_NEXT"] = "Далее &gt;&gt;";
$MESS["CITRUS_TSZH_TICKETS_BTN_FINISH"] = "Завершить";
$MESS["CITRUS_TSZH_TICKETS_XML_FILE"] = "XML-файл со списком подрядных организаций";
$MESS["CITRUS_TSZH_TICKETS_USERS_GROUP"] = "Группа подрядных организаций на сайте";
$MESS["CITRUS_TSZH_TICKETS_MISSING_ACTION"] = "Что делать с подрядными организациями, которых нет в импортируемом файле";
$MESS["CITRUS_TSZH_TICKETS_NOTHING"] = "ничего";
$MESS["CITRUS_TSZH_TICKETS_DEACTIVATE"] = "деактивировать";
$MESS["CITRUS_TSZH_TICKETS_DELETE"] = "удалить";
$MESS["CITRUS_TSZH_TICKETS_XML_FILE_TITLE"] = "XML-файл";

?>