<?
$MESS ['SUP_SE_TICKET_NEW_FOR_AUTHOR_TITLE'] = "Новое обращение (для автора)";
$MESS ['SUP_SE_TICKET_NEW_FOR_AUTHOR_TEXT'] = "
#ID# - ID обращения
#NUMBER# - Номер обращения
#LANGUAGE_ID# - идентификатор языка сайта к которому привязано обращение
#DATE_CREATE# - дата создания
#TIMESTAMP# - дата изменения
#DATE_CLOSE# - дата закрытия
#TITLE# - заголовок обращения
#CATEGORY# - категория обращения
#STATUS# - статус обращения
#CRITICALITY# - критичность обращения
#SOURCE# - источник обращения (web, email, телефон и т.п.)
#SPAM_MARK# - отметка о спаме
#MESSAGE_BODY# - текст сообщения
#FILES_LINKS# - ссылки на прикрепленные файлы
#ADMIN_EDIT_URL# - ссылка для изменения обращения (в административную часть)
#PUBLIC_EDIT_URL# - ссылка для изменения обращения (в публичную часть)

#OWNER_EMAIL# - #OWNER_USER_EMAIL# и/или #OWNER_SID#
#OWNER_USER_ID# - ID автора обращения
#OWNER_USER_NAME# - имя автора обращения
#OWNER_USER_LOGIN# - логин автора обращения
#OWNER_USER_EMAIL# - email автора обращения
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - произвольный идентификатор автора обращения (email, телефон и т.п.)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# или #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_ID# - ID ответственного за обращение
#RESPONSIBLE_USER_NAME# - имя ответственного за обращение
#RESPONSIBLE_USER_LOGIN# - логин ответственного за обращение
#RESPONSIBLE_USER_EMAIL# - email ответственного за обращение
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - EMail всех администраторов аварийно-диспетчерской службы

#CREATED_USER_ID# - ID создателя обращения
#CREATED_USER_LOGIN# - логин создателя обращения
#CREATED_USER_EMAIL# - email создателя обращения
#CREATED_USER_NAME# - имя создателя обращения
#CREATED_MODULE_NAME# - идентификатор модуля средствами которого было создан обращение
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME#

#SUPPORT_COMMENTS# - административный комментарий

";
$MESS ['SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_TITLE'] = "Новое обращение (для аварийно-диспетчерской службы)";
$MESS ['SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_TEXT'] = "
#ID# - ID обращения
#NUMBER# - Номер обращения
#LANGUAGE_ID# - идентификатор языка сайта к которому привязано обращение
#DATE_CREATE# - дата создания
#TIMESTAMP# - дата изменения
#DATE_CLOSE# - дата закрытия
#TITLE# - заголовок обращения
#CATEGORY# - категория обращения
#STATUS# - статус обращения
#CRITICALITY# - критичность обращения
#SOURCE# - источник обращения (web, email, телефон и т.п.)
#SPAM_MARK# - отметка о спаме
#MESSAGE_BODY# - текст сообщения
#FILES_LINKS# - ссылки на прикрепленные файлы
#ADMIN_EDIT_URL# - ссылка для изменения обращения (в административную часть)
#PUBLIC_EDIT_URL# - ссылка для изменения обращения (в публичную часть)

#OWNER_EMAIL# - #OWNER_USER_EMAIL# и/или #OWNER_SID#
#OWNER_USER_ID# - ID автора обращения
#OWNER_USER_NAME# - имя автора обращения
#OWNER_USER_LOGIN# - логин автора обращения
#OWNER_USER_EMAIL# - email автора обращения
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - произвольный идентификатор автора обращения (email, телефон и т.п.)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# или #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_ID# - ID ответственного за обращение
#RESPONSIBLE_USER_NAME# - имя ответственного за обращение
#RESPONSIBLE_USER_LOGIN# - логин ответственного за обращение
#RESPONSIBLE_USER_EMAIL# - email ответственного за обращение
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - EMail всех администраторов аварийно-диспетчерской службы

#CREATED_USER_ID# - ID создателя обращения
#CREATED_USER_LOGIN# - логин создателя обращения
#CREATED_USER_EMAIL# - email создателя обращения
#CREATED_USER_NAME# - имя создателя обращения
#CREATED_MODULE_NAME# - идентификатор модуля средствами которого было создан обращение
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME#

#SUPPORT_COMMENTS# - административный комментарий

#COUPON# - Купон
";
$MESS ['SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_TITLE'] = "Обращение изменено сотрудником аварийно-диспетчерской службы (для автора)";
$MESS ['SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_TEXT'] = "
#ID# - ID обращения
#NUMBER# - Номер обращения
#LANGUAGE_ID# - идентификатор языка сайта к которому привязано обращение
#WHAT_CHANGE# - список того что в обращении изменилось
#DATE_CREATE# - дата создания
#TIMESTAMP# - дата изменения
#DATE_CLOSE# - дата закрытия
#TITLE# - заголовок обращения
#STATUS# - статус обращения
#CATEGORY# - категория обращения
#CRITICALITY# - критичность обращения
#RATE# - оценка ответов
#SOURCE# - первоначальный источник обращения (web, email, телефон и т.д.)
#SPAM_MARK# - отметка о спаме
#MESSAGES_AMOUNT# - количество сообщений в обращении
#ADMIN_EDIT_URL# - ссылка для изменения обращения (в административную часть)
#PUBLIC_EDIT_URL# - ссылка для изменения обращения (в публичную часть)

#OWNER_EMAIL# - #OWNER_USER_EMAIL# и/или #OWNER_SID#
#OWNER_USER_ID# - ID автора обращения
#OWNER_USER_NAME# - имя автора обращения
#OWNER_USER_LOGIN# - логин автора обращения
#OWNER_USER_EMAIL# - email автора обращения
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - произвольный идентификатор автора обращения (email, телефон и т.п.)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# или #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_ID# - ID ответственного за обращение
#RESPONSIBLE_USER_NAME# - имя ответственного за обращение
#RESPONSIBLE_USER_LOGIN# - логин ответственного за обращение
#RESPONSIBLE_USER_EMAIL# - email ответственного за обращение
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - EMail администраторов аварийно-диспетчерской службы

#CREATED_USER_ID# - ID создателя обращения
#CREATED_USER_LOGIN# - логин создателя обращения
#CREATED_USER_EMAIL# - email создателя обращения
#CREATED_USER_NAME# - имя создателя обращения
#CREATED_MODULE_NAME# - идентификатор модуля средствами которого было создан обращение
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME#

#MODIFIED_USER_ID# - ID изменившего обращение
#MODIFIED_USER_LOGIN# - логин изменившего обращение
#MODIFIED_USER_EMAIL# - EMail изменившего обращение
#MODIFIED_USER_NAME# - имя изменившего обращение
#MODIFIED_MODULE_NAME# - идентификатор модуля средствами которого был изменен обращение
#MODIFIED_TEXT# - [#MODIFIED_USER_ID#] (#MODIFIED_USER_LOGIN#) #MODIFIED_USER_NAME#

#MESSAGE_AUTHOR_USER_ID# - ID автора сообщения
#MESSAGE_AUTHOR_USER_NAME# - имя автора сообщения
#MESSAGE_AUTHOR_USER_LOGIN# - логин автора сообщения
#MESSAGE_AUTHOR_USER_EMAIL# - email автора сообщения
#MESSAGE_AUTHOR_TEXT# - [#MESSAGE_AUTHOR_USER_ID#] (#MESSAGE_AUTHOR_USER_LOGIN#) #MESSAGE_AUTHOR_USER_NAME#
#MESSAGE_AUTHOR_SID# - произвольный идентификатор автора сообщения (email, телефон и т.д.)
#MESSAGE_SOURCE# - источник сообщения
#MESSAGE_BODY# - текст сообщения
#FILES_LINKS# - ссылки на прикрепленные файлы

#SUPPORT_COMMENTS# - административный комментарий

";
$MESS ['SUP_SE_TICKET_CHANGE_BY_AUTHOR_FOR_AUTHOR_TITLE'] = "Обращение изменено автором (для автора)";
$MESS ['SUP_SE_TICKET_CHANGE_BY_AUTHOR_FOR_AUTHOR_TEXT'] = "
#ID# - ID обращения
#NUMBER# - Номер обращения
#LANGUAGE_ID# - идентификатор языка сайта к которому привязано обращение
#WHAT_CHANGE# - список того что в обращении изменилось
#DATE_CREATE# - дата создания
#TIMESTAMP# - дата изменения
#DATE_CLOSE# - дата закрытия
#TITLE# - заголовок обращения
#STATUS# - статус обращения
#CATEGORY# - категория обращения
#CRITICALITY# - критичность обращения
#RATE# - оценка ответов
#SOURCE# - первоначальный источник обращения (web, email, телефон и т.д.)
#SPAM_MARK# - отметка о спаме
#MESSAGES_AMOUNT# - количество сообщений в обращении
#ADMIN_EDIT_URL# - ссылка для изменения обращения (в административную часть)
#PUBLIC_EDIT_URL# - ссылка для изменения обращения (в публичную часть)

#OWNER_EMAIL# - #OWNER_USER_EMAIL# и/или #OWNER_SID#
#OWNER_USER_ID# - ID автора обращения
#OWNER_USER_NAME# - имя автора обращения
#OWNER_USER_LOGIN# - логин автора обращения
#OWNER_USER_EMAIL# - email автора обращения
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - произвольный идентификатор автора обращения (email, телефон и т.п.)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# или #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_ID# - ID ответственного за обращение
#RESPONSIBLE_USER_NAME# - имя ответственного за обращение
#RESPONSIBLE_USER_LOGIN# - логин ответственного за обращение
#RESPONSIBLE_USER_EMAIL# - email ответственного за обращение
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - EMail администраторов аварийно-диспетчерской службы

#CREATED_USER_ID# - ID создателя обращения
#CREATED_USER_LOGIN# - логин создателя обращения
#CREATED_USER_EMAIL# - email создателя обращения
#CREATED_USER_NAME# - имя создателя обращения
#CREATED_MODULE_NAME# - идентификатор модуля средствами которого было создан обращение
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME#

#MODIFIED_USER_ID# - ID изменившего обращение
#MODIFIED_USER_LOGIN# - логин изменившего обращение
#MODIFIED_USER_EMAIL# - EMail изменившего обращение
#MODIFIED_USER_NAME# - имя изменившего обращение
#MODIFIED_MODULE_NAME# - идентификатор модуля средствами которого был изменен обращение
#MODIFIED_TEXT# - [#MODIFIED_USER_ID#] (#MODIFIED_USER_LOGIN#) #MODIFIED_USER_NAME#

#MESSAGE_AUTHOR_USER_ID# - ID автора сообщения
#MESSAGE_AUTHOR_USER_NAME# - имя автора сообщения
#MESSAGE_AUTHOR_USER_LOGIN# - логин автора сообщения
#MESSAGE_AUTHOR_USER_EMAIL# - email автора сообщения
#MESSAGE_AUTHOR_TEXT# - [#MESSAGE_AUTHOR_USER_ID#] (#MESSAGE_AUTHOR_USER_LOGIN#) #MESSAGE_AUTHOR_USER_NAME#
#MESSAGE_AUTHOR_SID# - произвольный идентификатор автора сообщения (email, телефон и т.д.)
#MESSAGE_SOURCE# - источник сообщения
#MESSAGE_BODY# - текст сообщения
#FILES_LINKS# - ссылки на прикрепленные файлы

#SUPPORT_COMMENTS# - административный комментарий

";
$MESS ['SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_TITLE'] = "Изменения в обращении (для аварийно-диспетчерской службы)";
$MESS ['SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_TEXT'] = "
#ID# - ID обращения
#NUMBER# - Номер обращения
#LANGUAGE_ID# - идентификатор языка сайта к которому привязано обращение
#WHAT_CHANGE# - список того что в обращении изменилось
#DATE_CREATE# - дата создания
#TIMESTAMP# - дата изменения
#DATE_CLOSE# - дата закрытия
#TITLE# - заголовок обращения
#STATUS# - статус обращения
#CATEGORY# - категория обращения
#CRITICALITY# - критичность обращения
#RATE# - оценка ответов
#SOURCE# - первоначальный источник обращения (web, email, телефон и т.д.)
#SPAM_MARK# - отметка о спаме
#MESSAGES_AMOUNT# - количество сообщений в обращении
#ADMIN_EDIT_URL# - ссылка для изменения обращения (в административную часть)
#PUBLIC_EDIT_URL# - ссылка для изменения обращения (в публичную часть)

#OWNER_EMAIL# - #OWNER_USER_EMAIL# и/или #OWNER_SID#
#OWNER_USER_ID# - ID автора обращения
#OWNER_USER_NAME# - имя автора обращения
#OWNER_USER_LOGIN# - логин автора обращения
#OWNER_USER_EMAIL# - email автора обращения
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - произвольный идентификатор автора обращения (email, телефон и т.п.)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# или #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_ID# - ID ответственного за обращение
#RESPONSIBLE_USER_NAME# - имя ответственного за обращение
#RESPONSIBLE_USER_LOGIN# - логин ответственного за обращение
#RESPONSIBLE_USER_EMAIL# - email ответственного за обращение
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - EMail администраторов аварийно-диспетчерской службы

#CREATED_USER_ID# - ID создателя обращения
#CREATED_USER_LOGIN# - логин создателя обращения
#CREATED_USER_EMAIL# - email создателя обращения
#CREATED_USER_NAME# - имя создателя обращения
#CREATED_MODULE_NAME# - идентификатор модуля средствами которого было создан обращение
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME#

#MODIFIED_USER_ID# - ID изменившего обращение
#MODIFIED_USER_LOGIN# - логин изменившего обращение
#MODIFIED_USER_EMAIL# - EMail изменившего обращение
#MODIFIED_USER_NAME# - имя изменившего обращение
#MODIFIED_MODULE_NAME# - идентификатор модуля средствами которого был изменен обращение
#MODIFIED_TEXT# - [#MODIFIED_USER_ID#] (#MODIFIED_USER_LOGIN#) #MODIFIED_USER_NAME#

#MESSAGE_AUTHOR_USER_ID# - ID автора сообщения
#MESSAGE_AUTHOR_USER_NAME# - имя автора сообщения
#MESSAGE_AUTHOR_USER_LOGIN# - логин автора сообщения
#MESSAGE_AUTHOR_USER_EMAIL# - email автора сообщения
#MESSAGE_AUTHOR_TEXT# - [#MESSAGE_AUTHOR_USER_ID#] (#MESSAGE_AUTHOR_USER_LOGIN#) #MESSAGE_AUTHOR_USER_NAME#
#MESSAGE_AUTHOR_SID# - произвольный идентификатор автора сообщения (email, телефон и т.д.)
#MESSAGE_SOURCE# - источник сообщения
#MESSAGE_HEADER# - \"******* СООБЩЕНИЕ *******\", либо \"******* СКРЫТОЕ СООБЩЕНИЕ *******\"
#MESSAGE_BODY# - текст сообщения
#MESSAGE_FOOTER# - \"*********************** \"
#FILES_LINKS# - ссылки на прикрепленные файлы

#SUPPORT_COMMENTS# - административный комментарий

";
$MESS ['SUP_SE_TICKET_OVERDUE_REMINDER_TITLE'] = "Напоминание о необходимости ответа (для аварийно-диспетчерской службы)";
$MESS ['SUP_SE_TICKET_OVERDUE_REMINDER_TEXT'] = "
#ID# - ID обращения
#NUMBER# - Номер обращения
#LANGUAGE_ID# - идентификатор языка сайта к которому привязано обращение
#DATE_CREATE# - дата создания
#TITLE# - заголовок обращения
#STATUS# - статус обращения
#CATEGORY# - категория обращения
#CRITICALITY# - критичность обращения
#RATE# - оценка ответов
#SOURCE# - первоначальный источник обращения (web, email, телефон и т.д.)
#ADMIN_EDIT_URL# - ссылка для изменения обращения (в административную часть)

#EXPIRATION_DATE# - дата истечения времени реакции
#REMAINED_TIME# - сколько осталось до даты истечения времени реакции

#OWNER_EMAIL# - #OWNER_USER_EMAIL# и/или #OWNER_SID#
#OWNER_USER_ID# - ID автора обращения
#OWNER_USER_NAME# - имя автора обращения
#OWNER_USER_LOGIN# - логин автора обращения
#OWNER_USER_EMAIL# - email автора обращения
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - произвольный идентификатор автора обращения (email, телефон и т.п.)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# или #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_ID# - ID ответственного за обращение
#RESPONSIBLE_USER_NAME# - имя ответственного за обращение
#RESPONSIBLE_USER_LOGIN# - логин ответственного за обращение
#RESPONSIBLE_USER_EMAIL# - email ответственного за обращение
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - EMail администраторов аварийно-диспетчерской службы

#CREATED_USER_ID# - ID создателя обращения
#CREATED_USER_LOGIN# - логин создателя обращения
#CREATED_USER_EMAIL# - email создателя обращения
#CREATED_USER_NAME# - имя создателя обращения
#CREATED_MODULE_NAME# - идентификатор модуля средствами которого было создан обращение
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME#

#MESSAGE_BODY# - текст сообщения клиента требующее ответа
";
$MESS ['SUP_SE_TICKET_NEW_FOR_AUTHOR_SUBJECT'] = "[##NUMBER#] #SERVER_NAME#: Ваше обращение принято";
$MESS ['SUP_SE_TICKET_NEW_FOR_AUTHOR_MESSAGE'] = "Ваше обращение принято, ему присвоен номер #NUMBER#.

Вы не должны отвечать на это письмо. Это только подтверждение,
что аварийно-диспетчерская служба получила ваше обращение и работает с ним.

Информация о вашем обращении:

Тема          - #TITLE#
От кого       - #SOURCE##OWNER_SID##OWNER_TEXT#
Категория     - #CATEGORY#
Критичность   - #CRITICALITY#

Создано           - #CREATED_TEXT##CREATED_MODULE_NAME# [#DATE_CREATE#]
Ответственный     - #RESPONSIBLE_TEXT#

>======================= СООБЩЕНИЕ ===================================

#FILES_LINKS##MESSAGE_BODY#

>=====================================================================

Для просмотра и редактирования обращения воспользуйтесь ссылкой:
http://#SERVER_NAME##PUBLIC_EDIT_URL#?ID=#ID#

Письмо сгенерировано автоматически.
";
$MESS ['SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_SUBJECT'] = "[##NUMBER#] #SERVER_NAME#: Новое обращение";
$MESS ['SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_MESSAGE'] = "Новое обращение # #NUMBER# в аварийно-диспетчерскую службу #SERVER_NAME#.
#SPAM_MARK#
От кого: #SOURCE##OWNER_SID##OWNER_TEXT#

Тема: #TITLE#

>======================= СООБЩЕНИЕ ===================================

#FILES_LINKS##MESSAGE_BODY#

>=====================================================================

Ответственный     - #RESPONSIBLE_TEXT#
Категория         - #CATEGORY#
Критичность       - #CRITICALITY#
Создано           - #CREATED_TEXT##CREATED_MODULE_NAME# [#DATE_CREATE#]

Для просмотра и редактирования обращения воспользуйтесь ссылкой:
http://#SERVER_NAME##ADMIN_EDIT_URL#?ID=#ID#&lang=#LANGUAGE_ID#

Письмо сгенерировано автоматически.
";
$MESS ['SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_SUBJECT'] = "[##NUMBER#] #SERVER_NAME#: Изменения в вашем обращении";
$MESS ['SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_MESSAGE'] = "Изменения в вашем обращении # #NUMBER# на сайте #SERVER_NAME#.

#WHAT_CHANGE#
Тема: #TITLE#

От кого: #MESSAGE_SOURCE##MESSAGE_AUTHOR_SID##MESSAGE_AUTHOR_TEXT#

>======================= СООБЩЕНИЕ ===================================#FILES_LINKS##MESSAGE_BODY#
>=====================================================================

Автор    - #SOURCE##OWNER_SID##OWNER_TEXT#
Создано  - #CREATED_TEXT##CREATED_MODULE_NAME# [#DATE_CREATE#]
Изменено - #MODIFIED_TEXT##MODIFIED_MODULE_NAME# [#TIMESTAMP#]

Ответственный     - #RESPONSIBLE_TEXT#
Категория         - #CATEGORY#
Критичность       - #CRITICALITY#
Статус            - #STATUS#
Оценка ответов    - #RATE#

Для просмотра и редактирования обращения воспользуйтесь ссылкой:
http://#SERVER_NAME##PUBLIC_EDIT_URL#?ID=#ID#

Мы просим Вас не забыть оценить ответы аварийно-диспетчерской службы после закрытия обращения:
http://#SERVER_NAME##PUBLIC_EDIT_URL#?ID=#ID#

Письмо сгенерировано автоматически.
";
$MESS ['SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_SUBJECT'] = "[##NUMBER#] #SERVER_NAME#: Изменения в обращении";
$MESS ['SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_MESSAGE'] = "Изменения в обращении # #NUMBER# в аварийно-диспетчерскую службу #SERVER_NAME#.
#SPAM_MARK#
#WHAT_CHANGE#
Тема: #TITLE#

От кого: #MESSAGE_SOURCE##MESSAGE_AUTHOR_SID##MESSAGE_AUTHOR_TEXT#

>#MESSAGE_HEADER##FILES_LINKS##MESSAGE_BODY#
>#MESSAGE_FOOTER#

Автор    - #SOURCE##OWNER_SID##OWNER_TEXT#
Создано  - #CREATED_TEXT##CREATED_MODULE_NAME# [#DATE_CREATE#]
Изменено - #MODIFIED_TEXT##MODIFIED_MODULE_NAME# [#TIMESTAMP#]

Ответственный     - #RESPONSIBLE_TEXT#
Категория         - #CATEGORY#
Критичность       - #CRITICALITY#
Статус            - #STATUS#
Оценка ответов    - #RATE#

>====================== КОММЕНТАРИЙ ==================================#SUPPORT_COMMENTS#
>=====================================================================

Для просмотра и редактирования обращения воспользуйтесь ссылкой:
http://#SERVER_NAME##ADMIN_EDIT_URL#?ID=#ID#&lang=#LANGUAGE_ID#

Письмо сгенерировано автоматически.
";
$MESS ['SUP_SE_TICKET_OVERDUE_REMINDER_SUBJECT'] = "[##NUMBER#] #SERVER_NAME#: Напоминание о необходимости ответа";
$MESS ['SUP_SE_TICKET_OVERDUE_REMINDER_MESSAGE'] = "Напоминание о необходимости ответа в обращении # #NUMBER# в аварийно-диспетчерскую службу #SERVER_NAME#.

Когда будет просрочено - #EXPIRATION_DATE# (осталось: #REMAINED_TIME#)

>================= ИНФОРМАЦИЯ ПО ОБРАЩЕНИЮ ===========================

Тема    - #TITLE#

Автор   - #SOURCE##OWNER_SID##OWNER_TEXT#
Создано - #CREATED_TEXT##CREATED_MODULE_NAME# [#DATE_CREATE#]

Ответственный     - #RESPONSIBLE_TEXT#
Категория         - #CATEGORY#
Критичность       - #CRITICALITY#
Статус            - #STATUS#
Оценка ответов    - #RATE#

>================ СООБЩЕНИЕ ТРЕБУЮЩЕЕ ОТВЕТА =========================
#MESSAGE_BODY#
>=====================================================================

Для просмотра и редактирования обращения воспользуйтесь ссылкой:
http://#SERVER_NAME##ADMIN_EDIT_URL#?ID=#ID#&lang=#LANGUAGE_ID#

Письмо сгенерировано автоматически.
";
$MESS ['SUP_SE_TICKET_GENERATE_SUPERCOUPON_TITLE'] = "Использован купон";
$MESS ['SUP_SE_TICKET_GENERATE_SUPERCOUPON_TEXT'] = "#COUPON# - Купон
#COUPON_ID# - ID Купона
#DATE# - Дата использования
#USER_ID# - ID использовавшего пользователя
#SESSION_ID# - ID сессии
#GUEST_ID# - ID гостя
";
?>