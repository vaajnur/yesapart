<?
IncludeModuleLangFile(__FILE__);

class CTszhSupportSuperCoupon
{
	function Generate($arParams = array())
	{
		global $DB, $USER, $APPLICATION;
		if (!is_array($arParams)) $arParams = array();

		if(array_key_exists('KEY_FORMAT', $arParams) && strlen($arParams['KEY_FORMAT']) > 0)
		{
			$COUPON_FORMAT = $arParams['KEY_FORMAT'];
		}
		else
		{
			$COUPON_FORMAT = COption::GetOptionString('citrus.tszhtickets', 'SUPERTICKET_COUPON_FORMAT');
		}

		$COUNT = array_key_exists('COUNT_TICKETS', $arParams) ? intval($arParams['COUNT_TICKETS']) : 5;
		if ($COUNT <= 0) $COUNT = 5;

		$SLA_ID = array_key_exists('SLA_ID', $arParams) ? $arParams['SLA_ID'] : COption::GetOptionString("citrus.tszhtickets", 'SUPERTICKET_DEFAULT_SLA');
		$SLA_ID = intval($SLA_ID);
		if ($SLA_ID <= 0) $SLA_ID = false;

		$coupon = false;
		$DB->StartTransaction();
		for ($i = 0; $i < 100; ++$i)
		{
			$coupon = preg_replace_callback('|#|'.BX_UTF_PCRE_MODIFIER, array('CTszhSupportSuperCoupon', '_getrandsymbol'), $COUPON_FORMAT);
			$rs = CTszhSupportSuperCoupon::GetList(false, array('COUPON' => $coupon));
			if ($rs->Fetch())
			{
				$coupon = false;
			}
			else
			{
				break;
			}
		}

		if ($coupon !== false)
		{
			$arFields = array(
				'COUPON' => $coupon,
				'COUNT_TICKETS' => $COUNT,
				'SLA_ID' => $SLA_ID,
			);

			if(array_key_exists('ACTIVE_FROM', $arParams) && strlen($arParams['ACTIVE_FROM']) && CheckDateTime($arParams['ACTIVE_FROM']))
			{
				$arFields['ACTIVE_FROM'] = $arParams['ACTIVE_FROM'];
			}

			if(array_key_exists('ACTIVE_TO', $arParams) && strlen($arParams['ACTIVE_TO']) && CheckDateTime($arParams['ACTIVE_TO']))
			{
				$arFields['ACTIVE_TO'] = $arParams['ACTIVE_TO'];
			}

			if (array_key_exists('ACTIVE', $arParams))
			{
				$arFields['ACTIVE'] = $arParams['ACTIVE'];
			}

			$ID = CTszhSupportSuperCoupon::Add($arFields);
		}
		else
		{
			$APPLICATION->ThrowException(GetMessage('SUP_ST_ERROR_NO_NEW_COUPON'));
		}
		$DB->Commit();

		return $coupon;
	}

	function Add($arFields)
	{
		global $DB, $USER;

		$arFields['~TIMESTAMP_X'] = $DB->GetNowFunction();
		$arFields['~DATE_CREATE'] = $DB->GetNowFunction();

		if(isset($USER) && is_object($USER))
		{
			$arFields['CREATED_USER_ID'] = intval($USER->GetID());
		}

		if (array_key_exists('TIMESTAMP_X', $arFields))
		{
			unset($arFields['TIMESTAMP_X']);
		}

		if (array_key_exists('DATE_CREATE', $arFields))
		{
			unset($arFields['DATE_CREATE']);
		}

		return $DB->Add('b_tszh_ticket_supercoupons', $arFields);
	}

	function Update($ID, $arFields)
	{
		global $DB, $APPLICATION, $USER;
		if (!is_array($arFields))
		{
			$arFields = array();
		}

		$ID = intval($ID);

		if (array_key_exists('ID', $arFields))
		{
			unset($arFields['ID']);
		}

		if (array_key_exists('COUPON', $arFields))
		{
			unset($arFields['COUPON']);
		}

		if (array_key_exists('TIMESTAMP_X', $arFields))
		{
			unset($arFields['TIMESTAMP_X']);
		}

		if (array_key_exists('DATE_CREATE', $arFields))
		{
			unset($arFields['DATE_CREATE']);
		}

		if (array_key_exists('UPDATED_USER_ID', $arFields))
		{
			unset($arFields['UPDATED_USER_ID']);
		}

		if (array_key_exists('~UPDATED_USER_ID', $arFields))
		{
			unset($arFields['~UPDATED_USER_ID']);
		}

		if (array_key_exists('ACTIVE', $arFields))
		{
			$arFields['ACTIVE'] = $arFields['ACTIVE'] == 'Y' ? 'Y' : 'N';
		}

		if (count($arFields) <= 0)
		{
			$APPLICATION->ThrowException(GetMessage('SUP_ST_ERROR_NO_UPDATE_DATA'));
			return false;
		}

		$arFields['~TIMESTAMP_X'] = $DB->GetNowFunction();
		if(isset($USER) && is_object($USER))
		{
			$arFields['UPDATED_USER_ID'] = $USER->GetID();
		}

		$strUpdate = $DB->PrepareUpdate('b_tszh_ticket_supercoupons', $arFields);
		if (strlen($strUpdate) > 0)
		{
			$strSql = "UPDATE b_tszh_ticket_supercoupons SET $strUpdate WHERE ID=$ID";
			$q = $DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);
			$rows = intval($q->AffectedRowsCount());
		}
		else
		{
			$APPLICATION->ThrowException(GetMessage('SUP_ST_ERROR_NO_UPDATE_DATA'));
			return false;
		}

		if ($rows <= 0)
		{
			$APPLICATION->ThrowException(GetMessage('SUP_ST_ERROR_NO_UPDATES_ROWS'));
			return false;
		}

		return true;
	}

	function UseCoupon($coupon)
	{
		global $DB, $USER;
		$ret = false;

		$arFields = false;
		$arLogFields = false;
		$arCoupon = false;

		$curr = ConvertTimeStamp();
		$arFilter = array(
			'LOGIC' => 'AND',

			'COUPON' => $coupon,
			'=ACTIVE' => 'Y',
			array(
				'LOGIC' => 'OR',

				'ACTIVE_FROM' => false,
				'<=ACTIVE_FROM' => $curr,
			),
			array(
				'LOGIC' => 'OR',

				'ACTIVE_TO' => false,
				'>=ACTIVE_TO' => $curr,
			)
		);


		$DB->StartTransaction();

		$rs = CTszhSupportSuperCoupon::GetList(false, $arFilter);
		if ($arCoupon = $rs->Fetch())
		{
			$COUNT_TICKETS = intval($arCoupon['COUNT_TICKETS']);
			if ($COUNT_TICKETS > 0)
			{
				$COUNT_TICKETS--;
				$arFields = array(
					'COUNT_TICKETS' => $COUNT_TICKETS,
					'TIMESTAMP_X' => $DB->GetNowFunction(),
					//'UPDATED_USER_ID' => (isset($USER) && is_object($USER)) ? $USER->GetID() : false,
					'COUNT_USED' => 'COUNT_USED + 1'
				);
				if(isset($USER) && is_object($USER))
				{
					$arFields['UPDATED_USER_ID'] = $USER->GetID();
				}
				if ($aff_rows = $DB->Update('b_tszh_ticket_supercoupons', $arFields, 'WHERE ID=' . $arCoupon['ID']))
				{
					$ret = true;
				}
			}

			$arLogFields = array(
					'~TIMESTAMP_X' => $DB->GetNowFunction(),
					'COUPON_ID' => $arCoupon['ID'],
					'USER_ID' => ((isset($USER) && is_object($USER)) ? $USER->GetID() : false),
					'SUCCESS' => $ret ? 'Y' : 'N',
					'AFTER_COUNT' => $COUNT_TICKETS,
					'SESSION_ID' => (array_key_exists('SESS_SESSION_ID', $_SESSION) ? $_SESSION['SESS_SESSION_ID'] : false),
					'GUEST_ID' => (array_key_exists('SESS_GUEST_ID', $_SESSION) ? $_SESSION['SESS_GUEST_ID'] : false),
					'AFFECTED_ROWS' => $aff_rows,
					'COUPON' => $coupon,
				);

			$arInsert = $DB->PrepareInsert('b_tszh_ticket_supercoupons_log', $arLogFields);
			$strSql =
				"INSERT INTO b_tszh_ticket_supercoupons_log (".$arInsert[0].") ".
				"VALUES(".$arInsert[1].")";
			$DB->Query($strSql, false, 'File: ' . __FILE__ . ' Line: ' . __LINE__);

			$arMail = array(
				'COUPON' => $coupon,
				'COUPON_ID' => $arCoupon['ID'],
				'DATE' => ConvertTimeStamp(false, 'FULL'),
				'USER_ID' => ((isset($USER) && is_object($USER)) ? $USER->GetID() : -1),
				'SESSION_ID' => (array_key_exists('SESS_SESSION_ID', $_SESSION) ? $_SESSION['SESS_SESSION_ID'] : -1),
				'GUEST_ID' => (array_key_exists('SESS_GUEST_ID', $_SESSION) ? $_SESSION['SESS_GUEST_ID'] : -1),
			);

			$rsEvents = GetModuleEvents('citrus.tszhtickets', 'OnBeforeSendCouponEMail');
			while ($arEvent = $rsEvents->Fetch())
			{
				$arMail = ExecuteModuleEvent($arEvent, $arMail);
			}

			if ($arMail)
			{
				$e = new CEvent();
				$e->Send('TSZH_TICKET_GENERATE_SUPERCOUPON', SITE_ID, $arMail);
			}
		}

		$DB->Commit();

		$rsEvents = GetModuleEvents('citrus.tszhtickets', 'OnAfterUseCoupon');
		while ($arEvent = $rsEvents->Fetch())
		{
			ExecuteModuleEvent($arEvent, $coupon, $arCoupon, $arFields, $arLogFields);
		}

		return $ret;
	}

	function GetList($arOrder = array(), $arFilter = array())
	{
		global $DB;
		$arFields = array(
			'ID' => array(
				'TABLE_ALIAS' => 'C',
				'FIELD_NAME' => 'C.ID',
				'FIELD_TYPE' => 'int', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'COUPON' => array(
				'TABLE_ALIAS' => 'C',
				'FIELD_NAME' => 'C.COUPON',
				'FIELD_TYPE' => 'string', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'COUNT_TICKETS' => array(
				'TABLE_ALIAS' => 'C',
				'FIELD_NAME' => 'C.COUNT_TICKETS',
				'FIELD_TYPE' => 'int', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'TIMESTAMP_X' => array(
				'TABLE_ALIAS' => 'C',
				'FIELD_NAME' => 'C.TIMESTAMP_X',
				'FIELD_TYPE' => 'datetime', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'DATE_CREATE' => array(
				'TABLE_ALIAS' => 'C',
				'FIELD_NAME' => 'C.DATE_CREATE',
				'FIELD_TYPE' => 'datetime', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'CREATED_USER_ID' => array(
				'TABLE_ALIAS' => 'C',
				'FIELD_NAME' => 'C.CREATED_USER_ID',
				'FIELD_TYPE' => 'int', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'UPDATED_USER_ID' => array(
				'TABLE_ALIAS' => 'C',
				'FIELD_NAME' => 'C.UPDATED_USER_ID',
				'FIELD_TYPE' => 'int', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'ACTIVE' => array(
				'TABLE_ALIAS' => 'C',
				'FIELD_NAME' => 'C.ACTIVE',
				'FIELD_TYPE' => 'string', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'ACTIVE_FROM' => array(
				'TABLE_ALIAS' => 'C',
				'FIELD_NAME' => 'C.ACTIVE_FROM',
				'FIELD_TYPE' => 'date', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'ACTIVE_TO' => array(
				'TABLE_ALIAS' => 'C',
				'FIELD_NAME' => 'C.ACTIVE_TO',
				'FIELD_TYPE' => 'date', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'COUNT_USED' => array(
				'TABLE_ALIAS' => 'C',
				'FIELD_NAME' => 'C.COUNT_USED',
				'FIELD_TYPE' => 'date', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'SLA_ID' => array(
				'TABLE_ALIAS' => 'S',
				'FIELD_NAME' => 'S.ID',
				'FIELD_TYPE' => 'int', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'SLA_NAME' => array(
				'TABLE_ALIAS' => 'S',
				'FIELD_NAME' => 'S.NAME',
				'FIELD_TYPE' => 'string', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
		);

		$obQueryWhere = new CSQLWhere;
		$obQueryWhere->SetFields($arFields);

		$where = $obQueryWhere->GetQuery($arFilter);

		$order = '';
		if (is_array($arOrder))
		{
			foreach ($arOrder as $k => $v)
			{
				if (array_key_exists($k, $arFields))
				{
					$v = strtoupper($v);
					if($v != 'DESC')
					{
						$v  ='ASC';
					}
					if (strlen($order) > 0)
					{
						$order .= ', ';
					}
					$order .= $arFields[$k]['FIELD_NAME'] . ' ' . $v;
				}
			}
		}

		$strQuery = 'SELECT C.ID, C.COUPON, C.COUNT_TICKETS, C.CREATED_USER_ID, C.UPDATED_USER_ID, C.ACTIVE ACTIVE, C.COUNT_USED COUNT_USED,
		'.$DB->DateToCharFunction('C.TIMESTAMP_X').' TIMESTAMP_X,
		'.$DB->DateToCharFunction('C.DATE_CREATE').' DATE_CREATE,
		'.$DB->DateToCharFunction('C.ACTIVE_FROM', 'SHORT').' ACTIVE_FROM,
		'.$DB->DateToCharFunction('C.ACTIVE_TO', 'SHORT').' ACTIVE_TO,
		UCR.LOGIN CREATED_LOGIN, UCR.NAME CREATED_FIRST_NAME, UCR.LAST_NAME CREATED_LAST_NAME,
		UUP.LOGIN UPDATED_LOGIN, UUP.NAME UPDATED_FIRST_NAME, UUP.LAST_NAME UPDATED_LAST_NAME,
		S.ID SLA_ID, S.NAME SLA_NAME
		FROM b_tszh_ticket_supercoupons C
		LEFT JOIN b_user UCR ON (C.CREATED_USER_ID IS NOT NULL AND C.CREATED_USER_ID = UCR.ID)
		LEFT JOIN b_user UUP ON (C.CREATED_USER_ID IS NOT NULL AND C.UPDATED_USER_ID = UUP.ID)
		LEFT JOIN b_tszh_ticket_sla S ON (C.SLA_ID IS NOT NULL AND C.SLA_ID = S.ID)
		';

		if (strlen($where) > 0)
		{
			$strQuery .= ' WHERE ' . $where;
		}

		if (strlen($order) > 0)
		{
			$strQuery .= ' ORDER BY ' . $order;
		}

		//echo $strQuery;
		return $DB->Query($strQuery, false, "File: ".__FILE__."<br>Line: ".__LINE__);
	}

	function Delete($ID)
	{
		global $DB;
		$ID = intval($ID);
		if ($ID > 0)
		{
			$DB->Query('DELETE FROM b_tszh_ticket_supercoupons WHERE ID=' . $ID, false, "File: ".__FILE__."<br>Line: ".__LINE__);
		}
		return true;
	}

	function GetLogList($arOrder = array(), $arFilter = array())
	{
		global $DB;
		$arFields = array(
			'COUPON_ID' => array(
				'TABLE_ALIAS' => 'C',
				'FIELD_NAME' => 'C.ID',
				'FIELD_TYPE' => 'int', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),


			'COUPON' => array(
				'TABLE_ALIAS' => 'L',
				'FIELD_NAME' => 'L.COUPON',
				'FIELD_TYPE' => 'string', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'SUCCESS' => array(
				'TABLE_ALIAS' => 'L',
				'FIELD_NAME' => 'L.SUCCESS',
				'FIELD_TYPE' => 'string', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'USER_ID' => array(
				'TABLE_ALIAS' => 'L',
				'FIELD_NAME' => 'L.USER_ID',
				'FIELD_TYPE' => 'int', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'TIMESTAMP_X' => array(
				'TABLE_ALIAS' => 'L',
				'FIELD_NAME' => 'L.TIMESTAMP_X',
				'FIELD_TYPE' => 'datetime', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
		);

		$obQueryWhere = new CSQLWhere;
		$obQueryWhere->SetFields($arFields);

		$where = $obQueryWhere->GetQuery($arFilter);

		$order = '';
		if (is_array($arOrder))
		{
			foreach ($arOrder as $k => $v)
			{
				if (array_key_exists($k, $arFields))
				{
					$v = strtoupper($v);
					if($v != 'DESC')
					{
						$v  ='ASC';
					}
					if (strlen($order) > 0)
					{
						$order .= ', ';
					}
					$order .= $arFields[$k]['FIELD_NAME'] . ' ' . $v;
				}
			}
		}

		$strQuery = "SELECT  C.ID COUPON_ID, L.COUPON COUPON,
		L.USER_ID USER_ID, L.SUCCESS SUCCESS, L.AFTER_COUNT AFTER_COUNT,
		L.SESSION_ID SESSION_ID, L.GUEST_ID GUEST_ID,
		U.LOGIN LOGIN, U.NAME FIRST_NAME, U.LAST_NAME LAST_NAME,
		".$DB->DateToCharFunction('L.TIMESTAMP_X')." TIMESTAMP_X
		FROM b_tszh_ticket_supercoupons_log L
		LEFT JOIN b_tszh_ticket_supercoupons C ON (L.COUPON_ID = C.ID)
		LEFT JOIN b_user U ON (L.USER_ID IS NOT NULL AND L.USER_ID = U.ID)";

		if (strlen($where) > 0)
		{
			$strQuery .= ' WHERE ' . $where;
		}

		if (strlen($order) > 0)
		{
			$strQuery .= ' ORDER BY ' . $order;
		}

		//echo $strQuery;
		return $DB->Query($strQuery, false, "File: ".__FILE__."<br>Line: ".__LINE__);


	}

	function _getrandsymbol($x)
	{
		return ToUpper(randString(1));
	}
}
?>