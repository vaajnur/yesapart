<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 28.04.2018 13:23
 */

IncludeModuleLangFile(__FILE__);

class CTszhSupportUser2UserGroup
{
	function GetList($arOrder = array(), $arFilter = array())
	{
		global $DB;
		$arFields = array(
			'GROUP_ID' => array(
				'TABLE_ALIAS' => 'UG',
				'FIELD_NAME' => 'UG.GROUP_ID',
				'FIELD_TYPE' => 'int', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'USER_ID' => array(
				'TABLE_ALIAS' => 'UG',
				'FIELD_NAME' => 'UG.USER_ID',
				'FIELD_TYPE' => 'int', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'CAN_VIEW_GROUP_MESSAGES' => array(
				'TABLE_ALIAS' => 'UG',
				'FIELD_NAME' => 'UG.CAN_VIEW_GROUP_MESSAGES',
				'FIELD_TYPE' => 'string', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),

			'GROUP_NAME' => array(
				'TABLE_ALIAS' => 'G',
				'FIELD_NAME' => 'G.NAME',
				'FIELD_TYPE' => 'string', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'IS_TEAM_GROUP' => array(
				'TABLE_ALIAS' => 'G',
				'FIELD_NAME' => 'G.IS_TEAM_GROUP',
				'FIELD_TYPE' => 'string', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),

			'LOGIN' => array(
				'TABLE_ALIAS' => 'U',
				'FIELD_NAME' => 'U.LOGIN',
				'FIELD_TYPE' => 'string', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'FIRST_NAME' => array(
				'TABLE_ALIAS' => 'U',
				'FIELD_NAME' => 'U.NAME',
				'FIELD_TYPE' => 'string', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),
			'LAST_NAME' => array(
				'TABLE_ALIAS' => 'U',
				'FIELD_NAME' => 'U.LAST_NAME',
				'FIELD_TYPE' => 'string', //int, double, file, enum, int, string, date, datetime
				'JOIN' => false,
			),

		);

		$strOrder = '';
		if (is_array($arOrder) && count($arOrder) > 0)
		{
			foreach ($arOrder as $k => $v)
			{
				if (array_key_exists($k, $arFields))
				{
					$v = strtoupper($v);
					if ($v != 'DESC')
					{
						$v = 'ASC';
					}
					if (strlen($strOrder) > 0)
					{
						$strOrder .= ', ';
					}
					$strOrder .= $arFields[$k]['FIELD_NAME'] . ' ' . $v;
				}
			}
		}

		$obQueryWhere = new CSQLWhere;
		$obQueryWhere->SetFields($arFields);

		$where = $obQueryWhere->GetQuery($arFilter);

		$strQuery = 'SELECT ' .
		            'UG.*, G.NAME GROUP_NAME, G.IS_TEAM_GROUP, ' .
		            'U.LOGIN, U.NAME FIRST_NAME, U.LAST_NAME ' .
		            'FROM b_tszh_ticket_user_ugroup UG ' .
		            'INNER JOIN b_tszh_ticket_ugroups G ON (UG.GROUP_ID=G.ID) ' .
		            'INNER JOIN b_user U ON (UG.USER_ID=U.ID) ';

		if (strlen($where) > 0)
		{
			$strQuery .= ' WHERE ' . $where;
		}
		if (strlen($strOrder) > 0)
		{
			$strQuery .= ' ORDER BY ' . $strOrder;
		}

		return $DB->Query($strQuery, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
	}

	function Add($arFields)
	{
		global $DB;
		if (CTszhSupportUser2UserGroup::CheckFields($arFields))
		{
			$arInsert = $DB->PrepareInsert('b_tszh_ticket_user_ugroup', $arFields);

			return $DB->Query('INSERT INTO b_tszh_ticket_user_ugroup (' . $arInsert[0] . ') VALUES (' . $arInsert[1] . ')', false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		}

		return false;
	}

	function Update($GROUP_ID, $USER_ID, $arFields)
	{
		if (CTszhSupportUser2UserGroup::CheckFields($arFields, $GROUP_ID, $USER_ID))
		{
			global $DB;
			$GROUP_ID = intval($GROUP_ID);
			$USER_ID = intval($USER_ID);

			$strUpdate = $DB->PrepareUpdate('b_tszh_ticket_user_ugroup', $arFields);
			if (strlen($strUpdate) > 0)
			{
				$strSql = "UPDATE b_tszh_ticket_user_ugroup SET $strUpdate WHERE USER_ID=$USER_ID AND GROUP_ID=$GROUP_ID";

				return $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			}
		}

		return false;
	}

	function CheckFields(&$arFields, $GROUP_ID = 0, $USER_ID = 0)
	{
		global $APPLICATION, $DB, $USER;
		$GROUP_ID = intval($GROUP_ID);
		$USER_ID = intval($USER_ID);
		if (!is_array($arFields))
		{
			$arFields = array();
		}

		//if update
		if ($USER_ID > 0 || $GROUP_ID > 0)
		{
			if ($USER_ID <= 0)
			{
				$APPLICATION->ThrowException(GetMessage('SUP_ERROR_USER_ID_EMPTY'));

				return false;
			}
			if ($GROUP_ID <= 0)
			{
				$APPLICATION->ThrowException(GetMessage('SUP_ERROR_GROUP_ID_EMPTY'));

				return false;
			}

			if (array_key_exists('GROUP_ID', $arFields))
			{
				unset($arFields['GROUP_ID']);
			}
			if (array_key_exists('USER_ID', $arFields))
			{
				unset($arFields['USER_ID']);
			}
		}

		//if add
		if ($USER_ID <= 0 && $GROUP_ID <= 0)
		{
			$arFields['GROUP_ID'] = array_key_exists('GROUP_ID', $arFields) ? intval($arFields['GROUP_ID']) : 0;
			$arFields['USER_ID'] = array_key_exists('USER_ID', $arFields) ? intval($arFields['USER_ID']) : 0;

			if ($arFields['USER_ID'] <= 0)
			{
				$APPLICATION->ThrowException(GetMessage('SUP_ERROR_USER_ID_EMPTY'));

				return false;
			}
			if ($arFields['GROUP_ID'] <= 0)
			{
				$APPLICATION->ThrowException(GetMessage('SUP_ERROR_GROUP_ID_EMPTY'));

				return false;
			}

			$rs = $USER->GetByID($arFields['USER_ID']);
			if (!$rs->Fetch())
			{
				$APPLICATION->ThrowException(GetMessage('SUP_ERROR_NO_USER'));

				return false;
			}
			$rs = CTszhSupportUserGroup::GetList(false, array('ID' => $arFields['GROUP_ID']));
			if (!$arGroup = $rs->Fetch())
			{
				$APPLICATION->ThrowException(GetMessage('SUP_ERROR_NO_GROUP'));

				return false;
			}
			if (CTszhTicket::IsAdmin($arFields['USER_ID']) || CTszhTicket::IsSupportTeam($arFields['USER_ID']))
			{
				if ($arGroup['IS_TEAM_GROUP'] <> 'Y')
				{
					$APPLICATION->ThrowException(GetMessage('SUP_ERROR_USER_NO_CLIENT'));

					return false;
				}
			}
			elseif (CTszhTicket::IsSupportClient($arFields['USER_ID']))
			{
				if ($arGroup['IS_TEAM_GROUP'] == 'Y')
				{
					$APPLICATION->ThrowException(GetMessage('SUP_ERROR_USER_NO_TEAM'));

					return false;
				}
			}
			else
			{
				$APPLICATION->ThrowException(GetMessage('SUP_ERROR_NO_SUPPORT_USER'));

				return false;
			}

			$rs = CTszhSupportUser2UserGroup::GetList(false, array('GROUP_ID' => $arFields['GROUP_ID'], 'USER_ID' => $arFields['USER_ID']));
			if ($rs->Fetch())
			{
				$APPLICATION->ThrowException(GetMessage('SUP_ERROR_USERGROUP_EXISTS'));

				return false;
			}
		}

		if (array_key_exists('CAN_VIEW_GROUP_MESSAGES', $arFields))
		{
			$arFields['CAN_VIEW_GROUP_MESSAGES'] = $arFields['CAN_VIEW_GROUP_MESSAGES'] == 'Y' ? 'Y' : 'N';
		}
		elseif ($USER_ID <= 0 && $GROUP_ID <= 0)
		{
			$arFields['CAN_VIEW_GROUP_MESSAGES'] = 'N';
		}

		return true;
	}

	function Delete($GROUP_ID, $USER_ID)
	{
		$GROUP_ID = intval($GROUP_ID);
		$USER_ID = intval($USER_ID);
		if ($GROUP_ID > 0 && $USER_ID > 0)
		{
			global $DB;

			return $DB->Query("DELETE FROM b_tszh_ticket_user_ugroup WHERE USER_ID=$USER_ID AND GROUP_ID=$GROUP_ID");
		}

		return false;
	}

	function SetGroupUsers($GROUP_ID, $arUsers)
	{
		global $APPLICATION;
		$GROUP_ID = intval($GROUP_ID);

		$ret = array();

		if ($GROUP_ID > 0)
		{
			global $DB;
			$DB->Query('DELETE FROM b_tszh_ticket_user_ugroup WHERE GROUP_ID=' . $GROUP_ID);
			if (is_array($arUsers) && count($arUsers) > 0)
			{
				foreach ($arUsers as $user)
				{
					if (is_array($user) && isset($user['USER_ID']) && intval($user['USER_ID']) > 0)
					{
						$arr = array(
							'GROUP_ID' => $GROUP_ID,
							'USER_ID' => $user['USER_ID'],
							'CAN_VIEW_GROUP_MESSAGES' => $user['CAN_VIEW_GROUP_MESSAGES'],
						);

						if (!CTszhSupportUser2UserGroup::Add($arr))
						{
							if ($e = $APPLICATION->GetException())
							{
								$ret[] = $e->GetString();
							}
						}
					}
				}
			}
		}

		return $ret;
	}
}