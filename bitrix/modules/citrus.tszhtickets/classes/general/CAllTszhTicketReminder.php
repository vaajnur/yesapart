<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 28.04.2018 13:16
 */

/**
 * class CAllTszhTicketSLA
 * {
 * function err_mess()
 * {
 * $module_id = "citrus.tszhtickets";
 * @include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$module_id."/install/version.php");
 * return "<br>Module: ".$module_id." (".$arModuleVersion["VERSION"].")<br>Class: CAllTszhTicketSLA<br>File: ".__FILE__;
 * }
 *
 * // ��������� ����� SLA ��� ������������ ������������
 * function Set($arFields, $ID, $CHECK_RIGHTS=true)
 * {
 * $err_mess = (CAllTszhTicketSLA::err_mess())."<br>Function: Set<br>Line: ";
 * global $DB, $USER, $APPLICATION;
 * $ID = intval($ID);
 * $table = "b_tszh_ticket_sla";
 * CTszhTicket::GetRoles($isDemo, $isSupportClient, $isSupportTeam, $isAdmin, $isAccess, $USER_ID, $CHECK_RIGHTS);
 * if ($isAdmin)
 * {
 * if (CTszhTicket::CheckFields($arFields, $ID, array("NAME")))
 * {
 * //echo "arFields:<pre>"; print_r($arFields); echo "</pre>";
 * $arFields_i = CTszhTicket::PrepareFields($arFields, $table, $ID);
 * //echo "arFields_i:<pre>"; print_r($arFields_i); echo "</pre>";
 * if (intval($ID)>0) $DB->Update($table, $arFields_i, "WHERE ID=".intval($ID), $err_mess.__LINE__);
 * else $ID = $DB->Insert($table, $arFields_i, $err_mess.__LINE__);
 *
 * if (intval($ID)>0)
 * {
 * if (is_set($arFields, "arGROUPS"))
 * {
 * $DB->Query("DELETE FROM b_tszh_ticket_sla_2_user_group WHERE SLA_ID = $ID", false, $err_mess.__LINE__);
 * if (is_array($arFields["arGROUPS"]) && count($arFields["arGROUPS"])>0)
 * {
 * foreach($arFields["arGROUPS"] as $GROUP_ID)
 * {
 * $GROUP_ID = intval($GROUP_ID);
 * if ($GROUP_ID>0)
 * {
 * $strSql = "INSERT INTO b_tszh_ticket_sla_2_user_group (SLA_ID, GROUP_ID) VALUES ($ID, $GROUP_ID)";
 * $DB->Query($strSql, false, $err_mess.__LINE__);
 * }
 * }
 * }
 * }
 *
 * if (is_set($arFields, "arSITES"))
 * {
 * $DB->Query("DELETE FROM b_tszh_ticket_sla_2_site WHERE SLA_ID = $ID", false, $err_mess.__LINE__);
 * if (is_array($arFields["arSITES"]) && count($arFields["arSITES"])>0)
 * {
 * foreach($arFields["arSITES"] as $SITE_ID)
 * {
 * if (strlen($FIRST_SITE_ID)<=0) $FIRST_SITE_ID = $SITE_ID;
 * $SITE_ID = $DB->ForSql($SITE_ID);
 * $strSql = "INSERT INTO b_tszh_ticket_sla_2_site (SLA_ID, SITE_ID) VALUES ($ID, '$SITE_ID')";
 * $DB->Query($strSql, false, $err_mess.__LINE__);
 * }
 * }
 * }
 *
 * if (is_set($arFields, "arCATEGORIES"))
 * {
 * $DB->Query("DELETE FROM b_tszh_ticket_sla_2_category WHERE SLA_ID = $ID", false, $err_mess.__LINE__);
 * if (is_array($arFields["arCATEGORIES"]) && count($arFields["arCATEGORIES"])>0)
 * {
 * foreach($arFields["arCATEGORIES"] as $CATEGORY_ID)
 * {
 * $CATEGORY_ID = intval($CATEGORY_ID);
 * $strSql = "INSERT INTO b_tszh_ticket_sla_2_category (SLA_ID, CATEGORY_ID) VALUES ($ID, $CATEGORY_ID)";
 * $DB->Query($strSql, false, $err_mess.__LINE__);
 * }
 * }
 * }
 *
 * if (is_set($arFields, "arCRITICALITIES"))
 * {
 * $DB->Query("DELETE FROM b_tszh_ticket_sla_2_criticality WHERE SLA_ID = $ID", false, $err_mess.__LINE__);
 * if (is_array($arFields["arCRITICALITIES"]) && count($arFields["arCRITICALITIES"])>0)
 * {
 * foreach($arFields["arCRITICALITIES"] as $CRITICALITY_ID)
 * {
 * $CRITICALITY_ID = intval($CRITICALITY_ID);
 * $strSql = "INSERT INTO b_tszh_ticket_sla_2_criticality (SLA_ID, CRITICALITY_ID) VALUES ($ID, $CRITICALITY_ID)";
 * $DB->Query($strSql, false, $err_mess.__LINE__);
 * }
 * }
 * }
 *
 * if (is_set($arFields, "arMARKS"))
 * {
 * $DB->Query("DELETE FROM b_tszh_ticket_sla_2_mark WHERE SLA_ID = $ID", false, $err_mess.__LINE__);
 * if (is_array($arFields["arMARKS"]) && count($arFields["arMARKS"])>0)
 * {
 * foreach($arFields["arMARKS"] as $MARK_ID)
 * {
 * $MARK_ID = intval($MARK_ID);
 * $strSql = "INSERT INTO b_tszh_ticket_sla_2_mark (SLA_ID, MARK_ID) VALUES ($ID, $MARK_ID)";
 * $DB->Query($strSql, false, $err_mess.__LINE__);
 * }
 * }
 * }
 *
 * if (is_set($arFields, "arSHEDULE"))
 * {
 * $DB->Query("DELETE FROM b_tszh_ticket_sla_shedule WHERE SLA_ID = $ID", false, $err_mess.__LINE__);
 * if (is_array($arFields["arSHEDULE"]) && count($arFields["arSHEDULE"])>0)
 * {
 * while(list($weekday, $arSHEDULE) = each($arFields["arSHEDULE"]))
 * {
 * $arF = array(
 * "SLA_ID"            => $ID,
 * "WEEKDAY_NUMBER"    => intval($weekday),
 * "OPEN_TIME"            => "'".$DB->ForSql($arSHEDULE["OPEN_TIME"], 10)."'",
 * );
 * if ($arSHEDULE["OPEN_TIME"]=="CUSTOM" && is_array($arSHEDULE["CUSTOM_TIME"]) && count($arSHEDULE["CUSTOM_TIME"])>0)
 * {
 * foreach($arSHEDULE["CUSTOM_TIME"] as $ar)
 * {
 * if (strlen(trim($ar["MINUTE_FROM"]))>0 || strlen(trim($ar["MINUTE_TILL"]))>0)
 * {
 * $minute_from = strlen($ar["MINUTE_FROM"])>0 ? $ar["MINUTE_FROM"] : "00:00";
 * $a = explode(":",$minute_from);
 * $minute_from = intval($a[0]*60 + $a[1]);
 * $arF["MINUTE_FROM"] = $minute_from;
 *
 * $minute_till = strlen($ar["MINUTE_TILL"])>0 ? $ar["MINUTE_TILL"] : "23:59";
 * $a = explode(":",$minute_till);
 * $minute_till = intval($a[0]*60 + $a[1]);
 * $arF["MINUTE_TILL"] = $minute_till;
 *
 * $DB->Insert("b_tszh_ticket_sla_shedule", $arF, $err_mess.__LINE__);
 * }
 * }
 * }
 * else $DB->Insert("b_tszh_ticket_sla_shedule", $arF, $err_mess.__LINE__);
 * }
 * }
 * }
 *
 * $FIRST_SITE_ID = strlen($FIRST_SITE_ID)>0 ? "'".$DB->ForSql($FIRST_SITE_ID)."'" : "null";
 * $DB->Update($table, array("FIRST_SITE_ID" => $FIRST_SITE_ID), "WHERE ID=".intval($ID), $err_mess.__LINE__);
 * }
 * }
 * }
 * else
 * {
 * //$APPLICATION->ThrowException(GetMessage("SUP_ERROR_ACCESS_DENIED"));
 * $arMsg = Array();
 * $arMsg[] = array("id"=>"PERMISSION", "text"=> GetMessage("SUP_ERROR_ACCESS_DENIED"));
 * $e = new CAdminException($arMsg);
 * $APPLICATION->ThrowException($e);
 * }
 * return $ID;
 * }
 *
 * // ������� SLA
 * function Delete($ID, $CHECK_RIGHTS=true)
 * {
 * $err_mess = (CAllTszhTicketSLA::err_mess())."<br>Function: Delete<br>Line: ";
 * global $DB, $USER, $APPLICATION;
 * $ID = intval($ID);
 * if ($ID<=1) return false;
 * CTszhTicket::GetRoles($isDemo, $isSupportClient, $isSupportTeam, $isAdmin, $isAccess, $USER_ID, $CHECK_RIGHTS);
 * if ($isAdmin)
 * {
 * $strSql = "SELECT DISTINCT 'x' FROM b_tszh_ticket WHERE SLA_ID = $ID";
 * $rs = $DB->Query($strSql, false, $err_mess.__LINE__);
 * if (!$rs->Fetch())
 * {
 * $DB->Query("DELETE FROM b_tszh_ticket_sla_2_site WHERE SLA_ID = $ID", false, $err_mess.__LINE__);
 * $DB->Query("DELETE FROM b_tszh_ticket_sla_2_category WHERE SLA_ID = $ID", false, $err_mess.__LINE__);
 * $DB->Query("DELETE FROM b_tszh_ticket_sla_2_criticality WHERE SLA_ID = $ID", false, $err_mess.__LINE__);
 * $DB->Query("DELETE FROM b_tszh_ticket_sla_2_mark WHERE SLA_ID = $ID", false, $err_mess.__LINE__);
 * $DB->Query("DELETE FROM b_tszh_ticket_sla_2_user_group WHERE SLA_ID = $ID", false, $err_mess.__LINE__);
 * $DB->Query("DELETE FROM b_tszh_ticket_sla_shedule WHERE SLA_ID = $ID", false, $err_mess.__LINE__);
 * $DB->Query("DELETE FROM b_tszh_ticket_sla WHERE ID = $ID", false, $err_mess.__LINE__);
 * return true;
 * }
 * else
 * $APPLICATION->ThrowException(str_replace("#ID#", "$ID", GetMessage("SUP_ERROR_SLA_HAS_TICKET")));
 * }
 * else
 * $APPLICATION->ThrowException(GetMessage("SUP_ERROR_ACCESS_DENIED"));
 * return false;
 * }
 *
 * // ���������� SLA �� ID
 * function GetByID($ID)
 * {
 * $ID = intval($ID);
 * if ($ID<=0) return false;
 * $arFilter = array("ID" => $ID, "ID_EXACT_MATCH" => "Y");
 * $rs = CTszhTicketSLA::GetList($arSort, $arFilter, $is_filtered);
 * return $rs;
 * }
 *
 * // ���������� ������ ���������� ���������� SLA
 * function GetSheduleArray($SLA_ID)
 * {
 * $err_mess = (CAllTszhTicketSLA::err_mess())."<br>Function: GetSheduleArray<br>Line: ";
 * global $DB, $USER, $APPLICATION;
 * $arResult = array();
 * $SLA_ID = intval($SLA_ID);
 * if ($SLA_ID>0)
 * {
 * $strSql = "SELECT * FROM b_tszh_ticket_sla_shedule WHERE SLA_ID = $SLA_ID ORDER BY WEEKDAY_NUMBER, MINUTE_FROM, MINUTE_TILL";
 * $rs = $DB->Query($strSql, false, $err_mess.__LINE__);
 * while($ar = $rs->Fetch())
 * {
 * if ($ar["OPEN_TIME"]=="CUSTOM")
 * {
 * if (intval($ar["MINUTE_FROM"])>0)
 * {
 * $h_from = floor($ar["MINUTE_FROM"]/60);
 * $m_from = $ar["MINUTE_FROM"] - $h_from*60;
 * }
 * if (intval($ar["MINUTE_TILL"])>0)
 * {
 * $h_till = floor($ar["MINUTE_TILL"]/60);
 * $m_till = $ar["MINUTE_TILL"] - $h_till*60;
 * }
 * $arResult[$ar["WEEKDAY_NUMBER"]]["OPEN_TIME"] = $ar["OPEN_TIME"];
 * $arResult[$ar["WEEKDAY_NUMBER"]]["CUSTOM_TIME"][] = array(
 * "MINUTE_FROM"    => $ar["MINUTE_FROM"],
 * "SECOND_FROM"    => $ar["MINUTE_FROM"]*60,
 * "MINUTE_TILL"    => $ar["MINUTE_TILL"],
 * "SECOND_TILL"    => $ar["MINUTE_TILL"]*60,
 * "FROM"            => $h_from.":".str_pad($m_from, 2, 0),
 * "TILL"            => $h_till.":".str_pad($m_till, 2, 0)
 * );
 * }
 * else
 * {
 * $arResult[$ar["WEEKDAY_NUMBER"]] = array("OPEN_TIME" => $ar["OPEN_TIME"]);
 * }
 * $arResult[$ar["WEEKDAY_NUMBER"]]["WEEKDAY_TITLE"] = GetMessage("SUP_WEEKDAY_".$ar["WEEKDAY_NUMBER"]);
 * }
 * }
 * return $arResult;
 * }
 *
 * // ���������� ������ ID ����� ���������� SLA
 * function GetGroupArray($SLA_ID)
 * {
 * $err_mess = (CAllTszhTicketSLA::err_mess())."<br>Function: GetGroupArray<br>Line: ";
 * global $DB, $USER, $APPLICATION;
 * $arResult = array();
 * $SLA_ID = intval($SLA_ID);
 * if ($SLA_ID>0)
 * {
 * $strSql = "SELECT GROUP_ID FROM b_tszh_ticket_sla_2_user_group WHERE SLA_ID = $SLA_ID";
 * $rs = $DB->Query($strSql, false, $err_mess.__LINE__);
 * while($ar = $rs->Fetch()) $arResult[] = $ar["GROUP_ID"];
 * }
 * return $arResult;
 * }
 *
 * // ���������� ������ ID ������ ���������� SLA
 * function GetSiteArray($SLA_ID)
 * {
 * $err_mess = (CAllTszhTicketSLA::err_mess())."<br>Function: GetSiteArray<br>Line: ";
 * global $DB, $USER, $APPLICATION;
 * $arResult = array();
 * $SLA_ID = intval($SLA_ID);
 * if ($SLA_ID>0)
 * {
 * $strSql = "SELECT SITE_ID FROM b_tszh_ticket_sla_2_site WHERE SLA_ID = $SLA_ID";
 * $rs = $DB->Query($strSql, false, $err_mess.__LINE__);
 * while($ar = $rs->Fetch()) $arResult[] = $ar["SITE_ID"];
 * }
 * return $arResult;
 * }
 *
 * // ���������� ������ ID ��������� ���������� SLA
 * function GetCategoryArray($SLA_ID)
 * {
 * $err_mess = (CAllTszhTicketSLA::err_mess())."<br>Function: GetCategoryArray<br>Line: ";
 * global $DB, $USER, $APPLICATION;
 * $arResult = array();
 * $SLA_ID = intval($SLA_ID);
 * if ($SLA_ID>0)
 * {
 * $strSql = "SELECT CATEGORY_ID FROM b_tszh_ticket_sla_2_category WHERE SLA_ID = $SLA_ID";
 * $rs = $DB->Query($strSql, false, $err_mess.__LINE__);
 * while($ar = $rs->Fetch()) $arResult[] = $ar["CATEGORY_ID"];
 * }
 * return $arResult;
 * }
 *
 * // ���������� ������ ID ������������ ���������� SLA
 * function GetCriticalityArray($SLA_ID)
 * {
 * $err_mess = (CAllTszhTicketSLA::err_mess())."<br>Function: GetCriticalityArray<br>Line: ";
 * global $DB, $USER, $APPLICATION;
 * $arResult = array();
 * $SLA_ID = intval($SLA_ID);
 * if ($SLA_ID>0)
 * {
 * $strSql = "SELECT CRITICALITY_ID FROM b_tszh_ticket_sla_2_criticality WHERE SLA_ID = $SLA_ID";
 * $rs = $DB->Query($strSql, false, $err_mess.__LINE__);
 * while($ar = $rs->Fetch()) $arResult[] = $ar["CRITICALITY_ID"];
 * }
 * return $arResult;
 * }
 *
 * // ���������� ������ ID ������ ���������� SLA
 * function GetMarkArray($SLA_ID)
 * {
 * $err_mess = (CAllTszhTicketSLA::err_mess())."<br>Function: GetMarkArray<br>Line: ";
 * global $DB, $USER, $APPLICATION;
 * $arResult = array();
 * $SLA_ID = intval($SLA_ID);
 * if ($SLA_ID>0)
 * {
 * $strSql = "SELECT MARK_ID FROM b_tszh_ticket_sla_2_mark WHERE SLA_ID = $SLA_ID";
 * $rs = $DB->Query($strSql, false, $err_mess.__LINE__);
 * while($ar = $rs->Fetch()) $arResult[] = $ar["MARK_ID"];
 * }
 * return $arResult;
 * }
 *
 * function GetDropDown($SITE_ID="")
 * {
 * if (strlen($SITE_ID)>0 && strtoupper($SITE_ID)!="ALL") $arFilter = array("SITE" => $SITE_ID);
 * $arSort = array("FIRST_SITE_ID" => "ASC", "PRIORITY" => "ASC");
 * $rs = CTszhTicketSLA::GetList($arSort, $arFilter, $is_filtered);
 * return $rs;
 * }
 *
 * function GetForUser($SITE_ID=false, $USER_ID=false)
 * {
 * $err_mess = (CAllTszhTicketSLA::err_mess())."<br>Function: GetForUser<br>Line: ";
 * global $DB, $USER, $APPLICATION;
 *
 * $SLA_ID = 1; // SLA �� ���������
 *
 * $arrGroups = array();
 * if (!is_object($USER)) $USER = new CUser;
 * if ($USER_ID===false && is_object($USER)) $USER_ID = $USER->GetID();
 * if ($SITE_ID==false) $SITE_ID = SITE_ID;
 *
 * $USER_ID = intval($USER_ID);
 * if ($USER_ID>0) $arrGroups = CUser::GetUserGroup($USER_ID);
 * if (count($arrGroups)<=0) $arrGroups[] = 2;
 *
 * $arSLA_2_SITE = array();
 * $rs = $DB->Query("SELECT SLA_ID, SITE_ID FROM b_tszh_ticket_sla_2_site", false, $err_mess.__LINE__);
 * while ($ar = $rs->Fetch()) $arSLA_2_SITE[$ar["SLA_ID"]][] = $ar["SITE_ID"];
 *
 * $strSql = "
 * SELECT
 * SG.SLA_ID
 * FROM
 * b_tszh_ticket_sla_2_user_group SG
 * INNER JOIN b_tszh_ticket_sla S ON (S.ID = SG.SLA_ID)
 * WHERE
 * SG.GROUP_ID in (".implode(",",$arrGroups).")
 * GROUP BY
 * SG.SLA_ID, S.PRIORITY
 * ORDER BY
 * S.PRIORITY DESC
 * ";
 * $rs = $DB->Query($strSql, false, $err_mess.__LINE__);
 * while ($ar = $rs->Fetch())
 * {
 * if (is_array($arSLA_2_SITE[$ar["SLA_ID"]]) && (in_array($SITE_ID, $arSLA_2_SITE[$ar["SLA_ID"]]) || in_array("ALL", $arSLA_2_SITE[$ar["SLA_ID"]])))
 * {
 * $SLA_ID = $ar["SLA_ID"];
 * break;
 * }
 * }
 * return $SLA_ID;
 * }
 * }
 **/
class CAllTszhTicketReminder
{
	function err_mess()
	{
		$module_id = "citrus.tszhtickets";
		@include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $module_id . "/install/version.php");

		return "<br>Module: " . $module_id . " (" . $arModuleVersion["VERSION"] . ")<br>Class: CAllTszhTicketReminder<br>File: " . __FILE__;
	}

	// ������� ��� ������� ������-�������������� � ��������� ��������� ���������
	function Update($TICKET_ID, $sla_changed = false)
	{
		$err_mess = (CAllTszhTicketReminder::err_mess()) . "<br>Function: Update<br>Line: ";
		global $DB;
		$TICKET_ID = intval($TICKET_ID);
		if ($TICKET_ID <= 0)
		{
			return;
		}

		$strSql = "
			SELECT
				NOTIFY_AGENT_ID,
				EXPIRE_AGENT_ID,
				SLA_ID,
				IS_NOTIFIED,
				IS_OVERDUE,
				" . $DB->DateToCharFunction("DATE_CLOSE", "FULL") . "	DATE_CLOSE
			FROM
				b_tszh_ticket
			WHERE
				ID = $TICKET_ID
			";
		$rsTicket = $DB->Query($strSql, false, $err_mess . __LINE__);
		$arTicket = $rsTicket->Fetch();
		$NOTIFY_AGENT_ID = intval($arTicket["NOTIFY_AGENT_ID"]);
		$EXPIRE_AGENT_ID = intval($arTicket["EXPIRE_AGENT_ID"]);

		if (strlen($arTicket["DATE_CLOSE"]) <= 0)
		{
			$strSql = "
				SELECT
					ID,
					OWNER_USER_ID,
					NOTIFY_AGENT_DONE,
					EXPIRE_AGENT_DONE,
					" . $DB->DateToCharFunction("DATE_CREATE", "FULL") . "		DATE_CREATE
				FROM
					b_tszh_ticket_message
				WHERE
					TICKET_ID=$TICKET_ID
				and (NOT_CHANGE_STATUS='N')
				and (IS_HIDDEN='N' or IS_HIDDEN is null or " . $DB->Length("IS_HIDDEN") . "<=0)
				and (IS_LOG='N' or IS_LOG is null or " . $DB->Length("IS_LOG") . "<=0)
				and (IS_OVERDUE='N' or IS_OVERDUE is null or " . $DB->Length("IS_OVERDUE") . "<=0)
				ORDER BY
					C_NUMBER desc
				";
			$rs = $DB->Query($strSql, false, $err_mess . __LINE__);
			if ($arLastMess = $rs->Fetch())
			{
				$LAST_MESSAGE_BY_SUPPORT_TEAM = (CTszhTicket::IsAdmin($arLastMess["OWNER_USER_ID"]) || CTszhTicket::IsSupportTeam($arLastMess["OWNER_USER_ID"])) ? "Y" : "N";

				$arFields = array();
				if ($LAST_MESSAGE_BY_SUPPORT_TEAM == "Y")
				{
					$arFields["IS_OVERDUE"] = "'N'";
					$arFields["IS_NOTIFIED"] = "'N'";
					if ($NOTIFY_AGENT_ID > 0 || $EXPIRE_AGENT_ID > 0)
					{
						CTszhTicketReminder::Delete($TICKET_ID);
						$arFields["NOTIFY_AGENT_ID"] = "null";
						$arFields["EXPIRE_AGENT_ID"] = "null";
					}
				}
				elseif ($LAST_MESSAGE_BY_SUPPORT_TEAM == "N" && $arTicket['SLA_ID'] > 0 && $arTicket['IS_OVERDUE'] != 'Y')
				{
					CTszhTicketReminder::Delete($TICKET_ID);
					if ($arLastMess["NOTIFY_AGENT_DONE"] != "Y" || $arLastMess["EXPIRE_AGENT_DONE"] != "Y")
					{
						$stmp = MakeTimeStamp($arLastMess["DATE_CREATE"]);

						if ($sla_changed)
						{
							$NOW_TS = time();
							$EXP_TS = CAllTszhTicketReminder::GetNextExec($TICKET_ID, $arTicket['SLA_ID'], $stmp);
							if ($EXP_TS < $NOW_TS)
							{
								$stmp = $NOW_TS;//+= $NOW_TS - $EXP_TS + 3;
							}

							//$arFields["IS_OVERDUE"] = "'N'";
							$arFields["IS_NOTIFIED"] = "'N'";

							$DB->Update('b_tszh_ticket_message', array("NOTIFY_AGENT_DONE" => "'N'"), "WHERE ID='" . $arLastMess['ID'] . "'", $err_mess . __LINE__);
						}

						$arAgents = CTszhTicketReminder::Add($TICKET_ID, $arLastMess["ID"], $arTicket["SLA_ID"], $stmp);
						if (intval($arAgents["NOTIFY_AGENT_ID"]) > 0)
						{
							$arFields["NOTIFY_AGENT_ID"] = $arAgents["NOTIFY_AGENT_ID"];
						}
						if (intval($arAgents["EXPIRE_AGENT_ID"]) > 0)
						{
							$arFields["EXPIRE_AGENT_ID"] = $arAgents["EXPIRE_AGENT_ID"];
						}
					}
					else
					{
						$arFields["NOTIFY_AGENT_ID"] = "null";
						$arFields["EXPIRE_AGENT_ID"] = "null";
					}
				}
				if (count($arFields) > 0)
				{
					$DB->Update("b_tszh_ticket", $arFields, "WHERE ID='" . $TICKET_ID . "'", $err_mess . __LINE__);
				}
			}
		}
		else
		{
			CTszhTicketReminder::Remove($TICKET_ID);
		}
	}

	function GetNextPoint(&$WORK_TIME, $INITIAL_STMP, $arSHEDULE)
	{
		$WORK_TIME = 0;
		$IS_OPEN = false;
		$weekday = date("w", $INITIAL_STMP);
		switch ($weekday)
		{
			case 0:
				$weekday = 6;
				break;
			case 1:
				$weekday = 0;
				break;
			case 2:
				$weekday = 1;
				break;
			case 3:
				$weekday = 2;
				break;
			case 4:
				$weekday = 3;
				break;
			case 5:
				$weekday = 4;
				break;
			case 6:
				$weekday = 5;
				break;
		}
		$arDayShedule = $arSHEDULE[$weekday];
		if (is_array($arDayShedule))
		{
			$day = date("d", $INITIAL_STMP);
			$month = date("m", $INITIAL_STMP);
			$year = date("Y", $INITIAL_STMP);
			$hour = date("H", $INITIAL_STMP);
			$minute = date("i", $INITIAL_STMP);
			$second = date("s", $INITIAL_STMP);

			if ($arDayShedule["OPEN_TIME"] == "24H")
			{
				$SHEDULE_POINT = mktime(0, 0, 0, $month, $day + 1, $year);
				$IS_OPEN = true;
			}
			elseif ($arDayShedule["OPEN_TIME"] == "CLOSED")
			{
				$SHEDULE_POINT = mktime(0, 0, 0, $month, $day + 1, $year);
				$IS_OPEN = false;
			}
			elseif ($arDayShedule["OPEN_TIME"] == "CUSTOM")
			{
				$arInterval = array();
				$arCustomTime = $arDayShedule["CUSTOM_TIME"];
				$null_point = mktime(0, 0, 0, $month, $day, $year);
				foreach ($arCustomTime as $arPeriod)
				{
					$arInterval[$null_point + $arPeriod["SECOND_FROM"]] = "FROM";
					$arInterval[$null_point + $arPeriod["SECOND_TILL"]] = "TILL";
				}
				if (count($arInterval) > 0)
				{
					reset($arInterval);
					$prev_value = $null_point;
					while (list($value, $type) = each($arInterval))
					{
						$next_value = $value;
						if ($INITIAL_STMP >= $prev_value && $INITIAL_STMP < $next_value)
						{
							$SHEDULE_POINT = $value;
							$IS_OPEN = ($type == "FROM") ? false : true;
						}
						$prev_value = $next_value;
					}
					if (doubleval($SHEDULE_POINT) <= 0)
					{
						$SHEDULE_POINT = mktime(0, 0, 0, $month, $day + 1, $year);
						$IS_OPEN = false;
					}

				}
			}
			if ($IS_OPEN)
			{
				$WORK_TIME = $SHEDULE_POINT - $INITIAL_STMP;
			}

			return $SHEDULE_POINT;
		}

		return false;
	}

	// ���������� ���� ����� ���������� ���������
	function GetNextExec($TICKET_ID, $SLA_ID, $DATE_START_STMP, $GET_EXPIRE_STMP = false)
	{
		$err_mess = (CAllTszhTicketReminder::err_mess()) . "<br>Function: GetNextExec<br>Line: ";
		global $DB, $APPLICATION;
		$SLA_ID = intval($SLA_ID);
		if ($SLA_ID <= 0 || strlen($DATE_START_STMP) < 0)
		{
			return;
		}

		/** $rs = CTszhTicketSLA::GetByID($SLA_ID);
		 * if ($arSLA = $rs->Fetch())
		 * {
		 * // ���� ����� ������� �� ��������� ����������, ��
		 * if (intval($arSLA["M_RESPONSE_TIME"])>0)
		 * {
		 * // �������� ���������� ������ ������������
		 * $arSHEDULE = CTszhTicketSLA::GetSheduleArray($SLA_ID);
		 *
		 * // �������� ���������� ����� ��� ���� ���� ������� ����
		 * $arr = array();
		 * while (list($weekday, $ar) = each($arSHEDULE)) $arr[$weekday] = $ar["OPEN_TIME"];
		 * foreach($arr as $s)
		 * {
		 * if ($s=="24H" || $s=="CUSTOM")
		 * {
		 * $WORK_DAY_EXISTS = "Y";
		 * break;
		 * }
		 * }
		 *
		 * if ($WORK_DAY_EXISTS=="Y")
		 * {
		 * // ���� ����� � ���������� �������� ���� ��������� ������� �������, ��
		 * if ($GET_EXPIRE_STMP)
		 * {
		 * $DEADLINE_PERIOD = 60*intval($arSLA["M_RESPONSE_TIME"]);
		 * }
		 * else
		 * {
		 * $DEADLINE_PERIOD = 60*(intval($arSLA["M_RESPONSE_TIME"]) - intval($arSLA["M_NOTICE_TIME"]));
		 * }
		 *
		 * $WORK_TIME = $i = 0;
		 * $point = $DATE_START_STMP;
		 * while($WORK_TIME < $DEADLINE_PERIOD)
		 * {
		 * $i++;
		 * $point = CTszhTicketReminder::GetNextPoint($WTIME, $point, $arSHEDULE);
		 * $WORK_TIME = $WORK_TIME + $WTIME;
		 * }
		 * $SURPLUS = $WORK_TIME - $DEADLINE_PERIOD;
		 * $WARNING_STMP = $point - $SURPLUS;
		 * return $WARNING_STMP;
		 * }
		 * }
		 * }**/
		return false;
	}

	// ������� �������
	function Add($TICKET_ID, $MESSAGE_ID, $SLA_ID, $DATE_START_STMP)
	{
		$NEXT_EXEC = CAllTszhTicketReminder::GetNextExec($TICKET_ID, $SLA_ID, $DATE_START_STMP);
		if (intval($NEXT_EXEC) > 0)
		{
			$NEXT_EXEC = ConvertTimeStamp($NEXT_EXEC, "FULL");
			$NOTIFY_AGENT_ID = CAgent::AddAgent("CTszhTicketReminder::Notify(" . $TICKET_ID . ", " . $MESSAGE_ID . ");", "citrus.tszhtickets", "Y", 0, "", "Y", $NEXT_EXEC);
		}
		$NEXT_EXEC = 0;

		$NEXT_EXEC = CTszhTicketReminder::GetNextExec($TICKET_ID, $SLA_ID, $DATE_START_STMP, true);
		if (intval($NEXT_EXEC) > 0)
		{
			$NEXT_EXEC = ConvertTimeStamp($NEXT_EXEC, "FULL");
			$EXPIRE_AGENT_ID = CAgent::AddAgent("CTszhTicketReminder::Expire(" . $TICKET_ID . ", " . $MESSAGE_ID . ");", "citrus.tszhtickets", "Y", 0, "", "Y", $NEXT_EXEC);
		}

		return array("NOTIFY_AGENT_ID" => $NOTIFY_AGENT_ID, "EXPIRE_AGENT_ID" => $EXPIRE_AGENT_ID);
	}

	// ������� ������� � ��������� ��������� ���������
	function Remove($TICKET_ID)
	{
		$err_mess = (CAllTszhTicketReminder::err_mess()) . "<br>Function: Remove<br>Line: ";
		global $DB;
		CTszhTicketReminder::Delete($TICKET_ID);
		$arFields = array(
			"IS_OVERDUE" => "'N'",
			"NOTIFY_AGENT_ID" => "null",
			"EXPIRE_AGENT_ID" => "null",
		);
		$DB->Update("b_tszh_ticket", $arFields, "WHERE ID='" . $TICKET_ID . "'", $err_mess . __LINE__);
	}

	// ������� �������
	function Delete($TICKET_ID)
	{
		$err_mess = (CAllTszhTicketReminder::err_mess()) . "<br>Function: Delete<br>Line: ";
		global $DB, $USER, $APPLICATION;
		$TICKET_ID = intval($TICKET_ID);
		$rs = $DB->Query("SELECT NOTIFY_AGENT_ID, EXPIRE_AGENT_ID FROM b_tszh_ticket WHERE ID=$TICKET_ID", false, $err_mess . __LINE__);
		if ($ar = $rs->Fetch())
		{
			CAgent::Delete($ar["NOTIFY_AGENT_ID"]);
			CAgent::Delete($ar["EXPIRE_AGENT_ID"]);
		}
	}

	// �������� ����������� � ������������� ������ �� ��������� �������
	function Notify($TICKET_ID, $MESSAGE_ID)
	{
		$err_mess = (CAllTszhTicketReminder::err_mess()) . "<br>Function: Notify<br>Line: ";
		global $DB, $USER, $APPLICATION;
		$TICKET_ID = intval($TICKET_ID);
		if ($TICKET_ID <= 0)
		{
			return;
		}

		$rs = $DB->Query("SELECT SITE_ID FROM b_tszh_ticket WHERE ID=$TICKET_ID", false, $err_mess . __LINE__);
		if ($ar = $rs->Fetch())
		{
			$rs = CTszhTicket::GetByID($TICKET_ID, $ar["SITE_ID"], "N");
			if ($arTicket = $rs->Fetch())
			{
				$rsMessage = CTszhTicket::GetMessageByID($MESSAGE_ID, "N", "N");
				if ($arMessage = $rsMessage->Fetch())
				{
					if ($arMessage["MESSAGE_BY_SUPPORT_TEAM"] != "Y")
					{
						$arMessage["EXPIRATION_DATE_STMP"] = CTszhTicketReminder::GetNextExec($arMessage["TICKET_ID"], $arMessage["SLA_ID"], MakeTimeStamp($arMessage["DATE_CREATE"]), true);
						if (intval($arMessage["EXPIRATION_DATE_STMP"]) > 0)
						{
							$arMessage["EXPIRATION_DATE"] = ConvertTimeStamp($arMessage["EXPIRATION_DATE_STMP"], "FULL", $arTicket["SITE_ID"]);
						}
					}

					if (strlen($arMessage["EXPIRATION_DATE"]) > 0)
					{
						$rsSite = CSite::GetByID($arTicket["SITE_ID"]);
						$arSite = $rsSite->Fetch();

						global $MESS;
						$OLD_MESS = $MESS;
						IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhtickets/classes/general/messages.php", $arSite["LANGUAGE_ID"]);

						$SOURCE_NAME = strlen($arTicket["SOURCE_NAME"]) <= 0 ? "" : "[" . $arTicket["SOURCE_NAME"] . "] ";
						if (intval($arTicket["OWNER_USER_ID"]) > 0 || strlen(trim($arTicket["OWNER_LOGIN"])) > 0)
						{
							$OWNER_TEXT = "[" . $arTicket["OWNER_USER_ID"] . "] (" . $arTicket["OWNER_LOGIN"] . ") " . $arTicket["OWNER_NAME"];
							if (strlen(trim($OWNER_SID)) > 0 && $OWNER_SID != "null")
							{
								$OWNER_TEXT = " / " . $OWNER_TEXT;
							}
						}

						if (intval($arTicket["RESPONSIBLE_USER_ID"]) > 0)
						{
							$RESPONSIBLE_TEXT = "[" . $arTicket["RESPONSIBLE_USER_ID"] . "] (" . $arTicket["RESPONSIBLE_LOGIN"] . ") " . $arTicket["RESPONSIBLE_NAME"];
							if (CTszhTicket::IsSupportTeam($arTicket["RESPONSIBLE_USER_ID"]) || CTszhTicket::IsAdmin($arTicket["RESPONSIBLE_USER_ID"]))
							{
								$RESPONSIBLE_TEXT .= " " . GetMessage("SUP_TECHSUPPORT_HINT");
							}
						}

						$arAdminEMails = CTszhTicket::GetAdminEmails();
						if (count($arAdminEMails) > 0)
						{
							$support_admin_email = implode(",", $arAdminEMails);
						}

						// ���������� email ������
						$arrOwnerEMail = array($arTicket["OWNER_EMAIL"]);
						$arrEmails = explode(",", $arTicket["OWNER_SID"]);
						if (is_array($arrEmails) && count($arrEmails) > 0)
						{
							foreach ($arrEmails as $email)
							{
								$email = trim($email);
								if (strlen($email) > 0)
								{
									preg_match_all("#[<\[\(](.*?)[>\]\)]#i" . BX_UTF_PCRE_MODIFIER, $email, $arr);
									if (is_array($arr[1]) && count($arr[1]) > 0)
									{
										foreach ($arr[1] as $email)
										{
											$email = trim($email);
											if (strlen($email) > 0 && !in_array($email, $arrOwnerEMail) && check_email($email))
											{
												$arrOwnerEMail[] = $email;
											}
										}
									}
									elseif (!in_array($email, $arrOwnerEMail) && check_email($email))
									{
										$arrOwnerEMail[] = $email;
									}
								}
							}
						}
						TrimArr($arrOwnerEMail);
						$owner_email = implode(", ", $arrOwnerEMail);

						// ���������� email ������������
						$support_email = $arTicket["RESPONSIBLE_EMAIL"];
						if (strlen($support_email) <= 0)
						{
							$support_email = $support_admin_email;
						}
						if (strlen($support_email) <= 0)
						{
							$support_email = COption::GetOptionString("main", "email_from", "");
						}

						$arr = explode(",", $support_email);
						$arr = array_unique($arr);
						$support_email = implode(",", $arr);
						if (is_array($arr) && count($arr) > 0)
						{
							foreach ($arr as $email)
							{
								unset($arAdminEMails[$email]);
							}
						}
						$support_admin_email = implode(",", $arAdminEMails);

						if ($arTicket["CREATED_MODULE_NAME"] == "citrus.tszhtickets" && strlen($arTicket["CREATED_MODULE_NAME"]) > 0)
						{
							$CREATED_MODULE_NAME = "";
							if (intval($arTicket["CREATED_USER_ID"]) > 0)
							{
								$CREATED_TEXT = "[" . $arTicket["CREATED_USER_ID"] . "] (" . $arTicket["CREATED_LOGIN"] . ") " . $arTicket["CREATED_NAME"];
								if (CTszhTicket::IsSupportTeam($arTicket["CREATED_USER_ID"]) || CTszhTicket::IsAdmin($arTicket["CREATED_USER_ID"]))
								{
									$CREATED_TEXT .= " " . GetMessage("SUP_TECHSUPPORT_HINT");
								}
							}
						}
						else
						{
							$CREATED_MODULE_NAME = "[" . $arTicket["CREATED_MODULE_NAME"] . "]";
						}

						$MESSAGE = PrepareTxtForEmail($arMessage["MESSAGE"], $arSite["LANGUAGE_ID"], false, false);
						$EXPIRATION_DATE = ConvertTimeStamp($arMessage["EXPIRATION_DATE"], "FULL", $arTicket["SITE_ID"]);
						$REMAINED_TIME = $arMessage["EXPIRATION_DATE_STMP"] - time();
						if ($REMAINED_TIME > 0)
						{
							$str_REMAINED_TIME = "";
							$hours = intval($REMAINED_TIME / 3600);
							if ($hours > 0)
							{
								$str_REMAINED_TIME .= $hours . " " . GetMessage("SUP_HOUR") . " ";
								$REMAINED_TIME = $REMAINED_TIME - $hours * 3600;
							}
							$str_REMAINED_TIME .= intval($REMAINED_TIME / 60) . " " . GetMessage("SUP_MIN") . " ";
							$str_REMAINED_TIME .= ($REMAINED_TIME % 60) . " " . GetMessage("SUP_SEC");
						}

						$arFields = array(
							"ID" => $arTicket["ID"],
							"NUMBER" => $arTicket['NUMBER'],
							"LANGUAGE_ID" => $arSite["LANGUAGE_ID"],
							"DATE_CREATE" => $arTicket["DATE_CREATE"],
							"TITLE" => $arTicket["TITLE"],
							"STATUS" => $arTicket["STATUS_NAME"],
							"CATEGORY" => $arTicket["CATEGORY_NAME"],
							"CRITICALITY" => $arTicket["CRITICALITY_NAME"],
							"DIFFICULTY" => $arTicket["DIFFICULTY_NAME"],
							"RATE" => $arTicket["MARK_NAME"],
							"SLA" => $arTicket["SLA_NAME"],
							"SOURCE" => $SOURCE_NAME,
							"ADMIN_EDIT_URL" => "/bitrix/admin/tszh_ticket_edit.php",
							"EXPIRATION_DATE" => $arMessage["EXPIRATION_DATE"],
							"REMAINED_TIME" => $str_REMAINED_TIME,

							"OWNER_EMAIL" => TrimEx($owner_email, ","),
							"OWNER_USER_ID" => $arTicket["OWNER_USER_ID"],
							"OWNER_USER_NAME" => $arTicket["OWNER_NAME"],
							"OWNER_USER_LOGIN" => $arTicket["OWNER_LOGIN"],
							"OWNER_USER_EMAIL" => $arTicket["OWNER_EMAIL"],
							"OWNER_TEXT" => $OWNER_TEXT,
							"OWNER_SID" => $arTicket["OWNER_SID"],

							"SUPPORT_EMAIL" => TrimEx($support_email, ","),
							"RESPONSIBLE_USER_ID" => $arTicket["RESPONSIBLE_USER_ID"],
							"RESPONSIBLE_USER_NAME" => $arTicket["RESPONSIBLE_NAME"],
							"RESPONSIBLE_USER_LOGIN" => $arTicket["RESPONSIBLE_LOGIN"],
							"RESPONSIBLE_USER_EMAIL" => $arTicket["RESPONSIBLE_EMAIL"],
							"RESPONSIBLE_TEXT" => $RESPONSIBLE_TEXT,
							"SUPPORT_ADMIN_EMAIL" => TrimEx($support_admin_email, ","),

							"CREATED_USER_ID" => $arTicket["CREATED_USER_ID"],
							"CREATED_USER_LOGIN" => $arTicket["CREATED_LOGIN"],
							"CREATED_USER_EMAIL" => $arTicket["CREATED_EMAIL"],
							"CREATED_USER_NAME" => $arTicket["CREATED_NAME"],
							"CREATED_MODULE_NAME" => $CREATED_MODULE_NAME,
							"CREATED_TEXT" => $CREATED_TEXT,

							"MESSAGE_BODY" => $MESSAGE,
						);
						//echo "<pre>"; print_r($arFields); echo "</pre>";
						CEvent::Send("TSZH_TICKET_OVERDUE_REMINDER", $arTicket["SITE_ID"], $arFields);
						$MESS = $OLD_MESS;

						$arFields = array("NOTIFY_AGENT_ID" => "null", "IS_NOTIFIED" => "'Y'");
						$DB->Update("b_tszh_ticket", $arFields, "WHERE ID='" . $arTicket["ID"] . "'", $err_mess . __LINE__);

						$arFields = array("NOTIFY_AGENT_DONE" => "'Y'");
						$DB->Update("b_tszh_ticket_message", $arFields, "WHERE ID='" . $arMessage["ID"] . "'", $err_mess . __LINE__);
					}
				}
			}
		}
	}

	function Expire($TICKET_ID, $MESSAGE_ID)
	{
		$err_mess = (CAllTszhTicketReminder::err_mess()) . "<br>Function: Expire<br>Line: ";
		global $DB, $USER, $APPLICATION;
		$TICKET_ID = intval($TICKET_ID);
		if ($TICKET_ID <= 0)
		{
			return;
		}

		$rs = $DB->Query("SELECT SITE_ID FROM b_tszh_ticket WHERE ID=$TICKET_ID", false, $err_mess . __LINE__);
		if ($ar = $rs->Fetch())
		{
			$rs = CTszhTicket::GetByID($TICKET_ID, false, "N");
			if ($arTicket = $rs->Fetch())
			{
				$rsMessage = CTszhTicket::GetMessageByID($MESSAGE_ID, "N", "N");
				if ($arMessage = $rsMessage->Fetch())
				{
					$rsSite = CSite::GetByID($arTicket["SITE_ID"]);
					$arSite = $rsSite->Fetch();

					global $MESS;
					$OLD_MESS = $MESS;
					IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhtickets/classes/general/messages.php", $arSite["LANGUAGE_ID"]);

					// ��������� ��������� ���������
					$arFields = array(
						"EXPIRE_AGENT_ID" => "null",
						"IS_OVERDUE" => "'Y'",
						"OVERDUE_MESSAGES" => "OVERDUE_MESSAGES + 1",
					);
					$DB->Update("b_tszh_ticket", $arFields, "WHERE ID='" . $arTicket["ID"] . "'", $err_mess . __LINE__);

					// ��������� ��������� ���������
					$arFields = array("EXPIRE_AGENT_DONE" => "'Y'");
					$DB->Update("b_tszh_ticket_message", $arFields, "WHERE ID='" . $arMessage["ID"] . "'", $err_mess . __LINE__);

					// ��������� ���-���������
					$message = str_replace("#ID#", $arMessage["ID"], GetMessage("SUP_MESSAGE_OVERDUE_LOG"));
					$message = str_replace("#NUMBER#", $arMessage["C_NUMBER"], $message);
					$message .= "<br><li>" . htmlspecialcharsEx(str_replace("#VALUE#", $arTicket["SLA_NAME"], GetMessage("SUP_SLA_LOG")));

					if (intval($arTicket["RESPONSIBLE_USER_ID"]) > 0)
					{
						$rsUser = CUser::GetByID(intval($arTicket["RESPONSIBLE_USER_ID"]));
						$arUser = $rsUser->Fetch();
						$RESPONSIBLE_TEXT = "[" . $arUser["ID"] . "] (" . $arUser["LOGIN"] . ") " . $arUser["NAME"] . " " . $arUser["LAST_NAME"];
						$message .= "<li>" . htmlspecialcharsEx(str_replace("#VALUE#", $RESPONSIBLE_TEXT, GetMessage("SUP_RESPONSIBLE_LOG")));
					}

					$arFields = array(
						"IS_LOG" => "Y",
						"IS_OVERDUE" => "Y",
						"MESSAGE_CREATED_USER_ID" => "null",
						"MESSAGE_CREATED_MODULE_NAME" => "auto expiration",
						"MESSAGE_CREATED_GUEST_ID" => "null",
						"MESSAGE_SOURCE_ID" => "null",
						"MESSAGE" => $message,
					);
					$MID = CTszhTicket::AddMessage($TICKET_ID, $arFields, $v, "N");

					$MESS = $OLD_MESS;
				}
			}
		}
	}
}