<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 28.04.2018 13:15
 */

IncludeModuleLangFile(__FILE__);

class CAllTszhTicketDictionary
{
	function err_mess()
	{
		$module_id = "citrus.tszhtickets";
		@include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $module_id . "/install/version.php");

		return "<br>Module: " . $module_id . " (" . $arModuleVersion["VERSION"] . ")<br>Class: CAllTszhTicketDictionary<br>File: " . __FILE__;
	}

	function GetDefault($type, $site_id = SITE_ID)
	{
		if ($site_id == "all")
		{
			$site_id = "";
		}
		$arFilter = array("DEFAULT" => "Y", "TYPE" => $type, "SITE" => $site_id);
		$rs = CTszhTicketDictionary::GetList(($v1 = "s_dropdown"), $v2, $arFilter, $v3);
		$ar = $rs->Fetch();

		return $ar["ID"];
	}

	function GetNextSort($TYPE_ID)
	{
		global $DB;
		$err_mess = (CAllTszhTicketDictionary::err_mess()) . "<br>Function: GetNextSort<br>Line: ";
		$strSql = "SELECT max(C_SORT) MAX_SORT FROM b_tszh_ticket_dictionary WHERE C_TYPE='" . $DB->ForSql($TYPE_ID, 5) . "'";
		$z = $DB->Query($strSql, false, $err_mess . __LINE__);
		$zr = $z->Fetch();

		return intval($zr["MAX_SORT"]) + 100;
	}

	function GetDropDown($type = "C", $site_id = false, $sla_id = false)
	{
		$err_mess = (CAllTszhTicketDictionary::err_mess()) . "<br>Function: GetDropDown<br>Line: ";
		global $DB;
		if ($site_id == false || $site_id == "all")
		{
			$site_id = "";
		}
		$arFilter = array("TYPE" => $type, "SITE" => $site_id);
		$rs = CTszhTicketDictionary::GetList(($v1 = "s_dropdown"), $v2, $arFilter, $v3);

		/** if (intval($sla_id)>0 && ($type=="C" || $type=="K" || $type=="M"))
		 * {
		 * $sla_id = intval($sla_id);
		 * switch($type)
		 * {
		 * case "C": $strSql = "SELECT CATEGORY_ID as DID FROM b_tszh_ticket_sla_2_category WHERE SLA_ID=$sla_id"; break;
		 * case "K": $strSql = "SELECT CRITICALITY_ID as DID FROM b_tszh_ticket_sla_2_criticality WHERE SLA_ID=$sla_id"; break;
		 * case "M": $strSql = "SELECT MARK_ID as DID FROM b_tszh_ticket_sla_2_mark WHERE SLA_ID=$sla_id"; break;
		 * }
		 * $r = $DB->Query($strSql, false, $err_mess.__LINE__);
		 * while ($a = $r->Fetch()) $arDID[] = $a["DID"];
		 * $arRecords = array();
		 * while($ar = $rs->Fetch()) if (is_array($arDID) && (in_array($ar["ID"], $arDID) || in_array(0,$arDID))) $arRecords[] = $ar;
		 *
		 * $rs = new CDBResult;
		 * $rs->InitFromArray($arRecords);
		 * }**/
		return $rs;
	}


	function GetDropDownArray($SITE_ID = false, $SLA_ID = false, $arUnsetType = Array("F"))
	{
		//M, C, K, S, SR, D, F
		global $DB;

		if ($SITE_ID == false || $SITE_ID == "all")
		{
			$SITE_ID = "";
		}

		$arFilter = Array("SITE" => $SITE_ID);

		$arReturn = Array();
		$rs = CTszhTicketDictionary::GetList(($v1 = "s_dropdown"), $v2, $arFilter, $v3);
		while ($ar = $rs->Fetch())
		{
			if (in_array($ar["C_TYPE"], $arUnsetType))
			{
				continue;
			}

			$arReturn[$ar["C_TYPE"]][$ar["ID"]] = $ar;
		}

		/** if (intval($SLA_ID)>0)
		 * {
		 * $SLA_ID = intval($SLA_ID);
		 *
		 * $strSql = "SELECT 'M' as C_TYPE, SLA_ID, MARK_ID as DIC_ID FROM b_tszh_ticket_sla_2_mark WHERE SLA_ID = ".$SLA_ID."
		 * UNION ALL
		 * SELECT 'K' as C_TYPE, SLA_ID, CRITICALITY_ID as DIC_ID FROM b_tszh_ticket_sla_2_criticality WHERE SLA_ID = ".$SLA_ID."
		 * UNION ALL
		 * SELECT 'C' as C_TYPE, SLA_ID, CATEGORY_ID as DIC_ID FROM b_tszh_ticket_sla_2_category WHERE SLA_ID = ".$SLA_ID;
		 *
		 * $r = $DB->Query($strSql, false, $err_mess.__LINE__);
		 *
		 * $arUnset = Array();
		 * while ($ar = $r->Fetch())
		 * {
		 * if ($ar["DIC_ID"] == 0)
		 * continue;
		 * else
		 * $arUnset[$ar["C_TYPE"]][] = $ar["DIC_ID"];
		 * }
		 *
		 * if (!empty($arUnset) && !empty($arReturn))
		 * {
		 * foreach ($arReturn as $type => $arID)
		 * {
		 * if (!array_key_exists($type, $arUnset))
		 * continue;
		 *
		 * $arID = array_keys($arID);
		 * $arID = array_diff($arID, $arUnset[$type]);
		 * foreach ($arID as $val)
		 * unset($arReturn[$type][$val]);
		 * }
		 * }
		 * } **/

		//echo "<pre>";print_r($arReturn);echo "</pre>";

		return $arReturn;

	}

	// �������� ������ ������ ��������� � ����������
	function GetSiteArray($DICTIONARY_ID)
	{
		$err_mess = (CAllTszhTicketDictionary::err_mess()) . "<br>Function: GetSiteArray<br>Line: ";
		global $DB;
		$DICTIONARY_ID = intval($DICTIONARY_ID);
		if ($DICTIONARY_ID <= 0)
		{
			return false;
		}
		$arrRes = array();
		$strSql = "
			SELECT
				DS.SITE_ID
			FROM
				b_tszh_ticket_dictionary_2_site DS
			WHERE
				DS.DICTIONARY_ID = $DICTIONARY_ID
			";
		//echo "<pre>".$strSql."</pre>";
		$rs = $DB->Query($strSql, false, $err_mess . __LINE__);
		while ($ar = $rs->Fetch())
		{
			$arrRes[] = $ar["SITE_ID"];
		}

		return $arrRes;
	}

	function GetTypeList()
	{
		$arr = array(
			"reference" => array(
				GetMessage("SUP_CATEGORY"),
				GetMessage("SUP_CRITICALITY"),
				GetMessage("SUP_STATUS"),
				GetMessage("SUP_MARK"),
				GetMessage("SUP_FUA"),
				GetMessage("SUP_SOURCE"),
				GetMessage("SUP_DIFFICULTY"),
			),
			"reference_id" => array(
				"C",
				"K",
				"S",
				"M",
				"F",
				"SR",
				"D",
			),
		);

		return $arr;
	}

	function GetTypeNameByID($ID)
	{
		$arr = CTszhTicketDictionary::GetTypeList();
		$KEY = array_search($ID, $arr["reference_id"]);

		return $arr["reference"][$KEY];
	}

	function GetByID($ID)
	{
		$err_mess = (CAllTszhTicketDictionary::err_mess()) . "<br>Function: GetByID<br>Line: ";
		global $DB;
		$ID = intval($ID);
		if ($ID <= 0)
		{
			return;
		}
		$res = CTszhTicketDictionary::GetList($by, $order, array("ID" => $ID), $is_filtered);

		return $res;
	}

	function GetBySID($sid, $type, $site_id = SITE_ID)
	{
		$err_mess = (CAllTszhTicketDictionary::err_mess()) . "<br>Function: GetBySID<br>Line: ";
		global $DB;
		$rs = CTszhTicketDictionary::GetList($v1, $v2, array(
			"SITE" => $site_id,
			"TYPE" => $type,
			"SID" => $sid,
			"SITE_EXACT_MATCH" => "Y",
			"TYPE_EXACT_MATCH" => "Y",
			"SID_EXACT_MATCH" => "Y",
		), $v3);

		return $rs;
	}

	function Delete($ID, $CHECK_RIGHTS = "Y")
	{
		$err_mess = (CAllTszhTicketDictionary::err_mess()) . "<br>Function: Delete<br>Line: ";
		global $DB, $APPLICATION;
		$ID = intval($ID);
		if ($ID <= 0)
		{
			return;
		}
		$bAdmin = "N";
		if ($CHECK_RIGHTS == "Y")
		{
			$bAdmin = (CTszhTicket::IsAdmin()) ? "Y" : "N";
		}
		else
		{
			$bAdmin = "Y";
		}
		if ($bAdmin == "Y")
		{
			$DB->Query("DELETE FROM b_tszh_ticket_dictionary WHERE ID='$ID'", false, $err_mess . __LINE__);
			$DB->Query('DELETE FROM b_tszh_ticket_dictionary_2_site WHERE DICTIONARY_ID=' . $ID, false, $err_mess . __LINE__);
		}
	}

	function CheckFields($arFields, $ID = false)
	{
		$arMsg = Array();

		if ($ID === false && !(array_key_exists('NAME', $arFields) && strlen($arFields['NAME']) > 0))
		{
			$arMsg[] = array("id" => "NAME", "text" => GetMessage("SUP_FORGOT_NAME"));
		}

		if ($ID !== false)
		{
			$rs = CTszhTicketDictionary::GetByID($ID);
			if (!$rs->Fetch())
			{
				$arMsg[] = array("id" => "ID", "text" => GetMessage("SUP_UNKNOWN_ID", array('#ID#' => $ID)));
			}
		}

		if (array_key_exists('SID', $arFields) && preg_match("[^A-Za-z_0-9]", $arFields['SID']))
		{
			$arMsg[] = array("id" => "SID", "text" => GetMessage("SUP_INCORRECT_SID"));
		}
		elseif (
			strlen($arFields['SID']) > 0 && array_key_exists('arrSITE', $arFields) &&
			is_array($arFields['arrSITE']) && count($arFields['arrSITE']) > 0
		)
		{
			$arFilter = array(
				"TYPE" => $arFields['C_TYPE'],
				"SID" => $arFields['SID'],
				"SITE" => $arFields['arrSITE'],
			);
			if (intval($ID) > 0)
			{
				$arFilter['ID'] = '~' . intval($ID);
			}

			$z = CTszhTicketDictionary::GetList($v1, $v2, $arFilter, $v3);
			if ($zr = $z->Fetch())
			{
				$arMsg[] = array(
					"id" => "SID",
					"text" => GetMessage(
						'SUP_SID_ALREADY_IN_USE',
						array(
							'#TYPE#' => CTszhTicketDictionary::GetTypeNameByID($arFields['C_TYPE']),
							'#LANG#' => $zr['LID'],
							'#RECORD_ID#' => $zr['ID'],
						)
					),
				);
			}
		}

		if (count($arMsg) > 0)
		{
			$e = new CAdminException($arMsg);
			$GLOBALS['APPLICATION']->ThrowException($e);

			return false;
		}

		return true;
	}

	function Add($arFields)
	{
		global $DB;
		$DB->StartTransaction();
		if (!CTszhTicketDictionary::CheckFields($arFields))
		{
			return false;
		}

		CTszhTicketDictionary::__CleanDefault($arFields);

		$ID = intval($DB->Add('b_tszh_ticket_dictionary', $arFields));
		if ($ID > 0)
		{
			CTszhTicketDictionary::__SetSites($ID, $arFields);
			$DB->Commit();

			return $ID;
		}

		$DB->Rollback();
		$GLOBALS['APPLICATION']->ThrowException(GetMessage('SUP_ERROR_ADD_DICTONARY'));

		return false;
	}

	function Update($ID, $arFields)
	{
		global $DB;
		$DB->StartTransaction();
		$ID = intval($ID);
		if (!CTszhTicketDictionary::CheckFields($arFields, $ID))
		{
			return false;
		}

		CTszhTicketDictionary::__CleanDefault($arFields);

		$strUpdate = $DB->PrepareUpdate('b_tszh_ticket_dictionary', $arFields);
		$rs = $DB->Query('UPDATE b_tszh_ticket_dictionary SET ' . $strUpdate . ' WHERE ID=' . $ID);
		if ($rs->AffectedRowsCount() > 0)
		{
			;
		}
		{
			CTszhTicketDictionary::__SetSites($ID, $arFields);
			$DB->Commit();

			return true;
		}

		$DB->Rollback();
		$GLOBALS['APPLICATION']->ThrowException(GetMessage('SUP_ERROR_UPDATE_DICTONARY'));

		return false;
	}

	function __CleanDefault(&$arFields)
	{
		if (
			array_key_exists('SET_AS_DEFAULT', $arFields) && $arFields['SET_AS_DEFAULT'] == 'Y' &&
			array_key_exists('arrSITE', $arFields) && array_key_exists('C_TYPE', $arFields)
		)
		{
			global $DB;
			$arFilter = array(
				'TYPE' => $arFields['C_TYPE'],
				'SITE' => $arFields['arrSITE'],
			);
			$z = CTszhTicketDictionary::GetList($v1, $v2, $arFilter, $v3);
			while ($zr = $z->Fetch())
			{
				$DB->Update('b_tszh_ticket_dictionary', array('SET_AS_DEFAULT' => "'N'"), 'WHERE ID=' . $zr['ID'], '', false, false, false);
			}
		}
		else
		{
			$arFields['SET_AS_DEFAULT'] = 'N';
		}
	}

	function __SetSites($ID, $arFields)
	{
		global $DB;
		if (!array_key_exists('arrSITE', $arFields))
		{
			return;
		}
		$ID = intval($ID);
		$DB->Query('DELETE FROM b_tszh_ticket_dictionary_2_site WHERE DICTIONARY_ID=' . $ID);
		if (is_array($arFields['arrSITE']) && count($arFields['arrSITE']) > 0)
		{
			foreach ($arFields['arrSITE'] as $sid)
			{
				$strSql = "INSERT INTO b_tszh_ticket_dictionary_2_site (DICTIONARY_ID, SITE_ID) VALUES ($ID, '" . $DB->ForSql($sid, 2) . "')";
				$DB->Query($strSql);
			}
		}
	}
}