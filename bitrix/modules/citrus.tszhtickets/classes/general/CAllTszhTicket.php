<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 28.04.2018 13:13
 */

IncludeModuleLangFile(__FILE__);

global $TSZH_SUPPORT_CACHE_USER_ROLES;
$TSZH_SUPPORT_CACHE_USER_ROLES  = Array();

class CAllTszhTicket
{
	function ModuleID()
	{
		return "citrus.tszhtickets";
	}

	function err_mess()
	{
		$module_id = CTszhTicket::ModuleID();
		@include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $module_id . "/install/version.php");

		return "<br>Module: " . $module_id . " (" . $arModuleVersion["VERSION"] . ")<br>Class: CAllTszhTicket<br>File: " . __FILE__;
	}

	/***************************************************************
	 *
	 * ������ ������� �� ������ � ������ �� ������
	 *
	 * �������������� �����:
	 *
	 * D - ������ ������
	 * R - ������ ������������
	 * T - ��������� ������������
	 * V - ����-������
	 * W - ������������� ������������
	 *****************************************************************/

	function GetDeniedRoleID()
	{
		return "D";
	}

	function GetSupportClientRoleID()
	{
		return "R";
	}

	function GetSupportTeamRoleID()
	{
		return "T";
	}

	function GetDemoRoleID()
	{
		return "V";
	}

	function GetAdminRoleID()
	{
		return "W";
	}

	// ���������� true ���� �������� ������������ ����� �������� ���� �� ������
	function HaveRole($role, $USER_ID = false)
	{
		global $DB, $USER, $APPLICATION, $TSZH_SUPPORT_CACHE_USER_ROLES;
		if (!is_object($USER))
		{
			$USER = new CUser;
		}

		if ($USER_ID === false && is_object($USER))
		{
			$UID = $USER->GetID();
		}
		else
		{
			$UID = $USER_ID;
		}

		$arRoles = Array();
		if (array_key_exists($UID, $TSZH_SUPPORT_CACHE_USER_ROLES) && is_array($TSZH_SUPPORT_CACHE_USER_ROLES[$UID]))
		{
			$arRoles = $TSZH_SUPPORT_CACHE_USER_ROLES[$UID];
		}
		else
		{
			$arrGroups = Array();
			if ($USER_ID === false && is_object($USER))
			{
				$arrGroups = $USER->GetUserGroupArray();
			}
			else
			{
				$arrGroups = CUser::GetUserGroup($USER_ID);
			}

			sort($arrGroups);
			$arRoles = $APPLICATION->GetUserRoles("citrus.tszhtickets", $arrGroups);
			$TSZH_SUPPORT_CACHE_USER_ROLES[$UID] = $arRoles;
		}

		if (in_array($role, $arRoles))
		{
			return true;
		}

		return false;

	}

	// true - ���� ������������ ����� ���� "������������� ������������"
	// false - � ��������� ������
	function IsAdmin($USER_ID = false)
	{
		global $USER;

		if ($USER_ID === false && is_object($USER))
		{
			if ($USER->IsAdmin())
			{
				return true;
			}
		}

		return CTszhTicket::HaveRole(CTszhTicket::GetAdminRoleID(), $USER_ID);
	}

	// true - ���� ������������ ����� ���� "����-������"
	// false - � ��������� ������
	function IsDemo($USER_ID = false)
	{
		return CTszhTicket::HaveRole(CTszhTicket::GetDemoRoleID(), $USER_ID);
	}

	// true - ���� ������������ ����� ���� "��������� ������������"
	// false - � ��������� ������
	function IsSupportTeam($USER_ID = false)
	{
		return CTszhTicket::HaveRole(CTszhTicket::GetSupportTeamRoleID(), $USER_ID);
	}

	// true - ���� ������������ ����� ���� "��������� ������������"
	// false - � ��������� ������
	function IsSupportClient($USER_ID = false)
	{
		return CTszhTicket::HaveRole(CTszhTicket::GetSupportClientRoleID(), $USER_ID);
	}

	function IsOwner($TICKET_ID, $USER_ID = false)
	{
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: IsOwner<br>Line: ";
		global $DB, $USER;
		if ($USER_ID === false && is_object($USER))
		{
			$USER_ID = $USER->GetID();
		}
		$USER_ID = intval($USER_ID);
		$TICKET_ID = intval($TICKET_ID);
		if ($USER_ID <= 0 || $TICKET_ID <= 0)
		{
			return false;
		}

		$strSql = "SELECT 'x' FROM b_tszh_ticket WHERE ID=$TICKET_ID and (OWNER_USER_ID=$USER_ID or CREATED_USER_ID=$USER_ID)";
		$rs = $DB->Query($strSql, false, $err_mess . __LINE__);
		if ($ar = $rs->Fetch())
		{
			return true;
		}

		return false;
	}

	// ���������� ���� ��������� ������������
	function GetRoles(&$isDemo, &$isSupportClient, &$isSupportTeam, &$isAdmin, &$isAccess, &$USER_ID, $CHECK_RIGHTS = true)
	{
		global $DB, $USER, $APPLICATION;
		static $arTICKET_USER_ROLES;
		$isDemo = $isSupportClient = $isSupportTeam = $isAdmin = $isAccess = false;
		if (is_object($USER))
		{
			$USER_ID = intval($USER->GetID());
		}
		else
		{
			$USER_ID = 0;
		}
		if ($CHECK_RIGHTS)
		{
			if ($USER_ID > 0)
			{
				if (is_array($arTICKET_USER_ROLES) && in_array($USER_ID, array_keys($arTICKET_USER_ROLES)))
				{
					$isDemo = $arTICKET_USER_ROLES[$USER_ID]["isDemo"];
					$isSupportClient = $arTICKET_USER_ROLES[$USER_ID]["isSupportClient"];
					$isSupportTeam = $arTICKET_USER_ROLES[$USER_ID]["isSupportTeam"];
					$isAdmin = $arTICKET_USER_ROLES[$USER_ID]["isAdmin"];
				}
				else
				{
					$isDemo = CTszhTicket::IsDemo($USER_ID);
					$isSupportClient = CTszhTicket::IsSupportClient($USER_ID);
					$isSupportTeam = CTszhTicket::IsSupportTeam($USER_ID);
					$isAdmin = CTszhTicket::IsAdmin($USER_ID);
					$arTICKET_USER_ROLES[$USER_ID] = array(
						"isDemo" => $isDemo,
						"isSupportClient" => $isSupportClient,
						"isSupportTeam" => $isSupportTeam,
						"isAdmin" => $isAdmin,
					);
				}
			}
		}
		else
		{
			$isAdmin = true;
		}

		if ($isDemo || $isSupportClient || $isSupportTeam || $isAdmin)
		{
			$isAccess = true;
		}
	}

	// ���������� ������ ID ����� ��� ������� ������ ����
	// $role - ������������� ����
	function GetGroupsByRole($role)
	{
		//Todo: ������������ � �������� �� ���������

		global $APPLICATION, $USER;
		if (!is_object($USER))
		{
			$USER = new CUser;
		}

		$arGroups = array();
		$arBadGroups = Array();
		$res = $APPLICATION->GetGroupRightList(Array("MODULE_ID" => "citrus.tszhtickets"/*, "G_ACCESS" => $role*/));
		while ($ar = $res->Fetch())
		{
			if ($ar["G_ACCESS"] == $role)
			{
				$arGroups[] = $ar["GROUP_ID"];
			}
			else
			{
				$arBadGroups[] = $ar["GROUP_ID"];
			}
		}

		$right = COption::GetOptionString("citrus.tszhtickets", "GROUP_DEFAULT_RIGHT", "D");
		if ($right == $role)
		{
			$res = CGroup::GetList($v1 = "dropdown", $v2 = "asc", array("ACTIVE" => "Y"));
			while ($ar = $res->Fetch())
			{
				if (!in_array($ar["ID"], $arGroups) && !in_array($ar["ID"], $arBadGroups))
				{
					$arGroups[] = $ar["ID"];
				}
			}
		}

		//echo "1".$role."1"; print_r($arGroups);echo "<br>";
		return $arGroups;

		/*$arGroups = array();

		$z = CGroup::GetList($v1="dropdown", $v2="asc", array("ACTIVE" => "Y"));
		while($zr = $z->Fetch())
		{
			$arRoles = $APPLICATION->GetUserRoles("citrus.tszhtickets", array(intval($zr["ID"])), "Y", "N");
			if (in_array($role, $arRoles)) $arGroups[] = intval($zr["ID"]);
		}

		//echo "2".$role."2"; print_r(array_unique($arGroups));echo "<br>";
		return array_unique($arGroups);*/
	}

	// ���������� ������ ����� � ����� "������������� ������������"
	function GetAdminGroups()
	{
		return CTszhTicket::GetGroupsByRole(CTszhTicket::GetAdminRoleID());
	}

	// ���������� ������ ����� � ����� "��������� ������������"
	function GetSupportTeamGroups()
	{
		return CTszhTicket::GetGroupsByRole(CTszhTicket::GetSupportTeamRoleID());
	}

	// ���������� ������ EMail ������� ���� ������������� ������� �������� ����
	function GetEmailsByRole($role)
	{
		global $DB, $APPLICATION, $USER;
		if (!is_object($USER))
		{
			$USER = new CUser;
		}
		$arrEMail = array();
		$arGroups = CTszhTicket::GetGroupsByRole($role);
		if (is_array($arGroups) && count($arGroups) > 0)
		{
			$rsUser = CUser::GetList($v1 = "id", $v2 = "desc", array("ACTIVE" => "Y", "GROUPS_ID" => $arGroups));
			while ($arUser = $rsUser->Fetch())
			{
				$arrEMail[$arUser["EMAIL"]] = $arUser["EMAIL"];
			}
		}

		return array_unique($arrEMail);
	}

	// ���������� ������ EMail'�� ���� ������������� ������� ���� "�������������"
	function GetAdminEmails()
	{
		return CTszhTicket::GetEmailsByRole(CTszhTicket::GetAdminRoleID());
	}

	// ���������� ������ EMail'�� ���� ������������� ������� ���� "��������� ������������"
	function GetSupportTeamEmails()
	{
		return CTszhTicket::GetEmailsByRole(CTszhTicket::GetSupportTeamRoleID());
	}

	/*****************************************************************
	 * ������ ������� ����� ��� ���� �������
	 *****************************************************************/

	// �������� ����� �������
	function CheckFilter($arFilter)
	{
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: CheckFilter<br>Line: ";
		global $DB, $USER, $APPLICATION;
		$str = "";
		$arMsg = Array();

		$arDATES = array(
			"DATE_MODIFY_1",
			"DATE_MODIFY_2",
			"DATE_CREATE_1",
			"DATE_CREATE_2",
		);
		foreach ($arDATES as $key)
		{
			if (is_set($arFilter, $key) && strlen($arFilter[$key]) > 0 && !CheckDateTime($arFilter[$key]))
			{
				$arMsg[] = array("id" => $key, "text" => GetMessage("SUP_ERROR_REQUIRED_" . $key));
			}
			//$str.= GetMessage("SUP_ERROR_INCORRECT_".$key)."<br>";
		}

		if (!empty($arMsg))
		{
			$e = new CAdminException($arMsg);
			$GLOBALS["APPLICATION"]->ThrowException($e);

			return false;
		}

		return true;
	}

	// �������� ����� ����� �������� � ���� ������
	function CheckFields($arFields, $ID, $arrREQUIRED)
	{
		global $DB, $USER, $APPLICATION, $MESS;

		$arMsg = Array();

		// ��������� ��������� ������������ ����
		if (is_array($arrREQUIRED) && count($arrREQUIRED) > 0)
		{
			foreach ($arrREQUIRED as $key)
			{
				if ($ID <= 0 || ($ID > 0 && is_set($arFields, $key)))
				{
					if (!is_array($arFields[$key]) && (strlen($arFields[$key]) <= 0 || $arFields[$key] == "NOT_REF"))
					{
						$arMsg[] = array("id" => $key, "text" => GetMessage("SUP_ERROR_REQUIRED_" . $key));
						//$str.= GetMessage("SUP_ERROR_REQUIRED_".$key)."<br>";
					}
				}
			}
		}

		// ��������� ������������ ���
		$arrDATE = array(
			"DATE_CREATE",
			"DATE_MODIFY",
			"LAST_MESSAGE_DATE",
		);
		foreach ($arrDATE as $key)
		{
			if (strlen($arFields[$key]) > 0)
			{
				if (!CheckDateTime($arFields[$key]))
				{
					$arMsg[] = array("id" => $key, "text" => GetMessage("SUP_ERROR_INCORRECT_" . $key));
				}
				//$str.= GetMessage("SUP_ERROR_INCORRECT_".$key)."<br>";
			}
		}

		$arrEMAIL = array(
			"EMAIL",
		);
		foreach ($arrEMAIL as $key)
		{
			if (strlen($arFields[$key]) > 0)
			{
				if (!check_email($arFields[$key]))
				{
					$arMsg[] = array("id" => $key, "text" => GetMessage("SUP_ERROR_INCORRECT_" . $key));
				}
				//$str.= GetMessage("SUP_ERROR_INCORRECT_".$key)."<br>";
			}
		}

		if (!empty($arMsg))
		{
			$e = new CAdminException($arMsg);
			$GLOBALS["APPLICATION"]->ThrowException($e);

			return false;
		}

		return true;
	}

	// �������������� ������������ ������ �������� ��� ������� � ���� ������
	function PrepareFields($arFields, $table, $ID)
	{
		global $DB, $USER, $APPLICATION;

		$ID = intval($ID);
		$arFields_i = array();

		// �����
		$arrNUMBER = array(
			//"SLA_ID",
			"AGENT_ID",
			"CATEGORY_ID",
			"CRITICALITY_ID",
			"STATUS_ID",
			"MARK_ID",
			"SOURCE_ID",
			"DIFFICULTY_ID",
			"DICTIONARY_ID",
			"TICKET_ID",
			"MESSAGE_ID",
			"AUTO_CLOSE_DAYS",
			"MESSAGES",
			"OVERDUE_MESSAGES",
			"EXTERNAL_ID",
			"OWNER_USER_ID",
			"OWNER_GUEST_ID",
			"CREATED_USER_ID",
			"CREATED_GUEST_ID",
			"MODIFIED_USER_ID",
			"MODIFIED_GUEST_ID",
			"RESPONSIBLE_USER_ID",
			"LAST_MESSAGE_USER_ID",
			"LAST_MESSAGE_GUEST_ID",
			"CURRENT_RESPONSIBLE_USER_ID",
			"USER_ID",
			"C_NUMBER",
			"C_SORT",
			"PRIORITY",
			"RESPONSE_TIME",
			"NOTICE_TIME",
			"WEEKDAY_NUMBER",
			"MINUTE_FROM",
			"MINUTE_TILL",
		);
		foreach ($arrNUMBER as $key)
		{
			if (is_set($arFields, $key))
			{
				$arFields_i[$key] = (strlen($arFields[$key]) > 0) ? intval($arFields[$key]) : "null";
			}
		}

		// ��� ������
		$arrTYPE = array(
			"PREVIEW_TYPE",
			"DESCRIPTION_TYPE",
		);
		foreach ($arrTYPE as $key)
		{
			if (is_set($arFields, $key))
			{
				$arFields_i[$key] = $arFields[$key] == "text" ? "'text'" : "'html'";
			}
		}

		// �������
		$arrBOOLEAN = array(
			"AUTO_CLOSED",
			"IS_SPAM",
			"LAST_MESSAGE_BY_SUPPORT_TEAM",
			"IS_HIDDEN",
			"IS_LOG",
			"IS_OVERDUE",
			"IS_SPAM",
			"MESSAGE_BY_SUPPORT_TEAM",
			"SET_AS_DEFAULT",
			"AUTO_CLOSED",
			"HOLD_ON",
			"NOT_CHANGE_STATUS",
		);
		foreach ($arrBOOLEAN as $key)
		{
			if (is_set($arFields, $key))
			{
				$arFields_i[$key] = $arFields[$key] == "Y" ? "'Y'" : "'N'";
			}
		}

		// �����
		$arrTEXT = array(
			"OWNER_SID",
			"LAST_MESSAGE_SID",
			"SUPPORT_COMMENTS",
			"MESSAGE",
			"MESSAGE_SEARCH",
			"EXTERNAL_FIELD_1",
			"DESCR",
			"DESCRIPTION",
		);
		foreach ($arrTEXT as $key)
		{
			if (is_set($arFields, $key))
			{
				$arFields_i[$key] = (strlen($arFields[$key]) > 0) ? "'" . $DB->ForSql($arFields[$key]) . "'" : "null";
			}
		}

		// ������
		$arrSTRING = array(
			"NAME",
			"TITLE",
			"CREATED_MODULE_NAME",
			"MODIFIED_MODULE_NAME",
			"HASH",
			"EXTENSION_SUFFIX",
			"C_TYPE",
			"SID",
			"EVENT1",
			"EVENT2",
			"EVENT3",
			"RESPONSE_TIME_UNIT",
			"NOTICE_TIME_UNIT",
			"OPEN_TIME",
		);
		foreach ($arrSTRING as $key)
		{
			if (is_set($arFields, $key))
			{
				$arFields_i[$key] = (strlen($arFields[$key]) > 0) ? "'" . $DB->ForSql($arFields[$key], 255) . "'" : "null";
			}
		}

		// ����
		$arrDATE = array(
			"TIMESTAMP_X",
			"DATE_CLOSE",
			"LAST_MESSAGE_DATE",
		);
		foreach ($arrDATE as $key)
		{
			if (is_set($arFields, $key))
			{
				$arFields_i[$key] = (strlen($arFields[$key]) > 0) ? $DB->CharToDateFunction($arFields[$key]) : "null";
			}
		}

		// �����������
		$arIMAGE = array();
		foreach ($arIMAGE as $key)
		{
			if (is_set($arFields, $key))
			{
				if (is_array($arFields[$key]))
				{
					$arIMAGE = $arFields[$key];
					$arIMAGE["MODULE_ID"] = "citrus.tszhtickets";
					$arIMAGE["del"] = $_POST[$key . "_del"];
					if ($ID > 0)
					{
						$rs = $DB->Query("SELECT $key FROM $table WHERE ID=$ID", false, $err_mess . __LINE__);
						$ar = $rs->Fetch();
						$arIMAGE["old_file"] = $ar[$key];
					}
					if (strlen($arIMAGE["name"]) > 0 || strlen($arIMAGE["del"]) > 0)
					{
						$fid = CFile::SaveFile($arIMAGE, "citrus.tszhtickets");
						$arFields_i[$key] = (intval($fid) > 0) ? intval($fid) : "null";
					}
				}
				else
				{
					if ($ID > 0)
					{
						$rs = $DB->Query("SELECT $key FROM $table WHERE ID=$ID", false, $err_mess . __LINE__);
						$ar = $rs->Fetch();
						if (intval($ar[$key]) > 0)
						{
							CFile::Delete($ar[$key]);
						}
					}
					$arFields_i[$key] = intval($arFields[$key]);
				}
			}
		}

		if (is_set($arFields, "CREATED_USER_ID"))
		{
			if (intval($arFields["CREATED_USER_ID"]) > 0)
			{
				$arFields_i["CREATED_USER_ID"] = intval($arFields["CREATED_USER_ID"]);
			}
		}
		elseif ($ID <= 0 && $USER->IsAuthorized())
		{
			$arFields_i["CREATED_USER_ID"] = intval($USER->GetID());
		}

		if (is_set($arFields, "CREATED_GUEST_ID"))
		{
			if (intval($arFields["CREATED_GUEST_ID"]) > 0)
			{
				$arFields_i["CREATED_GUEST_ID"] = intval($arFields["CREATED_GUEST_ID"]);
			}
		}
		elseif ($ID <= 0 && array_key_exists('SESS_GUEST_ID', $_SESSION))
		{
			$arFields_i["CREATED_GUEST_ID"] = intval($_SESSION["SESS_GUEST_ID"]);
		}

		if (is_set($arFields, "MODIFIED_USER_ID"))
		{
			if (intval($arFields["MODIFIED_USER_ID"]) > 0)
			{
				$arFields_i["MODIFIED_USER_ID"] = intval($arFields["MODIFIED_USER_ID"]);
			}
		}
		elseif ($USER->IsAuthorized())
		{
			$arFields_i["MODIFIED_USER_ID"] = intval($USER->GetID());
		}

		if (is_set($arFields, "MODIFIED_GUEST_ID"))
		{
			if (intval($arFields["MODIFIED_GUEST_ID"]) > 0)
			{
				$arFields_i["MODIFIED_GUEST_ID"] = intval($arFields["MODIFIED_GUEST_ID"]);
			}
		}
		elseif (array_key_exists('SESS_GUEST_ID', $_SESSION))
		{
			$arFields_i["MODIFIED_GUEST_ID"] = intval($_SESSION["SESS_GUEST_ID"]);
		}

		if (is_set($arFields, "DATE_CREATE"))
		{
			if (strlen($arFields["DATE_CREATE"]) > 0)
			{
				$arFields_i["DATE_CREATE"] = $DB->CharToDateFunction($arFields["DATE_CREATE"]);
			}
		}
		elseif ($ID <= 0)
		{
			$arFields_i["DATE_CREATE"] = $DB->CurrentTimeFunction();
		}


		if (is_set($arFields, "LAST_MESSAGE_DATE"))
		{
			if (strlen($arFields["LAST_MESSAGE_DATE"]) > 0)
			{
				$arFields_i["LAST_MESSAGE_DATE"] = $DB->CharToDateFunction($arFields["LAST_MESSAGE_DATE"]);
			}
		}
		elseif ($ID <= 0)
		{
			$arFields_i["LAST_MESSAGE_DATE"] = $DB->CurrentTimeFunction();
		}


		if (is_set($arFields, "DATE_MODIFY"))
		{
			if (strlen($arFields["DATE_MODIFY"]) > 0)
			{
				$arFields_i["DATE_MODIFY"] = $DB->CharToDateFunction($arFields["DATE_MODIFY"]);
			}
		}
		else
		{
			$arFields_i["DATE_MODIFY"] = $DB->CurrentTimeFunction();
		}

		// ������� ������ ���� ��� ��������� �������
		unset($arFields_i["ID"]);
		$ar1 = $DB->GetTableFieldsList($table);
		$ar2 = array_keys($arFields_i);
		$arDiff = array_diff($ar2, $ar1);
		if (is_array($arDiff) && count($arDiff) > 0)
		{
			foreach ($arDiff as $value)
			{
				unset($arFields_i[$value]);
			}
		}

		return $arFields_i;
	}

	/*****************************************************************
	 * ������ ������� �� ������ �� ������
	 *****************************************************************/

	function MarkMessageAsSpam($MESSAGE_ID, $EXACTLY = "Y", $CHECK_RIGHTS = "Y")
	{
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: MarkMessageAsSpam<br>Line: ";
		global $DB, $USER;
		$MESSAGE_ID = intval($MESSAGE_ID);
		if ($MESSAGE_ID <= 0)
		{
			return;
		}

		$bAdmin = "N";
		$bSupportTeam = "N";
		if ($CHECK_RIGHTS == "Y")
		{
			$bAdmin = (CTszhTicket::IsAdmin()) ? "Y" : "N";
			$bSupportTeam = (CTszhTicket::IsSupportTeam()) ? "Y" : "N";
		}
		else
		{
			$bAdmin = "Y";
			$bSupportTeam = "Y";
		}

		if (($bAdmin == "Y" || $bSupportTeam == "Y") && CModule::IncludeModule("mail"))
		{
			$EXACTLY = ($EXACTLY == "Y" && $bAdmin == "Y") ? "Y" : "N";
			if ($rsMessage = CTszhTicket::GetMessageByID($MESSAGE_ID, $CHECK_RIGHTS))
			{
				if ($arMessage = $rsMessage->Fetch())
				{
					if ($arMessage["IS_LOG"] != "Y")
					{
						$email_id = intval($arMessage["EXTERNAL_ID"]);
						$header = $arMessage["EXTERNAL_FIELD_1"];
						$arFields = array("IS_SPAM" => "'" . $EXACTLY . "'");
						$DB->Update("b_tszh_ticket_message", $arFields, "WHERE ID=" . $MESSAGE_ID, $err_mess . __LINE__);

						$EXACTLY = ($EXACTLY == "Y") ? true : false;
						$rsEmail = CMailMessage::GetByID($email_id);
						if ($rsEmail->Fetch())
						{
							CMailMessage::MarkAsSpam($email_id, $EXACTLY);
						}
						else
						{
							CmailFilter::MarkAsSpam($header . " \n\r " . $arMessage["MESSAGE"], $EXACTLY);
						}
					}
				}
			}
		}
	}

	function UnMarkMessageAsSpam($MESSAGE_ID, $CHECK_RIGHTS = "Y")
	{
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: UnMarkMessageAsSpam<br>Line: ";
		global $DB, $USER;
		$MESSAGE_ID = intval($MESSAGE_ID);
		if ($MESSAGE_ID <= 0)
		{
			return;
		}

		$bAdmin = "N";
		$bSupportTeam = "N";
		if ($CHECK_RIGHTS == "Y")
		{
			$bAdmin = (CTszhTicket::IsAdmin()) ? "Y" : "N";
			$bSupportTeam = (CTszhTicket::IsSupportTeam()) ? "Y" : "N";
		}
		else
		{
			$bAdmin = "Y";
			$bSupportTeam = "Y";
		}

		if (($bAdmin == "Y" || $bSupportTeam == "Y") && CModule::IncludeModule("mail"))
		{
			$rsMessage = CTszhTicket::GetMessageByID($MESSAGE_ID, $CHECK_RIGHTS);
			if ($arMessage = $rsMessage->Fetch())
			{
				$arFields = array("IS_SPAM" => "null");
				$DB->Update("b_tszh_ticket_message", $arFields, "WHERE ID=" . $MESSAGE_ID, $err_mess . __LINE__);

				$email_id = intval($arMessage["EXTERNAL_ID"]);
				$header = $arMessage["EXTERNAL_FIELD_1"];
				$rsEmail = CMailMessage::GetByID($email_id);
				if ($rsEmail->Fetch())
				{
					CMailMessage::MarkAsSpam($email_id, false);
				}
				else
				{
					CmailFilter::DeleteFromSpamBase($header . " \n\r " . $arMessage["MESSAGE"], true);
					CmailFilter::MarkAsSpam($header . " \n\r " . $arMessage["MESSAGE"], false);
				}
			}
		}
	}

	function MarkAsSpam($TICKET_ID, $EXACTLY = "Y", $CHECK_RIGHTS = "Y")
	{
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: MarkAsSpam<br>Line: ";
		global $DB, $USER;
		$TICKET_ID = intval($TICKET_ID);
		if ($TICKET_ID <= 0)
		{
			return;
		}

		$bAdmin = "N";
		$bSupportTeam = "N";
		if ($CHECK_RIGHTS == "Y")
		{
			$bAdmin = (CTszhTicket::IsAdmin()) ? "Y" : "N";
			$bSupportTeam = (CTszhTicket::IsSupportTeam()) ? "Y" : "N";
		}
		else
		{
			$bAdmin = "Y";
			$bSupportTeam = "Y";
		}

		if ($bAdmin == "Y" || $bSupportTeam == "Y")
		{
			$EXACTLY = ($EXACTLY == "Y" && $bAdmin == "Y") ? "Y" : "N";

			$arFilter = array("TICKET_ID" => $TICKET_ID, "TICKET_ID_EXACT_MATCH" => "Y", "IS_LOG" => "N");
			if ($rsMessages = CTszhTicket::GetMessageList($a, $b, $arFilter, $c, $CHECK_RIGHTS))
			{
				// �������� �������� ���������
				if ($arMessage = $rsMessages->Fetch())
				{
					CTszhTicket::MarkMessageAsSpam($arMessage["ID"], $EXACTLY, $CHECK_RIGHTS);
				}
			}
			$arFields = array("IS_SPAM" => "'" . $EXACTLY . "'");
			$DB->Update("b_tszh_ticket", $arFields, "WHERE ID=" . $TICKET_ID, $err_mess . __LINE__);
		}
	}

	function UnMarkAsSpam($TICKET_ID, $CHECK_RIGHTS = "Y")
	{
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: UnMarkAsSpam<br>Line: ";
		global $DB, $USER;
		$TICKET_ID = intval($TICKET_ID);
		if ($TICKET_ID <= 0)
		{
			return;
		}

		if ($CHECK_RIGHTS == "Y")
		{
			$bAdmin = (CTszhTicket::IsAdmin()) ? "Y" : "N";
			$bSupportTeam = (CTszhTicket::IsSupportTeam()) ? "Y" : "N";
		}
		else
		{
			$bAdmin = "Y";
			$bSupportTeam = "Y";
		}

		if ($bAdmin == "Y" || $bSupportTeam == "Y")
		{
			$arFilter = array("TICKET_ID" => $TICKET_ID, "TICKET_ID_EXACT_MATCH" => "Y");
			if ($rsMessages = CTszhTicket::GetMessageList($a, $b, $arFilter, $c, $CHECK_RIGHTS))
			{
				// ������� ������� � ����� ������ � ������� ���������
				if ($arMessage = $rsMessages->Fetch())
				{
					CTszhTicket::UnMarkMessageAsSpam($arMessage["ID"], $CHECK_RIGHTS);
				}
			}
			$arFields = array("IS_SPAM" => "null");
			$DB->Update("b_tszh_ticket", $arFields, "WHERE ID=" . $TICKET_ID, $err_mess . __LINE__);
		}
	}


	/*****************************************************************
	 * ������ ������� �� ���������� �����������
	 *****************************************************************/

	function UpdateLastParams($TICKET_ID, $RESET_AUTO_CLOSE = false, $CHANGE_LAST_MESSAGE_DATE = true, $SET_REOPEN_DEFAULT = true)
	{
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: UpdateLastParams<br>Line: ";
		global $DB, $USER;
		$TICKET_ID = intval($TICKET_ID);
		if ($TICKET_ID <= 0)
		{
			return;
		}

		$arFields = array();
		if ($RESET_AUTO_CLOSE == "Y")
		{
			$arFields["AUTO_CLOSE_DAYS"] = "null";
		}

		// ��������� ���������� ������
		$strSql = "
			SELECT
				ID,
				" . $DB->DateToCharFunction("DATE_CREATE", "FULL") . " DATE_CREATE,
				OWNER_USER_ID,
				OWNER_GUEST_ID,
				OWNER_SID
			FROM
				b_tszh_ticket_message
			WHERE
				TICKET_ID=$TICKET_ID
			and (NOT_CHANGE_STATUS='N')
			and (IS_HIDDEN='N' or IS_HIDDEN is null or " . $DB->Length("IS_HIDDEN") . "<=0)
			and (IS_LOG='N' or IS_LOG is null or " . $DB->Length("IS_LOG") . "<=0)
			and (IS_OVERDUE='N' or IS_OVERDUE is null or " . $DB->Length("IS_OVERDUE") . "<=0)
			ORDER BY
				C_NUMBER desc
			";
		//echo $strSql; exit;
		$rs = $DB->Query($strSql, false, $err_mess . __LINE__);
		if ($arLastMess = $rs->Fetch())
		{
			$LAST_MESSAGE_BY_SUPPORT_TEAM = (CTszhTicket::IsAdmin($arLastMess["OWNER_USER_ID"]) || CTszhTicket::IsSupportTeam($arLastMess["OWNER_USER_ID"])) ? "Y" : "N";

			$arFields["LAST_MESSAGE_USER_ID"] = $arLastMess["OWNER_USER_ID"];

			if ($CHANGE_LAST_MESSAGE_DATE)
			{
				$arFields["LAST_MESSAGE_DATE"] = $DB->CharToDateFunction($arLastMess["DATE_CREATE"]);
			}//NN

			$arFields["LAST_MESSAGE_GUEST_ID"] = intval($arLastMess["OWNER_GUEST_ID"]);
			$arFields["LAST_MESSAGE_SID"] = "'" . $DB->ForSql($arLastMess["OWNER_SID"], 255) . "'";
			$arFields["LAST_MESSAGE_BY_SUPPORT_TEAM"] = "'" . $LAST_MESSAGE_BY_SUPPORT_TEAM . "'";
		}

		// ��������� ���������� ���������
		$strSql = "
			SELECT
				SUM(CASE WHEN IS_HIDDEN='Y' THEN 0 ELSE 1 END) MESSAGES,
				SUM(TASK_TIME) ALL_TIME
			FROM
				b_tszh_ticket_message
			WHERE
				TICKET_ID = $TICKET_ID
			and (IS_LOG='N' or IS_LOG is null or " . $DB->Length("IS_LOG") . "<=0)
			and (IS_OVERDUE='N' or IS_OVERDUE is null or " . $DB->Length("IS_OVERDUE") . "<=0)
			";
		$z = $DB->Query($strSql, false, $err_mess . __LINE__);
		$zr = $z->Fetch();
		$arFields["MESSAGES"] = intval($zr["MESSAGES"]);
		$arFields["PROBLEM_TIME"] = intval($zr["ALL_TIME"]);

		if ($SET_REOPEN_DEFAULT)
		{
			$arFields["REOPEN"] = "'N'";
		}

		$DB->Update("b_tszh_ticket", $arFields, "WHERE ID='" . $TICKET_ID . "'", $err_mess . __LINE__);
	}

	function UpdateMessages($TICKET_ID)
	{
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: UpdateMessages<br>Line: ";
		global $DB;
		$TICKET_ID = intval($TICKET_ID);
		if ($TICKET_ID <= 0)
		{
			return;
		}

		$arFields = array();

		// ��������� ���������� ���������
		$strSql = "
			SELECT
				SUM(CASE WHEN IS_HIDDEN='Y' THEN 0 ELSE 1 END) MESSAGES,
				SUM(TASK_TIME) ALL_TIME
			FROM
				b_tszh_ticket_message
			WHERE
				TICKET_ID = $TICKET_ID
			and (IS_LOG='N' or IS_LOG is null or " . $DB->Length("IS_LOG") . "<=0)
			and (IS_OVERDUE='N' or IS_OVERDUE is null or " . $DB->Length("IS_OVERDUE") . "<=0)
			";
		$z = $DB->Query($strSql, false, $err_mess . __LINE__);
		$zr = $z->Fetch();
		$arFields["MESSAGES"] = intval($zr["MESSAGES"]);
		$arFields["PROBLEM_TIME"] = intval($zr["ALL_TIME"]);

		$DB->Update("b_tszh_ticket", $arFields, "WHERE ID='" . $TICKET_ID . "'", $err_mess . __LINE__);
	}


	function GetFileList(&$by, &$order, $arFilter = array())
	{
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: GetFileList<br>Line: ";
		global $DB, $USER;
		$arSqlSearch = Array();
		$strSqlSearch = "";
		if (is_array($arFilter))
		{
			$filter_keys = array_keys($arFilter);
			for ($i = 0; $i < count($filter_keys); $i++)
			{
				$key = $filter_keys[$i];
				$val = $arFilter[$filter_keys[$i]];
				if ((is_array($val) && count($val) <= 0) || (!is_array($val) && (strlen($val) <= 0 || $val === 'NOT_REF')))
				{
					continue;
				}
				$match_value_set = (in_array($key . "_EXACT_MATCH", $filter_keys)) ? true : false;
				$key = strtoupper($key);
				switch ($key)
				{
					case "LINK_ID":
						$match = ($arFilter[$key . "_EXACT_MATCH"] == "N" && $match_value_set) ? "Y" : "N";
						$arSqlSearch[] = GetFilterQuery("MF.ID", $val, $match);
						break;
					case "MESSAGE":
					case "TICKET_ID":
					case "FILE_ID":
					case "HASH":
					case "MESSAGE_ID":
						$match = ($arFilter[$key . "_EXACT_MATCH"] == "N" && $match_value_set) ? "Y" : "N";
						$arSqlSearch[] = GetFilterQuery("MF." . $key, $val, $match);
						break;
				}
			}
		}
		if ($by == "s_id")
		{
			$strSqlOrder = "ORDER BY MF.ID";
		}
		elseif ($by == "s_file_id")
		{
			$strSqlOrder = "ORDER BY F.ID";
		}
		elseif ($by == "s_message_id")
		{
			$strSqlOrder = "ORDER BY MF.MESSAGE_ID";
		}
		else
		{
			$by = "s_id";
			$strSqlOrder = "ORDER BY MF.ID";
		}
		if ($order == "desc")
		{
			$strSqlOrder .= " desc ";
			$order = "desc";
		}
		else
		{
			$strSqlOrder .= " asc ";
			$order = "asc";
		}
		$strSqlSearch = GetFilterSqlSearch($arSqlSearch);
		$strSql = "
			SELECT
				F.*,
				MF.ID as LINK_ID,
				MF.HASH,
				MF.MESSAGE_ID,
				MF.TICKET_ID,
				MF.EXTENSION_SUFFIX
			FROM
				b_tszh_ticket_message_2_file MF
			INNER JOIN b_file F ON (MF.FILE_ID = F.ID)
			WHERE
				$strSqlSearch
			$strSqlOrder
		";
		//echo "<pre>".$strSql."</pre>";
		$res = $DB->Query($strSql, false, $err_mess . __LINE__);

		return $res;
	}

	function GetMessageByID($ID, $CHECK_RIGHTS = "Y", $get_user_name = "Y")
	{
		return CTszhTicket::GetMessageList($by, $order, array("ID" => $ID, "ID_EXACT_MATCH" => "Y"), $is_filtered, $CHECK_RIGHTS, $get_user_name);
	}

	function GetByID($ID, $lang = LANG, $CHECK_RIGHTS = "Y", $get_user_name = "Y", $get_extra_names = "Y")
	{
		return CTszhTicket::GetList($by, $order, array(
			"ID" => $ID,
			"ID_EXACT_MATCH" => "Y",
		), $is_filtered, $CHECK_RIGHTS, $get_user_name, $get_extra_names, $lang);
	}

	function Delete($TICKET_ID, $CHECK_RIGHTS = "Y")
	{
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: Delete<br>Line: ";
		global $DB, $USER;
		$TICKET_ID = intval($TICKET_ID);
		if ($TICKET_ID <= 0)
		{
			return;
		}
		$bAdmin = "N";
		if ($CHECK_RIGHTS == "Y")
		{
			$bAdmin = (CTszhTicket::IsAdmin()) ? "Y" : "N";
		}
		else
		{
			$bAdmin = "Y";
		}
		if ($bAdmin == "Y")
		{
			$strSql = "
				SELECT
					F.ID
				FROM
					b_tszh_ticket_message_2_file MF,
					b_file F
				WHERE
					MF.TICKET_ID = '$TICKET_ID'
				and F.ID=MF.FILE_ID
				";
			$z = $DB->Query($strSql, false, $err_mess . __LINE__);
			while ($zr = $z->Fetch())
			{
				CFile::Delete($zr["ID"]);
			}

			CTszhTicketReminder::Delete($TICKET_ID);
			$DB->Query("DELETE FROM b_tszh_ticket_message_2_file WHERE TICKET_ID='$TICKET_ID'", false, $err_mess . __LINE__);
			$DB->Query("DELETE FROM b_tszh_ticket_message WHERE TICKET_ID='$TICKET_ID'", false, $err_mess . __LINE__);
			$DB->Query("DELETE FROM b_tszh_ticket WHERE ID='$TICKET_ID'", false, $err_mess . __LINE__);

			$GLOBALS["USER_FIELD_MANAGER"]->Delete("TSZH_TICKET", $TICKET_ID);
		}
	}

	function UpdateOnline($TICKET_ID, $USER_ID = false, $CURRENT_MODE = "")
	{
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: UpdateOnline<br>Line: ";
		global $DB, $USER;
		if ($USER_ID === false && is_object($USER))
		{
			$USER_ID = $USER->GetID();
		}
		$TICKET_ID = intval($TICKET_ID);
		$USER_ID = intval($USER_ID);
		if ($TICKET_ID <= 0 || $USER_ID <= 0)
		{
			return;
		}
		$arFields = array(
			"TIMESTAMP_X" => $DB->GetNowFunction(),
			"TICKET_ID" => $TICKET_ID,
			"USER_ID" => $USER_ID,
		);
		if ($CURRENT_MODE !== false)
		{
			$arFields["CURRENT_MODE"] = strlen($CURRENT_MODE) > 0 ? "'" . $DB->ForSQL($CURRENT_MODE, 20) . "'" : "null";
		}
		$rows = $DB->Update("b_tszh_ticket_online", $arFields, "WHERE TICKET_ID=$TICKET_ID and USER_ID=$USER_ID", $err_mess . __LINE__);
		if (intval($rows) <= 0)
		{
			$DB->Insert("b_tszh_ticket_online", $arFields, $err_mess . __LINE__);
		}
	}

	function SetTicket($arFields, $TICKET_ID = "", $CHECK_RIGHTS = "Y", $SEND_EMAIL_TO_AUTHOR = "Y", $SEND_EMAIL_TO_TECHSUPPORT = "Y")
	{
		//global $DB;
		//$DB->DebugToFile = true;
		$x = CTszhTicket::Set($arFields, $MESSAGE_ID, $TICKET_ID, $CHECK_RIGHTS, $SEND_EMAIL_TO_AUTHOR, $SEND_EMAIL_TO_TECHSUPPORT);

		//$DB->DebugToFile = false;
		return $x;
	}

	function Set($arFields, &$MID, $ID = "", $CHECK_RIGHTS = "Y", $SEND_EMAIL_TO_AUTHOR = "Y", $SEND_EMAIL_TO_TECHSUPPORT = "Y")
	{
		global $DB, $APPLICATION, $USER, $USER_FIELD_MANAGER;
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: Set<br>Line: ";
		if (!is_object($USER))
		{
			$USER = new CUser;
		}
		$ID = intval($ID);

		// ��������� � ��������� - ������������ ���� ��� ������ ���������
		if ($ID <= 0)
		{
			if (strlen($arFields["TITLE"]) <= 0)
			{
				$APPLICATION->ThrowException(GetMessage('SUP_ERROR_EMPTY_TITLE'));

				return false;
			}

			if (strlen($arFields["MESSAGE"]) <= 0)
			{
				$APPLICATION->ThrowException(GetMessage('SUP_ERROR_EMPTY_MESSAGE'));

				return false;
			}
		}

		$CHECK_RIGHTS = ($CHECK_RIGHTS == "Y") ? "Y" : "N";

		$bAdmin = $bSupportTeam = $bSupportClient = $bDemo = $bOwner = "N";
		if ($CHECK_RIGHTS == "Y")
		{
			$bAdmin = (CTszhTicket::IsAdmin()) ? "Y" : "N";
			$bSupportTeam = (CTszhTicket::IsSupportTeam()) ? "Y" : "N";
			$bSupportClient = (CTszhTicket::IsSupportClient()) ? "Y" : "N";
			$bDemo = (CTszhTicket::IsDemo()) ? "Y" : "N";
			$uid = intval($USER->GetID());
			if ($ID <= 0)
			{
				$bOwner = "Y";
			}
			else
			{
				$bOwner = CTszhTicket::IsOwner($ID, $uid) ? "Y" : "N";
			}
		}
		else
		{
			$bAdmin = $bSupportTeam = $bSupportClient = $bDemo = $bOwner = "Y";
			$uid = 0;
		}
		if ($bAdmin != "Y" && $bSupportTeam != "Y" && $bSupportClient != "Y")
		{
			return false;
		}

		// �������� ID ������� �����������
		if (strlen($arFields["SITE_ID"]) > 0)
		{
			$SITE_ID = $arFields["SITE_ID"];
		}
		elseif (strlen($arFields["SITE"]) > 0)
		{
			$SITE_ID = $arFields["SITE"];
		}
		elseif (strlen($arFields["LANG"]) > 0)
		{
			$SITE_ID = $arFields["LANG"];
		}  // ������������� �� ������ �������
		else
		{
			$SITE_ID = SITE_ID;
		}
		$arr = array(
			"CATEGORY" => "C",
			"CRITICALITY" => "K",
			"STATUS" => "S",
			"MARK" => "M",
			"SOURCE" => "SR",
			"MESSAGE_SOURCE" => "SR",
			"DIFFICULTY" => "D",
		);
		while (list($key, $value) = each($arr))
		{
			if (intval($arFields[$key . "_ID"]) <= 0 && strlen($arFields[$key . "_SID"]) > 0)
			{
				$z = CTszhTicketDictionary::GetBySID($arFields[$key . "_SID"], $value, $SITE_ID);
				$zr = $z->Fetch();
				$arFields[$key . "_ID"] = $zr["ID"];
			}
		}

		// �������� ���������������� email'�
		$arAdminEMails = CTszhTicket::GetAdminEmails();
		if (count($arAdminEMails) > 0)
		{
			$support_admin_email = implode(",", $arAdminEMails);
		}

		// ���� ������������ ��������� ��
		if ($ID > 0)
		{
			unset($arFields['NUMBER']);
			$close = ($arFields["CLOSE"] == "Y") ? $DB->GetNowFunction() : "null";

			// ���������� ���������� ������ ��������
			$arr = array(
				"RESPONSIBLE_USER_ID",
				//"SLA_ID",
				"CATEGORY_ID",
				"CRITICALITY_ID",
				"STATUS_ID",
				"MARK_ID",
				"DIFFICULTY_ID",
				"DATE_CLOSE",
			);
			$str = "ID";
			foreach ($arr as $s)
			{
				$str .= "," . $s;
			}
			$strSql = "SELECT " . $str . ", SITE_ID FROM b_tszh_ticket WHERE ID='$ID'";
			$z = $DB->Query($strSql, false, $err_mess . __LINE__);
			if ($zr = $z->Fetch())
			{
				$SITE_ID = $zr["SITE_ID"];
				if (intval($uid) == $zr["RESPONSIBLE_USER_ID"])
				{
					$bSupportTeam = "Y";
				}
				foreach ($arr as $key)
				{
					$arOldFields[$key] = $zr[$key];
				}
			}

			/******************************************
			 * update ������ ���� ��������������
			 ******************************************/

			$MODIFIED_MODULE_NAME = strlen($arFields["MODIFIED_MODULE_NAME"]) > 0 ? $DB->ForSql($arFields["MODIFIED_MODULE_NAME"], 255) : "citrus.tszhtickets";
			$MODIFIED_GUEST_ID = intval($_SESSION["SESS_GUEST_ID"]) > 0 ? intval($_SESSION["SESS_GUEST_ID"]) : "null";
			$MODIFIED_USER_ID = intval($uid) > 0 ? intval($uid) : "null";
			$OWNER_USER_ID = intval($arFields["OWNER_USER_ID"]) > 0 ? intval($arFields["OWNER_USER_ID"]) : "null";
			$OWNER_SID = strlen($arFields["OWNER_SID"]) > 0 ? "'" . $DB->ForSql($arFields["OWNER_SID"], 255) . "'" : "null";
			$SUPPORT_COMMENTS = strlen($arFields["SUPPORT_COMMENTS"]) > 0 ? "'" . $DB->ForSql($arFields["SUPPORT_COMMENTS"], 2000) . "'" : "null";
			$RESPONSIBLE_USER_ID = intval($arFields["RESPONSIBLE_USER_ID"]) > 0 ? intval($arFields["RESPONSIBLE_USER_ID"]) : "null";
			$HOLD_ON = ($arFields["HOLD_ON"] == "Y" ? "Y" : "N");

			$IS_GROUP_USER = 'N';
			if ($bAdmin == 'Y')
			{
				$IS_GROUP_USER = 'Y';
			}
			elseif ($CHECK_RIGHTS == 'Y' && ($bSupportClient == 'Y' || $bSupportTeam == 'Y'))
			{
				if ($bSupportTeam == 'Y')
				{
					$join_query = '(T.RESPONSIBLE_USER_ID IS NOT NULL AND T.RESPONSIBLE_USER_ID=O.USER_ID)';
				}
				else//if ($bSupportClient == 'Y')
				{
					$join_query = '(T.OWNER_USER_ID IS NOT NULL AND T.OWNER_USER_ID=O.USER_ID)';
				}
				$strSql = "SELECT 'x'
				FROM b_tszh_ticket T
				INNER JOIN b_tszh_ticket_user_ugroup O ON $join_query
				INNER JOIN b_tszh_ticket_user_ugroup C ON (O.GROUP_ID=C.GROUP_ID)
				INNER JOIN b_tszh_ticket_ugroups G ON (O.GROUP_ID=G.ID)
				WHERE T.ID='$ID' AND C.USER_ID='$uid' AND C.CAN_VIEW_GROUP_MESSAGES='Y' AND G.IS_TEAM_GROUP='$bSupportTeam'";
				$z = $DB->Query($strSql);
				if ($zr = $z->Fetch())
				{
					$IS_GROUP_USER = 'Y';
				}
			}

			$arFields_i = array(
				"TIMESTAMP_X" => $DB->GetNowFunction(),
				"DATE_CLOSE" => $close,
				//"SLA_ID"				=> intval($arFields["SLA_ID"]),
				"CATEGORY_ID" => intval($arFields["CATEGORY_ID"]),
				"STATUS_ID" => intval($arFields["STATUS_ID"]),
				"SOURCE_ID" => intval($arFields["SOURCE_ID"]),
				"DIFFICULTY_ID" => intval($arFields["DIFFICULTY_ID"]),
				"CRITICALITY_ID" => intval($arFields["CRITICALITY_ID"]),
				"OWNER_USER_ID" => $OWNER_USER_ID,
				"OWNER_SID" => $OWNER_SID,
				"RESPONSIBLE_USER_ID" => $RESPONSIBLE_USER_ID,
				"MODIFIED_USER_ID" => $MODIFIED_USER_ID,
				"HOLD_ON" => "'" . $HOLD_ON . "'",
				"MODIFIED_GUEST_ID" => $MODIFIED_GUEST_ID,
				"MODIFIED_MODULE_NAME" => "'" . $MODIFIED_MODULE_NAME . "'",
				"SUPPORT_COMMENTS" => $SUPPORT_COMMENTS,
			);
			if (
				intval($arFields["AUTO_CLOSE_DAYS"]) > 0 &&
				strlen($arFields["MESSAGE"]) > 0 &&
				$arFields["HIDDEN"] != "Y" && $arFields["NOT_CHANGE_STATUS"] != "Y"
			)
			{
				$arFields_i["AUTO_CLOSE_DAYS"] = intval($arFields["AUTO_CLOSE_DAYS"]);
			}
			elseif (intval($arFields["AUTO_CLOSE_DAYS"]) == 0)
			{
				$arFields_i["AUTO_CLOSE_DAYS"] = "null";
			}

			$arrUnset = array(
				"AUTO_CLOSE_DAYS",
				//"SLA_ID",
				"CATEGORY_ID",
				"STATUS_ID",
				"SOURCE_ID",
				"DIFFICULTY_ID",
				"CRITICALITY_ID",
				"OWNER_USER_ID",
				"OWNER_SID",
				"RESPONSIBLE_USER_ID",
				"SUPPORT_COMMENTS",
			);
			while (list($key, $value) = each($arrUnset))
			{
				if (!isset($arFields[$value]))
				{
					unset($arFields_i[$value]);
				}
			}
			if (!isset($arFields["CLOSE"]))
			{
				unset($arFields_i["DATE_CLOSE"]);
			}

			// ����
			if (strlen($arFields["SITE_ID"]) > 0)
			{
				$arFields_i["SITE_ID"] = "'" . $DB->ForSql($arFields["SITE_ID"], 2) . "'";
				$SITE_ID = $arFields["SITE_ID"];
			}
			elseif (strlen($arFields["LANG"]) > 0) // ������������� �� ������ �������
			{
				$arFields_i["SITE_ID"] = "'" . $DB->ForSql($arFields["LANG"], 2) . "'";
				$SITE_ID = $arFields["LANG"];
			}

			if (is_array($arOldFields) && is_array($arFields) && $arFields["CLOSE"] == "N" && strlen($arOldFields["DATE_CLOSE"]) > 0)
			{
				$arFields_i["REOPEN"] = "'Y'";
			}

			if (is_array($arFields_i) && count($arFields_i) > 0 && ($bSupportTeam == "Y" || $bAdmin == "Y"))
			{
				$rows1 = $DB->Update("b_tszh_ticket", $arFields_i, "WHERE ID='$ID'", $err_mess . __LINE__);

				// ���� ������� ������� � ����� ��
				if (strlen($arFields["IS_SPAM"]) > 0)
				{
					// ��������� ������� � �����
					CTszhTicket::MarkAsSpam($ID, $arFields["IS_SPAM"], $CHECK_RIGHTS);
				}
			}
			elseif (is_array($arFields_i) && count($arFields_i) > 0 && array_key_exists('DATE_CLOSE', $arFields_i))
			{
				$rows1 = $DB->Update("b_tszh_ticket", $arFields_i, "WHERE ID='$ID'", $err_mess . __LINE__);
			}


			/******************************************
			 * update ��������� �������
			 ******************************************/

			$arFields_i = array(
				"TIMESTAMP_X" => $DB->GetNowFunction(),
				"DATE_CLOSE" => $close,
				"CRITICALITY_ID" => intval($arFields["CRITICALITY_ID"]),
				"MARK_ID" => intval($arFields["MARK_ID"]),
				"MODIFIED_USER_ID" => $MODIFIED_USER_ID,
				"MODIFIED_GUEST_ID" => $MODIFIED_GUEST_ID,
				"MODIFIED_MODULE_NAME" => "'" . $MODIFIED_MODULE_NAME . "'",
			);
			$arrUnset = array(
				"MARK_ID",
				"CRITICALITY_ID",
			);
			while (list($key, $value) = each($arrUnset))
			{
				if (!isset($arFields[$value]))
				{
					unset($arFields_i[$value]);
				}
			}
			if (!isset($arFields["CLOSE"]))
			{
				unset($arFields_i["DATE_CLOSE"]);
			}

			if (is_array($arOldFields) && is_array($arFields) && $arFields["CLOSE"] == "N" && strlen($arOldFields["DATE_CLOSE"]) > 0)
			{
				$arFields_i["REOPEN"] = "'Y'";
			}

			if (is_array($arFields_i) && count($arFields_i) > 0 && ($bOwner == "Y" || $bSupportClient == "Y"))
			{
				$rows2 = $DB->Update("b_tszh_ticket", $arFields_i, "WHERE ID='$ID' and (OWNER_USER_ID='$uid' or CREATED_USER_ID='$uid' or '$CHECK_RIGHTS'='N' or '$IS_GROUP_USER'='Y')", $err_mess . __LINE__);
			}

			// ���� ��� ������ ����
			$arFields_log = array(
				"LOG" => "Y",
				"MESSAGE_CREATED_USER_ID" => $MODIFIED_USER_ID,
				"MESSAGE_CREATED_MODULE_NAME" => $MODIFIED_MODULE_NAME,
				"MESSAGE_CREATED_GUEST_ID" => $MODIFIED_GUEST_ID,
				"MESSAGE_SOURCE_ID" => intval($arFields["SOURCE_ID"]),
			);

			// ���� ���������� ��������� ����� ��
			if ($CHECK_RIGHTS == "Y")
			{
				// ���� update ������������ �� ������ ��
				if (intval($rows1) <= 0)
				{
					// ������� �� ������� �������� �������� �� ��� ����� ������ ������ ������������
					unset($arOldFields["RESPONSIBLE_USER_ID"]);
					//unset($arOldFields["SLA_ID"]);
					unset($arOldFields["CATEGORY_ID"]);
					unset($arOldFields["DIFFICULTY_ID"]);
					unset($arOldFields["STATUS_ID"]);
				}
				// ���� update ������ �� ������ ��
				if (intval($rows2) <= 0)
				{
					// ������� �� ������� �������� �������� �� ��� ����� ������ ������ �����
					unset($arOldFields["MARK_ID"]);
					//unset($arOldFields["CRITICALITY_ID"]);
				}
			}

			// ���� ��������� ���� �� updat'�� ��
			if (intval($rows1) > 0 || intval($rows2) > 0)
			{
				// ��������� ���������
				$arFields["MESSAGE_CREATED_MODULE_NAME"] = $arFields["MODIFIED_MODULE_NAME"];
				if (is_set($arFields, "IMAGE"))
				{
					$arFields["FILES"][] = $arFields["IMAGE"];
				}
				$MID = CTszhTicket::AddMessage($ID, $arFields, $arrFILES, $CHECK_RIGHTS);
				$MID = intval($MID);

				// ���� ��������� ��������� ��
				if (strlen($close) > 0 && $close != "null")
				{
					// ������ �������-��������������� � ������� ��������� ���������
					CTszhTicketReminder::Remove($ID);

					//$DB->Update("b_tszh_ticket_message",Array("NOT_CHANGE_STATUS" => "'N'"),"WHERE TICKET_ID='".$ID."' AND NOT_CHANGE_STATUS = 'Y' ",$err_mess.__LINE__);
				}

				/**********************************
				 * �������� ����������
				 **********************************/

				if (is_array($arOldFields) && is_array($arFields))
				{
					// ���������� ��� ����������
					if ($MID > 0)
					{
						if ($arFields["HIDDEN"] != "Y")
						{
							$arChange["MESSAGE"] = "Y";
						}
						else
						{
							$arChange["HIDDEN_MESSAGE"] = "Y";
						}
					}
					//echo "<pre>"; print_r($arOldFields); echo "</pre>";
					//echo "<pre>"; print_r($arFields); echo "</pre>";
					while (list($key, $value) = each($arOldFields))
					{
						if ($arFields["CLOSE"] == "Y" && strlen($arOldFields["DATE_CLOSE"]) <= 0)
						{
							$arChange["CLOSE"] = "Y";
						}
						elseif ($arFields["CLOSE"] == "N" && strlen($arOldFields["DATE_CLOSE"]) > 0)
						{
							$arChange["OPEN"] = "Y";
						}

						if (isset($arFields[$key]) && intval($value) != intval($arFields[$key]))
						{
							$arChange[$key] = "Y";
						}
					}

					// ������� ������� �������� ���������
					$z = CTszhTicket::GetByID($ID, $SITE_ID, "N");
					if ($zr = $z->Fetch())
					{
						// ���������� ������ ���� ��� ����������
						$rsSite = CSite::GetByID($zr["SITE_ID"]);
						$arSite = $rsSite->Fetch();
						$change = "";
						$change_log = "";
						IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhtickets/classes/general/messages.php", $arSite["LANGUAGE_ID"]);

						// ���������� ������ �� ������������ �����
						if (is_array($arrFILES) && count($arrFILES) > 0)
						{
							$FILES_LINKS = GetMessage("SUP_ATTACHED_FILES") . "\n";
							foreach ($arrFILES as $arFile)
							{
								$FILES_LINKS .= "http://" . $_SERVER["HTTP_HOST"] . "/bitrix/tools/tszh_ticket_show_file.php?hash=" . $arFile["HASH"] . "&action=download&lang=" . $arSite["LANGUAGE_ID"] . "\n";
							}
							if (strlen($FILES_LINKS) > 0)
							{
								$FILES_LINKS = "\n\n" . $FILES_LINKS;
							}
						}

						// �������������� ���������� � ����� ���������
						$MESSAGE = PrepareTxtForEmail($arFields["MESSAGE"], $arSite["LANGUAGE_ID"], false, false);
						if (strlen($MESSAGE) > 0)
						{
							if (strlen($FILES_LINKS) > 0)
							{
								$MESSAGE = "\n" . $MESSAGE . "\n";
							}
							else
							{
								$MESSAGE = "\n\n" . $MESSAGE . "\n";
							}
						}

						// ���������� email ������
						$arrOwnerEMail = array($zr["OWNER_EMAIL"]);
						$arrEmails = explode(",", $zr["OWNER_SID"]);
						if (is_array($arrEmails) && count($arrEmails) > 0)
						{
							foreach ($arrEmails as $email)
							{
								$email = trim($email);
								if (strlen($email) > 0)
								{
									preg_match_all("#[<\[\(](.*?)[>\]\)]#i" . BX_UTF_PCRE_MODIFIER, $email, $arr);
									if (is_array($arr[1]) && count($arr[1]) > 0)
									{
										foreach ($arr[1] as $email)
										{
											$email = trim($email);
											if (strlen($email) > 0 && !in_array($email, $arrOwnerEMail) && check_email($email))
											{
												$arrOwnerEMail[] = $email;
											}
										}
									}
									elseif (!in_array($email, $arrOwnerEMail) && check_email($email))
									{
										$arrOwnerEMail[] = $email;
									}
								}
							}
						}
						TrimArr($arrOwnerEMail);
						$owner_email = implode(", ", $arrOwnerEMail);

						// ���������� email ������������
						$support_email = $zr["RESPONSIBLE_EMAIL"];
						if (strlen($support_email) <= 0)
						{
							$support_email = $support_admin_email;
						}
						if (strlen($support_email) <= 0)
						{
							$support_email = COption::GetOptionString("main", "email_from", "");
						}

						// ������ ���������������� ������ �� ������� #SUPPORT_ADMIN_EMAIL#
						$arr = explode(",", $support_email);
						$arr = array_unique($arr);
						$support_email = implode(",", $arr);
						if (is_array($arr) && count($arr) > 0)
						{
							foreach ($arr as $email)
							{
								unset($arAdminEMails[$email]);
							}
						}
						$support_admin_email = implode(",", $arAdminEMails);

						if (array_key_exists('PUBLIC_EDIT_URL', $arFields) && strlen($arFields['PUBLIC_EDIT_URL']) > 0)
						{
							$public_edit_url = $arFields['PUBLIC_EDIT_URL'];
						}
						else
						{
							$public_edit_url = COption::GetOptionString("citrus.tszhtickets", "SUPPORT_DIR");
							$public_edit_url = str_replace("#LANG_DIR#", $arSite["DIR"], $public_edit_url); // �������������
							$public_edit_url = str_replace("#SITE_DIR#", $arSite["DIR"], $public_edit_url);
							$public_edit_url = str_replace("\\", "/", $public_edit_url);
							$public_edit_url = str_replace("//", "/", $public_edit_url);
							$public_edit_url = TrimEx($public_edit_url, "/");
							$public_edit_url = "/" . $public_edit_url . "/tszh_ticket_edit.php";
						}

						$SUPPORT_COMMENTS = PrepareTxtForEmail($zr["SUPPORT_COMMENTS"], $arSite["LANGUAGE_ID"]);
						if (strlen($SUPPORT_COMMENTS) > 0)
						{
							$SUPPORT_COMMENTS = "\n\n" . $SUPPORT_COMMENTS . "\n";
						}

						// #OWNER_TEXT# = #SOURCE##OWNER_SID##OWNER_TEXT#
						$SOURCE_NAME = strlen($zr["SOURCE_NAME"]) <= 0 ? "" : "[" . $zr["SOURCE_NAME"] . "] ";
						if (intval($zr["OWNER_USER_ID"]) > 0 || strlen(trim($zr["OWNER_LOGIN"])) > 0)
						{
							$OWNER_TEXT = "[" . $zr["OWNER_USER_ID"] . "] (" . $zr["OWNER_LOGIN"] . ") " . $zr["OWNER_NAME"];
							if (strlen(trim($zr["OWNER_SID"])) > 0)
							{
								$OWNER_TEXT = " / " . $OWNER_TEXT;
							}
							if (intval($zr["OWNER_USER_ID"]) > 0)
							{
								if (CTszhTicket::IsSupportTeam($zr["OWNER_USER_ID"]) || CTszhTicket::IsAdmin($zr["OWNER_USER_ID"]))
								{
									$OWNER_TEXT .= " " . GetMessage("SUP_TECHSUPPORT_HINT");
								}
							}
						}

						// #CREATED_TEXT# = #CREATED_TEXT##CREATED_MODULE_NAME#
						if ($zr["CREATED_MODULE_NAME"] == "citrus.tszhtickets" && strlen($zr["CREATED_MODULE_NAME"]) > 0)
						{
							$CREATED_MODULE_NAME = "";
							if (intval($zr["CREATED_USER_ID"]) > 0)
							{
								$CREATED_TEXT = "[" . $zr["CREATED_USER_ID"] . "] (" . $zr["CREATED_LOGIN"] . ") " . $zr["CREATED_NAME"];
								if (CTszhTicket::IsSupportTeam($zr["CREATED_USER_ID"]) || CTszhTicket::IsAdmin($zr["CREATED_USER_ID"]))
								{
									$CREATED_TEXT .= " " . GetMessage("SUP_TECHSUPPORT_HINT");
								}
							}
						}
						else
						{
							$CREATED_MODULE_NAME = "[" . $zr["CREATED_MODULE_NAME"] . "]";
						}

						// #MODIFIED_TEXT# = #MODIFIED_TEXT##MODIFIED_MODULE_NAME#
						if ($zr["MODIFIED_MODULE_NAME"] == "citrus.tszhtickets" && strlen($zr["MODIFIED_MODULE_NAME"]) > 0)
						{
							$MODIFIED_MODULE_NAME = "";
							if (intval($zr["MODIFIED_USER_ID"]) > 0)
							{
								$MODIFIED_TEXT = "[" . $zr["MODIFIED_USER_ID"] . "] (" . $zr["MODIFIED_LOGIN"] . ") " . $zr["MODIFIED_NAME"];
								if (CTszhTicket::IsSupportTeam($zr["MODIFIED_USER_ID"]) || CTszhTicket::IsAdmin($zr["MODIFIED_USER_ID"]))
								{
									$MODIFIED_TEXT .= " " . GetMessage("SUP_TECHSUPPORT_HINT");
								}
							}
						}
						else
						{
							$MODIFIED_MODULE_NAME = "[" . $zr["MODIFIED_MODULE_NAME"] . "]";
						}

						// #MESSAGE_SOURCE##MESSAGE_AUTHOR_SID##MESSAGE_AUTHOR_TEXT#
						$arSource = array();
						if ($rsSource = CTszhTicketDictionary::GetByID($arFields["MESSAGE_SOURCE_ID"]))
						{
							$arSource = $rsSource->Fetch();
						}
						$MESSAGE_SOURCE_NAME = strlen($arSource["NAME"]) <= 0 ? "" : "[" . $arSource["NAME"] . "] ";

						if ((strlen(trim($arFields["MESSAGE_AUTHOR_SID"])) > 0 || intval($arFields["MESSAGE_AUTHOR_USER_ID"]) > 0) && $bSupportTeam == "Y")
						{
							$MESSAGE_AUTHOR_ID = intval($arFields["MESSAGE_AUTHOR_USER_ID"]);
							$MESSAGE_AUTHOR_SID = $arFields["MESSAGE_AUTHOR_SID"];
						}
						else
						{
							$MESSAGE_AUTHOR_ID = intval($uid);
							$MESSAGE_AUTHOR_SID = "null";
						}

						// #MESSAGE_AUTHOR_TEXT#
						$arMA = array();
						if ($rsMA = CUser::GetByID($MESSAGE_AUTHOR_ID))
						{
							$arMA = $rsMA->Fetch();
						}
						if (intval($MESSAGE_AUTHOR_ID) > 0 || strlen(trim($arMA["LOGIN"])) > 0)
						{
							$MESSAGE_AUTHOR_TEXT = "[" . $MESSAGE_AUTHOR_ID . "] (" . $arMA["LOGIN"] . ") " . $arMA["NAME"] . " " . $arMA["LAST_NAME"];
							if (strlen(trim($arFields["MESSAGE_AUTHOR_SID"])) > 0)
							{
								$MESSAGE_AUTHOR_TEXT = " / " . $MESSAGE_AUTHOR_TEXT;
							}
							if (intval($MESSAGE_AUTHOR_ID) > 0)
							{
								if (CTszhTicket::IsSupportTeam($MESSAGE_AUTHOR_ID) || CTszhTicket::IsAdmin($MESSAGE_AUTHOR_ID))
								{
									$MESSAGE_AUTHOR_TEXT .= " " . GetMessage("SUP_TECHSUPPORT_HINT");
								}
							}
						}

						// #RESPONSIBLE_TEXT#
						if (intval($zr["RESPONSIBLE_USER_ID"]) > 0)
						{
							$RESPONSIBLE_TEXT = "[" . $zr["RESPONSIBLE_USER_ID"] . "] (" . $zr["RESPONSIBLE_LOGIN"] . ") " . $zr["RESPONSIBLE_NAME"];
							if (CTszhTicket::IsSupportTeam($zr["RESPONSIBLE_USER_ID"]) || CTszhTicket::IsAdmin($zr["RESPONSIBLE_USER_ID"]))
							{
								$RESPONSIBLE_TEXT .= " " . GetMessage("SUP_TECHSUPPORT_HINT");
							}
						}

						$spam_mark = "";
						if (strlen($zr["IS_SPAM"]) > 0)
						{
							if ($zr["IS_SPAM"] == "Y")
							{
								$spam_mark = "\n" . GetMessage("SUP_EXACTLY_SPAM") . "\n";
							}
							else
							{
								$spam_mark = "\n" . GetMessage("SUP_POSSIBLE_SPAM") . "\n";
							}
						}

						$line1 = str_repeat("=", 23);
						$line2 = str_repeat("=", 34);
						$MESSAGE_HEADER = $line1 . " " . GetMessage("SUP_MAIL_MESSAGE") . " " . $line2;

						//echo "<pre>"; print_r($arChange); echo "</pre>";
						if (is_array($arChange) && count($arChange) > 0)
						{
							while (list($key, $value) = each($arChange))
							{
								if ($value == "Y")
								{
									switch ($key)
									{
										case "CLOSE":
											$change .= GetMessage("SUP_REQUEST_CLOSED") . "\n";
											$change_log .= "<li>" . GetMessage("SUP_REQUEST_CLOSED_LOG");
											break;
										case "OPEN":
											$change .= GetMessage("SUP_REQUEST_OPENED") . "\n";
											$change_log .= "<li>" . GetMessage("SUP_REQUEST_OPENED_LOG");
											break;
										case "RESPONSIBLE_USER_ID":
											$change .= GetMessage("SUP_RESPONSIBLE_CHANGED") . "\n";
											$change_log .= "<li>" . htmlspecialcharsEx(str_replace("#VALUE#", $RESPONSIBLE_TEXT, GetMessage("SUP_RESPONSIBLE_CHANGED_LOG")));
											break;
										case "CATEGORY_ID":
											$change .= GetMessage("SUP_CATEGORY_CHANGED") . "\n";
											$change_log .= "<li>" . htmlspecialcharsEx(str_replace("#VALUE#", $zr["CATEGORY_NAME"], GetMessage("SUP_CATEGORY_CHANGED_LOG")));
											break;
										case "CRITICALITY_ID":
											$change .= GetMessage("SUP_CRITICALITY_CHANGED") . "\n";
											$change_log .= "<li>" . htmlspecialcharsEx(str_replace("#VALUE#", $zr["CRITICALITY_NAME"], GetMessage("SUP_CRITICALITY_CHANGED_LOG")));
											break;
										case "STATUS_ID":
											$change .= GetMessage("SUP_STATUS_CHANGED") . "\n";
											$change_log .= "<li>" . htmlspecialcharsEx(str_replace("#VALUE#", $zr["STATUS_NAME"], GetMessage("SUP_STATUS_CHANGED_LOG")));
											break;

										case "DIFFICULTY_ID":
											$change .= GetMessage("SUP_DIFFICULTY_CHANGED") . "\n";
											$change_log .= "<li>" . htmlspecialcharsEx(str_replace("#VALUE#", $zr["DIFFICULTY_NAME"], GetMessage("SUP_DIFFICULTY_CHANGED_LOG")));
											break;

										case "MARK_ID":
											$change .= GetMessage("SUP_MARK_CHANGED") . "\n";
											$change_log .= "<li>" . htmlspecialcharsEx(str_replace("#VALUE#", $zr["MARK_NAME"], GetMessage("SUP_MARK_CHANGED_LOG")));
											break;
										/** case "SLA_ID":
										 * $change .= GetMessage("SUP_SLA_CHANGED")."\n";
										 * $change_log .= "<li>".htmlspecialcharsEx(str_replace("#VALUE#", $zr["SLA_NAME"], GetMessage("SUP_SLA_CHANGED_LOG")));
										 * break;**/
										case "MESSAGE":
											$change .= GetMessage("SUP_NEW_MESSAGE") . "\n";
											break;
										case "HIDDEN_MESSAGE":
											$change_hidden .= GetMessage("SUP_NEW_HIDDEN_MESSAGE") . "\n";
											$line1 = str_repeat("=", 20);
											$line2 = str_repeat("=", 30);
											$MESSAGE_HEADER = $line1 . " " . GetMessage("SUP_MAIL_HIDDEN_MESSAGE") . " " . $line2;
											break;
									}
								}
							}
						}

						$MESSAGE_FOOTER = str_repeat("=", strlen($MESSAGE_HEADER));

						// ������� ��������� � ���
						if (strlen($change_log) > 0)
						{
							$arFields_log["MESSAGE"] = $change_log;
							if ($arChange['STATUS_ID'] == 'Y' && IntVal($arFields['STATUS_ID']) > 0)
							{
								$arFields_log['STATUS_ID'] = IntVal($arFields['STATUS_ID']);
							}
							//echo "<pre>"; print_r($arFields_log); echo "</pre>";
							CTszhTicket::AddMessage($ID, $arFields_log, $v, "N");
						}

						if (
							/** (is_set($arChange, 'SLA_ID') && $arChange['SLA_ID'] == 'Y') ||**/
						(is_set($arChange, 'OPEN') && $arChange['OPEN'] == 'Y')
						)
						{
							CTszhTicketReminder::Update($ID, true);
						}

						$arEventFields = array(
							"ID" => $ID,
							"NUMBER" => $zr['NUMBER'],
							"LANGUAGE" => $arSite["LANGUAGE_ID"],
							"LANGUAGE_ID" => $arSite["LANGUAGE_ID"],
							"WHAT_CHANGE" => $change,
							"DATE_CREATE" => $zr["DATE_CREATE"],
							"TIMESTAMP" => $zr["TIMESTAMP_X"],
							"DATE_CLOSE" => $zr["DATE_CLOSE"],
							"TITLE" => $zr["TITLE"],
							"STATUS" => $zr["STATUS_NAME"],
							"DIFFICULTY" => $zr["DIFFICULTY_NAME"],
							"CATEGORY" => $zr["CATEGORY_NAME"],
							"CRITICALITY" => $zr["CRITICALITY_NAME"],
							"RATE" => $zr["MARK_NAME"],
							//"SLA"						=> $zr["SLA_NAME"],
							"SOURCE" => $SOURCE_NAME,
							"MESSAGES_AMOUNT" => $zr["MESSAGES"],
							"SPAM_MARK" => $spam_mark,
							"ADMIN_EDIT_URL" => "/bitrix/admin/ticket_edit.php",
							"PUBLIC_EDIT_URL" => $public_edit_url,

							"OWNER_EMAIL" => TrimEx($owner_email, ","),
							"OWNER_USER_ID" => $zr["OWNER_USER_ID"],
							"OWNER_USER_NAME" => $zr["OWNER_NAME"],
							"OWNER_USER_LOGIN" => $zr["OWNER_LOGIN"],
							"OWNER_USER_EMAIL" => $zr["OWNER_EMAIL"],
							"OWNER_TEXT" => $OWNER_TEXT,
							"OWNER_SID" => $zr["OWNER_SID"],

							"SUPPORT_EMAIL" => TrimEx($support_email, ","),
							"RESPONSIBLE_USER_ID" => $zr["RESPONSIBLE_USER_ID"],
							"RESPONSIBLE_USER_NAME" => $zr["RESPONSIBLE_NAME"],
							"RESPONSIBLE_USER_LOGIN" => $zr["RESPONSIBLE_LOGIN"],
							"RESPONSIBLE_USER_EMAIL" => $zr["RESPONSIBLE_EMAIL"],
							"RESPONSIBLE_TEXT" => $RESPONSIBLE_TEXT,
							"SUPPORT_ADMIN_EMAIL" => TrimEx($support_admin_email, ","),

							"CREATED_USER_ID" => $zr["CREATED_USER_ID"],
							"CREATED_USER_LOGIN" => $zr["CREATED_LOGIN"],
							"CREATED_USER_EMAIL" => $zr["CREATED_EMAIL"],
							"CREATED_USER_NAME" => $zr["CREATED_NAME"],
							"CREATED_MODULE_NAME" => $CREATED_MODULE_NAME,
							"CREATED_TEXT" => $CREATED_TEXT,

							"MODIFIED_USER_ID" => $zr["MODIFIED_USER_ID"],
							"MODIFIED_USER_LOGIN" => $zr["MODIFIED_LOGIN"],
							"MODIFIED_USER_EMAIL" => $zr["MODIFIED_EMAIL"],
							"MODIFIED_USER_NAME" => $zr["MODIFIED_NAME"],
							"MODIFIED_MODULE_NAME" => $MODIFIED_MODULE_NAME,
							"MODIFIED_TEXT" => $MODIFIED_TEXT,

							"MESSAGE_AUTHOR_USER_ID" => $MESSAGE_AUTHOR_ID,
							"MESSAGE_AUTHOR_USER_NAME" => $arMA["NAME"] . " " . $arMA["LAST_NAME"],
							"MESSAGE_AUTHOR_USER_LOGIN" => $arMA["LOGIN"],
							"MESSAGE_AUTHOR_USER_EMAIL" => $arMA["EMAIL"],
							"MESSAGE_AUTHOR_TEXT" => $MESSAGE_AUTHOR_TEXT,
							"MESSAGE_AUTHOR_SID" => $arFields["MESSAGE_AUTHOR_SID"],
							"MESSAGE_SOURCE" => $MESSAGE_SOURCE_NAME,
							"MESSAGE_HEADER" => $MESSAGE_HEADER,
							"MESSAGE_BODY" => $MESSAGE,
							"MESSAGE_FOOTER" => $MESSAGE_FOOTER,
							"FILES_LINKS" => $FILES_LINKS,
							"IMAGE_LINK" => $FILES_LINKS,  // ������ ��������

							"SUPPORT_COMMENTS" => $SUPPORT_COMMENTS,
						);

						$arEventFields_support = $arEventFields;
						$arEventFields_author = $arEventFields;

						if ($SEND_EMAIL_TO_TECHSUPPORT == "Y" && (strlen($change) > 0 || strlen($change_hidden) > 0))
						{
							$arEventFields_support["WHAT_CHANGE"] .= $change_hidden;
							//echo "<pre>TICKET_CHANGE_FOR_TECHSUPPORT: "; print_r($arEventFields_support); echo "</pre>";
							$arEventFields_support = CTszhTicket::ExecuteEvents('OnBeforeSendMailToSupport', $arEventFields_support, false);
							if ($arEventFields_support)
							{
								CEvent::Send("TSZH_TICKET_CHANGE_FOR_TECHSUPPORT", $arSite["ID"], $arEventFields_support);
							}
						}

						if ($SEND_EMAIL_TO_AUTHOR == "Y" && strlen($change) > 0)
						{
							if ($arFields["HIDDEN"] == "Y" && (strlen($arEventFields_author["MESSAGE_BODY"]) > 0 ||
							                                   strlen($arEventFields_author["IMAGE_LINK"]) > 0))
							{
								$arrUnsetHidden = array(
									"MESSAGE_BODY",
									"IMAGE_LINK",
								);
								foreach ($arrUnsetHidden as $value)
								{
									$arEventFields_author[$value] = "";
								}
							}
							$event_type = "TSZH_TICKET_CHANGE_BY_AUTHOR_FOR_AUTHOR";
							if (CTszhTicket::IsSupportTeam($MESSAGE_AUTHOR_ID) || CTszhTicket::IsAdmin($MESSAGE_AUTHOR_ID))
							{
								$event_type = "TSZH_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR";
							}
							//echo "<pre>".$event_type.": "; print_r($arEventFields_author); echo "</pre>";
							$arEventFields_author = CTszhTicket::ExecuteEvents('OnBeforeSendMailToAuthor', $arEventFields_author, false);
							if ($arEventFields_author)
							{
								CEvent::Send($event_type, $arSite["ID"], $arEventFields_author);
							}
						}
					}
				}
				$arFields['ID'] = $ID;
				CTszhTicket::ExecuteEvents('OnAfterTicketUpdate', $arFields, false);
			}
		}
		else
		{
			/* ����� ��������� */
			$ticketNumber = COption::GetOptionString(CTszhTicket::ModuleID(), "CURRENT_TICKET_NUMBER");
			$ticketYear = COption::GetOptionString(CTszhTicket::ModuleID(), "CURRENT_TICKET_YEAR");

			if (IntVal($ticketNumber) <= 0 || $ticketYear != date('Y'))
			{
				$ticketNumber = 1;
				$ticketYear = date('Y');
				COption::SetOptionString(CTszhTicket::ModuleID(), "CURRENT_TICKET_YEAR", $ticketYear);
			}
			$arFields['NUMBER'] = $ticketNumber++;
			COption::SetOptionString(CTszhTicket::ModuleID(), "CURRENT_TICKET_NUMBER", $ticketNumber);

			if ((strlen(trim($arFields["OWNER_SID"])) > 0 || intval($arFields["OWNER_USER_ID"]) > 0) && ($bSupportTeam == "Y" || $bAdmin == "Y"))
			{
				$OWNER_USER_ID = intval($arFields["OWNER_USER_ID"]) > 0 ? intval($arFields["OWNER_USER_ID"]) : "null";
				$OWNER_SID = "'" . $DB->ForSql($arFields["OWNER_SID"], 2000) . "'";
				$OWNER_GUEST_ID = "null";
			}
			else
			{
				$OWNER_USER_ID = intval($uid) > 0 ? intval($uid) : "null";
				$OWNER_SID = "null";
				$OWNER_GUEST_ID = intval($_SESSION["SESS_GUEST_ID"]) > 0 ? intval($_SESSION["SESS_GUEST_ID"]) : "null";
			}

			$CREATED_MODULE_NAME = (strlen($arFields["CREATED_MODULE_NAME"]) > 0) ? $DB->ForSql($arFields["CREATED_MODULE_NAME"], 255) : "citrus.tszhtickets";

			$CATEGORY_ID = (intval($arFields["CATEGORY_ID"]) > 0 ? intval($arFields["CATEGORY_ID"]) : "null");
			$STATUS_ID = (intval($arFields["STATUS_ID"]) > 0 ? intval($arFields["STATUS_ID"]) : "null");
			$DIFFICULTY_ID = (intval($arFields["DIFFICULTY_ID"]) > 0 ? intval($arFields["DIFFICULTY_ID"]) : "null");
			$CRITICALITY_ID = (intval($arFields["CRITICALITY_ID"]) > 0 ? intval($arFields["CRITICALITY_ID"]) : "null");
			$SOURCE_ID = (intval($arFields["SOURCE_ID"]) > 0 ? intval($arFields["SOURCE_ID"]) : "null");
			$MODIFIED_GUEST_ID = (intval($_SESSION["SESS_GUEST_ID"]) > 0 ? intval($_SESSION["SESS_GUEST_ID"]) : "null");
			$CREATED_USER_ID = (intval($uid) > 0 ? intval($uid) : "null");
			$HOLD_ON = ($arFields["HOLD_ON"] == "Y" ? "Y" : "N");

			if (strlen($arFields["SITE_ID"]) > 0)
			{
				$SITE_ID = $arFields["SITE_ID"];
			}
			elseif (strlen($arFields["SITE"]) > 0)
			{
				$SITE_ID = $arFields["SITE"];
			}
			elseif (strlen($arFields["LANG"]) > 0)
			{
				$SITE_ID = $arFields["LANG"];
			}  // ������������� �� ������ �������
			else
			{
				$SITE_ID = SITE_ID;
			}

			// ���� ��� ������ ���������
			$arFields_i = array(
				"SITE_ID" => "'" . $DB->ForSql($SITE_ID, 2) . "'",
				"DATE_CREATE" => $DB->GetNowFunction(),
				"LAST_MESSAGE_DATE" => $DB->GetNowFunction(),
				"DAY_CREATE" => $DB->CurrentDateFunction(),
				"TIMESTAMP_X" => $DB->GetNowFunction(),
				"TITLE" => "'" . $DB->ForSql($arFields["TITLE"], 255) . "'",
				"CATEGORY_ID" => $CATEGORY_ID,
				"STATUS_ID" => $STATUS_ID,
				"DIFFICULTY_ID" => $DIFFICULTY_ID,
				"CRITICALITY_ID" => $CRITICALITY_ID,
				"SOURCE_ID" => $SOURCE_ID,
				"MESSAGES" => 0,
				"HOLD_ON" => "'" . $HOLD_ON . "'",
				"OWNER_USER_ID" => $OWNER_USER_ID,
				"OWNER_SID" => $OWNER_SID,
				"OWNER_GUEST_ID" => $OWNER_GUEST_ID,
				"MODIFIED_USER_ID" => $CREATED_USER_ID,
				"MODIFIED_GUEST_ID" => $MODIFIED_GUEST_ID,
				"MODIFIED_MODULE_NAME" => "'" . $CREATED_MODULE_NAME . "'",
				"CREATED_USER_ID" => $CREATED_USER_ID,
				"CREATED_MODULE_NAME" => "'" . $CREATED_MODULE_NAME . "'",
				"CREATED_GUEST_ID" => $MODIFIED_GUEST_ID,
				"NUMBER" => $arFields['NUMBER'],
			);

			// ���� ��� ������ ����
			$arFields_log = array(
				"LOG" => "Y",
				"MESSAGE_CREATED_USER_ID" => $CREATED_USER_ID,
				"MESSAGE_CREATED_MODULE_NAME" => $CREATED_MODULE_NAME,
				"MESSAGE_CREATED_GUEST_ID" => $MODIFIED_GUEST_ID,
				"MESSAGE_SOURCE_ID" => $SOURCE_ID,
			);

			// ���� ��������� ��������� ����������� ������������, ��������������� ��� ���� �������������
			if ($bSupportTeam == "Y" || $bAdmin == "Y" || $bDemo == "Y")
			{
				$arFields_i["SUPPORT_COMMENTS"] = (strlen($arFields["SUPPORT_COMMENTS"]) > 0) ? "'" . $DB->ForSql($arFields["SUPPORT_COMMENTS"], 2000) . "'" : "null";

				// ���� ����� ����� ������������� ��
				if (intval($arFields["RESPONSIBLE_USER_ID"]) > 0)
				{
					// ������������� ��������� ��������������
					$RESPONSIBLE_USER_ID = intval($arFields["RESPONSIBLE_USER_ID"]);
					$arFields_i["RESPONSIBLE_USER_ID"] = "'" . $RESPONSIBLE_USER_ID . "'";
				}
			}
			else
			{
				unset($arFields["RESPONSIBLE_USER_ID"]);
			}

			/*
			������� �������������� ������� � �������������� � ����������� ��
				1) ���������
				2) �����������
				3) ���������
			*/
			$strSql = "
				SELECT ID, C_TYPE, RESPONSIBLE_USER_ID, EVENT1, EVENT2, EVENT3
				FROM b_tszh_ticket_dictionary
				WHERE
					(ID='" . intval($arFields["CATEGORY_ID"]) . "'		and C_TYPE='C') or
					(ID='" . intval($arFields["CRITICALITY_ID"]) . "'	and C_TYPE='K') or
					(ID='" . intval($arFields["SOURCE_ID"]) . "'		and C_TYPE='SR')
				ORDER BY
					C_TYPE
				";
			$z = $DB->Query($strSql, false, $err_mess . __LINE__);
			while ($zr = $z->Fetch())
			{
				// ����
				//    1) ������������� ��������� � �����������
				//    2) �� ��� ��� �� �� ��� ���������
				//    3) �� ��� ����� ���� ������������� ������� �� ��� �����
				if (intval($zr["RESPONSIBLE_USER_ID"]) > 0 && intval($RESPONSIBLE_USER_ID) <= 0 && !is_set($arFields, "RESPONSIBLE_USER_ID"))
				{
					$RESPONSIBLE_USER_ID = intval($zr["RESPONSIBLE_USER_ID"]);
					if (CTszhTicket::IsSupportTeam($RESPONSIBLE_USER_ID) || CTszhTicket::IsAdmin($RESPONSIBLE_USER_ID))
					{
						$arFields_i["RESPONSIBLE_USER_ID"] = "'" . $RESPONSIBLE_USER_ID . "'";
					}
				}
				if ($zr["C_TYPE"] == "C")
				{
					$T_EVENT1 = trim($zr["EVENT1"]);
					$T_EVENT2 = trim($zr["EVENT2"]);
					$T_EVENT3 = trim($zr["EVENT3"]);
					$category_set = "Y";
					break;
				}
			}

			// ���� ������������� ���� �� ��������� ��
			if (!is_set($arFields, "RESPONSIBLE_USER_ID"))
			{
				// ������������� �� �������� ������
				if (intval($RESPONSIBLE_USER_ID) <= 0)
				{
					// ����� �� �������� ������ �������������� �� ���������
					$RESPONSIBLE_USER_ID = intval(COption::GetOptionString("citrus.tszhtickets", "DEFAULT_RESPONSIBLE_ID"));
					if ($RESPONSIBLE_USER_ID > 0)
					{
						$arFields_i["RESPONSIBLE_USER_ID"] = "'" . $RESPONSIBLE_USER_ID . "'";
					}
				}
			}

			// ������� ���������
			$ID = $DB->Insert("b_tszh_ticket", $arFields_i, $err_mess . __LINE__);
			if ($ID > 0)
			{
				$arFields["MESSAGE_AUTHOR_SID"] = $arFields["OWNER_SID"];
				$arFields["MESSAGE_AUTHOR_USER_ID"] = $arFields["OWNER_USER_ID"];
				$arFields["MESSAGE_CREATED_MODULE_NAME"] = $arFields["CREATED_MODULE_NAME"];
				$arFields["MESSAGE_SOURCE_ID"] = $arFields["SOURCE_ID"];
				$arFields["HIDDEN"] = "N";
				$arFields["LOG"] = "N";
				if (is_set($arFields, "IMAGE"))
				{
					$arFields["FILES"][] = $arFields["IMAGE"];
				}
				$MID = CTszhTicket::AddMessage($ID, $arFields, $arrFILES, $CHECK_RIGHTS);
				$MID = intval($MID);

				if (intval($MID) > 0)
				{
					// ���� ������� ������� � ����� ��
					if (strlen($arFields["IS_SPAM"]) > 0)
					{
						// ��������� ������� � �����
						CTszhTicket::MarkAsSpam($ID, $arFields["IS_SPAM"], $CHECK_RIGHTS);
					}

					$z = CTszhTicket::GetByID($ID, $SITE_ID, "N");
					if ($zr = $z->Fetch())
					{
						$rsSite = CSite::GetByID($zr["SITE_ID"]);
						$arSite = $rsSite->Fetch();

						IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszhtickets/classes/general/messages.php", $arSite["LANGUAGE_ID"]);

						// ���������� ������ �� ������������ �����
						if (is_array($arrFILES) && count($arrFILES) > 0)
						{
							$FILES_LINKS = GetMessage("SUP_ATTACHED_FILES") . "\n";
							foreach ($arrFILES as $arFile)
							{
								$FILES_LINKS .= "http://" . $_SERVER["HTTP_HOST"] . "/bitrix/tools/tszh_ticket_show_file.php?hash=" . $arFile["HASH"] . "&action=download&lang=" . $arSite["LANGUAGE_ID"] . "\n";
							}
							if (strlen($FILES_LINKS))
							{
								$FILES_LINKS .= "\n";
							}
						}

						$MESSAGE = PrepareTxtForEmail($arFields["MESSAGE"], $arSite["LANGUAGE_ID"], false, false);

						// ���������� email ������
						$arrOwnerEMail = array($zr["OWNER_EMAIL"]);
						$arrEmails = explode(",", $zr["OWNER_SID"]);
						if (is_array($arrEmails) && count($arrEmails) > 0)
						{
							foreach ($arrEmails as $email)
							{
								$email = trim($email);
								if (strlen($email) > 0)
								{
									preg_match_all("#[<\[\(](.*?)[>\]\)]#i" . BX_UTF_PCRE_MODIFIER, $email, $arr);
									if (is_array($arr[1]) && count($arr[1]) > 0)
									{
										foreach ($arr[1] as $email)
										{
											$email = trim($email);
											if (strlen($email) > 0 && !in_array($email, $arrOwnerEMail) && check_email($email))
											{
												$arrOwnerEMail[] = $email;
											}
										}
									}
									elseif (!in_array($email, $arrOwnerEMail) && check_email($email))
									{
										$arrOwnerEMail[] = $email;
									}
								}
							}
						}
						TrimArr($arrOwnerEMail);
						$owner_email = implode(", ", $arrOwnerEMail);

						// ���������� email ������������
						$support_email = $zr["RESPONSIBLE_EMAIL"];
						if (strlen($support_email) <= 0)
						{
							$support_email = $support_admin_email;
						}
						if (strlen($support_email) <= 0)
						{
							$support_email = COption::GetOptionString("main", "email_from", "");
						}

						// ������ ���������������� ������ �� ������� #SUPPORT_ADMIN_EMAIL#
						$arr = explode(",", $support_email);
						$arr = array_unique($arr);
						$support_email = implode(",", $arr);
						if (is_array($arr) && count($arr) > 0)
						{
							foreach ($arr as $email)
							{
								unset($arAdminEMails[$email]);
							}
						}
						$support_admin_email = implode(",", $arAdminEMails);

						if (array_key_exists('PUBLIC_EDIT_URL', $arFields) && strlen($arFields['PUBLIC_EDIT_URL']) > 0)
						{
							$public_edit_url = $arFields['PUBLIC_EDIT_URL'];
						}
						else
						{
							$public_edit_url = COption::GetOptionString("citrus.tszhtickets", "SUPPORT_DIR");
							$public_edit_url = str_replace("#LANG_DIR#", $arSite["DIR"], $public_edit_url); // �������������
							$public_edit_url = str_replace("#SITE_DIR#", $arSite["DIR"], $public_edit_url);
							$public_edit_url = str_replace("\\", "/", $public_edit_url);
							$public_edit_url = str_replace("//", "/", $public_edit_url);
							$public_edit_url = TrimEx($public_edit_url, "/");
							$public_edit_url = "/" . $public_edit_url . "/ticket_edit.php";
						}

						$SUPPORT_COMMENTS = PrepareTxtForEmail($arFields["SUPPORT_COMMENTS"], $arSite["LANGUAGE_ID"]);
						if (strlen($SUPPORT_COMMENTS) > 0)
						{
							$SUPPORT_COMMENTS = "\n\n" . $SUPPORT_COMMENTS . "\n";
						}

						// #SOURCE##OWNER_SID##OWNER_TEXT#
						$SOURCE_NAME = strlen($zr["SOURCE_NAME"]) <= 0 ? "" : "[" . $zr["SOURCE_NAME"] . "] ";
						if (intval($zr["OWNER_USER_ID"]) > 0 || strlen(trim($zr["OWNER_LOGIN"])) > 0)
						{
							$OWNER_TEXT = "[" . $zr["OWNER_USER_ID"] . "] (" . $zr["OWNER_LOGIN"] . ") " . $zr["OWNER_NAME"];
							if (strlen(trim($OWNER_SID)) > 0 && $OWNER_SID != "null")
							{
								$OWNER_TEXT = " / " . $OWNER_TEXT;
							}
						}

						// #CREATED_TEXT##CREATED_MODULE_NAME#
						if ($zr["CREATED_MODULE_NAME"] == "citrus.tszhtickets" && strlen($zr["CREATED_MODULE_NAME"]) > 0)
						{
							$CREATED_MODULE_NAME = "";
							$CREATED_TEXT = "[" . $zr["CREATED_USER_ID"] . "] (" . $zr["CREATED_LOGIN"] . ") " . $zr["CREATED_NAME"];
						}
						else
						{
							$CREATED_MODULE_NAME = "[" . $zr["CREATED_MODULE_NAME"] . "]";
						}

						// #RESPONSIBLE_TEXT#
						if (intval($zr["RESPONSIBLE_USER_ID"]) > 0)
						{
							$RESPONSIBLE_TEXT = "[" . $zr["RESPONSIBLE_USER_ID"] . "] (" . $zr["RESPONSIBLE_LOGIN"] . ") " . $zr["RESPONSIBLE_NAME"];
						}

						$change_log = "";

						$spam_mark = "";
						if (strlen($zr["IS_SPAM"]) > 0)
						{
							if ($zr["IS_SPAM"] == "Y")
							{
								$spam_mark = "\n" . GetMessage("SUP_EXACTLY_SPAM") . "\n";
							}
							else
							{
								$spam_mark = "\n" . GetMessage("SUP_POSSIBLE_SPAM") . "\n";
							}
						}

						if (strlen($zr["CATEGORY_NAME"]) > 0)
						{
							$change_log .= "<li>" . htmlspecialcharsEx(str_replace("#VALUE#", $zr["CATEGORY_NAME"], GetMessage("SUP_CATEGORY_CHANGED_LOG")));
						}
						if (strlen($zr["CRITICALITY_NAME"]) > 0)
						{
							$change_log .= "<li>" . htmlspecialcharsEx(str_replace("#VALUE#", $zr["CRITICALITY_NAME"], GetMessage("SUP_CRITICALITY_CHANGED_LOG")));
						}
						if (strlen($zr["STATUS_NAME"]) > 0)
						{
							$change_log .= "<li>" . htmlspecialcharsEx(str_replace("#VALUE#", $zr["STATUS_NAME"], GetMessage("SUP_STATUS_CHANGED_LOG")));
						}

						if (strlen($zr["DIFFICULTY_NAME"]) > 0)
						{
							$change_log .= "<li>" . htmlspecialcharsEx(str_replace("#VALUE#", $zr["DIFFICULTY_NAME"], GetMessage("SUP_DIFFICULTY_CHANGED_LOG")));
						}

						if (strlen($RESPONSIBLE_TEXT) > 0)
						{
							$change_log .= "<li>" . htmlspecialcharsEx(str_replace("#VALUE#", $RESPONSIBLE_TEXT, GetMessage("SUP_RESPONSIBLE_CHANGED_LOG")));
						}

						/*if ($bActiveCoupon)
						{
							$change_log .= "<li>".htmlspecialcharsEx(GetMessage('SUP_IS_SUPER_COUPON', array('#COUPON#' => $arFields['COUPON'])));
						}*/

						// ������� ��������� � ���
						if (strlen($change_log) > 0)
						{
							$arFields_log["MESSAGE"] = $change_log;
							//echo "<pre>"; print_r($arFields_log); echo "</pre>";
							CTszhTicket::AddMessage($ID, $arFields_log, $v, "N");
						}

						$arEventFields = array(
							"ID" => $ID,
							"NUMBER" => $zr['NUMBER'],
							"LANGUAGE" => $arSite["LANGUAGE_ID"],
							"LANGUAGE_ID" => $arSite["LANGUAGE_ID"],
							"DATE_CREATE" => $zr["DATE_CREATE"],
							"TIMESTAMP" => $zr["TIMESTAMP_X"],
							"DATE_CLOSE" => $zr["DATE_CLOSE"],
							"TITLE" => $zr["TITLE"],
							"CATEGORY" => $zr["CATEGORY_NAME"],
							"CRITICALITY" => $zr["CRITICALITY_NAME"],
							"DIFFICULTY" => $zr["DIFFICULTY_NAME"],
							"STATUS" => $zr["STATUS_NAME"],
							//"SLA"						=> $zr["SLA_NAME"],
							"SOURCE" => $SOURCE_NAME,
							"SPAM_MARK" => $spam_mark,
							"MESSAGE_BODY" => $MESSAGE,
							"FILES_LINKS" => $FILES_LINKS,
							"IMAGE_LINK" => $FILES_LINKS, // ������ ��������
							"ADMIN_EDIT_URL" => "/bitrix/admin/tszh_ticket_edit.php",
							"PUBLIC_EDIT_URL" => $public_edit_url,

							"OWNER_EMAIL" => TrimEx($owner_email, ","),
							"OWNER_USER_ID" => $zr["OWNER_USER_ID"],
							"OWNER_USER_NAME" => $zr["OWNER_NAME"],
							"OWNER_USER_LOGIN" => $zr["OWNER_LOGIN"],
							"OWNER_USER_EMAIL" => $zr["OWNER_EMAIL"],
							"OWNER_TEXT" => $OWNER_TEXT,
							"OWNER_SID" => $zr["OWNER_SID"],

							"SUPPORT_EMAIL" => TrimEx($support_email, ","),
							"RESPONSIBLE_USER_ID" => $zr["RESPONSIBLE_USER_ID"],
							"RESPONSIBLE_USER_NAME" => $zr["RESPONSIBLE_NAME"],
							"RESPONSIBLE_USER_LOGIN" => $zr["RESPONSIBLE_LOGIN"],
							"RESPONSIBLE_USER_EMAIL" => $zr["RESPONSIBLE_EMAIL"],
							"RESPONSIBLE_TEXT" => $RESPONSIBLE_TEXT,
							"SUPPORT_ADMIN_EMAIL" => TrimEx($support_admin_email, ","),

							"CREATED_USER_ID" => $zr["CREATED_USER_ID"],
							"CREATED_USER_LOGIN" => $zr["CREATED_LOGIN"],
							"CREATED_USER_EMAIL" => $zr["CREATED_EMAIL"],
							"CREATED_USER_NAME" => $zr["CREATED_NAME"],
							"CREATED_MODULE_NAME" => $CREATED_MODULE_NAME,
							"CREATED_TEXT" => $CREATED_TEXT,

							"SUPPORT_COMMENTS" => $SUPPORT_COMMENTS,
						);
						/*if ($bActiveCoupon)
						{
							$arEventFields['COUPON'] = $arFields['COUPON'];
						}*/

						// �������� ������ ������
						if ($SEND_EMAIL_TO_AUTHOR == "Y")
						{
							//echo "TICKET_NEW_FOR_AUTHOR:<pre>"; echo htmlspecialcharsEx(print_r($arEventFields,true));echo "</pre>";
							$arEventFields_author = CTszhTicket::ExecuteEvents('OnBeforeSendMailToAuthor', $arEventFields, true);
							if ($arEventFields_author)
							{
								CEvent::Send("TSZH_TICKET_NEW_FOR_AUTHOR", $arSite["ID"], $arEventFields_author);
							}
						}

						// �������� ������ ������������
						if ($SEND_EMAIL_TO_TECHSUPPORT == "Y")
						{
							//echo "TICKET_NEW_FOR_TECHSUPPORT:<pre>"; echo htmlspecialcharsEx(print_r($arEventFields,true));echo "</pre>";
							$arEventFields_support = CTszhTicket::ExecuteEvents('OnBeforeSendMailToSupport', $arEventFields, true);
							if ($arEventFields_support)
							{
								CEvent::Send("TSZH_TICKET_NEW_FOR_TECHSUPPORT", $arSite["ID"], $arEventFields_support);
							}
						}

						// ������� ������� � ������ ����������
						if (CModule::IncludeModule("statistic"))
						{
							if ($category_set != "Y")
							{
								$T_EVENT1 = "tszh_ticket";
								$T_EVENT2 = "";
							}
							if (strlen($T_EVENT3) <= 0)
							{
								$T_EVENT3 = "http://" . $_SERVER["HTTP_HOST"] . "/bitrix/admin/tszh_ticket_edit.php?ID=" . $ID . "&lang=" . $arSite["LANGUAGE_ID"];
							}
							CStatEvent::AddCurrent($T_EVENT1, $T_EVENT2, $T_EVENT3);
						}
					}
				}
				$arFields['ID'] = $ID;
				CTszhTicket::ExecuteEvents('OnAfterTicketAdd', $arFields, true);
			}
		}
		if (IntVal($ID) > 0)
		{
			$USER_FIELD_MANAGER->Update("TSZH_TICKET", IntVal($ID), $arFields); /// !!!!!
		}

		return $ID;
	}

	function GetFUA($site_id)
	{
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: GetFUA<br>Line: ";
		global $DB;
		if ($site_id == "all")
		{
			$site_id = "";
		}
		$arFilter = array("TYPE" => "F", "SITE" => $site_id);
		$rs = CTszhTicketDictionary::GetList(($v1 = "s_dropdown"), $v2, $arFilter, $v3);

		return $rs;
	}

	function GetRefBookValues($type, $site_id = false)
	{
		$err_mess = (CAllTszhTicket::err_mess()) . "<br>Function: GetRefBookValues<br>Line: ";
		global $DB;
		if ($site_id == "all")
		{
			$site_id = "";
		}
		$arFilter = array("TYPE" => $type, "SITE" => $site_id);
		$rs = CTszhTicketDictionary::GetList(($v1 = "s_dropdown"), $v2, $arFilter, $v3);

		return $rs;
	}

	function GetMessages($TICKET_ID, $arFilter = array(), $CHECK_RIGHTS = "Y")
	{
		$arFilter["TICKET_ID"] = $TICKET_ID;
		$arFilter["TICKET_ID_EXACT_MATCH"] = "Y";

		return CTszhTicket::GetMessageList($by, $order, $arFilter, $is_filtered, $CHECK_RIGHTS, "Y");
	}

	function GetResponsible()
	{
		return CTszhTicket::GetSupportTeamList();
	}

	function IsResponsible($USER_ID = false)
	{
		return CTszhTicket::IsSupportTeam($USER_ID);
	}

	function ExecuteEvents($message, $arFields, $is_new)
	{
		$rs = GetModuleEvents('citrus.tszhtickets', $message);
		while ($arr = $rs->Fetch())
		{
			$arFields = ExecuteModuleEvent($arr, $arFields, $is_new);
		}

		return $arFields;
	}
}