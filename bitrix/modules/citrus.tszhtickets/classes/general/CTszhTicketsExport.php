<?

class CTszhTicketsExport
{
	var $fp = false;
	var $next_step = false;
	var $bIsSupportAdmin = false;
	var $arDictionaries = Array();

	function Init($fp, $next_step)
	{
		$this->fp = $fp;
		$this->next_step = $next_step;
		//$this->$bIsSupportAdmin = CModule::IncludeModule('citrus.tszhtickets') && CTszhTicket::IsAdmin();

		//return $this->$bIsSupportAdmin;
		return true;
	}

	function StartExport() {
		$date = date("d.m.Y H:i:s");
		fwrite($this->fp, "<?xml version=\"1.0\" encoding=\"" . SITE_CHARSET . "\"?>\n");
		fwrite($this->fp, "<requests filedate=\"$date\" filetype=\"tickets\" version=\"" . TSZH_EXCHANGE_CURRENT_VERSION . "\">\n");
		return true;
	}
	function EndExport() {
		fwrite($this->fp, "</requests>\n");
		return true;
	}

	function _GetDictionary($TYPE) {
		$by = "s_id";
		$order = "asc";
		$arFilter = Array(
			"TYPE" => $TYPE,
		);
		$arDictionary = Array();
		$dbDictionary = CTszhTicketDictionary::GetList($by, $order, $arFilter);
		while ($arDictionaryItem = $dbDictionary->GetNext()) {
			$this->arDictionaries[$TYPE][$arDictionaryItem['ID']] = $arDictionaryItem;
		}
	}

	function _ExportDictionary($TYPE, $element_name, $elements_name) {

		$arDictionary = Array();
		$this->_GetDictionary($TYPE);
		foreach ($this->arDictionaries[$TYPE] as $arDictionaryItem) {
			$arDictionary[$arDictionaryItem['ID']] = $arDictionaryItem['NAME'];
		}

		if (count($arDictionary) > 0) {
			fwrite($this->fp, "\t<$elements_name>\n");
			foreach ($arDictionary as $itemID => $itemName) {
				fwrite($this->fp, "\t\t<$element_name id=\"$itemID\" title=\"" . htmlspecialchars($itemName) . "\" />\n");
			}
			fwrite($this->fp, "\t</$elements_name>\n");
		}
	}

	function ExportDictionaries() {
		$this->_ExportDictionary('S', "status", "statuses");
		$this->_ExportDictionary('C', "category", "categories");
		//$this->_ExportDictionary('M', "mark", "marks");
		fwrite($this->fp, "\n");
	}

	function ExportTickets($start_time, $INTERVAL, $arParams=Array()) {

		static $usersCache;

		CModule::IncludeModule('citrus.tszh');

		$counter = 0;
		$by = "s_id";
		$order = "asc";
		$arFilter = Array();
		if ($this->next_step['last_id']) {
			$arFilter[">ID"] = $this->next_step['last_id'];
		}
		if ($arParams['ONLY_OPENED']) {
			$arFilter['CLOSE'] = 'N';
		}
		if (array_key_exists('STATUSES', $arParams) && is_array($arParams['STATUSES'])) {
			$arFilter['STATUS'] = $arParams['STATUSES'];
			//$arFilter['STATUS'] = array_shift($arParams['STATUSES']);
		}
		if (array_key_exists('DATE_TIMESTAMP_1', $arParams) && strlen($arParams['DATE_TIMESTAMP_1']) > 0) {
			$arFilter['DATE_TIMESTAMP_1'] = $arParams['DATE_TIMESTAMP_1'];
		}
		if (array_key_exists('SITE_ID', $arParams) && strlen($arParams['SITE_ID']) > 0) {
			$arFilter["LID"] = $NS["SITE_ID"];
			$arFilter["LID_EXACT_MATCH"] = "Y";
		}
		$this->_GetDictionary('C');

		$rsTickets = CTszhTicket::GetList($by, $order, $arFilter, $is_filtered, "N", "N", "N");
		while ($arTicket = $rsTickets->GetNext()) {

			// ������� ����� ���������
			// =======================
			$is_filtered = false; $by = 's_number'; $order = 'asc';
			$dbMessages = CTszhTicket::GetMessageList($by, $order, array("TICKET_ID" => $arTicket['ID'], "TICKET_ID_EXACT_MATCH" => "Y"), $is_filtered, "N");
			if ($arMessage = $dbMessages->GetNext()) {
				$arTicket['MESSAGE'] = PrepareTxtForEmail($arMessage['~MESSAGE']);
			}

			// ����������� ���� ����������
			// ===========================
			$time_to_solve = $this->arDictionaries['C'][$arTicket['CATEGORY_ID']]['TIME_TO_SOLVE'];
			if (IntVal($time_to_solve) > 0) {
				$timestamp = strtotime("+" . $time_to_solve . ' hours', MakeTimeStamp($arTicket["DATE_CREATE"]));
				$arTicket['CONTROL_DATE'] = ConvertTimeStamp($timestamp, "FULL");
			} else {
				$arTicket['CONTROL_DATE'] = $arTicket["DATE_CREATE"];
			}

			// �������� ���� <request>
			$arTicketAttr = Array(
				"id" => str_pad($arTicket['ID'], 9, '0', STR_PAD_LEFT),
				"number" =>  $arTicket['NUMBER'],
				"status" => $arTicket['STATUS_ID'],
				"category" => $arTicket['CATEGORY_ID'],
			);
			array_walk($arTicketAttr, create_function('&$item1, $key', '$item = htmlspecialchars($item);'));

			// �������� ���� <request>
			$arTicketElements = Array(
				"title" => $arTicket['TITLE'],
				'text' => $arTicket['MESSAGE'],
				'date' => $arTicket['DATE_CREATE'],
				'contoldate' => $arTicket['CONTROL_DATE'],
				'address' => Array(
					'building' => $arTicket['UF_ADDRESS_BUILDING'],
					'flat' => $arTicket['UF_ADDRESS_FLAT'],
				),
				'fio' => $arTicket['UF_FIO'],
				'account' => $arTicket['UF_ACCOUNT'],
			);
			array_walk_recursive($arTicketAttr, create_function('&$item1, $key', '$item = htmlspecialchars($item);'));

			// ����� �������� ���������
			// ���� <status>
			$arTicketStatuses = Array();
			$dbMessages = CTszhTicket::GetMessageList(($by = 's_number'), ($order = 'asc'), Array('TICKET_ID' => $arTicket['ID'], 'TICKET_ID_EXACT_MATCH' => 'Y', 'IS_LOG' => 'Y'), ($is_filtered = false), 'N');
			while ($arMessage = $dbMessages->Fetch()) {
				if ($arMessage['STATUS_ID']) {

					if (!array_key_exists($arMessage['MODIFIED_USER_ID'], $usersCache)) {
						$rsUser = CUser::GetByID($arMessage['MODIFIED_USER_ID']);
						$arUser = $rsUser->Fetch();
						$usersCache[$arMessage['MODIFIED_USER_ID']] = $arUser;
					}

					$arTicketStatuses[$arMessage['STATUS_ID']] = Array(
						'date' => $arMessage['DATE_CREATE'],
						'user' => strlen($usersCache[$arMessage['MODIFIED_USER_ID']]['XML_ID']) > 0 ? $usersCache[$arMessage['MODIFIED_USER_ID']]['XML_ID'] : $usersCache[$arMessage['MODIFIED_USER_ID']]['ID'] . 'S',
						'user_name' => CTszhAccount::GetFullName($usersCache[$arMessage['MODIFIED_USER_ID']]),
					);
				}
			}
			array_walk($arTicketStatuses, create_function('&$item1, $key', '$item = htmlspecialchars($item);'));

			$xml = "\t<request";
			foreach ($arTicketAttr as $key=>$val) {
				$xml .= " $key=\"$val\"";
			}
			$xml .= ">\n";
			foreach ($arTicketElements as $key=>$val) {
				if (is_array($val)) {
					$xml .= "\t\t<$key>\n";
					foreach ($val as $key2=>$val2) {
						$xml .= "\t\t\t<$key2>$val2</$key2>\n";
					}
					$xml .= "\t\t</$key>\n";
				} else
					$xml .= "\t\t<$key>$val</$key>\n";
			}
			foreach ($arTicketStatuses as $key=>$arStatus) {
				$xml .= "\t\t<status";
				foreach ($arStatus as $key2=>$val2) {
					$xml .= " $key2=\"$val2\"";
				}
				$xml .= ">$key</status>\n";
			}
			$xml .= "\t</request>\n";
			fwrite($this->fp, $xml);

			$counter++;

			$this->next_step['last_id'] = $arTicket['ID'];

			if($INTERVAL > 0 && (time()-$start_time) > $INTERVAL)
				break;
		}
		return $counter;
	}
}

?>