<?

/**
 * class CTszhTicketSLA extends CAllTszhTicketSLA
 * {
 * function err_mess()
 * {
 * $module_id = "citrus.tszhtickets";
 * @include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$module_id."/install/version.php");
 * return "<br>Module: ".$module_id." (".$arModuleVersion["VERSION"].")<br>Class: CTszhTicketSLA<br>File: ".__FILE__;
 * }
 *
 * // ���������� ������ SLA
 * function GetList(&$arSort, $arFilter=Array(), &$is_filtered)
 * {
 * $err_mess = (CTszhTicketSLA::err_mess())."<br>Function: GetList<br>Line: ";
 * global $DB, $USER, $APPLICATION;
 * $is_filtered = false;
 *
 * // ���� ��������� ������� ��������� ��
 * if (CTszhTicket::CheckFilter($arFilter)):
 *
 * $arSqlSearch = Array();
 *
 * // ���� ��������� ������ ��
 * if (is_array($arFilter) && count($arFilter)>0):
 *
 * // ���������� ������ �������
 * $filter_keys = array_keys($arFilter);
 * for ($i=0; $i<count($filter_keys); $i++):
 *
 * $key = $filter_keys[$i];
 * $val = $arFilter[$filter_keys[$i]];
 * if ((is_array($val) && count($val)<=0) || (!is_array($val) && (strlen($val)<=0 || $val==='NOT_REF')))
 * continue;
 * $match_value_set = (in_array($key."_EXACT_MATCH", $filter_keys)) ? true : false;
 * $key = strtoupper($key);
 * if (is_array($val)) $val = implode(" | ",$val);
 * switch($key) :
 *
 * case "ID":
 * case "SLA_ID":
 * $match = ($arFilter[$key."_EXACT_MATCH"]=="N" && $match_value_set) ? "Y" : "N";
 * $arSqlSearch[] = GetFilterQuery("S.".$key,$val,$match);
 * break;
 * case "NAME":
 * case "DESCRIPTION":
 * $match = ($arFilter[$key."_EXACT_MATCH"]=="Y" && $match_value_set) ? "N" : "Y";
 * $arSqlSearch[] = GetFilterQuery("S.".$key, $val, $match);
 * break;
 * case "SITE":
 * $val .= " | ALL";
 * $match = ($arFilter[$key."_EXACT_MATCH"]=="N" && $match_value_set) ? "Y" : "N";
 * $arSqlSearch[] = GetFilterQuery("SS.SITE_ID", $val, $match);
 * $left_join_site = "LEFT JOIN b_tszh_ticket_sla_2_site SS ON (S.ID = SS.SLA_ID)";
 * break;
 *
 * endswitch;
 * endfor;
 * endif;
 * endif;
 *
 * // SQL ������ �������
 * $strSqlSearch = GetFilterSqlSearch($arSqlSearch);
 *
 * // ����������
 * $arSort = is_array($arSort) ? $arSort : array();
 * if (count($arSort)>0)
 * {
 * $ar1 = array_merge($DB->GetTableFieldsList("b_ticket_sla"), array());
 * $ar2 = array_keys($arSort);
 * $arDiff = array_diff($ar2, $ar1);
 * if (is_array($arDiff) && count($arDiff)>0) foreach($arDiff as $value) unset($arSort[$value]);
 * }
 * if (count($arSort)<=0) $arSort = array("PRIORITY" => "DESC");
 * while(list($by, $order) = each($arSort))
 * {
 * if ($by=="RESPONSE_TIME")
 * {
 * $arSqlOrder[] = "case RESPONSE_TIME_UNIT when 'day' then 3 when 'hour' then 2 when 'minute' then 1 end $order";
 * $arSqlOrder[] = $by." ".$order;
 * }
 * else
 * {
 * $arSqlOrder[] = $by." ".$order;
 * }
 * }
 * if (is_array($arSqlOrder) && count($arSqlOrder)>0) $strSqlOrder = " ORDER BY ".implode(",", $arSqlOrder);
 *
 * $strSql = "
 * SELECT DISTINCT
 * S.*,
 * case S.RESPONSE_TIME_UNIT
 * when 'day' then S.RESPONSE_TIME*1440
 * when 'hour' then S.RESPONSE_TIME*60
 * when 'minute' then S.RESPONSE_TIME
 * end                                            M_RESPONSE_TIME,
 * case S.NOTICE_TIME_UNIT
 * when 'day' then S.NOTICE_TIME*1440
 * when 'hour' then S.NOTICE_TIME*60
 * when 'minute' then S.NOTICE_TIME
 * end                                            M_NOTICE_TIME,
 * S.ID                                            REFERENCE_ID,
 * concat('[',S.ID,'] ',S.NAME)                    REFERENCE,
 * ".$DB->DateToCharFunction("S.DATE_MODIFY")."    DATE_MODIFY_F,
 * ".$DB->DateToCharFunction("S.DATE_CREATE")."    DATE_CREATE_F
 * FROM
 * b_tszh_ticket_sla S
 * $left_join_site
 * WHERE
 * $strSqlSearch
 * $strSqlOrder
 * ";
 * //echo "<pre>".$strSql."</pre>";
 * $rs = $DB->Query($strSql, false, $err_mess.__LINE__);
 * $is_filtered = (IsFiltered($strSqlSearch));
 * return $rs;
 * }
 *
 * }
 **/
class CTszhTicketReminder extends CAllTszhTicketReminder
{
	function err_mess()
	{
		$module_id = "citrus.tszhtickets";
		@include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $module_id . "/install/version.php");

		return "<br>Module: " . $module_id . " (" . $arModuleVersion["VERSION"] . ")<br>Class: CTszhTicketReminder<br>File: " . __FILE__;
	}
}

?>