<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заявки");
?><?$APPLICATION->IncludeComponent("citrus:support.ticket", "request", Array(
	"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
	"TICKETS_PER_PAGE" => "50",	// Количество обращений на одной странице
	"MESSAGES_PER_PAGE" => "20",	// Количество сообщений на одной странице
	"SET_PAGE_TITLE" => "Y",	// Устанавливать заголовок страницы
	"SHOW_COUPON_FIELD" => "N",	// Показывать поле ввода купона
	"SEF_FOLDER" => "/personal/support/",	// Каталог ЧПУ (относительно корня сайта)
	"SEF_URL_TEMPLATES" => array(
		"ticket_list" => "index.php",
		"ticket_edit" => "#ID#.php",
	),
	"VARIABLE_ALIASES" => array(
		"ticket_list" => "",
		"ticket_edit" => "",
	)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>