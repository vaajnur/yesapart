<?
define("STOP_STATISTICS", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
__IncludeLang(dirname(__FILE__).'/lang/'.LANGUAGE_ID.'/index.php');
__IncludeLang(dirname(__FILE__).'/lang/'.LANGUAGE_ID.'/'.basename(__FILE__));

global $APPLICATION, $USER;

CUtil::JSPostUnescape();

if ($_REQUEST['action'] == 'createUser' && check_bitrix_sessid())
{
	$APPLICATION->RestartBuffer();

	$arErrors = $arMessage = Array();
	if (!$USER->CanDoOperation('edit_all_users'))
		$arErrors[] = GetMessage("ACCESS_DENIED");

	if (!CModule::IncludeModule("citrus.tszhtickets"))
		$arErrors[] = 'citrus.tszhtickets module is not installed.';

	$rsGroup = CGroup::GetList(($by="c_sort"), ($order="desc"), Array("ID" => $_REQUEST['citrus_gd_group_id']), "Y"); 
	$arGroup = is_object($rsGroup) ? $rsGroup->GetNext() : false;
	if (!$arGroup)
		$arErrors[] = GetMessage("GD_CITRUS_SUP_ERROR_GROUP_NOT_FOUND");

	$usersCount = $arGroup['USERS'];

	if (empty($arErrors))
	{	
		$obUser = new CUser();
		$arFields = Array(
			"ACTIVE"            => "Y",
			"NAME"              => $_REQUEST['citrus_gd_name'],
			"EMAIL"             => $_REQUEST['citrus_gd_email'],
			"LOGIN"             => $_REQUEST['citrus_gd_email'],
			"GROUP_ID"          => array($_REQUEST['citrus_gd_group_id']),
			"PASSWORD"          => $_REQUEST['citrus_gd_password'],
			"CONFIRM_PASSWORD"  => $_REQUEST['citrus_gd_confirm_password']
		);

		$ID = $obUser->Add($arFields);
		if (intval($ID) > 0)
		{
			$arMessage[] = GetMessage("GD_CITRUS_SUP_USER_SUCCESSFULLY", Array("#ID#" => $ID));
			$usersCount++;
		}
		else
			$arErrors[] = $obUser->LAST_ERROR;
	}

	if (!empty($arErrors))
	{
		?><div class="bx-gadget-error"><?=implode('<br>', $arErrors)?></div><?
	}
	elseif (!empty($arMessage))
	{
		?><div class="bx-gadget-success"><?=implode('<br>', $arMessage)?></div><?
	}
	else
	{
		?><div class="bx-gadget-error">Error!</div><?
	}
	?>
	<script>
		window.citrus_gd_user_count = <?=IntVal($usersCount)?>;
	</script>
	<?
	
	die();
}