<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("citrus.tszhtickets"))
	return;

if (!$USER->CanDoOperation('edit_all_users'))
	return;

CJsCore::Init('jquery');
$APPLICATION->AddHeadScript($arGadget['PATH_SITEROOT'] . '/jquery.placeholder.min.js');
$APPLICATION->AddHeadScript($arGadget['PATH_SITEROOT'] . '/script.js');
$APPLICATION->SetAdditionalCSS($arGadget['PATH_SITEROOT'] . '/styles.css');

// ������������� ������ ������������� - ������� ������
if (!is_array($arGadgetParams))
	$arGadgetParams["UserGroup"] = 'TSZH_SUPPORT_ADMINISTRATORS';
	
$arGroup = CTszhTicket::GetUserGroupByCode($arGadgetParams["UserGroup"]);
if (!is_array($arGroup))
{
	ShowError(GetMessage("GD_CITRUS_SUP_ERROR_GROUP_NOT_FOUND"));
	return;
}
?>
<div class="bx-gadgets-title">
	<?=GetMessage("GD_CITRUS_SUP_NAME_" . $arGadgetParams["UserGroup"]);?> <small><?=GetMessage("GD_CITRUS_SUP_CNT", Array("#LINK#" => "user_admin.php?lang=".LANGUAGE_ID."&find_group_id[]=".$arGroup["ID"]."&set_filter=Y\"", "#CNT#" => $arGroup["USERS"]))?></small>
</div>

<form class="citrus-tickets-gadget-form" method="post" action="<?=$arGadget['PATH_SITEROOT']?>/ajax.php">
	<?=bitrix_sessid_post()?>
	<input type="hidden" name="action" value="createUser">
	<input type="hidden" name="citrus_gd_group_id" value="<?=$arGroup['ID']?>">

	<p><?=GetMessage("GD_CITRUS_SUP_CREATE_" . $arGadgetParams["UserGroup"])?></p>
	<div class="citrus-tickets-gadget-result"></div>

	<!-- Text input-->
	<div class="control-group control-group-left">
		<input id="citrus_gd_name" name="citrus_gd_name" type="text"  class="input-xlarge" required="" placeholder="<?=GetMessage("GD_CITRUS_SUP_TEXT_NAME")?>" value="">
	</div>
	
	<!-- Text input-->
	<div class="control-group">
		<input id="citrus_gd_email" name="citrus_gd_email" type="text" class="input-xlarge" required="" placeholder="E-mail" value="">
	</div>

	<!-- Password input-->
	<div class="control-group control-group-left">
		<input id="citrus_gd_password" name="citrus_gd_password" type="password" placeholder="<?=GetMessage("GD_CITRUS_SUP_PASSWORD")?>" class="input-xlarge" required="">
	</div>

	<!-- Password input-->
	<div class="control-group">
		<input id="citrus_gd_confirm_password" name="citrus_gd_confirm_password" type="password" placeholder="<?=GetMessage("GD_CITRUS_SUP_CONFIRM_PASSWORD")?>" class="input-xlarge" required="">
	</div>

	<!-- Button -->
	<div class="control-group" >
		<button class="bx-gadget-button bx-gadget-button-clickable">
			<span class="bx-gadget-button-text"><?=GetMessage("GD_CITRUS_SUP_BUTON_NAME")?></span>
		</button>
	</div>
</form>

<a>
	<div class="bx-gadget-shield"></div>
</a>
<a class="citrus-gadgets-config" title="<?=GetMessage("GD_CITRUS_SUP_CONFIG")?>" onclick="return getAdminGadgetHolder('admin_index').ShowSettings('<?=$arGadget["ID"];?>', '<?=$arGadget["NAME"];?>');" href="javascript:void(0)"></a>
