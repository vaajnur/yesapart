<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arDescription = Array(
		"NAME" => GetMessage("GD_CITRUS_SUPPORT_NAME"),
		"DESCRIPTION" => GetMessage("GD_CITRUS_SUPPORT_DESC"),
		"ICON" => "",
		"TITLE_ICON_CLASS" => "bx-gadgets-citrus-support",
		"GROUP" => Array("ID"=>"other"),
		"NOPARAMS" => "N",
		"AI_ONLY" => true,
		"COLOURFUL" => true
	);
?>
