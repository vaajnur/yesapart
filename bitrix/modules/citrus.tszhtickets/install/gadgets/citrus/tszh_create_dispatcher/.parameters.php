<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arUserGroup = array(
	'TSZH_SUPPORT_ADMINISTRATORS' 	=> GetMessage("GD_CITRUS_SUP_USER_SUPPORT_ADMINISTRATORS"),
	'TSZH_SUPPORT_CONTRACTORS'		=> GetMessage("GD_CITRUS_SUP_USER_SUPPORT_CONTRACTORS")
);
$arParameters = Array(
	"PARAMETERS"=> Array(), //���������, ����� ��� ���� �������������
	"USER_PARAMETERS"=> Array(	//���������, ���������� ��� ������� ������������
		"UserGroup"=>Array(
			"NAME" => GetMessage("GD_CITRUS_SUP_USER_GROUP"),
			"TYPE" => "LIST",
			"MULTIPLE" => "N",
			"DEFAULT" => 'TSZH_SUPPORT_ADMINISTRATORS',
			"VALUES"=>$arUserGroup,
		),
	), 
);
?>
