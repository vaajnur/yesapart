<?
$MESS["GD_CITRUS_SUP_CNT"] = " (<a href=\"#LINK#\" title=\"Перейти к списку пользователей\">пользователей: <b>#CNT#</b></a>)";

$MESS["GD_CITRUS_SUP_NAME_TSZH_SUPPORT_ADMINISTRATORS"] = "Диспетчеры АДС";
$MESS["GD_CITRUS_SUP_NAME_TSZH_SUPPORT_CONTRACTORS"] = "Подрядные организации АДС";
$MESS["GD_CITRUS_SUP_TEXT_NAME"] = "Введите имя";
$MESS["GD_CITRUS_SUP_PASSWORD"] = "Пароль";
$MESS["GD_CITRUS_SUP_CONFIRM_PASSWORD"] = "Подтверждение пароля";
$MESS["GD_CITRUS_SUP_BUTON_NAME"] = "Создать пользователя";

$MESS["GD_CITRUS_SUP_CREATE_TSZH_SUPPORT_ADMINISTRATORS"] = "Добавить нового диспетчера АДС:";
$MESS["GD_CITRUS_SUP_CREATE_TSZH_SUPPORT_CONTRACTORS"] = "Добавить новую подрядную организацию АДС:";
$MESS["GD_CITRUS_SUP_ERROR_GROUP_NOT_FOUND"] = "Группа не найдена";
$MESS["GD_CITRUS_SUP_ERROR_SUBMIT_FAILED"] = "Ошибка отправки формы";
$MESS["GD_CITRUS_SUP_CONFIG"] = "Настроить"
?>