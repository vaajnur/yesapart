<?
$MESS["SUP_NEW_TICKET_TITLE"] = "Нова заявка";
$MESS["SUP_EDIT_TICKET_TITLE"] = "Заявка номер";
$MESS["MODULE_NOT_INSTALL"] = "Модуль аварійно-диспетчерської служби не встановлено.";
$MESS["SUP_FORGOT_TITLE"] = "Ви забули ввести поле \"Заголовок\" ";
$MESS["SUP_FORGOT_MESSAGE"] = "Ви забули ввести поле \"Повідомлення\" ";
$MESS["SUP_MAX_FILE_SIZE_EXCEEDING"] = "Помилка! Неможливе завантаження файлу \"#FILE_NAME#\". Можливо, перевищено максимально припустимий розмір файлу.";
$MESS["SUP_PAGES"] = "Повідомлення";
$MESS["SUP_ERROR"] = "Помилка створення повідомлення";
$MESS["CTT_VIEWERS"] = "Ким проглядалося";
$MESS["CTT_OWNER"] = "Автор звернення";
$MESS["CTT_SOUREC"] = "Джерело";
$MESS["CTT_DATE_CREATE"] = "Дата створення";
$MESS["CTT_CREATED"] = "Ким стровено";
$MESS["CTT_TIMESTAMP_X"] = "Дата зміни";
$MESS["CTT_DATE_CLOSE"] = "Дата закриття";
$MESS["CTT_STATUS"] = "Статус";
$MESS["CTT_CATEGORY"] = "Тип повідомлення";
$MESS["CTT_TIME_TO_SOLVE"] = "Контрольна дата виконання ";
$MESS["CTT_CRIT"] = "Критичність";
$MESS["CTT_RESPONSIBLE"] = "Відповідальний";
$MESS["CTT_MESSAGE"] = "Повідомлення";
$MESS["CTT_CLOSE_TICKET"] = "Закрити заявку";
$MESS["CTT_CLIENT_TICKET_ADDED"] = "Ваша заявка передана диспетчеру. Заявці привласнений номер #NUMBER#";
$MESS["CTT_CLIENT_TICKET_SAVED"] = "Заявка номер #NUMBER# збережена";
$MESS["CTT_TICKET_ADDED"] = "Заявка добавлена, присвоєно номер #NUMBER#";
$MESS["CTT_TICKET_SAVED"] = "Заявка номер #NUMBER# збережена";
?>