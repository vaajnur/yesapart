<?
$MESS["SUP_EDIT_DEFAULT_TEMPLATE_PARAM_1_NAME"] = "ID повідомлення";
$MESS["SUP_EDIT_DEFAULT_TEMPLATE_PARAM_2_NAME"] = "Сторінка списку звернень";
$MESS["SUP_EDIT_MESSAGES_PER_PAGE"] = "Кількість повідомлень на одній сторінці ";
$MESS["SUP_DESC_YES"] = "Так";
$MESS["SUP_DESC_NO"] = "Ні";
$MESS["SUP_SET_PAGE_TITLE"] = "Встановлювати заголовок сторінки ";
$MESS["SUP_EDIT_FIELDS"] = "Поля, що виводяться на редагування ";
$MESS["TT_VIEWERS"] = "Ким проглядалося";
$MESS["TT_OWNER"] = "Автор звернення";
$MESS["TT_SOURCE"] = "Джерело";
$MESS["TT_DATE_CREATE"] = "Дата створення";
$MESS["TT_CREATED"] = "Ким стровено";
$MESS["TT_TIMESTAMP_X"] = "Дата зміни";
$MESS["TT_DATE_CLOSE"] = "Дата закриття";
$MESS["TT_STATUS"] = "Статус";
$MESS["TT_CATEGORY"] = "Тип звернення";
$MESS["TT_TIME_TO_SOLVE"] = "Контрольна дата виконання ";
$MESS["TT_CRIT"] = "Критичність";
$MESS["TT_RESP"] = "Відповідальний";
$MESS["TT_MESSAGE"] = "Повідомлення";
$MESS["TT_CLOSE_TICKET"] = "Закрити заявку";
?>