<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arYesNo = Array(
	"Y" => GetMessage("SUP_DESC_YES"),
	"N" => GetMessage("SUP_DESC_NO"),

);

$arRes = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("TSZH_TICKET", 0, LANGUAGE_ID);
$userProp = array();
if (!empty($arRes))
{
	foreach ($arRes as $key => $val)
		$userProp[$val["FIELD_NAME"]] = (strLen($val["EDIT_FORM_LABEL"]) > 0 ? $val["EDIT_FORM_LABEL"] : $val["FIELD_NAME"]);
}

$arShowFields = Array(
	'VIEWERS' => GetMessage("TT_VIEWERS"),
	'OWNER' => GetMessage("TT_OWNER"),
	'SOURCE' => GetMessage("TT_SOURCE"),
	'DATE_CREATE' => GetMessage("TT_DATE_CREATE"),
	'CREATED' => GetMessage("TT_CREATED"),
	'TIMESTAMP_X' => GetMessage("TT_TIMESTAMP_X"),
	'DATE_CLOSE' => GetMessage("TT_DATE_CLOSE"),
	'STATUS' => GetMessage("TT_STATUS"),
	'CATEGORY' => GetMessage("TT_CATEGORY"),
	'TIME_TO_SOLVE' => GetMessage("TT_TIME_TO_SOLVE"),
	'CRITICALITY' => GetMessage("TT_CRIT"),
	'RESPONSIBLE' => GetMessage("TT_RESP"),
);
$arEditFields = Array(
	'MESSAGE' => GetMessage("TT_MESSAGE"),
	'CRITICALITY' => GetMessage("TT_CRIT"),
	'CATEGORY' => GetMessage("TT_CATEGORY"),
	'STATUS' => GetMessage("TT_STATUS"),
	'RESPONSIBLE' => GetMessage("TT_RESP"),
	'CLOSE' => GetMessage("TT_CLOSE_TICKET"),
);
$arEditFields = array_merge($arEditFields, $userProp);

$arComponentParameters = array(
	"PARAMETERS" => array(

		"ID" => array(
			"NAME" => GetMessage("SUP_EDIT_DEFAULT_TEMPLATE_PARAM_1_NAME"),
			"TYPE" => "STRING",
			"PARENT" => "BASE",
			"DEFAULT" => "={\$_REQUEST[\"ID\"]}"
		),

        "SEF_MODE" => Array(
            "TEMPLATE_ID" => Array(
                "NAME" => GetMessage("SUP_EDIT_TEMPLATE_ID"),
                "TYPE" => "STRING",
                "MULTIPLE" => "N",
                "DEFAULT" => "#ID#.php"
            ),
        ),

        "TICKET_LIST_URL" => Array(
			"NAME" => GetMessage("SUP_EDIT_DEFAULT_TEMPLATE_PARAM_2_NAME"),
			"TYPE" => "STRING",
			"COLS" => 45,
			"PARENT" => "URL_TEMPLATES",
			"DEFAULT" => "ticket_list.php"
		),

		"MESSAGES_PER_PAGE" => Array(
			"NAME" => GetMessage("SUP_EDIT_MESSAGES_PER_PAGE"),
			"TYPE" => "STRING",
			"PARENT" => "ADDITIONAL_SETTINGS",
			"MULTIPLE" => "N",
			"DEFAULT" => "20"
		),

		"SET_PAGE_TITLE" => Array(
			"NAME"=>GetMessage("SUP_SET_PAGE_TITLE"),
			"TYPE"=>"LIST",
			"MULTIPLE"=>"N",
			"DEFAULT"=>"Y",
			"PARENT" => "ADDITIONAL_SETTINGS",
			"VALUES"=>$arYesNo,
			"ADDITIONAL_VALUES"=>"N"
		),

		"USER_PROPERTY"=>array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("SUP_EDIT_FIELDS"),
			"TYPE" => "LIST",
			"VALUES" => $arEditFields,
			"MULTIPLE" => "Y",
			"DEFAULT" => array(),
			"ADDITIONAL_VALUES"=>"Y",
		),

		"SHOW_FIELDS" => Array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => "���������� ����",
			"TYPE" => "LIST",
			"VALUES" => $arShowFields,
			"MULTIPLE" => "Y",
			"DEFAULT" => array(),
			"ADDITIONAL_VALUES"=>"Y",
		),

        "ORDER_DESC" => Array(
            "NAME"=>GetMessage("SUP_EDIT_ORDER_DESC"),
            "TYPE"=>"LIST",
            "MULTIPLE"=>"N",
            "DEFAULT"=>"N",
            "PARENT" => "ADDITIONAL_SETTINGS",
            "VALUES"=>$arYesNo,
            "ADDITIONAL_VALUES"=>"N"
        ),
	)
);
?>