<?
$MESS ['SUP_ID'] = "ID";
$MESS ['SUP_NUMBER'] = "Номер";
$MESS ['SUP_DATE_CREATE'] = "Дата подачи";
$MESS ['SUP_AUTHOR'] = "Заявитель";
$MESS ['SUP_CONTROL_DATE'] = "Контрольная дата исполнения";
$MESS ['SUP_F_FILTER'] = "Фильтр";
$MESS ['SUP_ASK'] = "Добавить заявку";
$MESS ['SUP_F_ID'] = "ID обращения";
$MESS ['SUP_F_NUMBER'] = "Номер заявки";
$MESS ['SUP_F_STATUS'] = "Состояние";
$MESS ['SUP_F_CATEGORY'] = "Тип заявки";
$MESS ['SUP_EXACT_MATCH'] = "Искать точное совпадение при фильтрации";
$MESS ['SUP_F_LAMP'] = "Индикатор";
$MESS ['SUP_RED'] = "красный";
$MESS ['SUP_GREEN'] = "зеленый";
$MESS ['SUP_GREY'] = "серый";
$MESS ['SUP_F_CLOSE'] = "Закрыто/открыто";
$MESS ['SUP_CLOSED'] = "закрыто";
$MESS ['SUP_OPENED'] = "открыто";
$MESS ['SUP_ALL'] = "(все)";
$MESS ['SUP_F_MESSAGE'] = "Текст сообщения";
$MESS ['SUP_F_SET_FILTER'] = "Установить";
$MESS ['SUP_F_DEL_FILTER'] = "Сбросить";
$MESS ['SUP_LAMP'] = "Индикатор";
$MESS ['SUP_TITLE'] = "Заголовок";
$MESS ['SUP_TIMESTAMP'] = "Изменено";
$MESS ['SUP_MODIFIED_BY'] = "Кто изменил";
$MESS ['SUP_MESSAGES'] = "Сооб.";
$MESS ['SUP_STATUS'] = "Состояние";
$MESS ['SUP_GREEN_ALT'] = "последний раз в обращение писали вы";
$MESS ['SUP_EDIT_TICKET'] = "Изменить обращение";
$MESS ['SUP_EDIT'] = "Изменить";
$MESS ['SUP_TOTAL'] = "Всего";
$MESS ['SUP_RED_ALT_2'] = "последний раз в обращение писал ваш оппонент";
$MESS ['SUP_GREY_ALT'] = "обращение закрыто";

$MESS ['TL_ACCEPT'] = "Принять";
$MESS ['TL_REPORT_DONE'] = "Отчет о выполнении";
$MESS ['TL_VIEW'] = "Просмотр";
$MESS ['TL_CLOSE_REVERT'] = "Закрыть/вернуть";
$MESS ['TL_CHANGE'] = "Изменить";
$MESS ['TL_ADD_REQUEST_TITLE'] = "Подать заявку в аварийно-диспетчерскую службу";
$MESS ['TL_ALL'] = "все";
$MESS ['TL_VIEW_TICKET'] = "Просмотр заявки";
$MESS ['TL_FLAT_ABBR'] = "кв.";
?>