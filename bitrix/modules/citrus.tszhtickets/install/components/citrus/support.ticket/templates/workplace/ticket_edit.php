<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arTicket = false;
$arAction = Array(
	'NEW_STATUS' => false,
	'TITLE' => GetMessage("TE_SAVE"),
);
if (CModule::IncludeModule('citrus.tszhtickets') && !array_key_exists('view', $_GET)) {

	$bIsSupportAdmin = CTszhTicket::IsAdmin();
	$bIsSupportTeam = CTszhTicket::IsSupportTeam();

	if (!$bIsSupportAdmin && !$bIsSupportTeam) {
		$APPLICATION->AuthForm(GetMessage("TE_ACCESS_DENIED"));
		return;
	}

	$TICKET_ID = $arResult["VARIABLES"]["ID"];
	if ($TICKET_ID > 0) {
		$dbTicket = CTszhTicket::GetByID($TICKET_ID);
		$arTicket = $dbTicket->Fetch();
	}

	// ��������� �����������
	if ($bIsSupportTeam && is_array($arTicket)) {

		// ����������� �������� �� �������� (��� ��������� �������� ��������� ���������)
		$arActions = Array(
			'delegated' => Array(
				'NEW_STATUS' => 'accepted',
				'TITLE' => GetMessage("TE_ACCEPT_TICKET"),
			),
			'accepted' => Array(
				'NEW_STATUS' => 'done',
				'TITLE' => GetMessage("TE_REPORT_DONE_TICKET"),
			),
		);

		if (array_key_exists($arTicket['STATUS_SID'], $arActions)) {
			$arUnsetFields = Array(
				"CRITICALITY",
				"CATEGORY",
				"STATUS",
				"RESPONSIBLE",
				"UF_FIO",
				"UF_ACCOUNT",
				"UF_ADDRESS_BUILDING",
				"UF_ADDRESS_FLAT",
			);
			foreach ($arParams['EDIT_FIELDS'] as $key=>$sEditField) {
				if (in_array($sEditField, $arUnsetFields)) {
					unset($arParams['EDIT_FIELDS'][$key]);
				}
			}
			$arAction = $arActions[$arTicket['STATUS_SID']];
			$arParams['EDIT_FIELDS'][] = 'STATUS';
		} else {
			unset($arParams['EDIT_FIELDS']);
		}
	} elseif ($bIsSupportAdmin) {
/*		$arActions = Array(
			'new' => Array(
				'NEW_STATUS' => 'delegated',
				'TITLE' => GetMessage("TE_ACCEPT_TICKET"),
			),
			'accepted' => Array(
				'NEW_STATUS' => 'done',
				'TITLE' => GetMessage("TE_REPORT_DONE_TICKET"),
			),
		);*/
	} else {
		$arParams['EDIT_FIELDS'] = Array();
	}

}
// ������ ��������
if (array_key_exists('view', $_GET)) {
	unset($arParams['EDIT_FIELDS']);
}

$APPLICATION->IncludeComponent(
	"citrus:support.ticket.edit",
	"",
	Array(
		"ID" => $arResult["VARIABLES"]["ID"],
		"TICKET_LIST_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["ticket_list"],
		"TICKET_EDIT_TEMPLATE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["ticket_edit"],
		"MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
		"SET_PAGE_TITLE" =>$arParams["SET_PAGE_TITLE"],
		'SHOW_COUPON_FIELD' => $arParams['SHOW_COUPON_FIELD'],
		'EDIT_FIELDS' => $arParams['EDIT_FIELDS'],
		'ACTION' => $arAction,
	),
	$component
);

?>