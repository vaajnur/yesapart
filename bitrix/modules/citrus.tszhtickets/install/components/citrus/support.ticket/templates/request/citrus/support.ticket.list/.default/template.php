<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (strlen($arResult['MESSAGE']) > 0) {
	echo ShowNote($arResult['MESSAGE']);
}

?>
<h3><a href="<?=$arResult["NEW_TICKET_PAGE"]?>" class="link" title="<?=GetMessage("SUP_ASK_TITLE")?>"><?=GetMessage("SUP_ASK")?></a></h3>

<form action="<?=$arResult["CURRENT_PAGE"]?>" method="get">
<?
	$arOpenClose= Array(
		"" => GetMessage("TT_ALL"),
		"N" => GetMessage("TT_ONLY_ACTIVE"),
		"Y" => GetMessage("TT_ARCHIVE"),
	);
	$arOpenCloseURL = Array(
		"" => $APPLICATION->GetCurPageParam('del_filter=Y', Array('find_close', 'set_filter', 'del_filter')),
		"N" => $APPLICATION->GetCurPageParam('find_close=N&set_filter=Y', Array('find_close', 'set_filter', 'del_filter')),
		"Y" => $APPLICATION->GetCurPageParam('find_close=Y&set_filter=Y', Array('find_close', 'set_filter', 'del_filter')),
	);
?>

<p><?=GetMessage("TT_SHOW_TICKETS")?>:&nbsp;
<?
foreach ($arOpenClose as $value => $option):
	if (isset($_REQUEST["find_close"]) && $_REQUEST["find_close"] == $value
		|| !isset($_REQUEST["find_close"]) && $value == ''):
		?><strong><?=$option?></strong><?
	else:
		?><a href="<?=$arOpenCloseURL[$value]?>"><?=$option?></a><?
	endif;
	?>&nbsp;&nbsp;<?
endforeach;
?>
</p>
</form>

<?if (strlen($arResult["NAV_STRING"]) > 0):?>
	<?=$arResult["NAV_STRING"]?><br /><br />
<?endif?>

<table class="support-ticket-list data-table">
	<thead>
	<tr>
		<th>
			<?/*=SortingEx("s_number")*/?>
			<?=GetMessage("TT_F_NUMBER")?>
		</th>
		<th>
			<?=SortingEx("s_created")?>
			<?=GetMessage("TT_F_DATE")?>
		</th>
		<th>
			<?=GetMessage("SUP_TITLE")?>
		</th>
		<th>
			<?=SortingEx("s_status")?>
			<?=GetMessage("SUP_STATUS")?>
		</th>
	</tr>
	</thead>

	<?foreach ($arResult["TICKETS"] as $arTicket):

		global $DB;
		$day_create = $DB->FormatDate($arTicket['DAY_CREATE'], 'YYYY-MM-DD HH:MI:SS', 'DD.MM.YYYY');

	?>
	<tr>

		<td width="1%" align="right">
			<?=$arTicket['NUMBER']?>
		</td>

		<td width="10%" align="center">
			<?=$day_create?><br />
		</td>


		<td>
			<a href="<?=$arTicket["TICKET_EDIT_URL"]?>" title="<?=GetMessage("SUP_EDIT_TICKET")?>">
				<?=$arTicket["TITLE"]?>
			</a>
		</td>

		<td><?

			if ($arTicket['STATUS_ID'] > 0 && array_key_exists($arTicket['STATUS_ID'], $arResult['DICT']['S'])):
				$arStatus = $arResult['DICT']['S'][$arTicket['STATUS_ID']];

				?>
				<div class="support-lamp support-lamp-<?=str_replace("_","-",$arStatus["SID"])?>" title="<?=$arStatus['DESCR_PUBLIC']?>"></div>
				<?=$arStatus["NAME"]?>
				<br /><a href="<?=$arTicket["TICKET_EDIT_URL"]?>" title="<?=GetMessage("SUP_EDIT_TICKET")?>"><?=GetMessage("TT_VIEW")?></a><?
			endif;
		?></td>
	</tr>
	<?endforeach?>



	<tfoot>
	<tr>
		<td colspan="5" style="text-align: left;"><?=GetMessage("SUP_TOTAL")?>: <?=$arResult["TICKETS_COUNT"]?></td>
	</tr>
	</tfoot>
</table>

<?if (strlen($arResult["NAV_STRING"]) > 0):?>
	<br /><?=$arResult["NAV_STRING"]?><br />
<?endif?>

<div class="support-ticket-hint"><?

foreach ($arResult['DICT']['S'] as $status_id=>$arStatus):
?>
	<div class="support-lamp support-lamp-<?=str_replace("_","-",$arStatus["SID"])?>"></div> - <?=$arStatus['NAME']?>
	<div class="hint-desc"><?=$arStatus['DESCR_PUBLIC']?></div>

<?
endforeach;
?>
</div>