<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arParams['SET_PAGE_TITLE'] == 'Y')
	$APPLICATION->SetTitle(GetMessage("TL_LIST_TITLE"));

$APPLICATION->IncludeComponent(
	"citrus:support.ticket.list",
	"",
	Array(
		"TICKET_EDIT_TEMPLATE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["ticket_edit"],
		"TICKETS_PER_PAGE" => $arParams["TICKETS_PER_PAGE"],
		"SET_PAGE_TITLE" => "N",
		"TICKET_ID_VARIABLE" => $arResult["ALIASES"]["ID"],
		"USE_FILTER" => 'Y',
		"EDIT_FIELDS" => $arParams['EDIT_FIELDS'],
	),
	$component
);
?>