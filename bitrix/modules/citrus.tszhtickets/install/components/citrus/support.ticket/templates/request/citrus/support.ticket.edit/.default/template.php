<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//$APPLICATION->AddHeadScript($this->GetFolder() . '/ru/script.js');

if (strlen($arResult['ERROR_MESSAGE']) > 0) {
	echo ShowError($arResult["ERROR_MESSAGE"]);
}
if (strlen($arResult["NOTE"]) > 0) {
	ShowNote($arResult["NOTE"]);
}

if (empty($arResult['TICKET'])) {
	$APPLICATION->SetTitle(GetMessage("TTE_TITLE"));
}

if (!empty($arResult["TICKET"])):

	if (!empty($arResult["ONLINE"]) && $arParams['SHOW_FIELDS']['VIEWERS']):
		?><p>
			<?$time = intval($arResult["OPTIONS"]["ONLINE_INTERVAL"]/60)." ".GetMessage("SUP_MIN");?>
			<?=str_replace("#TIME#",$time,GetMessage("SUP_USERS_ONLINE"));?>:<br />
			<?foreach($arResult["ONLINE"] as $arOnlineUser):?>
			<small>(<?=$arOnlineUser["USER_LOGIN"]?>) <?=$arOnlineUser["USER_NAME"]?> [<?=$arOnlineUser["TIMESTAMP_X"]?>]</small><br />
			<?endforeach?>
		</p><?
	endif;

?>
<table class="support-ticket-edit data-table">
<tr>
	<td style="width: 30%;"><?=GetMessage("SUP_CREATE")?></td>
	<td><?=$arResult["TICKET"]["DATE_CREATE"]?></td>
</tr>
<?if ($arResult["TICKET"]["DATE_CREATE"]!=$arResult["TICKET"]["TIMESTAMP_X"]):?>
<tr>
	<td><?=GetMessage("SUP_TIMESTAMP")?></td>
	<td><?=$arResult["TICKET"]["TIMESTAMP_X"]?><?

		if (strlen($arResult["TICKET"]["MODIFIED_MODULE_NAME"])<=0 || $arResult["TICKET"]["MODIFIED_MODULE_NAME"]=="tszhtickets") {
			echo ' &mdash; ' . $arResult["TICKET"]["MODIFIED_BY_NAME"];
		} else {
			echo ' &mdash; ' . $arResult["TICKET"]["MODIFIED_MODULE_NAME"];
		}

	?></td>
</tr>
<?endif;?>
<?if (strlen($arResult["TICKET"]["STATUS_NAME"])>0):?>
<tr>
	<td><?=GetMessage("SUP_STATUS")?></td>
	<td><span title="<?=$arResult["TICKET"]["STATUS_DESC"]?>"><?=$arResult["TICKET"]["STATUS_NAME"]?></span></td>
</tr>
<?endif;?>
<?if (strlen($arResult["TICKET"]["DATE_CLOSE"])>0):?>
<tr>
	<td><?=GetMessage("SUP_CLOSE")?></td>
	<td><?=$arResult["TICKET"]["DATE_CLOSE"]?></td>
</tr>
<?endif;?>
<?if (strlen($arResult["TICKET"]["CATEGORY_NAME"]) > 0):?>
<tr>
	<td><?=GetMessage("SUP_CATEGORY")?></td>
	<td><span title="<?=$arResult["TICKET"]["CATEGORY_DESC"]?>"><?=$arResult["TICKET"]["CATEGORY_NAME"]?></span></td>
</tr>
<?endif?>
<?
$dbStatus = CTszhTicketDictionary::GetList(($by = 'id'), ($order = 'desc'), Array("ID" => $arResult['TICKET']['CATEGORY_ID']));
if ($arStatus = $dbStatus->Fetch()) {
	if (IntVal($arStatus['TIME_TO_SOLVE']) > 0) {
		$timestamp = strtotime("+" . $arStatus['TIME_TO_SOLVE'] . ' hours', MakeTimeStamp($arResult["TICKET"]["DATE_CREATE"]));
		?>
<tr>
	<td><?=GetMessage("TTE_CONTROL_DATE")?></td>
	<td><?=ConvertTimeStamp($timestamp, "SHORT")?></td>
</tr>
<?
	}
}
?>

	<tr>
		<td colspan="2">
			<h3 style="margin-top: 10px;;"><?=GetMessage("SUP_DISCUSSION")?></h3>
	<?=$arResult["NAV_STRING"]?>

	<?foreach ($arResult["MESSAGES"] as $arMessage):?>
		<div class="ticket-edit-message">

			<?/*<div class="support-float-quote">[&nbsp;<a href="#postform" OnMouseDown="javascript:SupQuoteMessage()" title="<?=GetMessage("SUP_QUOTE_LINK_DESCR");?>"><?echo GetMessage("SUP_QUOTE_LINK");?></a>&nbsp;]</div>*/?>


			<div align="left"><b><?=GetMessage("SUP_TIME")?></b>: <?=$arMessage["DATE_CREATE"]?></div>
			<b><?=GetMessage("SUP_FROM")?></b>:

			<?=$arMessage["OWNER_SID"]?>

			<?if (intval($arMessage["OWNER_USER_ID"])>0):?>
				[<?=$arMessage["OWNER_USER_ID"]?>]
				(<?=$arMessage["OWNER_LOGIN"]?>)
				<?=$arMessage["OWNER_NAME"]?>
			<?endif?>

			<p><?=$arMessage["MESSAGE"]?></p>
		</div>

	<?endforeach;?>
	<?=$arResult["NAV_STRING"]?>

		</td>

	</tr>
</table>



<br />
<?endif;?>


<form name="support_edit" method="post" action="<?=$arResult["REAL_FILE_PATH"]?>" enctype="multipart/form-data">
<?=bitrix_sessid_post()?>
<input type="hidden" name="set_default" value="Y" />
<input type="hidden" name="ID" value=<?=(empty($arResult["TICKET"]) ? 0 : $arResult["TICKET"]["ID"])?> />
<input type="hidden" name="lang" value="<?=LANG?>" />
<table class="support-ticket-edit-form data-table no-border">

	<?if (empty($arResult["TICKET"])):?>

	<tr>
		<td class="field-name border-none"><span class="starrequired">*</span><?=GetMessage("SUP_TITLE")?>:</td>
		<td class="border-none"><input type="text" name="TITLE" value="<?=htmlspecialchars($_REQUEST["TITLE"])?>" size="48" maxlength="255" class="input" /></td>
	</tr>
	<?elseif (strlen($arResult["TICKET"]["DATE_CLOSE"]) <= 0):?>

	<tr>
		<td colspan="2"><h3 style="margin-bottom: 10px;"><?=GetMessage("TTE_LEAVE_YOUR_COMMENT")?></h3></td>
	</tr>

	<?endif?>


	<?if (strlen($arResult["TICKET"]["DATE_CLOSE"]) <= 0):?>
	<tr>
		<td class="field-name"><span class="starrequired">*</span><?=GetMessage("SUP_MESSAGE")?>:</td>
		<td><textarea name="MESSAGE" id="MESSAGE" rows="20" cols="45" wrap="virtual" class="input"><?=htmlspecialchars($_REQUEST["MESSAGE"])?></textarea></td>
	</tr>
<?/*	<?endif?>*/?>

	<?if (!empty($arResult["TICKET"]) && strlen($arResult["TICKET"]["DATE_CLOSE"])<=0):?>
	<tr>
		<td class="field-name"><?=GetMessage("SUP_CLOSE_TICKET")?>:</td>
		<td><input type="checkbox" name="CLOSE" value="Y" <?if($arResult["TICKET"]["CLOSE"] == "Y"):?>checked="checked" <?endif?>/>
		</td>
	</tr>
<?/*	<?elseif(!empty($arResult["TICKET"])):?>
	<tr>
		<td  class="field-name"><?=GetMessage("SUP_OPEN_TICKET")?>:</td>
		<td><input type="checkbox" name="OPEN" value="Y" <?if($arResult["TICKET"]["OPEN"] == "Y"):?>checked="checked" <?endif?>/>
		</td>
	</tr>*/?>
	<?endif;?>
<?// ********************* User properties ***************************************************
if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
	<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
	<tr><td class="field-name">
		<?if ($arUserField["MANDATORY"]=="Y"):?>
			<span class="starrequired">*</span>
		<?endif;?>
		<?=$arUserField["EDIT_FORM_LABEL"]?>:</td><td class="field-value">
			<?$APPLICATION->IncludeComponent(
				"bitrix:system.field.edit",
				$arUserField["USER_TYPE"]["USER_TYPE_ID"],
				array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS"=>"Y"));?></td></tr>
	<?endforeach;?>
<?endif;
// ******************** /User properties ***************************************************?>
	<tr>
		<td></td>
		<td><span class="starrequired">*</span><?=GetMessage("TTE_REQUIRED_FIELDS")?></td>
	</tr>
	<tr>
		<td></td>
		<td><?
		if (function_exists('TemplateShowButton')) {
			TemplateShowButton(Array(
				'type' => 'submit',
				'attr' => Array(
					'name' => 'save',
					'value' => '1',
				),
				'title' => (empty($arResult['TICKET']) ? GetMessage("TTE_SEND_TICKET") : GetMessage("TTE_SAVE_TICKET")),
			));
		} else {
			?><input type="submit" name="save" value="<?=(empty($arResult['TICKET']) ? GetMessage("TTE_SEND_TICKET") : GetMessage("TTE_SAVE_TICKET"))?>" /><?
		}

		?>
	</tr>
	<?endif;?>
</table>

<input type="hidden" value="Y" name="save" />

</form>

<p><a href="<?=$arParams['TICKET_LIST_URL']?>"><?=GetMessage("TTE_BACK_TO_LIST")?></a></p>
