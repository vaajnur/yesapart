<?
$MESS["SUP_LIST_TICKETS_PER_PAGE"] = "Кількість звернень на одній сторінці";
$MESS["SUP_SET_PAGE_TITLE"] = "Встановлювати заголовок сторінки";
$MESS["SUP_EDIT_MESSAGES_PER_PAGE"] = "Кількість повідомлень на одній сторінці";
$MESS["SUP_DESC_YES"] = "Так";
$MESS["SUP_DESC_NO"] = "Ні";
$MESS["SUP_TICKET_ID_DESC"] = "Індентифікатор звернення";
$MESS["SUP_TICKET_LIST_DESC"] = "Список звернень";
$MESS["SUP_TICKET_EDIT_DESC"] = "Редагування звернення";
$MESS["SUP_EDIT_FIELDS"] = "Поля, які виводяться на редагування";
?>