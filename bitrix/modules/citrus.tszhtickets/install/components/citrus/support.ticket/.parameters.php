<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arYesNo = Array(
	"Y" => GetMessage("SUP_DESC_YES"),
	"N" => GetMessage("SUP_DESC_NO"),
);


$arRes = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("TSZH_TICKET", 0, LANGUAGE_ID);
$userProp = array();
if (!empty($arRes))
{
	foreach ($arRes as $key => $val)
		$userProp[$val["FIELD_NAME"]] = (strLen($val["EDIT_FORM_LABEL"]) > 0 ? $val["EDIT_FORM_LABEL"] : $val["FIELD_NAME"]);
}

$arShowFields = Array(
	'VIEWERS' => GetMessage("CTT_F_VIEWERS"),
	'OWNER' => GetMessage("CTT_F_OWNER"),
	'SOURCE' => GetMessage("CTT_F_SOURCE"),
	'DATE_CREATE' => GetMessage("CTT_F_DATE_CREATE"),
	'CREATED' => GetMessage("CTT_F_CREATED"),
	'TIMESTAMP_X' => GetMessage("CTT_F_TIMESTAMP_X"),
	'DATE_CLOSE' => GetMessage("CTT_F_DATE_CLOSE"),
	'STATUS' => GetMessage("CTT_F_STATUS"),
	'CATEGORY' => GetMessage("CTT_F_CATEGORY"),
	'TIME_TO_SOLVE' => GetMessage("CTT_F_TIME_TO_SOLVE"),
	'CRITICALITY' => GetMessage("CTT_F_CRITICALITY"),
	'RESPONSIBLE' => GetMessage("CTT_F_RESPONSIBLE"),
);

$arEditFields = Array(
	'MESSAGE' => GetMessage("CTT_F_MESSAGE"),
	'CRITICALITY' => GetMessage("CTT_F_CRITICALITY"),
	'CATEGORY' => GetMessage("CTT_F_CATEGORY"),
	'STATUS' => GetMessage("CTT_F_STATUS"),
	'RESPONSIBLE' => GetMessage("CTT_F_RESPONSIBLE"),
	'CLOSE' => GetMessage("CTT_F_CLOSE_TICKET"),
);
$arEditFields = array_merge($arEditFields, $userProp);

$arComponentParameters = array(
	"PARAMETERS" => array(

		"VARIABLE_ALIASES" => Array(
			"ID" => Array("NAME" => GetMessage("SUP_TICKET_ID_DESC"))
		),

		"SEF_MODE" => Array(
			"ticket_list" => Array(
				"NAME" => GetMessage("SUP_TICKET_LIST_DESC"),
				"DEFAULT" => "index.php",
				"VARIABLES" => array()
			),

			"ticket_edit" => Array(
				"NAME" => GetMessage("SUP_TICKET_EDIT_DESC"),
				"DEFAULT" => "#ID#.php",
				"VARIABLES" => array("ID")
			),
		),

		"TICKETS_PER_PAGE" => Array(
			"NAME" => GetMessage("SUP_LIST_TICKETS_PER_PAGE"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"PARENT" => "ADDITIONAL_SETTINGS",
			"DEFAULT" => "50"
		),

		"MESSAGES_PER_PAGE" => Array(
			"NAME" => GetMessage("SUP_EDIT_MESSAGES_PER_PAGE"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"PARENT" => "ADDITIONAL_SETTINGS",
			"DEFAULT" => "20"
		),


		"SET_PAGE_TITLE" => Array(
			"NAME"=>GetMessage("SUP_SET_PAGE_TITLE"),
			"TYPE"=>"LIST",
			"MULTIPLE"=>"N",
			"DEFAULT"=>"Y",
			"PARENT" => "ADDITIONAL_SETTINGS",
			"VALUES"=>$arYesNo,
			"ADDITIONAL_VALUES"=>"N"
		),

		"EDIT_FIELDS"=>array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("SUP_EDIT_FIELDS"),
			"TYPE" => "LIST",
			"VALUES" => $arEditFields,
			"MULTIPLE" => "Y",
			"DEFAULT" => array(),
			"ADDITIONAL_VALUES"=>"Y",
		),

		"SHOW_FIELDS" => Array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("SUP_SHOW_FIELDS"),
			"TYPE" => "LIST",
			"VALUES" => $arShowFields,
			"MULTIPLE" => "Y",
			"DEFAULT" => array(),
			"ADDITIONAL_VALUES"=>"Y",
		),
	)
);
?>
