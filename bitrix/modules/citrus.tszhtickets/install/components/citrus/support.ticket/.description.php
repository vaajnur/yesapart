<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SUP_SUPPORT_NAME"),
	"DESCRIPTION" => GetMessage("SUP_SUPPORT_DESCRIPTION"),
	"ICON" => "/images/support.gif",
	"COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "citrus",
		"NAME" => GetMessage("C_CITRUS"),
		"CHILD" => array(
			"ID" => "tszh_support",
			"NAME" => GetMessage("C_TSZH_TICKETS"),
			"SORT" => 10,
		)
	),
);
?>