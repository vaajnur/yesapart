<?
$MESS ['SUP_ASK_TITLE'] = "Подать заявку в «Аварийно-диспетчерскую службу»";
$MESS ['SUP_ASK'] = "Создать заявку";
$MESS ['TT_ALL'] = "Все";
$MESS ['TT_ONLY_ACTIVE'] = "Только активные";
$MESS ['TT_ARCHIVE'] = "Архив";
$MESS ['TT_F_NUMBER'] = "Номер заявки";
$MESS ['TT_F_DATE'] = "Дата подачи";
$MESS ['TT_DESCRIPTION'] = "Описание";
$MESS ['SUP_STATUS'] = "Статус";
$MESS ['STATUS_ORDER_EXECUTION'] = "Статус выполнения заявки";
$MESS ['SUP_EDIT_TICKET'] = "Изменить обращение";
$MESS ['SUP_TOTAL'] = "Всего";
$MESS ['TT_SHOW_TICKETS'] = "Отображать заявки";
$MESS ['FR_TITLE'] = "Создание новой заявки";
$MESS ['FR_REASON'] = "Укажите причину обращения";
$MESS ['FR_TELEPHONE'] = "Контактный телефон для связи";
?>