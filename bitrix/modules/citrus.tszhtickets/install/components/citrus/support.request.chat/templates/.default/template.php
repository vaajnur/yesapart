<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (strlen($arResult['ERROR_MESSAGE']) > 0) {
	echo ShowError($arResult["ERROR_MESSAGE"]);
}

if ($arResult["MESSAGE"] != "") {
	ShowNote($arResult["MESSAGE"]);
}
?>
<div class="req-descrip">
    <div>
        <div><?=GetMessage('SUP_CREATE')?></div>
        <div><?=$arResult["TICKET"]["DATE_CREATE"]?></div>
    </div>
    <div>
        <div><?=GetMessage('SUP_DATE_END')?></div>
        <div><?= $arResult["TICKET"]["DATE_SOLVE"] ?></div>
    </div>
    <div>
        <div><?=GetMessage('SUP_STATUS')?></div>
        <div><?= $arResult["TICKET"]["STATUS_NAME"] ?></div>
    </div>
</div>
<div class="chat">
    <?if ($arResult["TICKET"]["DATE_CLOSE"] == ""):?>
    <form name="support_edit" method="post" action="<?=$arResult["CURPAGE"]?>" enctype="multipart/form-data">
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="set_default" value="Y" />
        <input type="hidden" name="ID" value=<?=(empty($arResult["TICKET"]) ? 0 : $arResult["TICKET"]["ID"])?> />
        <input type="hidden" name="lang" value="<?=LANG?>" />
        <input type="hidden" value="Y" name="save" />
        <div><?=GetMessage('SUP_NEW_MESSAGE')?>:</div>
        <textarea name="MESSAGE" id="MESSAGE" wrap="virtual" class="chat__textarea"><?=htmlspecialchars($_REQUEST["MESSAGE"])?></textarea>
        <div class="input-checkbox">
            <input type="checkbox" name="CLOSE" id="CLOSE" value="Y" <?if($arResult["TICKET"]["CLOSE"] == "Y"):?>checked="checked" <?endif?>>
            <label for="CLOSE">
                <?=GetMessage("SUP_CLOSE_TICKET")?> </label>
        </div>
        <input type="submit" value="<?=GetMessage("SUP_SEND_MESSAGE")?>" />
    </form>
    <?endif;?>
        <?=$arResult["NAV_STRING"]?>
    <div class="chat__messages">
    <?foreach ($arResult["MESSAGES"] as $arMessage):?>
        <div class="chat__message">
            <div class="chat__message-time">
                <?= $arMessage["DATE_CREATE"] ?>
            </div>
            <div class="chat__message-text">
                <div class="bold"><?= ($arMessage["POSITION"]!="" && strtolower(trim($arMessage["POSITION"]))!=strtolower(trim($arMessage["OWNER_NAME"]))?$arMessage["POSITION"].". ":"").$arMessage["OWNER_NAME"] ?></div>
                <div><?= $arMessage["MESSAGE"] ?></div>
            </div>
        </div>
    <?endforeach;?>
    </div>
        <?=$arResult["NAV_STRING"];?>
</div>
