<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (strlen($arResult['MESSAGE']) > 0) {
	echo ShowNote($arResult['MESSAGE']);
}

if (CTszhTicket::IsSupportTeam()) {
	$arStatusActions = Array(
		'delegated' => GetMessage("CTT_ACCEPT"),
		'accepted' => GetMessage("CTT_REPORT_COMPLETE"),
	);
	foreach ($arResult['DICT']['S'] as $status_id => $arStatus) {
		if (array_key_exists($arStatus['SID'], $arStatusActions)) {
			$arResult['DICT']['S'][$status_id]['ACTION'] = $arStatusActions[$arStatus['SID']];
		} else {
			$arResult['DICT']['S'][$status_id]['ACTION'] = '';
		}
	}
} else {
	$arStatusActions = Array(
		'new' => GetMessage("CTT_ACCEPT"),
		'done' => GetMessage("CTT_CLOSE_SEND_BACK"),
	);
	foreach ($arResult['DICT']['S'] as $status_id => $arStatus) {
		if (array_key_exists($arStatus['SID'], $arStatusActions)) {
			$arResult['DICT']['S'][$status_id]['ACTION'] = $arStatusActions[$arStatus['SID']];
		} else {
			$arResult['DICT']['S'][$status_id]['ACTION'] = GetMessage("CTT_CHANGE_TICKET");
		}
	}
}


//echo '<pre>' . htmlspecialchars(print_r($arResult, true)) . '</pre>';

?>
<h3 style="margin: 15px 0;"><a href="<?=$arResult["NEW_TICKET_PAGE"]?>" class="link" title="������ ������ � ������ �05�"><?=GetMessage("SUP_ASK")?></a></h3>

<form action="<?=$arResult["CURRENT_PAGE"]?>" method="get">
<?if ($arParams['USE_FILTER'] == 'Y'):?>
<table cellspacing="0" class="support-ticket-filter data-table">
	<tr>
		<th colspan="2"><?=GetMessage("SUP_F_FILTER")?></th>
	</tr>
	<tr>
		<td><?=GetMessage("SUP_F_NUMBER")?>:</td>
		<td>
			<input type="text" name="find_number" size="20" value="<?=htmlspecialchars($arResult['FILTER']['NUMBER'])?>" />
<?/*			<input type="checkbox" name="find_number_exact_match" value="Y" title="<?=GetMessage("SUP_EXACT_MATCH")?>" <?
				if($arResult['FILTER']['NUMBER_EXACT_MATCH']):?>checked="checked" <?endif?>/>*/?>
		</td>
	</tr>
	<tr>
		<td><?=GetMessage("SUP_F_STATUS")?>:</td>
		<td>
			<select multiple="multiple" name="find_status[]" id="find_status" size="<?=count($arResult['DICTIONARY']['STATUS'])+1?>">
			<?foreach ($arResult['DICTIONARY']['STATUS'] as $value => $option):?>
					<option value="<?=$value?>" <?if(is_array($arResult['FILTER']["STATUS_ID"]) && in_array($value, $arResult['FILTER']["STATUS_ID"])):?>selected="selected"<?endif?>><?=$option?></option>
			<?endforeach?>
			</select>
		</td>
	</tr>
	<tr>
		<td><?=GetMessage("SUP_F_CATEGORY")?>:</td>
		<td>
			<select multiple="multiple" name="find_category[]" id="find_category" size="<?=count($arResult['DICTIONARY']['CATEGORY'])+1?>">
				<option value="0" <?if(!is_array($arResult['FILTER']["CATEGORY_ID"])):?>selected="selected"<?endif?>>&lt;���&gt;</option>
			<?foreach ($arResult['DICTIONARY']['CATEGORY'] as $value => $option):?>
					<option value="<?=$value?>" <?if(is_array($arResult['FILTER']["CATEGORY_ID"]) && in_array($value, $arResult['FILTER']["CATEGORY_ID"])):?>selected="selected"<?endif?>><?=$option?></option>
			<?endforeach?>
			</select>
		</td>
	</tr>
<?/*
	<tr>
		<td><?=GetMessage("SUP_F_CLOSE")?>:</td>
		<td>
			<?
				$arOpenClose= Array(
					"Y" => GetMessage("SUP_CLOSED"),
					"N" => GetMessage("SUP_OPENED"),
				);
			?>
			<select name="find_close" id="find_close">
				<option value=""><?=GetMessage("SUP_ALL")?></option>
			<?foreach ($arOpenClose as $value => $option):?>
				<option value="<?=$value?>" <?if(isset($_REQUEST["find_close"]) && $_REQUEST["find_close"] == $value):?>selected="selected"<?endif?>><?=$option?></option>
			<?endforeach?>
			</select>
		</td>
	</tr>*/?>
	<tr>
		<td><?=GetMessage("SUP_TITLE")?>:</td>
		<td>
		<input type="text" name="find_title" size="40" value="<?=htmlspecialchars($_REQUEST["find_title"])?>" />
		<input type="checkbox" name="find_title_exact_match" value="Y" title="<?=GetMessage("SUP_EXACT_MATCH")?>" <?
				if(isset($_REQUEST["find_title_exact_match"]) && $_REQUEST["find_title_exact_match"] == "Y"):?>checked="checked" <?endif?>/>
		</td>
	</tr>
	<tr>
		<td><?=GetMessage("SUP_F_MESSAGE")?>:</td>
		<td>
			<input type="text" name="find_message" size="40" value="<?=htmlspecialchars($_REQUEST["find_message"])?>" />
			<input type="checkbox" name="find_message_exact_match" value="Y" title="<?=GetMessage("SUP_EXACT_MATCH")?>" <?
				if(isset($_REQUEST["find_message_exact_match"]) && $_REQUEST["find_message_exact_match"] == "Y"):?>checked="checked" <?endif?>/>
		</td>
	</tr>
	<tr>
		<th colspan="2">
			<input name="set_filter" value="<?=GetMessage("SUP_F_SET_FILTER")?>" type="submit" />&nbsp;&nbsp;
			<input name="del_filter" value="<?=GetMessage("SUP_F_DEL_FILTER")?>" type="submit" />
			<input name="set_filter" value="Y" type="hidden" />
		</th>
	</tr>
</table>
<?endif;?>
</form>

<br />

<?if (strlen($arResult["NAV_STRING"]) > 0):?>
	<?=$arResult["NAV_STRING"]?><br /><br />
<?endif?>

<table cellspacing="0" class="support-ticket-list data-table">

	<tr>
		<th>
			<?=GetMessage("SUP_NUMBER")?><?=SortingEx("s_number")?>
		</th>
		<th>
			<?=GetMessage("SUP_DATE_CREATE")?><?=SortingEx("s_date_create")?>
		</th>
		<th>
			<?=GetMessage("SUP_TITLE")?>
		</th>
		<th>
			<?=GetMessage("SUP_AUTHOR")?>
		</th>
		<th>
			<?=GetMessage("SUP_CONTROL_DATE")?>
		</th>
		<th>
			<?=GetMessage("SUP_STATUS")?><br />
		</th>
	</tr>

	<?foreach ($arResult["TICKETS"] as $arTicket):

		$strRowColor = false;

		if ($arTicket['CATEGORY_ID'] && array_key_exists($arTicket['CATEGORY_ID'], $arResult['DICT']['C'])) {
			$arCategory = $arResult['DICT']['C'][$arTicket['CATEGORY_ID']];
			if (IntVal($arCategory['TIME_TO_SOLVE']) > 0) {
				$timestamp = strtotime("+" . $arCategory['TIME_TO_SOLVE'] . ' hours', MakeTimeStamp($arTicket["DATE_CREATE"]));
				$arTicket['CONTROL_TIMESTAMP'] = $timestamp;
				$arTicket['CONTROL_DATE'] = ConvertTimeStamp($timestamp, "FULL");
				if ($timestamp <= time()) {
					$strRowColor = ' style="background: #fbb"';
				}
			}
		}


	?>
	<tr>

		<td width="5%" align="right"<?=$strRowColor?>>
			<?=$arTicket["NUMBER"]?><?/*<br />
			<div class="support-lamp-<?=str_replace("_","-",$arTicket["LAMP"])?>" title="<?=GetMessage("SUP_".strtoupper($arTicket["LAMP"])."_ALT")?>"></div>
			[&nbsp;<a href="<?=$arTicket["TICKET_EDIT_URL"]?>" title="<?=GetMessage("SUP_EDIT_TICKET")?>"><?=GetMessage("SUP_EDIT")?></a>&nbsp;]*/?>
		</td>


		<td<?=$strRowColor?>>
			<?=$arTicket["DATE_CREATE"]?>
		</td>

		<td<?=$strRowColor?>>
			<?=$arTicket["TITLE"]?>
		</td>

		<td<?=$strRowColor?>><?

			echo implode('<br />', Array($arTicket['UF_FIO'], $arTicket['UF_ADDRESS_BUILDING'] . (strlen($arTicket['UF_ADDRESS_FLAT']) > 0 ? ', ��. ' . $arTicket['UF_ADDRESS_FLAT'] : ''))) . '<br />';

		?></td>

		<td<?=$strRowColor?>><?=$arTicket['CONTROL_DATE']?></td>


		<td<?=$strRowColor?>><?

			if ($arTicket['STATUS_ID'] > 0 && array_key_exists($arTicket['STATUS_ID'], $arResult['DICT']['S'])):
				$arStatus = $arResult['DICT']['S'][$arTicket['STATUS_ID']];

				?>
				<div class="support-lamp support-lamp-<?=str_replace("_","-",$arStatus["SID"])?>" title="<?=$arStatus['NAME']?>"></div>
				<?=$arStatus["NAME"]?>
				<?if (strlen($arStatus['ACTION']) > 0):?>
				<br /><a href="<?=$arTicket["TICKET_EDIT_URL"]?>" title="<?=GetMessage("SUP_EDIT_TICKET")?>"><?=$arStatus['ACTION']?></a>
				<?endif;
			endif;
		?></td>
	</tr>
	<?endforeach?>



	<tr>
		<th colspan="6"><?=GetMessage("SUP_TOTAL")?>: <?=$arResult["TICKETS_COUNT"]?></th>
	</tr>
</table>

<?if (strlen($arResult["NAV_STRING"]) > 0):?>
	<br /><?=$arResult["NAV_STRING"]?><br />
<?endif?>

<div class="support-ticket-hint"><?

foreach ($arResult['DICT']['S'] as $status_id=>$arStatus):
?>
	<div class="support-lamp support-lamp-<?=str_replace("_","-",$arStatus["SID"])?>"></div> - <?=$arStatus['NAME']?>
	<div class="hint-desc"><?=$arStatus['DESCR']?></div>

<?
endforeach;
?>
</div>