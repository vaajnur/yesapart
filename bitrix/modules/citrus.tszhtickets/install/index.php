<?
global $MESS, $APPLICATION;
$PathInstall = str_replace("\\", "/", __FILE__);
$PathInstall = substr($PathInstall, 0, strlen($PathInstall)-strlen("/index.php"));
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszhtickets/include.php");
IncludeModuleLangFile($PathInstall."/install.php");

if(class_exists("citrus_tszhtickets")) return;
Class citrus_tszhtickets extends CModule
{
	var $MODULE_ID = "citrus.tszhtickets";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";
	var $SHOW_SUPER_ADMIN_GROUP_RIGHTS = "Y";

	/**
	 * @var array ������ ��������� ���������� ��� ���������/�������� ������ (�-�, ��������� ��� ��� ������� � �� ��� ����� ��������� �����)
	 */
	public $params = array();

	function citrus_tszhtickets()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else
		{
			$this->MODULE_VERSION = SUPPORT_VERSION;
			$this->MODULE_VERSION_DATE = SUPPORT_VERSION_DATE;
		}

		$this->MODULE_NAME = GetMessage("VDGB_SUP_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("VDGB_SUP_MODULE_DESCRIPTION");
		$this->MODULE_CSS = "/bitrix/modules/{$this->MODULE_ID}/support_admin.css";

		$this->PARTNER_NAME = GetMessage("C_MODULE_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("C_MODULE_PARTNER_URI");
	}

	protected function setParams($arParams)
	{
		$this->params = $arParams;

		return $this;
	}

	protected function getParams($method)
	{
		return array_key_exists($method, $this->params) ? $this->params[$method] : array();
	}

	function InstallDB()
	{
		$arParams = $this->getParams(__FUNCTION__);

		/*if (!is_array($arParams))
			 $arParams = array();*/
		
		global $DB, $APPLICATION, $DBType;
		$EMPTY = false;
		$errors = false;
		if (!$DB->Query("SELECT 'x' FROM b_tszh_ticket", true))
		{
			$EMPTY = true;
		}

		if ($EMPTY)
		{
			$errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/{$this->MODULE_ID}/install/db/".$DBType.'/install.sql');
		}

		if (is_array($errors))
		{
			$APPLICATION->ThrowException(implode(' ', $errors));
			return false;
		}

		RegisterModule($this->MODULE_ID);

		RegisterModuleDependences('mail', 'OnGetFilterList', $this->MODULE_ID, 'CTszhSupportEMail', 'OnGetFilterList');

		CAgent::AddAgent('CTszhTicket::CleanUpOnline();', $this->MODULE_ID, 'N');
		CAgent::AddAgent('CTszhTicket::AutoClose();', $this->MODULE_ID, 'N');


		if ($EMPTY)
		{
			if ($arParams['admin'] == 'Y')
			{
				$this->InstallEvents();
			}

			if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/install/demodata.php'))
			{
				$DD_ERROR = false;
				include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/install/demodata.php');
				if ($DD_ERROR)
				{
					$APPLICATION->ThrowException(implode('<br />', $arDemoDataErrors));
					return false;
				}
			}
		}

		return true;
	}

	function UnInstallDB()
	{
		$arParams = $this->getParams(__FUNCTION__);

		/*if (!is_array($arParams))
			 $arParams = array();*/
		
		global $DB, $APPLICATION, $DBType;
		$errors = false;
		if($arParams['savedata'] != 'Y')
		{
			$errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/db/'.$DBType.'/uninstall.sql');
			if (!is_array($errors))
			{
				@set_time_limit(600);
				COption::RemoveOption($this->MODULE_ID);

				$db_res = $DB->Query("SELECT ID FROM b_file WHERE MODULE_ID = '{$this->MODULE_ID}'");
				while($arRes = $db_res->Fetch())
				{
					CFile::Delete($arRes['ID']);
				}

				if ($arParams['admin'] == 'Y')
				{
					$this->UnInstallEvents();
				}
			}
		}

		if (is_array($errors))
		{
			$APPLICATION->ThrowException(implode(' ', $errors));
			return false;
		}

		CAgent::RemoveModuleAgents($this->MODULE_ID);
		UnRegisterModuleDependences('mail', 'OnGetFilterList', $this->MODULE_ID, 'CTszhSupportEMail', 'OnGetFilterList');
		UnRegisterModule($this->MODULE_ID);

		return true;
	}

	function InstallEvents()
	{
		if (file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/events/set_events.php'))
		{
			$SE_ERROR = false;
			include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/events/set_events.php');
			if ($SE_ERROR)
			{
				return false;
			}
		}

		return true;
	}

	function UnInstallEvents()
	{
		if (file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/events/del_events.php'))
		{
			include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/events/del_events.php');
		}

		return true;
	}

	//���������� ������� �� ������� ����
	function InstallCitrusGadgets($gadgets_id, $user_group)
	{
		$gdid = $gadgets_id."@".rand();
		
		$arUserOptions = array();

		$arUserOptions = CUserOptions::GetOption("intranet", "~gadgets_".'admin_index', false, false);

		foreach($arUserOptions[0]["GADGETS"] as $tempid=>$tempgadget)
		if($tempgadget["COLUMN"]==0)
			$arUserOptions[0]["GADGETS"][$tempid]["ROW"]++;

		$arUserOptions[0]["GADGETS"][$gdid] = Array("COLUMN"=>0, "ROW"=>0, 'HIDE' => 'N');
		$arUserOptions[0]["GADGETS"][$gdid]["SETTINGS"]["UserGroup"] = $user_group;
		
		CUserOptions::SetOption("intranet", "~gadgets_".'admin_index', $arUserOptions, true, false);
		
		return true;
	}

	function InstallFiles()
	{
		$arParams = $this->getParams(__FUNCTION__);

		CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/admin/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin');
		CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/images/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/images/tszh_tickets', true, true);
		CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/upload/tszh_tickets/not_image/', $_SERVER['DOCUMENT_ROOT'].'/'.COption::GetOptionString('main', 'upload_dir', 'upload').'/tszh_tickets/not_image/');
		CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/tools/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/tools/');
		CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/components', $_SERVER['DOCUMENT_ROOT'].'/bitrix/components', True, True);
		
		//Gadgets
		CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/gadgets', $_SERVER['DOCUMENT_ROOT'].'/bitrix/gadgets', True, True);
		$this->InstallCitrusGadgets('TSZH_CREATE_DISPATCHER',"TSZH_SUPPORT_ADMINISTRATORS");
		$this->InstallCitrusGadgets('TSZH_CREATE_DISPATCHER',"TSZH_SUPPORT_CONTRACTORS");


		//Theme
		CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/themes/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/themes/', true, true);

		if ($arParams['install_public'] == 'Y' && !empty($arParams['public_dir']))
		{
			$bReWriteAdditionalFiles = $arParams['public_rewrite'] == 'Y';

			$rsSite = CSite::GetList(($by='sort'),($order='asc'));
			while ($arSite = $rsSite->Fetch())
			{
				CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/public/all/', $arSite['ABS_DOC_ROOT'].$arSite['DIR'].$arParams['public_dir'], $bReWriteAdditionalFiles, true);
			}
		}

		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/admin/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin');
		DeleteDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/themes/.default/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/themes/.default');//css
		DeleteDirFilesEx('/bitrix/themes/.default/icons/tszh_tickets/');//icons
		DeleteDirFilesEx('/bitrix/images/tszh_tickets/');//images
		DeleteDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/' . $this->MODULE_ID . '/install/tools/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/tools/');

		return true;
	}

	function DoInstall()
	{
		global $DB, $APPLICATION, $step;

		$step = IntVal($step);
		if($step<2)
			$APPLICATION->IncludeAdminFile(GetMessage("SUP_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/{$this->MODULE_ID}/install/step1.php");
		elseif($step==2)
		{
			$APPLICATION->ResetException();
			$this->setParams(
				array(
					'InstallDB' => array(
						'admin' => 'Y'
					)
				)
			);
			if ($this->InstallDB())
			{
				$this->setParams(
						array(
							'InstallFiles' => array(
								'install_public' => $_REQUEST['install_public'],
								'public_dir' => $_REQUEST['public_dir'],
								'public_rewrite' => $_REQUEST['public_rewrite'],
							)
						)
					)
					->InstallFiles();
			}
			$APPLICATION->IncludeAdminFile(GetMessage("SUP_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/{$this->MODULE_ID}/install/step2.php");
		}
	}

	function DoUninstall()
	{
		global $DB, $APPLICATION, $step;

		$step = IntVal($step);
		if($step<2)
			$APPLICATION->IncludeAdminFile(GetMessage("SUP_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/{$this->MODULE_ID}/install/unstep1.php");
		elseif($step==2)
		{
			$APPLICATION->ResetException();
			$this->setParams(
				array(
					'UnInstallDB' => array(
						'admin' => 'Y',
						'savedata' => $_REQUEST['savedata']
					)
				)
			);
			if ($this->UnInstallDB())
			{
				$this->UnInstallFiles();
			}
			$APPLICATION->IncludeAdminFile(GetMessage("SUP_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/{$this->MODULE_ID}/install/unstep2.php");
		}
	}

	function GetModuleRightList()
	{
		$arr = array(
			"reference_id" => array("D","R","T","V","W"),
			"reference" => array(
				"[D] ".GetMessage("SUP_DENIED"),
				"[R] ".GetMessage("SUP_CREATE_TICKET"),
				"[T] ".GetMessage("SUP_SUPPORT_STAFF_MEMBER"),
				"[V] ".GetMessage("SUP_DEMO_ACCESS"),
				"[W] ".GetMessage("SUP_SUPPORT_ADMIN"))
			);
		return $arr;
	}
}
?>