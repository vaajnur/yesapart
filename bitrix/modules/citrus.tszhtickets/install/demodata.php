<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/citrus.tszhtickets/classes/general/CAllTszhTicket.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/citrus.tszhtickets/classes/general/CAllTszhTicketDictionary.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/citrus.tszhtickets/classes/general/CAllTszhTicketReminder.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/citrus.tszhtickets/classes/general/CTszhSupportEMail.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/citrus.tszhtickets/classes/general/CTszhSupportSuperCoupon.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/citrus.tszhtickets/classes/general/CTszhSupportUser2UserGroup.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/citrus.tszhtickets/classes/general/CTszhSupportUserGroup.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/citrus.tszhtickets/classes/general/CTszhTicketsExport.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/citrus.tszhtickets/classes/mysql/CTszhTicket.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/citrus.tszhtickets/classes/mysql/CTszhTicketDictionary.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/citrus.tszhtickets/classes/mysql/CTszhTicketReminder.php');

IncludeModuleLangFile(__FILE__);

$arTszhTicketsUserTypes = Array(
	array (
	  'ENTITY_ID' => 'TSZH_TICKET',
	  'FIELD_NAME' => 'UF_FIO',
	  'USER_TYPE_ID' => 'string',
	  'XML_ID' => '',
	  'SORT' => '100',
	  'MULTIPLE' => 'N',
	  'MANDATORY' => 'N',
	  'SHOW_FILTER' => 'E',
	  'SHOW_IN_LIST' => 'Y',
	  'EDIT_IN_LIST' => 'Y',
	  'IS_SEARCHABLE' => 'Y',
	  'SETTINGS' =>
	  array (
	    'SIZE' => 40,
	    'ROWS' => 1,
	    'REGEXP' => '',
	    'MIN_LENGTH' => 0,
	    'MAX_LENGTH' => 0,
	    'DEFAULT_VALUE' => '',
	  ),
	  "EDIT_FORM_LABEL" => Array("ru" => GetMessage("CTT_OWNER_NAME")),
	  "LIST_COLUMN_LABEL" => Array("ru" => GetMessage("CTT_OWNER_NAME")),
	  "LIST_FILTER_LABEL" => Array("ru" => GetMessage("CTT_OWNER_NAME")),
	),
	array (
	  'ENTITY_ID' => 'TSZH_TICKET',
	  'FIELD_NAME' => 'UF_ACCOUNT',
	  'USER_TYPE_ID' => 'string',
	  'XML_ID' => '',
	  'SORT' => '110',
	  'MULTIPLE' => 'N',
	  'MANDATORY' => 'N',
	  'SHOW_FILTER' => 'N',
	  'SHOW_IN_LIST' => 'Y',
	  'EDIT_IN_LIST' => 'Y',
	  'IS_SEARCHABLE' => 'Y',
	  'SETTINGS' =>
	  array (
	    'SIZE' => 40,
	    'ROWS' => 1,
	    'REGEXP' => '',
	    'MIN_LENGTH' => 0,
	    'MAX_LENGTH' => 0,
	    'DEFAULT_VALUE' => '',
	  ),
	  "EDIT_FORM_LABEL" => Array("ru" => GetMessage("CTT_OWNER_ACCOUNT")),
	  "LIST_COLUMN_LABEL" => Array("ru" => GetMessage("CTT_OWNER_ACCOUNT")),
	  "LIST_FILTER_LABEL" => Array("ru" => GetMessage("CTT_OWNER_ACCOUNT")),
	),
	array (
	  'ENTITY_ID' => 'TSZH_TICKET',
	  'FIELD_NAME' => 'UF_ADDRESS_BUILDING',
	  'USER_TYPE_ID' => 'string',
	  'XML_ID' => '',
	  'SORT' => '120',
	  'MULTIPLE' => 'N',
	  'MANDATORY' => 'N',
	  'SHOW_FILTER' => 'E',
	  'SHOW_IN_LIST' => 'Y',
	  'EDIT_IN_LIST' => 'Y',
	  'IS_SEARCHABLE' => 'Y',
	  'SETTINGS' =>
	  array (
	    'SIZE' => 40,
	    'ROWS' => 1,
	    'REGEXP' => '',
	    'MIN_LENGTH' => 0,
	    'MAX_LENGTH' => 0,
	    'DEFAULT_VALUE' => '',
	  ),
	  "EDIT_FORM_LABEL" => Array("ru" => GetMessage("CTT_OWNER_ADDRESS")),
	  "LIST_COLUMN_LABEL" => Array("ru" => GetMessage("CTT_OWNER_ADDRESS")),
	  "LIST_FILTER_LABEL" => Array("ru" => GetMessage("CTT_OWNER_ADDRESS")),
	),
	array (
	  'ENTITY_ID' => 'TSZH_TICKET',
	  'FIELD_NAME' => 'UF_ADDRESS_FLAT',
	  'USER_TYPE_ID' => 'string',
	  'XML_ID' => '',
	  'SORT' => '130',
	  'MULTIPLE' => 'N',
	  'MANDATORY' => 'N',
	  'SHOW_FILTER' => 'E',
	  'SHOW_IN_LIST' => 'Y',
	  'EDIT_IN_LIST' => 'Y',
	  'IS_SEARCHABLE' => 'Y',
	  'SETTINGS' =>
	  array (
	    'SIZE' => 5,
	    'ROWS' => 1,
	    'REGEXP' => '',
	    'MIN_LENGTH' => 0,
	    'MAX_LENGTH' => 0,
	    'DEFAULT_VALUE' => '',
	  ),
	  "EDIT_FORM_LABEL" => Array("ru" => GetMessage("CTT_OWNER_FLAT")),
	  "LIST_COLUMN_LABEL" => Array("ru" => GetMessage("CTT_OWNER_FLAT")),
	  "LIST_FILTER_LABEL" => Array("ru" => GetMessage("CTT_OWNER_FLAT")),
	),
);

$rsEntities = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID" => "TSZH_TICKET"));
$arExistingEntities = Array();
while ($arEntity = $rsEntities->Fetch())
	$arExistingEntities[$arEntity["ENTITY_ID"]] = true;

$userTypeEntity = new CUserTypeEntity();
foreach ($arTszhTicketsUserTypes as $arTszhTicketsUserType)
	if (!array_key_exists($arTszhTicketsUserType['ENTITY_ID'], $arExistingEntities))
		$userTypeEntity->Add($arTszhTicketsUserType);

?>