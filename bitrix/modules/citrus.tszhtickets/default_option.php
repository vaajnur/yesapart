<?
$citrus_tszhtickets_default_option = array(
	"SUPPORT_DIR"					=> "#SITE_DIR#personal/support/",
	"SUPPORT_MAX_FILESIZE"			=> "300",
	"ONLINE_INTERVAL"				=> "300",
	"DEFAULT_VALUE_HIDDEN"			=> "N",
	"NOT_IMAGE_EXTENSION_SUFFIX"	=> "_",
	"NOT_IMAGE_UPLOAD_DIR"			=> "tszhtickets/not_image",
	"DEFAULT_AUTO_CLOSE_DAYS"		=> "7",
	"MAX_FILESIZE"					=> "0",
	"VIEW_TICKET_DEFAULT_MODE"	=> "view",
	"ONLINE_AUTO_REFRESH"			=> "60",
	"TICKETS_PER_PAGE"				=> "50",
	"MESSAGES_PER_PAGE"				=> "50",
	"CURRENT_TICKET_NUMBER"			=> "0",
	"CURRENT_TICKET_YEAR"			=> "0",
	);
?>