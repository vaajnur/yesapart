<?php

IncludeModuleLangFile(__FILE__);

Class CAllCitrusPollGroup
{
	// TODO ���� ACCESS � ����� �� �����������: ����������, ���� �������� ���!
	public static function GetAccess($ID)
	{
		global $USER;
		$GROUP = $USER->GetUserGroupArray();
		$GROUP = array_flip($GROUP);

		$ID = IntVal($ID);
		if ($ID > 0)
		{
			$arAccess = CCitrusPollGroup::GetByID($ID);
			if (is_array($arAccess))
			{
				if (array_key_exists($arAccess['ACCESS'], $GROUP) || $arAccess['ACCESS'] == 2)
				{
					return true;
				}
			}
		}

		return false;
	}

	public static function GetPermissionVoteAccess($ID)
	{
		global $USER;
		$GROUP = $USER->GetUserGroupArray();
		$GROUP = array_flip($GROUP);
		$VOTE_ACCESS = array("ACCESS" => false, "ACCESS_EDIT" => false, "ACCESS_SHOW" => false);

		$ID = IntVal($ID);
		if ($ID > 0)
		{
			$arAccess = CCitrusPoll::GetByID($ID);
			if ($arAccess)
			{

				if (array_key_exists($arAccess['ACCESS'], $GROUP) || $arAccess['ACCESS'] == 2)
				{
					$VOTE_ACCESS["ACCESS"] = true;
				}

				if (array_key_exists($arAccess['ACCESS_EDIT'], $GROUP) || $arAccess['ACCESS_EDIT'] == 2)
				{
					$VOTE_ACCESS["ACCESS_EDIT"] = true;
				}

				if (array_key_exists($arAccess['ACCESS_SHOW'], $GROUP) || $arAccess['ACCESS_SHOW'] == 2)
				{
					$VOTE_ACCESS["ACCESS_SHOW"] = true;
				}

				return $VOTE_ACCESS;
			}
		}

		return false;
	}
}

