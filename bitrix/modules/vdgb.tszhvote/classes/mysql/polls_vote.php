<?

IncludeModuleLangFile(__FILE__);

class CCitrusPollVote
{
	const tableName = 'b_citrus_voting_event';

	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		static $arFields = array(
			"ID" => Array("FIELD" => "CVE.ID", "TYPE" => "int"),
			"USER_ID" => Array("FIELD" => "CVE.USER_ID", "TYPE" => "int"),
			"VOTING_ID" => Array("FIELD" => "CVE.VOTING_ID", "TYPE" => "int"),
			"WEIGHT" => Array("FIELD" => "CVE.WEIGHT", "TYPE" => "float"),
			"ENTITY_ID" => Array("FIELD" => "CVE.ENTITY_ID", "TYPE" => "int"),
			"TIMESTAMP_X" => Array("FIELD" => "CVE.TIMESTAMP_X", "TYPE" => "datetime"),
			"IP" => Array("FIELD" => "CVE.IP", "TYPE" => "string"),
			"QUESTION_ID" => Array("FIELD" => "CVE.QUESTION_ID", "TYPE" => "int"),
			"VALID" => Array("FIELD" => "CVE.VALID", "TYPE" => "char"),

			"ENTITY_TYPE" => Array("FIELD" => "CV.ENTITY_TYPE", "TYPE" => "string", "FROM" => "INNER JOIN b_citrus_voting CV ON (CVE.VOTING_ID = CV.ID)"),
			"VOTING_NAME" => Array("FIELD" => "CV.NAME", "TYPE" => "string", "FROM" => "INNER JOIN b_citrus_voting CV ON (CVE.VOTING_ID = CV.ID)"),
			"QUESTION_TEXT" => Array("FIELD" => "CVQ.TEXT", "TYPE" => "string", "FROM" => "INNER JOIN b_citrus_voting_question CVQ ON (CVE.QUESTION_ID = CVQ.ID)"),

		);

		if (count($arSelectFields) <= 0 || array_key_exists('*', $arSelectFields))
		{
			$arSelectFields = array_keys($arFields);
		}

		return CCitrusPolls::GetListMakeQuery(self::tableName . ' CVE', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
	}

	protected static function CheckFields(&$arFields, $strOperation = 'ADD')
	{
		global $APPLICATION;

		if (array_key_exists("VOTING_ID", $arFields) || $strOperation == 'ADD')
		{
			$arFields['VOTING_ID'] = IntVal($arFields['VOTING_ID']);
			if (!is_array(CCitrusPoll::GetByID($arFields['VOTING_ID'])))
			{
				$APPLICATION->ThrowException(GetMessage("CVE_VOTING_NOT_FOUND"));

				return false;
			}
		}

		if (array_key_exists("QUESTION_ID", $arFields) || $strOperation == 'ADD')
		{
			$arFields['QUESTION_ID'] = IntVal($arFields['QUESTION_ID']);
			if (!is_array(CCitrusPollQuestion::GetByID($arFields['QUESTION_ID'])))
			{
				$APPLICATION->ThrowException(GetMessage("CVE_QUESTION_NOT_FOUND"));

				return false;
			}
		}

		if ($strOperation == "ADD")
		{
			if (!array_key_exists('VALID', $arFields))
				$arFields["VALID"] = "Y";
			if (!array_key_exists('IP', $arFields))
				$arFields['IP'] = $_SERVER['REMOTE_ADDR'];
		}

		// TODO �������� ��������
		return true;
	}

	public static function Add($arFields)
	{
		global $DB;

		if (!self::CheckFields($arFields, 'ADD'))
			return false;

		$ID = CDatabase::Add(self::tableName, $arFields);

		return $ID;
	}

	public static function IsVoted($VOTING_ID, $ENTITY_ID = false)
	{
		global $APPLICATION;
		$APPLICATION->ResetException();

		$arVoting = CCitrusPoll::GetByID($VOTING_ID);
		if (!is_array($arVoting))
		{
			$APPLICATION->ThrowException(GetMessage("CVE_VOTING_NOT_FOUND"));

			return false;
		}

		$ENTITY_ID = IntVal($ENTITY_ID);
		if ($ENTITY_ID <= 0)
		{
			$obVotingEntity = CCitrusPollEntityBase::GetObj($arVoting['ENTITY_TYPE']);
			if (!is_object($obVotingEntity))
			{
				$APPLICATION->ThrowException(GetMessage("CVE_ENTITY_TYPE_NOT_FOUND"));

				return false;
			}
			$ENTITY_ID = $obVotingEntity->GetCurrentID();
		}

		$cntVotes = CCitrusPollVote::GetList(Array(), Array("VOTING_ID" => $VOTING_ID, "ENTITY_ID" => $ENTITY_ID), Array());

		return $cntVotes > 0;
	}

	public static function DoVote($VOTING_ID, $arAnswers)
	{
		global $APPLICATION;
		$APPLICATION->ResetException();

		$arVoting = CCitrusPoll::GetByID($VOTING_ID);
		if (!is_array($arVoting))
		{
			$APPLICATION->ThrowException(GetMessage("CVE_VOTING_NOT_FOUND"));

			return false;
		}

		if (!self::CanVote($VOTING_ID, $arVoting))
		{
			$APPLICATION->ThrowException(GetMessage("CVE_CAN_NOT_VOTE"));

			return false;
		}

		$obVotingEntity = CCitrusPollEntityBase::GetObj($arVoting['ENTITY_TYPE']);
		if (!is_object($obVotingEntity))
		{
			$APPLICATION->ThrowException(GetMessage("CVE_ENTITY_TYPE_NOT_FOUND"));

			return false;
		}
		$ENTITY_ID = $obVotingEntity->GetCurrentID();
		if ($ENTITY_ID <= 0)
		{
			$APPLICATION->ThrowException(GetMessage("CVE_CURRENT_ENTITY_NOT_FOUND"));

			return false;
		}

		$arQuestions = Array();
		$rsQuestions = CCitrusPollQuestion::GetList(Array("SORT" => "ASC"), Array("VOTING_ID" => $arVoting['ID']));
		while ($arQuestion = $rsQuestions->GetNext(false))
			$arQuestions[$arQuestion['ID']] = $arQuestion;

		$obVotingMethod = CCitrusPollMethodBase::GetMethodObj($arVoting['VOTING_METHOD']);

		$arVoteIDs = Array();
		foreach ($arAnswers as $QUESTION_ID => $arAnswerIDs)
		{
			// �������� �� �� ������������ � ������ ������ ������
			if (!array_key_exists($QUESTION_ID, $arQuestions))
				continue;

			$QUESTION_ID = IntVal($QUESTION_ID);

			// ��������� �������� ������
			$rsQuestionAnswers = CCitrusPollAnswer::GetList(Array(), Array("QUESTION_ID" => $QUESTION_ID));
			$arAllowedAnswerIDs = Array();
			while ($arQuestionAnswer = $rsQuestionAnswers->Fetch())
			{
				$arAllowedAnswerIDs[$arQuestionAnswer['ID']] = true;
			}
			$arAllowedAnswerIDs = array_keys($arAllowedAnswerIDs);
			$arAnswerIDs = array_intersect($arAnswerIDs, $arAllowedAnswerIDs);

			$WEIGHT = is_object($obVotingMethod) ? $obVotingMethod->GetVoteVolume($ENTITY_ID, count($arAnswerIDs)) : 0; // TODO check

			$VOTING_EVENT_ID = self::Add(Array(
				"USER_ID" => $GLOBALS["USER"]->GetID(),
				"VOTING_ID" => $VOTING_ID,
				"ENTITY_ID" => $ENTITY_ID,
				"WEIGHT" => $WEIGHT,
				"QUESTION_ID" => $QUESTION_ID,
				"VALID" => "Y",
			));
			if ($VOTING_EVENT_ID <= 0)
				return false;

			$arVoteIDs[] = $VOTING_EVENT_ID;

			if ($arQuestions[$QUESTION_ID]['IS_MULTIPLE'] != "Y" && count($arAnswers) > 0)
				$arAnswerIDs = array_slice($arAnswerIDs, 0, 1);

			foreach ($arAnswerIDs as $ANSWER_ID)
			{
				CCitrusPollVoteAnswer::Add(Array(
					"VOTING_EVENT_ID" => $VOTING_EVENT_ID,
					"ANSWER_ID" => $ANSWER_ID,
				));
			}
		}

		return $arVoteIDs;
	}

	public static function CanVote($VOTING_ID, $arFields = false)
	{
		global $DB, $USER;

        if (!is_array($arFields) || count($arFields) <= 0)
            $arFields = CCitrusPoll::GetByID($VOTING_ID);

        // TODO �������� �������� ����� ���������� �������� ����������� (entity)

        // ��� ����������� ������� ������ �������� �������� �� ������������ ���������� �/�,
        // � ����������� �� �� ������� ��������� (���� �� ����� ��� �����������)
        if ($arFields["ENTITY_TYPE"] == "tszh_account") {
            if (!$USER->IsAuthorized())
                return false;

            if (!CModule::IncludeModule("citrus.tszh"))
                return false;

            $account = CTszhAccount::GetByUserID($USER->GetID());
            if (!is_array($account))
                return false;
            elseif ($arFields["TSZH_ID"] > 0 && $account["TSZH_ID"] !== $arFields["TSZH_ID"])
                return false;

        }

		if (self::IsVoted($VOTING_ID))
			return false;

		if ($arFields['ACTIVE'] != "Y" || $arFields['VOTING_COMPLETED'] == "Y")
			return false;

		$dateNow = ConvertTimeStamp(time());
		if ($DB->CompareDates($dateNow, $arFields['DATE_BEGIN']) < 0)
			return false;
		if ($DB->CompareDates($dateNow, $arFields['DATE_END']) > 0)
			return false;

		return true;
	}

	public static function UpdateEx($ID, $arFields)
	{
		return self::Update($ID, $arFields);
	}

	public static function Update($ID, $arFields)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		if (!self::CheckFields($arFields, 'UPDATE'))
			return false;

		$strUpdate = $DB->PrepareUpdate(self::tableName, $arFields);
		$strSql = "UPDATE " . self::tableName . " SET " . $strUpdate . " WHERE ID=" . $ID;
		$DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

		return true;
	}

	public static function Delete($ID)
	{
		global $DB;

		$ID = intval($ID);
		if ($ID <= 0)
			return false;

		$strSql = "DELETE FROM b_citrus_voting_event_answer WHERE VOTING_EVENT_ID = " . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		$strSql = "DELETE FROM " . self::tableName . " WHERE ID=" . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		return true;
	}

	public static function GetByID($ID)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		if (isset($GLOBALS["CITRUS_POLLS"]["VOTES_CACHE_" . $ID]) && is_array($GLOBALS["CITRUS_POLLS"]["VOTES_CACHE_" . $ID]) && is_set($GLOBALS["CITRUS_POLLS"]["VOTES_CACHE_" . $ID], "ID"))
		{
			return $GLOBALS["CITRUS_POLLS"]["VOTES_CACHE_" . $ID];
		}
		else
		{
			$strSql = "
SELECT 
	ID,
	VOTING_ID,
	WEIGHT,
	" . $DB->DateToCharFunction("TIMESTAMP_X") . " TIMESTAMP_X,
	IP,
	QUESTION_ID,
	VALID
FROM " . self::tableName . " 
WHERE ID = " . $ID . " ";

			$db = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			if ($ar = $db->Fetch())
			{
				$GLOBALS["CITRUS_POLLS"]["VOTES_CACHE_" . $ar['ID']] = $ar;

				return $ar;
			}
		}
	}

	public static function Reset($voting_id)
	{
		global $DB;

		$voting_id = IntVal($voting_id);

		if (IntVal($voting_id) > 0)
		{
			$strSQL = "DELETE FROM `b_citrus_voting_event` WHERE `VOTING_ID` = " . $voting_id;
			$res = $DB->Query($strSQL, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

			return true;
		}

		return false;
	}
}
