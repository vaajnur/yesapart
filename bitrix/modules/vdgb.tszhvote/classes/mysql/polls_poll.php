<?

IncludeModuleLangFile(__FILE__);

class CCitrusPoll
{
	const tableName = "b_citrus_voting";
	const userFieldEntity = 'CITRUS_POLL';

    /**
     * @param array $arOrder
     * @param array $arFilter
     * @param bool|array $arGroupBy
     * @param bool|array $arNavStartParams
     * @param array $arSelectFields
     * @return bool|CDBResult
     */
    public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		static $arFields = array(
			"ID" => Array("FIELD" => "CV.ID", "TYPE" => "int"),
			"GROUP_ID" => Array("FIELD" => "CV.GROUP_ID", "TYPE" => "int"),
            "TSZH_ID" => Array("FIELD" => "CV.TSZH_ID", "TYPE" => "int"),
			"DATE_BEGIN" => Array("FIELD" => "CV.DATE_BEGIN", "TYPE" => "date"),
			"DATE_END" => Array("FIELD" => "CV.DATE_END", "TYPE" => "date"),
			"NAME" => Array("FIELD" => "CV.NAME", "TYPE" => "string"),
			"DETAIL_TEXT" => Array("FIELD" => "CV.DETAIL_TEXT", "TYPE" => "string"),
			"ACTIVE" => Array("FIELD" => "CV.ACTIVE", "TYPE" => "char"),
			"PERCENT_NEEDED" => Array("FIELD" => "CV.PERCENT_NEEDED", "TYPE" => "int"),
			"VOTING_METHOD" => Array("FIELD" => "CV.VOTING_METHOD", "TYPE" => "string"),
			"ENTITY_TYPE" => Array("FIELD" => "CV.ENTITY_TYPE", "TYPE" => "string"),
			"CREATED_BY" => Array("FIELD" => "CV.CREATED_BY", "TYPE" => "int"),
			"TITLE_TEXT" => Array("FIELD" => "CV.TITLE_TEXT", "TYPE" => "string"),
			"VOTING_COMPLETED" => Array("FIELD" => "CV.VOTING_COMPLETED", "TYPE" => "char"),
		);

		if (count($arSelectFields) <= 0 || array_key_exists('*', $arSelectFields))
		{
			$arSelectFields = array_keys($arFields);
		}

		$obUserFieldsSql = new CUserTypeSQL;
		$obUserFieldsSql->SetEntity(self::userFieldEntity, "CV.ID");
		$obUserFieldsSql->SetSelect($arSelectFields);
		$obUserFieldsSql->SetFilter($arFilter);
		$obUserFieldsSql->SetOrder($arOrder);

		$dbRes = CCitrusPolls::GetListMakeQuery(self::tableName . ' CV', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields, $obUserFieldsSql);
		if (is_object($dbRes))
			$dbRes->SetUserFields($GLOBALS['USER_FIELD_MANAGER']->GetUserFields(self::userFieldEntity));

		return $dbRes;
	}

	protected static function CheckFields(&$arFields, $strOperation = 'ADD')
	{
		// TODO �������� ��������

		if (array_key_exists('ID', $arFields))
			unset($arFields['ID']);

		if (array_key_exists('CREATED_BY', $arFields) && $strOperation == "UPDATE")
			unset($arFields['CREATED_BY']);
		elseif ($strOperation == 'ADD')
			$arFields['CREATED_BY'] = $GLOBALS['USER']->GetID();

		return true;
	}

	public static function Add($arFields)
	{
		if (!self::CheckFields($arFields, 'ADD'))
			return false;

		$ID = CDatabase::Add(self::tableName, $arFields);
		if ($ID > 0)
			$GLOBALS['USER_FIELD_MANAGER']->Update(self::userFieldEntity, $ID, $arFields);

		return $ID;
	}

	public static function UpdateEx($ID, $arFields)
	{
		return self::Update($ID, $arFields);
	}

	public static function Update($ID, $arFields)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		if (!self::CheckFields($arFields, 'UPDATE'))
			return false;

		$strUpdate = $DB->PrepareUpdate(self::tableName, $arFields);
		$strSql = "UPDATE " . self::tableName . " SET " . $strUpdate . " WHERE ID=" . $ID;
		if ($DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__))
			$GLOBALS['USER_FIELD_MANAGER']->Update(self::userFieldEntity, $ID, $arFields);

		return true;
	}

	public static function Delete($ID)
	{
		global $DB;

		$ID = intval($ID);
		if ($ID <= 0)
			return false;

		$rsQuestions = CCitrusPollQuestion::GetList(Array(), Array("VOTING_ID" => $ID), false, false, Array("ID"));
		while ($arQuestion = $rsQuestions->Fetch())
			CCitrusPollQuestion::Delete($arQuestion['ID']);

		$strSql = "DELETE FROM " . self::tableName . " WHERE ID=" . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		return true;
	}

	public static function GetByID($ID)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		if (isset($GLOBALS["CITRUS_POLLS"]["CACHE_" . $ID]) && is_array($GLOBALS["CITRUS_POLLS"]["CACHE_" . $ID]) && is_set($GLOBALS["CITRUS_POLLS"]["CACHE_" . $ID], "ID"))
		{
			return $GLOBALS["CITRUS_POLLS"]["CACHE_" . $ID];
		}
		else
		{
            $ar = self::GetList(array(), array("ID" => $ID), false, false, array('*'))->GetNext(true, false);
            $GLOBALS["CITRUS_POLLS"]["CACHE_" . $ar['ID']] = $ar;

            return $ar;
		}
	}
}

