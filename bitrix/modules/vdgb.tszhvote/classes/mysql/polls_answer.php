<?

IncludeModuleLangFile(__FILE__);

class CCitrusPollAnswer
{
	const tableName = 'b_citrus_voting_answers';

	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		static $arFields = array(
			"ID" => Array("FIELD" => "CVA.ID", "TYPE" => "int"),
			"ACTIVE" => Array("FIELD" => "CVA.ACTIVE", "TYPE" => "char"),
			"QUESTION_ID" => Array("FIELD" => "CVA.QUESTION_ID", "TYPE" => "int"),
			"TEXT" => Array("FIELD" => "CVA.TEXT", "TYPE" => "string"),
			"TIMESTAMP_X" => Array("FIELD" => "CVA.TIMESTAMP_X", "TYPE" => "datetime"),
			"SORT" => Array("FIELD" => "CVA.SORT", "TYPE" => "int"),
			"COLOR" => Array("FIELD" => "CVA.COLOR", "TYPE" => "string"),
		);
		if (count($arSelectFields) <= 0 || array_key_exists('*', $arSelectFields))
		{
			$arSelectFields = array_keys($arFields);
		}

		return CCitrusPolls::GetListMakeQuery(self::tableName . ' CVA', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
	}

	protected static function CheckFields(&$arFields, $strOperation = 'ADD')
	{
		// TODO �������� ��������
		return true;
	}

	public static function Add($arFields)
	{
		global $DB;

		if (!self::CheckFields($arFields, 'ADD'))
			return false;

		$ID = CDatabase::Add(self::tableName, $arFields);

		return $ID;
	}

	public static function UpdateEx($ID, $arFields)
	{
		return self::Update($ID, $arFields);
	}

	public static function Update($ID, $arFields)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		if (!self::CheckFields($arFields, 'UPDATE'))
			return false;

		$strUpdate = $DB->PrepareUpdate(self::tableName, $arFields);
		$strSql = "UPDATE " . self::tableName . " SET " . $strUpdate . " WHERE ID=" . $ID;
		$DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

		return true;
	}

	public static function Delete($ID)
	{
		global $DB;

		$ID = intval($ID);
		if ($ID <= 0)
			return false;

		$strSql = "DELETE FROM b_citrus_voting_event_answer WHERE ANSWER_ID=" . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		$strSql = "DELETE FROM " . self::tableName . " WHERE ID=" . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		return true;
	}
        
        /*
         * ������� ������ �� ������� �����������
         */
        public static function DeleteByVotingEventId($ID)
	{
		global $DB;

		$ID = intval($ID);
		if ($ID <= 0)
			return false;

		$strSql = "DELETE FROM " . self::tableName . " WHERE VOTING_EVENT_ID=" . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		return true;
	}

	public static function GetByID($ID)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		if (isset($GLOBALS["CITRUS_POLLS"]["ANSWERS_CACHE_" . $ID]) && is_array($GLOBALS["CITRUS_POLLS"]["ANSWERS_CACHE_" . $ID]) && is_set($GLOBALS["CITRUS_POLLS"]["ANSWERS_CACHE_" . $ID], "ID"))
		{
			return $GLOBALS["CITRUS_POLLS"]["ANSWERS_CACHE_" . $ID];
		}
		else
		{
			$strSql = "SELECT * " . "FROM " . self::tableName . " CV " . "WHERE CV.ID = " . $ID . " ";

			$db = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			if ($ar = $db->Fetch())
			{
				$GLOBALS["CITRUS_POLLS"]["ANSWERS_CACHE_" . $ar['ID']] = $ar;

				return $ar;
			}
		}
	}
}

