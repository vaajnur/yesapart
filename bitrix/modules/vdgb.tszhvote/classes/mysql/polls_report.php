<?

class CCitrusPollReport
{
	public static function GetTemplates()
	{
		static $arTemplates = false;
		if ($arTemplates !== false)
			return $arTemplates;

		$arPaths = Array($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/vdgb.tszhvote/reports/" . LANGUAGE_ID . '/', $_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/vdgb.tszhvote/reports/",);
		foreach ($arPaths as $strPath)
		{
			if (is_dir($strPath) && $hDir = opendir($strPath))
			{
				while (false !== ($entry = readdir($hDir)))
				{
					if ($entry != '.' && $entry != '..' && is_dir($strPath . $entry) && file_exists($strPath . $entry . '/.description.php'))
					{
						$arTemplateDescription = false;
						@include($strPath . $entry . '/.description.php');
						if (is_array($arTemplateDescription) && isset($arTemplateDescription["NAME"]))
						{
							$arTemplates[$entry] = array_merge(Array('PATH' => $strPath . $entry . '/template.php'), $arTemplateDescription);
						}
					}
				}
				closedir($hDir);
			}
		}

		return $arTemplates;
	}

	public static function GetData($VOTING_ID)
	{
		$arVoting = CCitrusPoll::GetByID($VOTING_ID);
		if (!is_array($arVoting))
			return false;

		$arResult = array("VOTING" => $arVoting);

		//������� ������ �������� ��� ������� �����������
		$rsQuestions = CCitrusPollQuestion::GetList(array("SORT" => "ASC"), array("VOTING_ID" => $arVoting['ID'], "ACTIVE" => "Y"));
		while ($arQuestion = $rsQuestions->Fetch())
		{

			$rsAnswer = CCitrusPollAnswer::GetList(array(), array("QUESTION_ID" => $arQuestion["ID"], "ACTIVE" => "Y",));
			while ($arAnswer = $rsAnswer->Fetch())
			{
				$arQuestion["ANSWERS"][$arAnswer['ID']] = $arAnswer;
			}

			$arAnswerSums = Array();
			$rsAnswers = CCitrusPollVoteAnswer::GetList(Array(), Array("VOTE_QUESTION_ID" => $arQuestion['ID']), Array("ANSWER_ID", "VOTE_QUESTION_ID", "SUM" => 'VOTE_WEIGHT'));
			while ($arAnswer = $rsAnswers->GetNext(false))
			{
				$arQuestion['ANSWERS'][$arAnswer['ANSWER_ID']]["SUM"] = $arAnswer["VOTE_WEIGHT"];
				// TODO WTF?
				/*if ($sampleEntity === false)
					$sampleEntity = $arAnswer["ENTITY_ID"];*/
			}

			$obMethod = CCitrusPollMethodBase::GetMethodObj($arVoting['VOTING_METHOD']);
			if (is_object($obMethod))
			{
				$arResult['VOTING_METHOD'] = $obMethod;
				$ar = CCitrusPollVote::GetList(Array(), Array("QUESTION_ID" => $arQuestion["ID"], false, Array('nTopCount' => 1)), Array("ENTITY_ID"))->Fetch();
				$totalVolume = $obMethod->GetTotalVolume($ar['ENTITY_ID']);
				if ($totalVolume > 0)
				{
					$votedVolume = 0;
					foreach ($arQuestion['ANSWERS'] as $arAnswer)
						$votedVolume += $arAnswer["SUM"];
					$arQuestion['ANSWERS']['-'] = Array(
						"COLOR" => "#CCCCCC",
						"TEXT" => "(�� ����������)", // TODO ������� � �������� ���������
						"SUM" => $totalVolume - $votedVolume,
					);
					$arResult['VOTING']['TOTAL_VOLUME'] = $obMethod->FormatVolume($totalVolume, $bHTML = true);
				}
			}

			$arResult["QUESTIONS"][$arQuestion['ID']] = $arQuestion;
		}

		return $arResult;
	}
}
