<?

IncludeModuleLangFile(__FILE__);

class CCitrusPollQuestion
{
	const tableName = 'b_citrus_voting_question';

	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		global $DB;

		static $arFields = array(
			"ID" => Array("FIELD" => "CVQ.ID", "TYPE" => "int"),
			"ACTIVE" => Array("FIELD" => "CVQ.ACTIVE", "TYPE" => "char"),
			"VOTING_ID" => Array("FIELD" => "CVQ.VOTING_ID", "TYPE" => "int"),
			"TEXT" => Array("FIELD" => "CVQ.TEXT", "TYPE" => "string"),
			"SORT" => Array("FIELD" => "CVQ.SORT", "TYPE" => "int"),
			"IS_MULTIPLE" => Array("FIELD" => "CVQ.IS_MULTIPLE", "TYPE" => "char"),
		);

		if (count($arSelectFields) <= 0 || array_key_exists('*', $arSelectFields))
		{
			$arSelectFields = array_keys($arFields);
		}

		return CCitrusPolls::GetListMakeQuery(self::tableName . ' CVQ', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
	}

	protected static function CheckFields(&$arFields, $strOperation = 'ADD')
	{
		// TODO �������� ��������
		return true;
	}

	public static function Add($arFields)
	{
		global $DB;

		if (!self::CheckFields($arFields, 'ADD'))
			return false;

		$ID = CDatabase::Add(self::tableName, $arFields);

		return $ID;
	}

	public static function UpdateEx($ID, $arFields)
	{
		return self::Update($ID, $arFields);
	}

	public static function Update($ID, $arFields)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		if (!self::CheckFields($arFields, 'UPDATE'))
			return false;

		$strUpdate = $DB->PrepareUpdate(self::tableName, $arFields);
		$strSql = "UPDATE " . self::tableName . " SET " . $strUpdate . " WHERE ID=" . $ID;
		$DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

		return true;
	}

	public static function Delete($ID)
	{
		global $DB;

		$ID = intval($ID);
		if ($ID <= 0)
			return false;

		$strSql = "DELETE FROM b_citrus_voting_event_answer WHERE VOTING_EVENT_ID in (SELECT ID FROM b_citrus_voting_event WHERE QUESTION_ID = {$ID})";
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		$strSql = "DELETE FROM b_citrus_voting_event WHERE QUESTION_ID=" . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		$strSql = "DELETE FROM b_citrus_voting_answers WHERE QUESTION_ID=" . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		$strSql = "DELETE FROM " . self::tableName . " WHERE ID=" . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		return true;
	}

	public static function DeleteByVoting($VOTING_ID)
	{
		global $DB;

		$VOTING_ID = intval($VOTING_ID);
		if ($VOTING_ID <= 0)
			return false;

		$strSql = "DELETE FROM " . self::tableName . " WHERE VOTING_ID=" . $VOTING_ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		return true;
	}

	public static function GetByID($ID)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		if (isset($GLOBALS["CITRUS_POLLS"]["QUESTION_CACHE_" . $ID]) && is_array($GLOBALS["CITRUS_POLLS"]["QUESTION_CACHE_" . $ID]) && is_set($GLOBALS["CITRUS_POLLS"]["QUESTION_CACHE_" . $ID], "ID"))
		{
			return $GLOBALS["CITRUS_POLLS"]["QUESTION_CACHE_" . $ID];
		}
		else
		{
			$strSql = "SELECT * " . "FROM " . self::tableName . " CVQ " . "WHERE CVQ.ID = " . $ID . " ";

			$db = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			if ($ar = $db->Fetch())
			{
				$GLOBALS["CITRUS_POLLS"]["QUESTION_CACHE_" . $ar['ID']] = $ar;

				return $ar;
			}
		}
	}
}
