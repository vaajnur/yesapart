<?

IncludeModuleLangFile(__FILE__);

class CCitrusPollEntityBase
{

	var $name;
	var $description;
	var $code;

	public function GetCurrentID()
	{
		return false;
	}

	public function GetEntityName($ID = false)
	{
		return false;
	}

	public function GetName()
	{
		return $this->name;
	}

	public function GetDescription()
	{
		return $this->description;
	}

	public function GetCode()
	{
		return $this->code;
	}

	static function GetList()
	{
		static $arMethods = false;
		if ($arMethods !== false)
			return $arMethods;

		$arPaths = Array($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/vdgb.tszhvote/entity/" . LANGUAGE_ID . '/', $_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/vdgb.tszhvote/entity/",);

		foreach ($arPaths as $strPath)
		{
            if (is_dir($strPath)) {
                if ($hDir = opendir($strPath)) {
                    while (false !== ($entry = readdir($hDir))) {
                        if ($entry != '.' && $entry != '..' && is_dir($strPath . $entry) && file_exists($strPath . $entry . '/include.php')) {
                            $arMethodDesc = false;
                            @include($strPath . $entry . '/.description.php');
                            if (is_array($arMethodDesc) && isset($arMethodDesc["CLASS"])) {
                                $arMethods[$arMethodDesc['CODE']] = array_merge(
                                    Array('PATH' => $strPath . $entry . '/include.php'),
                                    $arMethodDesc
                                );
                            }
                        }
                    }
                    closedir($hDir);
                }
            }
		}

		return $arMethods;
	}

	public static function GetGridList()
	{
		$ar = self::GetList();
		$arResult = Array('' => '');
		foreach ($ar as $key => $value)
		{
			$arResult[$value["CODE"]] = $value["NAME"];
		}

		return $arResult;
	}

	public static function GetObj($code)
	{
		$obj = false;
		$arMethods = self::GetList();
		if (array_key_exists($code, $arMethods))
		{
			$arMethodDesc = $arMethods[$code];
			if (!class_exists($arMethodDesc["CLASS"]))
				@include($arMethodDesc['PATH']);
			if (class_exists($arMethodDesc["CLASS"]))
			{
				$obj = new $arMethodDesc['CLASS'];
			}
		}

		return $obj;
	}
}
