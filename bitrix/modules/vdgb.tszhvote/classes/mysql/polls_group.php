<?

IncludeModuleLangFile(__FILE__);

class CCitrusPollGroup extends CAllCitrusPollGroup
{
	const tableName = 'b_citrus_voting_group';

    /**
     * @param array $arOrder
     * @param array $arFilter
     * @param bool|array $arGroupBy
     * @param bool|array $arNavStartParams
     * @param array $arSelectFields
     * @return bool|CDBResult
     */
    public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		global $DB;

		static $arFields = array(
			"ID" => Array("FIELD" => "CVG.ID", "TYPE" => "int"),
			"TIMESTAMP_X" => Array("FIELD" => "CVG.TIMESTAMP_X", "TYPE" => "datetime"),
			"LID" => Array("FIELD" => "CVG.LID", "TYPE" => "string"),
			"NAME" => Array("FIELD" => "CVG.NAME", "TYPE" => "string"),
			"CODE" => Array("FIELD" => "CVG.CODE", "TYPE" => "string"),
			"SORT" => Array("FIELD" => "CVG.SORT", "TYPE" => "int"),
			"ACTIVE" => Array("FIELD" => "CVG.ACTIVE", "TYPE" => "char"),
			"XML_ID" => Array("FIELD" => "CVG.XML_ID", "TYPE" => "string"),
			"TITLE" => Array("FIELD" => "CVG.TITLE", "TYPE" => "string"),
		);

		if (count($arSelectFields) <= 0 || array_key_exists('*', $arSelectFields))
		{
			$arSelectFields = array_keys($arFields);
		}

		return CCitrusPolls::GetListMakeQuery(self::tableName . ' CVG', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
	}

	public static function Delete($ID)
	{
		global $DB;

		$ID = intval($ID);
		if ($ID <= 0)
			return false;

		$rsPolls = CCitrusPoll::GetList(Array(), Array("GROUP_ID" => $ID), false, false, Array("ID"));
		while ($arPoll = $rsPolls->Fetch())
			CCitrusPoll::Delete($arPoll["ID"]);

		$strSql = "DELETE FROM " . self::tableName . " WHERE ID=" . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		return true;
	}

	public static function GetByID($ID)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		if (isset($GLOBALS["CITRUS_POLLS"]["GROUP_CACHE_" . $ID]) && is_array($GLOBALS["CITRUS_POLLS"]["GROUP_CACHE_" . $ID]) && is_set($GLOBALS["CITRUS_POLLS"]["GROUP_CACHE_" . $ID], "ID"))
		{
			return $GLOBALS["CITRUS_POLLS"]["GROUP_CACHE_" . $ID];
		}
		else
		{
			$strSql = "SELECT * " . "FROM " . self::tableName . " CVG " . "WHERE CVG.ID = " . $ID . " ";

			$db = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			if ($ar = $db->Fetch())
			{
				$GLOBALS["CITRUS_POLLS"]["GROUP_CACHE_" . $ar['ID']] = $ar;

				return $ar;
			}
		}

		return false;
	}

	public static function UpdateEx($ID, $arFields)
	{
		return self::Update($ID, $arFields);
	}

	public static function Update($ID, $arFields)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		if (!self::CheckFields($arFields, 'UPDATE'))
			return false;

		$strUpdate = $DB->PrepareUpdate(self::tableName, $arFields);
		$strSql = "UPDATE " . self::tableName . " SET " . $strUpdate . " WHERE ID=" . $ID;
		$DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

		return true;
	}

	public static function Add($arFields)
	{
		global $DB;

		if (!self::CheckFields($arFields, 'ADD'))
		{
			return false;
		}

		$ID = CDatabase::Add(self::tableName, $arFields);

		return $ID;
	}

	protected static function CheckFields(&$arFields, $strOperation = 'ADD')
	{
		// TODO �������� ��������
		return true;
	}
}

