<?

IncludeModuleLangFile(__FILE__);

class CCitrusPollVoteAnswer extends CDBResult
{
	const tableName = 'b_citrus_voting_event_answer';

	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		static $arFields = array(
			"ID" => Array("FIELD" => "CVEA.ID", "TYPE" => "int"),
			"VOTING_EVENT_ID" => Array("FIELD" => "CVEA.VOTING_EVENT_ID", "TYPE" => "int"),
			"ANSWER_ID" => Array("FIELD" => "CVEA.ANSWER_ID", "TYPE" => "int"),

			"VOTE_ID" => Array("FIELD" => "CVE.ID", "TYPE" => "int", "FROM" => "INNER JOIN b_citrus_voting_event CVE ON (CVEA.VOTING_EVENT_ID = CVE.ID)"),
			"VOTE_VOTING_ID" => Array("FIELD" => "CVE.VOTING_ID", "TYPE" => "int", "FROM" => "INNER JOIN b_citrus_voting_event CVE ON (CVEA.VOTING_EVENT_ID = CVE.ID)"),
			"VOTE_WEIGHT" => Array("FIELD" => "CVE.WEIGHT", "TYPE" => "float", "FROM" => "INNER JOIN b_citrus_voting_event CVE ON (CVEA.VOTING_EVENT_ID = CVE.ID)"),
			"VOTE_ENTITY_ID" => Array("FIELD" => "CVE.ENTITY_ID", "TYPE" => "int", "FROM" => "INNER JOIN b_citrus_voting_event CVE ON (CVEA.VOTING_EVENT_ID = CVE.ID)"),
			"VOTE_TIMESTAMP_X" => Array("FIELD" => "CVE.TIMESTAMP_X", "TYPE" => "datetime", "FROM" => "INNER JOIN b_citrus_voting_event CVE ON (CVEA.VOTING_EVENT_ID = CVE.ID)"),
			"VOTE_IP" => Array("FIELD" => "CVE.IP", "TYPE" => "string", "FROM" => "INNER JOIN b_citrus_voting_event CVE ON (CVEA.VOTING_EVENT_ID = CVE.ID)"),
			"VOTE_QUESTION_ID" => Array("FIELD" => "CVE.QUESTION_ID", "TYPE" => "int", "FROM" => "INNER JOIN b_citrus_voting_event CVE ON (CVEA.VOTING_EVENT_ID = CVE.ID)"),
			"VOTE_VALID" => Array("FIELD" => "CVE.VALID", "TYPE" => "char", "FROM" => "INNER JOIN b_citrus_voting_event CVE ON (CVEA.VOTING_EVENT_ID = CVE.ID)"),
		);

		if (count($arSelectFields) <= 0 || array_key_exists('*', $arSelectFields))
		{
			$arSelectFields = array_keys($arFields);
		}

		return CCitrusPolls::GetListMakeQuery(self::tableName . ' CVEA', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
	}

	protected static function CheckFields(&$arFields, $strOperation = 'ADD')
	{
		// TODO �������� ��������
		return true;
	}

	public static function Add($arFields)
	{
		global $DB;

		if (!self::CheckFields($arFields, 'ADD'))
			return false;

		$ID = CDatabase::Add(self::tableName, $arFields);

		return $ID;
	}

	public static function UpdateEx($ID, $arFields)
	{
		return self::Update($ID, $arFields);
	}

	public static function Update($ID, $arFields)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		if (!self::CheckFields($arFields, 'UPDATE'))
			return false;

		$strUpdate = $DB->PrepareUpdate(self::tableName, $arFields);
		$strSql = "UPDATE " . self::tableName . " SET " . $strUpdate . " WHERE ID=" . $ID;
		$DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

		return true;
	}

	public static function Delete($ID)
	{
		global $DB;

		$ID = intval($ID);
		if ($ID <= 0)
			return false;

		$strSql = "DELETE FROM " . self::tableName . " WHERE ID=" . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		return true;
	}

	public static function GetByID($ID)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		$strSql = "SELECT * " . "FROM " . self::tableName . " CV " . "WHERE CV.ID = " . $ID . " ";

		$db = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		if ($ar = $db->Fetch())
			return $ar;

		return false;
	}
}
