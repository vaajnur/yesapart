<?
IncludeModuleLangFile(__FILE__);

class CCitrusPollEntityUnauthorizedUser extends CCitrusPollEntityBase
{
	function __construct()
	{
		$path = dirname(__FILE__) . "/.description.php";
		$arMethodDesc = false;
		if (file_exists($path))
		{
			@include($path);
			if (is_array($arMethodDesc))
			{
				$this->name = $arMethodDesc['NAME'];
				$this->description = $arMethodDesc['DESC'];
				$this->code = $arMethodDesc['CODE'];
			}
		}
	}

	public function GetCurrentID()
	{
		global $APPLICATION;

		// ������ �������� �� ������
		$ID = trim($_SESSION["VDGB_TSZHVOTE_VOTE_USER_ID"]);

		if (strlen($ID) != 32)
		{
			// ������ �������� �� ����
			$ID = trim($APPLICATION->get_cookie("_VDGB_TSZHVOTE_VOTE_USER_ID"));
		}

		if (strlen($ID) != 32)
		{
			$ID = md5(uniqid(mt_rand(), true));
		}

		// �������� �������� � ������
		$_SESSION["VDGB_TSZHVOTE_VOTE_USER_ID"] = $ID;

		// �������� ����
		$APPLICATION->set_cookie("_VDGB_TSZHVOTE_VOTE_USER_ID", $ID);

		return abs(intval(crc32($ID) / 2));
	}

	public function GetEntityName($ID = false)
	{
		if ($ID === false)
			$ID = $this->GetCurrentID();
		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		return "���������� #{$ID}";
	}
}
