<?
IncludeModuleLangFile(__FILE__);

class CCitrusPollEntityAccount extends CCitrusPollMethodBase
{
	function __construct()
	{
		$path = dirname(__FILE__) . "/.description.php";
		$arMethodDesc = false;
		if (file_exists($path))
		{
			@include($path);
			if (is_array($arMethodDesc))
			{
				$this->name = $arMethodDesc['NAME'];
				$this->description = $arMethodDesc['DESC'];
				$this->code = $arMethodDesc['CODE'];
			}
		}
	}

	public function GetCurrentID()
	{
		if (!CModule::IncludeModule('citrus.tszh'))
			return false;
		$arCurrentAccount = CTszhAccount::GetByUserID($GLOBALS['USER']->GetID());

		return is_array($arCurrentAccount) ? $arCurrentAccount['ID'] : false;
	}

	public function GetEntityName($ID = false)
	{
		if (!CModule::IncludeModule('citrus.tszh'))
			return false;

		if ($ID === false)
			$ID = $this->GetCurrentID();
		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		$arAccount = CTszhAccount::GetByID($ID);

		return is_array($arAccount) ? $arAccount['NAME'] : false;
	}
}

