<?
IncludeModuleLangFile(__FILE__);

class CCitrusPollEntityUser extends CCitrusPollEntityBase
{
	function __construct()
	{
		$path = dirname(__FILE__) . "/.description.php";
		$arMethodDesc = false;
		if (file_exists($path))
		{
			@include($path);
			if (is_array($arMethodDesc))
			{
				$this->name = $arMethodDesc['NAME'];
				$this->description = $arMethodDesc['DESC'];
				$this->code = $arMethodDesc['CODE'];
			}
		}
	}

	public function GetCurrentID()
	{
		return $GLOBALS['USER']->GetID();
	}

	public function GetEntityName($ID = false)
	{
		if ($ID === false)
			$ID = $this->GetCurrentID();
		$ID = IntVal($ID);
		if ($ID <= 0)
			return false;

		$arUser = CUser::GetByID($ID);

		return CUser::FormatName("#LAST_NAME# #NAME# #SECOND_NAME#", $arUser, false, false);
	}
}
