<?php

$arMethodDesc = array(
    "NAME" => "Любой посетитель",
    "DESC" => "Голосование неавторизованных посетителей сайта",
    "CODE" => "unauthorized_user",
    "CLASS" => "CCitrusPollEntityUnauthorizedUser",
);

?>