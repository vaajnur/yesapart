<?php
$MESS ['ID'] = "ID";
$MESS ['NAME'] = "Наименование";
$MESS ['ID_HOLDER'] = "Владелец группы";
$MESS ['LID'] = "Сайт";
$MESS ['VOTE_GROUP_LIST'] = "Список групп голосований";
$MESS ['VOTE_GROUP_LIST_TITLE'] = "Список групп голосований";
$MESS ['GROUP_ADD'] = "Добавить группу";
$MESS ['GROUP_ADD_TITLE'] = "Создать группу";

$MESS ['page_nav'] = "Группы";
$MESS ['group_edit'] = "Редактировать";
$MESS ['qroup_del_err'] = "Группу не удалось удалить";
$MESS ['group_del'] = "Удалить";
$MESS ['group_del_conf'] = "Вы действительно хотите удалить элемент?";
$MESS ['group_save_error'] = "Ошибка при сохранении";
$MESS ['qroup_del_ok'] = "Группа успешно удалена";
$MESS ['group_del_error'] = "Ошибка при удалении группы.";
$MESS ['group_no'] = "Группа отсутствует";

$MESS ['group_name'] = "Название";
$MESS ['site_site'] = "Сайт";
$MESS ['group_sort'] = "сортировка";
$MESS ['group_act'] = "Активность";
$MESS ['group_timestamp_x'] = "Дата изменения";
$MESS ['group_code'] = "Символьный код";
$MESS ['group_id_holder'] = "Владелец группы";
$MESS ['group_count_topic'] = "Количество опросов";

$MESS ['find_name'] = "Название:";
$MESS ['find_id_holder'] = "Владелец:";
$MESS ['find_id'] = "ID:";
$MESS ['find_lid'] = "Сайт";
$MESS ['group_find'] = "Найти";
$MESS ['group_find_title'] = "Введите строку для поиска";

?>