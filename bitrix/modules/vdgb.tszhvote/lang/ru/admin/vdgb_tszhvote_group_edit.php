<?php

$MESS ['VOTE_GROUP_ADD_TITLE'] = "Добавление группы голосований";
$MESS ['VOTE_GROUP_EDIT_TITLE'] = "Редактирование группы голосований #";

$MESS ['qroup_edit_err'] = "Ошибка при редактировании данных";
$MESS ['qroup_add_err'] = "Ошибка при добавлении данных";

$MESS ['qroup_edit_ok'] = "Данные успешно изменены";
$MESS ['qroup_add_ok'] = "Данные успешно добавлены";

$MESS ['group_tab_edit1'] = "Свойства";
$MESS ['group_tab_edit1_title'] = "Свойства группы голосований";
$MESS ['group_act'] = "Активность:";
$MESS ['site'] = "Сайт:";
$MESS ['group_name'] = "Наименование:";
$MESS ['group_sort'] = "Порядок сортировки:";
$MESS ['group_code'] = "Символьный код:";
$MESS ['group_title'] = "Заголовок:";
$MESS ['group_id_holder'] = "Владелец группы опросов:";
$MESS ['group_tab_edit2'] = "Доступ";
$MESS ['group_tab_edit2_title'] = "Настройка прав доступа";

$MESS ['group_list'] = "Список";
$MESS ['group_list_title'] = "Перейти к списоку";

$MESS ['group_new'] = "Создать";
$MESS ['group_new_title'] = "Перейти к форме создания группы опросов";

$MESS ['group_del'] = "Удалить";
$MESS ['group_del_title'] = "Удалить группу";

$MESS ['voting_new'] = "Создать голосование";
$MESS ['voting_new_title'] = "Перейти к форме создания голосования";

$MESS ['voting_list'] = "Голосования";
$MESS ['voting_list_title'] = "Перейти к списку голосований группы";

$MESS ['group_access'] = "Группа доступна для:";

?>
