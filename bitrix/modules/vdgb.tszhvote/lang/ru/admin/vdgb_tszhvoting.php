<?php

$MESS ['VOTE_LIST_TITLE'] = "Список голосований";
$MESS ['ID'] = "ID:";
$MESS ['NAME'] = "Наименование:";
$MESS ['DATE'] = "Дата активности:";
$MESS ['END_DATE'] = "Дата начала активности:";
$MESS ['ACTIVE'] = "Активные:";
$MESS ['ID_GROUP'] = "ID группы";
$MESS ['VOTING_COMPLETED'] = "Состоялся";

$MESS ['VOTING_LIST'] = "Список опросов";

$MESS ['voting_find'] = "Найти";
$MESS ['voting_find_title'] = "Введите строку для поиска";

$MESS ['find_date_voting'] = "Дата активности";
$MESS ['find_id_group'] = "ID группы";
$MESS ['find_id'] = "ID";
$MESS ['find_name'] = "Наименование";
$MESS ['find_begin_date'] = "от";
$MESS ['find_begin_end'] = "до";
$MESS ['find_active'] = "Активные";
$MESS ['find_voting_comp'] = "Состоялся";
$MESS ['find_voting_comp_Y'] = "Да";
$MESS ['find_voting_comp_N'] = "Нет";

$MESS ['voting_name'] = "Наименование";
$MESS ['id_group'] = "ID группы";
$MESS ['voting_act'] = "Активность";
$MESS ['voting_timestamp_x'] = "Дата изменения";
$MESS ['voting_code'] = "Код";
$MESS ['voting_date_begin'] = "Дата начала активности";
$MESS ['voting_date_end'] = "Дата окончания активности";
$MESS ['voting_title'] = "Заголовок";
$MESS ['voting_detail'] = "Описание";
$MESS ['xml_id'] = "XML_ID";
$MESS ['count_question'] = "Количество вопросов";
$MESS ['count_voting'] = "Количество голосов";
$MESS ['voting_completed'] = "Состоялся";
$MESS ['voting_method'] = "Метод посчета голосов";
$MESS ['voting_is_multiple'] = "Множественный ответ";
$MESS ['TITLE_TEXT'] = "Заголовок опроса";
$MESS ['DETAIL_TEXT'] = "Подробное описание";

$MESS ['voting_edit'] = "Изменить";
$MESS ['voting_del'] = "Удалить";
$MESS ['voting_copy'] = "Копировать";
$MESS ['voting_reset'] = "Обнулить";
$MESS ['voting_del_conf'] = "Вы действительно хотите удалить опрос?";

$MESS ['VOTING_ADD'] = "Создать";
$MESS ['VOTING_ADD_TITLE'] = "Перейти к форме добавления опроса";

$MESS ['voting_save_error'] = "Ошибка при сохранении";
$MESS ['voting_no'] = "Опрос отсутствует";
$MESS ['voting_del_error'] = "Ошибка при удалении опроса";
$MESS ['voting_reset_error'] = "Ошибка при обновлении опроса";
$MESS ['voting_reset_ok'] = "Опрос удачно обновлен";

$MESS ['REPORT_PRINT'] = "напечатать отчет";
$MESS ['voting_result'] = "Результат";
$MESS ['voting_not_found'] = "Данные по этому опросу не найдены";

$MESS ['find_voting_active_Y'] = "Да";
$MESS ['find_voting_active_N'] = "Нет";
?>
