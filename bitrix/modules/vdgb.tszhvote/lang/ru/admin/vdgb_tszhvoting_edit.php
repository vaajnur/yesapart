<?php

$MESS ['VOTING_ADD_TITLE'] = "Добавление голосования";
$MESS ['VOTING_EDIT_TITLE'] = "Редактирование голосования #";

$MESS ['voting_tab_edit1'] = "Голосование";
$MESS ['voting_tab_edit1_title'] = "Поля голосования";

//tab - 1
$MESS ['voting_act'] = "Активность:";
$MESS ['voting_group_id'] = "Группа голосований:";
$MESS ['voting_name'] = "Наименование:";
$MESS ['voting_date_begin'] = "Дата начала:";
$MESS ['voting_date_end'] = "Дата окончания:";
$MESS ['voting_count_of_comp'] = "Состоится при % голосов:";
$MESS ['voting_completed_explanation'] = "Процент голосов, при котором голосование считается состоявшимся";
$MESS ['voting_code'] = "Символьный код:";
$MESS ['voting_is_multiplate'] = "Множественный ответ:";
$MESS ['voting_method'] = "Метод подсчета голосов:";
$MESS["CV_PUBLIC_SECTION"] = "Представление в публичной части";
$MESS ['TITLE_TEXT'] = "Заголовок опроса:";
$MESS ['DETAIL_TEXT'] = "Текст описания:";
$MESS ['USER_FIELDS'] = "Дополнительные поля";


$MESS ['voting_list'] = "Список";
$MESS ['voting_list_title'] = "Список голосований";
$MESS ['voting_completed'] = "Состоялось:";


//message

$MESS ['voting_add_err']    = "Ошибка при добавлении";
$MESS ['voting_edit_err']   = "Ошибка при сохранении";

$MESS ['voting_edit_ok']    = "Опрос успешно отредактирован";
$MESS ['voting_add_ok']     = "Опрос успешно добавлен";

$MESS ['voting_save_ok'] = "Данные успешно сохранены";


$MESS ['voting_copy'] = "Копировать";
$MESS ['voting_copy_title'] = "Скопировать данный опрос";
$MESS ['voting_new'] = "Создать";
$MESS ['voting_new_title'] = "Перейти к форме создания опроса";
$MESS ['voting_del'] = "Удалить";
$MESS ['voting_del_title'] = "Удалить опрос";
$MESS ['voting_reset'] = "Обнулить";
$MESS ['voting_reset_title'] = "Обнулить результаты опроса";
$MESS ['voting_question'] = "Вопросы";
$MESS ['voting_question_title'] = "Перейти к списку вопросов";
$MESS ['voting_question_new'] = "Создать вопрос";
$MESS ['voting_question_new_title'] = "Создать вопрос";

$MESS ['CV_ENTITY_TYPE'] = "Кто голосует:";
$MESS["CV_ALL"] = "(все)";
$MESS["CV_TSZH_ID"] = "Объект управления:";
$MESS["CV_TSZH_ID_NOTE_TITLE"] = "Примечание для поля «Объект управления»";
$MESS["CV_TSZH_ID_NOTE"] = "Поле «Объект управления» позволяет выбрать <b>организацию</b>, жильцам которой доступно это голосование.";
