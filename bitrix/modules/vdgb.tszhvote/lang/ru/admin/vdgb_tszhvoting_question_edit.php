<?php

$MESS ['question_tab_edit1'] = "Вопрос";
$MESS ['question_tab_edit1_title'] = "Текст вопроса";
$MESS ['question_tab_edit2'] = "Ответ";
$MESS ['question_tab_edit2_title'] = "Текст ответа";

//page title
$MESS ['QUESTION_ADD_TITLE'] = "Добавить вопрос";
$MESS ['QUESTION_EDIT_TITLE'] = "Редактировать вопрос #";

$MESS ['question_list'] = "Список";
$MESS ['question_list_title'] = "Перейти к списку опросов";

$MESS ['question_act'] = "Активность:";
$MESS ['is_multiplete'] = "Разрешить несколько вариантов ответа:";
$MESS ['text'] = "Текст вопроса:";
$MESS ['sort'] = "Сортировка:";
$MESS ['voting_id'] = "ID опроса:";

$MESS ['answer_title'] = "Текст ответа";
$MESS ['answer_sort'] = "Сортировка";
$MESS ['answer_act'] = "Активность";
$MESS ['answer_del'] = "Удалить";
$MESS ['answer_id'] = "ID";
$MESS ['answer_color'] = "Цвет";

//errors
$MESS ['question_edit_err'] = "Ошибка при редактировании вопроса";

?>
