<?php
$MESS ['QESTION_LIST_TITLE'] = "Список вопросов голосования";
$MESS ['QUESTION_LIST'] = "Список вопросов";
$MESS ['active'] = "Активный";
$MESS ['voting_id'] = "ID опроса";
$MESS ['text_question'] = "Текст вопроса";
$MESS ['sort'] = "Порядок сортировки";
$MESS ['is_multiplate'] = "Множественный ответ";
$MESS ['QUESTION_ADD'] = "Создать";
$MESS ['QUESTION_ADD_TITLE'] = "Перейти к форме создания вопроса";
$MESS ['question_edit'] = "Редактировать";
$MESS ['question_del'] = "Удалить";
$MESS ['question_del_conf'] = "Вы действительно хотите удалить этот вопрос?";

//errors
$MESS ['question_save_error']   = "Ошибка при сохранении вопроса";
$MESS ['question_no']           = "Вопрос не найден";
?>
