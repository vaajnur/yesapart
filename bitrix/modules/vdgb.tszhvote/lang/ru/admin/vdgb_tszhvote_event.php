<?php

$MESS ['voting_name'] = "Опрос";
$MESS ['user_id'] = "Пользователь";
$MESS ['timestap_x'] = "Дата изменения";
$MESS ['question_id'] = "Вопрос";
$MESS ['valid'] = "Валидный";
$MESS ['vote'] = "Голосование";

$MESS ['ID'] = "ID";
$MESS ['VOTING_ID'] = "Опрос";
$MESS ['USER_ID'] = "Пользователь";
$MESS ['IP'] = "IP";
$MESS ['VALID'] = "Валидный";
$MESS ['TIMESTAMP_X'] = "Дата изменения";
$MESS ['QUESTION_ID'] = "Вопрос";
$MESS ['VOTING_EVENT_LIST'] = "Список голосов";
$MESS ['VOTE_EVENT_LIST_TITLE'] = "Голосования";

$MESS ['voting_event_find']  = "Найти";
$MESS ['voting_event_find_title']  = "Введите сторку поиска";

$MESS ['find_voting_id'] = "ID опроса";
$MESS ['find_id'] = "ID:";
$MESS ['find_user_id'] = "ID пользователя:";
$MESS ['find_ip'] = "IP пользователя:";
$MESS ['find_valid'] = "Валидный:";
$MESS ['find_voting_id'] = "ID опроса:";
$MESS ['find_timestap_x'] = "Дата изменения:";
$MESS ['find_question_id'] = "ID вопроса:";

$MESS ['EMPTY_USER'] = "(не авторизован)";
$MESS ['VIEW_VOTING'] = "Посмотреть ответ";

$MESS ['F_WEIGHT'] = "Вес голоса";

$MESS ['vote_del_error'] = "Ошибка при удалении голоса";
?>
