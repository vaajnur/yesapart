<?
$MESS ['C_VOTE_MODULE_NAME'] = "1С: Сайт ЖКХ. Голосования";
$MESS ['C_VOTE_MODULE_DESC'] = "Позволяет организовать опросы, просматривать их результаты и формировать отчеты";
$MESS ['C_VOTE_MODULE_INSTAL_PAGE_TITLE'] = "Установка модуля голосований";
$MESS ['C_VOTE_MODULE_UNINSTALL_PAGE_TITLE'] = "Удаление модуля голосования";
$MESS ['C_VOTE_PARTNER_NAME'] = "1С-Рарус Тиражные решения";
$MESS ['C_VOTE_PARTNER_URI'] = "https://otr-soft.ru/";

$MESS ['C_ERROR_INSTALL_DB'] = "Ошибка при установке таблиц базы данных модуля";
$MESS ['C_ERROR_INSTALL_EVENTS'] = "Ошибка при установке типов почтовых событий модуля";
$MESS ['C_ERROR_INSTALL_FILES'] = "Ошибка при копировании файлов модуля";
$MESS ['C_ERROR_INSTALL_FILES_ADMIN'] = "Ошибка при копировании административной части модуля";
$MESS ['C_ERROR_INSTALL_FILES_COMPONENTS'] = "Ошибка при копировании компонентов модуля";
$MESS ['C_ERROR_INSTALL_FILES_GADGET'] = "Ошибка при копировании гаджета";
$MESS ['C_ERROR_INSTALL_GADGET'] = "Ошибка при установке гаджета";
$MESS ['C_ERROR_UNINSTALL_DB'] = "Ошибка при удалении таблиц базы дынных модуля";
$MESS ['C_ERROR_UNINSTALL_EVENTS'] = "Ошибка при удалении типов почтовых событий модуля";
$MESS ['C_ERROR_UNINSTALL_FILES'] = "Ошибка при удалении файлов модуля";

$MESS ['C_VOTNOT_MODULE_EVENT_NAME'] = "Код подтверждения при голосовании";
$MESS ['C_VOTNOT_MODULE_EVENT_CODE'] = "Код подтверждения";
$MESS ['C_VOTNOT_MODULE_EVENT_EMAIL'] = "Адрес электронной почты";
$MESS ['C_VOTNOT_MODULE_EVENT_SUBJECT'] = "Тема сообщения";
$MESS ['C_VOTNOT_MODULE_CONFIRM_EMAIL'] = "Подтверждение голосования через EMAIL";
$MESS ['C_VOTNOT_MODULE_CONFIRM_SMS'] = "Подтверждение голосования через SMS";
$MESS ['C_VOTNOT_MODULE_CONFIRM_NO'] = "По умолчанию (Подтверждение голосования отключено)";

$MESS ["VOTE_VISTA"] = "Новые возможности вы можете посмотреть";
$MESS ["VOTE_HERE"] = "здесь";

$MESS["VOTE_EMAIL_BODY"] = '
<div class="content">
<!-- /content -->
<table cellspacing="0" cellpadding="0">
      <tbody><tr>
          <td style="background-color:#ffc200; height:9px;"></td>
      </tr>
      <tr>
          <td style="padding:0px 15px; border-left: 1px solid #cccccc; border-right: 1px solid #cccccc;">
              <table style="border-bottom: 1px solid #cccccc; margin-bottom: 20px;" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td><!--img src="logo.png" style="margin: 20px 0px"--></td>
                        <td valign="middle" align="center"><h1>Подтверждение голосования на сайте #SITE_NAME#</h1></td>
                    </tr>
                </tbody></table>
                
                <table style="border-bottom: 1px solid #cccccc; margin-bottom: 20px;" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <p>Подтвердите ваш выбор!</p>
                            <p>Уважаемый пользователь! Вы получили это письмо, потому что приняли участие в голосовании на сайте #SITE_NAME#.</p>
							<p>Для того, чтобы ваш голос в опросе был учтен, введите этот код в форму на сайте:</p>
                            <p class="code">#CODE#</p>
                            <p class="italic" style="margin-bottom: 20px;">Это письмо отправлено автоматически, отвечать на него не нужно.Если вы не совершали никаких подобных действий, просто проигнорируйте его.</p>
                        </td>
                    </tr>
                </tbody></table>
                
                <table>
                    <tbody><tr>
                        <td>
                            <p class="signature bold">С уважением,<br><a href="#SERVER_NAME#">#SITE_NAME#</a></p>							
                        </td>
                    </tr>
                </tbody></table>
          </td>
      </tr>
      <tr>
          <td style="background-color:#0399ad; height:11px;"></td>
      </tr>
  </tbody></table>
  </div>
';
?>
