<?php

$MESS ["VOTE_OPTIONS_TAB_MAIN"] = "Настройки";
$MESS ["MAIN_OPT_APPLY"] = "Применить";
$MESS ["VOTE_OPTIONS_MAIN"] = "Главные";
$MESS ["VOTE_OPTIONS_TAB_TITLE_STATISTIC"] = "Настройки модуля голосований";
$MESS ["VOTE_OPTIONS_MAIN_TITLE"] = "Главные";

$MESS ["NOTI_OPTIONS_MAIL_EVENT"] = "Почтовое событие:";
$MESS ["NOTI_OPTIONS_MAIL_TEMPLATE"] = "Форма уведомления пользователя (почтовый шаблон):";
$MESS ["SMS_OPTION_LOGIN"] = "Логин:";
$MESS ["SMS_OPTION_PASSWORD"] = "Пароль:";
$MESS ["SMS_OPTION_SOURCE"] = "Имя отправителя:";
$MESS ["SMS_OPTION_ALERT"] = "Пожалуйста, заполните все поля во вкладке СМС!";
$MESS ["SMS_OPTION_HELP"] = "Для того чтобы у вас была возможность отправлять СМС"
        ." для подтверждения, вам необходимо пополнить свой баланс СМС."
        ." Для пополнения баланса СМС - свяжитесь с "
        ."<a href=\"http://vgkh.ru/contacts/\">отделом продаж</a>.";
$MESS ["EMAIL_OPTION_NOTI"] = "Хотите изменить шаблон письма, который"
        . " будет отправляться пользователю для подтвреждения голосования? Пройдите по ссылке.";
$MESS ["VOTE_OPTION_COUNT"] = "Количество попыток  ввода кода";
$MESS ["VOTE_OPTION_TEMP_DESC"] = "Код подтверждения при голосовании";
?>
