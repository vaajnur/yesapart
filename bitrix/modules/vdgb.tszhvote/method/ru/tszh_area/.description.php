<?php

$arMethodDesc = array(
    "NAME" => "По общей площади лицевого счета",
    "DESC" => "Вес голоса опредляется общей площадью помещения лицевого счета",
    "CODE" => "tszh_area",
    "CLASS" => "CCitrusPollMethodArea",
    "ENTITIES" => array(
        "tszh_account",
    )
);

?>