<?php

$arMethodDesc = array(
    "NAME" => "По количеству проживающих",
    "DESC" => "Вес голоса опредляется количеством проживающих в помещении людей",
    "CODE" => "tszh_people",
    "CLASS" => "CCitrusPollMethodPeople",
    "ENTITIES" => array(
        "tszh_account",
    )
);

?>