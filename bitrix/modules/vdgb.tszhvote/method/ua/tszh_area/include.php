<?
IncludeModuleLangFile(__FILE__);

class CCitrusPollMethodArea extends CCitrusPollMethodBase
{
	function __construct()
	{
		$path = dirname(__FILE__) . "/.description.php";
		$arMethodDesc = false;
		if (file_exists($path))
		{
			@include($path);
			if (is_array($arMethodDesc))
			{
				$this->name = $arMethodDesc['NAME'];
				$this->description = $arMethodDesc['DESC'];
				$this->code = $arMethodDesc['CODE'];
			}
		}
	}

	public function GetVoteVolume($ENTITY_ID = false, $cntAnswers = 1)
	{
		if (!CModule::IncludeModule('citrus.tszh'))
			return 0;

		if ($ENTITY_ID === false)
		{
			$arAccount = CTszhAccount::GetByUserID($GLOBALS['USER']->GetID());

			return $arAccount['AREA'] / $cntAnswers;
		}
		else
		{
			$ENTITY_ID = IntVal($ENTITY_ID);
			$arAccount = CTszhAccount::GetByID($ENTITY_ID);

			return $arAccount['AREA'] / $cntAnswers;
		}

		return 1;
	}

	public function GetTotalVolume($ENTITY_ID)
	{
		if (!CModule::IncludeModule('citrus.tszh'))
			return 0;

		$ENTITY_ID = IntVal($ENTITY_ID);

		$arAccount = CTszhAccount::GetByID($ENTITY_ID);
		$arTotals = CTszhAccount::GetList(Array(), Array(
			"CITY" => $arAccount['CITY'],
			"DISTRICT" => $arAccount['DISTRICT'],
			"REGION" => $arAccount['REGION'],
			'SETTLEMENT' => $arAccount['SETTLEMENT'],
			'STREET' => $arAccount['STREET'],
			'HOUSE' => $arAccount['HOUSE'],
		), Array("SUM" => "AREA"))->Fetch();

		return $arTotals["AREA"];
	}

	function GetTotalVolumeTitle()
	{
		return "����� ������� ������� � ������� ���������";
	}

	function FormatVolume($volume, $bHTML = false)
	{
		$str = number_format($volume, 2, ',', '');

		return $bHTML ? $str . '&nbsp;�<sup>2</sup>' : $str . ' ��.�.';
	}
}
