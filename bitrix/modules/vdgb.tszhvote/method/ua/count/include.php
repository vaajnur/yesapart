<?
IncludeModuleLangFile(__FILE__);

class CCitrusPollMethodCount extends CCitrusPollMethodBase
{
	function __construct()
	{
		$path = dirname(__FILE__) . "/.description.php";
		$arMethodDesc = false;
		if (file_exists($path))
		{
			@include($path);
			if (is_array($arMethodDesc))
			{
				$this->name = $arMethodDesc['NAME'];
				$this->description = $arMethodDesc['DESC'];
				$this->code = $arMethodDesc['CODE'];
			}
		}
	}

	public function GetVoteVolume($ENTITY_ID = false)
	{
		return 1;
	}

	function FormatVolume($volume, $bHTML = false)
	{
		$str = number_format($volume, 0, ',', '');
		$pluralForm = CTszhPublicHelper::pluralForm($volume, array("�����", "������", "�������"));

		return $bHTML ? $str . "&nbsp;" . $pluralForm : $str . " " . $pluralForm;
	}
}

