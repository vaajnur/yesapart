<?
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
IncludeModuleLangFile(__FILE__);
CJsCore::Init(array('jquery'));

$aTabs = array(
    array(
        "DIV"     => "tabMain",
        "TAB"     => GetMessage("VOTE_OPTIONS_TAB_MAIN"),
        "ICON"    => "search_settings",
        "TITLE"   => GetMessage("VOTE_OPTIONS_TAB_TITLE_STATISTIC"),
        "OPTIONS" => Array()
    ),
    array(
        "DIV"     => "EMAIL",
        "TAB"     => "EMAIL",
        "ICON"    => "search_settings",
        "TITLE"   => "EMAIL",
        "OPTIONS" => Array()
    ),
    array(
        "DIV"     => "SMS",
        "TAB"     => "SMS",
        "ICON"    => "search_settings",
        "TITLE"   => "SMS",
        "OPTIONS" => Array()
    ),
);

$tabControl = new CAdminTabControl("tabControlSetings", $aTabs);

$aMenu = array(
    array(
        "TEXT"  => GetMessage("VOTE_OPTIONS_MAIN"),
        "LINK"  => "search_reindex.php?lang=".LANGUAGE_ID,
        "TITLE" => GetMessage("VOTE_OPTIONS_MAIN_TITLE"),
    ),
);

$context = new CAdminContextMenu($aMenu);
$context->Show();
$tabControl->Begin();
$tabControl->BeginNextTab();

function SetOptionSend($option)
{
    switch ($option)
    {
        case "EMAIL":
            $param = 1;
            break;
        case "SMS":
            $param = 2;
            break;
        default:
            $param = 0;
            break;
    }
    return COption::SetOptionInt("vdgb.tszhvote", "sendToWay", $param, FALSE,
                    FALSE);
}

if ($REQUEST_METHOD == "POST" && check_bitrix_sessid())
    if (!empty($_REQUEST['UpdateButton']))
    {
        if (!empty($_REQUEST['option']))
            SetOptionSend($_REQUEST['option']);

        if (!empty($_REQUEST['count_number']) && ($_REQUEST['count_number'] >= 1) && ($_REQUEST['count_number'] <= 10))
            COption::SetOptionInt("vdgb.tszhvote", "sendToWay.count",
                    $_REQUEST['count_number'], FALSE, FALSE);

        if (!empty($_REQUEST['sms_login']))
            COption::SetOptionString("vdgb.tszhvote", "sendToWay.login",
                    $_REQUEST['sms_login'], FALSE, FALSE);

        if (!empty($_REQUEST['sms_pass']))
            COption::SetOptionString("vdgb.tszhvote", "sendToWay.pass",
                    $_REQUEST['sms_pass'], FALSE, FALSE);

        if (!empty($_REQUEST['sms_source']))
            COption::SetOptionString("vdgb.tszhvote", "sendToWay.source",
                    $_REQUEST['sms_source'], FALSE, FALSE);
    }

$valOpt = COption::GetOptionInt("vdgb.tszhvote", "sendToWay", 0, FALSE);
$count_number = COption::GetOptionInt("vdgb.tszhvote", "sendToWay.count", 3,
                FALSE, FALSE);
?>

<form method="post" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<?= LANGUAGE_ID ?>">

    <tr>
        <td valign="top" width="50%" colspan="2"><?= GetMessage("C_VOTNOT_MODULE_CONFIRM_NO") ?></td>
        <td valign="top" width="50%"><input type="radio" name="option" id="off-option" value="OFF" <?
            if ($valOpt == 0)
                echo 'checked';
            ?>></td>
    </tr>
    <tr>
        <td valign="top" width="50%" colspan="2"><?= GetMessage("C_VOTNOT_MODULE_CONFIRM_EMAIL") ?></td>
        <td valign="top" width="50%"><input type="radio" name="option" id="email-option" value="EMAIL" <?
            if ($valOpt == 1)
                echo 'checked';
            ?>></td>
    </tr>
    <tr>
        <td valign="top" width="50%" colspan="2"><?= GetMessage("C_VOTNOT_MODULE_CONFIRM_SMS") ?></td>
        <td valign="top" width="50%"><input type="radio" name="option" id="sms-option" value="SMS" <?
            if ($valOpt == 2)
                echo 'checked';
            ?>></td>
    </tr>
    <tr>
        <td valign="top" width="50%" colspan="2"><?= GetMessage("VOTE_OPTION_COUNT") ?></td>
        <td valign="top" width="50%"><input type="number" id="count_number" name="count_number" min="1" max="10" step="1" value="<?= $count_number ?>"></td>
    </tr>

    <? $tabControl->BeginNextTab(); ?>

    <tr>
        <td valign="top" width="50%" colspan="2"><?= GetMessage("NOTI_OPTIONS_MAIL_TEMPLATE") ?></td>
        <td> 
            <a href="../admin/message_edit.php?ID=<?
            $rsMess = CEventMessage::GetList($by = "event_name",
                            $order = "desc", array('TYPE_ID' => 'voteNot'));

            $arFiltr = $rsMess->Fetch();
            echo $arFiltr['ID'];
            ?>"><?= GetMessage("VOTE_OPTION_TEMP_DESC") ?></a><?= ' ('.$arFiltr['ID'].')' ?></td>
    </tr>
    <tr>
        <td valign="top" width="50%" colspan="2"></td>
        <td><small><i>(<?= GetMessage("EMAIL_OPTION_NOTI") ?>)</i></small></td>
    </tr>

    <? $tabControl->BeginNextTab(); ?>

    <tr>
        <td colspan="2"  width="40%"><?= GetMessage("SMS_OPTION_LOGIN") ?></td>
        <td><input type="text" name="sms_login" id="sms_login" value="<?=
            COption::GetOptionString("vdgb.tszhvote", "sendToWay.login", "",
                    FALSE)
            ?>"></td>
    </tr>
    <tr>
        <td colspan="2"  width="40%"><?= GetMessage("SMS_OPTION_PASSWORD") ?></td>
        <td><input type="password" name="sms_pass" id="sms_pass" value="<?=
            COption::GetOptionString("vdgb.tszhvote", "sendToWay.pass", "",
                    FALSE)
            ?>"></td>
    <tr/>
    <tr>
        <td colspan="2"  width="40%"><?= GetMessage("SMS_OPTION_SOURCE") ?></td>
        <td><input type="text" name="sms_source" id="sms_source" value="<?=
            COption::GetOptionString("vdgb.tszhvote", "sendToWay.source", "",
                    FALSE)
            ?>"></td>
    <tr/>
    <tr>
        <td colspan="3" align="center"><small><i><?= GetMessage('SMS_OPTION_HELP') ?></i></small></td>
    <tr/>

    <? $tabControl->Buttons(); ?>

    <form method="post" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<?= LANGUAGE_ID ?>">
        <input type="submit" name="UpdateButton" id="UpdateButton" value="<?= GetMessage("MAIN_SAVE") ?>" title="<?= GetMessage("MAIN_OPT_SAVE_TITLE") ?>">
        <?= bitrix_sessid_post(); ?>
    </form>

    <script>
        BX.ready(function ()
        {
            BX.bind(BX('UpdateButton'), 'click', function (event)
            {
                if (document.getElementById('sms-option').checked == true)
                {
                    if ((document.getElementById('sms_login').value == "") || (document.getElementById('sms_pass').value == "") || (document.getElementById('sms_source').value == ""))
                    {
                        event.preventDefault();
                        alert('<?= GetMessage("SMS_OPTION_ALERT") ?>');
                    }

                }
            });
        });
    </script>
    
    <? $tabControl->End(); ?>