<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/prolog.php");

IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("vdgb.tszhvote");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if(isset($_REQUEST['GROUP_ID']) && IntVal($_REQUEST['GROUP_ID']) > 0) {
    $GROUP_ID = $_REQUEST['GROUP_ID'];
}
else {
    $GROUP_ID = 0;
}

$APPLICATION->SetTitle(GetMessage("VOTE_RESULT_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$sTableID = "tbl_citrus_vote_form";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
//$lAdmin = new CAdminList($sTableID, $oSort);


// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //

// ���������� ���� �� ������ ������ - ���������� ������
$aContext = array(
  array(
    "TEXT"=>GetMessage("VOTING_LIST"),
    "LINK"=>"vdgb_tszhvoting.php?lang=".LANG,
    "TITLE"=>GetMessage("VOTING_LIST_TITLE"),
    "ICON"=>"btn_list",
  ),
);

// � ��������� ��� � ������
//$lAdmin->AddAdminContextMenu($aContext);
$lAdmin = new CAdminContextMenu($aContext);

$lAdmin->Show();
//$lAdmin->DisplayList();

if(isset($VOTING_ID) && $VOTING_ID > 0) {
    
$APPLICATION->IncludeComponent(
	"citrus:tszh.voting.result",
	"",
	Array(
		"GROUP_ID" => $GROUP_ID,
		"VOTING_ID" => array("0"=>$VOTING_ID),
		"VOTE_TYPE_DIOGRAM" => "0",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000"
	)
);

}
else {
    CAdminMessage::ShowMessage(GetMessage("voting_not_found"));
}

?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>