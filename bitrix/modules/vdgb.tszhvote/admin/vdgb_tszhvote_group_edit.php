<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/prolog.php");

IncludeModuleLangFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("vdgb.tszhvote");
if($POST_RIGHT=="D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if(isset($_GET["ID"])){
	$strURL = "?ID=".IntVal($_GET["ID"]);
}
else {
	$strURL = "";
}


if(isset($_REQUEST["ID"]) && IntVal($_REQUEST["ID"]) > 0) {
	$CURENT_ID = IntVal($_REQUEST["ID"]);

	// ������� ������ �����
	$rsData = CCitrusPollGroup::GetList(array(), array("ID"=>IntVal($_REQUEST["ID"])));
	if ($arFields = $rsData->GetNext(false))
	{
		foreach ($arFields as $key => $value)
		{
			$data[$key] = $value;
		}
	}
}
elseif(isset($_POST["ID"]) && IntVal($_POST["ID"]) > 0) {
	$CURENT_ID = IntVal($_POST["ID"]);
}
else {
	$CURENT_ID = 0;
}


if(isset($_POST) && !empty($_POST)) {

	$arData = array();

	if(isset($_POST["LID"]) && strlen($_POST["LID"]) > 0) {
		$str_LID = $_POST["LID"];
		$arData["LID"] = $str_LID;
	}

	if(isset($_POST["NAME"]) && strlen($_POST["NAME"]) > 0) {
		$str_NAME = $_POST["NAME"];
		$arData["NAME"] = $str_NAME;
	}

	if(isset($_POST["CODE"]) && strlen($_POST["CODE"]) > 0) {
		$str_CODE = $_POST["CODE"];
		$arData["CODE"] = $str_CODE;
	}

	if(isset($_POST["XML_ID"]) && strlen($_POST["XML_ID"]) > 0) {
		$str_XML_ID = $_POST["XML_ID"];
		$arData["XML_ID"] = $str_XML_ID;
	}

	if(isset($_POST["ACTIVE"]) && strlen($_POST["ACTIVE"]) > 0) {
		$str_ACTIVE = $_POST["ACTIVE"];
		$arData["ACTIVE"] = $str_ACTIVE;
	}
	else {
		$str_ACTIVE = "N";
		$arData["ACTIVE"] = $str_ACTIVE;
	}

	if(isset($_POST["TITLE"]) && strlen($_POST["TITLE"]) > 0) {
		$str_TITLE = $_POST["TITLE"];
		$arData["TITLE"] = $str_TITLE;
	}

	if(isset($_POST["SORT"]) && IntVal($_POST["SORT"]) > 0) {
		$str_SORT = IntVal($_POST["SORT"]);
		$arData["SORT"] = $str_SORT;
	}

	if ((isset($_POST["ID"]) &&  IntVal($_POST["ID"]) > 0))
	{
		$str_ID = IntVal($_POST["ID"]);
		$arData['TIMESTAMP_X'] = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time());

		if (!CCitrusPollGroup::Update(IntVal($str_ID),$arData))
		{
			$error = GetMessage("qroup_edit_err");
			$errorType = 1;
		}
		else
		{
			$errorType = 0;
			$error = GetMessage("qroup_edit_ok");
			if (isset($_POST["save"]))
			{
				LocalRedirect("vdgb_tszhvote_group_list.php?lang=".LANG);
			}
		}
	}
	else
	{
		$newId = CCitrusPollGroup::Add($arData);
		if (!$newId)
		{
			$errorType = 1;
			$error = GetMessage("qroup_add_err");
		}
		else
		{
			$CURENT_ID = $newId;
			$errorType = 0;
			$error = GetMessage("qroup_add_ok");

			if (isset($_POST["save"]))
			{
				LocalRedirect("vdgb_tszhvote_group_list.php?lang=".LANG);
			}
		}
	}

	$data =  $arData;

	$separator = "?";
	$successfully = "successfully=Y";

	if(strlen($strURL) > 0){
		$separator = "&";
	}
	else {
		$strURL = "?ID=" . $CURENT_ID;
	}

	if($errorType) {
		$successfully = "successfully=N";
	}

	LocalRedirect($APPLICATION->GetCurPage() . $strURL . $separator . $successfully);
}

$aTabs = array(

	array (
		"DIV" => "edit1",
		"TAB" => GetMessage("group_tab_edit1"),
		"ICON"=>"main_user_edit",
		"TITLE"=>GetMessage("group_tab_edit1_title")
	),

);

$aMenu = array(
	array(
		"TEXT"	=> GetMessage("group_list"),
		"TITLE"	=> GetMessage("group_list_title"),
		"LINK"	=> "/bitrix/admin/vdgb_tszhvote_group_list.php?lang=".LANGUAGE_ID."&set_default=Y",
		"ICON"	=> "btn_list"

	),
);

if(isset($_GET["ID"]) && IntVal($_GET["ID"]) > 0) {

	$data["COUNT_TOPIC"] = CCitrusPoll::GetList(array(), array("GROUP_ID" => $_GET["ID"]), array());

	$APPLICATION->SetTitle(GetMessage("VOTE_GROUP_EDIT_TITLE").IntVal($_REQUEST["ID"]));

	$aMenu[] =  array(
		"TEXT"	=> GetMessage("voting_list") . " [" . $data["COUNT_TOPIC"] . "]",
		"TITLE"	=> GetMessage("voting_list_title"),
		"LINK"	=> "/bitrix/admin/vdgb_tszhvoting.php?find_id_group=".$CURENT_ID."&lang=".LANGUAGE_ID."&set_default=Y",
//                    "ICON"	=> "btn_list"
	);

	$aMenu[] = 	array(
		"TEXT"	=> GetMessage("group_new"),
		"TITLE"	=> GetMessage("group_new_title"),
		"LINK"	=> "/bitrix/admin/vdgb_tszhvote_group_edit.php?lang=".LANGUAGE_ID."&set_default=Y",
		"ICON"	=> "btn_new"
	);

	$aMenu[] =  array(
		"TEXT"	=> GetMessage("group_del"),
		"TITLE"	=> GetMessage("group_del_title"),
		"LINK"	=> "/bitrix/admin/vdgb_tszhvote_group_list.php?lang=".LANGUAGE_ID."&edit_form_del=1&ID=".IntVal($_REQUEST["ID"]),
		"ICON"	=> "btn_delete"
	);

	$aMenu[] =  array(
		"TEXT"	=> GetMessage("voting_new"),
		"TITLE"	=> GetMessage("voting_new_title"),
		"LINK"	=> "/bitrix/admin/vdgb_tszhvoting_edit.php?GROUP_ID=".$CURENT_ID."&lang=".LANGUAGE_ID."&set_default=Y",
		"ICON"	=> "btn_new"
	);
}
else {
	$APPLICATION->SetTitle(GetMessage("VOTE_GROUP_ADD_TITLE"));
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

if(isset($_GET["successfully"]) && $_GET["successfully"] == "Y" && isset($_GET["ID"])) {
	$message = new CAdminMessage(array("MESSAGE" => GetMessage("qroup_edit_ok"), "TYPE" => "OK"), false);
	echo $message->Show();
}
elseif(isset($_GET["successfully"]) && $_GET["successfully"] == "Y" && !isset($_GET["ID"])){
	$message = new CAdminMessage(array("MESSAGE" => GetMessage("qroup_add_ok"), "TYPE" => "OK"), false);
	echo $message->Show();
}
elseif(isset($_GET["successfully"]) && $_GET["successfully"] == "N" && isset($_GET["ID"])){
	CAdminMessage::ShowMessage(GetMessage("qroup_edit_err"));
}
elseif(isset($_GET["successfully"]) && $_GET["successfully"] == "N" && !isset($_GET["ID"])){
	CAdminMessage::ShowMessage(GetMessage("qroup_add_err"));
}

$context = new CAdminContextMenu($aMenu);
$context->Show();

$tabControl = new CAdminTabControl("tabControl", $aTabs);



?>
	<form method="POST" Action="<?echo $APPLICATION->GetCurPage().$strURL?>" ENCTYPE="multipart/form-data" name="post_form">
		<input type="hidden" name="ID" value="<? echo $CURENT_ID;?>"/>
		<?
		$tabControl->Begin();
		?>
		<?
		//********************
		//Property
		//********************
		$tabControl->BeginNextTab();
		?>
		<tr>
			<td width="40%"><?echo GetMessage("group_act")?></td>
			<td width="60%"><input type="checkbox" name="ACTIVE" value="Y"<?if(($data['ACTIVE'] == "Y")||(empty($data['ACTIVE']))) echo " checked"?>></td>
		</tr>

		<tr>
			<td><?echo GetMessage("site")?></td>
			<td><?echo CLang::SelectBox("LID", $data['LID']);?></td>
		</tr>
		<tr>
			<td><span class="required">*</span><?echo GetMessage("group_name")?></td>
			<td><input type="text" name="NAME" value="<?echo $data['NAME'];?>" size="30" maxlength="100"></td>
		</tr>

		<tr>
			<td><span class="required">*</span><?echo GetMessage("group_code")?></td>
			<td><input type="text" name="CODE" value="<?echo $data['CODE'];?>" size="30"></td>
		</tr>

		<tr>
			<td><span class="required">*</span><?echo GetMessage("group_title")?></td>
			<td><input type="text" name="TITLE" value="<?echo $data['TITLE'];?>" size="30"></td>
		</tr>

		<tr>
			<td><?echo GetMessage("group_sort")?></td>
			<td><input type="text" name="SORT" value="<?echo $data['SORT'];?>" size="5"></td>
		</tr>

		<?
		$tabControl->Buttons(
			array(
				"disabled"=>($POST_RIGHT<"W"),
				"back_url"=>"vdgb_tszhvote_group_list.php?lang=".LANG,

			)
		);
		?>
		<?echo bitrix_sessid_post();?>
		<input type="hidden" name="lang" value="<?=LANG?>">

		<?
		$tabControl->End();
		?>
	</form>
<?
$tabControl->ShowWarnings("post_form", $message);
?>

<?echo BeginNote();?>
	<span class="required">*</span><?echo GetMessage("REQUIRED_FIELDS")?>
<?echo EndNote();?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>