<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/vdgb.tszhvote/include.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/vdgb.tszhvote/prolog.php");

IncludeModuleLangFile(__FILE__);

CJsCore::Init(array('jquery'));

$modulePermissions = $APPLICATION->GetGroupRight("vdgb.tszhvote");
if ($modulePermissions <= "R") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$redirectURL = BX_ROOT . "/admin/vdgb_tszhvoting_edit.php?lang=" . LANG;
$redirectListURL = BX_ROOT . "/admin/vdgb_tszhvoting.php?lang=" . LANG;
$ufEntity = "CITRUS_POLL";

$tabs = array(
    array(
        "DIV" => "edit1",
        "TAB" => GetMessage("voting_tab_edit1"),
        "ICON" => "main_user_edit",
        "TITLE" => GetMessage("voting_tab_edit1_title")
    ),
);

$tabsControl = new CAdminForm("citrusPollEdit", $tabs);

// ������ ���������� (������������, ������� ����� � �.�.)
$entities = CCitrusPollEntityBase::GetList();
$entityList = $allowedEntitiesForMethod = $entityDescriptions = array();
foreach ($entities as $code => $entity) {
    $entityList[$code] = $entity['NAME'];
    $entityDescriptions[$code] = $entity['DESC'];
}

// ������ ������� �������� �������
$methods = CCitrusPollMethodBase::GetList();
$methodList = $methodDescriptions = array();
foreach ($methods as $methodCode => $method) {
    $methodList[$methodCode] = $method['NAME'];
    $methodDescriptions[$methodCode] = $method['DESC'];
    // �������� ������ ���������� (���������), � ������� �������� ������ ����� �������� �������
    if (isset($method["ENTITIES"]) && is_array($method["ENTITIES"]))
        foreach ($method["ENTITIES"] as $entityCode)
            if (array_key_exists($entityCode, $entityList))
                $allowedEntitiesForMethod[$methodCode][] = $entityCode;
}

$errorMessage = '';
$bVarsFromForm = false;
$ID = isset($_REQUEST["ID"]) && IntVal($_REQUEST["ID"]) > 0 ? IntVal($_REQUEST['ID']) : 0;
$copyFromId = isset($_REQUEST["COPY"]) && IntVal($_REQUEST["COPY"]) > 0 ? IntVal($_REQUEST['COPY']) : 0;

if ($_SERVER["REQUEST_METHOD"] == "POST" && strlen($_REQUEST['Update']) > 0 && $modulePermissions >= "W" && check_bitrix_sessid()) {
    if ($modulePermissions < "W") {
        $errorMessage .= GetMessage("ACCESS_DENIED") . "<br>";
    }

    $updateFields = array(
        "GROUP_ID" => IntVal($_POST["GROUP_ID"]),
        "DATE_BEGIN" => trim($_POST["DATE_BEGIN"]),
        "DATE_END" => trim($_POST["DATE_END"]),
        "TITLE_TEXT" => trim($_POST["TITLE_TEXT"]),
        "DETAIL_TEXT" => trim($_POST["DETAIL_TEXT"]),
        "NAME" => trim($_POST["NAME"]),
        "ACTIVE" => $_POST["ACTIVE"] == "Y" ? "Y" : "N",
        "PERCENT_NEEDED" => isset($_POST["PERCENT_NEEDED"]) ? IntVal($_POST["PERCENT_NEEDED"]) : false,
        "VOTING_COMPLETED" => $_POST["VOTING_COMPLETED"] == "Y" ? "Y" : "N",
        "VOTING_METHOD" => trim($_POST["VOTING_METHOD"]),
        "ENTITY_TYPE" => trim($_POST["ENTITY_TYPE"]),
        "TSZH_ID" => $TSZH_ID,
    );
    $USER_FIELD_MANAGER->EditFormAddFields($ufEntity, $updateFields);

    if ($ID > 0) {
        $success = CCitrusPoll::Update($ID, $updateFields);
    } else {
        $ID = CCitrusPoll::Add($updateFields);
        $success = $ID > 0;
    }
    if (!$success) {
        if ($ex = $APPLICATION->GetException()) {
            $errorMessage .= $ex->GetString() . ".<br />";
        } else {
            $errorMessage .= GetMessage("voting_edit_err") . "<br />";
        }
    }

    if (strlen($errorMessage) <= 0) {
        if (strlen($_REQUEST['apply']) <= 0) {
            LocalRedirect($redirectListURL . GetFilterParams("filter_", false));
        } else {
            LocalRedirect($redirectURL . "&ID=" . $ID . "&" . $tabsControl->ActiveTabParam());
        }
    } else {
        $bVarsFromForm = true;
    }
}

if ($ID > 0) {
    $APPLICATION->SetTitle(GetMessage("VOTING_EDIT_TITLE") . $_GET["ID"]);
} else {
    $APPLICATION->SetTitle(GetMessage("VOTING_ADD_TITLE"));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");


if ($bVarsFromForm) {
    $DB->InitTableVarsForEdit("b_citrus_voting", "", "str_");
} elseif ($ID > 0 || $copyFromId > 0) {
    $dbPoll = CCitrusPoll::GetList(
        Array(),
        Array("ID" => $ID > 0 ? $ID : $copyFromId),
        false,
        Array("nTopCount" => 1),
        Array("*")
    );
    if (!$updateFields = $dbPoll->ExtractFields("str_")) {
        $ID = 0;
    }
} else {
    $str_ACTIVE = "Y";
    $str_GROUP_ID = isset($_GET['id_group']) && IntVal($_GET['id_group']) > 0 ? IntVal($_GET['id_group']) : 0;
    $str_DATE_BEGIN = ConvertTimeStamp();
    $str_DATE_END = ConvertTimeStamp(strtotime('+1 month'));
}

if (strlen($errorMessage) > 0) {
    CAdminMessage::ShowMessage(Array("DETAILS" => $errorMessage, "TYPE" => "ERROR", "MESSAGE" => GetMessage("TE_ERROR_HAPPENED"), "HTML" => true));
}

// ���������� �������� � ������ ������
$questionsCount = CCitrusPollQuestion::GetList(array(), array("VOTING_ID" => $ID), array());

$aMenu = array(
    array(
        "TEXT" => GetMessage("voting_list"),
        "TITLE" => GetMessage("voting_list_title"),
        "LINK" => "/bitrix/admin/vdgb_tszhvoting.php?lang=" . LANGUAGE_ID . "&set_default=Y",
        "ICON" => "btn_list"

    ),
);

if ($ID > 0) {

    $aMenu[] = array(
        "TEXT" => GetMessage("voting_new"),
        "TITLE" => GetMessage("voting_new_title"),
        "LINK" => "/bitrix/admin/vdgb_tszhvoting_edit.php?lang=" . LANGUAGE_ID . "&set_default=Y",
        "ICON" => "btn_new"
    );

    $aMenu[] = array(
        "TEXT" => GetMessage("voting_copy"),
        "TITLE" => GetMessage("voting_copy_title"),
        "LINK" => "/bitrix/admin/vdgb_tszhvoting_edit.php?COPY=" . $ID . "&lang=" . LANGUAGE_ID . "&set_default=Y",
        "ICON" => "btn_copy"
    );

    $aMenu[] = array(
        "TEXT" => GetMessage("voting_del"),
        "TITLE" => GetMessage("voting_del_title"),
        "LINK" => "/bitrix/admin/vdgb_tszhvoting.php?del_element=" . $ID . "&lang=" . LANGUAGE_ID . "&set_default=Y",
        "ICON" => "btn_delete"
    );

    $aMenu[] = array(
        "TEXT" => GetMessage("voting_reset"),
        "TITLE" => GetMessage("voting_reset_title"),
        "LINK" => "/bitrix/admin/vdgb_tszhvoting.php?RESET=" . $ID . "lang=" . LANGUAGE_ID . "&set_default=Y",
        "ICON" => "btn_reset"
    );

    $aMenu[] = array(
        "TEXT" => GetMessage("voting_question") . " [" . $questionsCount . "]",
        "TITLE" => GetMessage("voting_question_title"),
        "LINK" => "/bitrix/admin/vdgb_tszhvoting_question.php?VOTING_ID=" . $ID . "lang=" . LANGUAGE_ID . "&set_default=Y",
        "ICON" => "btn_list"
    );

    $aMenu[] = array(
        "TEXT" => GetMessage("voting_question_new"),
        "TITLE" => GetMessage("voting_question_new_title"),
        "LINK" => "/bitrix/admin/vdgb_tszhvoting_question_edit.php?VOTING_ID=" . $ID . "lang=" . LANGUAGE_ID . "&set_default=Y",
        "ICON" => "btn_new"
    );
}
$context = new CAdminContextMenu($aMenu);
$context->Show();


if (method_exists($USER_FIELD_MANAGER, 'showscript')) {
    $tabsControl->BeginPrologContent();
    echo $USER_FIELD_MANAGER->ShowScript();
    $tabsControl->EndPrologContent();
}

$tabsControl->BeginEpilogContent();
echo GetFilterHiddens("filter_");
?>
    <input type="hidden" name="ID" value="<?=$ID?>"/>
    <input type="hidden" name="Update" value="Y"/>
<?
echo bitrix_sessid_post();
$tabsControl->EndEpilogContent();

$tabsControl->Begin(array(
    "FORM_ACTION" => $APPLICATION->GetCurPage(),
    "FORM_ATTRIBUTES" => "method=\"POST\" enctype=\"multipart/form-data\"",
));


$pollGroups = Array();
$dbGroup = CCitrusPollGroup::GetList(array("SORT"), array("ACTIVE" => "Y"), false, false, array("ID", "NAME", "CODE"));
while ($group = $dbGroup->GetNext(false)) {
    $pollGroups[$group["ID"]] = "[" . $group["CODE"] . "] " . $group["NAME"];
}


$tabsControl->BeginNextFormTab();

if ($ID > 0) {
    $tabsControl->AddViewField("ID", "ID", $ID);
    $tabsControl->AddViewField("TIMESTAMP_X", GetMessage("TIMESTAMP_X"), $str_TIMESTAMP_X);
}

$tabsControl->AddCheckBoxField("ACTIVE", GetMessage("voting_act"), false, "Y", ($str_ACTIVE == "Y"));
$tabsControl->AddDropDownField("GROUP_ID", GetMessage("voting_group_id"), true, $pollGroups, $str_GROUP_ID);
$tabsControl->AddEditField("NAME", GetMessage("voting_name"), true, array("size" => 30, "maxlength" => 255), $str_NAME);
$tabsControl->AddCalendarField("DATE_BEGIN", GetMessage("voting_date_begin"), $str_DATE_BEGIN, true);
$tabsControl->AddCalendarField("DATE_END", GetMessage("voting_date_end"), $str_DATE_END, true);
$tabsControl->AddEditField("PERCENT_NEEDED", GetMessage("voting_count_of_comp"), false, array("size" => 5, "maxlength" => 3), $str_PERCENT_NEEDED);
$tabsControl->AddCheckBoxField("VOTING_COMPLETED", GetMessage("voting_completed"), false, "Y", ($str_VOTING_COMPLETED == "Y"));
$tabsControl->AddDropDownField("ENTITY_TYPE", GetMessage("CV_ENTITY_TYPE"), true, $entityList, $str_ENTITY_TYPE);
$tabsControl->AddDropDownField("VOTING_METHOD", GetMessage("voting_method"), true, $methodList, $str_VOTING_METHOD);

if (CModule::IncludeModule("citrus.tszh")) {
    $dbTszh = CTszh::GetList(Array("NAME" => "ASC"));
    $tszhList = Array('' => GetMessage("CV_ALL"));
    while ($tszh = $dbTszh->Fetch()) {
        $tszhList[$tszh['ID']] = $tszh["NAME"];
    }
    $tabsControl->AddDropDownField("TSZH_ID", GetMessage("CV_TSZH_ID"), false, $tszhList, $str_TSZH_ID);
    $tabsControl->BeginCustomField("TSZH_ID_NOTE", GetMessage("CV_TSZH_ID_NOTE_TITLE"), false);
    ?>
    <tr id="tr_TSZH_ID_NOTE">
        <td width="20%" valign="top"></td>
        <td width="80%" valign="top">
            <? echo BeginNote();?>
            <?=GetMessage("CV_TSZH_ID_NOTE")?>
            <? echo EndNote();?>
        </td>
    </tr>
    <?
    $tabsControl->EndCustomField("TSZH_ID_NOTE");
}

$tabsControl->AddSection('PUBLIC_SECTION', GetMessage("CV_PUBLIC_SECTION"));
$tabsControl->AddEditField("TITLE_TEXT", GetMessage("TITLE_TEXT"), false, array("size" => 80, "maxlength" => 255), $str_TITLE_TEXT);
$tabsControl->BeginCustomField("DETAIL_TEXT", GetMessage("DETAIL_TEXT"), false);
?>
    <tr>
        <td width="20%" valign="top"><label for="DETAIL_TEXT"><?=GetMessage("DETAIL_TEXT");?></label></td>
        <td width="80%" valign="top">
            <?
            if (CModule::IncludeModule("fileman")):
                $LHE = new CLightHTMLEditor();
                $LHE->Show(array(
                    'id' => 'DETAIL_TEXT',
                    'width' => '100%',
                    'height' => '200px',
                    'inputName' => "DETAIL_TEXT",
                    'content' => htmlspecialcharsback($str_DETAIL_TEXT),
                    'bUseFileDialogs' => false,
                    'bFloatingToolbar' => true,
                    'bArisingToolbar' => true,
                    'toolbarConfig' => array(
                        'Bold',
                        'Italic',
                        'Underline',
                        'RemoveFormat',
                        'CreateLink',
                        'DeleteLink',
                        'Image',
                        'Video',
                        //'BackColor', 'ForeColor',
                        'JustifyLeft',
                        'JustifyCenter',
                        'JustifyRight',
                        'JustifyFull',
                        'InsertOrderedList',
                        'InsertUnorderedList',
                        'Outdent',
                        'Indent',
                        'StyleList',
                        'HeaderList',
                        //'FontList', 'FontSizeList',
                    ),
                ));
            else:
                ?>
                <textarea rows="7" cols="60" name="DETAIL_TEXT"><?=$str_DETAIL_TEXT?></textarea>
            <?
            endif;
            ?>
        </td>
    </tr>
<?
$tabsControl->EndCustomField("DETAIL_TEXT", '<input type="hidden" name="DETAIL_TEXT" value="' . $str_DETAIL_TEXT . '">');

if ($USER_FIELD_MANAGER->GetRights($ufEntity) >= "W") {
    $tabsControl->AddSection('USER_FIELDS', GetMessage("USER_FIELDS"));
    $tabsControl->ShowUserFields($ufEntity, $str_ID, $bVarsFromForm);
}


$tabsControl->Buttons(Array(
    "disabled" => ($modulePermissions < "W"),
    "back_url" => $redirectListURL . GetFilterParams("filter_"),
));

$tabsControl->Show();
$tabsControl->ShowWarnings($tabsControl->GetName(), $message);

if (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):?>
    <? echo BeginNote();?>
    <span class="required">*</span> <? echo GetMessage("REQUIRED_FIELDS")?>
    <? echo EndNote(); ?>
<? endif; ?>

<script type="text/javascript">
;(function(window, $){
    $(function () {
        var allowedEntitiesForMethod = <?=CUtil::PhpToJSObject($allowedEntitiesForMethod)?>,
            methodDescriptions =  <?=CUtil::PhpToJSObject($methodDescriptions)?>,
            entityDescriptions =  <?=CUtil::PhpToJSObject($entityDescriptions)?>,
            $entityNode = $('select[name=ENTITY_TYPE]'),
            $methodNode = $('select[name=VOTING_METHOD]');

        $entityNode.after('<span class="desc" style="margin-left: 1em; font-style: italic;"></span>');
        $methodNode.after('<span class="desc" style="margin-left: 1em; font-style: italic;"></span>');

        var entityChanged = function() {
            var currentEntity = $entityNode.val(),
                currentMethod = $methodNode.val(),
                disablingSelected = false;

            $entityNode.siblings('.desc').text(entityDescriptions[currentEntity]);

            $('#tr_TSZH_ID, #tr_TSZH_ID_NOTE').toggle('tszh_account' === currentEntity)
            $('select[name=VOTING_METHOD] option').each(function () {
                var method = this.value,
                    enabled = false;
                if (method in allowedEntitiesForMethod) {
                    enabled = allowedEntitiesForMethod[method].indexOf(currentEntity) >= 0;
                    if (!enabled && method == currentMethod)
                        disablingSelected = true;
                } else {
                    enabled = true;
                }
                if (disablingSelected) {
                    $methodNode.val($methodNode.find('option:first').val());
                    methodChanged();
                }

                $(this).css('display', enabled ? '' : 'none');
            });
        };
        var methodChanged = function() {
            var currentMethod = $methodNode.val();
            $methodNode.siblings('.desc').text(methodDescriptions[currentMethod]);
        };

        $entityNode.change(entityChanged);
        $methodNode.change(methodChanged);
        entityChanged();
        methodChanged();
    });
})(window, $);
</script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>