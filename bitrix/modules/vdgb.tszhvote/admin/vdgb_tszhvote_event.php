<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/prolog.php");

IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("vdgb.tszhvote");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$sTableID = "tbl_voting_event";
$oSort = new CAdminSorting($sTableID, "ID", "ASC");
$lAdmin = new CAdminList($sTableID, $oSort);

// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
  global $FilterArr, $lAdmin;
  foreach ($FilterArr as $value) {
    global $$value;
  }

  return true;

  // $lAdmin->AddFilterError('�����_������').

  return count($lAdmin->arFilterErrors)==0; // ���� ������ ����, ������ false;
}

// ������ �������� �������
$FilterArr = Array(
	"find",
	"find_type",
	"find_id",
	"find_voting_id",
	"find_user_id",
	"find_ip",
	"find_valid",
	"find_timestap_x",
	"find_question_id",
);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //

if(isset($_GET['find_id'])) {
    $find_id = IntVal($_GET['find_id']);
    $arFilter["ID"] = IntVal($_GET['find_id']);
}


// ���� ��� �������� ������� ���������, ���������� ���
if (CheckFilter()) {
  // �������� ������ ���������� ��� ������� �� ������ �������� �������
    $arFilter = array();

    if($find_name!="") {
        $arFilter["NAME"] = $find_name;
    }

    if(IntVal($find_voting_id) > 0) {
        $arFilter["VOTING_ID"] = IntVal($find_voting_id);
    }

    //$arFilter["USER_ID"] = IntVal($find_voting_id);

    if(strlen($find_ip) > 0) {
        $arFilter["IP"] = htmlspecialcharsbx($find_ip);
    }

    if(IntVal($find_question_id) > 0) {
        $arFilter["QUESTION_ID"] = IntVal($find_question_id);
    }


    if(strlen($find_id) > 0) {
        $arFilter["ID"] = $find_id;
    }

    if($find!="" && $find_type == "id") {
        if(IntVal($find) > 0) {
            $arFilter["ID"] = IntVal($find);
        }
        else {
            if(IntVal($find_id) > 0) {
                $arFilter["ID"] = IntVal($find_id);
            }
        }
    }
    elseif(strlen($find) > 0 && $find_type == "find_voting_id") {
        $arFilter["VOTING_ID"] = $find;
        if(IntVal($find_id) > 0) {
            $arFilter["ID"] = IntVal($find_id);
        }
    }
    else {
        if(IntVal($find_id) > 0) {
            $arFilter["ID"] = IntVal($find_id);
        }
    }

}
else {
    $arFilter = array();
}

// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //

// ��������� ��������� � ��������� ��������
if(($arID = $lAdmin->GroupAction()) && $POST_RIGHT=="W")
{
	// ���� ������� "��� ���� ���������"
	if($_REQUEST['action_target']=='selected')
	{
		$cData = new CCitrusPollVote;
		$rsData = $cData->GetList(array($by=>$order), $arFilter);
		while($arRes = $rsData->Fetch())
			$arID[] = $arRes['ID'];
	}
	
	// ������� �� ������ ���������
	foreach($arID as $ID)
	{
		if(strlen($ID)<=0)
			continue;
		$ID = IntVal($ID);
	
		// ��� ������� �������� �������� ��������� ��������
		switch($_REQUEST['action'])
		{
			// ��������
			case "delete":
				if (!CCitrusPollVote::Delete($ID))
				{
					$lAdmin->AddGroupError(GetMessage("vote_del_error"), $ID);
				}
				break;
		}
	}
}

// ������� ������ �����
$rsData = CCitrusPollVote::GetList(array($by=>$order), $arFilter);

// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("page_nav")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

$lAdmin->AddHeaders(array(
  array(
        "id"        =>  "ID",
        "content"   =>  "ID",
        "sort"      =>  "id",
        "align"     =>  "right",
        "default"   =>  true,
  ),

  array(
        "id"    =>"VOTING_ID",
        "content"  =>GetMessage("voting_name"),
        "sort"    =>"VOTING_ID",
        "default"  =>true,
  ),

  array(
        "id"    =>"USER_ID",
        "content"  => GetMessage("user_id"),
        "sort"    =>"USER_ID",
        "align"     =>  "centr",
        "default"  =>true,
  ),

  array(
        "id"    =>"WEIGHT",
        "content"  => GetMessage("F_WEIGHT"),
        "sort"    =>"WEIGHT",
        "align"     =>  "right",
        "default"  =>true,
  ),

  array(
        "id"    =>"TIMESTAMP_X",
        "content"  =>GetMessage("timestap_x"),
        "sort"    =>"TIMESTAMP_X",
        "default"  =>false,
  ),

  array(
        "id"        => "IP",
        "content"   => "IP",
        "sort"      => "IP",
        "default"   => false,
  ),

  array(
        "id"        =>  "QUESTION_ID",
        "content"   =>  GetMessage("question_id"),
        "sort"      =>  "QUESTION_ID",
        "default"   =>  true,
  ),

  array(
        "id"        =>  "VALID",
        "content"   =>  GetMessage("valid"),
        "sort"      =>  "VALID",
        "default"   =>  true,
  ),

  array(
        "id"        =>  "VOTE",
        "content"   =>  GetMessage("vote"),
        "sort"      =>  "VOTE",
        "default"   =>  true,
  ),

));

while($arRes = $rsData->NavNext(true, "f_")):

    // ������� ������. ��������� - ��������� ������ CAdminListRow
    $row =& $lAdmin->AddRow($f_ID, $arRes);
    //print_r($row);
    // ����� �������� ����������� �������� ��� ��������� � �������������� ������

    // �������� NAME ����� ��������������� ��� �����, � ������������ �������
    $row->AddViewField("NAME", '<a href="vdgb_tszhvoting_edit.php?ID='.$f_ID.'&lang='.LANG.'">'.$f_NAME.'</a>');
    $row->AddInputField("NAME", array("size"=>20));

    //VOTING_ID
    $arVoting = CCitrusPoll::GetByID($f_VOTING_ID);
    if (is_array($arVoting) && !empty($arVoting)) 
	{
        $votingTitle = $arVoting['NAME'];
    }
    $row->AddViewField("VOTING_ID", '<span>[<a href="vdgb_tszhvoting_edit.php?ID='.$f_VOTING_ID.'&lang='.LANG.'">'.$f_VOTING_ID.'</a>]</span><span>'. $votingTitle .'</sapn>');

    $userTitle = GetMessage("EMPTY_USER");
    $arUser = $USER->GetByID($f_USER_ID)->GetNext();
    if (is_array($arUser))
	{
        $userTitle = "(". $arUser['LOGIN'] .") ". $arUser['NAME'] ." ". $arUser['LAST_NAME'];
		$userTitle = '<span>[<a href="user_edit.php?ID='.$f_USER_ID.'&lang='.LANG.'">'.$f_USER_ID.'</a>]</span> <span>'. $userTitle .'</span>';
    }
    //�������� USER_ID ����� ����������� � ���� ������ �� ������ �������������
    $row->AddViewField("USER_ID", $userTitle);
    
    // ����� VOTING_COMPLETED  ����� ��������������� ����������
    $row->AddCheckField("VOTING_COMPLETED");

    // ����� ACTIVE  ����� ��������������� ����������
    $row->AddCheckField("ACTIVE");


   //�������� QUESTION_ID ����� ����������� � ���� ������ �������
    $questionTitle = "";
    $rsQuets = CCitrusPollQuestion::GetByID($f_QUESTION_ID);
    if(is_object($rsQuets)){
        $questionInfo = $rsQuets->Fetch();
        $questionTitle = $questionInfo['TEXT'];
    }
    $row->AddViewField("QUESTION_ID", '<span>['. $f_QUESTION_ID .']'. $questionTitle .'</sapn>');

    //VOTE
	$row->AddViewField("VOTE", '<a href="vdgb_tszhvote_form.php?VOTING_ID='. $f_VOTING_ID .'&lang='.LANG.'">' . GetMessage("VIEW_VOTING") . '</a>');
 
endwhile;

// ������ �������
$lAdmin->AddFooter(
  array(
    array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()), // ���-�� ���������
    array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"), // ������� ��������� ���������
  )
);

// ��������� ��������
$lAdmin->AddGroupActionTable(Array(
    "delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
    "activate"=>GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // ������������ ��������� ��������
    "deactivate"=>GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // �������������� ��������� ��������
));

$lAdmin->AddAdminContextMenu();

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //

// �������������� �����
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("VOTE_EVENT_LIST_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

$oFilter = new CAdminFilter(
	$sTableID."_filter",
	array(
            GetMessage("ID"),
            GetMessage("VOTING_ID"),
            GetMessage("USER_ID"),
            GetMessage("IP"),
            GetMEssage("VALID"),
            GetMessage("TIMESTAMP_X"),
            GetMessage("QUESTION_ID"),
	)
);

?>

<form name="form_filter" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
<?$oFilter->Begin();?>
    
<tr>
	<td><b><?=GetMessage("voting_event_find")?>:</b></td>
	<td>
		<input type="text" size="40" name="find" value="<?echo htmlspecialcharsbx($find)?>" title="<?=GetMessage("voting_event_find_title")?>">
		<?
		$arr = array(
			"reference" => array(
				GetMessage("find_voting_id"),
				"ID",
			),
			"reference_id" => array(
				"VOTING_ID",
				"ID",
			)
		);
		echo SelectBoxFromArray("find_type", $arr, $find_type, "", "");
		?>
	</td>
</tr>

<tr>
	<td><?=GetMessage("find_id")?></td>
	<td><input type="text" name="find_id" size="5" value="<?echo htmlspecialcharsbx($find_id)?>"></td>
</tr>

<tr>
	<td><?=GetMessage("find_voting_id")?></td>
	<td><input type="text" name="find_voting_id" size="5" value="<?echo htmlspecialcharsbx($find_voting_id)?>"></td>
</tr>

<tr>
	<td><?=GetMessage("find_user_id")?></td>
	<td>
            <?=FindUserID("find_user_id",$find_user_id,"","form_filter","5");?>
            <!---input type="text" name="find_user_id" size="25" value="<?//echo htmlspecialcharsbx($find_user_id)?>"></td-->

</tr>

<tr>
	<td><?=GetMessage("find_ip")?></td>
	<td><input type="text" name="find_ip" size="25" value="<?echo htmlspecialcharsbx($find_ip)?>"></td>

</tr>

<tr>
	<td><?=GetMessage("find_valid")?></td>
	<td><input type="checkbox" name="find_valid" value="<?echo htmlspecialcharsbx($find_valid)?>"></td>
</tr>

<tr>
	<td><?=GetMessage("find_timestap_x")?>
        </td>
        <td>
            <input type="text" name="find_timestap_x" size="9" value="<?echo htmlspecialcharsbx($find_timestap_x)?>">
            <?=Calendar("find_timestap_x","form_filter") ?>
        </td>
</tr>

<tr>
	<td><?=GetMessage("find_question_id")?></td>
	<td><input type="text" name="find_question_id" size="25" value="<?echo htmlspecialcharsbx($find_question_id)?>"></td>

</tr>

<?
$oFilter->Buttons(array("table_id"=>$sTableID,"url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
$oFilter->End();
?>
</form>


<?

if(isset($error)) {
    echo CAdminMessage::ShowNote($error);
}

// ������� ������� ������ ���������
$lAdmin->DisplayList();
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>