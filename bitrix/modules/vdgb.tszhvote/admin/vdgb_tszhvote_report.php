<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/prolog.php");

IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("vdgb.tszhvote");
if ($POST_RIGHT < "R") {
	ShowError(GetMessage("ACCESS_DENIED"));
	return;
}

$printTemplate = preg_replace('/[^\d\w_.]+/i', '', $_REQUEST['tpl']);
$voteID = IntVal($_REQUEST['id']);
if ($voteID <= 0)
{
	ShowError(GetMessage("ERROR_VOTE_NOT_FOUND"));
	return;
}

$arTemplates = CCitrusPollReport::GetTemplates();
if (!array_key_exists($printTemplate, $arTemplates))
{
	ShowError(GetMessage("ERROR_TEMPLATE_NOT_FOUND"));
	return;
}

$arTpl = $arTemplates[$printTemplate];

$reportData = CCitrusPollReport::GetData($voteID);
//$APPLICATION->RestartBuffer();
include($arTpl['PATH']);
            
?>
