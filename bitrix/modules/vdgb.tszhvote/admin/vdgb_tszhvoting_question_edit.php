<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/vdgb.tszhvote/include.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/vdgb.tszhvote/prolog.php");

IncludeModuleLangFile(__FILE__);

CUtil::InitJSCore();

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("vdgb.tszhvote");
if ($POST_RIGHT == "D") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

//�������� �������� ������� � ������������� ������. ���� �������� ���, �� �������������� ��� 
// �� �������� �� ������� �������
if (isset($_REQUEST['VOTING_ID']) && IntVal($_REQUEST['VOTING_ID']) > 0) {

    $arFilter = array("VOTING_ID" => IntVal($_REQUEST['VOTING_ID']));

    $VOTING_ID = IntVal($_REQUEST['VOTING_ID']);

    $update['VOTING_ID'] = $VOTING_ID;
    $strURL = "?VOTING_ID=" . $VOTING_ID;

    $answer_list = array();
    $updateFields = array();

    //���� �� ����������� ������, ������� ������ ��������� ��������� ������, ��� ������� �������
    if (isset($_REQUEST['ID']) && IntVal($_REQUEST['ID']) > 0) {

        $CURENT_ID = $_REQUEST['ID'];

        $rsList = CCitrusPollAnswer::GetList(array("SORT" => "ASC"), array("QUESTION_ID" => $CURENT_ID));
        if (is_object($rsList)) {
            while ($ar = $rsList->Fetch()) {
                $answer_list[] = $ar;
                $arUpdateAnswer[$ar['ID']] = $ar;
            }
        }

        //������� ������ �������� �������
        $rsListQuestion = CCitrusPollQuestion::GetList(array(), array("ID" => $CURENT_ID));
        if (is_object($rsListQuestion)) {
            while ($arQ = $rsListQuestion->Fetch()) {
                $updateFields = $arQ;
            }
        }

        $update = $updateFields;

        // ���� ������ �� ������ �� �������������� ������������ �� �������� �� ������� �������
        if (empty($updateFields)) {
            LocalRedirect("/bitrix/admin/vdgb_tszhvoting.php?lang=ru");
        }
    } else {
        $update = array(
            "ACTIVE" => "Y",
            "SORT" => "500",
        );
    }

} else {
    LocalRedirect("/bitrix/admin/vdgb_tszhvoting.php?lang=ru");
}

// ���� ��������� POST ������, �������� ���������� ����� ������
if (isset($_POST) && !empty($_POST)) {
    $arAddField = array();

    //�������� ���� �� ��������� ����� ������
    if (isset($_POST['new_TEXT_field_1']) && strlen($_POST['new_TEXT_field_1']) > 0) {
        $arField1 = array();

        $arField1["TEXT"] = htmlspecialcharsbx($_POST['new_TEXT_field_1']);
        $arField1["COLOR"] = htmlspecialcharsbx($_POST['old_COLOR_01']);

        if (isset($_POST['new_SORT_field_1']) && strlen($_POST['new_SORT_field_1']) > 0) {
            $arField1["SORT"] = IntVal($_POST['new_SORT_field_1']);
        }

        if (isset($_POST['new_ACTIVE_field_1']) && strlen($_POST['new_ACTIVE_field_1']) > 0) {
            $arField1["ACTIVE"] = "Y";
        } else {
            $arField1["ACTIVE"] = "N";
        }

        $arAddField[] = $arField1;
    }

    if (isset($_POST['new_TEXT_field_2']) && strlen($_POST['new_TEXT_field_2']) > 0) {
        $arField2 = array();

        $arField2["TEXT"] = htmlspecialcharsbx($_POST['new_TEXT_field_2']);
        $arField2["COLOR"] = htmlspecialcharsbx($_POST['old_COLOR_02']);

        if (isset($_POST['new_SORT_field_2']) && strlen($_POST['new_SORT_field_2']) > 0) {
            $arField2["SORT"] = IntVal($_POST['new_SORT_field_2']);
        }

        if (isset($_POST['new_ACTIVE_field_2']) && strlen($_POST['new_ACTIVE_field_2']) > 0) {
            $arField2["ACTIVE"] = "Y";
        } else {
            $arField2["ACTIVE"] = "N";
        }

        $arAddField[] = $arField2;
    }

    if (isset($_POST['new_TEXT_field_3']) && strlen($_POST['new_TEXT_field_3']) > 0) {
        $arField3 = array();

        $arField3["TEXT"] = htmlspecialcharsbx($_POST['new_TEXT_field_3']);
        $arField3["COLOR"] = htmlspecialcharsbx($_POST['old_COLOR_03']);

        if (isset($_POST['new_SORT_field_3']) && strlen($_POST['new_SORT_field_3']) > 0) {
            $arField3["SORT"] = IntVal($_POST['new_SORT_field_3']);
        }

        if (isset($_POST['new_ACTIVE_field_3']) && strlen($_POST['new_ACTIVE_field_3']) > 0) {
            $arField3["ACTIVE"] = "Y";
        } else {
            $arField3["ACTIVE"] = "N";
        }

        $arAddField[] = $arField3;
    }

    // �������� ������������ ����� ������ �������
    if (isset($_POST['SORT']) && strlen($_POST['SORT']) > 0) {
        $update['SORT'] = IntVal($_POST['SORT']);
    }

    if (isset($_POST['TEXT'])) {
        $update['TEXT'] = htmlspecialcharsbx($_POST['TEXT']);
    }

    if (isset($_POST['ACTIVE']) && strlen($_POST['ACTIVE']) > 0) {
        $update['ACTIVE'] = "Y";
    } else {
        $update['ACTIVE'] = "N";
    }

    if (isset($_POST['IS_MULTIPLE']) && strlen($_POST['IS_MULTIPLE']) > 0) {
        $update['IS_MULTIPLE'] = "Y";
    } else {
        $update['IS_MULTIPLE'] = "N";
    }

    $update['VOTING_ID'] = $VOTING_ID;

    $isOK = true;

    if (isset($_REQUEST["ID"]) && IntVal($_REQUEST["ID"]) > 0) {
        if (isset($arUpdateAnswer) && !empty($arUpdateAnswer)) {
            //�������� ���� �� ��������������� ����������� �������
            foreach ($arUpdateAnswer as $answer) {

                if (isset($_POST['old_ID_' . $answer['ID']]) && IntVal($_POST['old_ID_' . $answer['ID']]) > 0) {
                    $arUpdateAnswer[$answer['ID']]['ID'] = IntVal($_POST['old_ID_' . $answer['ID']]);
                }

                if (isset($_POST['old_TEXT_' . $answer['ID']]) && strlen($_POST['old_TEXT_' . $answer['ID']]) > 0) {
                    $arUpdateAnswer[$answer['ID']]['TEXT'] = htmlspecialcharsbx($_POST['old_TEXT_' . $answer['ID']]);
                }

                if (isset($_POST['old_COLOR_' . $answer['ID']]) && strlen($_POST['old_COLOR_' . $answer['ID']]) > 0) {
                    $arUpdateAnswer[$answer['ID']]['COLOR'] = htmlspecialcharsbx($_POST['old_COLOR_' . $answer['ID']]);
                }

                if (isset($_POST['old_SORT_' . $answer['ID']]) && IntVal($_POST['old_SORT_' . $answer['ID']]) > 0) {
                    $arUpdateAnswer[$answer['ID']]['SORT'] = IntVal($_POST['old_SORT_' . $answer['ID']]);
                }
                if (isset($_POST['old_ACTIVE_' . $answer['ID']]) && strlen($_POST['old_ACTIVE_' . $answer['ID']]) > 0) {
                    $arUpdateAnswer[$answer['ID']]['ACTIVE'] = "Y";
                } else {
                    $arUpdateAnswer[$answer['ID']]['ACTIVE'] = "N";
                }

                if (isset($_POST['old_DEL_' . $answer['ID']]) && $_POST['old_DEL_' . $answer['ID']] == "Y") {
                    $arUpdateAnswer[$answer['ID']]['ACTION'] = "del";
                } else {
                    $arUpdateAnswer[$answer['ID']]['ACTION'] = "edit";
                }
            }
        }

        @set_time_limit(0);
        $DB->StartTransaction();

        //update ������������ �������
        if (!empty($arUpdateAnswer)) {
            foreach ($arUpdateAnswer as $id => $value) {
                if (IntVal($id) > 0 && $isOK) {
                    if ($value['ACTION'] == "del") {
                        if (!CCitrusPollAnswer::Delete(IntVal($id))) {
                            $isOK = false;
                        } else {
                            unset($arUpdateAnswer[IntVal($id)]);
                        }
                    } else {
                        unset($arUpdateAnswer[IntVal($id)]['ACTION']);
                        if (!CCitrusPollAnswer::Update(IntVal($id), $arUpdateAnswer[IntVal($id)])) {
                            $isOK = false;
                        }
                    }
                } else {
                    continue;
                }
            }
        }

        //������� ����� ������ ���� ��� ��������������� ������� �������� ����� ������
        if (!empty($arAddField)) {
            foreach ($arAddField as $id => $value) {
                if (!empty($value)) {
                    $value["QUESTION_ID"] = IntVal($_REQUEST["ID"]);
                    $id = CCitrusPollAnswer::Add($value);
                    if (!$id) {
                        $isOK = false;
                    } else {
                        $value['ID'] = $id;
                        $arUpdateAnswer[$id] = $value;
                    }
                } else {
                    continue;
                }
            }
        }

        // ��������� � �������������� �������, ���� �������������� �������� ������
        // ������� (���� ��� ���� ����������)
        if ($isOK) {
            if (!CCitrusPollQuestion::Update(IntVal($_POST["ID"]), $update)) {
                $DB->Rollback();
                $error = GetMessage("question_edit_err");
                $errorType = 1;
            } else {
                $errorType = 0;
                if (isset($_POST["apply"])) {
                    $error = GetMessage("question_save_ok");
                } else {
                    $error = GetMessage("question_edit_ok");
                }
            }
        } else {
            $DB->Rollback();
        }

        $update = $updateFields;

        $DB->Commit();

        if (isset($_POST["save"]) && !isset($error)) {
            LocalRedirect("vdgb_tszhvoting_question.php?VOTING_ID=" . $VOTING_ID . "&lang=" . LANG);
        }
    } else {
        @set_time_limit(0);
        $DB->StartTransaction();

        $newId = CCitrusPollQuestion::Add($update);
        if (!$newId) {
            $DB->Rollback();
            $errorType = 1;
            $error = GetMessage("voting_add_err");
        } else {
            $CURENT_ID = $newId;
            $cAnswer = new CCitrusPollAnswer();

            if (!empty($arAddField)) {
                foreach ($arAddField as $id => $value) {
                    if (!empty($value)) {
                        $value["QUESTION_ID"] = $newId;
                        $id = CCitrusPollAnswer::Add($value);
                        if (!$id) {
                            $isOK = false;
                        } else {
                            $arUpdateAnswer["ID"] = $id;
                            $arUpdateAnswer[$id] = $value;
                        }
                    } else {
                        continue;
                    }
                }
            }

            if (!$isOK) {
                $DB->Rollback();
                $errorType = 1;
                $error = GetMessage("voting_add_err");
            } else {
                $CURENT_ID = $newId;
                $errorType = 0;
                $error = GetMessage("voting_add_ok");
            }
        }
        $DB->Commit();

        if (isset($_POST["save"]) && !isset($error)) {
            LocalRedirect("vdgb_tszhvoting_question.php?VOTING_ID=" . $VOTING_ID . "&lang=" . LANG);
        }
    }
}

$aTabs = array(
    array(
        "DIV" => "edit1",
        "TAB" => GetMessage("question_tab_edit1"),
        "ICON" => "main_user_edit",
        "TITLE" => GetMessage("question_tab_edit1_title")
    ),
);

$aMenu = array(
    array(
        "TEXT" => GetMessage("question_list"),
        "TITLE" => GetMessage("question_list_title"),
        "LINK" => "/bitrix/admin/vdgb_tszhvoting_question.php?VOTING_ID=" . $VOTING_ID . "&lang=" . LANGUAGE_ID . "&set_default=Y",
        "ICON" => "btn_list"
    ),
);

if (isset($_GET["ID"]) && IntVal($_GET["ID"]) > 0) {
    $APPLICATION->SetTitle(GetMessage("QUESTION_EDIT_TITLE") . $_GET["ID"]);
} else {
    $APPLICATION->SetTitle(GetMessage("QUESTION_ADD_TITLE"));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");


$context = new CAdminContextMenu($aMenu);
$context->Show();

$tabControl = new CAdminTabControl("tabControl", $aTabs);


/************** Table of colors ************************************/
$t_COL = array("00", "33", "66", "99", "CC", "FF");
?>
    <div id="ColorPick" style="display:block;position:absolute;top:0;left:0;z-index:1500;">
        <table cellspacing="0" cellpadding="1" border="0" bgcolor="#666666">
            <tr>
                <td colspan=2>
                    <table cellspacing="1" cellpadding="0" border="0" bgcolor="#FFFFFF">
                        <?
                        for ($i = 0; $i < 216; $i++) {
                            $t_R = $i % 6;
                            $t_G = floor($i / 36) % 6;
                            $t_B = floor($i / 6) % 6;
                            $t_curCOL = "#" . $t_COL[$t_R] . $t_COL[$t_G] . $t_COL[$t_B];
                            print ($i % 18 == 0) ? "<tr>" : "";
                            ?>
                            <td bgcolor='<?=$t_curCOL?>'><a href='javascript:void(0)' onmousedown='javascript:col_set("<?=$t_curCOL?>")' <?
                                ?>onmouseover='javascript:col_show("<?=$t_curCOL?>")'><img src=/bitrix/images/1.gif border="0" width="10" height="10"></a></td>
                        <?
                        }
                        ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td width=50% style="border:1px solid black" id="t_fillCOL"><img src=/bitrix/images/1.gif style="width:100%;height:5px"></td>
                <td width=50%><input id="t_COL" size=10 style="width:100%;border:1px solid black"></td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        <!--
        jsUtils.addEvent(document, "mousedown", function (e) {
            hidePicker();
        });
        jsUtils.addEvent(document, "keypress", function (e) {
            hidePicker();
        });

        function col_show(clr) {
            BX('t_COL').value = clr;
            BX('t_fillCOL').style.backgroundColor = clr;
        }

        function col_set(clr, node) {
            var node = node || top.node;
            node.value = clr;
            BX.adjust(node.parentNode, {"style": {"backgroundColor": clr}});
        }

        function hidePicker() {
            var pickerNode = BX("ColorPick");
            if (pickerNode)
                BX.adjust(pickerNode, {"style": {"display": "none"}});
        }

        function ColorPicker(node) {
            top.node = node;
            try {
                var res = BX.pos(node);
                BX.adjust(BX('ColorPick'), {
                    "style": {
                        "display": "block",
                        "top": (res["top"] + 22) + 'px',
                        "left": (res["left"] - 204) + 'px'
                    }
                });
                col_show(node.value);
            } catch (e) {
            }
        }

        document.body.appendChild(BX('ColorPick'));
        hidePicker();

        //-->
    </script>
<?
/************** Table of colors/************************************/
?>

    <form method="POST" Action="<? echo $APPLICATION->GetCurPage() . $strURL; ?>" ENCTYPE="multipart/form-data" name="post_form">
        <input type="hidden" name="ID" value="<? echo $CURENT_ID; ?>">
        <?

        $tabControl->Begin();

        $tabControl->BeginNextTab();

        ?>

        <tr>
            <td width="40%"><span class="required">*</span><? echo GetMessage("voting_id") ?></td>
            <td width="60%">
                <div id="VOTING_ID"><?=$VOTING_ID?></div>
            </td>
        </tr>

        <tr>
            <td width="40%"><? echo GetMessage("question_act") ?></td>
            <td width="60%">
                <input type="checkbox" name="ACTIVE" value="Y"<?=($update['ACTIVE'] == "Y" ? " checked" : '')?>>
            </td>
        </tr>


        <tr>
            <td width="40%"><? echo GetMessage("is_multiplete") ?></td>
            <td width="60%">
                <input type="checkbox" name="IS_MULTIPLE" value="Y"<?=($update['IS_MULTIPLE'] == "Y" ? " checked" : '')?>>
            </td>
        </tr>

        <tr>
            <td width="40%"><? echo GetMessage("sort") ?></td>
            <td width="60%">
                <input type="text" size="4" name="SORT" value="<?=$update['SORT']?>">
            </td>
        </tr>

        <tr>
            <td width="40%"><span class="required">*</span><? echo GetMessage("text") ?></td>
            <td width="60%">
                <textarea name="TEXT" cols="50" rows="5"><?=$update["TEXT"]?></textarea>
            </td>
        </tr>

        <?
        //$tabControl->BeginNextTab();
        ?>
        <tr class="heading">
            <td colspan="2"><?=GetMessage("question_tab_edit2")?></td>
        </tr>

        <tr>
            <td colspan="2">
                <table cellspacing="0" cellpadding="0" border="0" class="internal" style="margin: 0 auto;">
                    <thead>
                    <tr class="heading">
                        <td><?=GetMessage("answer_id");?></td>
                        <td><?=GetMessage("answer_title");?></td>
                        <td><?=GetMessage("answer_sort");?></td>
                        <td><?=GetMessage("answer_color");?></td>
                        <td><?=GetMessage("answer_act");?></td>
                        <td><?=GetMessage("answer_del");?></td>
                    </tr>
                    </thead>
                    <tbody>

                    <?

                    $maxSort = 0;

                    if (!empty($arUpdateAnswer)): ?>
                        <? foreach ($arUpdateAnswer as $key => $answer):

                            if ($answer["SORT"] > $maxSort) {
                                $maxSort = $answer["SORT"];
                            }

                            ?>
                            <tr>

                                <td>
                                    <input type="hidden" name="old_ID_<?=$answer['ID']?>" value="<?=$answer['ID']?>">
                                    <span><?=$answer['ID']?></span>
                                </td>

                                <td>
                                    <input name="old_TEXT_<?=$answer['ID']?>" value="<?=$answer['TEXT']?>" type="text">
                                </td>

                                <td>
                                    <input type="text" name="old_SORT_<?=$answer['ID']?>" value="<?=$answer['SORT']?>" size="4">
                                </td>

                                <td id="COLB<?=$i?>" style="background:<?=$answer["COLOR"]?>;">
                                    <input id="old_COLOR_<?=$i?>" name="old_COLOR_<?=$i?>" onchange="col_set(this.value, this)" onclick="ColorPicker(this);" <?
                                    ?>type="text" value="<?=htmlspecialcharsbx($answer["COLOR"])?>" size="7">
                                </td>

                                <td>
                                    <input type="checkbox" name="old_ACTIVE_<?=$answer['ID']?>" value="Y"<?=($answer['ACTIVE'] == "Y" ? "  checked=\"checked\"" : '')?>>
                                </td>

                                <td>
                                    <input type="checkbox" name="old_DEL_<?=$answer['ID']?>" value="Y">
                                </td>
                            </tr>
                        <? endforeach; ?>
                    <? endif; ?>
                    <? for ($i = 1; $i <= 3; $i++):

                        $maxSort += 100;

                        ?>
                        <tr>
                            <td></td>
                            <td>
                                <input name="new_TEXT_field_<?=$i?>" value="" type="text">
                            </td>
                            <td>
                                <input type="text" name="new_SORT_field_<?=$i?>" value="<?=$maxSort?>" size="4">
                            </td>

                            <td id="COLB<?=$i?>">
                                <input id="old_COLOR_0<?=$i?>" name="old_COLOR_0<?=$i?>" onchange="col_set(this.value, this)" onclick="ColorPicker(this);" <?
                                ?>type="text" value="" size="7">
                            </td>


                            <td>
                                <input type="checkbox" name="new_ACTIVE_field_<?=$i?>" value="Y" checked="checked">
                            </td>
                            <td>
                            </td>
                        </tr>
                    <? endfor; ?>

                    </tbody>
                </table>
            </td>
        </tr>

        <? $tabControl->Buttons(
            array(
                "disabled" => ($POST_RIGHT < "W"),
                "back_url" => "vdgb_tszhvoting.php?lang=" . LANG,

            )
        );
        ?>
        <? echo bitrix_sessid_post(); ?>
        <input type="hidden" name="lang" value="<?=LANG?>">

        <?
        $tabControl->End();
        ?>
    </form>
<?
$tabControl->ShowWarnings("post_form", $message);
?>

<? echo BeginNote(); ?>
    <span class="required">*</span><? echo GetMessage("REQUIRED_FIELDS") ?>
<? echo EndNote(); ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>