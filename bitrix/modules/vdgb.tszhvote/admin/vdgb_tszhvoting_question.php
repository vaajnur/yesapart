<?

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/prolog.php");

IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("vdgb.tszhvote");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
{
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$sTableID = "tbl_question";
$oSort    = new CAdminSorting($sTableID, "ID", "asc");
$lAdmin   = new CAdminList($sTableID, $oSort);

$arFilter = array();

if (isset($_REQUEST['VOTING_ID']) && IntVal($_REQUEST['VOTING_ID']) > 0)
{
    $arFilter  = array("VOTING_ID" => IntVal($_REQUEST['VOTING_ID']));
    $VOTING_ID = IntVal($_REQUEST['VOTING_ID']);
    $ar        = CCitrusPoll::GetByID($VOTING_ID);
    if (!is_array($ar))
    {
        LocalRedirect("/bitrix/admin/vdgb_tszhvoting.php?lang=ru");
    }
}
else
{
    LocalRedirect("/bitrix/admin/vdgb_tszhvoting.php?lang=ru");
}

// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //
// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
    global $FilterArr, $lAdmin;
    foreach ($FilterArr as $value)
    {
        global $$value;
    }

    return true;

    // $lAdmin->AddFilterError('�����_������').

    return count($lAdmin->arFilterErrors) == 0; // ���� ������ ����, ������ false;
}

CheckFilter();

// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //
//�������� ��������
if (isset($_REQUEST['del_element']) && IntVal($_REQUEST['del_element']) > 0)
{
    $ID_DEL = IntVal($_REQUEST['del_element']);
    if (!CCitrusPollQuestion::Delete($ID_DEL))
    {
        $lAdmin->AddGroupError(GetMessage("voting_del_error"), $ID_DEL);
    }
}

//�������� ����� ����������� ����
if (!empty($_REQUEST['action_button']) && !empty($_REQUEST['ID']))
{
    $ID = IntVal($_REQUEST['ID']);
    @set_time_limit(0);
    $DB->StartTransaction();
    if (!CCitrusPollQuestion::Delete($ID))
    {
        $DB->Rollback();
        $lAdmin->AddGroupError(GetMessage("voting_del_error"), $ID);
    }
    else
    {
        $DB->Commit();
        LocalRedirect("vdgb_tszhvoting_question.php?VOTING_ID=".$VOTING_ID);
    }
}

// ���������� ����������������� ���������
if ($lAdmin->EditAction() && $POST_RIGHT == "W")
{

    // ������� �� ������ ���������� ���������
    foreach ($FIELDS as $ID => $arFields)
    {
        if (!$lAdmin->IsUpdated($ID))
        {
            continue;
        }

        // �������� ��������� ������� ��������
        $DB->StartTransaction();
        $ID = IntVal($ID);

        $cData = new CCitrusPollQuestion;

        if ($arData = $cData->GetByID($ID))
        {
            if (!$cData->UpdateEx($ID, $arFields))
            {
                $lAdmin->AddGroupError(GetMessage("question_save_error")." ".$cData->LAST_ERROR,
                                                  $ID);
                $DB->Rollback();
            }
        }
        else
        {
            $lAdmin->AddGroupError(GetMessage("question_save_error").". ".GetMessage("question_no"),
                                                                                     $ID);
            $DB->Rollback();
        }

        $DB->Commit();
    }
}

// ��������� ��������� � ��������� ��������
if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT == "W")
{
    // ���� ������� "��� ���� ���������"
    if ($_REQUEST['action_target'] == 'selected')
    {
        $cData  = new CCitrusPollQuestion;
        $rsData = $cData->GetList(array($by => $order), $arFilter);
        while ($arRes  = $rsData->Fetch())
        {
            $arID[] = $arRes['ID'];
        }
    }

    // ������� �� ������ ���������
    foreach ($arID as $ID)
    {
        if (strlen($ID) <= 0)
        {
            continue;
        }
        $ID = IntVal($ID);

        // ��� ������� �������� �������� ��������� ��������
        switch ($_REQUEST['action'])
        {
            // ��������
            case "delete":
                @set_time_limit(0);
                $DB->StartTransaction();
                if (!CCitrusPollQuestion::Delete($ID))
                {
                    $DB->Rollback();
                    $lAdmin->AddGroupError(GetMessage("voting_del_error"), $ID);
                }
                $DB->Commit();
                break;

            // ���������/�����������
            case "activate":
            case "deactivate":
                $cData    = new CCitrusPollQuestion;
                if ($arFields = $cData->GetByID($ID))
                {
                    $arFields["ACTIVE"] = ($_REQUEST['action'] == "activate" ? "Y" : "N");
                    $arData["ACTIVE"]   = $arFields["ACTIVE"];

                    $arFields["IS_MULTIPLE"] = ($_REQUEST['action'] == "activate" ? "Y" : "N");
                    $arData["IS_MULTIPLE"]   = $arFields["IS_MULTIPLE"];

                    if (!$cData->UpdateEx($ID, $arData))
                    {
                        $lAdmin->AddGroupError(GetMessage("voting_save_error").$cData->LAST_ERROR,
                                                          $ID);
                    }
                }
                else
                {
                    $lAdmin->AddGroupError(GetMessage("voting_save_error")." ".GetMessage("voting_no"),
                                                                                          $ID);
                }

                break;
        }
    }
}

// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //
// ������� ������ �����
$cData  = new CCitrusPollQuestion;
$rsData = $cData->GetList(array($by => $order), $arFilter);

// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("page_nav")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

$lAdmin->AddHeaders(array(
    array(
        "id"      => "ID",
        "content" => "ID",
        "sort"    => "id",
        "align"   => "right",
        "default" => true,
    ),
    array(
        "id"      => "ACTIVE",
        "content" => GetMessage("active"),
        "sort"    => "ACTIVE",
        "align"   => "centr",
        "default" => true,
    ),
    array(
        "id"      => "VOTING_ID",
        "content" => GetMessage("voting_id"),
        "sort"    => "VOTING_ID",
        "align"   => "centr",
        "default" => true,
    ),
    array(
        "id"      => "TEXT",
        "content" => GetMessage("text_question"),
        "sort"    => "TEXT",
        "align"   => "centr",
        "default" => true,
    ),
    array(
        "id"      => "SORT",
        "content" => GetMessage("sort"),
        "align"   => "centr",
        "sort"    => "SORT",
        "default" => false,
    ),
    array(
        "id"      => "IS_MULTIPLE",
        "content" => GetMessage("is_multiplate"),
        "align"   => "centr",
        "sort"    => "IS_MULTIPLE",
        "default" => false,
    ),
));

while ($arRes = $rsData->NavNext(true, "f_")):

    // ������� ������. ��������� - ��������� ������ CAdminListRow
    $row = & $lAdmin->AddRow($f_ID, $arRes);
//print_r($row);
    // ����� �������� ����������� �������� ��� ��������� � �������������� ������
    // �������� NAME ����� ��������������� ��� �����, � ������������ �������
    $row->AddViewField("ID",
                       '<a href="vdgb_tszhvoting_question_edit.php?VOTING_ID='.$f_VOTING_ID.'&ID='.$f_ID.'&lang='.LANG.'">'.$f_ID.'</a>');

    // �������� VOTING_ID ����� ��������������� ��� �����, � ������������ �������
    $row->AddViewField("VOTING_ID",
                       '<a href="vdgb_tszhvoting.php?find_id='.$f_VOTING_ID.'&lang='.LANG.'">'.$f_VOTING_ID.'</a>');

    // ����� ACTIVE  ����� ��������������� ����������
    $row->AddCheckField("ACTIVE");

    // ����� IS_MULTIPLE  ����� ��������������� ����������
    $row->AddCheckField("IS_MULTIPLE");

    //$row->AddViewField("COUNT_QUESTION",'<a href="vdgb_tszhvoting_list.php?GROUP_ID='.$f_ID.'&lang='.LANG.'">' . $f_COUNT_QUESTION . '</a><span> [</span><a href="vdgb_tszhvoting_list_edit.php?GROUP_ID='.$f_ID.'&lang='.LANG.'">+</a><span>]</span>');
    // ���������� ����������� ����
    $arActions = Array();

    // �������������� ��������
    $arActions[] = array(
        "ICON"    => "edit",
        "DEFAULT" => true,
        "TEXT"    => GetMessage("question_edit"),
        "ACTION"  => $lAdmin->ActionRedirect("vdgb_tszhvoting_question_edit.php?VOTING_ID=".$VOTING_ID."&ID=".$f_ID."")
    );

    // ������� �����������
    $arActions[] = array("SEPARATOR" => true);

    // �������� ��������
    if ($POST_RIGHT >= "W")
    {
        $arActions[] = array(
            "ICON" => "delete",
            "TEXT" => GetMessage("question_del"),
            "ACTION" => "if(confirm('".GetMessage('question_del_conf')."')) ".$lAdmin->ActionRedirect("vdgb_tszhvoting_question.php?VOTING_ID=".$VOTING_ID."&ID=".$f_ID."&action_button=delete")
                // ��� � ����������� ����  
                //"if(confirm('" . GetMessage('question_del_conf') . "')) " .$lAdmin->ActionDoGroup($f_ID, "delete")
        );        
    }

    // ���� ��������� ������� - �����������, �������� �����.
    if (is_set($arActions[count($arActions) - 1], "SEPARATOR"))
    {
        unset($arActions[count($arActions) - 1]);
    }

    // �������� ����������� ���� � ������
    $row->AddActions($arActions);

endwhile;

// ������ �������
$lAdmin->AddFooter(
        array(
            array("title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()), // ���-�� ���������
            array("counter" => true, "title"   => GetMessage("MAIN_ADMIN_LIST_CHECKED"),
                "value"   => "0"), // ������� ��������� ���������
        )
);

// ��������� ��������
$lAdmin->AddGroupActionTable(Array(
    "delete"     => GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
    "activate"   => GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // ������������ ��������� ��������
    "deactivate" => GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // �������������� ��������� ��������
));

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //
// ���������� ���� �� ������ ������ - ���������� ������
$aContext = array(
    array(
        "TEXT"  => GetMessage("QUESTION_ADD"),
        "LINK"  => "vdgb_tszhvoting_question_edit.php?VOTING_ID=".$VOTING_ID."&lang=".LANG,
        "TITLE" => GetMessage("QUESTION_ADD_TITLE"),
        "ICON"  => "btn_new",
    ),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //
// �������������� �����
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("QESTION_LIST_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// ������� ������� ������ ���������
$lAdmin->DisplayList();
?>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>