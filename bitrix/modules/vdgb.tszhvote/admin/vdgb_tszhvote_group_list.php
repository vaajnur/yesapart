<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/vdgb.tszhvote/include.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/vdgb.tszhvote/prolog.php");

IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("vdgb.tszhvote");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$sTableID = "tbl_citrus_vote";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
    global $FilterArr, $lAdmin;
    foreach ($FilterArr as $value) {
        global $$value;
    }

    return true;

    // � ������ ������ ��������� ������.
    // � ����� ������ ����� ��������� �������� ���������� $find_���
    // � � ������ �������������� ������ ���������� �� �����������
    // ����������� $lAdmin->AddFilterError('�����_������').

    return count($lAdmin->arFilterErrors) == 0; // ���� ������ ����, ������ false;
}

// *********************** /CheckFilter ******************************* //

// ������ �������� �������
$FilterArr = Array(
    "find",
    "find_type",
    "find_name",
    "find_id",
    "find_id_holder",
);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

if (isset($_GET['find_id'])) {
    $find_id = IntVal($_GET['find_id']);
    $arFilter["ID"] = IntVal($_GET['find_id']);
}

// ���� ��� �������� ������� ���������, ���������� ���
if (CheckFilter()) {
    // �������� ������ ���������� ��� ������� �� ������ �������� �������
    $arFilter = array();

    if ($find_name != "") {
        $arFilter["NAME"] = $find_name;
    }

    if (IntVal($find_id_holder) > 0) {
        $arFilter["ID_HOLDER"] = IntVal($find_id_holder);
    }

    if (strlen($find_lid) > 0) {
        $arFilter["LID"] = $find_lid;
    }

    if ($find != "" && $find_type == "id") {
        if (IntVal($find) > 0) {
            $arFilter["ID"] = IntVal($find);
        } else {
            if (IntVal($find_id) > 0) {
                $arFilter["ID"] = IntVal($find_id);
            }
        }
    } elseif (strlen($find) > 0 && $find_type == "find_lid") {
        $arFilter["LID"] = $find;
        if (IntVal($find_id) > 0) {
            $arFilter["ID"] = IntVal($find_id);
        }
    } else {
        if (IntVal($find_id) > 0) {
            $arFilter["ID"] = IntVal($find_id);
        }
    }
} else {
    $arFilter = array();
}

// ******************************************************************** //
//                ��������� �������� ��������                           //
// ******************************************************************** //

if (isset($_REQUEST["edit_form_del"]) && IntVal($_REQUEST["edit_form_del"]) == 1) {
    if (isset($_REQUEST["ID"]) && IntVal($_REQUEST["ID"]) > 0) {
        @set_time_limit(0);
        $DB->StartTransaction();

        $group = CCitrusPollGroup::GetByID(IntVal($_REQUEST["ID"]));
        if ($group) {
            $rsVoting = CCitrusPoll::GetList(array(), array("GROUP_ID" => IntVal($_REQUEST["ID"])), array("ID"));

            $arGroup = true;
            $isOK = true;

            if (is_object($rsVoting)) {
                while ($isOK && $arGroup) {
                    $arGroup = $rsVoting->Fetch();
                    if (isset($arGroup["ID"]) && !CCitrusPoll::UpdateEx(IntVal($arGroup['ID']), array("GROUP_ID" => 0))) {
                        $isOK = false;
                        $DB->Rollback();
                        $lAdmin->AddGroupError(GetMessage("qroup_del_err"), IntVal($_REQUEST["ID"]));
                    }
                }
            }

            if ($isOK) {
                if (!CCitrusPollGroup::Delete(IntVal($_REQUEST["ID"]))) {
                    $DB->Rollback();
                    $lAdmin->AddGroupError(GetMessage("qroup_del_err"), IntVal($_REQUEST["ID"]));
                } else {
                    $messageOK = GetMessage("qroup_del_ok");
                }
            } else {
                $lAdmin->AddGroupError(GetMessage("group_del_error"), $ID);
            }
        } else {
            $lAdmin->AddGroupError(GetMessage("group_del_error") . " " . GetMessage("group_no"), $ID);
        }

        $DB->Commit();
    }
}


// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //

// ���������� ����������������� ���������
if ($lAdmin->EditAction() && $POST_RIGHT == "W") {

    // ������� �� ������ ���������� ���������
    foreach ($FIELDS as $ID => $arFields) {
        if (!$lAdmin->IsUpdated($ID)) {
            continue;
        }

        // �������� ��������� ������� ��������
        $DB->StartTransaction();
        $ID = IntVal($ID);

        $cData = new CCitrusPollGroup;

        if (($rsData = $cData->GetByID($ID)) && ($arData = $rsData->Fetch())) {
            if (!$cData->UpdateEx($ID, $arFields)) {
                $lAdmin->AddGroupError(GetMessage("group_save_error") . " " . $cData->LAST_ERROR, $ID);
                $DB->Rollback();
            }
        } else {
            $lAdmin->AddGroupError(GetMessage("group_save_error") . " " . GetMessage("group_no"), $ID);
            $DB->Rollback();
        }

        $DB->Commit();
    }
}

// ��������� ��������� � ��������� ��������
if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT == "W") {
    // ���� ������� "��� ���� ���������"
    if ($_REQUEST['action_target'] == 'selected') {
        $cData = new CCitrusPollGroup;
        $rsData = $cData->GetList(array($by => $order), $arFilter);
        while ($arRes = $rsData->Fetch()) {
            $arID[] = $arRes['ID'];
        }
    }

    // ������� �� ������ ���������
    foreach ($arID as $ID) {
        if (strlen($ID) <= 0) {
            continue;
        }

        $ID = IntVal($ID);

        // ��� ������� �������� �������� ��������� ��������
        switch ($_REQUEST['action']) {
            // ��������
            case "delete":
                @set_time_limit(0);
                $DB->StartTransaction();

                $group = CCitrusPollGroup::GetByID($ID);
                if ($group) {
                    if (!CCitrusPollGroup::Delete(IntVal($ID))) {
                        $DB->Rollback();
                        $lAdmin->AddGroupError(GetMessage("group_del_error"), $ID);
                    } else {
                        $messageOK = GetMessage("qroup_del_ok");
                    }
                } else {
                    $lAdmin->AddGroupError(GetMessage("group_del_error") . " " . GetMessage("group_no"), $ID);
                }

                $DB->Commit();

                break;

            // ���������/�����������
            case "activate":
            case "deactivate":
                $cData = new CCitrusPollGroup;
                if ($arFields = $cData->GetByID($ID)) {
                    $arFields["ACTIVE"] = ($_REQUEST['action'] == "activate" ? "Y" : "N");
                    $arData["ACTIVE"] = $arFields["ACTIVE"];
                    if (!$cData->UpdateEx($ID, $arData)) {
                        $lAdmin->AddGroupError(GetMessage("group_save_error") . $cData->LAST_ERROR, $ID);
                    }
                } else {
                    $lAdmin->AddGroupError(GetMessage("group_save_error") . " " . GetMessage("group_no"), $ID);
                }
                break;
        }
    }
}

// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //

// ������� ������ �����
$cData = new CCitrusPollGroup;
$rsData = $cData->GetList(array($by => $order), $arFilter);

// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("page_nav")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

$lAdmin->AddHeaders(array(
    array(
        "id" => "ID",
        "content" => "ID",
        "sort" => "id",
        "align" => "right",
        "default" => true,
    ),
    array(
        "id" => "NAME",
        "content" => GetMessage("group_name"),
        "sort" => "name",
        "default" => true,
    ),
    array(
        "id" => "LID",
        "content" => GetMessage("site_site"),
        "sort" => "lid",
        "default" => true,
    ),
    array(
        "id" => "SORT",
        "content" => GetMessage("group_sort"),
        "sort" => "sort",
        "align" => "right",
        "default" => false,
    ),
    array(
        "id" => "ACTIVE",
        "content" => GetMessage("group_act"),
        "sort" => "act",
        "default" => false,
    ),
    array(
        "id" => "TIMESTAMP_X",
        "content" => GetMessage("group_timestamp_x"),
        "sort" => "timestamp_x",
        "default" => true,
    ),
    array(
        "id" => "CODE",
        "content" => GetMessage("group_code"),
        "sort" => "code",
        "default" => false,
    ),
    array(
        "id" => "COUNT_TOPIC",
        "content" => GetMessage("group_count_topic"),
        "sort" => "count_topic",
        "default" => true,
    ),

));

while ($arRes = $rsData->NavNext(true, "f_")):

    $f_COUNT_TOPIC = CCitrusPoll::GetList(array(), array("GROUP_ID" => $arRes['ID']), array());

    // ������� ������. ��������� - ��������� ������ CAdminListRow
    $row =& $lAdmin->AddRow($f_ID, $arRes);
    // ����� �������� ����������� �������� ��� ��������� � �������������� ������
//print_r($row);
    // �������� NAME ����� ��������������� ��� �����, � ������������ �������
    $row->AddViewField("NAME", '<a href="vdgb_tszhvote_group_edit.php?ID=' . $f_ID . '&lang=' . LANG . '">' . $f_NAME . '</a>');
    $row->AddInputField("NAME", array("size" => 20));

    // �������� SORT ����� ��������������� �������
    $row->AddInputField("SORT", array("size" => 20));
    $row->AddViewField("SORT", '<sapn>' . $f_SORT . '</sapn>');

    // ����� ACTIVE  ����� ��������������� ����������
    $row->AddCheckField("ACTIVE");

    $row->AddViewField("COUNT_TOPIC", '<a href="vdgb_tszhvoting.php?find_id_group=' . $f_ID . '&lang=' . LANG . '">' . $f_COUNT_TOPIC . '</a><span> [</span><a href="vdgb_tszhvoting_edit.php?id_group=' . $f_ID . '&lang=' . LANG . '">+</a><span>]</span>');

    // ���������� ����������� ����
    $arActions = Array();

    // �������������� ��������
    $arActions[] = array(
        "ICON" => "edit",
        "DEFAULT" => true,
        "TEXT" => GetMessage("group_edit"),
        "ACTION" => $lAdmin->ActionRedirect("vdgb_tszhvote_group_edit.php?ID=" . $f_ID)
    );

    // �������� ��������
    if ($POST_RIGHT >= "W") {
        $arActions[] = array(
            "ICON" => "delete",
            "TEXT" => GetMessage("group_del"),
            "ACTION" => "if(confirm('" . GetMessage('group_del_conf') . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete")
        );
    }

    // ������� �����������
    $arActions[] = array("SEPARATOR" => true);

    // ���� ��������� ������� - �����������, �������� �����.
    if (is_set($arActions[count($arActions) - 1], "SEPARATOR")) {
        unset($arActions[count($arActions) - 1]);
    }

    // �������� ����������� ���� � ������
    $row->AddActions($arActions);

endwhile;

// ������ �������
$lAdmin->AddFooter(
    array(
        array("title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()), // ���-�� ���������
        array("counter" => true, "title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"), // ������� ��������� ���������
    )
);

// ��������� ��������
$lAdmin->AddGroupActionTable(Array(
    "delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
    "activate" => GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // ������������ ��������� ��������
    "deactivate" => GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // �������������� ��������� ��������
));

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //

// ���������� ���� �� ������ ������ - ���������� ������
$aContext = array(
    array(
        "TEXT" => GetMessage("GROUP_ADD"),
        "LINK" => "vdgb_tszhvote_group_edit.php?lang=" . LANG,
        "TITLE" => GetMessage("GROUP_ADD_TITLE"),
        "ICON" => "btn_new",
    ),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //

if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT == "W" && isset($messageOK) && $_REQUEST['action'] == "delete") {
    $message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
    echo $message->Show();
}

// �������������� �����
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("VOTE_GROUP_LIST_TITLE"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

if (isset($message) && isset($_REQUEST["edit_form_del"])) {
    $message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
    echo $message->Show();
}

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

$oFilter = new CAdminFilter(
    $sTableID . "_filter",
    array(
        GetMessage("NAME"),
        GetMessage("ID"),
        GetMessage("ID_HOLDER"),
    )
);

?>

    <form name="form_filter" method="get" action="<? echo $APPLICATION->GetCurPage(); ?>">
        <? $oFilter->Begin(); ?>
        <tr>
            <td><b><?=GetMessage("group_find")?>:</b></td>
            <td>
                <input type="text" size="25" name="find" value="<? echo htmlspecialcharsbx($find) ?>" title="<?=GetMessage("group_find_title")?>">
                <?
                $arr = array(
                    "reference" => array(
                        GetMessage("find_lid"),
                        "ID",
                    ),
                    "reference_id" => array(
                        "find_lid",
                        "id",
                    )
                );
                echo SelectBoxFromArray("find_type", $arr, $find_type, "", "");
                ?>
            </td>
        </tr>


        <tr>
            <td><?=GetMessage("find_name")?></td>
            <td><input type="text" name="find_name" size="47" value="<? echo htmlspecialcharsbx($find_name) ?>"></td>

        </tr>


        <tr>
            <td><?=GetMessage("find_id")?></td>
            <td><input type="text" name="find_id" size="5" value="<? echo htmlspecialcharsbx($find_id) ?>"></td>
        </tr>
        <tr>
            <td><?=GetMessage("find_id_holder")?></td>
            <td><input type="text" name="find_id_holder" size="5" value="<? echo htmlspecialcharsbx($find_id_holder) ?>"></td>

        </tr>

        <?
        $oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "find_form"));
        $oFilter->End();
        ?>
    </form>

<?
// ������� ������� ������ ���������
$lAdmin->DisplayList();

//require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin_js.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
