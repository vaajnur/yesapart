<?
IncludeModuleLangFile(__FILE__);

if($APPLICATION->GetGroupRight("vdgb.tszhvote")!="D") {
    $aMenu = array(
        "parent_menu" => "global_menu_services",
        "section" => "vdgb.tszhvote",
        "sort" => 50,
        "text" => GetMessage("mnu_vdgb_vote"),
        "title" => GetMessage("mnu_vdgb_vote_title"),
        "url" => "vdgb_tszhvote_index.php?lang=".LANGUAGE_ID,
		"icon" => "vdgb_tszhvote_menu_icon",
		"page_icon" => "vdgb_tszhvote_page_icon",
        "items_id" => "menu_vdgb_tszhvote",
        "items" => array(
            array(
                    "text" => GetMessage("mnu_vdgb_vote_group"),
                    "url" => "vdgb_tszhvote_group_list.php?lang=".LANGUAGE_ID,
                    "more_url" => Array("vdgb_tszhvote_group_list.php" , "vdgb_tszhvote_group_edit.php"),
                    "title" => GetMessage("mnu_vdgb_vote_group_alt"),
            ),
            array(
                    "text" => GetMessage("mnu_vdgb_vote_list"),
                    "url" => "vdgb_tszhvoting.php?lang=".LANGUAGE_ID,
                    "more_url" => Array("vdgb_tszhvote_result.php","vdgb_tszhvoting.php","vdgb_tszhvoting_edit.php","vdgb_tszhvoting_question.php","vdgb_tszhvoting_question_edit.php","vdgb_tszhvote_form.php"),
                    "title" => GetMessage("mnu_vdgb_vote_list_alt"),
            ),
            array(
                    "text" => GetMessage("mnu_vdgb_vote_voting"),
                    "url" => "vdgb_tszhvote_event.php?lang=".LANGUAGE_ID,
                    "more_url" => array("vdgb_tszhvote_event.php"),
                    "title" => GetMessage("mnu_vdgb_vote_voting_alt"),
            ),
            
        ),
    );
    
    return $aMenu;
    
}

return false;
?>
