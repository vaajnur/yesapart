<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/prolog.php");

IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("vdgb.tszhvote");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if(isset($_REQUEST['VOTING_ID']) && IntVal($_REQUEST['VOTING_ID']) > 0) {
    $VOTING_ID = IntVal($_REQUEST['VOTING_ID']);
    $arVote = CCitrusPoll::GetByID($VOTING_ID);
    if ($arVote) {
        $GROUP_ID = $arVote['GROUP_ID'];
    }
}

$APPLICATION->SetTitle(GetMessage("VOTE_RESULT_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$sTableID = "tbl_citrus_vote_form";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);


// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //

// ���������� ���� �� ������ ������ - ���������� ������
$aContext = array(
  array(
    "TEXT"=>GetMessage("VOTING_EVENT_LIST"),
    "LINK"=>"vdgb_tszhvote_event.php?lang=".LANG,
    "TITLE"=>GetMessage("VOTING_EVENT_LIST_TITLE"),
    "ICON"=>"btn_list",
  ),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

$lAdmin->DisplayList();

if(isset($VOTING_ID) && $VOTING_ID > 0) {
$APPLICATION->IncludeComponent(
	"citrus:tszh.voting.result",
	".default",
	Array(
		"VOTE_RESULT_TEMPLATE" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"GROUP_ID" => $GROUP_ID,
		"VOTING_ID" => array("0"=>$VOTING_ID)
	)
);


}
else {
    CAdminMessage::ShowMessage(GetMessage("voting_not_found"));
}

?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>