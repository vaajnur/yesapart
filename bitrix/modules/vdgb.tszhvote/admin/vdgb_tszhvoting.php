<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vdgb.tszhvote/prolog.php");
//
//$classMethod = new $arClasses["users"]["CLASS"];
//$classMethod::GetVoteSize();

IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("vdgb.tszhvote");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$sTableID = "tbl_voting";
$oSort = new CAdminSorting($sTableID, "ID", "ASC");
$lAdmin = new CAdminList($sTableID, $oSort);

// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //
// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
    global $FilterArr, $lAdmin;
    foreach ($FilterArr as $value)
    {
        global $$value;
    }

    return true;

    // $lAdmin->AddFilterError('�����_������').

    return count($lAdmin->arFilterErrors) == 0; // ���� ������ ����, ������ false;
}

// ������ �������� �������
$FilterArr = Array(
    "find",
    "find_type",
    "find_id",
    "find_name",
    "find_date_voting",
    "find_active",
    "find_id_group",
    "find_voting_comp",
);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

if (isset($_GET['find_id_group']))
{
    $find_id_group = IntVal($_GET['find_id_group']);
    $arFilter["GROUP_ID"] = IntVal($_GET['find_id_group']);
}

if (isset($_GET['find_id']))
{
    $find_id = IntVal($_GET['find_id']);
    $arFilter["ID"] = IntVal($_GET['find_id']);
}

// ���� ��� �������� ������� ���������, ���������� ���
if (CheckFilter())
{

    $arFilter = array();

    if (IntVal($find) > 0 && $find_type == 'id')
    {
        $arFilter["ID"] = IntVal($find);
        if (IntVal($find_id_group) > 0)
        {
            $arFilter["GROUP_ID"] = IntVal($find_id_group);
        }
    }

    if (IntVal($find) > 0 && $find_type == 'id_group')
    {
        $arFilter["GROUP_ID"] = IntVal($find);
        if (IntVal($find_id) > 0)
        {
            $arFilter["ID"] = IntVal($find_id);
        }
    }

    if (isset($find_id))
    {
        $arFilter["ID"] = IntVal($find_id);
    }

    if (isset($find_id_group))
    {
        $arFilter["GROUP_ID"] = IntVal($find_id_group);
    }

    if (strlen($find_name) > 0)
    {
        $arFilter["NAME"] = trim(htmlspecialcharsbx($find_name));
    }

    if (strlen($find_active) > 0)
    {
        if ($find_active == "Y")
        {
            $arFilter["ACTIVE"] = "Y";
        }
        else
        {
            $arFilter["ACTIVE"] = "N";
        }
    }


    if (strlen($find_voting_comp) > 0)
    {
        if ($find_voting_comp == "Y")
        {
            $arFilter["VOTING_COMPLETED"] = "Y";
        }
        else
        {
            $arFilter["VOTING_COMPLETED"] = "N";
        }
    }

    if (strlen($find_begin_date) > 0 && strlen($find_begin_end) > 0)
    {
        $arFilter[">DATE_BEGIN"] = trim(htmlspecialcharsbx($find_begin_date));
        $arFilter["<DATE_END"] = trim(htmlspecialcharsbx($find_begin_end));
    }
    elseif (strlen($find_begin_date) > 0 && strlen($find_begin_end) <= 0)
    {
        $arFilter[">DATE_BEGIN"] = trim(htmlspecialcharsbx($find_begin_date));
    }
    elseif (strlen($find_begin_date) <= 0 && strlen($find_begin_end) > 0)
    {
        $arFilter["<DATE_END"] = trim(htmlspecialcharsbx($find_begin_end));
    }
}
else
{
    $arFilter = array();
}

// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //
//�������� ��������
if (isset($_REQUEST['del_element']) && IntVal($_REQUEST['del_element']) > 0)
{
    $ID_DEL = IntVal($_REQUEST['del_element']);
    if (!CCitrusPoll::Delete($ID_DEL))
    {
        $lAdmin->AddGroupError(GetMessage("voting_del_error"), $ID_DEL);
    }

    CCitrusPollQuestion::DeleteByVoting($ID_DEL);
    CCitrusPollVote::Reset($ID_DEL);
}

//���������� ������
if (isset($_REQUEST['RESET']) && IntVal($_REQUEST['RESET']) > 0)
{

    //������� �����
    $RES_ID = IntVal($_REQUEST['RESET']);

    /*
     *  ������� ������ �� ������ �����������
     */
    //������� ��� ������� ����������� ��� ������� ������
    $rsEventVote = CCitrusPollVote::GetList(array($by => $order),
                    array('VOTING_ID' => $RES_ID));
    //�������� �� �������� ����������� ��� ������� ������
    while ($arEventVote = $rsEventVote->GetNext(false))
    //������� ������ �� �������
        CCitrusPollAnswer::DeleteByVotingEventId($arAnswer['ID']);
    /*
     * �����
     */

    $cData = new CCitrusPoll;
    $resEventData = new CCitrusPollVote;


    if (($arData = $cData->GetByID($RES_ID)) && is_array($arData) && !empty($arData))
    {

        if (!$resEventData->Reset($RES_ID))
        {
            $lAdmin->AddGroupError(GetMessage("voting_reset_error")." ".$cData->LAST_ERROR,
                    $ID);
        }
        else
        {
            $error = GetMessage("voting_reset_ok");
        }
    }
    else
    {
        $lAdmin->AddGroupError(GetMessage("voting_reset_error").". ".GetMessage("voting_no"),
                $ID);
    }
}

// ���������� ����������������� ���������
if ($lAdmin->EditAction() && $POST_RIGHT == "W")
{

    // ������� �� ������ ���������� ���������
    foreach ($FIELDS as $ID => $arFields)
    {
        if (!$lAdmin->IsUpdated($ID))
            continue;

        // �������� ��������� ������� ��������
        $ID = IntVal($ID);

        $cData = new CCitrusPoll;

        if (($arData = $cData->GetByID($RES_ID)) && is_array($arData) && !empty($arData))
        {

            if (!$cData->UpdateEx($ID, $arFields))
            {
                $lAdmin->AddGroupError(GetMessage("voting_save_error")." ".$cData->LAST_ERROR,
                        $ID);
            }
        }
        else
        {
            $lAdmin->AddGroupError(GetMessage("voting_save_error")." ".GetMessage("voting_no"),
                    $ID);
        }
    }
}

// ��������� ��������� � ��������� ��������
if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT == "W")
{
    // ���� ������� "��� ���� ���������"
    if ($_REQUEST['action_target'] == 'selected')
    {
        $cData = new CCitrusPoll;
        $rsData = $cData->GetList(array($by => $order), $arFilter);
        while ($arRes = $rsData->Fetch())
            $arID[] = $arRes['ID'];
    }

    $arOpenWindows = Array();
    if ($_REQUEST['action'] == 'print')
    {
        $arPrintTemplates = CCitrusPollReport::GetTemplates();
        $printTemplate = preg_replace('/[^\d\w_.]+/i', '', $_REQUEST['template']);
        $printTemplate = array_key_exists($printTemplate, $arPrintTemplates) ? $printTemplate : 'default';
    }

    // ������� �� ������ ���������
    foreach ($arID as $ID)
    {
        if (strlen($ID) <= 0)
            continue;
        $ID = IntVal($ID);

        // ��� ������� �������� �������� ��������� ��������
        switch ($_REQUEST['action'])
        {
            // ������
            case 'print':
                $arOpenWindows[] = '/bitrix/admin/vdgb_tszhvote_report.php?lang='.LANG.'&id='.rawurlencode($ID).'&tpl='.rawurldecode($printTemplate);
                break;

            // ��������
            case "delete":
                if (!CCitrusPoll::Delete($ID))
                {
                    $lAdmin->AddGroupError(GetMessage("voting_del_error"), $ID);
                }
                break;

            // ���������/�����������
            case "activate":
            case "deactivate":
                if ($arFields = CCitrusPoll::GetByID($ID))
                {
                    $arUpdate = Array(
                        "ACTIVE" => $_REQUEST['action'] == "activate" ? "Y" : "N",
                    );
                    if (!CCitrusPoll::Update($ID, $arUpdate))
                    {
                        if ($ex = $APPLICATION->GetException())
                            $lAdmin->AddGroupError(GetMessage("voting_save_error").': '.$ex->GetString(),
                                    $ID);
                        else
                            $lAdmin->AddGroupError(GetMessage("voting_save_error"),
                                    $ID);
                    }
                }
                else
                    $lAdmin->AddGroupError(GetMessage("voting_save_error")." ".GetMessage("voting_no"),
                            $ID);
                break;
        }
    }
    if (count($arOpenWindows) > 0)
    {
        ?>
        <script type="text/javascript">
            var w = (opener ? opener.window : parent.window);
            w.CloseWaitWindow();
        <?
        foreach ($arOpenWindows as $url)
        {
            ?>
                w.open('<?= $url ?>');
            <?
        }
        ?>
        </script>
        <?
    }
}

// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //
// ������� ������ �����
$rsData = CCitrusPoll::GetList(array($by => $order), $arFilter);

// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("page_nav")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

$lAdmin->AddHeaders(array(
    array(
        "id"      => "ID",
        "content" => "ID",
        "sort"    => "id",
        "align"   => "right",
        "default" => true,
    ),
    array(
        "id"      => "NAME",
        "content" => GetMessage("voting_name"),
        "sort"    => "NAME",
        "default" => true,
    ),
    array(
        "id"      => "GROUP_ID",
        "content" => GetMessage("id_group"),
        "sort"    => "GROUP_ID",
        "align"   => "centr",
        "default" => true,
    ),
    array(
        "id"      => "ACTIVE",
        "content" => GetMessage("voting_act"),
        "sort"    => "ACTIVE",
        "default" => false,
    ),
    array(
        "id"      => "TIMESTAMP_X",
        "content" => GetMessage("voting_timestamp_x"),
        "sort"    => "timestamp_x",
        "default" => false,
    ),
    array(
        "id"      => "DATE_BEGIN",
        "content" => GetMessage("voting_date_begin"),
        "sort"    => "date_begin",
        "default" => true,
    ),
    array(
        "id"      => "DATE_BEGIN",
        "content" => GetMessage("voting_date_begin"),
        "sort"    => "date_begin",
        "default" => true,
    ),
    array(
        "id"      => "DATE_END",
        "content" => GetMessage("voting_date_end"),
        "sort"    => "date_end",
        "default" => true,
    ),
    array(
        "id"      => "TITLE_TEXT",
        "content" => GetMessage("TITLE_TEXT"),
        "sort"    => "TITLE_TEXT",
        "default" => true,
    ),
    array(
        "id"      => "DETAIL_TEXT",
        "content" => GetMessage("DETAIL_TEXT"),
        "sort"    => "DETAIL_TEXT",
        "default" => false,
    ),
    array(
        "id"      => "COUNT_QUESTION",
        "content" => GetMessage("count_question"),
        "sort"    => "count_question",
        "default" => true,
    ),
    array(
        "id"      => "COUTN_VOTING",
        "content" => GetMessage("count_voting"),
        "sort"    => "count_voting",
        "default" => true,
    ),
    array(
        "id"      => "VOTING_COMPLETED",
        "content" => GetMessage("voting_completed"),
        "sort"    => "voting_completed",
        "default" => true,
    ),
));

while ($arRes = $rsData->NavNext(true, "f_"))
{
    // ������� ������. ��������� - ��������� ������ CAdminListRow
    $row = & $lAdmin->AddRow($f_ID, $arRes);

    //���������� �������� � ������ ������
    $question_count = CCitrusPollQuestion::GetList(array(),
                    array("VOTING_ID" => $f_ID), array());
    // ���������� �������
    $voting_count = CCitrusPollVote::GetList(array(),
                    array("VOTING_ID" => $f_ID), array());
    // ����� �������� ����������� �������� ��� ��������� � �������������� ������
    // �������� NAME ����� ��������������� ��� �����, � ������������ �������
    $row->AddViewField("NAME",
            '<a href="vdgb_tszhvoting_edit.php?ID='.$f_ID.'&lang='.LANG.'">'.$f_NAME.'</a>');
    $row->AddInputField("NAME", array("size" => 20));

    $row->AddViewField("COUTN_VOTING", '<span>'.$voting_count.'</span>');

    $row->AddViewField("GROUP_ID",
            '<a href="vdgb_tszhvote_group_list.php?find_id='.$f_GROUP_ID.'&lang='.LANG.'">'.$f_GROUP_ID.'</a>');

    // ����� VOTING_COMPLETED  ����� ��������������� ����������
    $row->AddCheckField("VOTING_COMPLETED");

    // ����� ACTIVE  ����� ��������������� ����������
    $row->AddCheckField("ACTIVE");

    $row->AddViewField("COUNT_QUESTION",
            '<a href="vdgb_tszhvoting_question.php?VOTING_ID='.$f_ID.'&lang='.LANG.'">'.$question_count.'</a><span> [</span><a href="vdgb_tszhvoting_question_edit.php?VOTING_ID='.$f_ID.'&lang='.LANG.'">+</a><span>]</span>');

    // ���������� ����������� ����
    $arActions = Array();

    // �������������� ��������
    $arActions[] = array(
        "ICON"    => "edit",
        "DEFAULT" => true,
        "TEXT"    => GetMessage("voting_edit"),
        "ACTION"  => $lAdmin->ActionRedirect("vdgb_tszhvoting_edit.php?ID=".$f_ID)
    );

    // ���������� ��������
    $arActions[] = array(
        "ICON"   => "copy",
        // "DEFAULT"=>true,
        "TEXT"   => GetMessage("voting_copy"),
        "ACTION" => $lAdmin->ActionRedirect("vdgb_tszhvoting_edit.php?COPY=".$f_ID)
    );

    // �������� ��������
    $arActions[] = array(
        "ICON"   => "reset",
        //"DEFAULT"=>true,
        "TEXT"   => GetMessage("voting_reset"),
        "ACTION" => $lAdmin->ActionRedirect("vdgb_tszhvoting.php?RESET=".$f_ID)
    );

    // ����� ���������� �� ������
    $arActions[] = array(
        "ICON"   => "result",
        //"DEFAULT"=>true,
        "TEXT"   => GetMessage("voting_result"),
        "ACTION" => $lAdmin->ActionRedirect("vdgb_tszhvote_result.php?VOTING_ID=".$f_ID.'&GROUP_ID='.$f_GROUP_ID)
    );

    // ������� �����������
    $arActions[] = array("SEPARATOR" => true);

    // �������� ��������
    if ($POST_RIGHT >= "W")
        $arActions[] = array(
            "ICON"   => "delete",
            "TEXT"   => GetMessage("voting_del"),
            "ACTION" => "if(confirm('".GetMessage('voting_del_conf')."')) ".$lAdmin->ActionDoGroup($f_ID,
                    "delete")
        );

    // ���� ��������� ������� - �����������, �������� �����.
    if (is_set($arActions[count($arActions) - 1], "SEPARATOR"))
        unset($arActions[count($arActions) - 1]);

    // �������� ����������� ���� � ������
    $row->AddActions($arActions);
}

// ������ �������
$lAdmin->AddFooter(
        array(
            array("title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()), // ���-�� ���������
            array("counter" => true, "title"   => GetMessage("MAIN_ADMIN_LIST_CHECKED"),
                "value"   => "0"), // ������� ��������� ���������
        )
);

// ��������� ��������
$arGroupActions = Array(
    "delete"     => GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
    "activate"   => GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // ������������ ��������� ��������
    "deactivate" => GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // �������������� ��������� ��������
);

$arGroupActionsParams = Array();

$arPrintTemplates = CCitrusPollReport::GetTemplates();

if (count($arPrintTemplates) > 0)
{
    $printTemplateSelectHTML = '&nbsp;&nbsp;<select name="template" id="print_template_select" style="display: none;">';
    foreach ($arPrintTemplates as $code => $arTemplate)
    {
        $code = htmlspecialcharsbx($code);
        $printTemplateSelectHTML .= "<option value=\"{$code}\">{$arTemplate['NAME']}</option>";
    }
    $printTemplateSelectHTML .= "</select>\n";

    $arGroupActions["print"] = GetMessage("REPORT_PRINT");
    $arGroupActions["print_html"] = Array(
        'type'  => 'html',
        'value' => $printTemplateSelectHTML,
    );

    $arGroupActionsParams["select_onchange"] = "document.getElementById('print_template_select').style.display = (this[this.selectedIndex].value == 'print' ? 'inline' : 'none')";
}

$lAdmin->AddGroupActionTable($arGroupActions, $arGroupActionsParams);

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //
// ���������� ���� �� ������ ������ - ���������� ������
$aContext = array(
    array(
        "TEXT"  => GetMessage("VOTING_ADD"),
        "LINK"  => "vdgb_tszhvoting_edit.php?lang=".LANG,
        "TITLE" => GetMessage("VOTING_ADD_TITLE"),
        "ICON"  => "btn_new",
    ),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //
// �������������� �����
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("VOTE_LIST_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

$oFilter = new CAdminFilter(
        $sTableID."_filter",
        array(
    GetMessage("ID"),
    GetMessage("NAME"),
    GetMessage("DATE"),
    GetMessage("ACTIVE"),
    GetMEssage("ID_GROUP"),
    GetMessage("VOTING_COMPLETED")
        )
);
?>

<form name="form_filter" method="get" action="<? echo $APPLICATION->GetCurPage(); ?>">
    <? $oFilter->Begin(); ?>

    <tr>
        <td><b><?= GetMessage("voting_find") ?>:</b></td>
        <td>
            <input type="text" size="40" name="find" value="<? echo htmlspecialcharsbx($find) ?>" title="<?= GetMessage("voting_find_title") ?>">
            <?
            $arr = array("reference"    => array("", GetMessage("find_id_group"),
                    "ID",), "reference_id" => array(
                    "", "id_group", "id",));
            echo SelectBoxFromArray("find_type", $arr, $find_type, "", "");
            ?>
        </td>
    </tr>

    <tr>
        <td><?= GetMessage("find_id") ?>:</td>
        <td><input type="text" name="find_id" size="5" value="<? echo htmlspecialcharsbx($find_id) ?>"></td>
    </tr>

    <tr>
        <td><?= GetMessage("find_name") ?>:</td>
        <td><input type="text" name="find_name" size="25" value="<? echo htmlspecialcharsbx($find_name) ?>"></td>
    </tr>

    <tr>
        <td><?= GetMessage("find_date_voting") ?>:</td>
        <td>
            <span style="margin-left: 12px;"><?= GetMessage("find_begin_date") ?></span>
            <input type="text" name="find_begin_date" size="9" value="<? echo htmlspecialcharsbx($find_begin_date) ?>">
            <?= Calendar("find_begin_date", "form_filter") ?>
            <span><?= GetMessage("find_begin_end") ?></span>
            <input type="text" name="find_begin_end" size="9" value="<? echo htmlspecialcharsbx($find_begin_end) ?>">
            <?= Calendar("find_begin_end", "form_filter") ?>
        </td>
    </tr>

    <tr>
        <td><?= GetMessage("find_active") ?>:</td>
        <td>
            <?
            $arValueActive = array("reference"    => array("", GetMessage("find_voting_active_Y"),
                    GetMessage("find_voting_active_N"),), "reference_id" => array(
                    "", "Y", "N",));
            ?>
            <?
            echo SelectBoxFromArray("find_active", $arValueActive, $find_active,
                    "", "");
            ?>
        <!--   <input type="text" name="find_active" size="25" value="<? //echo htmlspecialcharsbx($find_active)     ?>"></td>-->

    </tr>

    <tr>
        <td><?= GetMessage("find_id_group") ?>:</td>
        <td><input type="text" name="find_id_group" size="5" value="<? echo htmlspecialcharsbx($find_id_group) ?>"></td>
    </tr>

    <tr>
        <td><?= GetMessage("find_voting_comp") ?>:</td>
        <td>
            <?
            $arValue = array("reference"    => array("", GetMessage("find_voting_comp_Y"),
                    GetMessage("find_voting_comp_N"),),
                "reference_id" => array("", "Y", "N",));
            ?>
            <?
            echo SelectBoxFromArray("find_voting_comp", $arValue,
                    $find_voting_comp, "", "");
            ?>
        </td>
    </tr>

    <?
    $oFilter->Buttons(array("table_id" => $sTableID, "url"      => $APPLICATION->GetCurPage(),
        "form"     => "find_form"));
    $oFilter->End();
    ?>
</form>

<?
if (isset($error))
{
    echo CAdminMessage::ShowNote($error);
}

// ������� ������� ������ ���������
$lAdmin->DisplayList();
?>

<?
require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>
