<?$MESS ['CVL_VOTING_GROUP_NOT_FOUND'] = "Не указана группа голосований. Проверьте настройки компонента.";
$MESS ['СVL_VIEW'] = "Просмотр";
$MESS ['СVL_DELETE'] = "Удалить";$MESS ['СVL_CONFIRM_DELETE'] = "Вы действительно хотите удалить этот результат голосования?";
$MESS ['CVL_ALL'] = "(все)";
$MESS ['CVL_YES'] = "Да";$MESS ['CVL_NO'] = "Нет";$MESS ['TUL_ACCESS_DENIED'] = "Доступ запрещен. Вы не являетесь администратором ТСЖ";$MESS ['TVL_PORTAL_MODULE_NOT_INSTALLED'] = "Модуль «Портал» не установлен.";$MESS ['TVL_VOTE_MODULE_NOT_INSTALLED'] = "Модуль «Опросы/голосования» не установлен.";
$MESS ['TVL_ORG_NOT_FOUND'] = "Организация не найдена.";$MESS ['TVL_VOTING_GROUP_NOT_FOUND'] = "Группа опросов не найдена.";
$MESS ['TVL_VOTING_NOT_SPECIFIED'] = "Не указан опрос.";$MESS ['TVL_VOTING_NOT_FOUND'] = "Опрос не найден.";

$MESS ['TVL_EDIT_ELEMENT'] = "Редактировать";$MESS ['TVL_EDIT_ELEMENT_TITLE'] = "Редактировать опрос";$MESS ['TVL_RESULTS_ELEMENT'] = "Результаты";$MESS ['TVL_RESULTS_ELEMENT_TITLE'] = "Результаты опроса";$MESS ['TVL_QUESTIONS_ELEMENT'] = "Вопросы";$MESS ['TVL_QUESTIONS_ELEMENT_TITLE'] = "Список вопросов голосования";$MESS ['CVL_F_ID'] = "ID";
$MESS ['CVL_F_VALID'] = "Валидность";$MESS ['CVL_F_ENTITY_ID'] = "Кто голосовал";$MESS ['CVL_F_WEIGHT'] = "Вес голоса";
$MESS ['CVL_F_IP'] = "IP-адрес";$MESS ['CVL_F_TIMESTAMP_X'] = "Время";
$MESS ['CVL_F_VOTING_ID'] = "Голосование";
$MESS ['CVL_F_QUESTION_ID'] = "Вопрос";$MESS ['TVL_ADD_VOTING'] = "Новое голосование";$MESS ['TVL_VOTINGS_LIST'] = "Список голосований";
$MESS ['TVL_TITLE'] = "Результаты голосований";$MESS ['TVL_TITLE_NUM'] = "Результаты голосования №#ID#";
$MESS ['TVL_TB_SETTINGS'] = "Настройки";$MESS ['TVL_TB_SETTINGS_TITLE'] = "Настройки списка";$MESS ['TVL_TB_VIEWS'] = "Представление";
$MESS ['TVL_TB_VIEWS_TITLE'] = "Представление списка";?>