<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule("vdgb.tszhvote"))
	return;

$arGroupList = Array();
$rsGroups = CCitrusPollGroup::GetList(Array("SORT" => "ASC"), Array("ACTIVE" => "Y"), false, false, Array("ID", "NAME"));
while ($arGroup = $rsGroups->GetNext())
	$arGroupList[$arGroup['ID']] = $arGroup['NAME'];

$arLimitFieldNames = Array(
	"-" => GetMessage("CVE_F_NO_FIELD"),
	"ID" => GetMessage("CVE_F_ID"),
	"CREATED_BY" => GetMessage("CVE_F_CREATED_BY"),
);

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"ID" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CVE_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => '={$_GET["ID"]}',
		),
		"GROUP_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CVE_GROUP_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arGroupList,
			"DEFAULT" => '',
			"ADDITIONAL_VALUES" => "Y",
		),
		"ALLOW_NEW_ELEMENT" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CVE_ALLOW_NEW_ELEMENT"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => 'Y',
		), 
	),
);
?>
