<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (array_key_exists("MESSAGE", $arResult))
	ShowMessage($arResult["MESSAGE"]);

if (is_array($arResult["TOOLBAR_BUTTONS"]) && count($arResult["TOOLBAR_BUTTONS"]) > 0)
{
	?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.interface.toolbar",
		"",
		Array(
			"BUTTONS" => $arResult["TOOLBAR_BUTTONS"], 
			"TOOLBAR_ID" => $arResult["TOOLBAR_ID"],
		),
		false
	);?>
	<?
}
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.interface.grid",
	"",
	Array(
		"GRID_ID" => $arResult["GRID_ID"],
		"HEADERS" => $arResult["COLUMNS"],
		"ROWS" => $arResult["DATA"],
		"FOOTER" => Array(Array('title' => '�����', 'value' => $arResult["TOTAL_CNT"])),
		"EDITABLE" => 'N',
		"NAV_OBJECT" => $arResult["NAV_OBJECT"],
		"DEFAULT_SORT" => Array(
			'by' => 'id',
			'order' => 'desc',
		),
		"SORT" => Array(
			'by' => $arResult["SORT"]['by'],
			'order' => $arResult["SORT"]['order'],
			"by_varname" => 'sort',
			"order_varname" => 'order',
		),
//		"ACTION_ALL_ROWS" => "Y",
		"ACTIONS" => Array(
			'delete' => Array(
				'TEXT' => '�������',
				'TITLE' => '������� �������',
				"ONCLICK" => 'alert("�� �����������");',
				'ICONCLASS' => 'delete',
			)
		),
        'FILTER' => $arResult["FILTER"],
	),
	false
);?>