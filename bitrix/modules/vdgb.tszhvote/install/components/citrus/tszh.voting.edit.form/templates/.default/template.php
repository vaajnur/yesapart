<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (array_key_exists("MESSAGE", $arResult))
	ShowMessage($arResult["MESSAGE"]);


if (is_array($arResult["TOOLBAR_BUTTONS"]) && count($arResult["TOOLBAR_BUTTONS"]) > 0)
{
	?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.interface.toolbar",
		"",
		Array(
			"BUTTONS" => $arResult["TOOLBAR_BUTTONS"], 
			"TOOLBAR_ID" => $arResult["TOOLBAR_ID"],
		),
		false
	);?>
	<?
}

if (!empty($arResult["FORM_FIELDS"])):

?><?$APPLICATION->IncludeComponent(
	"bitrix:main.interface.form",
	"",
	Array(
		"FORM_ID" => 'vote_element_edit_' . $arResult["ID"],
		"TABS" => $arResult["TABS"],
		"DATA" => $arResult["ITEM"],
		"BUTTONS" => Array(
			'standard_buttons' => true,
			'back_url' => $_REQUEST['backurl'],
//			'custom_html' => $htmlButtons,
		),
	),
	false
);?><?

endif;

?>