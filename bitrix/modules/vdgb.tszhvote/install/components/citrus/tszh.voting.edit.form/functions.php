<?

//$this->IncludeComponentLang($relativePath, $lang = False); 

if (!function_exists('htmlspecialchars_array')):
	function htmlspecialchars_array(&$var)
	{
		if (is_array($var))
			array_walk($var, 'htmlspecialchars_array');
		else
			$var = htmlspecialcharsbx($var);
		return $var;
	}
endif;

if (!function_exists('CitrusVoteElementEditField')):
	function CitrusVoteElementEditField($arResult, $code, $arFields, $value = false)
	{
		if ($value === false)
		{
			$value = in_array($code, Array("DESCRIPTION")) ? $arFields['~' . $code] : $arFields[$code];
		}
		
		$arRequiredFields = Array(
			'DATE_BEGIN',
			'DATE_END',
			'NAME',
			"VOTING_METHOD",
			"ENTITY_TYPE",
		);
		switch ($code)
		{
			case 'ID':
			case 'TIMESTAMP_X':
				return Array(
					'id' => $code,
					'name' => GetMessage("CVE_F_" . $code),
					'required' => in_array($code, $arRequiredFields),
					'type' => 'custom',
					'value' => $value,
				);
				break;
			
			case 'VOTING_METHOD':
				$arMethods = CCitrusPollMethodBase::GetList();
				$arMethodList = Array();
				foreach ($arMethods as $key => $arMethod)
					$arMethodList[$key] = $arMethod["NAME"];
				return Array(
					'id' => $code,
					'name' => GetMessage("CVE_F_" . $code),
					'required' => in_array($code, $arRequiredFields),
					'type' => 'list',
					'items' => $arMethodList,
					'value' => $value,
				);
				break;

			case 'ENTITY_TYPE':
				$arMethods = CCitrusPollEntityBase::GetList();
				$arMethodList = Array();
				foreach ($arMethods as $key => $arMethod)
					$arMethodList[$key] = $arMethod["NAME"];
				return Array(
					'id' => $code,
					'name' => GetMessage("CVE_F_" . $code),
					'required' => in_array($code, $arRequiredFields),
					'type' => 'list',
					'items' => $arMethodList,
					'value' => $value,
				);
				break;
				
			case 'ACTIVE':
			case 'VOTING_COMPLETED':
				return Array(
					'id' => $code,
					'name' => GetMessage("CVE_F_" . $code),
					'required' => in_array($code, $arRequiredFields),
					'type' => 'checkbox',
					'value' => $value,
				);
				break;
			
			case 'DATE_BEGIN':
			case 'DATE_END':
				return Array(
					'id' => $code,
					'name' => GetMessage("CVE_F_" . $code),
					'required' => in_array($code, $arRequiredFields),
					'type' => 'date',
					'value' => $value,
				);
				break;

			case 'DETAIL_TEXT':
				if (CModule::IncludeModule("fileman"))
				{
					ob_start();
					$LHE = new CLightHTMLEditor;
					$LHE->Show(array(
						'id' => preg_replace("/[^a-z0-9]/i", '', $code),
						'width' => '100%',
						'height' => '300px',
						'inputName' => $code,
						'content' => htmlspecialchars_decode($value),
						'bUseFileDialogs' => false,
						'bFloatingToolbar' => false,
						'bArisingToolbar' => false,
						'toolbarConfig' => array(
							'Bold', 'Italic', 'Underline', 'RemoveFormat',
							'CreateLink', 'DeleteLink', 'Image',// 'Video',
							//'BackColor', 'ForeColor',
							'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
							'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
							'StyleList', 'HeaderList',
							//'FontList', 'FontSizeList',
						),
					));
					$html = ob_get_contents();
					ob_end_clean();
					return Array(
						'id' => $code,
						'name' => GetMessage("CVE_F_" . $code),
						'required' => $arIBlock["FIELDS"][$code]["IS_REQUIRED"] == "Y",
						'type' => 'custom',
						'value' => $html,
					);
				}
				else
				{
					return Array(
						'id' => $code,
						'name' => GetMessage("CVE_F_" . $code),
						'required' => $arIBlock["FIELDS"][$code]["IS_REQUIRED"] == "Y",
						'type' => 'textarea',
						'value' => $value,
						'params' => Array(
							'cols' => 50,
							'rows' => 10,
						),
					);
				}
				break;


			case 'SORT':
				return Array(
					'id' => $code,
					'name' => GetMessage("CVE_F_" . $code),
					'required' => in_array($code, $arRequiredFields),
					'type' => 'text',
					'value' => $value,
					'params' => Array(
						'size' => 10,
					),
				);
				break;
				
			default:
				return Array(
					'id' => $code,
					'name' => GetMessage("CVE_F_" . $code),
					'required' => in_array($code, $arRequiredFields),
					'type' => 'text',
					'value' => $value,
					'params' => Array(
						'size' => 50,
					),
				);
		}
	}
endif;


if (!function_exists('CitrusPollEditFormRenderQuestion')):
	
	function CitrusPollEditFormRenderQuestion($formQuestionID, $arQuestion, $arParams)
	{

		$html = '
			<input type="hidden" name="Q[' . $formQuestionID . '][ID]" value="' . $arQuestion['ID'] .'" />
			<table style="width: 100%;">
			<tr>
				<th>ID</th>
				<th>' . GetMessage("TVE_F_MESSAGE") . '</th>
				<th style="width: 50px;">' . GetMessage("TVE_F_DEL") . '</th>
			</tr>
		';
	
		$maxSort = 0;
		if (array_key_exists('ANSWERS', $arQuestion))
		{
			foreach ($arQuestion['ANSWERS'] as $arAnswer)
				$maxSort = $arAnswer["SORT"] > $maxSort ? $arAnswer["SORT"] : $maxSort;
	
			foreach ($arQuestion['ANSWERS'] as $answerID => $arAnswer)
			{
				$answerFieldName = "Q[$formQuestionID][ANSWERS][$answerID]";
				$del = $arAnswer['DEL'] ? 'checked="checked" ' : '';
				$html .= <<<HTML
					<tr>
						<td>{$arAnswer['ID']}<input type="hidden" name="{$answerFieldName}[ID]" value="{$arAnswer['ID']}" /></td>
						<td><input type="text" value="{$arAnswer['TEXT']}" name="{$answerFieldName}[TEXT]" style="width: 90%;" />
						<input type="hidden" value="{$arAnswer['SORT']}" name="{$answerFieldName}[SORT]" />
						</td>
						<td style="text-align: center;"><input type="checkbox" value="Y" name="{$answerFieldName}[DEL]" $del/></td>
					</tr>
HTML;
			}
		}

		for ($i = 0; $i < 5; $i++)
		{
			$answerFieldName = "Q[$formQuestionID][ANSWERS][n$i]";
			$sort = $maxSort + ($i+1)*100;
			$typeSelect = SelectBoxFromArray(
				"ANSWER[n{$i}][FIELD_TYPE]",
				Array(
					"REFERENCE" => Array(
						GetMessage("TVE_FIELD_TYPE_0"),
						GetMessage("TVE_FIELD_TYPE_1"),
					),
					"REFERENCE_ID" => Array(
						0,
						1,
					),
				),
				0,
				0,
				'style="width: 100%;"'
			); 
			$html .= <<<HTML
				<tr>
					<td>&nbsp;</td>
					<td><input type="text" value="" name="{$answerFieldName}[TEXT]" style="width: 90%;" />
						<input type="hidden" value="{$sort}" name="{$answerFieldName}[SORT]" />
					</td>
					<td>&nbsp;</td>
HTML;
		}
		$html .= "</table>";
		
		$arFormFields = Array();
		$arFormFields[] = Array(
			'id' => "QUESTIONS_TITLE_" . $formQuestionID,
			'name' => GetMessage("TVE_FIELD_QUESTIONS_TITLE", Array("#N#" => $arParams['QUESTION_IDX'])),
			'type' => 'section',
		);
		if (array_key_exists('ID', $arQuestion))
		{
			$arFormFields[] = Array(
				'id' => "Q[{$formQuestionID}][DEL]",
				'name' => GetMessage("CVE_F_DEL_QUESTION"),
				'type' => 'checkbox',
				'value' => $arQuestion['DEL'],
			);
		}
		$arFormFields[] = Array(
			'id' => "Q[{$formQuestionID}][TEXT]",
			'name' => GetMessage("CVE_F_QUESTION_TEXT"),
			'type' => 'textarea',
			'value' => $arQuestion['TEXT'],
			'params' => Array(
				'cols' => 50,
				'rows' => 3,
			),
		);
		$arFormFields[] = Array(
			'id' => "Q[{$formQuestionID}][IS_MULTIPLE]",
			'name' => GetMessage("CVE_F_ANSWERS_LABEL"),
			'type' => 'list',
			'items' => Array(
				'N' => GetMessage("TVE_FIELD_TYPE_0"),
				'Y' => GetMessage("TVE_FIELD_TYPE_1"),
			),
			'value' => $arQuestion['IS_MULTIPLE']
			
		);
		$arFormFields[] = Array(
			'id' => "Q[$formQuestionID]",
			'name' => GetMessage("CVE_F_ANSWERS"),
			'required' => false,
			'type' => 'custom',
			'value' => $html,
			'colspan' => true,
		);
		return $arFormFields;
	}
endif;

?>