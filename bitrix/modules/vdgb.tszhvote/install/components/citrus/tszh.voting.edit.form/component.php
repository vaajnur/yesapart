<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arParams["ALLOW_NEW_ELEMENT"] = $arParams["ALLOW_NEW_ELEMENT"] != "N" ? true : false;

$arParams["ID"] = intval($arParams["~ID"]);
if($arParams["ID"] > 0 && $arParams["ID"]."" != $arParams["~ID"])
{
	ShowError(GetMessage("CVE_ELEMENT_NOT_FOUND"));
	@define("ERROR_404", "Y");
	CHTTP::SetStatus("404 Not Found");
	return;
}

$arParams["GROUP_ID"] = IntVal($arParams['GROUP_ID']) > 0 ? IntVal($arParams['GROUP_ID']) : 0;


$strBackURLParam = "&backurl=" . rawurlencode($APPLICATION->GetCurPageParam());

$arParams["LIST_URL"] = trim($arParams["LIST_URL"]);
if (strlen($arParams["LIST_URL"]) <= 0)
	$arParams["LIST_URL"] = "index.php";

$arParams["QUESTIONS_URL"] = trim($arParams["QUESTIONS_URL"]);
if (strlen($arParams["QUESTIONS_URL"]) <= 0)
	$arParams["QUESTIONS_URL"] = "questions.php?ID=#ID#";
$arParams["QUESTIONS_URL"] .= $strBackURLParam;

$arParams["EDIT_URL"] = trim($arParams["EDIT_URL"]);
if (strlen($arParams["EDIT_URL"]) <= 0)
	$arParams["EDIT_URL"] = "form.php?ID=#ID#";
$arParams["EDIT_URL"] .= $strBackURLParam;

$arParams["RESULTS_URL"] = trim($arParams["RESULTS_URL"]);
if (strlen($arParams["RESULTS_URL"]) <= 0)
	$arParams["RESULTS_URL"] = "results.php?ID=#ID#";
$arParams["RESULTS_URL"] .= $strBackURLParam;


require('functions.php');


$arParams["FIELD_CODE"] = Array(
	"ID",
	"ACTIVE",
	"DATE_BEGIN",
	"DATE_END",
	"VOTING_COMPLETED",
	"NAME",
	"TITLE_TEXT",
	"DETAIL_TEXT",
	"PERCENT_NEEDED",
	"VOTING_METHOD",
	"ENTITY_TYPE",
);

if(!CModule::IncludeModule("vdgb.tszhvote"))
{
	ShowError(GetMessage("CVE_VOTE_MODULE_NOT_INSTALLED"));
	return;
}

$arResult["GROUP"] = CCitrusPollGroup::GetByID($arParams['GROUP_ID']);
if (!is_array($arResult["GROUP"]))
{
	ShowError(GetMessage("CVE_VOTE_CHANNEL_NOT_FOUND"));
	return;
}


$arErrors = Array();


$arFilter = Array(
	"GROUP_ID" => $arResult["GROUP"]["ID"],
);

if (IntVal($arParams["ID"]) > 0)
{
	$arFilter["ID"] = $arParams["ID"];
}
elseif (!$arParams["ALLOW_NEW_ELEMENT"])
{
	$this->AbortResultCache();
	ShowError(GetMessage("CVE_ELEMENT_NOT_FOUND"));
	@define("ERROR_404", "Y");
	CHTTP::SetStatus("404 Not Found");
	return;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && check_bitrix_sessid() && ($_REQUEST['save'] || $_REQUEST['apply'] || $_REQUEST['add_question'])) {

	$arElement = false;
	if (IntVal($arParams["ID"]) > 0)
	{
		$rsElement = CCitrusPoll::GetList(Array(), $arFilter, false, Array('nTopCount' => 1));	
		$arElement = $rsElement->GetNext();
	}
	
	if (is_array($arElement) || $arParams["ALLOW_NEW_ELEMENT"])
	{
		$arUpdateValues = Array(
			"GROUP_ID" => $arResult["GROUP"]['ID'],
		);
		
		foreach ($arParams["FIELD_CODE"] as $code)
		{
			$arUpdateValues[$code] = $_REQUEST[$code];
		}

		$arUpdateValues["DETAIL_TEXT"] = htmlspecialchars_decode($arUpdateValues["DETAIL_TEXT"]);
		
		if (!$_REQUEST['add_question'])
		{
			if (is_array($arElement))
				$bSuccess = CCitrusPoll::Update($arElement['ID'], $arUpdateValues);
			else
			{
				$arElement["ID"] = CCitrusPoll::Add($arUpdateValues);
				$bSuccess = $arElement["ID"] > 0; 
			}
		}
		
		if ($bSuccess)
		{
			// добавление / обновление вопросов
			// ================================
			$arExistingQuestions = Array();
			$rsQuestions = CCitrusPollQuestion::GetList(Array(), Array("VOTING_ID" => $arElement['ID']), false, false, Array("ID"));
			while ($arQuestion = $rsQuestions->Fetch())
				$arExistingQuestions[] = $arQuestion['ID'];
			
			$arQuestions = $_POST['Q'];
			$idx = 0;
			foreach ($arQuestions as $id => $arQuestion)
			{
				$idx += 10;
				$QUESTION_ID = in_array($id, $arExistingQuestions) ? $id : 0;

				if ($arQuestion['DEL'] == "Y" && $QUESTION_ID > 0)
				{
					CCitrusPollQuestion::Delete($QUESTION_ID);
					continue;
				}

				$arQuestionFields = Array(
					"ACTIVE" => "Y",
					"VOTING_ID" => $arElement['ID'],
					"SORT" => $idx,
					"TEXT" => $arQuestion["TEXT"],
					"IS_MULTIPLE" => $arQuestion["IS_MULTIPLE"] == "Y" ? "Y" : "N",
				);
				
				if ($QUESTION_ID > 0)
					CCitrusPollQuestion::Update($QUESTION_ID, $arQuestionFields);
				else
					$QUESTION_ID = CCitrusPollQuestion::Add($arQuestionFields);
				
				$bSuccess = $QUESTION_ID > 0;
				
				if ($bSuccess)
				{
					// добавление / обновление ответов
					// ===============================
					$arExistingAnswers = Array();
					$rsAnswers = CCitrusPollAnswer::GetList(Array(), Array("QUESTION_ID" => $QUESTION_ID), false, false, Array("ID"));
					while ($arAnswer = $rsAnswers->Fetch())
						$arExistingAnswers[] = $arAnswer['ID'];
					
					foreach ($arQuestion['ANSWERS'] as $answerID => $arAnswer)
					{
						$ANSWER_ID = in_array($answerID, $arExistingAnswers) ? $answerID : 0;
						$arAnswerFields = Array(
							"ACTIVE" => "Y",
							"QUESTION_ID" => $QUESTION_ID,
							"SORT" => $arAnswer["SORT"],
							"TEXT" => $arAnswer["TEXT"],
						);
						if ($ANSWER_ID > 0)
							CCitrusPollAnswer::Update($ANSWER_ID, $arAnswerFields);
						else {
								
							// пропустить незаполненные ответы
							if (strlen(trim($arAnswer['TEXT'])) <= 0)
								continue;
							
							$ANSWER_ID = CCitrusPollAnswer::Add($arAnswerFields);
						}
						
						$bSuccess = $bSuccess && $ANSWER_ID > 0;
					}
				}
			}
		}
		
			
		if ($bSuccess) {
	
			$_SESSION['citrus.tszh.voting.edit.message'] = GetMessage("CVE_SAVED_MESSAGE");
			if ($_REQUEST['save'] && strlen($_REQUEST['backurl']) > 0) {
				LocalRedirect($_REQUEST['backurl']);
			} else {
				LocalRedirect($APPLICATION->GetCurPageParam('ID=' . $arElement["ID"] . '&' . (strlen($_REQUEST['backurl']) > 0 ? 'backurl=' . rawurlencode($_REQUEST['backurl']) : ''), Array('ID', 'backurl')));
			}
		} else {

			if (!$_REQUEST['add_question'])
			{
				if($ex = $APPLICATION->GetException())
					$strError = $ex->GetString();
				else
					$strError = GetMessage("CVE_ERROR_SAVING");
	
				$arErrors[] = $strError;
			}
			$arResult['bVarsFromForm'] = true;
			$arResult["FIELDS_FROM_FORM"] = htmlspecialchars_array($arUpdateValues);
		}
	}
} elseif ($_SERVER['REQUEST_METHOD'] == 'POST' && check_bitrix_sessid() && ($_REQUEST['cancel'])) {
	if (strlen($_REQUEST['backurl']) > 0) {
		LocalRedirect($_REQUEST['backurl']);
	} else {
		LocalRedirect($APPLICATION->GetCurPageParam((strlen($_REQUEST['backurl']) > 0 ? 'backurl=' . rawurlencode($_REQUEST['backurl']) : ''), Array('backurl')));
	}
}


if (IntVal($arParams["ID"]) > 0 || $arParams["ALLOW_NEW_ELEMENT"])
{

	if (IntVal($arParams["ID"]) > 0)
	{
		$rsElement = CCitrusPoll::GetList(Array(), $arFilter, false, Array('nTopCount' => 1));	
		$arItem = false;
		if ($arItem = $rsElement->GetNext())
		{
			$arItem["FIELDS"] = array();
			foreach($arParams["FIELD_CODE"] as $code)
				if(array_key_exists($code, $arItem))
					$arItem["FIELDS"][$code] = $arItem[$code];
	
			$arResult["FORM_FIELDS"] = Array();
			foreach ($arParams["FIELD_CODE"] as $code)
			{
				$arResult["FORM_FIELDS"][] = CitrusVoteElementEditField($arResult, $code, $arItem, $arResult['bVarsFromForm'] ? $arResult["FIELDS_FROM_FORM"][$code] : false);
			}
		}
		
	} else {
		// values for new elements
		$arItem = Array(
			"ACTIVE" => "Y",
			"DATE_BEGIN" => ConvertTimeStamp(),
			"DATE_END" => ConvertTimeStamp(strtotime('+1 month')),
		);

		$arItem["FIELDS"] = array();
		foreach($arParams["FIELD_CODE"] as $code)
			if(array_key_exists($code, $arItem))
				$arItem["FIELDS"][$code] = $arItem[$code];

		$arResult["FORM_FIELDS"] = Array();
	
		foreach ($arParams["FIELD_CODE"] as $code)
		{
			if (in_array($code, Array("ID", "TIMESTAMP_X")))
				continue;
			$arResult["FORM_FIELDS"][] = CitrusVoteElementEditField($arResult, $code, $arItem, $arResult['bVarsFromForm'] ? $arResult["FIELDS_FROM_FORM"][$code] : false);
		}
	}
	
	
	if (is_array($arItem))
	{

		$arParams["QUESTION_IDX"] = 0;
		if ($arResult['bVarsFromForm'])
		{
			foreach ($_POST["Q"] as $questionID => $arQuestion)
			{
				$arParams['QUESTION_IDX']++;
				$arQuestion = htmlspecialchars_array($arQuestion);
				$arQuestionFields = CitrusPollEditFormRenderQuestion($questionID, $arQuestion, $arParams);
				if (is_array($arQuestionFields) && count($arQuestionFields) > 0)
					$arResult['FORM_FIELDS'] = array_merge($arResult['FORM_FIELDS'], $arQuestionFields);
			}
		}
		else
		{
			$rsQuestions = CCitrusPollQuestion::GetList(Array("SORT" => "ASC"), Array("VOTING_ID" => $arItem["ID"]), $arQuestionFilter);
			while ($arQuestion = $rsQuestions->GetNext())
			{
				$arParams['QUESTION_IDX']++;

				$rsAnswers = CCitrusPollAnswer::GetList(Array("SORT" => "ASC"), Array("QUESTION_ID" => $arQuestion["ID"]));
				$arQuestion['ANSWERS'] = Array();
				while ($arAnswer = $rsAnswers->GetNext())
					$arQuestion['ANSWERS'][$arAnswer["ID"]] = $arAnswer;
				
				$arQuestionFields = CitrusPollEditFormRenderQuestion($arQuestion['ID'], $arQuestion, $arParams);
				if (is_array($arQuestionFields) && count($arQuestionFields) > 0)
					$arResult['FORM_FIELDS'] = array_merge($arResult['FORM_FIELDS'], $arQuestionFields);
			}
		}
		
		if (IntVal($arParams["ID"]) <= 0 || $_SERVER['REQUEST_METHOD'] == 'POST' && array_key_exists('add_question', $_POST))
		{
			$arParams['QUESTION_IDX']++;
			$arQuestion = Array();
			$arQuestionFields = CitrusPollEditFormRenderQuestion('n' . $i, $arQuestion, $arParams, $arResult['bVarsFromForm']);
			if (is_array($arQuestionFields) && count($arQuestionFields) > 0)
				$arResult['FORM_FIELDS'] = array_merge($arResult['FORM_FIELDS'], $arQuestionFields);
		}
		else
		{
			$arResult['FORM_FIELDS'][] = Array(
				'id' => "ADD_QUESTION",
				'name' => GetMessage("CVE_F_ADD_QUESTION"),
				'required' => false,
				'type' => 'custom',
				'value' => '<input type="submit" name="add_question" value="' . GetMessage("CVE_F_ADD_QUESTION") . '" />',
				'colspan' => true,
			);
		}

		//$arItem["FIELDS_TYPE"] = $arResult["QUESTION"][""]
		$arResult["ITEM"] = $arItem;			
	}
	else
	{
		ShowError(GetMessage("CVE_ELEMENT_NOT_FOUND"));
		@define("ERROR_404", "Y");
		if($arParams["SET_STATUS_404"]==="Y")
			CHTTP::SetStatus("404 Not Found");
	}
}
else
{
	ShowError(GetMessage("CVE_ELEMENT_NOT_FOUND"));
	@define("ERROR_404", "Y");
	if($arParams["SET_STATUS_404"]==="Y")
		CHTTP::SetStatus("404 Not Found");
}

$arResult["TABS"] = Array(
	Array(
		"id" => 'vote_element_edit_' . $arResult["ID"] . '_tab_1',
		'name' => GetMessage("CVE_TAB1_NAME"),
		'fields' => $arResult["FORM_FIELDS"],
	),
);

if (!empty($arErrors))
{
	$arResult["MESSAGE"] = Array(
		"TYPE" => "ERROR",
		"MESSAGE" => implode('<br />', $arErrors),
	);
}
elseif (array_key_exists('citrus.tszh.voting.edit.message', $_SESSION))
{
	$arResult["MESSAGE"] = Array(
		"TYPE" => "OK",
		"MESSAGE" => $_SESSION['citrus.tszh.voting.edit.message'],
	);
	unset($_SESSION['citrus.tszh.voting.edit.message']);
}


$arResult["TOOLBAR_ID"] = 'citrus_polls_toolbar';
$arResult["TOOLBAR_BUTTONS"] = Array(
	array("TEXT" => GetMessage("CVE_VOTING_LIST"), "ICON" => 'btn-list', "LINK" => $arParams['LIST_URL']),
);
if ($arParams["ALLOW_NEW_ELEMENT"])
{
	$arResult["TOOLBAR_BUTTONS"] = array_merge(Array(
		array("TEXT" => GetMessage("CVE_ADD_VOTING"), "ICON" => 'btn-new', "LINK" => CComponentEngine::MakePathFromTemplate($arParams['EDIT_URL'], Array("ID" => "0"))), 
	), $arResult["TOOLBAR_BUTTONS"]);
}

if (is_array($arResult["ITEM"]))
	$this->IncludeComponentTemplate();

?>