<?
$MESS ['VOTE_MODULE_IS_NOT_INSTALLED'] = "Модуль опросов не установлен.";
$MESS ['VOTE_ACCESS_DENIED'] = "У вас нет прав на участие в данном опросе.";
$MESS ['VOTE_EMPTY'] = "Голосование не найдено.";
$MESS ['VOTE_GROUP_EMPTY'] = "Не указанна группа опроса";
$MESS ['VOTING_FAILED'] = "При сохранении голоса произошла ошибка";
$MESS ['VOTING_FAILED_MSG'] = "При сохранении голоса произошла ошибка: #MSG#";

$MESS ['VOTE_OK'] = "Спасибо за участие в опросе.";
$MESS ['VOTE_ALREADY_VOTE'] = "Вы не можете дважды принимать участие в этом опросе.";
$MESS ['VOTE_RED_LAMP'] = "Опрос не активен.";
$MESS ['VOTE_GROUP_EMPTY'] = "Не указан ID группа опросов";
$MESS ['VOTE_EMPTY'] = "Не указан ID опроса.";
$MESS ['USER_VOTE_EMPTY'] = "Вы не выбрали вариант ответа.";
$MESS ['VOTE_BAD_CAPTCHA'] = "Вы неправильно ввели символы на картинке.";
$MESS ['VOTE_NO_CAPTCHA'] = "Вы не ввели символы на картинке.";
$MESS ['VOTE_REQUIRED_MISSING'] = "Вы не ответили на обязательный вопрос.";
$MESS ['ANSWER_ERRROR'] = "Вы ответили не на все вопросы";
$MESS ['OK_MESSAGE'] = "Спасибо за участие в опросе.";
$MESS ['ADD_ERRROR'] = "Произошла непредвиденная ошибка";
$MESS ['VOTE_MODULE_NOT_ACCESS'] = "У вас недостаточно прав для участия в опросах данной группы";
$MESS ['ACCESS_EDIT'] = "Вы не можете голосовать повторно";
?>
