<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule("vdgb.tszhvote"))
	return;

$arGroup = array();
$arVotes = Array();

$rs = CCitrusPollGroup::GetList(array(),array("ACTIVE" => "Y"),array("ID","NAME","TITLE","LID"));

if(is_object($rs)) {
    while($arrGroup = $rs->Fetch()) {
        $arGroup[$arrGroup["ID"]] = "[".$arrGroup["ID"]."] ".$arrGroup["LID"]."-".$arrGroup["NAME"];
    }
}

$rsVoting = CCitrusPoll::GetList(array(),array("ACTIVE" => "Y","VOTING_COMPLETED" => "N", "GROUP_ID" => $arCurrentValues["GROUP_ID"]!="-"?$arCurrentValues["GROUP_ID"]:0));

if(is_object($rsVoting)) {
    while($arrVoting = $rsVoting->Fetch()) {
        $arVotes[$arrVoting["ID"]] = "[".$arrVoting["ID"]."]".$arrVoting["NAME"];

    }
}

$tszhList = array();
if (CModule::IncludeModule("citrus.tszh")) {
    $dbTszh = CTszh::GetList(Array("NAME" => "ASC"));
    $tszhList = Array('' => GetMessage("CV_ALL"));
    while ($tszh = $dbTszh->Fetch()) {
        $tszhList[$tszh['ID']] = $tszh["NAME"];
    }
}

$arComponentParameters = array(
    
        "GROUPS" => array(
            "SETTINGS" => array(
                "NAME" => GetMessage("SETTINGS_SECTION")
            ),
            "PARAMS" => array(
                "NAME" => GetMessage("PARAMS_PHR")
            ),
            "CACHE_SETTINGS" => array(
                "NAME" => GetMessage("CACHE_TITLE")
            ),
        ),
    
	"PARAMETERS" => array(
            
		"GROUP_ID" => array(
			"NAME" => GetMessage("CITRUS_VOTE_GROUP_ID"),
			"TYPE" => "LIST",
			"PARENT" => "SETTINGS",
			"VALUES" => $arGroup,
			"MULTIPLE"=>"N",
            "ADDITIONAL_VALUES"=>"Y",
		),

        "TSZH_ID" => array(
            "NAME" => GetMessage("CITRUS_VOTE_TSZH_ID"),
            "TYPE" => "LIST",
            "PARENT" => "SETTINGS",
            "VALUES" => $tszhList,
            "MULTIPLE"=>"N",
        ),

        "VOTING_ID" => array(
			"NAME" => GetMessage("CITRUS_VOTE_VOTING_ID"),
			"TYPE" => "LIST",
			"PARENT" => "SETTINGS",
			"VALUES" => $arVotes,
			"MULTIPLE"=>"Y",
            "ADDITIONAL_VALUES" => "Y",
		),

		"VOTE_RESULT_TEMPLATE" => array(
			"NAME" => GetMessage("CITRUS_VOTE_VOTE_RESULT_PAGE"),
			"TYPE" => "STRING",
			"PARENT" => "URL_TEMPLATES",
			"COLS" => 45,
			"DEFAULT" => "vote_result.php?GROUP_ID=#GROUP_ID#&VOTING_ID=#VOTING_ID#"
		),

		"CACHE_TIME"  =>  Array("DEFAULT"=>36000000),

	),
);
?>