<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/** @var $this CBitrixComponent */

if (!CModule::IncludeModule("vdgb.tszhvote"))
{
	ShowError(GetMessage("VOTE_MODULE_IS_NOT_INSTALLED"));

	return;
}

$arParams["GROUP_ID"] = IntVal($arParams["GROUP_ID"]);
if ($arParams["GROUP_ID"] <= 0)
{
	ShowError(GetMessage("VOTE_GROUP_EMPTY"));

	return;
}

$arParams["VOTING_ID"] = IntVal($arParams["VOTING_ID"]);
$arParams["VOTING_ID"] = $arParams["VOTING_ID"] > 0 ? $arParams["VOTING_ID"] : 0;

$URL_NAME_DEFAULT = array("vote_result" => "VOTING_ID=#VOTING_ID#");
foreach ($URL_NAME_DEFAULT as $URL => $URL_VALUE)
{
	if (strLen(trim($arParams[strToUpper($URL) . "_TEMPLATE"])) <= 0)
	{
		$arParams[strToUpper($URL) . "_TEMPLATE"] = $APPLICATION->GetCurPage() . "?" . $URL_VALUE;
	}

	$arParams["~" . strToUpper($URL) . "_TEMPLATE"] = $arParams[strToUpper($URL) . "_TEMPLATE"];
	$arParams[strToUpper($URL) . "_TEMPLATE"] = htmlspecialcharsbx($arParams["~" . strToUpper($URL) . "_TEMPLATE"]);
}

$arResult = array();

if ($_SERVER["REQUEST_METHOD"] == 'POST' && check_bitrix_sessid() && $_POST['do_vote'] == 1)
{
	try
	{
		$VOTING_ID = IntVal($_POST['voting_id']);
		if ($VOTING_ID <= 0)
		{
			throw new Exception(GetMessage("VOTE_EMPTY"));
		}

		if (CCitrusPollVote::IsVoted($VOTING_ID))
		{
			throw new Exception(GetMessage("VOTE_ALREADY_VOTE"));
		}

		if ($ex = $APPLICATION->GetException())
		{
			throw new Exception($ex->GetMessage());
		}

		$arAnswers = $_POST['Q'];
		if (!CCitrusPollVote::DoVote($VOTING_ID, $arAnswers))
		{
			if ($ex = $APPLICATION->GetException())
			{
				throw new Exception(GetMessage("VOTING_FAILED_MSG", Array("#MSG#" => $ex->GetString())));
			}
			throw new Exception(GetMessage("VOTING_FAILED"));
		}
	}
	catch (Exception $e)
	{
		$arResult["ERROR_MESSAGE"] = $e->getMessage();
	}

	if (empty($arResult["ERROR_MESSAGE"]))
	{
		LocalRedirect($APPLICATION->GetCurPageParam("VOTE_SUCCESSFULL=Y", Array('VOTE_SUCCESSFULL')));
	}
}

if (isset($_REQUEST['VOTE_SUCCESSFULL']) && $_REQUEST['VOTE_SUCCESSFULL'] == "Y")
{
	$arResult["OK_MESSAGE"] = GetMessage("OK_MESSAGE");
}

$arFilter = Array(
	"ACTIVE" => "Y",
	"<=DATE_BEGIN" => ConvertTimeStamp(time(), "SHORT"),
	">=DATE_END" => ConvertTimeStamp(time(), "SHORT"),
);
if ($arParams["VOTING_ID"])
{
	$arFilter["ID"] = $arParams['VOTING_ID'];
}
if (intval($arParams["GROUP_ID"]) > 0)
{
	$arFilter["GROUP_ID"] = $arParams['GROUP_ID'];
}
if ($arParams["TSZH_ID"] && intval($arParams["TSZH_ID"]) > 0)
{
	$arFilter["TSZH_ID"] = array(
		false,
		intval($arParams["TSZH_ID"])
	);
}

//������� ������ ��������� �������
$rsVoting = CCitrusPoll::GetList(array("RAND" => "ASC"), $arFilter);
while ($arVoting = $rsVoting->Fetch())
{
	$arVoting["IS_VOTED"] = CCitrusPollVote::IsVoted($arVoting['ID']);
	$arVoting["CAN_VOTE"] = CCitrusPollVote::CanVote($arVoting['ID']);

	$arResult["ITEM"][$arVoting["ID"]] = $arVoting;

	if (!$arVoting["CAN_VOTE"])
	{
		continue;
	}

	//������� ������ �������� �� ������ � id = $arVoting["ID"]
	$rsQuestion = CCitrusPollQuestion::GetList(array("SORT" => "ASC"), array("VOTING_ID" => $arVoting["ID"], "ACTIVE" => "Y"));
	while ($arQuestion = $rsQuestion->Fetch())
	{
		$arResult["ITEM"][$arVoting["ID"]]["QUESTION"][$arQuestion["ID"]] = $arQuestion;

		//������� ������ �������, ��� ������� �������
		$rsAnsw = CCitrusPollAnswer::GetList(array("SORT" => "ASC"), array("QUESTION_ID" => $arQuestion["ID"], "ACTIVE" => "Y"));
		while ($arAnsw = $rsAnsw->Fetch())
		{
			$arResult["ITEM"][$arVoting["ID"]]["QUESTION"][$arQuestion["ID"]]["ANSWER"][$arAnsw["ID"]] = $arAnsw;
		}
	}
	break;
}

$this->IncludeComponentTemplate();

