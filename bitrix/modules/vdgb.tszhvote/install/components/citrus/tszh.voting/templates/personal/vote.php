<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
$siteTemplate = COption::GetOptionString('main', 'wizard_template_id', false, SITE_ID);
if ($siteTemplate == 'citrus_tszh_adaptive') {
    $componentTemplate = "orchid_default";
} else {
    $componentTemplate = ".default";
}
?>
<?$APPLICATION->IncludeComponent(
    "citrus:tszh.voting.form",
    $componentTemplate,
    Array(
        "GROUP_ID" => $arParams['GROUP_ID'],
        "TSZH_ID" => $arParams['TSZH_ID'],
        "VOTING_ID" => $arParams['VOTING_ID'],
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000"
    ),
    $component
);?>

