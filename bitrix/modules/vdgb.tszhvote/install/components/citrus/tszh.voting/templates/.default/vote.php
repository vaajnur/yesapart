<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?$APPLICATION->IncludeComponent(
	"citrus:tszh.voting.form",
	"",
	Array(
		"GROUP_ID" => $arParams['GROUP_ID'],
        "TSZH_ID" => $arParams['TSZH_ID'],
		"VOTING_ID" => $arParams['VOTING_ID'],
		"VOTE_RESULT_TEMPLATE" => $arParams["SEF_FOLDER"].$arParams["SEF_URL_TEMPLATES"]["detail"],
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000"
	),
	$component
);?>
