<?
$MESS["MAIN_SETINGS"] = "Настройки формы опроса";
$MESS["GROUP_ID"] = "Идентификатор группы";

$MESS["VOTE_ID_TITLE"] = "Идентификатор опроса";

$MESS["FORM_PAGE"] = "Страница с формой опросов";
$MESS["RESULT_PAGE"] = "Страница с результатом опроса";

$MESS["RESULT_SETINGS"] = "Настройки страницы с результатом опроса";

$MESS["DIOGRAM_TYPE_ID"] = "Типа диаграммы";

$MESS["TypeCircul"] = "Круговая диаграмма";
$MESS["TypeGistogram"] = "Гистограмма";
$MESS ["CITRUS_VOTE_TSZH_ID"] = "Объект управления";
$MESS["CV_ALL"] = "(все)";
