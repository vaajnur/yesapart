<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!CModule::IncludeModule("vdgb.tszhvote")) {
    return;
}

$arGroup = array();

$rs = CCitrusPollGroup::GetList(array("SORT" => "ID"), array("ACTIVE" => "Y"), array("ID", "NAME", "TITLE", "LID", "CODE"));
if (is_object($rs)) {
    while ($arrGroup = $rs->Fetch()) {
        $arGroup[$arrGroup["CODE"]] = "[" . $arrGroup["CODE"] . "] " . $arrGroup["LID"] . " - " . $arrGroup["NAME"];
    }
}

$arTypeDiogram = array(
    "0" => GetMessage("TypeCircul"),
    "1" => GetMessage("TypeGistogram"),
);

$tszhList = array();
if (CModule::IncludeModule("citrus.tszh")) {
    $dbTszh = CTszh::GetList(Array("NAME" => "ASC"));
    $tszhList = Array('' => GetMessage("CV_ALL"));
    while ($tszh = $dbTszh->Fetch()) {
        $tszhList[$tszh['ID']] = $tszh["NAME"];
    }
}


$arComponentParameters = array(
    "GROUPS" => array(
        "SETTINGS" => array(
            "SORT" => 130,
            "NAME" => GetMessage("MAIN_SETINGS"),
        ),
        "RESULT_SETINGS" => array(
            "SORT" => 130,
            "NAME" => GetMessage("RESULT_SETINGS"),
        ),

    ),
    "PARAMETERS" => array(

        "GROUP_ID" => array(
            "NAME" => GetMessage("GROUP_ID"),
            "TYPE" => "LIST",
            "PARENT" => "SETTINGS",
            "VALUES" => $arGroup,
            "MULTIPLE" => "N",
            "ADDITIONAL_VALUES" => "Y",
        ),
        "TSZH_ID" => array(
            "NAME" => GetMessage("CITRUS_VOTE_TSZH_ID"),
            "TYPE" => "LIST",
            "PARENT" => "SETTINGS",
            "VALUES" => $tszhList,
            "MULTIPLE"=>"N",
        ),

        "VARIABLE_ALIASES" => Array(
            "VOTING_ID" => Array("NAME" => GetMessage("VOTE_ID_TITLE")),
        ),
        "SEF_MODE" => Array(
            "vote" => array(
                "NAME" => GetMessage("FORM_PAGE"),
                "DEFAULT" => "",
                "VARIABLES" => array(),
            ),
            "detail" => array(
                "NAME" => GetMessage("RESULT_PAGE"),
                "DEFAULT" => "#VOTING_ID#/",
                "VARIABLES" => array("#VOTING_ID#"),
            ),
        ),
        "VOTE_TYPE_DIOGRAM" => array(
            "NAME" => GetMessage("DIOGRAM_TYPE_ID"),
            "TYPE" => "LIST",
            "PARENT" => "RESULT_SETINGS",
            "VALUES" => $arTypeDiogram,
        ),
    )
);

?>