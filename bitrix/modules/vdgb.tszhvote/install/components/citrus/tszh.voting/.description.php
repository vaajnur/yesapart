<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("VOTE_NAME"),
	"DESCRIPTION" => GetMessage("VOTE_COMPONENT_DESC"),
	"ICON" => "/images/news_all.gif",
	"COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "citrus",
                "NAME" => GetMessage("SECTION_NAME"),
	),
);

?>