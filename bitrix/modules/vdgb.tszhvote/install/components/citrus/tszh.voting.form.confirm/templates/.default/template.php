<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>

<? //��������� �� �����, ��� ����� ������������� ����������� ?>		
<input name = "vote" id = "vote-submit" type = "submit" value = "<?= GetMessage("VOTE_SUBMIT_BUTTON") ?>" class = "voting-submit">

<noscript>
<span style="color: red;"><?= GetMessage("CONFIRM_FALSE"); ?></span>
</noscript>

<div id="box">
    <div id="box-inner">
    </div>            
    <div class="box-form" id="box1">
        <div class="close main-bg" onclick="show('none')" >&times;</div>
        <div class="header main">
            <div id="head1">
                <?= GetMessage("CONFIRM_TITLE") ?>
            </div>
        </div>
        <div class="field">
            <div id="field1">
                <? if ($arResult['option'] == 1) echo GetMessage("CONFIRM_CONTACT_EMAIL"); ?>
                <? if ($arResult['option'] == 2) echo GetMessage("CONFIRM_CONTACT_PHONE"); ?>
            </div>
        </div>
        <div class="input-contact" style="width: 100%;">
            <div id="in1">
                <input type="text" id="contact" name="contact" maxlength="40" <?
                if ($arResult["contact"] != false)
                    echo 'value="'.$arResult["contact"].'"';
                if ($arResult['option'] == 2)
                    echo 'placeholder="'.GetMessage("CONFIRM_EQ").', +7 999 999-99-99"';
                if ($arResult['option'] == 1)
                    echo 'placeholder="'.GetMessage("CONFIRM_EQ").', box@mail.com"';
                ?>/>
                <div class="main-bg" id="hint"><? if ($arResult['option'] == 2) echo GetMessage("CONFIRM_PHONE_VALID") ?></div>
				<div id="in1error" name="in1error" style="display:none; text-align: center; color: red;  margin-left: 10%;  margin-right: 10%; margin-bottom: 5px;">
					<?= GetMessage("CONFIRM_FLASE").' '.GetMessage("CONFIRM_CONTACT").' '.GetMessage("CONFIRM_RE") ?>
				</div>
            </div>
        </div>
        <div class="button-container" id="button">
            <div id="but1">
                <input type="submit" id="send-button" name="send-button" value="<?= GetMessage("CONFIRM_SEND") ?>" class="main-bg" <? if (!$arResult["contact"]) echo 'disabled' ?>/>
                <input type="submit" id="cancel-button" name="cancel-button" value="<?= GetMessage("CONFIRM_CANCEL") ?>"/>                        
            </div>
        </div>
        <div class="smstoemail" class="smstoemail">
            <a id="smstoemail" style="display: none;"><? if ($arResult['option'] ==
                           2) echo GetMessage("CONFIRM_SMSTOEMAIl") ?></a>
        </div>
    </div>

    <div class="box-form" id="box2" style="display: none;">
        <div class="close main-bg" onclick="show('none')" >&times;</div>
        <div class="header main">
            <div id="head2">
                <?= GetMessage("CONFIRM_SEND_TRUE") ?>
            </div>
        </div>
        <div class="field">
            <div id="field2">
                <?= GetMessage("CONFIRM_SEND_TRUE_COMMENT"); ?>
                <?= GetMessage("CONFIRM_SEND_TRUE_INPUT"); ?>
            </div>
        </div>
        <div class="input-contact">
            <div id="in2" name="in2">
                <input type="password" id="code" name="code" maxlength="40" value=""/>
            </div>
			<div id="in2error" name="in2error" style="display:none; text-align: center; color: red;">
				<?= GetMessage("CONFIRM_ERROR_READ") ?>
			</div>
        </div>
        <div class="button-container" id="button">
            <div id="but2">
                <input type="submit" id="confirm-button" name="confirm-button" class="main-bg" value="<?= GetMessage("CONFIRM_SEND_CONFIRM") ?>"/>
            </div>
        </div>
        <div class="no-code" id="no-code">
            <a id="return"><?= GetMessage("CONFIRM_SEND_FALSE") ?></a>
        </div>
    </div>

    <div class="box-form" id="box3" style="display: none;">
        <div id="congratulation" class="main" >
            <?= GetMessage("CONFIRM_TRUE").'</br></br>'.GetMessage("CONFIRM_TRUE_VOTE") ?>
        </div> 
    </div>            
</div>

<script>
    // �������/������� ���� ��� �������� � �����
    function show(state)
    {
        document.getElementById('box').style.display = state;
    }
	
	 // ��� ������� �� ������ ������        
    BX.ready(function ()
    {
        BX.bind(BX('cancel-button'), 'click', function (event)
        {
            event.preventDefault();
			document.getElementById('box').style.display = 'none';
        });
    });

    // ��� ������� �� ������ ����������        
    BX.ready(function ()
    {
        BX.bind(BX('vote-submit'), 'click', function (event)
        {
            event.preventDefault();
<?
if ($arResult['option'] == 0)
{
    echo 'BX(\''.$arResult['form_id'].'\').submit();';
}
else
{
    echo 'show(\'block\');';
    echo "document.getElementById('contact').focus();";
}
?>
        });
    });

    //��������� �������� ��� ��������� �����
    BX.ready(function ()
    {
        BX.bind(BX('contact'), 'bxchange', function (event)
        {
            event.preventDefault();
            BX.ajax({
                url: '<?= $templateFolder.'/send.php' ?>',
                data: {'valid': BX('contact').value},
                method: 'POST',
                dataType: 'json',
                timeout: 30,
                async: true,
                processData: true,
                scriptsRunFirst: true,
                emulateOnload: true,
                start: true,
                cache: false,
                onsuccess: function (data)
                {
                    if (data.valid)
                    {
                        document.getElementById('send-button').disabled = 0;
                        document.getElementById('contact').style.background = '';
                        document.getElementById('hint').style.display = 'none';
                    }
                    else
                    {
                        document.getElementById('send-button').disabled = 1;
                        document.getElementById('contact').style.background = 'antiquewhite';
                        document.getElementById('hint').style.display = 'block';
                    }
                },
                onfailure: function ()
                {
                    //���� ������ ����
                    document.getElementById('send-button').disabled = 1;
                    document.getElementById('contact').style.background = '';
                    document.getElementById('hint').style.display = 'display';
                }
            });
        });

		//��� ������� �� ������ ��������� (��� ����� �������� ��� ��������� �����)
        BX.bind(BX('send-button'), 'click', function (event)
        {
            event.preventDefault();
            document.getElementById('smstoemail').style.display = 'none';
            BX.ajax({
                url: '<?= $templateFolder.'/send.php' ?>',
                data: {'contact': BX('contact').value, 'send': BX('send-button').value},
                method: 'POST',
                dataType: 'json',
                timeout: 30,
                async: true,
                processData: true,
                scriptsRunFirst: true,
                emulateOnload: true,
                start: true,
                cache: false,
                onsuccess: function (data)
                {
                    if (data.sendFlag)
                    {

                        //�������� ������ ��������
                        document.getElementById('box1').style.display = 'none';
                        // ��������� ������ ��������
                        document.getElementById('box2').style.display = 'block';
                        document.getElementById('code').focus();

                    }
                    else
                    {
                        //alert('<?= GetMessage("CONFIRM_FLASE").' '.GetMessage("CONFIRM_CONTACT").' '.GetMessage("CONFIRM_RE") ?>');
						document.getElementById('in1error').style.display = 'block';						
                        document.getElementById('smstoemail').style.display = 'block';
						document.getElementById('contact').style.display = 'none';
						document.getElementById('but1').style.display = 'none';
                    }
                },
                onfailure: function (data)
                {//������
                    //alert('<?= GetMessage("CONFIRM_FLASE").' '.GetMessage("CONFIRM_CONTACT").' '.GetMessage("CONFIRM_RE") ?>');
					document.getElementById('in1error').style.display = 'block';
                    document.getElementById('smstoemail').style.display = 'block';
					document.getElementById('contact').style.display = 'none';
					document.getElementById('but1').style.display = 'none';
                }
            });
        });
    

    //��� ������� �� ���� ����� ����
        BX.bind(BX('code'), 'click', function (event)
        {
            event.preventDefault();
            document.getElementById('code').style.background = '';
            document.getElementById('code').placeholder = '';
        });
    });

    //������� �������            
    var $count_read = 0;
    // ������������ ���������� ������� - �������� � ������
    var $count = <?=
$arResult['count_read_code'];
?>;
    //��� ������� ������ ������������� ����
    BX.ready(function ()
    {
        BX.bind(BX('confirm-button'), 'click', function (event)
        {
            event.preventDefault();
            document.getElementById('code').style.background = '';
            $count_read++;
            if ($count_read < $count + 1)
            {
                BX.ajax({
                    url: '<?= $templateFolder.'/send.php' ?>',
                    data: {'code': BX('code').value},
                    method: 'POST',
                    dataType: 'json',
                    timeout: 30,
                    async: true,
                    processData: true,
                    scriptsRunFirst: true,
                    emulateOnload: true,
                    start: true,
                    cache: false,
                    onsuccess: function (data)
                    {
                        if (data.confirm)
                        {
                            document.getElementById('box2').style.display = 'none';
                            document.getElementById('box3').style.display = 'block';
                            //���������� ���������� �����������
                            BX('<?= $arResult['form_id'] ?>').submit();
                        }
                        else
                        {
                            //������������ ���
                            document.getElementById('code').style.background = 'antiquewhite';
                            document.getElementById('code').value = '';
                            document.getElementById('code').placeholder = '<?= GetMessage("CONFIRM_RECODE") ?>' + ($count - $count_read);
                            //��������� �������� ���������� �������                                
                            if ($count_read == $count)
                            {
								document.getElementById('code').disabled = 1;
								document.getElementById('confirm-button').disabled = 1;
								document.getElementById('confirm-button').style.display = 'none';
								document.getElementById('in2').style.display = 'none';                                
								document.getElementById('in2error').style.display = 'block';
                            }

                        }
                    },
                    onfailure: function ()
                    {
                        //������ ���
                        document.getElementById('code').style.background = 'antiquewhite';
                        document.getElementById('code').value = '';
                        document.getElementById('code').placeholder = '<?= GetMessage("CONFIRM_RECODE") ?>' + ($count - $count_read);
                        //��������� �������� ���������� �������
                        if ($count_read == $count)
                        {
							document.getElementById('code').disabled = 1;
							document.getElementById('confirm-button').disabled = 1;
							document.getElementById('confirm-button').style.display = 'none';
							document.getElementById('in2').style.display = 'none';                            
							document.getElementById('in2error').style.display = 'block';                            
                        }
                    }

                });
            }
        });
    
    //���������� ����� ���
        BX.bind(BX('return'), 'click', function (event)
        {
            event.preventDefault();
            // ��������� ������ ��������
            document.getElementById('box2').style.display = 'none';

            //�������� ���������� ������� � ���������� ����� � �������� ���������
            $count_read = 0;
            document.getElementById('confirm-button').disabled = 0;
			document.getElementById('code').disabled = 0;
			document.getElementById('code').style.background = '';
            document.getElementById('code').value = '';
			document.getElementById('confirm-button').style.display = 'block';
			document.getElementById('but1').style.display = 'block';
			document.getElementById('contact').style.display = 'block';
            document.getElementById('in2').style.display = 'block';
            document.getElementById('code').placeholder = "";			
			document.getElementById('in2error').style.display = 'none';			
			document.getElementById('in1error').style.display = 'none';						
            document.getElementById('smstoemail').style.display = 'none';
			
            //�������� ������ ��������
            document.getElementById('box1').style.display = 'block';
        });
    

    //�������� ����� ������ ���
        BX.bind(BX('smstoemail'), 'click', function (event)
        {
            event.preventDefault();
            BX.ajax({
                url: '<?= $templateFolder.'/send.php' ?>',
                data: {'contact': '<?= $arResult['email'] ?>', 'send': BX('send-button').value, 'smstoemail': '1'},
                method: 'POST',
                dataType: 'json',
                timeout: 30,
                async: true,
                processData: true,
                scriptsRunFirst: true,
                emulateOnload: true,
                start: true,
                cache: false,
                onsuccess: function (data)
                {
                    if (data.sendFlag)
                    {
                        document.getElementById('box1').style.display = 'none';
                        document.getElementById('box2').style.display = 'block';
                        document.getElementById('code').focus();
                    }
                    else
                    {
						document.getElementById('in1error').style.display = 'block';
                    }
                },
                onfailure: function (data)
                {//������
					document.getElementById('in1error').style.display = 'block';
                }
            });
        });
    });
</script>
<?
// ����� ������������� �����������
?>
