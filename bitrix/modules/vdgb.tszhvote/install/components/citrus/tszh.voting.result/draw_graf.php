<?php

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/img.php");

if(!CModule::IncludeModule("vdgb.tszhvote")){
    return;
}

if(isset($_REQUEST['QUESTION']) && IntVal($_REQUEST['QUESTION']) > 0) {
    $question_id = IntVal($_REQUEST['QUESTION']);
}
else {
    return;
}


$arColors = Array();
$rsAnswers = CCitrusPollAnswer::GetList(Array("SORT" => "ASC"), Array("QUESTION_ID" => $question_id), false, false, Array("ID", "COLOR"));
$totalRecords = $rsAnswers->SelectedRowsCount();
while ($arAnswer = $rsAnswers->Fetch())
{
	$arColors[$arAnswer['ID']] = $arAnswer['COLOR'];
	$arParam[$arAnswer['ID']] = Array("COUNTER" => 0);
}


// ����� ������� �� ��������� �������
$arAnswerSums = Array();
$rsAnswers = CCitrusPollVoteAnswer::GetList(Array(), Array("VOTE_QUESTION_ID" => $question_id), Array("ANSWER_ID", "SUM" => 'VOTE_WEIGHT'));
$votedVolume = 0;
while ($arAnswer = $rsAnswers->GetNext(false))
{
	$arParam[$arAnswer['ANSWER_ID']]['COUNTER'] += $arAnswer['VOTE_WEIGHT'];
	$votedVolume += $arAnswer['VOTE_WEIGHT'];
}

// totals
// =======
$obMethod = CCitrusPollMethodBase::GetMethodObj($VOTE['VOTING_METHOD']);
if (is_object($obMethod))
{
	$ar = CCitrusPollVote::GetList(Array(), Array("QUESTION_ID" => $question_id, false, Array('nTopCount' => 1)), Array("ENTITY_ID"))->Fetch();
	$totalVolume = $obMethod->GetTotalVolume($ar['ENTITY_ID']);
	if ($totalVolume > 0)
	{
		$arParam['-'] = Array(
			"COUNTER" => $totalVolume - $votedVolume,
			"COLOR" => "#CCCCCC",
		);
		$totalRecords++;
	}
}

if(empty($arParam))
    return;

foreach ($arColors as $key => $value)
{
    if (strlen($value) > 0)
    {
        $arParam[$key]['COLOR'] = $value;
    }
    else
    {
        $strColor = GetNextRGB($strColor, $totalRecords);
        while (in_array('#' . $strColor, $arColors))
            $strColor = GetNextRGB($strColor, $totalRecords);

        $arParam[$key]['COLOR'] = "#" . $strColor;
    }
}
$diameter = (intval($_REQUEST["dm"])>0) ? intval($_REQUEST["dm"]) : 150;

// create an image
$ImageHandle = CreateImageHandle($diameter, $diameter);
imagefill($ImageHandle, 0, 0, imagecolorallocate($ImageHandle, 255,255,255));


Circular_Diagram($ImageHandle, $arParam, "FFFFFF", $diameter, $diameter/2, $diameter/2,true);

ShowImageHeader($ImageHandle);


?>