<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule("vdgb.tszhvote"))
	return;

$arGroupList = Array();
$rsGroups = CCitrusPollGroup::GetList(Array("SORT" => "ASC"), Array("ACTIVE" => "Y"), false, false, Array("ID", "NAME"));
while ($arGroup = $rsGroups->GetNext())
	$arGroupList[$arGroup['ID']] = $arGroup['NAME'];

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"GROUP_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CVE_GROUP_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arGroupList,
			"DEFAULT" => '',
			"ADDITIONAL_VALUES" => "Y",
		),
		"ALLOW_NEW_ELEMENT" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CVE_ALLOW_NEW_ELEMENT"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => 'Y',
		), 
	),
);
?>
