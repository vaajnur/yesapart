DROP TABLE if exists b_citrus_voting_event_answer;
DROP TABLE if exists b_citrus_voting_event;
DROP TABLE if exists b_citrus_voting_group;
DROP TABLE if exists b_citrus_voting;
DROP TABLE if exists b_citrus_voting_answers;
DROP TABLE if exists b_citrus_voting_question;