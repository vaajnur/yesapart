<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$arDescription = Array(
    "NAME"             => GetMessage("GD_VDGB_VOTE_NAME"),
    "DESCRIPTION"      => GetMessage("GD_VDGB_VOTE_DESC"),
    "ICON"             => "",
    "TITLE_ICON_CLASS" => "bx-gadgets-citrus-support",
    "GROUP"            => Array("ID" => "other"),
    "NOPARAMS"         => "Y",
    "AI" => true,
    //"SU"               => true,
    //"SG"               => true,
    //"COLOURFUL"        => true
);
?>
