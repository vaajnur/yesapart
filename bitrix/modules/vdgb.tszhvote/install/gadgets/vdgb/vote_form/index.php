<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();


if (!IsModuleInstalled("vdgb.tszhvote"))
    return;

\Bitrix\Main\Loader::includeModule("vdgb.tszhvote");

$APPLICATION->SetAdditionalCSS($arGadget['PATH_SITEROOT'].'/styles.css');

//���� ��������� ����������� (�� ������� ������ ��������� �����)
$flag = FALSE;

$rsVoting = CCitrusPoll::GetList();
while ($arVoting = $rsVoting->Fetch())
{
    $arResult = $arVoting;
    //���������� �������

    $rsVote = CCitrusPollVote::GetList();
    //��������� �����
    $count  = 0;
    while ($arVote = $rsVote->Fetch())
    {
        $result = $arVote;
        if ($arVote['VOTING_ID'] == $arResult['ID'])
            $count++;
    }
    //���� ��������� ����� �� ������ �����    
    if ($result['VOTING_ID'] == $arResult['ID'])
        $flag = TRUE;
    //���� ����� ��������� �������� �����������
    if ($flag)
        break;
}

if ($flag)
{
    $APPLICATION->IncludeComponent(
            "citrus:tszh.voting.result", "",
            Array(
        "GROUP_ID"          => $arResult['GROUP_ID'],
        "VOTING_ID"         => array($arResult['ID']),
        "VOTE_TYPE_DIOGRAM" => "0",
        "CACHE_TYPE"        => "A",
        "CACHE_TIME"        => "36000000"
            )
    );

    echo '<p>'.GetMessage("GD_VOTE_LAST_VOICE").': '.$result['TIMESTAMP_X'].'</p>';
    echo '<p>'.GetMessage("GT_VOTE_COUNT_VOICE").': '.$count.'</p>';
    echo '<a href="../admin/vdgb_tszhvote_event.php">'.GetMessage("GD_VOTE_VIEW_RESULT").'</a><br/>';
    
}
else
    echo GetMessage("GD_VOTE_NOACTIVE");
?>
