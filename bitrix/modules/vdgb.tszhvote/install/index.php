<?

global $MESS;
IncludeModuleLangFile(__FILE__);

if (class_exists("vdgb_tszhvote"))
    return;

class vdgb_tszhvote extends CModule
{

    var $MODULE_ID   = "vdgb.tszhvote";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    //
    var $EVENT_NAME  = "voteNot";
    var $OPTION_NAME = "sendToWay";

    //

    function vdgb_tszhvote()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include ($path."/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION",
                                                           $arModuleVersion))
        {
            $this->MODULE_VERSION      = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        else
        {
            $this->MODULE_VERSION      = SEARCH_VERSION;
            $this->MODULE_VERSION_DATE = SEARCH_VERSION_DATE;
        }

        $this->MODULE_NAME        = GetMessage("C_VOTE_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("C_VOTE_MODULE_DESC");

        $this->PARTNER_NAME = GetMessage("C_VOTE_PARTNER_NAME");
        $this->PARTNER_URI  = GetMessage("C_VOTE_PARTNER_URI");
    }

    function DoInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION, $step;
        $step = IntVal($step);

        if ($step < 2)
        {
            $APPLICATION->ResetException();
            try
            {
                if (!$this->InstallDB())
                    throw new Exception(GetMessage("C_ERROR_INSTALL_DB"));
                if (!$this->InstallEvents())
                    throw new Exception(GetMessage("C_ERROR_INSTALL_EVENTS"));
                if (!$this->InstallFiles())
                    throw new Exception(GetMessage("C_ERROR_INSTALL_FILES"));
                if (!$this->InstallGadgets('VOTE_FORM'))
                    throw new Exception(GetMessage("C_ERROR_INSTALL_GADGET"));
            }
            catch (Exception $e)
            {
                $strAdditionalMessage = '';
                if ($ex                   = $APPLICATION->GetException())
                    $strAdditionalMessage .= ' ('.$ex->GetString().')';
                $APPLICATION->ThrowException($e->GetMessage().$strAdditionalMessage);
            }
            $APPLICATION->IncludeAdminFile(GetMessage("C_VOTE_MODULE_INSTAL_PAGE_TITLE"),
                                                      $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/step.php");
        }
    }

    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION, $step;
        $step = IntVal($step);
        if ($step < 2)
        {
            $APPLICATION->IncludeAdminFile(GetMessage("C_VOTE_MODULE_UNINSTALL_PAGE_TITLE"),
                                                      $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/unstep1.php");
        }
        elseif ($step < 3)
        {
            $APPLICATION->ResetException();
            try
            {
                if (!$this->UnInstallDB(array("save_tables" => $_REQUEST["save_tables"] ==
                            1)))
                    throw new Exception(GetMessage("C_ERROR_UNINSTALL_DB"));
                if (!$this->UnInstallEvents())
                    throw new Exception(GetMessage("C_ERROR_UNINSTALL_EVENTS"));
                if (!$this->UnInstallFiles())
                    throw new Exception(GetMessage("C_ERROR_UNINSTALL_FILES"));
            }
            catch (Exception $e)
            {
                $strAdditionalMessage = '';
                if ($ex                   = $APPLICATION->GetException())
                    $strAdditionalMessage .= ' ('.$ex->GetString().')';
                $APPLICATION->ThrowException($e->GetMessage().$strAdditionalMessage);
            }
            $APPLICATION->IncludeAdminFile(GetMessage("C_VOTE_MODULE_UNINSTALL_PAGE_TITLE"),
                                                      $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/unstep2.php");
        }
    }

    function InstallDB()
    {
        global $DBType, $APPLICATION, $DBName, $DB;
        $errors = false;

        if (!$DB->Query("SELECT 'x' FROM b_citrus_voting WHERE 1=0", true))
        {
            @set_time_limit(0);
            $DB->StartTransaction();
            $errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/db/".strtolower($DB->type)."/install.sql");
            if ($this->errors)
                $DB->Rollback();
            else
                $DB->Commit();
        }

        if ($errors !== false)
        {
            $APPLICATION->ThrowException(implode("<br>", $errors));
            return false;
        }

        RegisterModule($this->MODULE_ID);
        RegisterModuleDependences("main", "OnUserDelete", $this->MODULE_ID,
                                  "CVoteUser", "DeleteByUserID");

        return true;
    }

    function UnInstallDB($arParams = array())
    {
        global $DB, $DBType, $APPLICATION;

        $errors = false;

        if (!$arParams['save_tables'] && is_object($DB))
            $errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/db/".strtolower($DB->type)."/uninstall.sql");

        UnRegisterModuleDependences("main", "OnUserDelete", $this->MODULE_ID,
                                    "CVoteUser", "DeleteByUserID");
        UnRegisterModule($this->MODULE_ID);

        if ($errors !== false)
        {
            $APPLICATION->ThrowException(implode("<br>", $errors));
            return false;
        }
        return true;
    }

    function InstallFiles()
    {
        $errors = Array();

        if (!CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/{$this->MODULE_ID}/install/themes/",
                          $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/", true,
                          true))
            $errors[] = GetMessage("C_ERROR_INSTALL_THEME_ADMIN");
        if (!CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/{$this->MODULE_ID}/install/admin/",
                          $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true, true))
            $errors[] = GetMessage("C_ERROR_INSTALL_FILES_ADMIN");
        if (!CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/{$this->MODULE_ID}/install/components/",
                          $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true,
                          true))
        {
            $errors[] = GetMessage("C_ERROR_INSTALL_FILES_COMPONENTS");
        }

        //Gadgets
        if (!CopyDirFiles($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/{$this->MODULE_ID}/install/gadgets",
                          $_SERVER['DOCUMENT_ROOT'].'/bitrix/gadgets', True,
                          True))
            $errors[] = GetMessage("C_ERROR_INSTALL_FILES_GADGET");

        if (count($errors) > 0)
        {
            $APPLICATION->ThrowException(implode("<br>", $errors));
            return false;
        }

        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/{$this->MODULE_ID}/install/admin/",
                       $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin/");
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/{$this->MODULE_ID}/install/components/",
                       $_SERVER["DOCUMENT_ROOT"]."/bitrix/components/");
        DeleteDirFiles($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/{$this->MODULE_ID}/install/gadgets/",
                       $_SERVER['DOCUMENT_ROOT'].'/bitrix/gadgets/');
        DeleteDirFilesEx("/bitrix/themes/.default/icons/{$this->MODULE_ID}/");
        unlink($_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default/{$this->MODULE_ID}.css/");
        return true;
    }

    function InstallEvents()
    {
         COption::RemoveOption($this->MODULE_ID);
		//����� �������� ��� � email
        COption::SetOptionInt($this->MODULE_ID, $this->OPTION_NAME, 0);
        COption::SetOptionInt($this->MODULE_ID, "{$this->OPTION_NAME}.count", 3);

        COption::SetOptionString($this->MODULE_ID, "{$this->OPTION_NAME}.login", '');
        COption::SetOptionString($this->MODULE_ID, "{$this->OPTION_NAME}.pass", '');
        COption::SetOptionString($this->MODULE_ID, "{$this->OPTION_NAME}.source", '');

        //��� �������� email
        //������� ��� ��������� �������
        $obEventType = CEventType::Add(
                        array(
                            'EVENT_NAME'  => $this->EVENT_NAME,
                            'NAME'        => GetMessage("C_VOTNOT_MODULE_EVENT_NAME"),
                            'LID'         => 'ru',
                            'DESCRIPTION' =>
                            "#CODE# - ".GetMessage("C_VOTNOT_MODULE_EVENT_CODE")."\r\n".
                            "#TOEMAIL# - ".GetMessage("C_VOTNOT_MODULE_EVENT_EMAIL")."\r\n".
                            "#SUBJECT# - ".GetMessage("C_VOTNOT_MODULE_EVENT_SUBJECT")."\r\n",
                        )
        );

        if ($obEventType > 0)
        {
            $rsSites    = CSite::GetList($by         = "id", $order      = "desc");
            $arSiteId   = array();
            //������ ������ �� ID
            while ($arSite     = $rsSites->Fetch())
                $arSiteId[] = $arSite['ID'];
			
			//������ ��� ��� �����
			$SITE_TEMPLATE_ID = "citrus_tszh_subscribe";
			/*$arMailSiteTemplate = array();
			$mailSiteTemplateDb = CSiteTemplate::GetList(null, array('TYPE' => 'mail'));
			while($mailSiteTemplate = $mailSiteTemplateDb->GetNext())
				$arMailSiteTemplate[] = $mailSiteTemplate;
			foreach($arMailSiteTemplate as $mailTemplate)
			 if ($mailTemplate["ID"] == "citrus_tszh_subscribe") )
				$SITE_TEMPLATE_ID = $mailTemplate["ID"]*/
            $obTemplate = new CEventMessage;
            $result     = $obTemplate->Add(
                    array(
                        'ACTIVE'     => "Y",
                        'EVENT_NAME' => $this->EVENT_NAME,
                        'LID'        => $arSiteId, //������ �� ���� ������
                        'EMAIL_FROM' => "#DEFAULT_EMAIL_FROM#",
                        'EMAIL_TO'   => "#TOEMAIL#",
                        'BCC'        => "",
                        'SUBJECT'    => "#SUBJECT#",
                        'BODY_TYPE'  => "html",
                        'MESSAGE'    => GetMessage("VOTE_EMAIL_BODY"),
						"SITE_TEMPLATE_ID"	=> $SITE_TEMPLATE_ID,
                    )
            );

            /* $obTemplate->Add(
              array(
              'ACTIVE'     => "Y",
              'EVENT_NAME' => $this->EVENT_NAME,
              'LID'        => $arSiteId, //������ �� ���� ������
              'EMAIL_FROM' => "#DEFAULT_EMAIL_FROM#",
              'EMAIL_TO'   => "#DEFAULT_EMAIL_FROM#",
              'BCC'        => "",
              'SUBJECT'    => "#SUBJECT#",
              'BODY_TYPE'  => "text",
              'MESSAGE'    => '��� ������������� ������������: #CODE#',
              )
              ); */
            return ($result > 0);
        }
        return $obEventType;
    }

    function UnInstallEvents()
    {
        //������� ��� ����� ������
        COption::RemoveOption($this->MODULE_ID, "");

        //������� ������
        $rsMess = CEventMessage::GetList($by     = "event_name",
                                         $order  = "desc",
                                         array('TYPE_ID' => $this->EVENT_NAME));

        while ($arFiltr = $rsMess->Fetch())
        {
            //������� ������ �� ID
            $obTemplate = CEventMessage::Delete($arFiltr['ID']);
            if (!$obTemplate)
                return $obTemplate;
        }

        //���� ������ �������, ������� �������
        $obEventType = CEventType::Delete($this->EVENT_NAME);
        return ($obEventType > 0);
    }

    function InstallGadgets($gadgets_id)
    {
        $gdid = $gadgets_id."@".rand();

        $arUserOptions = array();
        //CUserOptions::DeleteOptionsByName("intranet", "~gadgets_".'admin_index');
        $arUserOptions = CUserOptions::GetOption('intranet',
          "~gadgets_admin_index",
          false, false);


        foreach ($arUserOptions[0]["GADGETS"] as $tempid => $tempgadget)
            if ($tempgadget["COLUMN"] == 0)
                $arUserOptions[0]["GADGETS"][$tempid]["ROW"] ++;

        $arUserOptions[0]["GADGETS"][$gdid] = Array("COLUMN" => 0,
            "ROW"    => 0, 'HIDE'   => 'N');

        CUserOptions::SetOption("intranet", "~gadgets_admin_index",
                                $arUserOptions);

        return true;
    }

}

?>