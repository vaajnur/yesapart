<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<?=$APPLICATION->ShowHead()?>
<style type="text/css">
body {
	margin: 0 auto;
	width: 80%;
}
h1 {
	text-transform: uppercase;
	text-align: center;
	margin: 1em 0 0 0;
}		
h2 {
	text-align: center;
	margin: .3em 0 1.5em 0;
}		
.voting-detail-text {
	margin: 2em 0 3em 0;
}
dl dt {
	font-weight: bold;
}
.voting-question-answers {
	margin-left: 1.5em;
}
</style>
</head>

<body>
<?

$componentPath = "/bitrix/components/citrus/tszh.voting.result";
$arParams['VOTE_TYPE_DIOGRAM'] = 0;

?>

	<h1>Протокол</h1>
	<h2>голосования собственников квартир и нежилых помещений</h2>
	
	<dl>
		<dt>Время проведения:</dt>
		<dd><?=$reportData['VOTING']["DATE_BEGIN"]?> &mdash; <?=$reportData['VOTING']["DATE_END"]?></dd>
		<?
		if ($reportData["VOTING"]['TOTAL_VOLUME'])
		{
			?>
			<dt><?=$reportData["VOTING_METHOD"]->GetTotalVolumeTitle()?></dt>
			<dd><?=$reportData["VOTING"]['TOTAL_VOLUME']?></dd>
			<?
		}
		?>
		<dt>Тема голосования:</dt>
		<dd><?=$reportData['VOTING']['TITLE_TEXT']?></dd>
	</dl>
	
	<div class="voting-detail-text">
		<?=htmlspecialcharsback($reportData['VOTING']['DETAIL_TEXT'])?>
	</div>
	
	<h3>Вопросы на голосовании:</h3>
	<?
	
	foreach ($reportData["QUESTIONS"] as $questionID => $arQuestion)
	{
		?>
		<p class="voting-question"><strong><?=$arQuestion["TEXT"]?></strong></p>
		<div class="voting-question-answers">
			<?
			foreach ($arQuestion["ANSWERS"] as $answerID => $arAnswer)
			{
				$sum = is_object($reportData['VOTING_METHOD']) ? $reportData["VOTING_METHOD"]->FormatVolume($arAnswer["SUM"], $bHTML = true) : $arAnswer['SUM'];
				?>
				<?=$arAnswer["TEXT"]?> — <?=$sum?><br />
				<?
			}
		?>
		</div>
		<?
	}
	
//echo '<pre>' . htmlspecialcharsbx(print_r($reportData, true)) . '</pre>';
?>
<br /><br /><br />	
</body>
</html>