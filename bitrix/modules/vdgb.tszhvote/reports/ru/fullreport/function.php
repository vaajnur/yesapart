<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/vdgb.tszhvote/include.php");

function GetReportData($VOTING_ID)
{

	$arResult = array();

//получаем данные для каждого опроса
	foreach ($VOTING_ID["ID"] as $key => $vote_id)
	{

		$vote_id = IntVal($vote_id);

		//если id опроса > 0 тогда произведем выборку элементов
		if ($vote_id > 0)
		{

			$rsVote = CCitrusPoll::GetByID($vote_id);

			if (is_object($rsVote))
			{
				$arVote = $rsVote->Fetch();
				if ($arVote)
				{
					$arResult['VOTE'][$arVote["ID"]] = $arVote;
				}
			}

			if (isset($arResult['VOTE'][$vote_id]) && !empty($arResult['VOTE'][$vote_id]))
			{

				//сформируем фильтр для выборки списка вопросов
				$arQuestionFilter = array(
					"VOTING_ID" => $vote_id,
					"ACTIVE" => "Y"
				);

				//получим список вопросов для данного опроса
				$rsQuestions = CCitrusPollQuestion::GetList(array(), $arQuestionFilter);
				if (is_object($rsQuestions))
				{
					while ($arQuestion = $rsQuestions->Fetch())
					{
						$arResult['VOTE'][$vote_id]["QUESTION"][$arQuestion["ID"]] = $arQuestion;

						if (!empty($arQuestion))
						{

							//сформируем фильтр для выборки ответов для данного вопроса
							$arAnswerFilter = array(
								"QUESTION_ID" => $arQuestion["ID"],
								"ACTIVE" => "Y",
							);

							$fullCount = 0;

							$rsAnswer = CCitrusPollAnswer::GetList(array(), $arAnswerFilter);
							if (is_object($rsAnswer))
							{
								while ($arAnswers = $rsAnswer->Fetch())
								{

									$arResult['VOTE'][$vote_id]["QUESTION"][$arQuestion["ID"]]['ANSWER'][$arAnswers["ID"]] = $arAnswers;
									if (empty($arAnswers))
									{
										//если массив с ответами пуст, то запишем сообщение об ошибке
										$arResult['ERRORS']['VOTE'][$vote_id] = GetMessage("ANSWER_NOT_FOUND");
									}
									else
									{
										$color[$arAnswers["ID"]] = $arResult['VOTE'][$vote_id]["QUESTION"][$arQuestion["ID"]]['ANSWER'][$arAnswers["ID"]]['COLOR'];

										//сформируем массив фильтра для выборки голосов
										$arEventVote = array(
											"VOTING_ID" => $vote_id,
											"QUESTION_ID" => $arQuestion["ID"],
											"ANSWER_ID" => $arAnswers["ID"],
										);

										$rsEvent = CCitrusPollVote::GetEventList(array(), $arEventVote, array());
										$count = 0;
										if (is_object($rsEvent))
										{
											while ($arEvent = $rsEvent->Fetch())
											{
												$count++;
											}
											$fullCount += $count;
											$arResult['VOTE'][$vote_id]["QUESTION"][$arQuestion["ID"]]['ANSWER'][$arAnswers["ID"]]['COUNT'] = $count;
										}
									}
								}
							}

							$arResult['VOTE'][$vote_id]["QUESTION"][$arQuestion["ID"]]['FULL_COUNT'] = $fullCount;
						}
						else
						{
							//если массив с вопросами пуст, то запишем сообщение об ошибке
							$arResult['ERRORS']['VOTE'][$vote_id] = GetMessage("QUESTION_NOT_FOUND");
						}
					}
				}
			}
			else
			{
				$arResult['ERRORS']['VOTE'][$vote_id] = GetMessage("VOTE_NOT_FOUND");
			}
		}
	}

	return $arResult;
}

