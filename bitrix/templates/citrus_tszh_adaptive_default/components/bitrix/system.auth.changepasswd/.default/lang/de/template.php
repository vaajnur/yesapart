<?
$MESS["AUTH_AUTH"] = "Anmeldung";
$MESS["AUTH_CHANGE"] = "Passwort Г¤ndern";
$MESS["AUTH_CHECKWORD"] = "Kontrollmeldung:";
$MESS["AUTH_LOGIN"] = "Login:";
$MESS["AUTH_NEW_PASSWORD"] = "Neues Passwort:";
$MESS["AUTH_NEW_PASSWORD_REQ"] = "Neues Passwort:";
$MESS["AUTH_CHANGE_PASSWORD"] = "Passwort Г¤ndern";
$MESS["AUTH_NEW_PASSWORD_CONFIRM"] = "PasswortbestГ¤tigung:";
$MESS["AUTH_REQ"] = "Pflichtfelder";
$MESS["AUTH_SECURE_NOTE"] = "Das Passwort wird verschlГјsselt, bevor es versendet wird. So wird das Passwort wГ¤hrend der Гњbertragung nicht offen angezeigt.";
$MESS["AUTH_NONSECURE_NOTE"] = "Das Passwort wird in offener Form versendet. Aktivieren Sie JavaScript  in Ihrem Web-Browser, um das Passwort vor dem Versenden zu verschlГјsseln.";
$MESS["system_auth_captcha"] = "Geben Sie die Symbole ein, die Sie auf dem Bild sehen:";
?>