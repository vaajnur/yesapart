<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("show_title", "N");
$APPLICATION->SetPageProperty("title", "Контакты компании");
$APPLICATION->SetTitle("Контакты");
?>
<h2>Контактная информация</h2>
<?$APPLICATION->IncludeComponent(
	"citrus:tszh.contacts", 
	"orchid_page", 
	array(
		"TSZH_ID" => array(
			0 => "1",
		),
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"SHOW_MAP" => "N",
		"MAP_INIT_MAP_TYPE" => "MAP",
		"MAP_MAP_WIDTH" => "370",
		"MAP_MAP_HEIGHT" => "300",
		"MAP_CONTROLS" => array(
			0 => "ZOOM",
			1 => "TYPECONTROL",
			2 => "SCALELINE",
		),
		"MAP_OPTIONS" => array(
			0 => "ENABLE_SCROLL_ZOOM",
			1 => "ENABLE_DBLCLICK_ZOOM",
			2 => "ENABLE_DRAGGING",
		),
		"SHOW_FEEDBACK_FORM" => "Y",
		"FEEDBACK_FORM_USE_CAPTCHA" => "Y",
		"FEEDBACK_FORM_OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"FEEDBACK_FORM_EMAIL_TO" => "",
		"FEEDBACK_FORM_REQUIRED_FIELDS" => array(
			0 => "NAME",
			1 => "EMAIL",
			2 => "MESSAGE",
		),
		"FEEDBACK_FORM_EVENT_MESSAGE_ID" => array(
		),
		"COMPONENT_TEMPLATE" => "orchid_page",
		"MAX_COUNT" => "3",
		"CONTACTS_URL" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>