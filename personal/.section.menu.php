<?
$aMenuLinks = Array(
	Array(
		"Общая информация", 
		"/personal/index.php", 
		Array(), 
		Array("class"=>"leftmenu__img-common"), 
		"" 
	),
	Array(
		"История начислений и оплат", 
		"/personal/circulating-sheet/", 
		Array(), 
		Array("class"=>"leftmenu__img-history"), 
		"" 
	),
	Array(
		"Квитанция на оплату коммунальных услуг", 
		"/personal/receipt/", 
		Array(), 
		Array("class"=>"leftmenu__img-print", "target"=>"_blank"), 
		"" 
	),
	Array(
		"Данные счетчиков", 
		"/personal/meters/", 
		Array(), 
		Array("class"=>"leftmenu__img-meters"), 
		"" 
	),
	Array(
		"Оплатить услуги через сайт", 
		"/personal/payment/", 
		Array(), 
		Array("class"=>"pay_utilities leftmenu__img-payment"), 
		"" 
	),
	Array(
		"Мои заявки", 
		"/personal/support/", 
		Array(), 
		Array("class"=>"service leftmenu__img-requests"), 
		"" 
	),
	Array(
		"Управление подпиской", 
		"/personal/subscription/", 
		Array(), 
		Array("class"=>"leftmenu__img-subscriptions"), 
		"false" 
	),
	Array(
		"Графики", 
		"/personal/chart/", 
		Array(), 
		Array("class"=>"chart_utilities leftmenu__img-chart"), 
		"" 
	)
);
?>