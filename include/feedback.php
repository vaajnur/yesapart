<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--noindex-->
<?$APPLICATION->IncludeComponent(
	"citrus:main.feedback", 
	"orchid_feedback", 
	array(
		"USE_CAPTCHA" => "Y",
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"EMAIL_TO" => "residence@yesapart.com",
		"REQUIRED_FIELDS" => array(
			0 => "EMAIL",
		),
		"EVENT_MESSAGE_ID" => array(
		),
		"COMPONENT_TEMPLATE" => "orchid_feedback",
		"MAX_FILE_SIZE" => "10",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
<!--noindex-->
<div class="search">
    <div class="search__form">
        <form id="search-form" action="<?=SITE_DIR?>search/" method="get">
            <input class="search__input" type="text" name="q"  placeholder="Поиск по сайту"/>
            <input class="search__button" value="" type="submit"/>
        </form>
    </div>
</div>
<!--/noindex-->