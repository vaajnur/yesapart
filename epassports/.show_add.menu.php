<?
 
use Citrus\Tszh\HouseTable;
use Vdgb\Tszhepasport\EpasportTable;


$by = 'ID';
$order = 'DESC';
$arFilter = [];
$arListFilter = [];
$arSelect = [];
if (CModule::IncludeModule('vdgb.tszhepasport'))
{
	$arSelect = Array("*", "UF_*", "FULL_ADDRESS", '' => 'Vdgb\Tszhepasport\EpasportTable:HOUSE.*');
}
else
{
	$arSelect = Array("*", "UF_*", "FULL_ADDRESS",);
}
$rsItems = HouseTable::getList(
	array(
		"order" => array(ToUpper($by) => $order),
		"filter" => array_merge($arFilter, $arListFilter),
		"select" => $arSelect
	)
);

$aMenuLinks = array();
while($obj = $rsItems->fetch()){
	// echo print_r($obj['STREET']);
	$id = $obj['ID'];
	$aMenuLinks[] = Array(
		$obj['STREET'], 
		"/epassports/?home-id=$id",
		Array("/epassports/?home-id=$id"),  
		Array(
			"FROM_IBLOCK" => 0,
			"IS_PARENT" => 1,
			"DEPTH_LEVEL" => 1
		)
	);
	$aMenuLinks2 = [
		Array(
		"Подробная информация", 
		"/punkt_link/", 
		Array("/punkt_link2/"), 
		Array(
			"FROM_IBLOCK" => 0,
			"IS_PARENT" => 0,
			"DEPTH_LEVEL" => 2
		)
		),
		Array(
		"Документы и отчетность", 
		"/documents/", 
		Array("/documents/"), 
		Array(
			"FROM_IBLOCK" => 0,
			"IS_PARENT" => 1,
			"DEPTH_LEVEL" => 2
		)
		),
		Array(
			"Общая информация", 
			"/punkt_link1/", 
			Array("/punkt_link1/"), 
			Array(
				"FROM_IBLOCK" => 0,
				"IS_PARENT" => 0,
				"DEPTH_LEVEL" => 3
			)
		),
		Array(
			"Основные показатели фин. хоз. деятельности", 
			"/punkt_link2/", 
			Array("/punkt_link2/"), 
			Array(
				"FROM_IBLOCK" => 0,
				"IS_PARENT" => 0,
				"DEPTH_LEVEL" => 3
			)
		),
		Array(
			"Порядок и условия оказания услуг", 
			"/punkt_link3/", 
			Array("/punkt_link3/"), 
			Array(
				"FROM_IBLOCK" => 0,
				"IS_PARENT" => 0,
				"DEPTH_LEVEL" => 3
			)
		),
		Array(
			"Сведения о стоимости ремонта", 
			"/punkt_link4/", 
			Array("/punkt_link4/"), 
			Array(
				"FROM_IBLOCK" => 0,
				"IS_PARENT" => 0,
				"DEPTH_LEVEL" => 3
			)
		)
	];
	$aMenuLinks = array_merge($aMenuLinks, $aMenuLinks2);
}


/*$aMenuLinks = Array(
	Array(
		'Боровая', 
		"/punkt_link0/",
		Array("/punkt_link0/"),  
		Array(
			"FROM_IBLOCK" => 0,
			"IS_PARENT" => 1,
			"DEPTH_LEVEL" => 1
		)
	),
	Array(
		"Подробная информация", 
		"/punkt_link/", 
		Array("/punkt_link2/"), 
		Array(
			"FROM_IBLOCK" => 0,
			"IS_PARENT" => 0,
			"DEPTH_LEVEL" => 2
		)
	),
		Array(
		"Документы и отчетность", 
		"/documents/", 
		Array("/documents/"), 
		Array(
			"FROM_IBLOCK" => 0,
			"IS_PARENT" => 1,
			"DEPTH_LEVEL" => 2
		)
	),
	Array(
		"Общая информация", 
		"/punkt_link/", 
		Array("/punkt_link3/"), 
		Array(
			"FROM_IBLOCK" => 0,
			"IS_PARENT" => 0,
			"DEPTH_LEVEL" => 3
		)
	),
	Array(
		"Основные показатели фин. хоз. деятельности", 
		"/punkt_link/", 
		Array("/punkt_link4/"), 
		Array(
			"FROM_IBLOCK" => 0,
			"IS_PARENT" => 0,
			"DEPTH_LEVEL" => 3
		)
	),
	Array(
		'Марата', 
		"/punkt_link0/",
		Array("/punkt_link0/"),  
		Array(
			"FROM_IBLOCK" => 0,
			"IS_PARENT" => 1,
			"DEPTH_LEVEL" => 1
		)
	),
	Array(
		"Подробная информация", 
		"/punkt_link/", 
		Array("/punkt_link/"), 
		Array(
			"FROM_IBLOCK" => 0,
			"IS_PARENT" => 0,
			"DEPTH_LEVEL" => 2
		)
	),
		Array(
		"Пункт меню 2 мараа", 
		"/punkt_link/", 
		Array("/punkt_link2/"), 
		Array(
			"FROM_IBLOCK" => 0,
			"IS_PARENT" => 0,
			"DEPTH_LEVEL" => 2
		)
	),		
);*/
?>